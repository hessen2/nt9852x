/**
    NVT max freq header file
    This file will provide NVT clock related structure & API
    @file       nvt-im-maxfreq.h
    @ingroup
    @note
    Copyright   Novatek Microelectronics Corp. 2019.  All rights reserved.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.
*/
#ifndef __NVT_IM_MAXFREQ_H__
#define __NVT_IM_MAXFREQ_H__

#if defined(CONFIG_COMMON_CLK_NA51055) || defined(CONFIG_COMMON_CLK_NA51089)
	#include "nvt-im-fr-maxfreq.h"
#endif

#endif
