#ifndef __NVT_PERF_H__
#define __NVT_PERF_H__

#define NVT_PERF_IOC_MAGIC   'P'

#define NVT_PERF_CACHE      _IO(NVT_PERF_IOC_MAGIC, 1)

#endif
