/*
	Register definition header file for CG module of NA51055.

	This file is the header file that define the address offset
	definition of registers of CG module.

	@file       pll.h

	Copyright   Novatek Microelectronics Corp. 2020.  All rights reserved
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License version 2 as
	published by the Free Software Foundation.
*/

#ifndef _CG_REG_H
#define _CG_REG_H

#include "hardware.h"

void nvt_clk_disable_childless(void);
#endif
