/**
    nvt ahbc barrier header file

    Copyright   Novatek Microelectronics Corp. 2019.  All rights reserved.
*/
#ifndef _NVT_AHBC_H
#define _NVT_AHBC_H
#include <mach/nvt_type.h>

u32 ahbc_trigger_read(void);

#endif /* _NVT_POWER_H */
