/**
    NVT common header

    @file       nvt-info.h
    @ingroup
    @note
    Copyright   Novatek Microelectronics Corp. 2019.  All rights reserved.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.
*/

#ifndef __ARCH_ARM_MACH_NVT_IVOT_NVT_INFO_H
#define __ARCH_ARM_MACH_NVT_IVOT_NVT_INFO_H

void nvt_bootts_add_ts(char *name);

#endif /* __ARCH_ARM_MACH_NVT_IVOT_NVT_INFO_H */
