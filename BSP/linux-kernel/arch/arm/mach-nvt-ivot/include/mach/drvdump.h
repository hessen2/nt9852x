/*
	Drvdump header
	Copyright Novatek Microelectronics Corp. 2019.  All rights reserved.
*/

#ifndef __ASM_ARCH_NVT_IVOT_DRVDUMP_H
#define __ASM_ARCH_NVT_IVOT_DRVDUMP_H

void nand_drvdump(void);

void sdio_drvdump(void);
#endif /* __ASM_ARCH_NVT_IVOT_DRVDUMP_H */

