/**
    NVT common header
 
    @file       common.h
    @ingroup
    @note
    Copyright   Novatek Microelectronics Corp. 2019.  All rights reserved.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.
*/

#ifndef __ASM_ARCH_NVT_IVOT_COMMON_H
#define __ASM_ARCH_NVT_IVOT_COMMON_H
void nvt_ivot_restart(enum reboot_mode mode, const char *cmd);
#endif /* __ASM_ARCH_NVT_IVOT_COMMON_H */