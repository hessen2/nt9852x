#ifndef IMAGEAPP_COMMON_H
#define IMAGEAPP_COMMON_H

typedef struct {
	UINT32                 id;
	char                   path[32];
	UINT32                 addr;
} SENSOR_PATH_INFO;

extern ER ImageApp_Common_Init(void);
extern ER ImageApp_Common_Uninit(void);
extern ER ImageApp_Common_RtspStart(UINT32 id);
extern ER ImageApp_Common_RtspStop(UINT32 id);

#endif//IMAGEAPP_COMMON_H
