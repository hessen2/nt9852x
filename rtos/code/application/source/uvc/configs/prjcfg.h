#ifndef _PRJCFG_H_
#define _PRJCFG_H_

#ifndef ENABLE
#define ENABLE 1
#define DISABLE 0
#endif

//==============================================================================
//   SYSTEM FUNCTIONS
//==============================================================================
//..............................................................................
// boot
#define POWERON_FAST_BOOT      ENABLE    ///< enable to use multi-thread init
#define POWERON_FAST_BOOT_MSG  ENABLE    ///< disable boot msg for fast boot, but hard to debug
#define POWERON_BOOT_REPORT    DISABLE   ///< enable to show report after booting
#define POWERON_EXAM_FLASH_LOAD DISABLE  ///< exam load 16MB data from flash

// share info memory on fdt
#define SHMEM_PATH "/nvt_memory_cfg/shmem"

// flash storage mapping
#define STRG_OBJ_FW_FDT   STRG_OBJ_FW_RSV1
#define STRG_OBJ_FW_APP   STRG_OBJ_FW_RSV2
#define STRG_OBJ_FW_UBOOT STRG_OBJ_FW_RSV3
#define STRG_OBJ_FW_RTOS  STRG_OBJ_FW_RSV4
#define STRG_OBJ_FW_LINUX STRG_OBJ_FW_RSV5
#define STRG_OBJ_FW_ROOTFS STRG_OBJ_FW_RSV6
#define STRG_OBJ_FW_ALL   STRG_OBJ_FW_RSV7

#endif
