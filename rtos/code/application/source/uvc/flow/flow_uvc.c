#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <hdal.h>
#include <kwrap/perf.h>
#include <kwrap/debug.h>
#include <kwrap/util.h>
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h>
#include "prjcfg.h"
#include "sys_fastboot.h"
#include "flow_uvc.h"
#include "vendor_videocapture.h"
#include "vendor_videoenc.h"
#include "vendor_audiocapture.h"
#define ENC_TYPE 1 // 0:JPEG 1:H264
#define AE_SKIP_FRAME 17 // Novatek AE shall process 17 frame for stable.



//#define RESOLUTION_SET  0 //0: 2M(IMX290), 1:5M(OS05A) 2: 2M (OS02K10) 3: 2M (AR0237IR) 4 : F37
#if defined(_sen_imx290_)
#define RESOLUTION_SET  0
#elif defined(_sen_os05a10_)
#define RESOLUTION_SET  1
#elif defined(_sen_os02k10_)
#define RESOLUTION_SET  2
#elif defined(_sen_ar0237ir_)
#define RESOLUTION_SET  3
#elif defined(_sen_f37_)
#define RESOLUTION_SET  4
#elif defined(_sen_f35_)
#define RESOLUTION_SET  5
#else
#error "un-implemented sensor for flow_preview.c"
#endif

#if ( RESOLUTION_SET == 0)
#define VDO_SIZE_W       1920
#define VDO_SIZE_H       1080
#define SEN_DRVIVER_NAME "nvt_sen_imx290"
#define SEN_IF_TYPE      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX       0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0        0
#define PIN_MAP_1        1
#define PIN_MAP_2        HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_3        HD_VIDEOCAP_SEN_IGNORE
#elif (RESOLUTION_SET == 1)
#define VDO_SIZE_W       2592
#define VDO_SIZE_H       1944
#define SEN_DRVIVER_NAME "nvt_sen_os05a10"
#define SEN_IF_TYPE      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX       0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0        0
#define PIN_MAP_1        1
#define PIN_MAP_2        2
#define PIN_MAP_3        3
#elif ( RESOLUTION_SET == 2)
#define VDO_SIZE_W       1920
#define VDO_SIZE_H       1080
#define SEN_DRVIVER_NAME "nvt_sen_os02k10"
#define SEN_IF_TYPE      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX       0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0        0
#define PIN_MAP_1        1
#define PIN_MAP_2        2
#define PIN_MAP_3        3
#elif ( RESOLUTION_SET == 3)
#define VDO_SIZE_W       1920
#define VDO_SIZE_H       1080
#define SEN_DRVIVER_NAME "nvt_sen_ar0237ir"
#define SEN_IF_TYPE      HD_COMMON_VIDEO_IN_P_RAW
#define SEN_PINMUX       0x204; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0        0
#define PIN_MAP_1        1
#define PIN_MAP_2        2
#define PIN_MAP_3        3
#elif ( RESOLUTION_SET == 4)
#define VDO_SIZE_W       1920
#define VDO_SIZE_H       1080
#define SEN_DRVIVER_NAME "nvt_sen_f37"
#define SEN_IF_TYPE      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX       0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0        0
#define PIN_MAP_1        1
#elif ( RESOLUTION_SET == 5)
#define VDO_SIZE_W       1920
#define VDO_SIZE_H       1080
#define SEN_DRVIVER_NAME "nvt_sen_f35"
#define SEN_IF_TYPE      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX       0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0        0
#define PIN_MAP_1        1
#endif

#if ((RESOLUTION_SET == 4) || (RESOLUTION_SET == 5))
#define SERIAL_IF_PINMUX  0x301;//0xf01;//PIN_MIPI_LVDS_CFG_CLK2 | PIN_MIPI_LVDS_CFG_DAT0 | PIN_MIPI_LVDS_CFG_DAT1
#else
#define SERIAL_IF_PINMUX  0xf01;//0xf01;//PIN_MIPI_LVDS_CFG_CLK2 | PIN_MIPI_LVDS_CFG_DAT0 | PIN_MIPI_LVDS_CFG_DAT1 | PIN_MIPI_LVDS_CFG_DAT2 | PIN_MIPI_LVDS_CFG_DAT3
#endif

#define CMD_IF_PINMUX     0x10;//PIN_I2C_CFG_CH2
#define CLK_LANE_SEL      HD_VIDEOCAP_SEN_CLANE_SEL_CSI0_USE_C0

#if ((RESOLUTION_SET == 4) || (RESOLUTION_SET == 5))
#define PIN_MAP_2       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_3       HD_VIDEOCAP_SEN_IGNORE
#endif
#define PIN_MAP_4       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_5       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_6       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_7       HD_VIDEOCAP_SEN_IGNORE

#define UVC_ON 1
#define AUDIO_ON 1
#define UVC_SUPPORT_YUV_FMT 0

#define COMMON_RECFRM_BUFFER 1
#define COMMON_RECFRM_FUNC_BASE   0
#define COMMON_RECFRM_FUNC_SVC    1
#define COMMON_RECFRM_FUNC_LTR    0

#if UVC_ON
#include "vendor_videoenc.h"
#include <sys/types.h>
#include <stdarg.h>
#include <compiler.h>
#include <usb2dev.h>
#include "UVAC.h"
#include "comm/timer.h"
#include "kwrap/stdio.h"
#include "kwrap/type.h"
#include "kwrap/semaphore.h"
#include "kwrap/task.h"
#include "kwrap/examsys.h"
#include "kwrap/sxcmd.h"
#include <kwrap/cmdsys.h>
#include "kwrap/error_no.h"
#include <kwrap/util.h>
#include "hd_common.h"
#include "FileSysTsk.h"
#include <sys/stat.h>

#define BULK_PACKET_SIZE    512

#define USB_VID                         0x0603
#define USB_PID_PCCAM                   0x8612 // not support pc cam
#define USB_PID_WRITE                   0x8614
#define USB_PID_PRINT                   0x8613
#define USB_PID_MSDC                    0x8611

#define USB_PRODUCT_REVISION            '1', '.', '0', '0'
#define USB_VENDER_DESC_STRING          'N', 0x00,'O', 0x00,'V', 0x00,'A', 0x00,'T', 0x00,'E', 0x00,'K', 0x00, 0x20, 0x00,0x00, 0x00 // NULL
#define USB_VENDER_DESC_STRING_LEN      0x09
#define USB_PRODUCT_DESC_STRING         'D', 0x00,'E', 0x00,'M', 0x00,'O', 0x00,'1', 0x00, 0x20, 0x00,0x00, 0x00 // NULL
#define USB_PRODUCT_DESC_STRING_LEN     0x07
#define USB_PRODUCT_STRING              'N','v','t','-','D','S','C'
#define USB_SERIAL_NUM_STRING           '9', '8', '5', '2', '0','0', '0', '0', '0', '0','0', '0', '1', '0', '0'
#define USB_VENDER_STRING               'N','O','V','A','T','E','K'
#define USB_VENDER_SIDC_DESC_STRING     'N', 0x00,'O', 0x00,'V', 0x00,'A', 0x00,'T', 0x00,'E', 0x00,'K', 0x00, 0x20, 0x00,0x00, 0x00 // NULL
#define USB_VENDER_SIDC_DESC_STRING_LEN 0x09



#define UVC_MJPEG_STREAM_BUF_MARGIN 110 //%
#define UVC_MJPEG_ENC_BLK_CNT   3
#define UVC_MJPEG_ENC_BUF_MS(fps) (1000*UVC_MJPEG_ENC_BLK_CNT*UVC_MJPEG_STREAM_BUF_MARGIN/100/(fps))

#define UVC_H264_ENC_BUF_MS 1000

typedef struct {
	UINT32  uiWidth;
	UINT32  uiHeight;
	UINT32  uiVidFrameRate;
	UINT32  uiH264TBR;
	UINT32  uiMJPEGTBR;
} UVC_TARGET_SIZE_PARAM;

static UVC_TARGET_SIZE_PARAM gUIUvcSizeTable[] = {
							//h.264			//MJPG
	{1920,	1080,	30,		625*1024,		13*1024*1024},
	{1280,  720,	30,		(312*1024)+512,	6*1024*1024},
};

#define NVT_UI_UVAC_RESO_CNT  2
static UVAC_VID_RESO gUIUvacVidReso[NVT_UI_UVAC_RESO_CNT] = {
	{ 1920,   1080,   1,      30,      0,      0},
	{ 1280,    720,   1,      30,      0,      0},
};
#define NVT_UI_UVAC_AUD_SAMPLERATE_CNT                  1
//#define NVT_UI_FREQUENCY_16K                    0x003E80   //16k
#define NVT_UI_FREQUENCY_32K                    0x007D00   //32k
//#define NVT_UI_FREQUENCY_48K                    0x00BB80   //48k
static UINT32 gUIUvacAudSampleRate[NVT_UI_UVAC_AUD_SAMPLERATE_CNT] = {
	NVT_UI_FREQUENCY_32K
};
#if UVC_SUPPORT_YUV_FMT
static UVAC_VID_RESO gUIUvacVidYuvReso[] = {
	{640,	480,	1,		10,		 0,		 0},
	{320,	240,	1,		30,		 0,		 0},
};
#endif
static  UVAC_VIDEO_FORMAT m_VideoFmt[UVAC_VID_DEV_CNT_MAX] = {0}; //UVAC_VID_DEV_CNT_MAX=2
static BOOL m_bIsStaticPattern = FALSE;

//static UINT32 uigFrameIdx = 0, uigAudIdx = 0;
static UINT32 uvac_pa = 0;
static UINT32 uvac_va = 0;
static UINT32 uvac_size = 0;
static BOOL UVAC_enable(void);
//static BOOL UVAC_disable(void);
#endif

///////////////////////////////////////////////////////////////////////////////

//header
#define DBGINFO_BUFSIZE()	(0x200)

//RAW
#define VDO_RAW_BUFSIZE(w, h, pxlfmt)   (ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NRX: RAW compress: Only support 12bit mode
#define RAW_COMPRESS_RATIO 59
#define VDO_NRX_BUFSIZE(w, h)           (ALIGN_CEIL_4(ALIGN_CEIL_64(w) * 12 / 8 * RAW_COMPRESS_RATIO / 100 * (h)))
//CA for AWB
#define VDO_CA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 3) << 1)
//LA for AE
#define VDO_LA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 1) << 1)

//YUV
#define VDO_YUV_BUFSIZE(w, h, pxlfmt)	(ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NVX: YUV compress
#define YUV_COMPRESS_RATIO 75
#define VDO_NVX_BUFSIZE(w, h, pxlfmt)	(VDO_YUV_BUFSIZE(w, h, pxlfmt) * YUV_COMPRESS_RATIO / 100)

///////////////////////////////////////////////////////////////////////////////

#define SEN_OUT_FMT		HD_VIDEO_PXLFMT_RAW12
#define CAP_OUT_FMT		HD_VIDEO_PXLFMT_RAW12
#define CA_WIN_NUM_W		32
#define CA_WIN_NUM_H		32
#define LA_WIN_NUM_W		32
#define LA_WIN_NUM_H		32
#define VA_WIN_NUM_W		16
#define VA_WIN_NUM_H		16
#define YOUT_WIN_NUM_W	128
#define YOUT_WIN_NUM_H	128
#define ETH_8BIT_SEL		0 //0: 2bit out, 1:8 bit out
#define ETH_OUT_SEL		1 //0: full, 1: subsample 1/2

///////////////////////////////////////////////////////////////////////////////
static UINT32 g_shdr_mode = 0;
///////////////////////////////////////////////////////////////////////////////
HD_VIDEOENC_BUFINFO phy_buf_main;
HD_AUDIOCAP_BUFINFO phy_buf_main2;
UINT32 vir_addr_main;
UINT32 vir_addr_main2;
pthread_t cap_thread_id;
UINT32     flow_start=0;
static UINT32 g_capbind = 1;  //0:D2D, 1:direct, 2: one-buf, 0xff: no-bind
static UINT32 g_capfmt = 0; //0:RAW, 1:RAW-compress
static UINT32 g_prcbind = 2;  //0:D2D, 1:lowlatency, 2: one-buf, 0xff: no-bind
static UINT32 g_3dnr = 1;    //0:disable, 1:enable 3DNR

#define JPG_YUV_TRANS 1

typedef struct _VIDEO_RECORD {

	// (1)
	HD_VIDEOCAP_SYSCAPS cap_syscaps;
	HD_PATH_ID cap_ctrl;
	HD_PATH_ID cap_path;

	HD_DIM  cap_dim;
	HD_DIM  proc_max_dim;

	// (2)
	HD_VIDEOPROC_SYSCAPS proc_syscaps;
	HD_PATH_ID proc_ctrl;
	HD_PATH_ID proc_path;

	HD_DIM  enc_max_dim;
	HD_DIM  enc_dim;

	// (3)
	HD_VIDEOENC_SYSCAPS enc_syscaps;
	HD_PATH_ID enc_path;

	// (4) user pull
	pthread_t  enc_thread_id;
	UINT32     enc_exit;
	UINT32     enc_start;
	UINT32     codec_type;

} VIDEO_RECORD;
static VIDEO_RECORD stream[1] = {0}; //0: main stream

#if  AUDIO_ON
typedef struct _AUDIO_CAPONLY {
	HD_AUDIO_SR sample_rate_max;
	HD_AUDIO_SR sample_rate;

	HD_PATH_ID cap_ctrl;
	HD_PATH_ID cap_path;

	UINT32 cap_exit;
} AUDIO_CAPONLY;

AUDIO_CAPONLY au_cap = {0};
#endif

static void GetUvcTBR(UVAC_VIDEO_FORMAT codec, UINT32 width, UINT32 height, UINT32 fps, UINT32 *pTBR)
{
	UINT32 tbr = 0x708000;			//Max TBR in table
	UINT32 i = 0;
	UINT32 ItemNum = sizeof(gUIUvcSizeTable) / sizeof(UVC_TARGET_SIZE_PARAM);
	for (i = 0; i < ItemNum; i++) {
		if ((gUIUvcSizeTable[i].uiWidth == width) && (gUIUvcSizeTable[i].uiHeight == height) && (gUIUvcSizeTable[i].uiVidFrameRate == fps)) {
			if (codec == UVAC_VIDEO_FORMAT_H264) {
				tbr = gUIUvcSizeTable[i].uiH264TBR;
			} else { //MJPEG
				tbr = gUIUvcSizeTable[i].uiMJPEGTBR;
			}
			break;
		}
	}
	if (ItemNum <= i) {
		printf("No Match Reso(%d, %d, %d) in gUIUvcSizeTable\r\n", (int)width, (int)height, (int)fps);
	}
	printf(":w=%d,%d,fps=%d,tbr=%d\r\n", (int)width, (int)height, (int)fps, (int)tbr);
	if (pTBR) {
		*pTBR = tbr;
	} else {
		DBG_ERR("NULL Input pTBR\r\n");
	}
}
static HD_RESULT mem_init(void)
{
	HD_RESULT              ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};

	// config common pool (cap)
	mem_cfg.pool_info[0].type = HD_COMMON_MEM_COMMON_POOL;
	if ((g_capbind == 0) || (g_capbind == 0xff)) {
		mem_cfg.pool_info[0].blk_size = DBGINFO_BUFSIZE()
															+VDO_CA_BUF_SIZE(CA_WIN_NUM_W, CA_WIN_NUM_H)
															+VDO_LA_BUF_SIZE(LA_WIN_NUM_W, LA_WIN_NUM_H);
		if (g_capfmt == HD_VIDEO_PXLFMT_NRX12)
			mem_cfg.pool_info[0].blk_size += VDO_NRX_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H);
		else
			mem_cfg.pool_info[0].blk_size += VDO_RAW_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, g_capfmt);
		mem_cfg.pool_info[0].blk_cnt = 2;
		mem_cfg.pool_info[0].ddr_id = DDR_ID0;
	} else if (g_capbind == 1) {
		//direct ... NOT require raw
		mem_cfg.pool_info[0].blk_size = DBGINFO_BUFSIZE()
															+VDO_CA_BUF_SIZE(CA_WIN_NUM_W, CA_WIN_NUM_H)
															+VDO_LA_BUF_SIZE(LA_WIN_NUM_W, LA_WIN_NUM_H);
		mem_cfg.pool_info[0].blk_cnt = 2;
		mem_cfg.pool_info[0].ddr_id = DDR_ID0;
	} else if (g_capbind == 2) {
		//one-buf
		mem_cfg.pool_info[0].blk_size = DBGINFO_BUFSIZE()
															+VDO_CA_BUF_SIZE(CA_WIN_NUM_W, CA_WIN_NUM_H)
															+VDO_LA_BUF_SIZE(LA_WIN_NUM_W, LA_WIN_NUM_H);
		mem_cfg.pool_info[0].blk_cnt = 1;
		mem_cfg.pool_info[0].ddr_id = DDR_ID0;
	} else {
		//not support
		mem_cfg.pool_info[0].blk_size = 0;
		mem_cfg.pool_info[0].blk_cnt = 0;
		mem_cfg.pool_info[0].ddr_id = DDR_ID0;
	}

	// config common pool (main)
	mem_cfg.pool_info[1].type = HD_COMMON_MEM_COMMON_POOL;
	if ((g_prcbind == 0) || (g_prcbind == 0xff)) {
		mem_cfg.pool_info[1].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
		mem_cfg.pool_info[1].blk_cnt = 3;
		mem_cfg.pool_info[1].ddr_id = DDR_ID0;
	} else if (g_prcbind == 1) {
		//low-latency
		mem_cfg.pool_info[1].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
		mem_cfg.pool_info[1].blk_cnt = 2;
		mem_cfg.pool_info[1].ddr_id = DDR_ID0;
	} else if (g_prcbind == 2) {
		//one-buf
		mem_cfg.pool_info[1].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
		mem_cfg.pool_info[1].blk_cnt = 1;
		mem_cfg.pool_info[1].ddr_id = DDR_ID0;
	} else {
		//not support
		mem_cfg.pool_info[1].blk_size = 0;
		mem_cfg.pool_info[1].blk_cnt = 0;
		mem_cfg.pool_info[1].ddr_id = DDR_ID0;
	}
#if AUDIO_ON
	// config common pool audio (main)
	mem_cfg.pool_info[2].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[2].blk_size = 0x10000;
	mem_cfg.pool_info[2].blk_cnt = 2;
	mem_cfg.pool_info[2].ddr_id = DDR_ID0;
#endif
#if COMMON_RECFRM_BUFFER
	mem_cfg.pool_info[3].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[3].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(ALIGN_CEIL_64(VDO_SIZE_W), ALIGN_CEIL_64(VDO_SIZE_H), HD_VIDEO_PXLFMT_YUV420);
	mem_cfg.pool_info[3].blk_cnt = COMMON_RECFRM_FUNC_BASE + COMMON_RECFRM_FUNC_SVC + COMMON_RECFRM_FUNC_LTR;
	mem_cfg.pool_info[3].ddr_id = DDR_ID0;
#endif

	ret = hd_common_mem_init(&mem_cfg);
	return ret;
}

///////////////////////////////////////////////////////////////////////////////
/*AUDIO */
#if AUDIO_ON
static HD_RESULT set_cap_cfg2(HD_PATH_ID *p_audio_cap_ctrl, HD_AUDIO_SR sample_rate)
{
	HD_RESULT ret;
	HD_PATH_ID audio_cap_ctrl = 0;
	HD_AUDIOCAP_DEV_CONFIG audio_dev_cfg = {0};
	HD_AUDIOCAP_DRV_CONFIG audio_drv_cfg = {0};
	INT32 prepwr_enable = TRUE;

	ret = hd_audiocap_open(0, HD_AUDIOCAP_0_CTRL, &audio_cap_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}

	// set audiocap dev parameter
	audio_dev_cfg.in_max.sample_rate = (gUIUvacAudSampleRate[0] == NVT_UI_FREQUENCY_32K) ? HD_AUDIO_SR_32000 : HD_AUDIO_SR_16000;
	audio_dev_cfg.in_max.sample_bit = HD_AUDIO_BIT_WIDTH_16;
	audio_dev_cfg.in_max.mode = HD_AUDIO_SOUND_MODE_MONO;
	audio_dev_cfg.in_max.frame_sample = 1024;
	audio_dev_cfg.frame_num_max = 10;
	ret = hd_audiocap_set(audio_cap_ctrl, HD_AUDIOCAP_PARAM_DEV_CONFIG, &audio_dev_cfg);
	if (ret != HD_OK) {
		return ret;
	}

	// set audiocap drv parameter
	audio_drv_cfg.mono = HD_AUDIO_MONO_RIGHT;
	ret = hd_audiocap_set(audio_cap_ctrl, HD_AUDIOCAP_PARAM_DRV_CONFIG, &audio_drv_cfg);

	// set PREPWR
	vendor_audiocap_set(audio_cap_ctrl, VENDOR_AUDIOCAP_ITEM_PREPWR_ENABLE, &prepwr_enable);

	*p_audio_cap_ctrl = audio_cap_ctrl;

	return ret;
}

static HD_RESULT set_cap_param2(HD_PATH_ID audio_cap_path, HD_AUDIO_SR sample_rate)
{
	HD_RESULT ret = HD_OK;
	HD_AUDIOCAP_IN audio_cap_param = {0};

	// set audiocap input parameter
	audio_cap_param.sample_rate = (gUIUvacAudSampleRate[0] == NVT_UI_FREQUENCY_32K) ? HD_AUDIO_SR_32000 : HD_AUDIO_SR_16000;
	audio_cap_param.sample_bit = HD_AUDIO_BIT_WIDTH_16;
	audio_cap_param.mode = HD_AUDIO_SOUND_MODE_MONO;
	audio_cap_param.frame_sample = 1024;
	ret = hd_audiocap_set(audio_cap_path, HD_AUDIOCAP_PARAM_IN, &audio_cap_param);

	return ret;
}

static HD_RESULT open_module2(AUDIO_CAPONLY *p_caponly)
{
	HD_RESULT ret;
	ret = set_cap_cfg2(&p_caponly->cap_ctrl, p_caponly->sample_rate_max);
	if (ret != HD_OK) {
		printf("set cap-cfg fail(%d)\n", ret);
		return HD_ERR_NG;
	}
	if((ret = hd_audiocap_open(HD_AUDIOCAP_0_IN_0, HD_AUDIOCAP_0_OUT_0, &p_caponly->cap_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static void *capture_thread(void *arg)
{
	HD_RESULT ret = HD_OK;
	HD_AUDIO_FRAME  data_pull;
	int get_phy_flag=0;
	//unsigned int uiaud_sample_rate = (gUIUvacAudSampleRate[0] == NVT_UI_FREQUENCY_32K) ? 32000 : 16000;

	AUDIO_CAPONLY *p_cap_only = (AUDIO_CAPONLY *)arg;
#if UVC_ON
	UVAC_STRM_FRM strmFrm = {0};
#endif
	#define PHY2VIRT_MAIN2(pa) (vir_addr_main2 + (pa - phy_buf_main2.buf_info.phy_addr))





	/* pull data test */
	while (p_cap_only->cap_exit == 0) {

		if (!flow_start){
			if (get_phy_flag){
				/* mummap for bs buffer */
				//printf("release common buffer2\r\n");
				if (vir_addr_main2)hd_common_mem_munmap((void *)vir_addr_main2, phy_buf_main2.buf_info.buf_size);

				get_phy_flag = 0;
			}
			/* wait flow_start */
			usleep(10);
			continue;
		}else{
			if (!get_phy_flag){

				/* query physical address of bs buffer	(this can ONLY query after hd_audiocap_start() is called !!) */
				hd_audiocap_get(au_cap.cap_ctrl, HD_AUDIOCAP_PARAM_BUFINFO, &phy_buf_main2);

				/* mmap for bs buffer  (just mmap one time only, calculate offset to virtual address later) */
				vir_addr_main2 = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, phy_buf_main2.buf_info.phy_addr, phy_buf_main2.buf_info.buf_size);
				if (vir_addr_main2 == 0) {
					printf("mmap error\r\n");
					return 0;
				}

				get_phy_flag = 1;
			}
		}

		// pull data
		ret = hd_audiocap_pull_out_buf(p_cap_only->cap_path, &data_pull, 200); // >1 = timeout mode

		if (ret == HD_OK) {
			//UINT8 *ptr = (UINT8 *)PHY2VIRT_MAIN2(data_pull.phy_addr[0]);
			//UINT32 size = data_pull.size;
			//UINT32 timestamp = hd_gettime_ms();
			//printf("audio fram size =%u\r\n",size);
#if UVC_ON
			strmFrm.path = UVAC_STRM_AUD;
			strmFrm.addr = data_pull.phy_addr[0];
			strmFrm.size = data_pull.size;
			UVAC_SetEachStrmInfo(&strmFrm);
#endif


			// release data
			ret = hd_audiocap_release_out_buf(p_cap_only->cap_path, &data_pull);
			if (ret != HD_OK) {
				printf("release buffer failed. ret=%x\r\n", ret);
			}
		}

		//usleep(1000000/uiaud_sample_rate*2);
	}

	if (get_phy_flag){
		/* mummap for bs buffer */
		//printf("release common buffer2\r\n");
		if (vir_addr_main2)hd_common_mem_munmap((void *)vir_addr_main2, phy_buf_main2.buf_info.buf_size);
	}


	return 0;
}
#endif // AUDIO_ON
/*AUDIO end*/
static HD_RESULT set_cap_cfg(HD_PATH_ID *p_video_cap_ctrl)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOCAP_DRV_CONFIG cap_cfg = {0};
	HD_PATH_ID video_cap_ctrl = 0;
	HD_VIDEOCAP_CTRL iq_ctl = {0};

	snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, SEN_DRVIVER_NAME);
	cap_cfg.sen_cfg.sen_dev.if_type = SEN_IF_TYPE;
    cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  SEN_PINMUX;
    cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.serial_if_pinmux = SERIAL_IF_PINMUX;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = CMD_IF_PINMUX;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.clk_lane_sel = CLK_LANE_SEL;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[0] = PIN_MAP_0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[1] = PIN_MAP_1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = PIN_MAP_2;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = PIN_MAP_3;
	#if (RESOLUTION_SET == 0)
	if (g_shdr_mode == 1) {
		cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = 2;
		cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = 3;
	}
	#endif
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[4] = PIN_MAP_4;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[5] = PIN_MAP_5;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[6] = PIN_MAP_6;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[7] = PIN_MAP_7;

	printf("Using %s\n", cap_cfg.sen_cfg.sen_dev.driver_name);
	if (cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux == HD_COMMON_VIDEO_IN_MIPI_CSI){
	    printf("Parallel interface\n");
    } else if (cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux == HD_COMMON_VIDEO_IN_MIPI_CSI){
	    printf("MIPI interface\n");
    }
    if (g_shdr_mode == 1) {
		printf("Using g_shdr_mode\n");
	}
	ret = hd_videocap_open(0, HD_VIDEOCAP_0_CTRL, &video_cap_ctrl); //open this for device control

	if (ret != HD_OK) {
		return ret;
	}
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_DRV_CONFIG, &cap_cfg);
	iq_ctl.func = HD_VIDEOCAP_FUNC_AE | HD_VIDEOCAP_FUNC_AWB;
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_CTRL, &iq_ctl);

	*p_video_cap_ctrl = video_cap_ctrl;
	return ret;
}

static HD_RESULT set_cap_param(HD_PATH_ID video_cap_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	{//select sensor mode, manually or automatically
		HD_VIDEOCAP_IN video_in_param = {0};

		video_in_param.sen_mode = HD_VIDEOCAP_SEN_MODE_AUTO; //auto select sensor mode by the parameter of HD_VIDEOCAP_PARAM_OUT
		video_in_param.frc = HD_VIDEO_FRC_RATIO(30,1);
		video_in_param.dim.w = p_dim->w;
		video_in_param.dim.h = p_dim->h;
		video_in_param.pxlfmt = SEN_OUT_FMT;
		video_in_param.out_frame_num = HD_VIDEOCAP_SEN_FRAME_NUM_1;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN, &video_in_param);
		//printf("set_cap_param MODE=%d\r\n", ret);
		if (ret != HD_OK) {
			return ret;
		}
	}
	#if 1 //no crop, full frame
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_OFF;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN_CROP, &video_crop_param);
		//printf("set_cap_param CROP NONE=%d\r\n", ret);
	}
	#else //HD_CROP_ON
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_ON;
		video_crop_param.win.rect.x = 0;
		video_crop_param.win.rect.y = 0;
		video_crop_param.win.rect.w = MAX_CAP_SIZE_W/2;
		video_crop_param.win.rect.h= MAX_CAP_SIZE_H/2;
		video_crop_param.align.w = 4;
		video_crop_param.align.h = 4;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN_CROP, &video_crop_param);
		//printf("set_cap_param CROP ON=%d\r\n", ret);
	}
	#endif
	{
		HD_VIDEOCAP_OUT video_out_param = {0};

		//without setting dim for no scaling, using original sensor out size
		video_out_param.pxlfmt = g_capfmt;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		video_out_param.depth = 0;
		if (g_capbind == 0xff) //no-bind mode
			video_out_param.depth = 1;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_OUT, &video_out_param);
		//printf("set_cap_param OUT=%d\r\n", ret);
	}
	{
		HD_VIDEOCAP_FUNC_CONFIG video_path_param = {0};

		video_path_param.out_func = 0;
		if (g_capbind == 1) //direct mode
			video_path_param.out_func = HD_VIDEOCAP_OUTFUNC_DIRECT;
		if (g_capbind == 2) //one-buf mode
			video_path_param.out_func = HD_VIDEOCAP_OUTFUNC_ONEBUF;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_FUNC_CONFIG, &video_path_param);
		//printf("set_cap_param PATH_CONFIG=0x%X\r\n", ret);
	}
	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_proc_cfg(HD_PATH_ID *p_video_proc_ctrl, HD_DIM* p_max_dim)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOPROC_DEV_CONFIG video_cfg_param = {0};
	HD_VIDEOPROC_CTRL video_ctrl_param = {0};
	HD_PATH_ID video_proc_ctrl = 0;

	ret = hd_videoproc_open(0, HD_VIDEOPROC_0_CTRL, &video_proc_ctrl); //open this for device control
	if (ret != HD_OK)
		return ret;

	if (p_max_dim != NULL ) {
		video_cfg_param.pipe = HD_VIDEOPROC_PIPE_RAWALL;
		video_cfg_param.isp_id = 0;
		video_cfg_param.ctrl_max.func = 0;
		if (g_3dnr == 1) {
			video_cfg_param.ctrl_max.func |= HD_VIDEOPROC_FUNC_3DNR | HD_VIDEOPROC_FUNC_3DNR_STA;
		}
		video_cfg_param.in_max.func = 0;
		video_cfg_param.in_max.dim.w = p_max_dim->w;
		video_cfg_param.in_max.dim.h = p_max_dim->h;
		video_cfg_param.in_max.pxlfmt = g_capfmt;
		video_cfg_param.in_max.frc = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_DEV_CONFIG, &video_cfg_param);
		if (ret != HD_OK) {
			return HD_ERR_NG;
		}
	}

	{
		HD_VIDEOPROC_FUNC_CONFIG video_path_param = {0};

		video_path_param.in_func = 0;
		if (g_capbind == 1)
			video_path_param.in_func |= HD_VIDEOPROC_INFUNC_DIRECT; //direct NOTE: enable direct
		if (g_capbind == 2)
			video_path_param.in_func |= HD_VIDEOPROC_INFUNC_ONEBUF;
		ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_FUNC_CONFIG, &video_path_param);
		//printf("set_proc_param PATH_CONFIG=0x%X\r\n", ret);
	}

	video_ctrl_param.func = 0;
	if (g_3dnr == 1) {
		video_ctrl_param.func |= HD_VIDEOPROC_FUNC_3DNR | HD_VIDEOPROC_FUNC_3DNR_STA;
		//use current path as reference path (if venc.dim == raw.dim)
		video_ctrl_param.ref_path_3dnr = HD_VIDEOPROC_0_OUT_0;
	}
	ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_CTRL, &video_ctrl_param);

	*p_video_proc_ctrl = video_proc_ctrl;

	return ret;
}

static HD_RESULT set_proc_param(HD_PATH_ID video_proc_path, HD_DIM* p_dim)
{
	HD_RESULT ret = HD_OK;

	if (p_dim != NULL) { //if videoproc is already binding to dest module, not require to setting this!
		HD_VIDEOPROC_OUT video_out_param = {0};
		video_out_param.func = 0;
		video_out_param.dim.w = p_dim->w;
		video_out_param.dim.h = p_dim->h;
		video_out_param.pxlfmt = HD_VIDEO_PXLFMT_YUV420;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		video_out_param.frc = HD_VIDEO_FRC_RATIO(1,1);
		video_out_param.depth = 1; //set 1 to allow pull
		ret = hd_videoproc_set(video_proc_path, HD_VIDEOPROC_PARAM_OUT, &video_out_param);
	}

	{
		HD_VIDEOPROC_FUNC_CONFIG video_path_param = {0};

		video_path_param.out_func = 0;
		if (g_prcbind == 1)
			video_path_param.out_func |= HD_VIDEOPROC_OUTFUNC_LOWLATENCY; //enable low-latency
		if (g_prcbind == 2)
			video_path_param.out_func |= HD_VIDEOPROC_OUTFUNC_ONEBUF;
		ret = hd_videoproc_set(video_proc_path, HD_VIDEOPROC_PARAM_FUNC_CONFIG, &video_path_param);
	}

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_enc_cfg(HD_PATH_ID video_enc_path, HD_DIM *p_max_dim, UINT32 max_bitrate, HD_VIDEO_CODEC codec_type)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOENC_PATH_CONFIG video_path_config = {0};
	HD_VIDEOENC_FUNC_CONFIG video_func_config = {0};
	VENDOR_VIDEOENC_H26X_ENC_COLMV h26x_enc_colmv = {0};
	VENDOR_VIDEOENC_FIT_WORK_MEMORY fit_work_mem_cfg = {0};

    fit_work_mem_cfg.b_enable = TRUE;
    vendor_videoenc_set(video_enc_path, VENDOR_VIDEOENC_PARAM_OUT_FIT_WORK_MEMORY, &fit_work_mem_cfg);

	h26x_enc_colmv.h26x_colmv_en = 0;
	vendor_videoenc_set(video_enc_path, VENDOR_VIDEOENC_PARAM_OUT_COLMV, &h26x_enc_colmv);

#if COMMON_RECFRM_BUFFER
	VENDOR_VIDEOENC_H26X_COMM_RECFRM h26x_comm_recfrm = {0};

	h26x_comm_recfrm.enable = 1;
	h26x_comm_recfrm.h26x_comm_base_recfrm_en = COMMON_RECFRM_FUNC_BASE;
	h26x_comm_recfrm.h26x_comm_svc_recfrm_en = COMMON_RECFRM_FUNC_SVC;
	h26x_comm_recfrm.h26x_comm_ltr_recfrm_en = COMMON_RECFRM_FUNC_LTR;
	vendor_videoenc_set(video_enc_path, VENDOR_VIDEOENC_PARAM_OUT_COMM_RECFRM, &h26x_comm_recfrm);
#endif

	if (p_max_dim != NULL) {

		//--- HD_VIDEOENC_PARAM_PATH_CONFIG ---
		video_path_config.max_mem.codec_type = codec_type;
		video_path_config.max_mem.max_dim.w  = p_max_dim->w;
		video_path_config.max_mem.max_dim.h  = p_max_dim->h;
		video_path_config.max_mem.bitrate    = max_bitrate;
		if (HD_CODEC_TYPE_JPEG == codec_type) {
			video_path_config.max_mem.enc_buf_ms = UVC_MJPEG_ENC_BUF_MS(30);
		} else {
			video_path_config.max_mem.enc_buf_ms = UVC_H264_ENC_BUF_MS;
			video_path_config.max_mem.svc_layer  = HD_SVC_4X;
			video_path_config.max_mem.ltr        = FALSE;
			video_path_config.max_mem.rotate     = FALSE;
			video_path_config.max_mem.source_output   = FALSE;
		}
		video_path_config.isp_id             = 0;
		ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_PATH_CONFIG, &video_path_config);
		if (ret != HD_OK) {
			printf("set_enc_path_config = %d\r\n", ret);
			return HD_ERR_NG;
		}

		video_func_config.in_func = 0;
		if (g_prcbind == 1)
			video_func_config.in_func |= HD_VIDEOENC_INFUNC_LOWLATENCY; //enable low-latency
		if (g_prcbind == 2)
			video_func_config.in_func |= HD_VIDEOENC_INFUNC_ONEBUF;

		ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_FUNC_CONFIG, &video_func_config);
		if (ret != HD_OK) {
			printf("set_enc_path_config = %d\r\n", ret);
			return HD_ERR_NG;
		}
	}

	return ret;
}

static HD_RESULT set_enc_param(HD_PATH_ID video_enc_path, HD_DIM *p_dim, HD_VIDEO_CODEC enc_type, UINT32 bitrate)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOENC_IN  video_in_param = {0};
	HD_VIDEOENC_OUT2 video_out_param = {0};
	HD_H26XENC_RATE_CONTROL2 rc_param = {0};

	if (p_dim != NULL) {

		//--- HD_VIDEOENC_PARAM_IN ---
		video_in_param.dir           = HD_VIDEO_DIR_NONE;
		video_in_param.pxl_fmt = HD_VIDEO_PXLFMT_YUV420;
		video_in_param.dim.w   = p_dim->w;
		video_in_param.dim.h   = p_dim->h;
		video_in_param.frc     = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_IN, &video_in_param);
		if (ret != HD_OK) {
			printf("set_enc_param_in = %d\r\n", ret);
			return ret;
		}

		//printf("enc_type=%d\r\n", enc_type);

		if (enc_type == HD_CODEC_TYPE_H264) {
			VENDOR_VIDEOENC_MIN_RATIO_CFG min_ratio = {0};

			min_ratio.min_i_ratio = UVC_H264_ENC_BUF_MS/2;
			min_ratio.min_p_ratio = UVC_H264_ENC_BUF_MS/3;
			vendor_videoenc_set(video_enc_path, VENDOR_VIDEOENC_PARAM_OUT_MIN_RATIO, &min_ratio);

			//--- HD_VIDEOENC_PARAM_OUT_ENC_PARAM ---
			video_out_param.codec_type         = HD_CODEC_TYPE_H264;
			video_out_param.h26x.profile       = HD_H264E_HIGH_PROFILE;
			video_out_param.h26x.level_idc     = HD_H264E_LEVEL_5_1;
			video_out_param.h26x.gop_num       = 50;
			video_out_param.h26x.ltr_interval  = 0;
			video_out_param.h26x.ltr_pre_ref   = 0;
			video_out_param.h26x.gray_en       = 0;
			video_out_param.h26x.source_output = 0;
			video_out_param.h26x.svc_layer     = HD_SVC_4X;
			video_out_param.h26x.entropy_mode  = HD_H264E_CABAC_CODING;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_ENC_PARAM2, &video_out_param);
			if (ret != HD_OK) {
				printf("set_enc_param_out = %d\r\n", ret);
				return ret;
			}

			//--- HD_VIDEOENC_PARAM_OUT_RATE_CONTROL ---
			rc_param.rc_mode             = HD_RC_MODE_CBR;
			rc_param.cbr.bitrate         = bitrate;
			rc_param.cbr.frame_rate_base = 30;
			rc_param.cbr.frame_rate_incr = 1;
			rc_param.cbr.init_i_qp       = 26;
			rc_param.cbr.min_i_qp        = 10;
			rc_param.cbr.max_i_qp        = 45;
			rc_param.cbr.init_p_qp       = 26;
			rc_param.cbr.min_p_qp        = 10;
			rc_param.cbr.max_p_qp        = 45;
			rc_param.cbr.static_time     = 4;
			rc_param.cbr.ip_weight       = 0;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_RATE_CONTROL2, &rc_param);
			if (ret != HD_OK) {
				printf("set_enc_rate_control = %d\r\n", ret);
				return ret;
			}

		} else if (enc_type == HD_CODEC_TYPE_JPEG) {
			VENDOR_VIDEOENC_MIN_RATIO_CFG min_ratio = {0};

			min_ratio.min_i_ratio = 1000*UVC_MJPEG_STREAM_BUF_MARGIN/100/30;//ms
			min_ratio.min_p_ratio = min_ratio.min_i_ratio;
		   //printf("min_ratio.min_i_ratio=%lu\r\n", min_ratio.min_i_ratio);
			vendor_videoenc_set(video_enc_path, VENDOR_VIDEOENC_PARAM_OUT_MIN_RATIO, &min_ratio);
			//--- HD_VIDEOENC_PARAM_OUT_ENC_PARAM ---
			video_out_param.codec_type         = HD_CODEC_TYPE_JPEG;
			video_out_param.jpeg.retstart_interval = 0;
			video_out_param.jpeg.image_quality = 50;
			video_out_param.jpeg.bitrate = bitrate;
			video_out_param.jpeg.frame_rate_base = 30;
			video_out_param.jpeg.frame_rate_incr = 1;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_ENC_PARAM2, &video_out_param);
			if (ret != HD_OK) {
				printf("set_enc_param_out = %d\r\n", ret);
				return ret;
			}
#if JPG_YUV_TRANS
			//MJPG YUV trans(YUV420 to YUV422)
			VENDOR_VIDEOENC_JPG_YUV_TRANS_CFG yuv_trans_cfg = {0};
			yuv_trans_cfg.jpg_yuv_trans_en = JPG_YUV_TRANS;
			//printf("MJPG YUV TRANS(%d)\r\n", JPG_YUV_TRANS);
			vendor_videoenc_set(video_enc_path, VENDOR_VIDEOENC_PARAM_OUT_JPG_YUV_TRANS, &yuv_trans_cfg);
#endif
		} else {

			printf("not support enc_type\r\n");
			return HD_ERR_NG;
		}
	}

	return ret;
}




static void fast_open_sensor(void)
{
	VENDOR_VIDEOCAP_FAST_OPEN_SENSOR cap_cfg = {0};

	snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, SEN_DRVIVER_NAME);
	cap_cfg.sen_cfg.sen_dev.if_type = SEN_IF_TYPE;
    cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  SEN_PINMUX;
    cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.serial_if_pinmux = SERIAL_IF_PINMUX;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = CMD_IF_PINMUX;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.clk_lane_sel = CLK_LANE_SEL;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[0] = PIN_MAP_0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[1] = PIN_MAP_1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = PIN_MAP_2;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = PIN_MAP_3;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[4] = PIN_MAP_4;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[5] = PIN_MAP_5;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[6] = PIN_MAP_6;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[7] = PIN_MAP_7;
	cap_cfg.sen_mode = 0;
	cap_cfg.frc = HD_VIDEO_FRC_RATIO(30,1);
	vendor_videocap_set(0, VENDOR_VIDEOCAP_PARAM_FAST_OPEN_SENSOR, &cap_cfg);
}

static HD_RESULT init_module_cap_proc(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_init()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_init()) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT init_module_enc(void)
{
	HD_RESULT ret;
    if ((ret = hd_videoenc_init()) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT open_module_cap_proc(VIDEO_RECORD *p_stream, HD_DIM* p_proc_max_dim)
{
	HD_RESULT ret;
	// set videocap config
	ret = set_cap_cfg(&p_stream->cap_ctrl);
	if (ret != HD_OK) {
		printf("set cap-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set videoproc config
	ret = set_proc_cfg(&p_stream->proc_ctrl, p_proc_max_dim);
	if (ret != HD_OK) {
		printf("set proc-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}

	if ((ret = hd_videocap_open(HD_VIDEOCAP_0_IN_0, HD_VIDEOCAP_0_OUT_0, &p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_0, &p_stream->proc_path)) != HD_OK)
		return ret;

	return HD_OK;
}

static HD_RESULT open_module_enc(VIDEO_RECORD *p_stream, HD_DIM* p_proc_max_dim)
{
	HD_RESULT ret;
	if ((ret = hd_videoenc_open(HD_VIDEOENC_0_IN_0, HD_VIDEOENC_0_OUT_0, &p_stream->enc_path)) != HD_OK)
		return ret;

	return HD_OK;
}

static void *thread_sensor(void *ptr)
{
	fastboot_wait_done(BOOT_INIT_SENSOR);
	vos_perf_list_mark("ss", __LINE__, 0);
	//quick sensor setup flow
	fast_open_sensor();
	vos_perf_list_mark("ss", __LINE__, 1);
	pthread_exit((void *)0);
	return NULL;
}

static void *thread_cap_proc(void *ptr)
{
	HD_RESULT ret;

	// wait sensor setup ready
	int join_ret;
	pthread_t *p_handle_sensor = (pthread_t *)ptr;
	int pthread_ret = pthread_join(*p_handle_sensor, (void *)&join_ret);
	if (0 != pthread_ret) {
		printf("thread_sensor pthread_join failed, ret %d\r\n", pthread_ret);
		pthread_exit((void *)-1);
		return NULL;
	}
	// wait driver init ready
	fastboot_wait_done(BOOT_INIT_CAPTURE);

	vos_perf_list_mark("capproc", __LINE__, 0);

	// init capture, proc
	ret = init_module_cap_proc();
	if (ret != HD_OK) {
		printf("init fail=%d\n", ret);
		pthread_exit((void *)-1);
		return NULL;
	}

	// open capture, proc
	stream[0].proc_max_dim.w = VDO_SIZE_W; //assign by user
	stream[0].proc_max_dim.h = VDO_SIZE_H; //assign by user
	ret = open_module_cap_proc(&stream[0], &stream[0].proc_max_dim);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		pthread_exit((void *)-1);
		return NULL;
	}

	// set videocap parameter
	stream[0].cap_dim.w = VDO_SIZE_W; //assign by user
	stream[0].cap_dim.h = VDO_SIZE_H; //assign by user
	ret = set_cap_param(stream[0].cap_path, &stream[0].cap_dim);
	if (ret != HD_OK) {
		printf("set cap fail=%d\n", ret);
		pthread_exit((void *)-1);
		return NULL;
	}

	// set videoproc parameter (main)
	ret = set_proc_param(stream[0].proc_path, &stream[0].cap_dim);
	if (ret != HD_OK) {
		printf("set proc fail=%d\n", ret);
		pthread_exit((void *)-1);
		return NULL;
	}

	hd_videocap_bind(HD_VIDEOCAP_0_OUT_0, HD_VIDEOPROC_0_IN_0);

	vos_perf_list_mark("capproc", __LINE__, 1);
	if (g_capbind == 1) {
		//direct NOTE: ensure videocap start after 1st videoproc phy path start
		hd_videoproc_start(stream[0].proc_path);
		vos_perf_list_mark("capproc", __LINE__, 2);
		hd_videocap_start(stream[0].cap_path);
	} else {
		hd_videocap_start(stream[0].cap_path);
		vos_perf_list_mark("capproc", __LINE__, 2);
		hd_videoproc_start(stream[0].proc_path);
	}
	vos_perf_list_mark("capproc", __LINE__, 3);

	HD_VIDEO_FRAME video_frame = {0};
	UINT32 skip = 0;

	// AE shall process AE_SKIP_FRAME frame for stable.
	while(skip < (AE_SKIP_FRAME-1) /*16*/ ) {
		hd_videoproc_pull_out_buf(stream[0].proc_path, &video_frame, -1);

		if (skip == 0) {
			vos_perf_list_mark("capproc", __LINE__, 4);
		}

		hd_videoproc_release_out_buf(stream[0].proc_path, &video_frame);
		skip++;
	}
	// skip to AE_SKIP_FRAME-1 and start encode
	stream[0].enc_start = 1;

	vos_perf_list_mark("capproc", __LINE__, 5);
	printf("skip: %lu\r\n", skip);

	pthread_exit((void *)0);
	return NULL;
}

static void *thread_enc(void *ptr)
{
	VIDEO_RECORD* p_stream0 = (VIDEO_RECORD *)ptr;
	HD_RESULT ret;
	UINT32 enc_type = stream[0].codec_type;
	UINT32 bit_rate;

	if (enc_type == HD_CODEC_TYPE_H264) {
		bit_rate = gUIUvcSizeTable[0].uiH264TBR*8;
	} else {
		bit_rate = gUIUvcSizeTable[0].uiMJPEGTBR*8;
	}

	// wait driver init ready

	fastboot_wait_done(BOOT_INIT_MEIDA_ENCODER); // Vanness can seperate to other thread.
	printf("##Wait init encode: done!!!!!\r\n");

	// init capture, proc
	ret = init_module_enc();
	if (ret != HD_OK) {
		printf("init fail=%d\n", ret);
		pthread_exit((void *)-1);
		return NULL;
	}

	// open capture, proc
	stream[0].proc_max_dim.w = VDO_SIZE_W; //assign by user
	stream[0].proc_max_dim.h = VDO_SIZE_H; //assign by user
	ret = open_module_enc(&stream[0], &stream[0].proc_max_dim);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		pthread_exit((void *)-1);
		return NULL;
	}

	// set videoenc config (main)
	stream[0].enc_max_dim.w = VDO_SIZE_W;
	stream[0].enc_max_dim.h = VDO_SIZE_H;
	ret = set_enc_cfg(stream[0].enc_path, &stream[0].enc_max_dim, bit_rate, enc_type);
	if (ret != HD_OK) {
		printf("set enc-cfg fail=%d\n", ret);
		pthread_exit((void *)-1);
		return NULL;
	}

	// set videoenc parameter (main)
	stream[0].enc_dim.w = VDO_SIZE_W;
	stream[0].enc_dim.h = VDO_SIZE_H;

	ret = set_enc_param(stream[0].enc_path, &stream[0].enc_dim, enc_type, bit_rate);
	if (ret != HD_OK) {
		printf("set enc fail=%d\n", ret);
		pthread_exit((void *)-1);
		return NULL;
	}

        // bind video_record modules (main)
	while( (ret =hd_videoproc_bind(HD_VIDEOPROC_0_OUT_0, HD_VIDEOENC_0_IN_0)) != 0){
		//printf("Fail to hd_videoproc_bind(), ret=%d\r\n", ret);
	        usleep(3 * 1000);
	}

	//------ wait enc_start ------
	while (p_stream0->enc_start == 0) usleep(3 * 1000);
	hd_videoenc_start(stream[0].enc_path);
	pthread_exit((void *)0);
	return NULL;
}

static void *encode_thread(void *arg)
{

	VIDEO_RECORD* p_stream0 = (VIDEO_RECORD *)arg;
	HD_RESULT ret = HD_OK;
	HD_VIDEOENC_BS  data_pull;
	int get_phy_flag=0;
	#define PHY2VIRT_MAIN(pa) (vir_addr_main + (pa - phy_buf_main.buf_info.phy_addr))

#if UVC_ON
	UVAC_STRM_FRM      strmFrm = {0};
	UINT32             loop =0;
#endif
	//--------- pull data test ---------
	while (p_stream0->enc_exit == 0) {
		if (!flow_start){
			if (get_phy_flag){
				/* mummap for bs buffer */
				//printf("release common buffer2\r\n");
				if (vir_addr_main)hd_common_mem_munmap((void *)vir_addr_main, phy_buf_main.buf_info.buf_size);

				get_phy_flag = 0;
			}
			//------ wait flow_start ------
			usleep(10);
			continue;
		}else{
			if (!get_phy_flag){

				// mmap for bs buffer (just mmap one time only, calculate offset to virtual address later)
				ret = hd_videoenc_get(p_stream0->enc_path, HD_VIDEOENC_PARAM_BUFINFO, &phy_buf_main);

				// mmap for bs buffer (just mmap one time only, calculate offset to virtual address later)
				vir_addr_main = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, phy_buf_main.buf_info.phy_addr, phy_buf_main.buf_info.buf_size);

				get_phy_flag = 1;
			}
		}
		//pull data
		ret = hd_videoenc_pull_out_buf(p_stream0->enc_path, &data_pull, -1); // -1 = blocking mode
		if (ret == HD_OK) {
#if UVC_ON
			//strmFrm.path = (i % 2 == 0) ? UVAC_STRM_VID : UVAC_STRM_VID2;
			strmFrm.path = UVAC_STRM_VID ;
			strmFrm.addr = data_pull.video_pack[data_pull.pack_num-1].phy_addr;
			strmFrm.size = data_pull.video_pack[data_pull.pack_num-1].size;
			strmFrm.pStrmHdr = 0;
			strmFrm.strmHdrSize = 0;
			if (p_stream0->codec_type == HD_CODEC_TYPE_H264){
				if (data_pull.pack_num > 1) {
					//printf("I-frame\r\n");
					strmFrm.pStrmHdr = (UINT8 *)data_pull.video_pack[0].phy_addr;
					for (loop = 0; loop < data_pull.pack_num - 1; loop ++) {
						strmFrm.strmHdrSize += data_pull.video_pack[loop].size;
					}
				} else {
					strmFrm.pStrmHdr = 0;
					strmFrm.strmHdrSize = 0;
					//printf("P-frame\r\n");
				}
			}
			UVAC_SetEachStrmInfo(&strmFrm);
#endif
			// release data
			ret = hd_videoenc_release_out_buf(p_stream0->enc_path, &data_pull);
			if (ret != HD_OK) {
				printf("enc_release error=%d !!\r\n", ret);
			}
		}

	}
	if (get_phy_flag){
		// mummap for bs buffer
		//printf("release common buffer\r\n");
		if (vir_addr_main) hd_common_mem_munmap((void *)vir_addr_main, phy_buf_main.buf_info.buf_size);
	}

	return 0;
}

#if UVC_ON
_ALIGNED(4) static UINT16 m_UVACSerialStrDesc3[] = {
	0x0320,                             // 20: size of String Descriptor = 32 bytes
	// 03: String Descriptor type
	'5', '1', '0', '5', '5',            // 96611-00000-001 (default)
	'0', '0', '0', '0', '0',
	'0', '0', '1', '0', '0'
};
_ALIGNED(4) const static UINT8 m_UVACManuStrDesc[] = {
	USB_VENDER_DESC_STRING_LEN * 2 + 2, // size of String Descriptor = 6 bytes
	0x03,                       // 03: String Descriptor type
	USB_VENDER_DESC_STRING
};

_ALIGNED(4) const static UINT8 m_UVACProdStrDesc[] = {
	USB_PRODUCT_DESC_STRING_LEN * 2 + 2, // size of String Descriptor = 6 bytes
	0x03,                       // 03: String Descriptor type
	USB_PRODUCT_DESC_STRING
};


static void USBMakerInit_UVAC(UVAC_VEND_DEV_DESC *pUVACDevDesc)
{

	pUVACDevDesc->pManuStringDesc = (UVAC_STRING_DESC *)m_UVACManuStrDesc;
	pUVACDevDesc->pProdStringDesc = (UVAC_STRING_DESC *)m_UVACProdStrDesc;

	pUVACDevDesc->pSerialStringDesc = (UVAC_STRING_DESC *)m_UVACSerialStrDesc3;

	pUVACDevDesc->VID = USB_VID;
	pUVACDevDesc->PID = USB_PID_PCCAM;

}

static UINT32 xUvacStartVideoCB(UVAC_VID_DEV_CNT vidDevIdx, UVAC_STRM_INFO *pStrmInfo)
{
	//HD_DIM main_dim;
	int ret=0;
	UINT32 byte_rate = 0;

	if (pStrmInfo) {
		//printf("UVAC Start[%d] resoIdx=%lu,W=%lu,H=%lu,codec=%lu,fps=%lu,path=%lu,tbr=0x%x\r\n", vidDevIdx, pStrmInfo->strmResoIdx, pStrmInfo->strmWidth, pStrmInfo->strmHeight, pStrmInfo->strmCodec, pStrmInfo->strmFps, pStrmInfo->strmPath, pStrmInfo->strmTBR);
		m_VideoFmt[vidDevIdx] = pStrmInfo->strmCodec;

		//stop poll data
		flow_start = 0;
		// mummap for video bs buffer
		if (vir_addr_main) hd_common_mem_munmap((void *)vir_addr_main, phy_buf_main.buf_info.buf_size);
		/* mummap for audio bs buffer */
		if (vir_addr_main2)hd_common_mem_munmap((void *)vir_addr_main2, phy_buf_main2.buf_info.buf_size);

#if AUDIO_ON
		//stop audio capture module
		hd_audiocap_stop(au_cap.cap_path);
#endif
		// stop VIDEO_STREAM modules (main)
		if (g_capbind == 1) {
			//direct NOTE: ensure videocap stop after all videoproc path stop
			hd_videoproc_stop(stream[0].proc_path);
			hd_videocap_stop(stream[0].cap_path);
		} else {
			hd_videocap_stop(stream[0].cap_path);
			hd_videoproc_stop(stream[0].proc_path);
		}
		hd_videoenc_stop(stream[0].enc_path);

		//set codec,resolution
		switch (pStrmInfo->strmCodec){
			case 0:
				stream[0].codec_type = HD_CODEC_TYPE_H264;
				//printf("set codec=H264\r\n");
				break;
			case 1:
				stream[0].codec_type = HD_CODEC_TYPE_JPEG;
				//printf("set codec=MJPG\r\n");
				break;
			default:
				printf("unrecognized codec(%d)\r\n",pStrmInfo->strmCodec);
				break;
		}

		//set encode resolution to device
		stream[0].enc_dim.w = pStrmInfo->strmWidth;
		stream[0].enc_dim.h = pStrmInfo->strmHeight;

		// set videoproc parameter (main)
		ret = set_proc_param(stream[0].proc_path, &stream[0].enc_dim);
		if (ret != HD_OK) {
			printf("set proc fail=%d\n", ret);
		}
		hd_videoenc_close(stream[0].enc_path);
		ret = open_module_enc(&stream[0], &stream[0].enc_dim);
		if (ret != HD_OK) {
			printf("open venc fail=%d\n", ret);
		}
		GetUvcTBR(pStrmInfo->strmCodec, pStrmInfo->strmWidth, pStrmInfo->strmHeight, pStrmInfo->strmFps, &byte_rate);
		ret = set_enc_cfg(stream[0].enc_path, &stream[0].enc_dim, byte_rate*8, stream[0].codec_type);
		if (ret != HD_OK) {
			printf("set enc cfg fail=%d\n", ret);
		}
		// set videoenc parameter (main)
		ret = set_enc_param(stream[0].enc_path, &stream[0].enc_dim, stream[0].codec_type, byte_rate*8);
		if (ret != HD_OK) {
			printf("set enc fail=%d\n", ret);
		}

		//start engine(modules)
		if (g_capbind == 1) {
			//direct NOTE: ensure videocap start after 1st videoproc phy path start
			hd_videoproc_start(stream[0].proc_path);
			hd_videocap_start(stream[0].cap_path);
		} else {
			hd_videocap_start(stream[0].cap_path);
			hd_videoproc_start(stream[0].proc_path);
		}
		hd_videoenc_start(stream[0].enc_path);
#if AUDIO_ON
		//start audio capture module
		hd_audiocap_start(au_cap.cap_path);
#endif
		flow_start = 1;
	}
	return E_OK;
}

static UINT32 xUvacStopVideoCB(UINT32 isClosed)
{
	//printf(":isClosed=%d\r\n", isClosed);
	printf("stop encode flow\r\n");
	flow_start = 0;
	return E_OK;
}

static BOOL UVAC_enable(void)
{

	UVAC_INFO       UvacInfo = {0};
	UINT32          retV = 0;

	UVAC_VEND_DEV_DESC UIUvacDevDesc = {0};
	UVAC_AUD_SAMPLERATE_ARY uvacAudSampleRateAry = {0};
	UINT32 bVidFmtType = UVAC_VIDEO_FORMAT_H264_MJPEG;
	m_bIsStaticPattern = FALSE;
	UVAC_VID_BUF_INFO vid_buf_info = {0};
	UINT32            uvacNeedMem  = 0;
	UINT32            ChannelNum   = 1;

	printf("###Real Open UVAC-lib\r\n");

	HD_COMMON_MEM_DDR_ID ddr_id = DDR_ID0;
	UINT32 pa;
	void *va;
	HD_RESULT hd_ret;

	hd_ret = hd_gfx_init();
	if(hd_ret != HD_OK) {
        printf("init gfx fail=%d\n", hd_ret);
	}
	uvac_size = UVAC_GetNeedMemSize();
	//printf("uvac_size = %u\r\n", uvac_size);
	if ((hd_ret = hd_common_mem_alloc("usbmovie", &pa, (void **)&va, uvac_size, ddr_id)) != HD_OK) {
		printf("hd_common_mem_alloc failed(%d)\r\n", hd_ret);
		uvac_va = 0;
		uvac_pa = 0;
		uvac_size = 0;
	} else {
		uvac_va = (UINT32)va;
		uvac_pa = (UINT32)pa;
	}
	uvacNeedMem *= ChannelNum;
	UvacInfo.UvacMemAdr    = uvac_pa;
	UvacInfo.UvacMemSize   = uvac_size;

	vid_buf_info.vid_buf_pa =  phy_buf_main.buf_info.phy_addr;
	vid_buf_info.vid_buf_size =  phy_buf_main.buf_info.buf_size;
	UVAC_SetConfig(UVAC_CONFIG_VID_BUF_INFO, (UINT32)&vid_buf_info);
	UvacInfo.fpStartVideoCB  = (UVAC_STARTVIDEOCB)xUvacStartVideoCB;
	UvacInfo.fpStopVideoCB  = (UVAC_STOPVIDEOCB)xUvacStopVideoCB;

	USBMakerInit_UVAC(&UIUvacDevDesc);
	UVAC_SetConfig(UVAC_CONFIG_VEND_DEV_DESC, (UINT32)&UIUvacDevDesc);
	UVAC_SetConfig(UVAC_CONFIG_HW_COPY_CB, (UINT32)hd_gfx_memcpy);
	//printf("%s:uvacAddr=0x%x,s=0x%x;IPLAddr=0x%x,s=0x%x\r\n", __func__, UvacInfo.UvacMemAdr, UvacInfo.UvacMemSize, m_VideoBuf.addr, m_VideoBuf.size);

	UvacInfo.hwPayload[0] = FALSE;//hit
	UvacInfo.hwPayload[1] = FALSE;//hit

	//must set to 1I14P ratio
	UvacInfo.strmInfo.strmWidth = VDO_SIZE_W;
	UvacInfo.strmInfo.strmHeight = VDO_SIZE_H;
	UvacInfo.strmInfo.strmFps = 30;
	//UvacInfo.strmInfo.strmCodec = MEDIAREC_ENC_H264;
	//w=1280,720,30,2
	UVAC_ConfigVidReso(gUIUvacVidReso, NVT_UI_UVAC_RESO_CNT);

#if UVC_SUPPORT_YUV_FMT
	{
		UVAC_VID_RESO_ARY uvacVidYuvResoAry = {0};

		uvacVidYuvResoAry.aryCnt = sizeof(gUIUvacVidYuvReso)/sizeof(UVAC_VID_RESO);
		uvacVidYuvResoAry.pVidResAry = &gUIUvacVidYuvReso[0];
		UVAC_SetConfig(UVAC_CONFIG_YUV_FRM_INFO, (UINT32)&uvacVidYuvResoAry);
	}
#endif
	uvacAudSampleRateAry.aryCnt = NVT_UI_UVAC_AUD_SAMPLERATE_CNT;
	uvacAudSampleRateAry.pAudSampleRateAry = &gUIUvacAudSampleRate[0]; //NVT_UI_FREQUENCY_32K
	UVAC_SetConfig(UVAC_CONFIG_AUD_SAMPLERATE, (UINT32)&uvacAudSampleRateAry);
	UvacInfo.channel = ChannelNum;
	//printf("w=%d,h=%d,fps=%d,codec=%d\r\n", UvacInfo.strmInfo.strmWidth, UvacInfo.strmInfo.strmHeight, UvacInfo.strmInfo.strmFps, UvacInfo.strmInfo.strmCodec);
	UVAC_SetConfig(UVAC_CONFIG_AUD_CHANNEL_NUM, ChannelNum);

	UVAC_SetConfig(UVAC_CONFIG_VIDEO_FORMAT_TYPE, bVidFmtType);
	retV = UVAC_Open(&UvacInfo);
	if (retV != E_OK) {
		printf("Error open UVAC task, retV=%lu\r\n", retV);
	}
	return TRUE;
}
#endif


int flow_uvc(void)
{
	int ret;
	int pthread_ret;
	int join_ret;
	pthread_t handle_sensor;
	pthread_t handle_cap_proc;
	pthread_t handle_enc;
	int policy;
	struct sched_param schedparam = {0};
	UINT32 enc_type = ENC_TYPE;
	if (1 == enc_type)
		printf("enc_type:h264\r\n");
	else if (2 == enc_type)
		printf("enc_type:MJPG\r\n");


	stream[0].codec_type = enc_type;
	stream[0].enc_start = 0;

	if (g_capfmt == 0) {
		g_capfmt = HD_VIDEO_PXLFMT_RAW12;
	} else {
		g_capfmt =  HD_VIDEO_PXLFMT_NRX12;
	}

	// quick sensor setup
	pthread_ret = pthread_create(&handle_sensor, NULL, thread_sensor , NULL);
	if (0 != pthread_ret) {
		printf("create thread_sensor failed, ret %d\r\n", pthread_ret);
		return -1;
	}
	if (0 != pthread_getschedparam(handle_sensor, &policy, &schedparam)) {
			printf("pthread_getschedparam failed\r\n");
	} else {
		schedparam.sched_priority = 20;
		pthread_setschedparam(handle_sensor, policy, &schedparam);
	}

	//hd_common_init(including vds), must wait until insmod_capture()
	fastboot_wait_done(BOOT_INIT_CAPTURE);

	// init hdal
	ret = hd_common_init(0);
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
		return -1;
	}

	// init memory
	ret = mem_init();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
		return -1;
	}

#if AUDIO_ON
	if((ret = hd_audiocap_init()) != HD_OK)
        return ret;

	//open capture module
	au_cap.sample_rate_max = (gUIUvacAudSampleRate[0] == NVT_UI_FREQUENCY_32K) ? HD_AUDIO_SR_32000 : HD_AUDIO_SR_16000; //assign by user
	ret = open_module2(&au_cap);
	if(ret != HD_OK) {
		printf("open fail=%d\n", ret);
		return -1;
	}

	//set audiocap parameter
	au_cap.sample_rate = (gUIUvacAudSampleRate[0] == NVT_UI_FREQUENCY_32K) ? HD_AUDIO_SR_32000 : HD_AUDIO_SR_16000; //assign by user
	ret = set_cap_param2(au_cap.cap_path, au_cap.sample_rate);
	if (ret != HD_OK) {
		printf("set cap fail=%d\n", ret);
		return -1;
	}
#endif

	// Create cap_proc
	pthread_ret = pthread_create(&handle_cap_proc, NULL, thread_cap_proc , (void *)&handle_sensor);
	if (0 != pthread_ret) {
		printf("create thread_cap_proc failed, ret %d\r\n", pthread_ret);
		return -1;
	}
	if (0 != pthread_getschedparam(handle_cap_proc, &policy, &schedparam)) {
			printf("pthread_getschedparam failed\r\n");
	} else {
		schedparam.sched_priority = 20;
		pthread_setschedparam(handle_cap_proc, policy, &schedparam);
	}

        // Start vidoe encode
        // create encode_thread (pull_out bitstream)
        ret = pthread_create(&stream[0].enc_thread_id, NULL, encode_thread, (void *)stream);
        if (ret < 0) {
            printf("create encode thread failed");
            return -1;
        }

#if AUDIO_ON
	//audio create capture thread
	ret = pthread_create(&cap_thread_id, NULL, capture_thread, (void *)&au_cap);
	if (ret < 0) {
		printf("create encode thread failed");
		return -1;
	}

	//start audio capture module
	hd_audiocap_start(au_cap.cap_path);
#endif

	// Create enc
	pthread_ret = pthread_create(&handle_enc, NULL, thread_enc , (void *)stream);
	if (0 != pthread_ret) {
		printf("create handle_enc failed, ret %d\r\n", pthread_ret);
		return -1;
	}
	if (0 != pthread_getschedparam(handle_enc, &policy, &schedparam)) {
			printf("pthread_getschedparam failed\r\n");
	} else {
		schedparam.sched_priority = 19;
		pthread_setschedparam(handle_enc, policy, &schedparam);
	}

    // Join cap_proc
	pthread_ret = pthread_join(handle_cap_proc, (void *)&join_ret);
	if (0 != pthread_ret) {
		printf("thread_cap_proc pthread_join failed, ret %d\r\n", pthread_ret);
		return -1;
	}

    // Join enc
	pthread_ret = pthread_join(handle_enc, (void *)&join_ret);
	if (0 != pthread_ret) {
		printf("handle_enc pthread_join failed, ret %d\r\n", pthread_ret);
		return -1;
	}

	// let encode_thread start to work
//	while (stream[0].enc_start == 0) usleep(3 * 1000);

	flow_start = 1;

#if UVC_ON
	sleep(1);
	UVAC_enable(); //init usb device, start to prepare video/audio buffer to send
#endif
	// destroy encode thread
	pthread_join(stream[0].enc_thread_id, NULL);

	return 0;
}
