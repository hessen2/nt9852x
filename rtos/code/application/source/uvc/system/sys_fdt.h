#ifndef SYS_FDT_H
#define SYS_FDT_H

extern void *fdt_get_app(void);
extern void *fdt_get_sensor(void);

#endif