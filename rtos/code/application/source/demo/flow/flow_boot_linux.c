#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <kwrap/type.h>
#include <kwrap/perf.h>
#include <kwrap/cpu.h>
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h>
#include <libfdt.h>
#include <compiler.h>
#include <rtosfdt.h>
#include "prjcfg.h"
#include "sys_fwload.h"
#include "sys_fastboot.h"
#include "sys_linuxboot.h"
#include "flow_boot_linux.h"
#include <kwrap/debug.h>
#include "vendor_videocapture.h"
#include "isp_api.h"
#include "sen_inc.h"
#include "kwrap/util.h"
#include <dispdevctrl.h>
#include <plat/nand.h>
#include "sys_storage_partition.h"
#include <MemCheck.h>
#include "kdrv_audioio/kdrv_audioio.h"
#include <kdrv_videocapture/kdrv_tge.h>

#define CFG_LOAD_AI_MODEL ENABLE

#define CFG_LOAD_AUDIOIO  DISABLE
#define CFG_LOAD_SOUND    DISABLE

#if CFG_LOAD_AUDIOIO
typedef enum {
	AUDIO_CH_LEFT,              ///< Left
	AUDIO_CH_RIGHT,             ///< Right
	AUDIO_CH_STEREO,            ///< Stereo
	AUDIO_CH_MONO,              ///< Mono two channel. Obselete. Shall not use this option.
	AUDIO_CH_DUAL_MONO,         ///< Dual Mono Channels. Valid for record(RX) only.

	ENUM_DUMMY4WORD(AUDIO_CH)
} AUDIO_CH;

typedef enum {
	AUDIO_SR_8000   = 8000,     ///< 8 KHz
	AUDIO_SR_11025  = 11025,    ///< 11.025 KHz
	AUDIO_SR_12000  = 12000,    ///< 12 KHz
	AUDIO_SR_16000  = 16000,    ///< 16 KHz
	AUDIO_SR_22050  = 22050,    ///< 22.05 KHz
	AUDIO_SR_24000  = 24000,    ///< 24 KHz
	AUDIO_SR_32000  = 32000,    ///< 32 KHz
	AUDIO_SR_44100  = 44100,    ///< 44.1 KHz
	AUDIO_SR_48000  = 48000,    ///< 48 KHz

	ENUM_DUMMY4WORD(AUDIO_SR)
} AUDIO_SR;

typedef struct _AUDIO_OBJ {
	UINT32              aud_eid;
	UINT32              aud_vol;
	AUDIO_SR            aud_sr;          ///< Sample Rate
	AUDIO_CH            aud_ch;          ///< Channel
	UINT32				ch_num;			///< Channel number
	KDRV_BUFFER_INFO    buf_info;
	KDRV_CALLBACK_FUNC  buf_cb;
	UINT32              play_num;
} AUDIO_OBJ, *PAUDIO_OBJ;
#endif

//#define RESOLUTION_SET  0 //0: 2M(IMX290), 1:5M(OS05A), 2: 2M (OS02K10), 3: 2M (AR0237IR), 4 : F37, 5 : F35, 6: GC4653, 7:GC4653_SLAVE
#if defined(_sen_imx290_TYPE1_)
#define RESOLUTION_SET_T1  0
#elif defined(_sen_os05a10_TYPE1_)
#define RESOLUTION_SET_T1  1
#elif defined(_sen_os02k10_TYPE1_)
#define RESOLUTION_SET_T1  2
#elif defined(_sen_ar0237ir_TYPE1_)
#define RESOLUTION_SET_T1  3
#elif defined(_sen_f37_TYPE1_)
#define RESOLUTION_SET_T1  4
#elif defined(_sen_f35_TYPE1_)
#define RESOLUTION_SET_T1  5
#elif defined(_sen_gc4653_TYPE1_)
#define RESOLUTION_SET_T1  6
#elif defined(_sen_gc4653_slave_TYPE1_)
#define RESOLUTION_SET_T1  7
#else
#error "un-implemented sensor for flow_preview.c"
#endif


#if defined(_sen_imx290_TYPE2_)
#define RESOLUTION_SET_T2  0
#elif defined(_sen_os05a10_TYPE2_)
#define RESOLUTION_SET_T2  1
#elif defined(_sen_os02k10_TYPE2_)
#define RESOLUTION_SET_T2  2
#elif defined(_sen_ar0237ir_TYPE2_)
#define RESOLUTION_SET_T2  3
#elif defined(_sen_f37_TYPE2_)
#define RESOLUTION_SET_T2  4
#elif defined(_sen_f35_TYPE2_)
#define RESOLUTION_SET_T2  5
#elif defined(_sen_gc4653_TYPE2_)
#define RESOLUTION_SET_T2  6
#elif defined(_sen_gc4653_slave_TYPE2_)
#define RESOLUTION_SET_T2  7
#elif defined(_sen_off_TYPE2_)
#define RESOLUTION_SET_T2  0 // compiled but not used
#else
#error "un-implemented sensor for flow_preview.c"
#endif

//for single HDR sensor
#define HDR_SEN1_VCAP_ID     0
#define HDR_SEN1_SUB_VCAP_ID 1
#define SEN1_SHDR_VCAP_MAP ((1<<HDR_SEN1_VCAP_ID)|(1<<HDR_SEN1_SUB_VCAP_ID))

//for dual HDR sensor
#define HDR_SEN2_VCAP_ID     3
#define HDR_SEN2_SUB_VCAP_ID 4
#define DUAL_SEN1_SHDR_VCAP_MAP SEN1_SHDR_VCAP_MAP
#define DUAL_SEN2_SHDR_VCAP_MAP ((1<<HDR_SEN2_VCAP_ID)|(1<<HDR_SEN2_SUB_VCAP_ID))

//for dual linear sensor
#define LINEAR_SEN2_VCAP_ID 1

//for dual TGE-synced sensor
#define TGE_SYNC_VCAP_ID (HD_VIDEOCAP_0|HD_VIDEOCAP_1)

#if ( RESOLUTION_SET_T1 == 0)
#define VDO_SIZE_W_T1      1920
#define VDO_SIZE_H_T1       1080
#define SEN_DRVIVER_NAME_T1 "nvt_sen_imx290"
#define SEN_CHGMODE_FPS_T1  3000  // 30 fps
#define SEN_EXPT_MAX_T1     30000 // 30 ms
#define SEN_IF_TYPE_T1      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T1       0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0_T1        0
#define PIN_MAP_1_T1        1
#define PIN_MAP_2_T1        2//HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_3_T1        3//HD_VIDEOCAP_SEN_IGNORE
#define FUNC_SHDR_T1        DISABLE
#define DATA_LANE_T1        4
#define FUNC_TGE_SYNC    DISABLE
#elif (RESOLUTION_SET_T1 == 1)
#define VDO_SIZE_W_T1       2592
#define VDO_SIZE_H_T1       1944
#define SEN_DRVIVER_NAME_T1 "nvt_sen_os05a10"
#define SEN_CHGMODE_FPS_T1  3000  // 30 fps
#define SEN_EXPT_MAX_T1     30000 // 30 ms
#define SEN_IF_TYPE_T1      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T1       0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0_T1        0
#define PIN_MAP_1_T1        1
#define PIN_MAP_2_T1        2
#define PIN_MAP_3_T1        3
#define FUNC_SHDR_T1        DISABLE
#define DATA_LANE_T1        4
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T1 == 2)
#define VDO_SIZE_W_T1       1920
#define VDO_SIZE_H_T1       1080
#define SEN_DRVIVER_NAME_T1 "nvt_sen_os02k10"
#define SEN_CHGMODE_FPS_T1  3000  // 30 fps
#define SEN_EXPT_MAX_T1     30000 // 30 ms
#define SEN_IF_TYPE_T1      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T1       0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0_T1        0
#define PIN_MAP_1_T1        1
#define PIN_MAP_2_T1        HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_3_T1        HD_VIDEOCAP_SEN_IGNORE
#define FUNC_SHDR_T1        DISABLE
#define DATA_LANE_T1        2
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T1 == 3)
#define VDO_SIZE_W_T1       1920
#define VDO_SIZE_H_T1       1080
#define SEN_DRVIVER_NAME_T1 "nvt_sen_ar0237ir"
#define SEN_CHGMODE_FPS_T1  3000  // 30 fps
#define SEN_EXPT_MAX_T1     30000 // 30 ms
#define SEN_IF_TYPE_T1      HD_COMMON_VIDEO_IN_P_RAW
#define SEN_PINMUX_T1       0x204; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0_T1        0
#define PIN_MAP_1_T1        1
#define PIN_MAP_2_T1        2
#define PIN_MAP_3_T1        3
#define FUNC_SHDR_T1        DISABLE
#define DATA_LANE_T1        4
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T1 == 4)
#define VDO_SIZE_W_T1       1920
#define VDO_SIZE_H_T1       1080
#define SEN_DRVIVER_NAME_T1 "nvt_sen_f37"
#define SEN_CHGMODE_FPS_T1  3000  // 30 fps
#define SEN_EXPT_MAX_T1     30000 // 30 ms
#define SEN_IF_TYPE_T1      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T1       0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0_T1        0
#define PIN_MAP_1_T1        1
#define FUNC_SHDR_T1        DISABLE
#define DATA_LANE_T1        2
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T1 == 5)
#define VDO_SIZE_W_T1       1920
#define VDO_SIZE_H_T1       1080
#define SEN_DRVIVER_NAME_T1 "nvt_sen_f35"
#define SEN_CHGMODE_FPS_T1  3000  // 30 fps
#define SEN_EXPT_MAX_T1     30000 // 30 ms
#define SEN_IF_TYPE_T1      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T1       0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0_T1        0
#define PIN_MAP_1_T1        1
#define FUNC_SHDR_T1        DISABLE
#define DATA_LANE_T1        2
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T1 == 6)
#define VDO_SIZE_W_T1       2560
#define VDO_SIZE_H_T1       1440
#define SEN_DRVIVER_NAME_T1 "nvt_sen_gc4653"
#define SEN_CHGMODE_FPS_T1  3000  // 30 fps
#define SEN_EXPT_MAX_T1     30000 // 30 ms
#define SEN_IF_TYPE_T1      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T1       0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0_T1        0
#define PIN_MAP_1_T1        1
#define FUNC_SHDR_T1        DISABLE
#define DATA_LANE_T1        2
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T1 == 7)
#define VDO_SIZE_W_T1      2560
#define VDO_SIZE_H_T1       1440
#define SEN_DRVIVER_NAME_T1 "nvt_sen_gc4653_slave"
#define SEN_CHGMODE_FPS_T1  3000  // 30 fps
#define SEN_EXPT_MAX_T1     30000 // 30 ms
#define SEN_IF_TYPE_T1      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T1       0x2A0; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0_T1        0
#define PIN_MAP_1_T1        1
#define FUNC_SHDR_T1        DISABLE
#define DATA_LANE_T1        2
#define FUNC_TGE_SYNC    ENABLE
#endif

#if ( RESOLUTION_SET_T2 == 0)
#define VDO_SIZE_W_T2       1920
#define VDO_SIZE_H_T2       1080
#define SEN_DRVIVER_NAME_T2 "nvt_sen_imx290"
#define SEN_CHGMODE_FPS_T2  3000  // 30 fps
#define SEN_EXPT_MAX_T2     30000 // 30 ms
#define SEN_IF_TYPE_T2      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T2       0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0_T2        0
#define PIN_MAP_1_T2        1
#define PIN_MAP_2_T2        2//HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_3_T2        3//HD_VIDEOCAP_SEN_IGNORE
#define FUNC_SHDR_T2        DISABLE
#define DATA_LANE_T2        4
#define FUNC_TGE_SYNC    DISABLE
#elif (RESOLUTION_SET_T2 == 1)
#define VDO_SIZE_W_T2       2592
#define VDO_SIZE_H_T2       1944
#define SEN_DRVIVER_NAME_T2 "nvt_sen_os05a10"
#define SEN_CHGMODE_FPS_T2  3000  // 30 fps
#define SEN_EXPT_MAX_T2     30000 // 30 ms
#define SEN_IF_TYPE_T2      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T2      0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0_T2        0
#define PIN_MAP_1_T2        1
#define PIN_MAP_2_T2        2
#define PIN_MAP_3_T2        3
#define FUNC_SHDR_T2        DISABLE
#define DATA_LANE_T2        4
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T2 == 2)
#define VDO_SIZE_W_T2       1920
#define VDO_SIZE_H_T2       1080
#define SEN_DRVIVER_NAME_T2 "nvt_sen_os02k10"
#define SEN_CHGMODE_FPS_T2  3000  // 30 fps
#define SEN_EXPT_MAX_T2     30000 // 30 ms
#define SEN_IF_TYPE_T2      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T2       0x220; //PIN_SENSOR_CFG_MIPI
#define PIN_MAP_0_T2        0
#define PIN_MAP_1_T2        1
#define PIN_MAP_2_T2        HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_3_T2        HD_VIDEOCAP_SEN_IGNORE
#define FUNC_SHDR_T2        DISABLE
#define DATA_LANE_T2        2
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T2 == 3)
#define VDO_SIZE_W_T2       1920
#define VDO_SIZE_H_T2       1080
#define SEN_DRVIVER_NAME_T2 "nvt_sen_ar0237ir"
#define SEN_CHGMODE_FPS_T2  3000  // 30 fps
#define SEN_EXPT_MAX_T2     30000 // 30 ms
#define SEN_IF_TYPE_T2      HD_COMMON_VIDEO_IN_P_RAW
#define SEN_PINMUX_T2       0x204; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0_T2        0
#define PIN_MAP_1_T2        1
#define PIN_MAP_2_T2        2
#define PIN_MAP_3_T2        3
#define FUNC_SHDR_T2        DISABLE
#define DATA_LANE_T2        4
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T2 == 4)
#define VDO_SIZE_W_T2       1920
#define VDO_SIZE_H_T2       1080
#define SEN_DRVIVER_NAME_T2 "nvt_sen_f37"
#define SEN_CHGMODE_FPS_T2  3000  // 30 fps
#define SEN_EXPT_MAX_T2     30000 // 30 ms
#define SEN_IF_TYPE_T2      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T2       0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0_T2        0
#define PIN_MAP_1_T2        1
#define FUNC_SHDR_T2        DISABLE
#define DATA_LANE_T2        2
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T2 == 5)
#define VDO_SIZE_W_T2       1920
#define VDO_SIZE_H_T2       1080
#define SEN_DRVIVER_NAME_T2 "nvt_sen_f35"
#define SEN_CHGMODE_FPS_T2  3000  // 30 fps
#define SEN_EXPT_MAX_T2     30000 // 30 ms
#define SEN_IF_TYPE_T2      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T2       0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0_T2        0
#define PIN_MAP_1_T2        1
#define FUNC_SHDR_T2        DISABLE
#define DATA_LANE_T2        2
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T2 == 6)
#define VDO_SIZE_W_T2       2560
#define VDO_SIZE_H_T2       1440
#define SEN_DRVIVER_NAME_T2 "nvt_sen_gc4653"
#define SEN_CHGMODE_FPS_T2  3000  // 30 fps
#define SEN_EXPT_MAX_T2     30000 // 30 ms
#define SEN_IF_TYPE_T2      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T2       0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0_T2        0
#define PIN_MAP_1_T2        1
#define FUNC_SHDR_T2        DISABLE
#define DATA_LANE_T2        2
#define FUNC_TGE_SYNC    DISABLE
#elif ( RESOLUTION_SET_T2 == 7)
#define VDO_SIZE_W_T2      2560
#define VDO_SIZE_H_T2       1440
#define SEN_DRVIVER_NAME_T2 "nvt_sen_gc4653_slave"
#define SEN_CHGMODE_FPS_T2  3000  // 30 fps
#define SEN_EXPT_MAX_T2     30000 // 30 ms
#define SEN_IF_TYPE_T2      HD_COMMON_VIDEO_IN_MIPI_CSI
#define SEN_PINMUX_T2       0x2A0; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
#define PIN_MAP_0_T2        0
#define PIN_MAP_1_T2        1
#define FUNC_SHDR_T2        DISABLE
#define DATA_LANE_T2        2
#define FUNC_TGE_SYNC    ENABLE
#endif

#define SEN_OUT_FMT     HD_VIDEO_PXLFMT_RAW12

#if ((RESOLUTION_SET_T1 == 4) || (RESOLUTION_SET_T1 == 5) || (RESOLUTION_SET_T1 == 6) || (RESOLUTION_SET_T1 == 7))
#define SERIAL_IF_PINMUX  0x301;//0xf01;//PIN_MIPI_LVDS_CFG_CLK2 | PIN_MIPI_LVDS_CFG_DAT0 | PIN_MIPI_LVDS_CFG_DAT1
#else
#define SERIAL_IF_PINMUX  0xf01;//0xf01;//PIN_MIPI_LVDS_CFG_CLK2 | PIN_MIPI_LVDS_CFG_DAT0 | PIN_MIPI_LVDS_CFG_DAT1 | PIN_MIPI_LVDS_CFG_DAT2 | PIN_MIPI_LVDS_CFG_DAT3
#endif

#define CMD_IF_PINMUX     0x10;//PIN_I2C_CFG_CH2
#define CLK_LANE_SEL      HD_VIDEOCAP_SEN_CLANE_SEL_CSI0_USE_C0

#if ((RESOLUTION_SET_T1 == 4) || (RESOLUTION_SET_T1 == 5) || (RESOLUTION_SET_T1 == 6) || (RESOLUTION_SET_T1 == 7))
#define PIN_MAP_2_T1       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_3_T1       HD_VIDEOCAP_SEN_IGNORE
#endif

#if ((RESOLUTION_SET_T2 == 4) || (RESOLUTION_SET_T2 == 5) || (RESOLUTION_SET_T2 == 6) || (RESOLUTION_SET_T2 == 7))
#define PIN_MAP_2_T2       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_3_T2       HD_VIDEOCAP_SEN_IGNORE
#endif

#define PIN_MAP_4_T1       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_5_T1       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_6_T1       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_7_T1       HD_VIDEOCAP_SEN_IGNORE

#define PIN_MAP_4_T2       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_5_T2       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_6_T2       HD_VIDEOCAP_SEN_IGNORE
#define PIN_MAP_7_T2       HD_VIDEOCAP_SEN_IGNORE

//<<<<<<< HEAD
#include "adc.h"
#define ADC_CH_PHOTORESISTOR         ADC_CHANNEL_0
#define ADC_READY_RETRY_CNT          (1000000/10) //1 sec
#define SENSOR_GAIN_MAX               15500 // for F37

#define _SEN_DRV_NAME(sen) STR(nvt_sen_##sen)
#define SEN_DRV_NAME(sen) _SEN_DRV_NAME(sen)
#define STR(s) #s

static UINT32 isp_sensor_shdr_map_path = 0xFFFFFFFF;
static UINT32 isp_sensor_shdr_id_mask = 0x1;   // HD_VIDEOCAP_0
static UINT32 isp_sensor_path_2 = 1;            // HD_VIDEOCAP_1
static UINT32 isp_sensor_shdr_map_path_2 = 0xFFFFFFFF;
static UINT32 isp_sensor_shdr_id_mask_2 = 0x2;  // HD_VIDEOCAP_1
static UINT32 isp_dgain = 128;
static UINT32 isp_rgain = 0;
static UINT32 isp_ggain = 0;
static UINT32 isp_bgain = 0;
static UINT32 isp_nr_lv = 100;
static UINT32 isp_3dnr_lv = 100;
static UINT32 isp_sharpness_lv = 100;
static UINT32 isp_saturation_lv = 100;
static UINT32 isp_contrast_lv = 100;
static UINT32 isp_brightness_lv = 100;
static UINT32 isp_night_mode = 0;
static UINT32 isp_ae_stitch_mode = 0;
static UINT32 isp_awb_stitch_mode = 0;

typedef struct _AE_ADC_CTRL_ {
	UINT32 enable;
	UINT32 adc_tbl_thr;
	UINT32 ir_mode_en;
	UINT32 ir_mode_thr;
	UINT32 sen_expt;
	UINT32 sen_gain;
	UINT32 isp_gain;
} AE_ADC_CTRL;

AE_ADC_CTRL ae_adc_ctrl = {
	0, 1000, 0, 50, 10000, 1000, 128
};

// bridge memory description
#define BRIDGE_FOURCC 0x47445242 ///< MAKEFOURCC('B', 'R', 'D', 'G');
#define BRIDGE_MAX_OPT_CNT 128
// bridge memory description !! (DO NOT MODIFY ANY MEMBER IN BRIDGE_DESC and BRIDGE_BOOT_OPTION)
typedef struct _BRIDGE_BOOT_OPTION {
	unsigned int tag; //a fourcc tag
	unsigned int val; //the value
} BRIDGE_BOOT_OPTION;

typedef struct _BRIDGE_DESC {
	unsigned int bridge_fourcc;     ///< always BRIDGE_FOURCC
	unsigned int bridge_size;       ///< sizeof(BRIDGE_DESC) for check if struct match on rtos and linux
	unsigned int phy_addr;          ///< address of bridge memory described on fdt
	unsigned int phy_size;          ///< size of whole bridge memory described on fdt
	BRIDGE_BOOT_OPTION opts[BRIDGE_MAX_OPT_CNT]; ///< boot options from rtos
} BRIDGE_DESC;

//element's fourcc
#define SENSOR_PRESET_NAME   0x454E5053 //MAKEFOURCC('S', 'P', 'N', 'E');
#define SENSOR_CHGMODE_FPS   0x53464353 //MAKEFOURCC('S', 'C', 'F', 'S');
#define SENSOR_PRESET_EXPT   0x54455053 //MAKEFOURCC('S', 'P', 'E', 'T');
#define SENSOR_PRESET_GAIN   0x4E475053 //MAKEFOURCC('S', 'P', 'G', 'N');
#define SENSOR_EXPT_MAX      0x584D4553 //MAKEFOURCC('S', 'E', 'M', 'X');
#define SENSOR_I2C_ID        0x44494953 //MAKEFOURCC('S', 'I', 'I', 'D');
#define SENSOR_I2C_ADDR      0x52414953 //MAKEFOURCC('S', 'I', 'A', 'R');
#define ISP_D_GAIN           0x4E474449 //MAKEFOURCC('I', 'D', 'G', 'N');
#define ISP_R_GAIN           0x4E475249 //MAKEFOURCC('I', 'R', 'G', 'N');
#define ISP_G_GAIN           0x4E474749 //MAKEFOURCC('I', 'G', 'G', 'N');
#define ISP_B_GAIN           0x4E474249 //MAKEFOURCC('I', 'B', 'G', 'N');
#define ISP_SHDR_ENABLE      0x45455349 //MAKEFOURCC('I', 'S', 'E', 'E');
#define ISP_SHDR_PATH        0x48505348 //MAKEFOURCC('I', 'S', 'P', 'H');
#define ISP_SHDR_MASK        0x4B4D5349 //MAKEFOURCC('I', 'S', 'M', 'K');
#define ISP_NR_LV            0x4C524E49 //MAKEFOURCC('I', 'N', 'R', 'L');
#define ISP_3DNR_LV          0x4C523349 //MAKEFOURCC('I', '3', 'R', 'L');
#define ISP_SHARPNESS_LV     0x4C535349 //MAKEFOURCC('I', 'S', 'S', 'L');
#define ISP_SATURATION_LV    0x4C4E5349 //MAKEFOURCC('I', 'S', 'N', 'L');
#define ISP_CONTRAST_LV      0x4C544349 //MAKEFOURCC('I', 'C', 'T', 'L');
#define ISP_BRIGHTNESS_LV    0x4C534248 //MAKEFOURCC('I', 'B', 'S', 'L');
#define ISP_NIGHT_MODE       0x4D544E49 //MAKEFOURCC('I', 'N', 'T', 'M');
#define AE_STITCH_MODE       0x4D485345 //MAKEFOURCC('E', 'S', 'H', 'M');
#define AWB_STITCH_MODE      0x4D485357 //MAKEFOURCC('W', 'S', 'H', 'M');

#define SENSOR_PRESET_NAME_2 0x324E5053 //MAKEFOURCC('S', 'P', 'N', '2');
#define SENSOR_CHGMODE_FPS_2 0x32464353 //MAKEFOURCC('S', 'C', 'F', '2');
#define SENSOR_PRESET_EXPT_2 0x32455053 //MAKEFOURCC('S', 'P', 'E', '2');
#define SENSOR_PRESET_GAIN_2 0x32475053 //MAKEFOURCC('S', 'P', 'G', '2');
#define SENSOR_EXPT_MAX_2    0x324D4553 //MAKEFOURCC('S', 'E', 'M', '2');
#define SENSOR_I2C_ID_2      0x32494953 //MAKEFOURCC('S', 'I', 'I', '2');
#define SENSOR_I2C_ADDR_2    0x32414953 //MAKEFOURCC('S', 'I', 'A', '2');
#define ISP_PATH_2           0x32485049 //MAKEFOURCC('I', 'P', 'H', '2');
#define ISP_D_GAIN_2         0x32474449 //MAKEFOURCC('I', 'D', 'G', '2');
#define ISP_R_GAIN_2         0x32475249 //MAKEFOURCC('I', 'R', 'G', '2');
#define ISP_G_GAIN_2         0x32474749 //MAKEFOURCC('I', 'G', 'G', '2');
#define ISP_B_GAIN_2         0x32474249 //MAKEFOURCC('I', 'B', 'G', '2');
#define ISP_SHDR_ENABLE_2    0x32455349 //MAKEFOURCC('I', 'S', 'E', '2');
#define ISP_SHDR_PATH_2      0x32505348 //MAKEFOURCC('I', 'S', 'P', '2');
#define ISP_SHDR_MASK_2      0x324D5349 //MAKEFOURCC('I', 'S', 'M', '2');
#define ISP_NR_LV_2          0x32524E49 //MAKEFOURCC('I', 'N', 'R', '2');
#define ISP_3DNR_LV_2        0x32523349 //MAKEFOURCC('I', '3', 'R', '2');
#define ISP_SHARPNESS_LV_2   0x32535349 //MAKEFOURCC('I', 'S', 'S', '2');
#define ISP_SATURATION_LV_2  0x324E5349 //MAKEFOURCC('I', 'S', 'N', '2');
#define ISP_CONTRAST_LV_2    0x32544349 //MAKEFOURCC('I', 'C', 'T', '2');
#define ISP_BRIGHTNESS_LV_2  0x32534248 //MAKEFOURCC('I', 'B', 'S', '2');
#define ISP_NIGHT_MODE_2     0x32544E49 //MAKEFOURCC('I', 'N', 'T', '2');
#define AE_STITCH_MODE_2     0x32485345 //MAKEFOURCC('E', 'S', 'H', '2');
#define AWB_STITCH_MODE_2    0x32485357 //MAKEFOURCC('W', 'S', 'H', '2');

#define JPG_FRAME_OFS        0x534F504A //MAKEFOURCC('J', 'P', 'O', 'S');
#define JPG_FRAME_SIZE       0x5A53504A //MAKEFOURCC('J', 'P', 'S', 'Z');

static BRIDGE_DESC *mp_bridge = NULL;
static pthread_t handle_sensor;
static pthread_t handle_panel;
static pthread_t handle_linux;
static pthread_t handle_audioout;

#define TURN_ON_SENSOR1 0x1
#define TURN_ON_SENSOR2 0x2

#if !defined(_sen_off_)
static int m_turn_on_sensor = TURN_ON_SENSOR1 | TURN_ON_SENSOR2;
#else
static int m_turn_on_sensor = TURN_ON_SENSOR1;
#endif

#define AE_ADC_TABLE_X      3
#define AE_ADC_TABLE_Y      18

static UINT32 ae_adc_tbl[AE_ADC_TABLE_Y][AE_ADC_TABLE_X] = {
{4096,   100, 1000},
{4096,   100, 1000},
{2018,   114, 1000},
{1996,   240, 1000},
{1980,   480, 1000},
{1916,   945, 1000},
{1306,  1892, 1000},
{ 670,  3758, 1000},
{ 340,  7478, 1000},
{ 167, 10000, 1530},
{  84, 20000,  1420},
{  44, 30000,  2310},
{  19, 30000,  4180},
{   4, 30000,  7310},
{   0, 30000, 14700},
{	0, 30000, 14700},
{	0, 30000, 14700},
{	0, 30000, 14700},
};

void ae_adc_get_exp(UINT32 adc_value, UINT32 *exptime, UINT32 *isogain, UINT32 *dgain)
{
	UINT32 idx0, idx1;
	UINT32 sen_expt0, sen_expt1, sen_gain0, sen_gain1;
	UINT32 adc_sen_expt, adc_sen_gain, adc_isp_gain;
	UINT32 adc_ev, ev0, ev1;

	for(idx1=1; idx1<AE_ADC_TABLE_Y; idx1++) {
		if(adc_value > ae_adc_tbl[idx1][0]) {
			break;
		}
	}

	if(idx1 >= AE_ADC_TABLE_Y) {
		idx1 = (AE_ADC_TABLE_Y-1);
	}

	idx0 = idx1-1;

	sen_expt0 = ae_adc_tbl[idx0][1];
	sen_gain0 = (ae_adc_tbl[idx0][2]);
	sen_expt1 = ae_adc_tbl[idx1][1];
	sen_gain1 = (ae_adc_tbl[idx1][2]);
	ev0 = (sen_expt0 * sen_gain0);
	ev1 = (sen_expt1 * sen_gain1);

	adc_ev = ((ev1 - ev0) * (adc_value - ae_adc_tbl[idx1][0]))/(ae_adc_tbl[idx0][0] - ae_adc_tbl[idx1][0]) + ev0;

	adc_sen_expt = sen_expt0;
	adc_sen_gain = (adc_ev / sen_expt0);

	adc_isp_gain = 128;

	if(adc_sen_gain > SENSOR_GAIN_MAX) {
		adc_isp_gain = ((adc_sen_gain<<7)/SENSOR_GAIN_MAX);
		adc_sen_gain = SENSOR_GAIN_MAX;
	}

	if(adc_isp_gain > 1023) {
		adc_isp_gain = 1023;
	}

	*exptime = adc_sen_expt;
	*isogain = adc_sen_gain;
	*dgain = adc_isp_gain;
	//DBG_ERR("idx = {%d, %d}, adc = %d, fast open preset exp = %d, %d\r\n", idx0, idx1, adc_value, *exptime, *isogain);
}

static void adc_dtsi_load(UINT32 adc_value)
{
	unsigned char *p_fdt = (unsigned char *)fdt_get_base();
	int len;
	int nodeoffset;
	const void *nodep;
	UINT32 size, ix, iy;
	UINT32 *p_ae_adc_tbl;

	if (p_fdt == NULL) {
		DBG_ERR("p_fdt is NULL.\n");
		return;
	}

	nodeoffset = fdt_path_offset(p_fdt, "/fastboot/ae_adc_table");
	if (nodeoffset < 0) {
		DBG_ERR("failed to offset for %s = %d \n", "/fastboot/ae_adc_table", nodeoffset);
		return;
	}

	nodep = fdt_getprop(p_fdt, nodeoffset, "valid", &len);
	if (len == 0 || nodep == NULL) {
		DBG_ERR("failed to access /fastboot/ae_adc/valid. \n");
		return;
	}
	ae_adc_ctrl.enable = be32_to_cpu(*(UINT32 *)nodep);

	nodep = fdt_getprop(p_fdt, nodeoffset, "def_sen_expt", &len);
	if (len == 0 || nodep == NULL) {
		DBG_ERR("failed to access /fastboot/ae_adc/def_sen_expt. \n");
		return;
	}
	ae_adc_ctrl.sen_expt = be32_to_cpu(*(UINT32 *)nodep);

	nodep = fdt_getprop(p_fdt, nodeoffset, "def_sen_gain", &len);
	if (len == 0 || nodep == NULL) {
		DBG_ERR("failed to access /fastboot/ae_adc/def_sen_gain. \n");
		return;
	}
	ae_adc_ctrl.sen_gain = be32_to_cpu(*(UINT32 *)nodep);

	nodep = fdt_getprop(p_fdt, nodeoffset, "def_isp_gain", &len);
	if (len == 0 || nodep == NULL) {
		DBG_ERR("failed to access /fastboot/ae_adc/def_isp_gain. \n");
		return;
	}
	ae_adc_ctrl.isp_gain = be32_to_cpu(*(UINT32 *)nodep);

	nodep = fdt_getprop(p_fdt, nodeoffset, "adc_tbl_thr", &len);
	if (len == 0 || nodep == NULL) {
		DBG_ERR("failed to access /fastboot/ae_adc/adc_tbl_thr. \n");
		return;
	}
	ae_adc_ctrl.adc_tbl_thr = be32_to_cpu(*(UINT32 *)nodep);

	nodep = fdt_getprop(p_fdt, nodeoffset, "ir_mode_en", &len);
	if (len == 0 || nodep == NULL) {
		DBG_ERR("failed to access /fastboot/ae_adc/ir_mode_en. \n");
		return;
	}
	ae_adc_ctrl.ir_mode_en = be32_to_cpu(*(UINT32 *)nodep);

	nodep = fdt_getprop(p_fdt, nodeoffset, "ir_mode_thr", &len);
	if (len == 0 || nodep == NULL) {
		DBG_ERR("failed to access /fastboot/ae_adc/ir_mode_thr. \n");
		return;
	}
	ae_adc_ctrl.ir_mode_thr = be32_to_cpu(*(UINT32 *)nodep);

#if 0
	DBG_ERR("\r\n");
	DBG_ERR("adc_en = %d, tbl_thr = %d, ir_en = %d, ir_thr = %d, adc_value = %d\r\n", ae_adc_ctrl.enable, ae_adc_ctrl.adc_tbl_thr, ae_adc_ctrl.ir_mode_en, ae_adc_ctrl.ir_mode_thr, adc_value);
	DBG_ERR("def_sen_expt = %d, def_sen_gain = %d, def_isp_gain = %d\r\n", ae_adc_ctrl.sen_expt, ae_adc_ctrl.sen_gain, ae_adc_ctrl.isp_gain);
	DBG_ERR("\r\n");
#endif
	nodep = fdt_getprop(p_fdt, nodeoffset, "size", &len);
	if (len == 0 || nodep == NULL) {
		DBG_ERR("failed to access /fastboot/ae_adc/size. \n");
		return;
	}
	size = be32_to_cpu(*(UINT32 *)nodep);

	if(adc_value > ae_adc_ctrl.adc_tbl_thr) {
		nodep = fdt_getprop(p_fdt, nodeoffset, "adc_expt_gain_1", &len);
		if (len == 0 || nodep == NULL) {
			DBG_ERR("failed to access /fastboot/ae_adc/adc_expt_gain. \n");
			return;
		}
	} else {
		nodep = fdt_getprop(p_fdt, nodeoffset, "adc_expt_gain_0", &len);
		if (len == 0 || nodep == NULL) {
			DBG_ERR("failed to access /fastboot/ae_adc/adc_expt_gain. \n");
			return;
		}
	}

	if (size != (UINT32)len) {
		DBG_ERR("ae adc table size incorrect. %d %d \n", size, (UINT32)len);
		return;
	}

	p_ae_adc_tbl = (UINT32 *)nodep;
	for(iy = 0; iy < AE_ADC_TABLE_Y; iy++) {
		for(ix = 0; ix < AE_ADC_TABLE_X; ix++) {
			// NOTE: update adc table here
			ae_adc_tbl[iy][ix] = be32_to_cpu(p_ae_adc_tbl[iy*AE_ADC_TABLE_X+ix]);
		}

		//DBG_ERR("ae_adc_tbl = %4d %6d %6d \n", ae_adc_tbl[iy][0], ae_adc_tbl[iy][1], ae_adc_tbl[iy][2]);
	}
}

UINT32 adc_get_value(UINT32 times, UINT32 delay)
{
	UINT32 i, retry = 0;
	UINT32 adc_avg, adc_cnt;
	UINT32 adc_mV;

	if (adc_open(ADC_CH_PHOTORESISTOR)) {
		printf("Can't open ADC channel for photoresistor\r\n");
		return 0xFFFFFFFF;
	}

	adc_setChConfig(ADC_CH_PHOTORESISTOR, ADC_CH_CONFIG_ID_SAMPLE_FREQ, 10000); //10K Hz, sample once about 100 us for CONTINUOUS mode
	//adc_setChConfig(ADC_CH_PHOTORESISTOR, ADC_CH_CONFIG_ID_SAMPLE_MODE, ADC_CH_SAMPLEMODE_ONESHOT);
	adc_setChConfig(ADC_CH_PHOTORESISTOR, ADC_CH_CONFIG_ID_SAMPLE_MODE, ADC_CH_SAMPLEMODE_CONTINUOUS);
	adc_setChConfig(ADC_CH_PHOTORESISTOR, ADC_CH_CONFIG_ID_INTEN, FALSE);
	adc_setEnable(TRUE);
	adc_triggerOneShot(ADC_CH_PHOTORESISTOR);
	while (FALSE == adc_isDataReady(ADC_CH_PHOTORESISTOR) && retry < ADC_READY_RETRY_CNT) {
	    vos_util_delay_us_polling(10);
		retry++;
	}

	adc_avg = 0;
	adc_cnt = 0;

	for(i=0; i<times; i++) {
		adc_mV = adc_readVoltage(ADC_CH_PHOTORESISTOR);
		adc_avg += adc_mV;
		//DBG_ERR("photoresistor voltage = %d\r\n", (unsigned int)adc_mV);
		vos_util_delay_us_polling(delay);
		adc_cnt++;
	}

	if(adc_cnt < 1) {
		adc_cnt = 1;
	}

	adc_avg /= adc_cnt;

	adc_close(ADC_CH_PHOTORESISTOR);

	return adc_avg;
}

static void adc_get_preset(UINT32 adc_value, UINT32 *p_exp_time, UINT32 *p_gain_ratio)
{
	UINT32 adc_sen_expt, adc_sen_gain, adc_dgain;

	ae_adc_get_exp(adc_value, &adc_sen_expt, &adc_sen_gain, &adc_dgain);

	*p_exp_time = adc_sen_expt;
	*p_gain_ratio = adc_sen_gain;
	isp_dgain = adc_dgain;

	//DBG_ERR("adc_mV = %d, adc_exp =========== %d, %d, %d\r\n", (unsigned int)adc_value, (unsigned int)adc_sen_expt, (unsigned int)adc_sen_gain, (unsigned int)adc_dgain);
}

static void fast_open_sensor(void)
{
	UINT32 adc_value;

	VENDOR_VIDEOCAP_FAST_OPEN_SENSOR cap_cfg = {0};

	snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, SEN_DRV_NAME(_SEN1_));
	cap_cfg.sen_cfg.sen_dev.if_type = SEN_IF_TYPE_T1;
	#if FUNC_TGE_SYNC
	cap_cfg.sen_cfg.sen_dev.if_cfg.tge.tge_en = TRUE;
	cap_cfg.sen_cfg.sen_dev.if_cfg.tge.swap = FALSE;
	cap_cfg.sen_cfg.sen_dev.if_cfg.tge.vcap_vd_src = HD_VIDEOCAP_SEN_TGE_CH1_VD_TO_VCAP0;
	cap_cfg.sen_cfg.sen_dev.if_cfg.tge.vcap_sync_set = TGE_SYNC_VCAP_ID;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  0x2A0;//PIN_SENSOR_CFG_LVDS_VDHD | PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
	#else
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  SEN_PINMUX_T1;
	#endif
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.serial_if_pinmux = SERIAL_IF_PINMUX;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = CMD_IF_PINMUX;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.clk_lane_sel = CLK_LANE_SEL;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[0] = PIN_MAP_0_T1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[1] = PIN_MAP_1_T1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = PIN_MAP_2_T1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = PIN_MAP_3_T1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[4] = PIN_MAP_4_T1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[5] = PIN_MAP_5_T1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[6] = PIN_MAP_6_T1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[7] = PIN_MAP_7_T1;
	#if FUNC_SHDR_T1
	cap_cfg.sen_cfg.shdr_map = HD_VIDEOCAP_SHDR_MAP(HD_VIDEOCAP_HDR_SENSOR1, SEN1_SHDR_VCAP_MAP);
	isp_sensor_shdr_map_path = HDR_SEN1_SUB_VCAP_ID;
	isp_sensor_shdr_id_mask = SEN1_SHDR_VCAP_MAP;
	cap_cfg.out_frame_num = HD_VIDEOCAP_SEN_FRAME_NUM_2;
	#else
	isp_sensor_shdr_map_path = 0xFFFFFFFF;
	isp_sensor_shdr_id_mask = HD_VIDEOCAP_0;
	cap_cfg.out_frame_num = HD_VIDEOCAP_SEN_FRAME_NUM_1;
	#endif
	cap_cfg.sen_mode = HD_VIDEOCAP_SEN_MODE_AUTO;
	cap_cfg.dim.w = VDO_SIZE_W_T1;
	cap_cfg.dim.h = VDO_SIZE_H_T1;
	cap_cfg.pxlfmt = SEN_OUT_FMT;
	cap_cfg.data_lane = DATA_LANE_T1;


	cap_cfg.frc = HD_VIDEO_FRC_RATIO(SEN_CHGMODE_FPS_T1/100,1);

	// TODO:
	#if 0
	cap_cfg.ae_preset.enable = TRUE;
	#elif 0
	cap_cfg.ae_preset.enable = FALSE;
	ae_adc_ctrl.enable = DISABLE;
	#else
	cap_cfg.ae_preset.enable = TRUE;
	ae_adc_ctrl.enable = DISABLE;
	#endif

	adc_value = adc_get_value(10, 10);

	if(ae_adc_ctrl.enable == ENABLE) {
		adc_dtsi_load(adc_value);
	}

	if(ae_adc_ctrl.enable == ENABLE) {
		if(ae_adc_ctrl.ir_mode_en) {
			if(adc_value > ae_adc_ctrl.ir_mode_thr) {
				isp_night_mode = 0;
			} else {
				isp_night_mode = 1;
			}
		} else {
			isp_night_mode = 0;
		}
	}

	if(ae_adc_ctrl.enable == ENABLE) {
		adc_get_preset(adc_value, &cap_cfg.ae_preset.exp_time, &cap_cfg.ae_preset.gain_ratio);
		printf("[fast_open_sensor] adc_en = %d, adc_value = %d, expt = %d, gain = %d, isp_gain = %d, ir_thr = %d, night_mode = %d\r\n", (unsigned int)ae_adc_ctrl.enable, (unsigned int)adc_value, (unsigned int)cap_cfg.ae_preset.exp_time, (unsigned int)cap_cfg.ae_preset.gain_ratio, (unsigned int)isp_dgain, (unsigned int)ae_adc_ctrl.ir_mode_thr, (unsigned int)isp_night_mode);
	} else {
		cap_cfg.ae_preset.exp_time = 10000;
		cap_cfg.ae_preset.gain_ratio = 1000;
		printf("[fast_open_sensor] expt = %d, gain = %d \r\n", (unsigned int)cap_cfg.ae_preset.exp_time, (unsigned int)cap_cfg.ae_preset.gain_ratio);
	}

	//printf("[fast_open_sensor] adc_en = %d, adc_value = %d, expt = %d, gain = %d, isp_gain = %d, ir_thr = %d, night_mode = %d\r\n", (unsigned int)ae_adc_ctrl.enable, (unsigned int)adc_value, (unsigned int)cap_cfg.ae_preset.exp_time, (unsigned int)cap_cfg.ae_preset.gain_ratio, (unsigned int)isp_dgain, (unsigned int)ae_adc_ctrl.ir_mode_thr, (unsigned int)isp_night_mode);

	vendor_videocap_set(0, VENDOR_VIDEOCAP_PARAM_FAST_OPEN_SENSOR, &cap_cfg);
}

//this sample is base on dual os02k10
static void fast_open_sensor2(void)
{
	//UINT32 adc_value;

	VENDOR_VIDEOCAP_FAST_OPEN_SENSOR cap_cfg = {0};

	snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, SEN_DRV_NAME(_SEN2_));
	cap_cfg.sen_cfg.sen_dev.if_type = SEN_IF_TYPE_T2;
	#if FUNC_TGE_SYNC
	cap_cfg.sen_cfg.sen_dev.if_cfg.tge.tge_en = TRUE;
	cap_cfg.sen_cfg.sen_dev.if_cfg.tge.swap = FALSE;
	cap_cfg.sen_cfg.sen_dev.if_cfg.tge.vcap_vd_src = HD_VIDEOCAP_SEN_TGE_CH1_VD_TO_VCAP0;
	cap_cfg.sen_cfg.sen_dev.if_cfg.tge.vcap_sync_set = TGE_SYNC_VCAP_ID;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  0x220; //PIN_SENSOR2_CFG_MIPI | PIN_SENSOR2_CFG_MCLK
	#else
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  0x20; //PIN_SENSOR2_CFG_MIPI
	#endif
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.serial_if_pinmux = 0xC02;//PIN_MIPI_LVDS_CFG_CLK1 | PIN_MIPI_LVDS_CFG_DAT2 | PIN_MIPI_LVDS_CFG_DAT3
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = 0x01;//PIN_I2C_CFG_CH1
	cap_cfg.sen_cfg.sen_dev.pin_cfg.clk_lane_sel = HD_VIDEOCAP_SEN_CLANE_SEL_CSI1_USE_C1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[0] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[1] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = 0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = 1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[4] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[5] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[6] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[7] = HD_VIDEOCAP_SEN_IGNORE;
	#if FUNC_SHDR_T2
	cap_cfg.sen_cfg.shdr_map = HD_VIDEOCAP_SHDR_MAP(HD_VIDEOCAP_HDR_SENSOR2, DUAL_SEN2_SHDR_VCAP_MAP);
	isp_sensor_path_2 = HDR_SEN2_VCAP_ID;
	isp_sensor_shdr_map_path_2 = HDR_SEN2_SUB_VCAP_ID;
	isp_sensor_shdr_id_mask_2 = DUAL_SEN2_SHDR_VCAP_MAP;
	cap_cfg.out_frame_num = HD_VIDEOCAP_SEN_FRAME_NUM_2;
	#else
	isp_sensor_path_2 = LINEAR_SEN2_VCAP_ID;
	isp_sensor_shdr_map_path_2 = 0xFFFFFFFF;
	isp_sensor_shdr_id_mask_2 = (1 << LINEAR_SEN2_VCAP_ID);
	cap_cfg.out_frame_num = HD_VIDEOCAP_SEN_FRAME_NUM_1;
	#endif
	cap_cfg.sen_mode = HD_VIDEOCAP_SEN_MODE_AUTO;
	cap_cfg.dim.w = VDO_SIZE_W_T2;
	cap_cfg.dim.h = VDO_SIZE_H_T2;
	cap_cfg.pxlfmt = SEN_OUT_FMT;
	cap_cfg.data_lane = DATA_LANE_T2;

	cap_cfg.frc = HD_VIDEO_FRC_RATIO(SEN_CHGMODE_FPS_T2/100,1);

#if 0
	cap_cfg.ae_preset.enable = TRUE;

	adc_value = adc_get_value(10, 10);

	adc_dtsi_load(adc_value);

	if(ae_adc_ctrl.enable == ENABLE) {
		if(ae_adc_ctrl.ir_mode_en) {
			if(adc_value > ae_adc_ctrl.ir_mode_thr) {
				isp_night_mode = 0;
			} else {
				isp_night_mode = 1;
			}
		} else {
			isp_night_mode = 0;
		}
	}

	if(ae_adc_ctrl.enable == ENABLE) {
		adc_get_preset(adc_value, &cap_cfg.ae_preset.exp_time, &cap_cfg.ae_preset.gain_ratio);
	} else {
		cap_cfg.ae_preset.exp_time = ae_adc_ctrl.sen_expt;
		cap_cfg.ae_preset.gain_ratio = ae_adc_ctrl.sen_gain;
	}

	printf("[fast_open_sensor] adc_en = %d, adc_value = %d, expt = %d, gain = %d, isp_gain = %d, ir_thr = %d, night_mode = %d\r\n", (unsigned int)ae_adc_ctrl.enable, (unsigned int)adc_value, (unsigned int)cap_cfg.ae_preset.exp_time, (unsigned int)cap_cfg.ae_preset.gain_ratio, (unsigned int)isp_dgain, (unsigned int)ae_adc_ctrl.ir_mode_thr, (unsigned int)isp_night_mode);
#endif
	vendor_videocap_set(1, VENDOR_VIDEOCAP_PARAM_FAST_OPEN_SENSOR, &cap_cfg);
}

#if !defined(_disp_off_)
#include "dispdev_ioctrl.h"
ER fast_dispdev1_ioctrl(DISPDEV_IOCTRL_OP disp_dev_ctrl, PDISPDEV_IOCTRL_PARAM p_disp_dev_param)
{
    if(disp_dev_ctrl==DISPDEV_IOCTRL_GET_REG_IF)
    {
		p_disp_dev_param->SEL.GET_REG_IF.lcd_ctrl     = DISPDEV_LCDCTRL_SIF;
		p_disp_dev_param->SEL.GET_REG_IF.ui_sif_ch    = SIF_CH2;
		p_disp_dev_param->SEL.GET_REG_IF.ui_gpio_sen   = 0;
		p_disp_dev_param->SEL.GET_REG_IF.ui_gpio_clk   = 0;
		p_disp_dev_param->SEL.GET_REG_IF.ui_gpio_data  = 0;
    }
    return 0;
}
static void fast_open_panel(void)
{
	DISPDEV_OBJ *p_dev = dispdev_get_lcd1_dev_obj();
    p_dev->set_dev_io_ctrl((FP)fast_dispdev1_ioctrl);
    p_dev->open();
}
#endif

int bridge_mem_init(void)
{
	// plan the bridge memory
	unsigned bridge_addr = 0;
	unsigned bridge_size = 0;
	unsigned char *p_fdt = (unsigned char *)fdt_get_base();

	if (p_fdt == NULL) {
		DBG_ERR("p_fdt is NULL.\n");
		return -1;
	}

	// read from fdt
	int len, nodeoffset;
	const void *nodep;
	const char path[] = "/nvt_memory_cfg/bridge";
	nodeoffset = fdt_path_offset(p_fdt, path);
	if (nodeoffset < 0) {
		DBG_ERR("failed to offset for  %s = %d \n", path, nodeoffset);
		return -1;
	}
	nodep = fdt_getprop(p_fdt, nodeoffset, "reg", &len);
	if (len == 0 || nodep == NULL) {
		DBG_ERR("failed to access reg.\n");
		return -1;
	} else {
		unsigned int *p_data = (unsigned int *)nodep;
		bridge_addr = be32_to_cpu(p_data[0]);
		bridge_size = be32_to_cpu(p_data[1]);
	}

	// start to plan
	mp_bridge = (BRIDGE_DESC *)bridge_addr;
	memset(mp_bridge, 0, sizeof(BRIDGE_DESC));
	mp_bridge->bridge_fourcc = BRIDGE_FOURCC;
	mp_bridge->bridge_size = sizeof(BRIDGE_DESC);
	mp_bridge->phy_addr = vos_cpu_get_phy_addr((VOS_ADDR) bridge_addr);
	mp_bridge->phy_size = bridge_size;

	return 0;
}

int bridge_mem_add_tag(unsigned int tag, unsigned val)
{
	int i;
	for(i = 0; i < BRIDGE_MAX_OPT_CNT; i++) {
		if (mp_bridge->opts[i].tag == 0) {
			mp_bridge->opts[i].tag = tag;
			mp_bridge->opts[i].val = val;
			return 0;
		}
		if (mp_bridge->opts[i].tag == tag) {
			DBG_ERR("tag: 0x%08X has been registered.\n", tag);
			return -1;
		}
	}

	DBG_ERR("BRIDGE_MAX_OPT_CNT is not enough.\n");
	return -2;
}

int bridge_mem_modify_tag(unsigned int tag, unsigned val)
{
	int i;
	for(i = 0; i < BRIDGE_MAX_OPT_CNT; i++) {
		if (mp_bridge->opts[i].tag == tag) {
			mp_bridge->opts[i].val = val;
			return 0;
		}
	}

	DBG_ERR("unable to find tag: 0x%08X\n", tag);
	return -1;
}

static int bridge_mem_plan_sensor1(void)
{
	ISP_SENSOR_CTRL sensor_ctrl_expt, sensor_ctrl_gain;
	UINT32 sensor_name, sensor_i2c_id, sensor_i2c_addr;

	// start to plan
	// add iq tag with value
	#if defined(_sen_imx290_TYPE1_)
	sensor_name = 0;                               // 0 as imx290
	sen_get_expt_imx290(0, &sensor_ctrl_expt);     // get expt from path 1
	sen_get_gain_imx290(0, &sensor_ctrl_gain);     // get gain from path 1
	sen_get_i2c_id_imx290(0, &sensor_i2c_id);      // get i2c id from path 1
	sen_get_i2c_addr_imx290(0, &sensor_i2c_addr);  // get i2c addr from path 1
	#elif defined(_sen_f37_TYPE1_)
	sensor_name = 1;                               // 1 as f37
	sen_get_expt_f37(0, &sensor_ctrl_expt);        // get expt from path 1
	sen_get_gain_f37(0, &sensor_ctrl_gain);        // get gain from path 1
	sen_get_i2c_id_f37(0, &sensor_i2c_id);         // get i2c id from path 1
	sen_get_i2c_addr_f37(0, &sensor_i2c_addr);     // get i2c addr from path 1
	#elif defined(_sen_os02k10_TYPE1_)
	sensor_name = 2;                               // 2 as os02k10
	sen_get_expt_os02k10(0, &sensor_ctrl_expt);    // get expt from path 1
	sen_get_gain_os02k10(0, &sensor_ctrl_gain);    // get gain from path 1
	sen_get_i2c_id_os02k10(0, &sensor_i2c_id);     // get i2c id from path 1
	sen_get_i2c_addr_os02k10(0, &sensor_i2c_addr); // get i2c addr from path 1
	#elif defined(_sen_os05a10_TYPE1_)
	sensor_name = 3;                               // 3 as os05a10
	sen_get_expt_os05a10(0, &sensor_ctrl_expt);    // get expt from path 1
	sen_get_gain_os05a10(0, &sensor_ctrl_gain);    // get gain from path 1
	sen_get_i2c_id_os05a10(0, &sensor_i2c_id);     // get i2c id from path 1
	sen_get_i2c_addr_os05a10(0, &sensor_i2c_addr); // get i2c addr from path 1
	#elif defined(_sen_f35_TYPE1_)
	sensor_name = 4;                               // 4 as f35
	sen_get_expt_f35(0, &sensor_ctrl_expt);        // get expt from path 1
	sen_get_gain_f35(0, &sensor_ctrl_gain);        // get gain from path 1
	sen_get_i2c_id_f35(0, &sensor_i2c_id);         // get i2c id from path 1
	sen_get_i2c_addr_f35(0, &sensor_i2c_addr);     // get i2c addr from path 1
	#elif defined(_sen_gc4653_TYPE1_)
	sensor_name = 5;                               // 5 as ggc4653
	sen_get_expt_gc4653(0, &sensor_ctrl_expt);     // get expt from path 1
	sen_get_gain_gc4653(0, &sensor_ctrl_gain);     // get gain from path 1
	sen_get_i2c_id_gc4653(0, &sensor_i2c_id);      // get i2c id from path 1
	sen_get_i2c_addr_gc4653(0, &sensor_i2c_addr);  // get i2c addr from path 1
	#elif defined(_sen_gc4653_slave_TYPE1_)
	sensor_name = 6;                               // 6 as ggc4653
	sen_get_expt_gc4653_slave(0, &sensor_ctrl_expt);     // get expt from path 1
	sen_get_gain_gc4653_slave(0, &sensor_ctrl_gain);     // get gain from path 1
	sen_get_i2c_id_gc4653_slave(0, &sensor_i2c_id);      // get i2c id from path 1
	sen_get_i2c_addr_gc4653_slave(0, &sensor_i2c_addr);  // get i2c addr from path 1
	#else
	sensor_ctrl_expt.exp_time[0] = 10000;
	sensor_ctrl_gain.gain_ratio[0] = 1000;
	sensor_name = 0;
	#endif

	if (sensor_ctrl_expt.exp_time[0] == 0) {
		sensor_ctrl_expt.exp_time[0] = 10000;
	}
	if (sensor_ctrl_gain.gain_ratio[0] == 0) {
		sensor_ctrl_gain.gain_ratio[0] = 1000;
	}

	bridge_mem_modify_tag(SENSOR_PRESET_NAME, sensor_name);
	bridge_mem_add_tag(SENSOR_CHGMODE_FPS, SEN_CHGMODE_FPS_T1);
	bridge_mem_add_tag(SENSOR_PRESET_EXPT, sensor_ctrl_expt.exp_time[0]);
	bridge_mem_add_tag(SENSOR_PRESET_GAIN, sensor_ctrl_gain.gain_ratio[0]);
	bridge_mem_add_tag(SENSOR_EXPT_MAX, SEN_EXPT_MAX_T1);
	bridge_mem_add_tag(SENSOR_I2C_ID, sensor_i2c_id);
	bridge_mem_add_tag(SENSOR_I2C_ADDR, sensor_i2c_addr);
	bridge_mem_add_tag(ISP_D_GAIN, isp_dgain);
	bridge_mem_add_tag(ISP_R_GAIN, isp_rgain);
	bridge_mem_add_tag(ISP_G_GAIN, isp_ggain);
	bridge_mem_add_tag(ISP_B_GAIN, isp_bgain);
	bridge_mem_add_tag(ISP_SHDR_ENABLE, FUNC_SHDR_T1);
	bridge_mem_add_tag(ISP_SHDR_PATH, isp_sensor_shdr_map_path);
	bridge_mem_add_tag(ISP_SHDR_MASK, isp_sensor_shdr_id_mask);
	bridge_mem_add_tag(ISP_NR_LV, isp_nr_lv);
	bridge_mem_add_tag(ISP_3DNR_LV, isp_3dnr_lv);
	bridge_mem_add_tag(ISP_SHARPNESS_LV, isp_sharpness_lv);
	bridge_mem_add_tag(ISP_SATURATION_LV, isp_saturation_lv);
	bridge_mem_add_tag(ISP_CONTRAST_LV, isp_contrast_lv);
	bridge_mem_add_tag(ISP_BRIGHTNESS_LV, isp_brightness_lv);
	bridge_mem_add_tag(ISP_NIGHT_MODE, isp_night_mode);
	bridge_mem_add_tag(AE_STITCH_MODE, isp_ae_stitch_mode);
	bridge_mem_add_tag(AWB_STITCH_MODE, isp_awb_stitch_mode);

	return 0;
}

int bridge_mem_plan_sensor2(void)
{
	ISP_SENSOR_CTRL sensor_ctrl_expt, sensor_ctrl_gain;
	UINT32 sensor_name, sensor_i2c_id, sensor_i2c_addr;

	// start to plan
	// add iq tag with value
	#if defined(_sen_imx290_TYPE2_)
	sensor_name = 0;                               // 0 as imx290
	sen_get_expt_imx290(1, &sensor_ctrl_expt);     // get expt from path 1
	sen_get_gain_imx290(1, &sensor_ctrl_gain);     // get gain from path 1
	sen_get_i2c_id_imx290(1, &sensor_i2c_id);      // get i2c id from path 1
	sen_get_i2c_addr_imx290(1, &sensor_i2c_addr);  // get i2c addr from path 1
	#elif defined(_sen_f37_TYPE2_)
	sensor_name = 1;                               // 1 as f37
	sen_get_expt_f37(1, &sensor_ctrl_expt);        // get expt from path 1
	sen_get_gain_f37(1, &sensor_ctrl_gain);        // get gain from path 1
	sen_get_i2c_id_f37(1, &sensor_i2c_id);         // get i2c id from path 1
	sen_get_i2c_addr_f37(1, &sensor_i2c_addr);     // get i2c addr from path 1
	#elif defined(_sen_os02k10_TYPE2_)
	sensor_name = 2;                               // 2 as os02k10
	sen_get_expt_os02k10(1, &sensor_ctrl_expt);    // get expt from path 1
	sen_get_gain_os02k10(1, &sensor_ctrl_gain);    // get gain from path 1
	sen_get_i2c_id_os02k10(1, &sensor_i2c_id);     // get i2c id from path 1
	sen_get_i2c_addr_os02k10(1, &sensor_i2c_addr); // get i2c addr from path 1
	#elif defined(_sen_os05a10_TYPE2_)
	sensor_name = 3;                               // 3 as os05a10
	sen_get_expt_os05a10(1, &sensor_ctrl_expt);    // get expt from path 1
	sen_get_gain_os05a10(1, &sensor_ctrl_gain);    // get gain from path 1
	sen_get_i2c_id_os05a10(1, &sensor_i2c_id);     // get i2c id from path 1
	sen_get_i2c_addr_os05a10(1, &sensor_i2c_addr); // get i2c addr from path 1
	#elif defined(_sen_f35_TYPE2_)
	sensor_name = 4;                               // 4 as f35
	sen_get_expt_f35(1, &sensor_ctrl_expt);        // get expt from path 1
	sen_get_gain_f35(1, &sensor_ctrl_gain);        // get gain from path 1
	sen_get_i2c_id_f35(1, &sensor_i2c_id);         // get i2c id from path 1
	sen_get_i2c_addr_f35(1, &sensor_i2c_addr);     // get i2c addr from path 1
	#elif defined(_sen_gc4653_TYPE2_)
	sensor_name = 5;                               // 5 as ggc4653
	sen_get_expt_gc4653(1, &sensor_ctrl_expt);     // get expt from path 1
	sen_get_gain_gc4653(1, &sensor_ctrl_gain);     // get gain from path 1
	sen_get_i2c_id_gc4653(1, &sensor_i2c_id);      // get i2c id from path 1
	sen_get_i2c_addr_gc4653(1, &sensor_i2c_addr);  // get i2c addr from path 1
	#elif defined(_sen_gc4653_slave_TYPE2_)
	sensor_name = 6;                               // 6 as ggc4653_slave
	sen_get_expt_gc4653_slave(1, &sensor_ctrl_expt);     // get expt from path 1
	sen_get_gain_gc4653_slave(1, &sensor_ctrl_gain);     // get gain from path 1
	sen_get_i2c_id_gc4653_slave(1, &sensor_i2c_id);      // get i2c id from path 1
	sen_get_i2c_addr_gc4653_slave(1, &sensor_i2c_addr);  // get i2c addr from path 1
	#elif defined(_sen_off_TYPE2_)
	sensor_name = 0;                               // 0 as imx290
	sen_get_expt_imx290(1, &sensor_ctrl_expt);     // get expt from path 1
	sen_get_gain_imx290(1, &sensor_ctrl_gain);     // get gain from path 1
	sen_get_i2c_id_imx290(1, &sensor_i2c_id);      // get i2c id from path 1
	sen_get_i2c_addr_imx290(1, &sensor_i2c_addr);  // get i2c addr from path 1
	#else
	sensor_ctrl_expt.exp_time[0] = 10000;
	sensor_ctrl_gain.gain_ratio[0] = 1000;
	sensor_name = 0;
	#endif

	if (sensor_ctrl_expt.exp_time[0] == 0) {
		sensor_ctrl_expt.exp_time[0] = 10000;
	}
	if (sensor_ctrl_gain.gain_ratio[0] == 0) {
		sensor_ctrl_gain.gain_ratio[0] = 1000;
	}

	bridge_mem_modify_tag(SENSOR_PRESET_NAME_2, sensor_name);
	bridge_mem_add_tag(SENSOR_CHGMODE_FPS_2, SEN_CHGMODE_FPS_T2);
	bridge_mem_add_tag(SENSOR_PRESET_EXPT_2, sensor_ctrl_expt.exp_time[0]);
	bridge_mem_add_tag(SENSOR_PRESET_GAIN_2, sensor_ctrl_gain.gain_ratio[0]);
	bridge_mem_add_tag(SENSOR_EXPT_MAX_2, SEN_EXPT_MAX_T2);
	bridge_mem_add_tag(SENSOR_I2C_ID_2, sensor_i2c_id);
	bridge_mem_add_tag(SENSOR_I2C_ADDR_2, sensor_i2c_addr);
	bridge_mem_add_tag(ISP_PATH_2, isp_sensor_path_2);
	bridge_mem_add_tag(ISP_D_GAIN_2, isp_dgain);
	bridge_mem_add_tag(ISP_R_GAIN_2, isp_rgain);
	bridge_mem_add_tag(ISP_G_GAIN_2, isp_ggain);
	bridge_mem_add_tag(ISP_B_GAIN_2, isp_bgain);
	bridge_mem_add_tag(ISP_SHDR_ENABLE_2, FUNC_SHDR_T2);
	bridge_mem_add_tag(ISP_SHDR_PATH_2, isp_sensor_shdr_map_path_2);
	bridge_mem_add_tag(ISP_SHDR_MASK_2, isp_sensor_shdr_id_mask_2);
	bridge_mem_add_tag(ISP_NR_LV_2, isp_nr_lv);
	bridge_mem_add_tag(ISP_3DNR_LV_2, isp_3dnr_lv);
	bridge_mem_add_tag(ISP_SHARPNESS_LV_2, isp_sharpness_lv);
	bridge_mem_add_tag(ISP_SATURATION_LV_2, isp_saturation_lv);
	bridge_mem_add_tag(ISP_CONTRAST_LV_2, isp_contrast_lv);
	bridge_mem_add_tag(ISP_BRIGHTNESS_LV_2, isp_brightness_lv);
	bridge_mem_add_tag(ISP_NIGHT_MODE_2, isp_night_mode);
	bridge_mem_add_tag(AE_STITCH_MODE_2, isp_ae_stitch_mode);
	bridge_mem_add_tag(AWB_STITCH_MODE_2, isp_awb_stitch_mode);

	return 0;
}

// plan the bridge memory
int bridge_mem_plan(void)
{
	//init all sensor by default off
	bridge_mem_add_tag(SENSOR_PRESET_NAME, 0xFFFFFFFF);
	bridge_mem_add_tag(SENSOR_PRESET_NAME_2, 0xFFFFFFFF);
	if (m_turn_on_sensor & TURN_ON_SENSOR1) {
		bridge_mem_plan_sensor1();
	}
	if (m_turn_on_sensor & TURN_ON_SENSOR2) {
		bridge_mem_plan_sensor2();
	}
	return 0;
}



static int is_fastboot(void)
{
	unsigned char *p_fdt = (unsigned char *)fdt_get_base();

	if (p_fdt == NULL) {
		DBG_ERR("p_fdt is NULL.\n");
		return 0;
	}

	int len;
	int nodeoffset;
	const void *nodep;  /* property node pointer */

	// get linux space
	nodeoffset = fdt_path_offset(p_fdt, "/fastboot");
	if (nodeoffset < 0) {
		DBG_ERR("failed to offset for  %s = %d \n", "/fastboot", nodeoffset);
		return 0;
	}

	nodep = fdt_getprop(p_fdt, nodeoffset, "enable", &len);
	if (len == 0 || nodep == NULL) {
		DBG_ERR("failed to access enable.\n");
		return 0;
	} else {
		unsigned int *p_data = (unsigned int *)nodep;
		return be32_to_cpu(p_data[0]);
	}
	return 0;
}

#if (CFG_LOAD_AI_MODEL)
static int get_ai_model(unsigned int *p_addr, unsigned int *p_size, unsigned long long *p_flash_ofs)
{
	unsigned char *p_fdt = (unsigned char *)fdt_get_base();

	if (p_fdt == NULL) {
		DBG_ERR("p_fdt is NULL.\n");
		return -1;
	}

	int len;
	int nodeoffset;
	unsigned int partition_size;
	const void *nodep;  /* property node pointer */

	// check case of fastboot-ai
	nodeoffset = fdt_path_offset(p_fdt, "/fastboot/ai_model");
	if (nodeoffset >= 0) {
		nodep = fdt_getprop(p_fdt, nodeoffset, "load_dst", &len);
		if (len == 0 || nodep == NULL) {
			DBG_ERR("failed to access load_dst.\n");
			return -1;
		} else {
			unsigned int *p_data = (unsigned int *)nodep;
			*p_addr = be32_to_cpu(p_data[0]);
			*p_size = be32_to_cpu(p_data[1]);
		}

		nodeoffset = fdt_path_offset(p_fdt, PARTITION_PATH_AI);
		if (nodeoffset < 0) {
			DBG_ERR("no ai_flash\n");
			return 1;
		}

		nodep = fdt_getprop(p_fdt, nodeoffset, "reg", &len);
		if (len == 0 || nodep == NULL) {
			DBG_ERR("failed to access reg.\n");
			return -1;
		} else {
			unsigned long long *p_data = (unsigned long long *)nodep;
			*p_flash_ofs = be64_to_cpu(p_data[0]);
			partition_size = (unsigned int)be64_to_cpu(p_data[1]);
			if (partition_size < *p_size) {
				*p_size = partition_size;
				DBG_ERR("ai size(0x%X) > partition size (0x%X)\r\n", *p_size, partition_size);
				return -1;
			}
		}
		return 0;
	}

	// check case of fastboot load ai model only
	nodeoffset = fdt_path_offset(p_fdt, "/ai-memory/ai_model");
	if (nodeoffset >= 0) {
		nodep = fdt_getprop(p_fdt, nodeoffset, "load_dst", &len);
		if (len == 0 || nodep == NULL) {
			DBG_ERR("failed to access load_dst.\n");
			return -1;
		} else {
			unsigned int *p_data = (unsigned int *)nodep;
			*p_addr = be32_to_cpu(p_data[0]);
			*p_size = be32_to_cpu(p_data[1]);
		}

		nodeoffset = fdt_path_offset(p_fdt, PARTITION_PATH_AI);
		if (nodeoffset < 0) {
			DBG_ERR("no ai_flash\n");
			return 1;
		}

		nodep = fdt_getprop(p_fdt, nodeoffset, "reg", &len);
		if (len == 0 || nodep == NULL) {
			DBG_ERR("failed to access reg.\n");
			return -1;
		} else {
			unsigned long long *p_data = (unsigned long long *)nodep;
			*p_flash_ofs = be64_to_cpu(p_data[0]);
			partition_size = (unsigned int)be64_to_cpu(p_data[1]);
			if (partition_size < *p_size) {
				*p_size = partition_size;
				DBG_ERR("ai size(0x%X) > partition size (0x%X)\r\n", *p_size, partition_size);
				return -1;
			}
		}
		return 0;
	}

	DBG_DUMP("no ai\n");
	return 1; //no ai
}
#endif

#if CFG_LOAD_AUDIOIO
#if CFG_LOAD_SOUND
/*
 * The required PCM audio sound data should be prepared,
 * And copy it to the allocated buffer.
 */
static const
_ALIGNED(4)
UINT8 uiPowerOn_SoundKey[0x10000] = {0x00};
#endif
static INT32 fast_trig_audio_play_buf_cb(VOID *callback_info, VOID *user_data)
{
	printf("[fast_trig_audio_play_buf_cb] user_data = %d\r\n", (unsigned int)user_data);
	return 0;
}

static INT32 fast_trig_audio_reserve_buf(UINT32 phy_addr)
{
	return 0;
}

static INT32 fast_trig_audio_free_buf(UINT32 phy_addr)
{
	return 0;
}

static int fast_trig_audio(PAUDIO_OBJ aud_obj)
{
	KDRV_BUFFER_INFO     buf_info = {0};
	KDRV_CALLBACK_FUNC   buf_cb = {0};
	UINT32 eid = aud_obj->aud_eid;
	UINT32 user_data = aud_obj->play_num;
	BOOL trigger = 1;

	buf_info.addr_pa = aud_obj->buf_info.addr_pa;
	buf_info.addr_va = aud_obj->buf_info.addr_va;
	buf_info.size    = aud_obj->buf_info.size;
	buf_info.ddr_id  = trigger;

	buf_cb.callback    = aud_obj->buf_cb.callback;
	buf_cb.reserve_buf = aud_obj->buf_cb.reserve_buf;
	buf_cb.free_buf    = aud_obj->buf_cb.free_buf;

	if (trigger) {
		if (kdrv_audioio_trigger(eid, &buf_info, &buf_cb, (VOID*)&user_data) != 0) {
			DBG_ERR("kdrv_audioio_trigger fail\r\n");
			return FALSE;
		}
	}

	printf("[fast_trig_audio] pa = %d va = %d size= %d\r\n", (unsigned int)aud_obj->buf_info.addr_pa, (unsigned int)aud_obj->buf_info.addr_va, (unsigned int)aud_obj->buf_info.size);

	return TRUE;
}

static int fast_trig_audioout(void)
{
	int ret = 0;
	AUDIO_OBJ aud_out_obj = {0};
	UINT32 mempool_audioout = 0, memsize_audioout = 0;
	UINT32 buf_size;

	aud_out_obj.aud_eid = KDRV_DEV_ID(0, KDRV_AUDOUT_ENGINE0, 0);

	buf_size = 0x20000;
	mempool_audioout = (UINT32)malloc(buf_size);
	memsize_audioout = buf_size;

#if CFG_LOAD_SOUND
/*
 * The required PCM audio sound data should be prepared,
 * And copy it to the allocated buffer.
 */
	buf_size = (sizeof(uiPowerOn_SoundKey) > memsize_audioout)? memsize_audioout : sizeof(uiPowerOn_SoundKey);
	memcpy((void *)mempool_audioout, (void *)uiPowerOn_SoundKey, buf_size);
#endif

	if (mempool_audioout == 0) {
		DBG_ERR("mempool_audioout malloc fail\r\n");
		ret = -1;
	} else {
		aud_out_obj.buf_info.addr_pa   = mempool_audioout;
		aud_out_obj.buf_info.addr_va   = mempool_audioout;
		aud_out_obj.buf_info.size      = buf_size;
		aud_out_obj.buf_info.ddr_id    = 1;

		aud_out_obj.buf_cb.callback    = fast_trig_audio_play_buf_cb;
		aud_out_obj.buf_cb.reserve_buf = fast_trig_audio_reserve_buf;
		aud_out_obj.buf_cb.free_buf    = fast_trig_audio_free_buf;

		aud_out_obj.play_num = 1;

		ret = fast_trig_audio(&aud_out_obj);
	}

	return ret;
}

static int fast_open_audio(PAUDIO_OBJ aud_obj)
{
	int ret = 0;
	UINT32 aud_vol = aud_obj->aud_vol;
	UINT32 max_vol_lvl = 160;
	UINT32 aud_sr = aud_obj->aud_sr, aud_ch = aud_obj->aud_ch, aud_ch_num = aud_obj->ch_num;
	UINT32 eid = aud_obj->aud_eid;
	UINT32 param[3] = {0};

	//... open hardware engine
	ret = kdrv_audioio_open(0, KDRV_DEV_ID_ENGINE(eid));
	if (ret != 0) {
		DBG_ERR("kdrv_audioio_open fail = %d\r\n", ret);
		return ret;
	}

	//... setting samplerate
	param[0] = aud_sr;
	kdrv_audioio_set(eid, KDRV_AUDIOIO_GLOBAL_SAMPLE_RATE, (VOID*)&param[0]);

	//... setting channel
	param[0] = 0;
	kdrv_audioio_set(eid, KDRV_AUDIOIO_OUT_MONO_EXPAND, (VOID*)&param[0]);
	if (aud_ch == AUDIO_CH_STEREO) {
		param[0] = aud_ch_num;
		kdrv_audioio_set(eid, KDRV_AUDIOIO_OUT_CHANNEL_NUMBER, (VOID*)&param[0]);
	} else if (aud_ch == AUDIO_CH_LEFT){
		param[0] = KDRV_AUDIO_OUT_MONO_LEFT;
		kdrv_audioio_set(eid, KDRV_AUDIOIO_OUT_MONO_SEL, (VOID*)&param[0]);
	} else {
		param[0] = KDRV_AUDIO_OUT_MONO_RIGHT;
		kdrv_audioio_set(eid, KDRV_AUDIOIO_OUT_MONO_SEL, (VOID*)&param[0]);
	}

	//... setting volume
	if (((UINT32)aud_vol) > max_vol_lvl) {
		DBG_WRN("Vol too large=%d, scale down to 100\r\n", aud_vol);
		aud_vol = 100;  //fix for CID 42874
	}
	param[0] = aud_vol;
	kdrv_audioio_set(eid, KDRV_AUDIOIO_OUT_VOLUME, (VOID*)&param[0]);

	//... setting outpath
	param[0] = KDRV_AUDIO_OUT_PATH_LINEOUT;
	kdrv_audioio_set(eid, KDRV_AUDIOIO_OUT_PATH, (VOID*)&param[0]);

	printf("[fast_open_audio] eid = %d, vol = %d, sr = %d, ch = %d, ch_num = %d\r\n", (unsigned int)aud_obj->aud_eid, (unsigned int)aud_obj->aud_vol, (unsigned int)aud_obj->aud_sr, (unsigned int)aud_obj->aud_ch, (unsigned int)aud_obj->ch_num);

	return ret;
}

static int fast_open_audioout(void)
{
	AUDIO_OBJ aud_out_obj = {0};

	aud_out_obj.aud_eid = KDRV_DEV_ID(0, KDRV_AUDOUT_ENGINE0, 0);
	aud_out_obj.aud_sr = AUDIO_SR_16000;
	aud_out_obj.aud_ch = AUDIO_CH_STEREO;
	aud_out_obj.ch_num = 2;
	aud_out_obj.aud_vol = 100; // max_vol_lvl = 160

	return fast_open_audio(&aud_out_obj);
}
#endif

// thread to setup sensor
static void *thread_sensor(void *ptr)
{
	//quick sensor setup flow
	fastboot_wait_done(BOOT_INIT_SENSOR);
	vos_perf_list_mark("ss", __LINE__, 0);

#if FUNC_TGE_SYNC
    kdrv_tge_init();        // tge engine
#endif

	if (is_fastboot()) {
		if (m_turn_on_sensor & TURN_ON_SENSOR1) {
			fast_open_sensor();
		}
		if (m_turn_on_sensor & TURN_ON_SENSOR2) {
			fast_open_sensor2();
		}
	}
	vos_perf_list_mark("ss", __LINE__, 1);

	pthread_exit((void *)0);
	return NULL;
}

// thread to setup panel
static void *thread_panel(void *ptr)
{
#if !defined(_disp_off_)
	//quick sensor setup flow
	fastboot_wait_done(BOOT_INIT_OTHERS);
	vos_perf_list_mark("ps", __LINE__, 0);
	if (is_fastboot()) {
		fast_open_panel();
	}
	vos_perf_list_mark("ps", __LINE__, 1);
#endif
	pthread_exit((void *)0);
	return NULL;
}

// thread to setup aout
static void *thread_audioout(void *ptr)
{
	//quick sensor setup flow
	//fastboot_wait_done(BOOT_INIT_SENSOR);
	fwload_wait_done(CODE_SECTION_05);
	vos_perf_list_mark("ao", __LINE__, 0);
	if (is_fastboot()) {
		#if (CFG_LOAD_AUDIOIO)
		fast_open_audioout();
		fast_trig_audioout();
		#endif
	}
	vos_perf_list_mark("ao", __LINE__, 1);

	pthread_exit((void *)0);

	return NULL;
}


#if (CFG_LOAD_AI_MODEL)
static int load_ai_model_nowait(LINUXBOOT_INFO *p_info)
{
	int er;
	UINT32 blksize = 0;
	unsigned int  read_buf = 0x0;
	unsigned int  read_size = 0x0;
	unsigned long long read_ofs = 0x0;

	er = get_ai_model(&read_buf, &read_size, &read_ofs);

	// check if ai exists in fdt
	if (er == 1) {
		return 0;
	}

	if (er != 0) {
		return er;
	}

	STORAGE_OBJ* pStrg = EMB_GETSTRGOBJ(STRG_OBJ_FW_ALL);
	if (pStrg == NULL) {
		DBG_ERR("pStrg is NULL.\n");
		return -1;
	}

	pStrg->GetParam(STRG_GET_BEST_ACCESS_SIZE, (UINT32)&blksize, 0);
	if (blksize == 0) {
		DBG_ERR("blksize is 0.\n");
		return -1;
	}

	unsigned int blkcnt = ALIGN_CEIL(read_size, blksize)/blksize;

#if 0 //calc check sum
	pStrg->Lock();
	pStrg->Open();
	pStrg->RdSectors((INT8 *)read_buf, (UINT32)(read_ofs/blksize), (UINT32)(blkcnt));
	pStrg->Close();
	pStrg->Unlock();
	DBGD(MemCheck_CalcCheckSum16Bit((UINT32)read_buf, (UINT32)read_size));
	memset((INT8 *)read_buf, 0, read_size);
	vos_cpu_dcache_sync((VOS_ADDR)read_buf, read_size, VOS_DMA_TO_DEVICE);
#endif

#if defined(_EMBMEM_SPI_NOR_)
	if (is_fastboot()) {
		//notify linux-spinor in preload mode
		linuxboot_set_flash_preload(p_info);

		//uiSectorAddr and uiSectorSize are bytealignment but rather block number and block count
		NOR_READ_NONBLK_CFG nor_cfg = {0};
		nor_cfg.uiSectorAddr = (UINT32)(read_ofs);
		nor_cfg.uiSectorSize = blkcnt*blksize;
		nor_cfg.pBuf = (UINT8*)read_buf;

		pStrg->Lock();
		pStrg->Open();
		if (pStrg->ExtIOCtrl(STRG_EXT_NOR_READ_NON_BLOCKING, (UINT32)&nor_cfg, 0) != E_OK) {
			DBG_ERR("failed to ExtIOCtrl\n");
			return -1;
		}
		//pStrg->Close(); //DO NOT close spi-nor for non-blocking load ai-model for spi-nor
		pStrg->Unlock();
	} else {
		DBG_WRN("not in fastboot, using safe-load-ai.\n");
		er = storage_partition_read_part(PARTITION_PATH_AI, (unsigned char *)read_buf, 0, (int)(blksize*blkcnt));
		if (er != 0) {
			return er;
		}
	}
#elif defined(_EMBMEM_EMMC_)
	if (is_fastboot()) {
		//notify linux-spinor in preload mode
		linuxboot_set_flash_preload(p_info);

		//uiSectorAddr and uiSectorSize are bytealignment but rather block number and block count
		SDIO_READ_NONBLK_CFG sdio_cfg = {0};
		sdio_cfg.uiSectorAddr = (UINT32)(read_ofs);
		sdio_cfg.uiSectorSize = blkcnt*blksize;
		sdio_cfg.pBuf = (UINT8*)read_buf;

		pStrg->Lock();
		pStrg->Open();
		if (pStrg->ExtIOCtrl(STRG_EXT_SDIO_READ_NON_BLOCKING, (UINT32)&sdio_cfg, 0) != E_OK) {
			DBG_ERR("failed to ExtIOCtrl\n");
			return -1;
		}
		//pStrg->Close(); //DO NOT close emmc for non-blocking load ai-model for emmc
		pStrg->Unlock();
	} else {
		DBG_WRN("not in fastboot, using safe-load-ai.\n");
		er = storage_partition_read_part(PARTITION_PATH_AI, (unsigned char *)read_buf, 0, (int)(blksize*blkcnt));
		if (er != 0) {
			return er;
		}
	}
#else
	DBG_WRN("spi-nand, using safe-load-ai.\n");
	er = storage_partition_read_part(PARTITION_PATH_AI, (unsigned char *)read_buf, 0, (int)(blksize*blkcnt));
	if (er != 0) {
		return er;
	}
#endif
	return 0;
}
#endif

// thread to start linux
static void *thread_linux(void *ptr)
{
	LINUXBOOT_INFO info = {0};
	// wait whole rtos loaded from flash finished
	fwload_wait_done(CODE_SECTION_10);
	// wait storage_partition_init2 finished
	fastboot_wait_done(BOOT_INIT_OTHERS);

	vos_perf_list_mark("linux", __LINE__, 0);
	if (linuxboot_setup(&info) != 0) {
		pthread_exit((void *)-1);
		return NULL;
	}
	vos_perf_list_mark("linux", __LINE__, 1);

	// wait sensor thread
	int join_ret;
	int pthread_ret;
	pthread_ret = pthread_join(handle_sensor, (void *)&join_ret);
	if (0 != pthread_ret) {
		DBG_ERR("handle_sensor pthread_join failed, ret %d\r\n", pthread_ret);
		pthread_exit((void *)-1);
		return NULL;
	}
	// wait panel thread
	pthread_ret = pthread_join(handle_panel, (void *)&join_ret);
	if (0 != pthread_ret) {
		DBG_ERR("handle_panel pthread_join failed, ret %d\r\n", pthread_ret);
		pthread_exit((void *)-1);
		return NULL;
	}

	// wait audioout thread
	pthread_ret = pthread_join(handle_audioout, (void *)&join_ret);
	if (0 != pthread_ret) {
		DBG_ERR("handle_audioout pthread_join failed, ret %d\r\n", pthread_ret);
		pthread_exit((void *)-1);
		return NULL;
	}

	// plan the bridge memory must be after hdal is ready
	if (bridge_mem_plan() != 0) {
		return NULL;
	}

#if (POWERON_FAST_BOOT_MSG == DISABLE)
	fastboot_msg_en(ENABLE);
#endif

#if (POWERON_BOOT_REPORT)
	vos_perf_list_dump();
#endif

#if (CFG_LOAD_AI_MODEL)
	load_ai_model_nowait(&info);
#endif

	linuxboot_go(&info); //never returned
	return NULL;
}

// main flow
int flow_boot_linux(void)
{
	int pthread_ret;
	int join_ret;
	int policy;
	struct sched_param schedparam = {0};

	// setup bridge mem description
	if ( 0 != bridge_mem_init()) {
		return -1;
	}

	// thread for sensor setup
	pthread_ret = pthread_create(&handle_sensor, NULL, thread_sensor , NULL);
	if (0 != pthread_ret) {
		DBG_ERR("create thread_sensor failed, ret %d\r\n", pthread_ret);
		return -1;
	}
	if (0 != pthread_getschedparam(handle_sensor, &policy, &schedparam)) {
			DBG_ERR("pthread_getschedparam failed\r\n");
	} else {
		schedparam.sched_priority = 20;
		pthread_setschedparam(handle_sensor, policy, &schedparam);
	}

	// thread for sensor setup
	pthread_ret = pthread_create(&handle_panel, NULL, thread_panel , NULL);
	if (0 != pthread_ret) {
		DBG_ERR("create thread_panel failed, ret %d\r\n", pthread_ret);
		return -1;
	}
	if (0 != pthread_getschedparam(handle_panel, &policy, &schedparam)) {
			DBG_ERR("pthread_getschedparam failed\r\n");
	} else {
		schedparam.sched_priority = 20;
		pthread_setschedparam(handle_panel, policy, &schedparam);
	}

	// thread for booting linux
	pthread_ret = pthread_create(&handle_linux, NULL, thread_linux , NULL);
	if (0 != pthread_ret) {
		DBG_ERR("create thread_cap_proc failed, ret %d\r\n", pthread_ret);
		return -1;
	}
	if (0 != pthread_getschedparam(handle_linux, &policy, &schedparam)) {
			DBG_ERR("pthread_getschedparam failed\r\n");
	} else {
		schedparam.sched_priority = 10;
		pthread_setschedparam(handle_linux, policy, &schedparam);
	}

	// thread for audioout setup
	pthread_ret = pthread_create(&handle_audioout, NULL, thread_audioout , NULL);
	if (0 != pthread_ret) {
		DBG_ERR("create thread_audioout failed, ret %d\r\n", pthread_ret);
		return -1;
	}
	if (0 != pthread_getschedparam(handle_audioout, &policy, &schedparam)) {
			DBG_ERR("pthread_getschedparam failed\r\n");
	} else {
		schedparam.sched_priority = 8;
		pthread_setschedparam(handle_audioout, policy, &schedparam);
	}

	// blocking here until linux booting
	pthread_ret = pthread_join(handle_linux, (void *)&join_ret);
	if (0 != pthread_ret) {
		DBG_ERR("handle_linux pthread_join failed, ret %d\r\n", pthread_ret);
		return -1;
	}

	return 0;
}
