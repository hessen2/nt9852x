#ifndef _PRJCFG_H_
#define _PRJCFG_H_

#ifndef ENABLE
#define ENABLE 1
#define DISABLE 0
#endif

//==============================================================================
//   SYSTEM FUNCTIONS
//==============================================================================
//..............................................................................
// boot
#define POWERON_FAST_BOOT      ENABLE    ///< enable to use multi-thread init
#define POWERON_FAST_BOOT_MSG  ENABLE   ///< disable boot msg for fast boot, but hard to debug
#define POWERON_BOOT_REPORT    ENABLE    ///< enable to show report after booting
#define POWERON_EXAM_FLASH_LOAD DISABLE  ///< exam load 16MB data from flash

// seletc boot mode
#define POWERON_MODE_NONE 0
#define POWERON_MODE_PREVIEW 1    ///< fast preview
#define POWERON_MODE_ENCODE 2     ///< fast encode
#define POWERON_MODE_BOOT_LINUX 3 ///< fast boot linux (only for cfg_RTOS_BOOT_LINUX_EVB)
#if defined(_MODEL_RTOS_BOOT_LINUX_EVB_) \
	|| defined(_MODEL_RTOS_BOOT_LINUX_NOR_EVB_) \
	|| defined(_MODEL_RTOS_BOOT_LINUX_EMMC_EVB_) \
	|| defined(_MODEL_528_RTOS_BOOT_LINUX_EVB_) \
	|| defined(_MODEL_528_RTOS_BOOT_LINUX_NOR_EVB_) \
	|| defined(_MODEL_528_RTOS_BOOT_LINUX_EMMC_EVB_) \
	|| defined(_MODEL_RTOS_BOOT_LINUX_AI_EVB_) \
	|| defined(_MODEL_RTOS_BOOT_LINUX_AI_NOR_EVB_) \
	|| defined(_MODEL_RTOS_BOOT_LINUX_AI_EMMC_EVB_) \
	|| defined(_BOOT_OS_RTOS_BOOT_LINUX_)
#define POWERON_MODE POWERON_MODE_BOOT_LINUX
#else
#define POWERON_MODE POWERON_MODE_PREVIEW
#endif

// share info memory on fdt
#define SHMEM_PATH "/nvt_memory_cfg/shmem"

// flash storage mapping
#define STRG_OBJ_FW_FDT   STRG_OBJ_FW_RSV1
#define STRG_OBJ_FW_APP   STRG_OBJ_FW_RSV2
#define STRG_OBJ_FW_UBOOT STRG_OBJ_FW_RSV3
#define STRG_OBJ_FW_RTOS  STRG_OBJ_FW_RSV4
#define STRG_OBJ_FW_LINUX STRG_OBJ_FW_RSV5
#define STRG_OBJ_FW_ROOTFS STRG_OBJ_FW_RSV6
#define STRG_OBJ_FW_ALL   STRG_OBJ_FW_RSV7

#endif
