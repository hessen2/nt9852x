#include "vos_version.h"

//init function is empty (FreeRTOS)
//set version here to compare with Linux version

VOS_MODULE_VERSION(nvt_vos, VOS_VER_MAJOR, VOS_VER_MINOR, VOS_VER_BUGFIX, VOS_VER_EXT);
