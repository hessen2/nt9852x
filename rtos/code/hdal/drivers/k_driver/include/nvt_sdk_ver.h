/**
 * @file nvt_sdk_ver.h
 * @brief type definition of SDK release version
 * @date 20220828
 * Copyright Novatek Microelectronics Corp. 2022.  All rights reserved.
 */

#define NVT_SDK_VER 0x205009  // "/proc/SDK/version" should be 2.05.009
