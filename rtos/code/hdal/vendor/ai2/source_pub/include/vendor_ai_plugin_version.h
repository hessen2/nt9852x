/**
	@brief Vendor ai plugin implementation version.

	@file vendor_ai_plugin_version.h

	@ingroup vendor_ai_plugin

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2019.  All rights reserved.
*/
#ifndef _VENDOR_AI_PLUGIN_VERSION_H_
#define _VENDOR_AI_PLUGIN_VERSION_H_

#define VENDOR_AI_PLUGIN_IMPL_VERSION      "01.02.2211290" //implementation version

#endif
