/**
    Source file for dal_ai

    This file is the source file that implements the API for vendor_preproc.

    @file       vendor_preproc.c
    @ingroup    vendor_preproc

    Copyright   Novatek Microelectronics Corp. 2018.    All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Include Files                                                               */
/*-----------------------------------------------------------------------------*/

#include "kwrap/error_no.h"
#include "kwrap/type.h"
#include "net_util_sample.h"
#include "vendor_ai/vendor_ai.h"
#include "ai_ioctl.h"
#include "hdal.h"
#include "hd_type.h"
#include "vendor_preproc/vendor_preproc.h"
#include <string.h>

#if defined(__LINUX)
#include <sys/ioctl.h>
#include <sys/mman.h>
#endif
#include "kflow_common/nvtmpp.h"
#include "kflow_common/nvtmpp_ioctl.h"
#if defined (__UITRON) || defined(__ECOS)  || defined (__FREERTOS)

#define NVTPREPROC_OPEN(...) 0
#define NVTPREPROC_IOCTL nvtmpp_ioctl
#define NVTPREPROC_CLOSE(...)

#endif

#if defined(__LINUX)

#define NVTPREPROC_OPEN  open
#define NVTPREPROC_IOCTL ioctl
#define NVTPREPROC_CLOSE close

#endif


/*-----------------------------------------------------------------------------*/
/* Local Constant Definitions                                                  */
/*-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------*/
/* Local Types Declarations                                                    */
/*-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------*/
/* Extern Global Variables                                                     */
/*-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------*/
/* Extern Function Prototype                                                   */
/*-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------*/
/* Local Function Prototype                                                    */
/*-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------*/
/* Debug Variables & Functions                                                 */
/*-----------------------------------------------------------------------------*/
#define PROF                DISABLE
#if PROF
	static struct timeval tstart, tend;
	#define PROF_START()    gettimeofday(&tstart, NULL);
	#define PROF_END(msg)   gettimeofday(&tend, NULL);  \
			printf("%s time (us): %lu\r\n", msg,         \
					(tend.tv_sec - tstart.tv_sec) * 1000000 + (tend.tv_usec - tstart.tv_usec));
#else
	#define PROF_START()
	#define PROF_END(msg)
#endif


/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
static INT32 ai_fd = -1;
static UINT32 g_preproc_flag = 0;
static UINT32 g_preproc_cnt  = 0;

/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/


static INT32 vendor_preproc_run_nue2(KDRV_AI_PREPROC_PARM *p_param)
{
	int ret = 0;
	
	if (p_param == NULL) {
		DBG_ERR("[VENDOR_PREPROC] p_param is null\n");
		return -1;
	}

	ret = NVTPREPROC_IOCTL(ai_fd, NUE2_IOC_RUN, p_param);
	if (ret < 0) {
		DBG_ERR("[VENDOR_PREPROC] run nue2 process fail!\n");
		return ret;
	}
	
	return ret;
}
static INT32 vendor_preproc_run_nue2_v2(KDRV_AI_PREPROC_INFO* p_preproc_mem)
{
	int ret = 0;
	
	if (p_preproc_mem == NULL) {
		DBG_ERR("vendor_preproc_run_nue2_v2: p_preproc_mem is null\n");
		return -1;
	}

	ret = NVTPREPROC_IOCTL(ai_fd, NUE2_IOC_RUN2, p_preproc_mem);
	if (ret < 0) {
		DBG_ERR("vendor_preproc_run_nue2_v2: run nue2 process fail!\n");
		return ret;
	}
	
	return ret;
}
static INT32 vendor_preproc_parse_nue2_param(AI_PREPROC_PARM *p_pre_param, KDRV_AI_PREPROC_PARM *p_nue2_param)
{
	int ret = 0;
	UINT32 func_idx = 0;
	UINT32 io_idx = 0;
	UINT32 i = 0;

	
	if (p_pre_param == NULL || p_nue2_param == NULL) {
		DBG_ERR("[VENDOR_PREPROC] input parameter is null\n");
		return -1;
	}
	
	// func list
	if (p_pre_param->func_en & AI_PREPROC_INPUT_CROP_EN) {
		// temporally not support...
	}
	if (p_pre_param->func_en & AI_PREPROC_TRANS_FMT_EN) {
		if (p_pre_param->trans_fmt_parm.mode == AI_PREPROC_YUV2RGB) {
			p_nue2_param->func_list[func_idx] = KDRV_AI_PREPROC_YUV2RGB_EN;
			func_idx++;
		} else {
			// temporally not support...
		}
	}
	if (p_pre_param->func_en & AI_PREPROC_SCALE_EN) {
		// set scale_ker
		if(p_pre_param->scale_parm.scl_out_size.width > p_pre_param->input[0].size.width
			|| p_pre_param->scale_parm.scl_out_size.height > p_pre_param->input[0].size.height){
			DBG_ERR("[VENDOR_PREPROC] Only support scale down.\n");
			return -1;
		}
		memcpy(&p_nue2_param->scale_ker.scl_out_size, &p_pre_param->scale_parm.scl_out_size, sizeof(KDRV_AI_SIZE));
		p_nue2_param->scale_ker.fact_update_en = 0;
	} else {
		p_nue2_param->scale_ker.scl_out_size.width  = p_pre_param->input[0].size.width;
		p_nue2_param->scale_ker.scl_out_size.height = p_pre_param->input[0].size.height;
		p_nue2_param->scale_ker.fact_update_en = 0;
	}
	
	if (p_pre_param->func_en & AI_PREPROC_SUB_EN) {
		p_nue2_param->func_list[func_idx] = KDRV_AI_PREPROC_SUB_EN;
		func_idx++;
		p_nue2_param->sub_ker.sub_mode = p_pre_param->sub_parm.mode;
		if (p_pre_param->sub_parm.mode == AI_PREPROC_SUB_SCALAR) {
			// dc mode
			p_nue2_param->sub_ker.sub_dc_coef[0] = p_pre_param->sub_parm.sub_val[0];
			p_nue2_param->sub_ker.sub_dc_coef[1] = p_pre_param->sub_parm.sub_val[1];
			p_nue2_param->sub_ker.sub_dc_coef[2] = p_pre_param->sub_parm.sub_val[2];
		} else {
			// planar mode
			p_nue2_param->sub_ker.sub_in_w = p_pre_param->sub_parm.sub_planar.size.width;
			p_nue2_param->sub_ker.sub_in_h = p_pre_param->sub_parm.sub_planar.size.height;
		}
		
		p_nue2_param->sub_ker.dup_rate = 0;
		p_nue2_param->sub_ker.sub_shf = 0;
	}
	if (p_pre_param->func_en & AI_PREPROC_SCL_CROP_EN) {
		// temporally not support...
	}
	if (p_pre_param->func_en & AI_PREPROC_PAD_EN) {
		// temporally not support...
		// set pad_ker
		p_nue2_param->pad_ker.crop_x     = p_pre_param->scl_crop_parm.crop_x;
		p_nue2_param->pad_ker.crop_y     = p_pre_param->scl_crop_parm.crop_y;
		p_nue2_param->pad_ker.crop_w     = p_pre_param->scl_crop_parm.crop_w;
		p_nue2_param->pad_ker.crop_h     = p_pre_param->scl_crop_parm.crop_h;
		p_nue2_param->pad_ker.pad_out_x  = p_pre_param->pad_parm.pad_out_x;
		p_nue2_param->pad_ker.pad_out_y  = p_pre_param->pad_parm.pad_out_y;
		p_nue2_param->pad_ker.pad_out_w  = p_pre_param->pad_parm.pad_out_w;
		p_nue2_param->pad_ker.pad_out_h  = p_pre_param->pad_parm.pad_out_h;
		p_nue2_param->pad_ker.pad_val[0] = p_pre_param->pad_parm.pad_val[0];
		p_nue2_param->pad_ker.pad_val[1] = p_pre_param->pad_parm.pad_val[1];
		p_nue2_param->pad_ker.pad_val[2] = p_pre_param->pad_parm.pad_val[2];
	}
	if (p_pre_param->func_en & AI_PREPROC_FLIP_EN) {
		// temporally not support...
	}
	if (p_pre_param->func_en & AI_PREPROC_ROTATE_EN) {
		// temporally not support...
	}
	
	/*
	if (func_idx == 0) {
		DBG_ERR("[VENDOR_PREPROC] no function enable!\n");
		return -1;
	}
	*/
	// set src fmt
	p_nue2_param->src_fmt = p_pre_param->src_fmt;
	
	// set rst fmt
	p_nue2_param->rst_fmt = p_pre_param->rst_fmt;
	
	// check format
	if (p_pre_param->func_en & AI_PREPROC_TRANS_FMT_EN) {
		if (p_pre_param->trans_fmt_parm.mode == AI_PREPROC_YUV2RGB 
		&& (p_pre_param->src_fmt != AI_PREPROC_FMT_YUV420 || p_pre_param->rst_fmt != AI_PREPROC_FMT_RGB)) {
			DBG_ERR("[VENDOR_PREPROC] if YUV2RGB is enable, the input/output format must be YUV/RGB!\n");
			return -1;
		}
	} else {
		if (p_nue2_param->src_fmt != p_nue2_param->rst_fmt) {
			DBG_ERR("[VENDOR_PREPROC] if format transform is not enable, the source format must be same as result format!\n");
			return -1;
		}
	}
	
	// set in_type
	p_nue2_param->in_type = p_pre_param->input[0].type;
	
	// set out_type
	p_nue2_param->out_type = p_pre_param->output[0].type;
	
	// set in_size
	memcpy(&p_nue2_param->in_size, &p_pre_param->input[0].size, sizeof(KDRV_AI_SIZE));
	
	// set io info (input addr/output addr/in ofs/out ofs)
	for (i = 0; i < AI_PREPROC_MAX_IO_NUM; i++) {
		memcpy(&p_nue2_param->in_ofs[i], &p_pre_param->input[i].ofs, sizeof(AI_PREPROC_OFS));
		memcpy(&p_nue2_param->out_ofs[i], &p_pre_param->output[i].ofs, sizeof(AI_PREPROC_OFS));
		p_nue2_param->in_addr[i] = p_pre_param->input[i].pa;
		p_nue2_param->out_addr[i] = p_pre_param->output[i].pa;
		if (i == 2 && (p_pre_param->func_en & AI_PREPROC_SUB_EN) && p_pre_param->sub_parm.mode == AI_PREPROC_SUB_PLANAR) {
			// for submean planar mode, in addr 2 must be sub planar
			p_nue2_param->in_addr[i] = p_pre_param->sub_parm.sub_planar.pa;
			p_nue2_param->in_ofs[i].line_ofs = p_pre_param->sub_parm.sub_planar.ofs.line_ofs;
		}
	}

	// check io info
	for (io_idx = 0; io_idx < 2; io_idx++) {
		UINT32 addr[AI_PREPROC_MAX_IO_NUM] = {0};
		UINT32 fmt = 0;
		
		for (i = 0; i < AI_PREPROC_MAX_IO_NUM; i++) {
			if (io_idx == 0) {
				// check input
				addr[i] = p_pre_param->input[i].pa;
				fmt = p_pre_param->src_fmt;
			} else {
				// check output
				addr[i] = p_pre_param->output[i].pa;
				fmt = p_pre_param->rst_fmt;
			}
		}
		 
		// check null address 
		if (addr[0] == 0) {
			DBG_ERR("[VENDOR_PREPROC] io %d channel 0 is NULL!\n", (int)io_idx);
			return -1;
		}
		
		if (fmt == AI_PREPROC_FMT_YUV420 || fmt == AI_PREPROC_FMT_RGB || fmt == AI_PREPROC_FMT_YUV420_NV21) {	
			if (addr[1] == 0) {
				DBG_ERR("[VENDOR_PREPROC] io %d channel 1 is NULL!\n", (int)io_idx);
				return -1;
			}
			
			if (fmt == AI_PREPROC_FMT_RGB) {
				if (addr[2] == 0) {
					DBG_ERR("[VENDOR_PREPROC] io %d input channel 2 is NULL!\n", (int)io_idx);
					return -1;
				}
			}
		}
	}

	return ret;
}

INT32 vendor_preproc_init_parm(AI_PREPROC_PARM *p_parm)
{
	int ret = 0;
	if (p_parm == NULL) {
		DBG_ERR("[VENDOR_PREPROC] input parameter is null\n");
		return -1;
	}
	memset((VOID*)p_parm, 0, sizeof(AI_PREPROC_PARM));
	
	return ret;
}

INT32 vendor_preproc_set_io_parm(AI_PREPROC_PARM *p_parm, VOID* p_input_parm, AI_PREPROC_FMT fmt, UINT32 io_type, AI_PREPROC_CH_FMT io_idx)
{
	int ret = 0;
	if (p_parm == NULL || p_input_parm == NULL) {
		DBG_ERR("[VENDOR_PREPROC] input parameter is null\n");
		return -1;
	}
	
	if (io_idx >= AI_PREPROC_MAX_IO_NUM) {
		DBG_ERR("[VENDOR_PREPROC] io_idx is invalid! max = %d\n", AI_PREPROC_MAX_IO_NUM - 1);
		return -1;
	}
	
	if (io_type == 0) {
		p_parm->src_fmt = fmt;
		memcpy(&p_parm->input[io_idx], p_input_parm, sizeof(AI_PREPROC_IO_INFO));
	} else {
		p_parm->rst_fmt = fmt;
		memcpy(&p_parm->output[io_idx], p_input_parm, sizeof(AI_PREPROC_IO_INFO));
	}
	
	
	return ret;
}

INT32 vendor_preproc_set_func_parm(AI_PREPROC_PARM *p_parm, VOID* p_input_parm, UINT32 func_type)
{
	int ret = 0;
	
	if (p_parm == NULL || p_input_parm == NULL) {
		DBG_ERR("[VENDOR_PREPROC] input parameter is null\n");
		return -1;
	}
	
	switch (func_type) {
	case AI_PREPROC_INPUT_CROP_EN: {
		AI_PREPROC_CROP_PARM* p_func_parm = (AI_PREPROC_CROP_PARM*)p_input_parm;
		memcpy(&p_parm->input_crop_parm, p_func_parm, sizeof(AI_PREPROC_CROP_PARM));
		p_parm->func_en |= func_type;
		}
		break;
	case AI_PREPROC_TRANS_FMT_EN: {
		AI_PREPROC_TRANS_FMT_PARM* p_func_parm = (AI_PREPROC_TRANS_FMT_PARM*)p_input_parm;
		memcpy(&p_parm->trans_fmt_parm, p_func_parm, sizeof(AI_PREPROC_TRANS_FMT_PARM));
		p_parm->func_en |= func_type;
		}
		break;
	case AI_PREPROC_SCALE_EN: {
		AI_PREPROC_SCALE_PARM* p_func_parm = (AI_PREPROC_SCALE_PARM*)p_input_parm;
		memcpy(&p_parm->scale_parm, p_func_parm, sizeof(AI_PREPROC_SCALE_PARM));
		p_parm->func_en |= func_type;
		}
		break;
	case AI_PREPROC_SUB_EN: {
		AI_PREPROC_SUB_PARM* p_func_parm = (AI_PREPROC_SUB_PARM*)p_input_parm;
		memcpy(&p_parm->sub_parm, p_func_parm, sizeof(AI_PREPROC_SUB_PARM));
		p_parm->func_en |= func_type;
		}
		break;
	case AI_PREPROC_SCL_CROP_EN: {
		AI_PREPROC_CROP_PARM* p_func_parm = (AI_PREPROC_CROP_PARM*)p_input_parm;
		memcpy(&p_parm->scl_crop_parm, p_func_parm, sizeof(AI_PREPROC_CROP_PARM));
		p_parm->func_en |= func_type;
		}
		break;
	case AI_PREPROC_PAD_EN: {
		AI_PREPROC_PAD_PARM* p_func_parm = (AI_PREPROC_PAD_PARM*)p_input_parm;
		memcpy(&p_parm->pad_parm, p_func_parm, sizeof(AI_PREPROC_PAD_PARM));
		p_parm->func_en |= func_type;
		}
		break;
	case AI_PREPROC_FLIP_EN: {
		AI_PREPROC_FLIP_PARM* p_func_parm = (AI_PREPROC_FLIP_PARM*)p_input_parm;
		memcpy(&p_parm->flip_parm, p_func_parm, sizeof(AI_PREPROC_FLIP_PARM));
		p_parm->func_en |= func_type;
		}
		break;
	case AI_PREPROC_ROTATE_EN: {
		AI_PREPROC_ROTATE_PARM* p_func_parm = (AI_PREPROC_ROTATE_PARM*)p_input_parm;
		memcpy(&p_parm->rotate_parm, p_func_parm, sizeof(AI_PREPROC_ROTATE_PARM));
		p_parm->func_en |= func_type;
		}
		break;
	default:
		DBG_ERR("[VENDOR_PREPROC] unknown function 0x%08X!\n", (unsigned int)func_type);
		ret = -1;
		break;
	}
	
	return ret;
}

INT32 vendor_preproc_run(AI_PREPROC_PARM *p_param, UINT32* run_flg)
{
	int ret = 0;
	KDRV_AI_PREPROC_PARM nue2_param = {0};
	
	if (p_param == NULL) {
		DBG_ERR("[VENDOR_PREPROC] p_param is null\n");
		return -1;
	}
	
	// parse parameters
	memset(&nue2_param, 0, sizeof(KDRV_AI_PREPROC_PARM));
	ret = vendor_preproc_parse_nue2_param(p_param, &nue2_param);
	if (ret < 0) {
		return ret;
	}
	
	// run engine
	ret = vendor_preproc_run_nue2(&nue2_param);
	if (ret < 0) {
		return ret;
	}
	
	if (run_flg != NULL) {
		*run_flg = AI_PREPROC_ENG_NUE2;
	} else {
		vendor_preproc_chk_done(AI_PREPROC_ENG_NUE2);
	}
	
	return ret;
}

INT32 vendor_preproc_chk_done(AI_PREPROC_ENG eng_type)
{
	int ret = 0;
	
	if (eng_type == AI_PREPROC_ENG_NUE2) {
		ret = NVTPREPROC_IOCTL(ai_fd, NUE2_IOC_DONE, NULL);
		if (ret < 0) {
			DBG_ERR("[VENDOR_PREPROC] check nue2 process fail!\n");
			return ret;
		}
	} else {
		DBG_ERR("[VENDOR_PREPROC] unknown engine type\n");
		ret = -1;
	}
	
	return ret;
}


INT32 vendor_preproc_init(VOID)
{
	int ret = 0;
	
	for(;;) {
        if(g_preproc_flag != 0) {
            // wait
			usleep(50);
        } else {
            __sync_fetch_and_add(&g_preproc_flag, 1);
            break;
        }
    }
	
	if(g_preproc_cnt == 0) {
        ai_fd = NVTPREPROC_OPEN("/dev/nvt_ai_module0", O_RDWR);
        if (ai_fd < 0) {
            DBG_ERR("[VENDOR_PREPROC] Open kdrv_ai fail!\n");
            return HD_ERR_NG;
        }
        ret = NVTPREPROC_IOCTL(ai_fd, NUE2_IOC_INIT, NULL);
        if (ret < 0) {
            DBG_ERR("[VENDOR_PREPROC] ai init nue2 fail!\n");
            return ret;
        }
    }
    g_preproc_cnt++;

    __sync_fetch_and_sub(&g_preproc_flag, 1);
	
	return ret;
}

INT32 vendor_preproc_uninit(VOID)
{
	int ret = 0;
	
	for(;;) {
        if(g_preproc_flag != 0) {
            // wait
			usleep(50);
        } else {
            __sync_fetch_and_add(&g_preproc_flag, 1);
            break;
        }
    }

    g_preproc_cnt--;
    if(g_preproc_cnt == 0) {
        ret = NVTPREPROC_IOCTL(ai_fd, NUE2_IOC_UNINIT, NULL);
        if (ret < 0) {
            DBG_ERR("[VENDOR_PREPROC] ai uninit nue2 fail!\n");
            return ret;
        }
        if (ai_fd != -1) {
            NVTPREPROC_CLOSE(ai_fd);
        }
    }

    __sync_fetch_and_sub(&g_preproc_flag, 1);
	
	return ret;
}

INT32 vendor_preproc_run_v2(KDRV_AI_PREPROC_INFO* p_preproc_mem, AI_PREPROC_PARM *p_param, UINT32* run_flg)
{
	int ret = 0;
	KDRV_AI_PREPROC_PARM* p_nue2_param = NULL;
	
	if (p_param == NULL || p_preproc_mem == NULL) {
		DBG_ERR("vendor_preproc_run2: input parameter is null\n");
		return -1;
	}
	
	if (p_preproc_mem->input_parm_pa == 0 || p_preproc_mem->input_parm_va == 0 ||
		p_preproc_mem->eng_parm_pa == 0 || p_preproc_mem->eng_parm_va == 0) {
		DBG_ERR("vendor_preproc_run2: input parameter address is 0\n");
		return -1;
	}
	
	// parse parameters
	p_nue2_param = (KDRV_AI_PREPROC_PARM*)p_preproc_mem->input_parm_va;
	memset(p_nue2_param, 0, sizeof(KDRV_AI_PREPROC_PARM));
	ret = vendor_preproc_parse_nue2_param(p_param, p_nue2_param);
	if (ret < 0) {
		DBG_ERR("vendor_preproc_run2: parse parameter fail\n");
		return ret;
	}
	
	// run engine
	ret = vendor_preproc_run_nue2_v2(p_preproc_mem);
	if (ret < 0) {
		DBG_ERR("vendor_preproc_run2: run engine fail\n");
		return ret;
	}
	
	if (run_flg != NULL) {
		*run_flg = AI_PREPROC_ENG_NUE2;
	} else {
		vendor_preproc_chk_done(AI_PREPROC_ENG_NUE2);
	}
	
	return ret;
}

INT32 vendor_preproc_auto_alloc_mem(UINT32 buf_pa, UINT32 buf_va, KDRV_AI_PREPROC_INFO* p_preproc_mem)
{
	UINT32 eng_parm_size = 0;
	INT32 ret = 0;
	
	if (buf_pa == 0 || buf_va == 0) {
		DBG_ERR("vendor_preproc_auto_alloc_mem: buf_pa/va is null\n");
		return -1;
	}
	
	if (p_preproc_mem == NULL) {
		DBG_ERR("vendor_preproc_auto_alloc_mem: p_preproc_mem is null\n");
		return -1;
	}
	
	ret = NVTPREPROC_IOCTL(ai_fd, NUE2_IOC_GET_PARM_SIZE, &eng_parm_size);
	if (ret < 0) {
		DBG_ERR("vendor_preproc_auto_alloc_mem: get parm size fail!\n");
		return ret;
	}
	
	// alloc mem
	p_preproc_mem->input_parm_pa = buf_pa;
	p_preproc_mem->input_parm_va = buf_va;
	p_preproc_mem->input_parm_size = sizeof(KDRV_AI_PREPROC_PARM);
	p_preproc_mem->eng_parm_pa = buf_pa + p_preproc_mem->input_parm_size;
	p_preproc_mem->eng_parm_va = buf_va + p_preproc_mem->input_parm_size;
	p_preproc_mem->eng_parm_size = eng_parm_size;
	
	return ret;
}

UINT32 vendor_preproc_get_mem_size(VOID)
{
	INT32 ret = 0;
	UINT32 eng_parm_size = 0;
	
	ret = NVTPREPROC_IOCTL(ai_fd, NUE2_IOC_GET_PARM_SIZE, &eng_parm_size);
	if (ret < 0) {
		DBG_ERR("vendor_preproc_get_mem_size: get parm size fail!\n");
		return ret;
	}
	
	return (eng_parm_size + sizeof(KDRV_AI_PREPROC_PARM));
}