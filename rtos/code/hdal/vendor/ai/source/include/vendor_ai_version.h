/**
	@brief Vendor ai implementation version.

	@file vendor_ai_version.h

	@ingroup vendor_ai

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2019.  All rights reserved.
*/
#ifndef _VENDOR_AI_VERSION_H_
#define _VENDOR_AI_VERSION_H_


#define VENDOR_AI_IMPL_VERSION      "02.19.2201270" //implementation version

#endif
