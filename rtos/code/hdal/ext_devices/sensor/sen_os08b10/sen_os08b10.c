#if defined(__KERNEL__)
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <kdrv_builtin/kdrv_builtin.h>
#include "isp_builtin.h"
#else
#include "plat/gpio.h"
#endif
#include "kwrap/error_no.h"
#include "kwrap/type.h"
#include "kwrap/util.h"
#include <kwrap/verinfo.h>
#include "kflow_videocapture/ctl_sen.h"
#include "isp_api.h"

#include "sen_cfg.h"
#include "sen_common.h"
#include "sen_inc.h"

//=============================================================================
//Module parameter : Set module parameters when insert the module
//=============================================================================
#if defined(__KERNEL__)
char *sen_cfg_path = "null";
module_param_named(sen_cfg_path, sen_cfg_path, charp, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(sen_cfg_path, "Path of cfg file");

#ifdef DEBUG
unsigned int sen_debug_level = THIS_DBGLVL;
module_param_named(sen_debug_level, sen_debug_level, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(sen_debug_level, "Debug message level");
#endif
#endif

//=============================================================================
// version
//=============================================================================
VOS_MODULE_VERSION(nvt_sen_os08b10, 1, 52, 000, 00);

//=============================================================================
// information
//=============================================================================
#define SEN_OS08B10_MODULE_NAME     "sen_os08b10"
#define SEN_MAX_MODE                 3
#define MAX_VD_PERIOD                0xFFFF
#define MAX_EXPOSURE_LINE            0xFFFF
#define MIN_EXPOSURE_LINE            8
#define NON_EXPOSURE_LINE            12
#define MIN_HDR_EXPOSURE_LINE        2
#define NON_HDR_LONG_EXPOSURE_LINE   8
#define NON_HDR_SHORT_EXPOSURE_LINE  2
#define HCG_ENABLE                   1

#define SEN_I2C_ADDR 0x6C >> 1
#define SEN_I2C_COMPATIBLE "nvt,sen_os08b10"

#include "sen_i2c.c"

//=============================================================================
// function declaration
//=============================================================================
static CTL_SEN_DRV_TAB *sen_get_drv_tab_os08b10(void);
static void sen_pwr_ctrl_os08b10(CTL_SEN_ID id, CTL_SEN_PWR_CTRL_FLAG flag, CTL_SEN_CLK_CB clk_cb);
static ER sen_open_os08b10(CTL_SEN_ID id);
static ER sen_close_os08b10(CTL_SEN_ID id);
static ER sen_sleep_os08b10(CTL_SEN_ID id);
static ER sen_wakeup_os08b10(CTL_SEN_ID id);
static ER sen_write_reg_os08b10(CTL_SEN_ID id, CTL_SEN_CMD *cmd);
static ER sen_read_reg_os08b10(CTL_SEN_ID id, CTL_SEN_CMD *cmd);
static ER sen_chg_mode_os08b10(CTL_SEN_ID id, CTL_SENDRV_CHGMODE_OBJ chgmode_obj);
static ER sen_chg_fps_os08b10(CTL_SEN_ID id, UINT32 fps);
static ER sen_set_info_os08b10(CTL_SEN_ID id, CTL_SENDRV_CFGID drv_cfg_id, void *data);
static ER sen_get_info_os08b10(CTL_SEN_ID id, CTL_SENDRV_CFGID drv_cfg_id, void *data);
static UINT32 sen_calc_chgmode_vd_os08b10(CTL_SEN_ID id, UINT32 fps);
static UINT32 sen_calc_exp_vd_os08b10(CTL_SEN_ID id, UINT32 fps);
static void sen_set_gain_os08b10(CTL_SEN_ID id, void *param);
static void sen_set_expt_os08b10(CTL_SEN_ID id, void *param);
static void sen_set_preset_os08b10(CTL_SEN_ID id, ISP_SENSOR_PRESET_CTRL *ctrl);
static void sen_set_flip_os08b10(CTL_SEN_ID id, CTL_SEN_FLIP *flip);
static ER sen_get_flip_os08b10(CTL_SEN_ID id, CTL_SEN_FLIP *flip);
#if defined(__FREERTOS)
void sen_get_gain_os08b10(CTL_SEN_ID id, void *param);
void sen_get_expt_os08b10(CTL_SEN_ID id, void *param);
#else
static void sen_get_gain_os08b10(CTL_SEN_ID id, void *param);
static void sen_get_expt_os08b10(CTL_SEN_ID id, void *param);
#endif
static void sen_get_min_expt_os08b10(CTL_SEN_ID id, void *param);
static void sen_get_mode_basic_os08b10(CTL_SENDRV_GET_MODE_BASIC_PARAM *mode_basic);
static void sen_get_attr_basic_os08b10(CTL_SENDRV_GET_ATTR_BASIC_PARAM *data);
static void sen_get_attr_signal_os08b10(CTL_SENDRV_GET_ATTR_SIGNAL_PARAM *data);
static ER sen_get_attr_cmdif_os08b10(CTL_SEN_ID id, CTL_SENDRV_GET_ATTR_CMDIF_PARAM *data);
static ER sen_get_attr_if_os08b10(CTL_SENDRV_GET_ATTR_IF_PARAM *data);
static void sen_get_fps_os08b10(CTL_SEN_ID id, CTL_SENDRV_GET_FPS_PARAM *data);
static void sen_get_speed_os08b10(CTL_SEN_ID id, CTL_SENDRV_GET_SPEED_PARAM *data);
static void sen_get_mode_mipi_os08b10(CTL_SENDRV_GET_MODE_MIPI_PARAM *data);
static void sen_get_modesel_os08b10(CTL_SENDRV_GET_MODESEL_PARAM *data);
static UINT32 sen_calc_rowtime_os08b10(CTL_SEN_ID id, CTL_SEN_MODE mode);
static UINT32 sen_calc_rowtime_step_os08b10(CTL_SEN_ID id, CTL_SEN_MODE mode);
static void sen_get_rowtime_os08b10(CTL_SEN_ID id, CTL_SENDRV_GET_MODE_ROWTIME_PARAM *data);
static void sen_set_cur_fps_os08b10(CTL_SEN_ID id, UINT32 fps);
static UINT32 sen_get_cur_fps_os08b10(CTL_SEN_ID id);
static void sen_set_chgmode_fps_os08b10(CTL_SEN_ID id, UINT32 fps);
static UINT32 sen_get_chgmode_fps_os08b10(CTL_SEN_ID id);

//=============================================================================
// global variable
//=============================================================================
static UINT32 sen_map = SEN_PATH_1;

static SEN_PRESET sen_preset[CTL_SEN_ID_MAX] = {
	{10000, 1000},
	{10000, 1000},
	{10000, 1000},
	{10000, 1000},
	{10000, 1000},
	{10000, 1000},
	{10000, 1000},
	{10000, 1000}
};

static SEN_DIRECTION sen_direction[CTL_SEN_ID_MAX] = {
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE}
};

static SEN_POWER sen_power[CTL_SEN_ID_MAX] = {
	//C_GPIO:+0x0; P_GPIO:+0x20; S_GPIO:+0x40; L_GPIO:0x60
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1}
};

static SEN_I2C sen_i2c[CTL_SEN_ID_MAX] = {
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_1, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR}
};

static CTL_SENDRV_GET_ATTR_BASIC_PARAM basic_param = {
	SEN_OS08B10_MODULE_NAME,
	CTL_SEN_VENDOR_OMNIVISION,
	SEN_MAX_MODE,
	CTL_SEN_SUPPORT_PROPERTY_MIRROR|CTL_SEN_SUPPORT_PROPERTY_FLIP|CTL_SEN_SUPPORT_PROPERTY_CHGFPS,
	0
};

static CTL_SENDRV_GET_ATTR_SIGNAL_PARAM signal_param = {
	CTL_SEN_SIGNAL_MASTER,
	{CTL_SEN_ACTIVE_HIGH, CTL_SEN_ACTIVE_HIGH, CTL_SEN_PHASE_RISING, CTL_SEN_PHASE_RISING, CTL_SEN_PHASE_RISING}
};

static CTL_SENDRV_I2C i2c = {
	{
		{CTL_SEN_I2C_W_ADDR_DFT,     0x6C},
		{CTL_SEN_I2C_W_ADDR_OPTION1, 0xFF},
		{CTL_SEN_I2C_W_ADDR_OPTION2, 0xFF},
		{CTL_SEN_I2C_W_ADDR_OPTION3, 0xFF},
		{CTL_SEN_I2C_W_ADDR_OPTION4, 0xFF},
		{CTL_SEN_I2C_W_ADDR_OPTION5, 0xFF}
	}
};

static CTL_SENDRV_GET_SPEED_PARAM speed_param[SEN_MAX_MODE] = {
	{
		CTL_SEN_MODE_1,
		CTL_SEN_SIEMCLK_SRC_DFT,
		24000000,
		48000000,
		236000000,
	},
	{
		CTL_SEN_MODE_2,
		CTL_SEN_SIEMCLK_SRC_DFT,
		24000000,
		96000000,
		283200000,
	},
	{
		CTL_SEN_MODE_3,
		CTL_SEN_SIEMCLK_SRC_DFT,
		24000000,
		48000000,
		236000000,
	}
};

static CTL_SENDRV_GET_MODE_MIPI_PARAM mipi_param[SEN_MAX_MODE] = {
	{
		CTL_SEN_MODE_1,
		CTL_SEN_CLKLANE_1,
		CTL_SEN_DATALANE_4,
		{ {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0} },
		0,
		{0, 0, 0, 0},
		SEN_BIT_OFS_NONE
	},
	{
		CTL_SEN_MODE_2,
		CTL_SEN_CLKLANE_1,
		CTL_SEN_DATALANE_4,
		{ {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0} },
		0,
		{1, 0, 0, 0},
		SEN_BIT_OFS_0|SEN_BIT_OFS_1
	},
	{
		CTL_SEN_MODE_3,
		CTL_SEN_CLKLANE_1,
		CTL_SEN_DATALANE_4,
		{ {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0} },
		0,
		{0, 0, 0, 0},
		SEN_BIT_OFS_NONE
	}
};

static CTL_SENDRV_GET_MODE_BASIC_PARAM mode_basic_param[SEN_MAX_MODE] = {
	{
		CTL_SEN_MODE_1,
		CTL_SEN_IF_TYPE_MIPI,
		CTL_SEN_DATA_FMT_RGB,
		CTL_SEN_MODE_LINEAR,
		3000,
		1,
		CTL_SEN_STPIX_R,
		CTL_SEN_PIXDEPTH_12BIT,
		CTL_SEN_FMT_POGRESSIVE,
		{3844, 2164},
		{{3, 3, 3840, 2160}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}},
		{3840, 2160},
		{0, 1080, 0, 1480},
		CTL_SEN_RATIO(16, 9),
		{1000, 992000},
		100
	},
	{
		CTL_SEN_MODE_2,
		CTL_SEN_IF_TYPE_MIPI,
		CTL_SEN_DATA_FMT_RGB,
		CTL_SEN_MODE_STAGGER_HDR,
		2500,
		2,
		CTL_SEN_STPIX_R,
		CTL_SEN_PIXDEPTH_10BIT,
		CTL_SEN_FMT_POGRESSIVE,
		{3844, 2164},
		{{3, 3, 3840, 2160}, {3, 3, 3840, 2160}, {0, 0, 0, 0}, {0, 0, 0, 0}},
		{3840, 2160},
		{0, 720, 0, 1332},
		CTL_SEN_RATIO(16, 9),
		{1000, 992000},
		100
	},
	{
		CTL_SEN_MODE_3,
		CTL_SEN_IF_TYPE_MIPI,
		CTL_SEN_DATA_FMT_RGB,
		CTL_SEN_MODE_LINEAR,
		2500,
		1,
		CTL_SEN_STPIX_R,
		CTL_SEN_PIXDEPTH_12BIT,
		CTL_SEN_FMT_POGRESSIVE,
		{3844, 2164},
		{{3, 3, 3840, 2160}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}},
		{3840, 2160},
		{0, 1296, 0, 1480},
		CTL_SEN_RATIO(16, 9),
		{1000, 992000},
		100
	}
};

static CTL_SEN_CMD os08b10_mode_1[] = {
	{0x0100, 1, {0x00, 0x0}},
	{0x0103, 1, {0x01, 0x0}},
	{SEN_CMD_DELAY, 1, { 5, 0x0}},	
	{0x0109, 1, {0x01, 0x0}},
	{0x0104, 1, {0x03, 0x0}},
	{0x0102, 1, {0x00, 0x0}},
	{0x0303, 1, {0x00, 0x0}},
	{0x0305, 1, {0x3b, 0x0}},
	{0x0306, 1, {0x03, 0x0}},
	{0x0307, 1, {0x00, 0x0}},
	{0x0308, 1, {0x04, 0x0}},
	{0x0309, 1, {0x02, 0x0}},
	{0x030a, 1, {0x01, 0x0}},
	{0x030c, 1, {0x00, 0x0}},
	{0x0317, 1, {0x4a, 0x0}},
	{0x0322, 1, {0x01, 0x0}},
	{0x0323, 1, {0x05, 0x0}},
	{0x0324, 1, {0x01, 0x0}},
	{0x0325, 1, {0x80, 0x0}},
	{0x0327, 1, {0x02, 0x0}},
	{0x0328, 1, {0x05, 0x0}},
	{0x0329, 1, {0x01, 0x0}},
	{0x032a, 1, {0x02, 0x0}},
	{0x032c, 1, {0x01, 0x0}},
	{0x032d, 1, {0x00, 0x0}},
	{0x032e, 1, {0x03, 0x0}},
	{0x032f, 1, {0x01, 0x0}},
	{0x3002, 1, {0x00, 0x0}},
	{0x300f, 1, {0x11, 0x0}},
	{0x3012, 1, {0x41, 0x0}},
	{0x301e, 1, {0xb8, 0x0}},
	{0x3026, 1, {0x12, 0x0}},
	{0x3027, 1, {0x08, 0x0}},
	{0x302c, 1, {0x1a, 0x0}},
	{0x3104, 1, {0x00, 0x0}},
	{0x3106, 1, {0x10, 0x0}},
	{0x3400, 1, {0x00, 0x0}},
	{0x3408, 1, {0x05, 0x0}},
	{0x3409, 1, {0x22, 0x0}},
	{0x340a, 1, {0x02, 0x0}},
	{0x340c, 1, {0x08, 0x0}},
	{0x340d, 1, {0xa4, 0x0}},
	{0x3420, 1, {0x00, 0x0}},
	{0x3421, 1, {0x00, 0x0}},
	{0x3422, 1, {0x00, 0x0}},
	{0x3423, 1, {0x00, 0x0}},
	{0x3424, 1, {0x00, 0x0}},
	{0x3425, 1, {0x00, 0x0}},
	{0x3426, 1, {0x55, 0x0}},
	{0x3427, 1, {0x15, 0x0}},
	{0x3428, 1, {0x55, 0x0}},
	{0x3429, 1, {0x15, 0x0}},
	{0x342a, 1, {0x50, 0x0}},
	{0x342b, 1, {0x00, 0x0}},
	{0x342c, 1, {0x01, 0x0}},
	{0x342d, 1, {0x54, 0x0}},
	{0x342e, 1, {0x01, 0x0}},
	{0x342f, 1, {0x04, 0x0}},
	{0x3501, 1, {0x01, 0x0}},
	{0x3502, 1, {0x40, 0x0}},
	{0x3503, 1, {0xa8, 0x0}},
	{0x3504, 1, {0x18, 0x0}}, 
	{0x3541, 1, {0x00, 0x0}},
	{0x3542, 1, {0x40, 0x0}},
	{0x3581, 1, {0x00, 0x0}},
	{0x3582, 1, {0x40, 0x0}},
	{0x3603, 1, {0x30, 0x0}},
	{0x3612, 1, {0x96, 0x0}},
	{0x3613, 1, {0xe0, 0x0}},
	{0x3623, 1, {0x00, 0x0}},
	{0x3624, 1, {0xff, 0x0}},
	{0x3625, 1, {0xff, 0x0}},
	{0x362a, 1, {0x0e, 0x0}},
	{0x362b, 1, {0x0e, 0x0}},
	{0x362c, 1, {0x0e, 0x0}},
	{0x362d, 1, {0x0e, 0x0}},
	{0x362e, 1, {0x17, 0x0}},
	{0x362f, 1, {0x2d, 0x0}},
	{0x3630, 1, {0x67, 0x0}},
	{0x3631, 1, {0x7f, 0x0}},
	{0x3638, 1, {0x00, 0x0}},
	{0x3639, 1, {0xff, 0x0}},
	{0x363a, 1, {0xff, 0x0}},
	{0x3643, 1, {0x0a, 0x0}},
	{0x3644, 1, {0x00, 0x0}},
	{0x3645, 1, {0x0a, 0x0}},
	{0x3646, 1, {0x0a, 0x0}},
	{0x3647, 1, {0x06, 0x0}},
	{0x3648, 1, {0x00, 0x0}},
	{0x3649, 1, {0x0a, 0x0}},
	{0x364a, 1, {0x0d, 0x0}},
	{0x364b, 1, {0x02, 0x0}},
	{0x364c, 1, {0x0e, 0x0}},
	{0x364d, 1, {0x0e, 0x0}},
	{0x364e, 1, {0x0e, 0x0}},
	{0x364f, 1, {0x0e, 0x0}},
	{0x3650, 1, {0xf7, 0x0}},
	{0x3651, 1, {0x77, 0x0}},
	{0x365a, 1, {0xbb, 0x0}},
	{0x365b, 1, {0x9d, 0x0}},
	{0x365c, 1, {0x8e, 0x0}},
	{0x365d, 1, {0x86, 0x0}},
	{0x3661, 1, {0x07, 0x0}},
	{0x3662, 1, {0x00, 0x0}},
	{0x3667, 1, {0xd4, 0x0}},
	{0x366f, 1, {0x00, 0x0}},
	{0x3671, 1, {0x88, 0x0}},
	{0x3673, 1, {0x2a, 0x0}},
	{0x3678, 1, {0x00, 0x0}},
	{0x3679, 1, {0x00, 0x0}},
	{0x3682, 1, {0xf1, 0x0}},
	{0x3687, 1, {0x02, 0x0}},
	{0x3688, 1, {0x81, 0x0}},
	{0x3689, 1, {0x02, 0x0}},
	{0x3692, 1, {0x0f, 0x0}},
	{0x3701, 1, {0x39, 0x0}},
	{0x3703, 1, {0x32, 0x0}},
	{0x3705, 1, {0x00, 0x0}},
	{0x3706, 1, {0x74, 0x0}},
	{0x3708, 1, {0x35, 0x0}},
	{0x370a, 1, {0x01, 0x0}},
	{0x370b, 1, {0x4b, 0x0}},
	{0x3712, 1, {0x13, 0x0}},
	{0x3714, 1, {0x02, 0x0}},
	{0x3715, 1, {0x00, 0x0}},
	{0x3716, 1, {0x00, 0x0}},
	{0x3719, 1, {0x23, 0x0}},
	{0x371c, 1, {0x00, 0x0}},
	{0x371d, 1, {0x08, 0x0}},
	{0x373b, 1, {0x50, 0x0}},
	{0x3755, 1, {0x01, 0x0}},
	{0x3756, 1, {0xea, 0x0}},
	{0x3757, 1, {0xea, 0x0}},
	{0x376b, 1, {0x02, 0x0}},
	{0x376c, 1, {0x02, 0x0}},
	{0x376d, 1, {0x04, 0x0}},
	{0x376e, 1, {0x04, 0x0}},
	{0x376f, 1, {0x22, 0x0}},
	{0x377a, 1, {0x02, 0x0}},
	{0x377b, 1, {0x01, 0x0}},
	{0x3785, 1, {0x08, 0x0}},
	{0x3790, 1, {0x10, 0x0}},
	{0x3792, 1, {0x00, 0x0}},
	{0x3796, 1, {0x00, 0x0}},
	{0x3797, 1, {0x00, 0x0}},
	{0x3798, 1, {0x00, 0x0}},
	{0x3799, 1, {0x00, 0x0}},
	{0x37bb, 1, {0x88, 0x0}},
	{0x37be, 1, {0x01, 0x0}},
	{0x37bf, 1, {0x01, 0x0}},
	{0x37c0, 1, {0x00, 0x0}},
	{0x37c7, 1, {0x51, 0x0}},
	{0x37c8, 1, {0x22, 0x0}},
	{0x37c9, 1, {0x00, 0x0}},
	{0x37ca, 1, {0xe4, 0x0}},
	{0x37cc, 1, {0x0e, 0x0}},
	{0x37cf, 1, {0x02, 0x0}},
	{0x37d0, 1, {0x00, 0x0}},
	{0x37d1, 1, {0x74, 0x0}},
	{0x37d2, 1, {0x01, 0x0}},
	{0x37d3, 1, {0x4b, 0x0}},
	{0x37d4, 1, {0x00, 0x0}},
	{0x37d5, 1, {0x74, 0x0}},
	{0x37d6, 1, {0x01, 0x0}},
	{0x37d7, 1, {0x4b, 0x0}},
	{0x3800, 1, {0x00, 0x0}},
	{0x3801, 1, {0x00, 0x0}},
	{0x3802, 1, {0x00, 0x0}},
	{0x3803, 1, {0x00, 0x0}},
	{0x3804, 1, {0x0f, 0x0}},
	{0x3805, 1, {0x1f, 0x0}},
	{0x3806, 1, {0x08, 0x0}},
	{0x3807, 1, {0x7f, 0x0}},
	{0x3808, 1, {0x0f, 0x0}},
	{0x3809, 1, {0x04, 0x0}},
	{0x380a, 1, {0x08, 0x0}},
	{0x380b, 1, {0x74, 0x0}},
	{0x380c, 1, {0x04, 0x0}},
	{0x380d, 1, {0x38, 0x0}},
	{0x380e, 1, {0x05, 0x0}},
	{0x380f, 1, {0xc8, 0x0}},
	{0x3811, 1, {0x0e, 0x0}},
	{0x3813, 1, {0x06, 0x0}},
	{0x3814, 1, {0x01, 0x0}},
	{0x3815, 1, {0x01, 0x0}},
	{0x3816, 1, {0x01, 0x0}},
	{0x3817, 1, {0x01, 0x0}},
	{0x381c, 1, {0x00, 0x0}},
	{0x3820, 1, {0x02, 0x0}},
	{0x3821, 1, {0x00, 0x0}},
	{0x3822, 1, {0x04, 0x0}},
	{0x3823, 1, {0x08, 0x0}},
	{0x382b, 1, {0x00, 0x0}},
	{0x382c, 1, {0x00, 0x0}},
	{0x3833, 1, {0x40, 0x0}},
	{0x383e, 1, {0x00, 0x0}},
	{0x384c, 1, {0x04, 0x0}},
	{0x384d, 1, {0x38, 0x0}},
	{0x3858, 1, {0x3c, 0x0}},
	{0x3850, 1, {0x00, 0x0}},
	{0x3851, 1, {0x00, 0x0}},
	{0x3852, 1, {0x00, 0x0}},
	{0x3853, 1, {0x00, 0x0}},
	{0x3865, 1, {0x02, 0x0}},
	{0x3866, 1, {0x00, 0x0}},
	{0x3867, 1, {0x02, 0x0}},
	{0x3868, 1, {0x00, 0x0}},
	{0x386a, 1, {0x05, 0x0}},
	{0x386b, 1, {0x00, 0x0}},
	{0x386c, 1, {0x05, 0x0}},
	{0x386d, 1, {0x61, 0x0}},
	{0x3881, 1, {0x02, 0x0}},
	{0x3882, 1, {0x00, 0x0}},
	{0x3883, 1, {0x08, 0x0}},
	{0x3884, 1, {0x01, 0x0}},
	{0x3885, 1, {0x00, 0x0}},
	{0x3886, 1, {0x00, 0x0}},
	{0x3887, 1, {0x20, 0x0}},
	{0x3888, 1, {0x40, 0x0}},
	{0x3c37, 1, {0x00, 0x0}},
	{0x3c49, 1, {0x00, 0x0}},
	{0x3c4a, 1, {0x9a, 0x0}},
	{0x3c4c, 1, {0x01, 0x0}},
	{0x3c4d, 1, {0x00, 0x0}},
	{0x3c4e, 1, {0x00, 0x0}},
	{0x3c50, 1, {0x00, 0x0}},
	{0x3c51, 1, {0x00, 0x0}},
	{0x3c52, 1, {0x00, 0x0}},
	{0x3c67, 1, {0x10, 0x0}},
	{0x3c69, 1, {0x10, 0x0}},
	{0x3c6a, 1, {0x00, 0x0}},
	{0x3c6b, 1, {0x62, 0x0}},
	{0x3c6c, 1, {0x00, 0x0}},
	{0x3c6d, 1, {0x62, 0x0}},
	{0x3c70, 1, {0x00, 0x0}},
	{0x3c71, 1, {0x04, 0x0}},
	{0x3c72, 1, {0x00, 0x0}},
	{0x3c73, 1, {0x62, 0x0}},
	{0x3c74, 1, {0x00, 0x0}},
	{0x3c75, 1, {0x62, 0x0}},
	{0x3c76, 1, {0x12, 0x0}},
	{0x3c77, 1, {0x12, 0x0}},
	{0x3c79, 1, {0x00, 0x0}},
	{0x3c7a, 1, {0x00, 0x0}},
	{0x3c7b, 1, {0x00, 0x0}},
	{0x3cb6, 1, {0x41, 0x0}},
	{0x3cb9, 1, {0x00, 0x0}},
	{0x3cc0, 1, {0x90, 0x0}},
	{0x3cc2, 1, {0x90, 0x0}},
	{0x3cc5, 1, {0x00, 0x0}},
	{0x3cc6, 1, {0x98, 0x0}},
	{0x3cc7, 1, {0x00, 0x0}},
	{0x3cc8, 1, {0x98, 0x0}},
	{0x3cc9, 1, {0x00, 0x0}},
	{0x3cca, 1, {0x98, 0x0}},
	{0x3ccc, 1, {0x01, 0x0}},
	{0x3cd9, 1, {0x7c, 0x0}},
	{0x3cda, 1, {0x7c, 0x0}},
	{0x3cdb, 1, {0x7c, 0x0}},
	{0x3cdc, 1, {0x7c, 0x0}},
	{0x3d8c, 1, {0x70, 0x0}},
	{0x3d8d, 1, {0x10, 0x0}},
	{0x4001, 1, {0x2b, 0x0}},
	{0x4004, 1, {0x00, 0x0}},
	{0x4005, 1, {0x40, 0x0}},
	{0x4008, 1, {0x02, 0x0}},
	{0x4009, 1, {0x0d, 0x0}},
	{0x400a, 1, {0x04, 0x0}},
	{0x400b, 1, {0xa8, 0x0}},
	{0x400e, 1, {0x40, 0x0}},
	{0x401a, 1, {0x50, 0x0}},
	{0x4028, 1, {0x6f, 0x0}},
	{0x4029, 1, {0x00, 0x0}},
	{0x402a, 1, {0x7f, 0x0}},
	{0x402b, 1, {0x01, 0x0}},
	{0x402e, 1, {0x00, 0x0}},
	{0x402f, 1, {0x40, 0x0}},
	{0x4030, 1, {0x00, 0x0}},
	{0x4031, 1, {0x40, 0x0}},
	{0x4032, 1, {0x9f, 0x0}},
	{0x4050, 1, {0x01, 0x0}},
	{0x4051, 1, {0x06, 0x0}},
	{0x405d, 1, {0x00, 0x0}},
	{0x405e, 1, {0x00, 0x0}},
	{0x4288, 1, {0xcf, 0x0}},
	{0x4289, 1, {0x00, 0x0}},
	{0x428a, 1, {0x40, 0x0}},
	{0x430b, 1, {0xff, 0x0}},
	{0x430c, 1, {0xff, 0x0}},
	{0x430d, 1, {0x00, 0x0}},
	{0x430e, 1, {0x00, 0x0}},
	{0x4314, 1, {0x04, 0x0}},
	{0x4500, 1, {0x1a, 0x0}},
	{0x4501, 1, {0x18, 0x0}},
	{0x4504, 1, {0x00, 0x0}},
	{0x4505, 1, {0x00, 0x0}},
	{0x4506, 1, {0x32, 0x0}},
	{0x4507, 1, {0x02, 0x0}},
	{0x4508, 1, {0x1a, 0x0}},
	{0x450a, 1, {0x12, 0x0}},
	{0x450b, 1, {0x28, 0x0}},
	{0x450c, 1, {0x00, 0x0}},
	{0x450f, 1, {0x80, 0x0}},
	{0x4513, 1, {0x00, 0x0}},
	{0x4516, 1, {0x28, 0x0}},
	{0x4603, 1, {0x00, 0x0}},
	{0x460a, 1, {0x50, 0x0}},
	{0x4640, 1, {0x62, 0x0}},
	{0x464d, 1, {0x00, 0x0}},
	{0x4800, 1, {0x04, 0x0}},
	{0x480e, 1, {0x00, 0x0}},
	{0x4813, 1, {0x00, 0x0}},
	{0x4837, 1, {0x0b, 0x0}},
	{0x484b, 1, {0x27, 0x0}},
	{0x4850, 1, {0x47, 0x0}},
	{0x4851, 1, {0xaa, 0x0}},
	{0x4852, 1, {0xff, 0x0}},
	{0x4853, 1, {0x20, 0x0}},
	{0x4854, 1, {0x08, 0x0}},
	{0x4855, 1, {0x30, 0x0}},
	{0x4860, 1, {0x00, 0x0}},
	{0x4d00, 1, {0x4d, 0x0}},
	{0x4d01, 1, {0x42, 0x0}},
	{0x4d02, 1, {0xb9, 0x0}},
	{0x4d03, 1, {0x94, 0x0}},
	{0x4d04, 1, {0x95, 0x0}},
	{0x4d05, 1, {0xce, 0x0}},
	{0x4d09, 1, {0x63, 0x0}},
	{0x5000, 1, {0x0f, 0x0}},
	{0x5001, 1, {0x0d, 0x0}},
	{0x5080, 1, {0x00, 0x0}},
	{0x50c0, 1, {0x00, 0x0}},
	{0x5100, 1, {0x00, 0x0}},
	{0x5200, 1, {0x70, 0x0}},
	{0x5201, 1, {0x70, 0x0}},
	{0x5681, 1, {0x00, 0x0}},
	{0x5780, 1, {0x53, 0x0}},
	{0x5781, 1, {0x03, 0x0}},
	{0x5782, 1, {0x60, 0x0}},
	{0x5783, 1, {0xf0, 0x0}},
	{0x5784, 1, {0x00, 0x0}},
	{0x5785, 1, {0x40, 0x0}},
	{0x5786, 1, {0x01, 0x0}},
	{0x5788, 1, {0x60, 0x0}},
	{0x5789, 1, {0xf0, 0x0}},
	{0x5791, 1, {0x08, 0x0}},
	{0x5792, 1, {0x11, 0x0}},
	{0x5793, 1, {0x33, 0x0}},	
	{SEN_CMD_SETVD, 1, {0x00, 0x0}},
	{SEN_CMD_PRESET, 1, {0x00, 0x0}},
	{SEN_CMD_DIRECTION, 1, {0x00, 0x0}},	
	{0x0100, 1, {0x01, 0x0}},
};

static CTL_SEN_CMD os08b10_mode_2[] = {
	{0x0100, 1, {0x00, 0x0}},
	{0x0103, 1, {0x01, 0x0}},
	{SEN_CMD_DELAY, 1, { 5, 0x0}},
	{0x0109, 1, {0x01, 0x0}},
	{0x0104, 1, {0x03, 0x0}},
	{0x0102, 1, {0x00, 0x0}},
	{0x0303, 1, {0x00, 0x0}},
	{0x0305, 1, {0x3b, 0x0}},
	{0x0306, 1, {0x03, 0x0}},
	{0x0307, 1, {0x00, 0x0}},
	{0x0308, 1, {0x04, 0x0}},
	{0x0309, 1, {0x02, 0x0}},
	{0x030a, 1, {0x01, 0x0}},
	{0x030c, 1, {0x00, 0x0}},
	{0x0317, 1, {0x49, 0x0}},
	{0x0322, 1, {0x01, 0x0}},
	{0x0323, 1, {0x05, 0x0}},
	{0x0324, 1, {0x01, 0x0}},
	{0x0325, 1, {0x80, 0x0}},
	{0x0327, 1, {0x03, 0x0}},
	{0x0328, 1, {0x05, 0x0}},
	{0x0329, 1, {0x02, 0x0}},
	{0x032a, 1, {0x02, 0x0}},
	{0x032c, 1, {0x01, 0x0}},
	{0x032d, 1, {0x00, 0x0}},
	{0x032e, 1, {0x03, 0x0}},
	{0x032f, 1, {0x01, 0x0}},
	{0x3002, 1, {0x00, 0x0}},
	{0x300f, 1, {0x11, 0x0}},
	{0x3012, 1, {0x41, 0x0}},
	{0x301e, 1, {0xb8, 0x0}},
	{0x3026, 1, {0x12, 0x0}},
	{0x3027, 1, {0x08, 0x0}},
	{0x302c, 1, {0x1a, 0x0}},
	{0x3104, 1, {0x00, 0x0}},
	{0x3106, 1, {0x10, 0x0}},
	{0x3400, 1, {0x00, 0x0}},
	{0x3408, 1, {0x05, 0x0}},
	{0x3409, 1, {0x22, 0x0}},
	{0x340a, 1, {0x02, 0x0}},
	{0x340c, 1, {0x08, 0x0}},
	{0x340d, 1, {0xa4, 0x0}},
	{0x3420, 1, {0x00, 0x0}},
	{0x3421, 1, {0x00, 0x0}},
	{0x3422, 1, {0x00, 0x0}},
	{0x3423, 1, {0x00, 0x0}},
	{0x3424, 1, {0x00, 0x0}},
	{0x3425, 1, {0x00, 0x0}},
	{0x3426, 1, {0x55, 0x0}},
	{0x3427, 1, {0x15, 0x0}},
	{0x3428, 1, {0x55, 0x0}},
	{0x3429, 1, {0x15, 0x0}},
	{0x342a, 1, {0x50, 0x0}},
	{0x342b, 1, {0x00, 0x0}},
	{0x342c, 1, {0x01, 0x0}},
	{0x342d, 1, {0x54, 0x0}},
	{0x342e, 1, {0x01, 0x0}},
	{0x342f, 1, {0x04, 0x0}},
	{0x3501, 1, {0x01, 0x0}},
	{0x3502, 1, {0x40, 0x0}},
	{0x3504, 1, {0x18, 0x0}},
	{0x3541, 1, {0x00, 0x0}},
	{0x3542, 1, {0x40, 0x0}},
	{0x3544, 1, {0x18, 0x0}},
	{0x3581, 1, {0x00, 0x0}},
	{0x3582, 1, {0x40, 0x0}},
	{0x3603, 1, {0x30, 0x0}},
	{0x3612, 1, {0x96, 0x0}},
	{0x3613, 1, {0xe0, 0x0}},
	{0x3623, 1, {0x00, 0x0}},
	{0x3624, 1, {0xff, 0x0}},
	{0x3625, 1, {0xff, 0x0}},
	{0x362a, 1, {0x12, 0x0}},
	{0x362b, 1, {0x12, 0x0}},
	{0x362c, 1, {0x12, 0x0}},
	{0x362d, 1, {0x12, 0x0}},
	{0x362e, 1, {0x17, 0x0}},
	{0x362f, 1, {0x2d, 0x0}},
	{0x3630, 1, {0x67, 0x0}},
	{0x3631, 1, {0x7f, 0x0}},
	{0x3638, 1, {0x00, 0x0}},
	{0x3639, 1, {0xff, 0x0}},
	{0x363a, 1, {0xff, 0x0}},
	{0x3643, 1, {0x0a, 0x0}},
	{0x3644, 1, {0x00, 0x0}},
	{0x3645, 1, {0x0a, 0x0}},
	{0x3646, 1, {0x0a, 0x0}},
	{0x3647, 1, {0x06, 0x0}},
	{0x3648, 1, {0x00, 0x0}},
	{0x3649, 1, {0x0a, 0x0}},
	{0x364a, 1, {0x0d, 0x0}},
	{0x364b, 1, {0x02, 0x0}},
	{0x364c, 1, {0x12, 0x0}},
	{0x364d, 1, {0x12, 0x0}},
	{0x364e, 1, {0x12, 0x0}},
	{0x364f, 1, {0x12, 0x0}},
	{0x3650, 1, {0xf7, 0x0}},
	{0x3651, 1, {0x77, 0x0}},
	{0x365a, 1, {0xbb, 0x0}},
	{0x365b, 1, {0x9d, 0x0}},
	{0x365c, 1, {0x8e, 0x0}},
	{0x365d, 1, {0x86, 0x0}},
	{0x3661, 1, {0x07, 0x0}},
	{0x3662, 1, {0x0a, 0x0}},
	{0x3667, 1, {0x54, 0x0}},
	{0x366f, 1, {0x00, 0x0}},
	{0x3671, 1, {0x89, 0x0}},
	{0x3673, 1, {0x2a, 0x0}},
	{0x3678, 1, {0x22, 0x0}},
	{0x3679, 1, {0x00, 0x0}},
	{0x3682, 1, {0xf1, 0x0}},
	{0x3687, 1, {0x02, 0x0}},
	{0x3688, 1, {0x81, 0x0}},
	{0x3689, 1, {0x02, 0x0}},
	{0x3692, 1, {0x0f, 0x0}},
	{0x3701, 1, {0x39, 0x0}},
	{0x3703, 1, {0x32, 0x0}},
	{0x3705, 1, {0x00, 0x0}},
	{0x3706, 1, {0x3a, 0x0}},
	{0x3708, 1, {0x27, 0x0}},
	{0x370a, 1, {0x00, 0x0}},
	{0x370b, 1, {0x8a, 0x0}},
	{0x3712, 1, {0x13, 0x0}},
	{0x3714, 1, {0x02, 0x0}},
	{0x3715, 1, {0x00, 0x0}},
	{0x3716, 1, {0x00, 0x0}},
	{0x3719, 1, {0x23, 0x0}},
	{0x371c, 1, {0x00, 0x0}},
	{0x371d, 1, {0x08, 0x0}},
	{0x373b, 1, {0x50, 0x0}},
	{0x3755, 1, {0x01, 0x0}},
	{0x3756, 1, {0xa9, 0x0}},
	{0x3757, 1, {0xa9, 0x0}},
	{0x376b, 1, {0x02, 0x0}},
	{0x376c, 1, {0x02, 0x0}},
	{0x376d, 1, {0x04, 0x0}},
	{0x376e, 1, {0x04, 0x0}},
	{0x376f, 1, {0x22, 0x0}},
	{0x377a, 1, {0x02, 0x0}},
	{0x377b, 1, {0x01, 0x0}},
	{0x3785, 1, {0x08, 0x0}},
	{0x3790, 1, {0x10, 0x0}},
	{0x3792, 1, {0x00, 0x0}},
	{0x3796, 1, {0x00, 0x0}},
	{0x3797, 1, {0x00, 0x0}},
	{0x3798, 1, {0x00, 0x0}},
	{0x3799, 1, {0x00, 0x0}},
	{0x37bb, 1, {0x88, 0x0}},
	{0x37be, 1, {0x01, 0x0}},
	{0x37bf, 1, {0x01, 0x0}},
	{0x37c0, 1, {0x00, 0x0}},
	{0x37c7, 1, {0x51, 0x0}},
	{0x37c8, 1, {0x22, 0x0}},
	{0x37c9, 1, {0x00, 0x0}},
	{0x37ca, 1, {0xb6, 0x0}},
	{0x37cc, 1, {0x0e, 0x0}},
	{0x37cf, 1, {0x02, 0x0}},
	{0x37d0, 1, {0x00, 0x0}},
	{0x37d1, 1, {0x3a, 0x0}},
	{0x37d2, 1, {0x00, 0x0}},
	{0x37d3, 1, {0x8a, 0x0}},
	{0x37d4, 1, {0x00, 0x0}},
	{0x37d5, 1, {0x3a, 0x0}},
	{0x37d6, 1, {0x00, 0x0}},
	{0x37d7, 1, {0x8a, 0x0}},
	{0x3800, 1, {0x00, 0x0}},
	{0x3801, 1, {0x00, 0x0}},
	{0x3802, 1, {0x00, 0x0}},
	{0x3803, 1, {0x00, 0x0}},
	{0x3804, 1, {0x0f, 0x0}},
	{0x3805, 1, {0x1f, 0x0}},
	{0x3806, 1, {0x08, 0x0}},
	{0x3807, 1, {0x7f, 0x0}},
	{0x3808, 1, {0x0f, 0x0}},
	{0x3809, 1, {0x04, 0x0}},
	{0x380a, 1, {0x08, 0x0}},
	{0x380b, 1, {0x74, 0x0}},
	{0x380c, 1, {0x02, 0x0}},
	{0x380d, 1, {0xd0, 0x0}},
	{0x380e, 1, {0x05, 0x0}},
	{0x380f, 1, {0x34, 0x0}},
	{0x3811, 1, {0x10, 0x0}},
	{0x3813, 1, {0x08, 0x0}},
	{0x3814, 1, {0x01, 0x0}},
	{0x3815, 1, {0x01, 0x0}},
	{0x3816, 1, {0x01, 0x0}},
	{0x3817, 1, {0x01, 0x0}},
	{0x381c, 1, {0x08, 0x0}},
	{0x3820, 1, {0x03, 0x0}},
	{0x3821, 1, {0x00, 0x0}},
	{0x3822, 1, {0x04, 0x0}},
	{0x3823, 1, {0x08, 0x0}},
	{0x382b, 1, {0x00, 0x0}},
	{0x382c, 1, {0x00, 0x0}},
	{0x3833, 1, {0x45, 0x0}},
	{0x383e, 1, {0x00, 0x0}},
	{0x384c, 1, {0x02, 0x0}},
	{0x384d, 1, {0xd0, 0x0}},
	{0x3858, 1, {0x7c, 0x0}},
	{0x3850, 1, {0xff, 0x0}},
	{0x3851, 1, {0xff, 0x0}},
	{0x3852, 1, {0xff, 0x0}},
	{0x3853, 1, {0xff, 0x0}},
	{0x3865, 1, {0x02, 0x0}},
	{0x3866, 1, {0x00, 0x0}},
	{0x3867, 1, {0x02, 0x0}},
	{0x3868, 1, {0x00, 0x0}},
	{0x386a, 1, {0x05, 0x0}},
	{0x386b, 1, {0x00, 0x0}},
	{0x386c, 1, {0x05, 0x0}},
	{0x386d, 1, {0x61, 0x0}},
	{0x3881, 1, {0x02, 0x0}},
	{0x3882, 1, {0x00, 0x0}},
	{0x3883, 1, {0x08, 0x0}},
	{0x3884, 1, {0x01, 0x0}},
	{0x3885, 1, {0x00, 0x0}},
	{0x3886, 1, {0x00, 0x0}},
	{0x3887, 1, {0x20, 0x0}},
	{0x3888, 1, {0x40, 0x0}},
	{0x3c37, 1, {0x00, 0x0}},
	{0x3c49, 1, {0x00, 0x0}},
	{0x3c4a, 1, {0x9a, 0x0}},
	{0x3c4c, 1, {0x01, 0x0}},
	{0x3c4d, 1, {0x00, 0x0}},
	{0x3c4e, 1, {0x00, 0x0}},
	{0x3c50, 1, {0x00, 0x0}},
	{0x3c51, 1, {0x00, 0x0}},
	{0x3c52, 1, {0x00, 0x0}},
	{0x3c67, 1, {0x10, 0x0}},
	{0x3c69, 1, {0x10, 0x0}},
	{0x3c6a, 1, {0x00, 0x0}},
	{0x3c6b, 1, {0x52, 0x0}},
	{0x3c6c, 1, {0x00, 0x0}},
	{0x3c6d, 1, {0x52, 0x0}},
	{0x3c70, 1, {0x00, 0x0}},
	{0x3c71, 1, {0x04, 0x0}},
	{0x3c72, 1, {0x00, 0x0}},
	{0x3c73, 1, {0x52, 0x0}},
	{0x3c74, 1, {0x00, 0x0}},
	{0x3c75, 1, {0x52, 0x0}},
	{0x3c76, 1, {0x12, 0x0}},
	{0x3c77, 1, {0x12, 0x0}},
	{0x3c79, 1, {0x00, 0x0}},
	{0x3c7a, 1, {0x00, 0x0}},
	{0x3c7b, 1, {0x00, 0x0}},
	{0x3cb6, 1, {0x41, 0x0}},
	{0x3cb9, 1, {0x00, 0x0}},
	{0x3cc0, 1, {0x90, 0x0}},
	{0x3cc2, 1, {0x90, 0x0}},
	{0x3cc5, 1, {0x00, 0x0}},
	{0x3cc6, 1, {0x98, 0x0}},
	{0x3cc7, 1, {0x00, 0x0}},
	{0x3cc8, 1, {0x98, 0x0}},
	{0x3cc9, 1, {0x00, 0x0}},
	{0x3cca, 1, {0x98, 0x0}},
	{0x3ccc, 1, {0x01, 0x0}},
	{0x3cd9, 1, {0x7c, 0x0}},
	{0x3cda, 1, {0x7c, 0x0}},
	{0x3cdb, 1, {0x7c, 0x0}},
	{0x3cdc, 1, {0x7c, 0x0}},
	{0x3d8c, 1, {0x70, 0x0}},
	{0x3d8d, 1, {0x10, 0x0}},
	{0x4001, 1, {0xeb, 0x0}},
	{0x4004, 1, {0x00, 0x0}},
	{0x4005, 1, {0x40, 0x0}},
	{0x4008, 1, {0x02, 0x0}},
	{0x4009, 1, {0x0d, 0x0}},
	{0x400a, 1, {0x02, 0x0}},
	{0x400b, 1, {0x00, 0x0}},
	{0x400e, 1, {0x40, 0x0}},
	{0x401a, 1, {0x50, 0x0}},
	{0x4028, 1, {0x6f, 0x0}},
	{0x4029, 1, {0x00, 0x0}},
	{0x402a, 1, {0x7f, 0x0}},
	{0x402b, 1, {0x01, 0x0}},
	{0x402e, 1, {0x00, 0x0}},
	{0x402f, 1, {0x40, 0x0}},
	{0x4030, 1, {0x00, 0x0}},
	{0x4031, 1, {0x40, 0x0}},
	{0x4032, 1, {0x1f, 0x0}},
	{0x4050, 1, {0x01, 0x0}},
	{0x4051, 1, {0x06, 0x0}},
	{0x405d, 1, {0x00, 0x0}},
	{0x405e, 1, {0x00, 0x0}},
	{0x4288, 1, {0xce, 0x0}},
	{0x4289, 1, {0x00, 0x0}},
	{0x428a, 1, {0x40, 0x0}},
	{0x430b, 1, {0x0f, 0x0}},
	{0x430c, 1, {0xfc, 0x0}},
	{0x430d, 1, {0x00, 0x0}},
	{0x430e, 1, {0x00, 0x0}},
	{0x4314, 1, {0x04, 0x0}},
	{0x4500, 1, {0x1a, 0x0}},
	{0x4501, 1, {0x18, 0x0}},
	{0x4504, 1, {0x00, 0x0}},
	{0x4505, 1, {0x00, 0x0}},
	{0x4506, 1, {0x32, 0x0}},
	{0x4507, 1, {0x03, 0x0}},
	{0x4508, 1, {0x1a, 0x0}},
	{0x450a, 1, {0x12, 0x0}},
	{0x450b, 1, {0x28, 0x0}},
	{0x450c, 1, {0x00, 0x0}},
	{0x450f, 1, {0x80, 0x0}},
	{0x4513, 1, {0x00, 0x0}},
	{0x4516, 1, {0x28, 0x0}},
	{0x4603, 1, {0x00, 0x0}},
	{0x460a, 1, {0x50, 0x0}},
	{0x4640, 1, {0x62, 0x0}},
	{0x464d, 1, {0x00, 0x0}},
	{0x4800, 1, {0x04, 0x0}},
	{0x480e, 1, {0x04, 0x0}},
	{0x4813, 1, {0x84, 0x0}},
	{0x4837, 1, {0x0b, 0x0}},
	{0x484b, 1, {0x67, 0x0}},
	{0x4850, 1, {0x47, 0x0}},
	{0x4851, 1, {0xaa, 0x0}},
	{0x4852, 1, {0xff, 0x0}},
	{0x4853, 1, {0x20, 0x0}},
	{0x4854, 1, {0x08, 0x0}},
	{0x4855, 1, {0x30, 0x0}},
	{0x4860, 1, {0x00, 0x0}},
	{0x4d00, 1, {0x4d, 0x0}},
	{0x4d01, 1, {0x42, 0x0}},
	{0x4d02, 1, {0xb9, 0x0}},
	{0x4d03, 1, {0x94, 0x0}},
	{0x4d04, 1, {0x95, 0x0}},
	{0x4d05, 1, {0xce, 0x0}},
	{0x4d09, 1, {0x63, 0x0}},
	{0x5000, 1, {0x0f, 0x0}},
	{0x5001, 1, {0x0d, 0x0}},
	{0x5080, 1, {0x00, 0x0}},
	{0x50c0, 1, {0x00, 0x0}},
	{0x5100, 1, {0x00, 0x0}},
	{0x5200, 1, {0x70, 0x0}},
	{0x5201, 1, {0x70, 0x0}},
	{0x5681, 1, {0x02, 0x0}},
	{0x5780, 1, {0x53, 0x0}},
	{0x5781, 1, {0x03, 0x0}},
	{0x5782, 1, {0x18, 0x0}},
	{0x5783, 1, {0x3c, 0x0}},
	{0x5784, 1, {0x00, 0x0}},
	{0x5785, 1, {0x40, 0x0}},
	{0x5786, 1, {0x01, 0x0}},
	{0x5788, 1, {0x18, 0x0}},
	{0x5789, 1, {0x3c, 0x0}},
	{0x5791, 1, {0x08, 0x0}},
	{0x5792, 1, {0x11, 0x0}},
	{0x5793, 1, {0x33, 0x0}},
	{SEN_CMD_SETVD, 1, {0x00, 0x0}},
	{SEN_CMD_PRESET, 1, {0x00, 0x0}},
	{SEN_CMD_DIRECTION, 1, {0x00, 0x0}},	
	{0x0100, 1, {0x01, 0x0}},
};

static CTL_SEN_CMD os08b10_mode_3[] = {
	{0x0100, 1, {0x00, 0x0}},
	{0x0103, 1, {0x01, 0x0}},
	{SEN_CMD_DELAY, 1, { 5, 0x0}},	
	{0x0109, 1, {0x01, 0x0}},
	{0x0104, 1, {0x03, 0x0}},
	{0x0102, 1, {0x00, 0x0}},
	{0x0303, 1, {0x00, 0x0}},
	{0x0305, 1, {0x3b, 0x0}},
	{0x0306, 1, {0x03, 0x0}},
	{0x0307, 1, {0x00, 0x0}},
	{0x0308, 1, {0x04, 0x0}},
	{0x0309, 1, {0x02, 0x0}},
	{0x030a, 1, {0x01, 0x0}},
	{0x030c, 1, {0x00, 0x0}},
	{0x0317, 1, {0x4a, 0x0}},
	{0x0322, 1, {0x01, 0x0}},
	{0x0323, 1, {0x05, 0x0}},
	{0x0324, 1, {0x01, 0x0}},
	{0x0325, 1, {0x80, 0x0}},
	{0x0327, 1, {0x02, 0x0}},
	{0x0328, 1, {0x05, 0x0}},
	{0x0329, 1, {0x01, 0x0}},
	{0x032a, 1, {0x02, 0x0}},
	{0x032c, 1, {0x01, 0x0}},
	{0x032d, 1, {0x00, 0x0}},
	{0x032e, 1, {0x03, 0x0}},
	{0x032f, 1, {0x01, 0x0}},
	{0x3002, 1, {0x00, 0x0}},
	{0x300f, 1, {0x11, 0x0}},
	{0x3012, 1, {0x41, 0x0}},
	{0x301e, 1, {0xb8, 0x0}},
	{0x3026, 1, {0x12, 0x0}},
	{0x3027, 1, {0x08, 0x0}},
	{0x302c, 1, {0x1a, 0x0}},
	{0x3104, 1, {0x00, 0x0}},
	{0x3106, 1, {0x10, 0x0}},
	{0x3400, 1, {0x00, 0x0}},
	{0x3408, 1, {0x05, 0x0}},
	{0x3409, 1, {0x22, 0x0}},
	{0x340a, 1, {0x02, 0x0}},
	{0x340c, 1, {0x08, 0x0}},
	{0x340d, 1, {0xa4, 0x0}},
	{0x3420, 1, {0x00, 0x0}},
	{0x3421, 1, {0x00, 0x0}},
	{0x3422, 1, {0x00, 0x0}},
	{0x3423, 1, {0x00, 0x0}},
	{0x3424, 1, {0x00, 0x0}},
	{0x3425, 1, {0x00, 0x0}},
	{0x3426, 1, {0x55, 0x0}},
	{0x3427, 1, {0x15, 0x0}},
	{0x3428, 1, {0x55, 0x0}},
	{0x3429, 1, {0x15, 0x0}},
	{0x342a, 1, {0x50, 0x0}},
	{0x342b, 1, {0x00, 0x0}},
	{0x342c, 1, {0x01, 0x0}},
	{0x342d, 1, {0x54, 0x0}},
	{0x342e, 1, {0x01, 0x0}},
	{0x342f, 1, {0x04, 0x0}},
	{0x3501, 1, {0x01, 0x0}},
	{0x3502, 1, {0x40, 0x0}},
	{0x3503, 1, {0xa8, 0x0}},
	{0x3504, 1, {0x18, 0x0}}, 
	{0x3541, 1, {0x00, 0x0}},
	{0x3542, 1, {0x40, 0x0}},
	{0x3581, 1, {0x00, 0x0}},
	{0x3582, 1, {0x40, 0x0}},
	{0x3603, 1, {0x30, 0x0}},
	{0x3612, 1, {0x96, 0x0}},
	{0x3613, 1, {0xe0, 0x0}},
	{0x3623, 1, {0x00, 0x0}},
	{0x3624, 1, {0xff, 0x0}},
	{0x3625, 1, {0xff, 0x0}},
	{0x362a, 1, {0x0e, 0x0}},
	{0x362b, 1, {0x0e, 0x0}},
	{0x362c, 1, {0x0e, 0x0}},
	{0x362d, 1, {0x0e, 0x0}},
	{0x362e, 1, {0x17, 0x0}},
	{0x362f, 1, {0x2d, 0x0}},
	{0x3630, 1, {0x67, 0x0}},
	{0x3631, 1, {0x7f, 0x0}},
	{0x3638, 1, {0x00, 0x0}},
	{0x3639, 1, {0xff, 0x0}},
	{0x363a, 1, {0xff, 0x0}},
	{0x3643, 1, {0x0a, 0x0}},
	{0x3644, 1, {0x00, 0x0}},
	{0x3645, 1, {0x0a, 0x0}},
	{0x3646, 1, {0x0a, 0x0}},
	{0x3647, 1, {0x06, 0x0}},
	{0x3648, 1, {0x00, 0x0}},
	{0x3649, 1, {0x0a, 0x0}},
	{0x364a, 1, {0x0d, 0x0}},
	{0x364b, 1, {0x02, 0x0}},
	{0x364c, 1, {0x0e, 0x0}},
	{0x364d, 1, {0x0e, 0x0}},
	{0x364e, 1, {0x0e, 0x0}},
	{0x364f, 1, {0x0e, 0x0}},
	{0x3650, 1, {0xf7, 0x0}},
	{0x3651, 1, {0x77, 0x0}},
	{0x365a, 1, {0xbb, 0x0}},
	{0x365b, 1, {0x9d, 0x0}},
	{0x365c, 1, {0x8e, 0x0}},
	{0x365d, 1, {0x86, 0x0}},
	{0x3661, 1, {0x07, 0x0}},
	{0x3662, 1, {0x00, 0x0}},
	{0x3667, 1, {0xd4, 0x0}},
	{0x366f, 1, {0x00, 0x0}},
	{0x3671, 1, {0x88, 0x0}},
	{0x3673, 1, {0x2a, 0x0}},
	{0x3678, 1, {0x00, 0x0}},
	{0x3679, 1, {0x00, 0x0}},
	{0x3682, 1, {0xf1, 0x0}},
	{0x3687, 1, {0x02, 0x0}},
	{0x3688, 1, {0x81, 0x0}},
	{0x3689, 1, {0x02, 0x0}},
	{0x3692, 1, {0x0f, 0x0}},
	{0x3701, 1, {0x39, 0x0}},
	{0x3703, 1, {0x32, 0x0}},
	{0x3705, 1, {0x00, 0x0}},
	{0x3706, 1, {0x74, 0x0}},
	{0x3708, 1, {0x35, 0x0}},
	{0x370a, 1, {0x01, 0x0}},
	{0x370b, 1, {0x4b, 0x0}},
	{0x3712, 1, {0x13, 0x0}},
	{0x3714, 1, {0x02, 0x0}},
	{0x3715, 1, {0x00, 0x0}},
	{0x3716, 1, {0x00, 0x0}},
	{0x3719, 1, {0x23, 0x0}},
	{0x371c, 1, {0x00, 0x0}},
	{0x371d, 1, {0x08, 0x0}},
	{0x373b, 1, {0x50, 0x0}},
	{0x3755, 1, {0x01, 0x0}},
	{0x3756, 1, {0xea, 0x0}},
	{0x3757, 1, {0xea, 0x0}},
	{0x376b, 1, {0x02, 0x0}},
	{0x376c, 1, {0x02, 0x0}},
	{0x376d, 1, {0x04, 0x0}},
	{0x376e, 1, {0x04, 0x0}},
	{0x376f, 1, {0x22, 0x0}},
	{0x377a, 1, {0x02, 0x0}},
	{0x377b, 1, {0x01, 0x0}},
	{0x3785, 1, {0x08, 0x0}},
	{0x3790, 1, {0x10, 0x0}},
	{0x3792, 1, {0x00, 0x0}},
	{0x3796, 1, {0x00, 0x0}},
	{0x3797, 1, {0x00, 0x0}},
	{0x3798, 1, {0x00, 0x0}},
	{0x3799, 1, {0x00, 0x0}},
	{0x37bb, 1, {0x88, 0x0}},
	{0x37be, 1, {0x01, 0x0}},
	{0x37bf, 1, {0x01, 0x0}},
	{0x37c0, 1, {0x00, 0x0}},
	{0x37c7, 1, {0x51, 0x0}},
	{0x37c8, 1, {0x22, 0x0}},
	{0x37c9, 1, {0x00, 0x0}},
	{0x37ca, 1, {0xe4, 0x0}},
	{0x37cc, 1, {0x0e, 0x0}},
	{0x37cf, 1, {0x02, 0x0}},
	{0x37d0, 1, {0x00, 0x0}},
	{0x37d1, 1, {0x74, 0x0}},
	{0x37d2, 1, {0x01, 0x0}},
	{0x37d3, 1, {0x4b, 0x0}},
	{0x37d4, 1, {0x00, 0x0}},
	{0x37d5, 1, {0x74, 0x0}},
	{0x37d6, 1, {0x01, 0x0}},
	{0x37d7, 1, {0x4b, 0x0}},
	{0x3800, 1, {0x00, 0x0}},
	{0x3801, 1, {0x00, 0x0}},
	{0x3802, 1, {0x00, 0x0}},
	{0x3803, 1, {0x00, 0x0}},
	{0x3804, 1, {0x0f, 0x0}},
	{0x3805, 1, {0x1f, 0x0}},
	{0x3806, 1, {0x08, 0x0}},
	{0x3807, 1, {0x7f, 0x0}},
	{0x3808, 1, {0x0f, 0x0}},
	{0x3809, 1, {0x04, 0x0}},
	{0x380a, 1, {0x08, 0x0}},
	{0x380b, 1, {0x74, 0x0}},
	{0x380c, 1, {0x05, 0x0}},
	{0x380d, 1, {0x10, 0x0}},
	{0x380e, 1, {0x05, 0x0}},
	{0x380f, 1, {0xc8, 0x0}},
	{0x3811, 1, {0x0e, 0x0}},
	{0x3813, 1, {0x06, 0x0}},
	{0x3814, 1, {0x01, 0x0}},
	{0x3815, 1, {0x01, 0x0}},
	{0x3816, 1, {0x01, 0x0}},
	{0x3817, 1, {0x01, 0x0}},
	{0x381c, 1, {0x00, 0x0}},
	{0x3820, 1, {0x02, 0x0}},
	{0x3821, 1, {0x00, 0x0}},
	{0x3822, 1, {0x04, 0x0}},
	{0x3823, 1, {0x08, 0x0}},
	{0x382b, 1, {0x00, 0x0}},
	{0x382c, 1, {0x00, 0x0}},
	{0x3833, 1, {0x40, 0x0}},
	{0x383e, 1, {0x00, 0x0}},
	{0x384c, 1, {0x04, 0x0}},
	{0x384d, 1, {0x38, 0x0}},
	{0x3858, 1, {0x3c, 0x0}},
	{0x3850, 1, {0x00, 0x0}},
	{0x3851, 1, {0x00, 0x0}},
	{0x3852, 1, {0x00, 0x0}},
	{0x3853, 1, {0x00, 0x0}},
	{0x3865, 1, {0x02, 0x0}},
	{0x3866, 1, {0x00, 0x0}},
	{0x3867, 1, {0x02, 0x0}},
	{0x3868, 1, {0x00, 0x0}},
	{0x386a, 1, {0x05, 0x0}},
	{0x386b, 1, {0x00, 0x0}},
	{0x386c, 1, {0x05, 0x0}},
	{0x386d, 1, {0x61, 0x0}},
	{0x3881, 1, {0x02, 0x0}},
	{0x3882, 1, {0x00, 0x0}},
	{0x3883, 1, {0x08, 0x0}},
	{0x3884, 1, {0x01, 0x0}},
	{0x3885, 1, {0x00, 0x0}},
	{0x3886, 1, {0x00, 0x0}},
	{0x3887, 1, {0x20, 0x0}},
	{0x3888, 1, {0x40, 0x0}},
	{0x3c37, 1, {0x00, 0x0}},
	{0x3c49, 1, {0x00, 0x0}},
	{0x3c4a, 1, {0x9a, 0x0}},
	{0x3c4c, 1, {0x01, 0x0}},
	{0x3c4d, 1, {0x00, 0x0}},
	{0x3c4e, 1, {0x00, 0x0}},
	{0x3c50, 1, {0x00, 0x0}},
	{0x3c51, 1, {0x00, 0x0}},
	{0x3c52, 1, {0x00, 0x0}},
	{0x3c67, 1, {0x10, 0x0}},
	{0x3c69, 1, {0x10, 0x0}},
	{0x3c6a, 1, {0x00, 0x0}},
	{0x3c6b, 1, {0x62, 0x0}},
	{0x3c6c, 1, {0x00, 0x0}},
	{0x3c6d, 1, {0x62, 0x0}},
	{0x3c70, 1, {0x00, 0x0}},
	{0x3c71, 1, {0x04, 0x0}},
	{0x3c72, 1, {0x00, 0x0}},
	{0x3c73, 1, {0x62, 0x0}},
	{0x3c74, 1, {0x00, 0x0}},
	{0x3c75, 1, {0x62, 0x0}},
	{0x3c76, 1, {0x12, 0x0}},
	{0x3c77, 1, {0x12, 0x0}},
	{0x3c79, 1, {0x00, 0x0}},
	{0x3c7a, 1, {0x00, 0x0}},
	{0x3c7b, 1, {0x00, 0x0}},
	{0x3cb6, 1, {0x41, 0x0}},
	{0x3cb9, 1, {0x00, 0x0}},
	{0x3cc0, 1, {0x90, 0x0}},
	{0x3cc2, 1, {0x90, 0x0}},
	{0x3cc5, 1, {0x00, 0x0}},
	{0x3cc6, 1, {0x98, 0x0}},
	{0x3cc7, 1, {0x00, 0x0}},
	{0x3cc8, 1, {0x98, 0x0}},
	{0x3cc9, 1, {0x00, 0x0}},
	{0x3cca, 1, {0x98, 0x0}},
	{0x3ccc, 1, {0x01, 0x0}},
	{0x3cd9, 1, {0x7c, 0x0}},
	{0x3cda, 1, {0x7c, 0x0}},
	{0x3cdb, 1, {0x7c, 0x0}},
	{0x3cdc, 1, {0x7c, 0x0}},
	{0x3d8c, 1, {0x70, 0x0}},
	{0x3d8d, 1, {0x10, 0x0}},
	{0x4001, 1, {0x2b, 0x0}},
	{0x4004, 1, {0x00, 0x0}},
	{0x4005, 1, {0x40, 0x0}},
	{0x4008, 1, {0x02, 0x0}},
	{0x4009, 1, {0x0d, 0x0}},
	{0x400a, 1, {0x04, 0x0}},
	{0x400b, 1, {0xa8, 0x0}},
	{0x400e, 1, {0x40, 0x0}},
	{0x401a, 1, {0x50, 0x0}},
	{0x4028, 1, {0x6f, 0x0}},
	{0x4029, 1, {0x00, 0x0}},
	{0x402a, 1, {0x7f, 0x0}},
	{0x402b, 1, {0x01, 0x0}},
	{0x402e, 1, {0x00, 0x0}},
	{0x402f, 1, {0x40, 0x0}},
	{0x4030, 1, {0x00, 0x0}},
	{0x4031, 1, {0x40, 0x0}},
	{0x4032, 1, {0x9f, 0x0}},
	{0x4050, 1, {0x01, 0x0}},
	{0x4051, 1, {0x06, 0x0}},
	{0x405d, 1, {0x00, 0x0}},
	{0x405e, 1, {0x00, 0x0}},
	{0x4288, 1, {0xcf, 0x0}},
	{0x4289, 1, {0x00, 0x0}},
	{0x428a, 1, {0x40, 0x0}},
	{0x430b, 1, {0xff, 0x0}},
	{0x430c, 1, {0xff, 0x0}},
	{0x430d, 1, {0x00, 0x0}},
	{0x430e, 1, {0x00, 0x0}},
	{0x4314, 1, {0x04, 0x0}},
	{0x4500, 1, {0x1a, 0x0}},
	{0x4501, 1, {0x18, 0x0}},
	{0x4504, 1, {0x00, 0x0}},
	{0x4505, 1, {0x00, 0x0}},
	{0x4506, 1, {0x32, 0x0}},
	{0x4507, 1, {0x02, 0x0}},
	{0x4508, 1, {0x1a, 0x0}},
	{0x450a, 1, {0x12, 0x0}},
	{0x450b, 1, {0x28, 0x0}},
	{0x450c, 1, {0x00, 0x0}},
	{0x450f, 1, {0x80, 0x0}},
	{0x4513, 1, {0x00, 0x0}},
	{0x4516, 1, {0x28, 0x0}},
	{0x4603, 1, {0x00, 0x0}},
	{0x460a, 1, {0x50, 0x0}},
	{0x4640, 1, {0x62, 0x0}},
	{0x464d, 1, {0x00, 0x0}},
	{0x4800, 1, {0x04, 0x0}},
	{0x480e, 1, {0x00, 0x0}},
	{0x4813, 1, {0x00, 0x0}},
	{0x4837, 1, {0x0b, 0x0}},
	{0x484b, 1, {0x27, 0x0}},
	{0x4850, 1, {0x47, 0x0}},
	{0x4851, 1, {0xaa, 0x0}},
	{0x4852, 1, {0xff, 0x0}},
	{0x4853, 1, {0x20, 0x0}},
	{0x4854, 1, {0x08, 0x0}},
	{0x4855, 1, {0x30, 0x0}},
	{0x4860, 1, {0x00, 0x0}},
	{0x4d00, 1, {0x4d, 0x0}},
	{0x4d01, 1, {0x42, 0x0}},
	{0x4d02, 1, {0xb9, 0x0}},
	{0x4d03, 1, {0x94, 0x0}},
	{0x4d04, 1, {0x95, 0x0}},
	{0x4d05, 1, {0xce, 0x0}},
	{0x4d09, 1, {0x63, 0x0}},
	{0x5000, 1, {0x0f, 0x0}},
	{0x5001, 1, {0x0d, 0x0}},
	{0x5080, 1, {0x00, 0x0}},
	{0x50c0, 1, {0x00, 0x0}},
	{0x5100, 1, {0x00, 0x0}},
	{0x5200, 1, {0x70, 0x0}},
	{0x5201, 1, {0x70, 0x0}},
	{0x5681, 1, {0x00, 0x0}},
	{0x5780, 1, {0x53, 0x0}},
	{0x5781, 1, {0x03, 0x0}},
	{0x5782, 1, {0x60, 0x0}},
	{0x5783, 1, {0xf0, 0x0}},
	{0x5784, 1, {0x00, 0x0}},
	{0x5785, 1, {0x40, 0x0}},
	{0x5786, 1, {0x01, 0x0}},
	{0x5788, 1, {0x60, 0x0}},
	{0x5789, 1, {0xf0, 0x0}},
	{0x5791, 1, {0x08, 0x0}},
	{0x5792, 1, {0x11, 0x0}},
	{0x5793, 1, {0x33, 0x0}},	
	{SEN_CMD_SETVD, 1, {0x00, 0x0}},
	{SEN_CMD_PRESET, 1, {0x00, 0x0}},
	{SEN_CMD_DIRECTION, 1, {0x00, 0x0}},	
	{0x0100, 1, {0x01, 0x0}},
};

static UINT32 cur_sen_mode[CTL_SEN_ID_MAX] = {CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1};
static UINT32 cur_fps[CTL_SEN_ID_MAX] = {0};
static UINT32 chgmode_fps[CTL_SEN_ID_MAX] = {0};
static UINT32 power_ctrl_mclk[CTL_SEN_CLK_SEL_MAX] = {0};
static UINT32 reset_ctrl_count[CTL_SEN_ID_MAX] = {0};
static UINT32 pwdn_ctrl_count[CTL_SEN_ID_MAX] = {0};
static ISP_SENSOR_CTRL sensor_ctrl_last[CTL_SEN_ID_MAX] = {0};
static ISP_SENSOR_PRESET_CTRL preset_ctrl[CTL_SEN_ID_MAX] = {0};
static UINT32 compensation_ratio[CTL_SEN_ID_MAX][ISP_SEN_MFRAME_MAX_NUM] = {0};
static INT32 is_fastboot[CTL_SEN_ID_MAX];
static UINT32 fastboot_i2c_id[CTL_SEN_ID_MAX];
static UINT32 fastboot_i2c_addr[CTL_SEN_ID_MAX];
static BOOL i2c_valid[CTL_SEN_ID_MAX];

static CTL_SEN_DRV_TAB os08b10_sen_drv_tab = {
	sen_open_os08b10,
	sen_close_os08b10,
	sen_sleep_os08b10,
	sen_wakeup_os08b10,
	sen_write_reg_os08b10,
	sen_read_reg_os08b10,
	sen_chg_mode_os08b10,
	sen_chg_fps_os08b10,
	sen_set_info_os08b10,
	sen_get_info_os08b10,
};

static CTL_SEN_DRV_TAB *sen_get_drv_tab_os08b10(void)
{
	return &os08b10_sen_drv_tab;
}

static void sen_pwr_ctrl_os08b10(CTL_SEN_ID id, CTL_SEN_PWR_CTRL_FLAG flag, CTL_SEN_CLK_CB clk_cb)
{
	UINT32 i = 0;
	UINT32 reset_count = 0, pwdn_count = 0;	
	DBG_IND("enter flag %d \r\n", flag);

	if ((flag == CTL_SEN_PWR_CTRL_TURN_ON) && ((!is_fastboot[id]) || (fastboot_i2c_id[id] != sen_i2c[id].id) || (fastboot_i2c_addr[id]  != sen_i2c[id].addr))) {

		if (sen_power[id].pwdn_pin != CTL_SEN_IGNORE) {
			for ( i = 0; i < CTL_SEN_ID_MAX ; i++ ) {
				if ( pwdn_ctrl_count[i] == (sen_power[id].pwdn_pin)) {
					pwdn_count++;
				}
			}
			pwdn_ctrl_count[id] = (sen_power[id].pwdn_pin);			

			if (!pwdn_count) {
				gpio_direction_output((sen_power[id].pwdn_pin), 0);
				gpio_set_value((sen_power[id].pwdn_pin), 0);
				gpio_set_value((sen_power[id].pwdn_pin), 1);
			}
		}

		if (sen_power[id].rst_pin != CTL_SEN_IGNORE) {
			for ( i = 0; i < CTL_SEN_ID_MAX ; i++ ) {
				if ( reset_ctrl_count[i] == (sen_power[id].rst_pin)) {
					reset_count++;
				}
			}
			reset_ctrl_count[id] = (sen_power[id].rst_pin);			

			if (!reset_count) {
				vos_util_delay_ms(sen_power[id].stable_time);				
				gpio_direction_output((sen_power[id].rst_pin), 0);
				gpio_set_value((sen_power[id].rst_pin), 0);
				vos_util_delay_ms(sen_power[id].stable_time);
				gpio_set_value((sen_power[id].rst_pin), 1);
				vos_util_delay_ms(sen_power[id].rst_time);
			}
		}
				
		if (clk_cb != NULL) {			
			if (sen_power[id].mclk != CTL_SEN_IGNORE) {
				if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK ) {
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK] += 1;
				} else if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK2) {
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK2] += 1;
				} else { //CTL_SEN_CLK_SEL_SIEMCLK3
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK3] += 1;
				}
				if (1 == (power_ctrl_mclk[sen_power[id].mclk])) {
					clk_cb(sen_power[id].mclk, TRUE);
					vos_util_delay_ms(sen_power[id].stable_time);					
				}
			}
		}
	}

	if (flag == CTL_SEN_PWR_CTRL_TURN_OFF) {
		
		if (sen_power[id].pwdn_pin != CTL_SEN_IGNORE) {
			pwdn_ctrl_count[id] = 0;

			for ( i = 0; i < CTL_SEN_ID_MAX ; i++ ) {
				if ( pwdn_ctrl_count[i] == (sen_power[id].pwdn_pin)) {
					pwdn_count++;
				}
			}

			if (!pwdn_count) {
				gpio_direction_output((sen_power[id].pwdn_pin), 0);
				gpio_set_value((sen_power[id].pwdn_pin), 0);
			}
		}

		if (clk_cb != NULL) {
			if (sen_power[id].mclk != CTL_SEN_IGNORE) {				
				if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK ) {
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK] -= 1;
				} else if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK2) {
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK2] -= 1;
				} else { //CTL_SEN_CLK_SEL_SIEMCLK3
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK3] -= 1;
				}
				if (!power_ctrl_mclk[sen_power[id].mclk]) {
					vos_util_delay_ms(sen_power[id].stable_time);
					clk_cb(sen_power[id].mclk, FALSE);
				}
			}
		}
	
		if (sen_power[id].rst_pin != CTL_SEN_IGNORE) {
			reset_ctrl_count[id] = 0;
			for ( i = 0; i < CTL_SEN_ID_MAX ; i++ ) {
				if ( reset_ctrl_count[i] == (sen_power[id].rst_pin)) {
					reset_count++;
				}
			}

			if (!reset_count) {
				vos_util_delay_ms(sen_power[id].stable_time);				
				gpio_direction_output((sen_power[id].rst_pin), 0);
				gpio_set_value((sen_power[id].rst_pin), 0);
				vos_util_delay_ms(sen_power[id].stable_time);
			}
		}
	}
}

static CTL_SEN_CMD sen_set_cmd_info_os08b10(UINT32 addr, UINT32 data_length, UINT32 data0, UINT32 data1)
{
	CTL_SEN_CMD cmd;

	cmd.addr = addr;
	cmd.data_len = data_length;
	cmd.data[0] = data0;
	cmd.data[1] = data1;
	return cmd;
}

#if defined(__KERNEL__)
static void sen_load_cfg_from_compatible_os08b10(struct device_node *of_node)
{
	DBG_DUMP("compatible valid, using peri-dev.dtsi \r\n");
	sen_common_load_cfg_preset_compatible(of_node, &sen_preset);
	sen_common_load_cfg_direction_compatible(of_node, &sen_direction);
	sen_common_load_cfg_power_compatible(of_node, &sen_power);
	sen_common_load_cfg_i2c_compatible(of_node, &sen_i2c);
}
#endif

static ER sen_open_os08b10(CTL_SEN_ID id)
{
	ER rt = E_OK;

	#if defined(__KERNEL__)
	sen_i2c_reg_cb(sen_load_cfg_from_compatible_os08b10);
	#endif

	preset_ctrl[id].mode = ISP_SENSOR_PRESET_DEFAULT;
	i2c_valid[id] = TRUE;
	if ((!is_fastboot[id]) || (fastboot_i2c_id[id] != sen_i2c[id].id) || (fastboot_i2c_addr[id] != sen_i2c[id].addr)) {
		rt = sen_i2c_init_driver(id, &sen_i2c[id]);

		if (rt != E_OK) {
			i2c_valid[id] = FALSE;

			DBG_ERR("init. i2c driver fail (%d) \r\n", id);
		}
	}

	return rt;
}

static ER sen_close_os08b10(CTL_SEN_ID id)
{
	if ((!is_fastboot[id]) || (fastboot_i2c_id[id] != sen_i2c[id].id) || (fastboot_i2c_addr[id] != sen_i2c[id].addr)) {
		if (i2c_valid[id]) {
			sen_i2c_remove_driver(id);
		}
	} else {
		is_fastboot[id] = 0;
		#if defined(__KERNEL__)
		isp_builtin_uninit_i2c(id);
		#endif
	}

	i2c_valid[id] = FALSE;

	return E_OK;
}

static ER sen_sleep_os08b10(CTL_SEN_ID id)
{
	DBG_IND("enter \r\n");
	return E_OK;
}

static ER sen_wakeup_os08b10(CTL_SEN_ID id)
{
	DBG_IND("enter \r\n");
	return E_OK;
}

static ER sen_write_reg_os08b10(CTL_SEN_ID id, CTL_SEN_CMD *cmd)
{
	struct i2c_msg msgs;
	unsigned char buf[3];
	int i;

	if (!i2c_valid[id]) {
		return E_NOSPT;
	}

	buf[0]     = (cmd->addr >> 8) & 0xFF;
	buf[1]     = cmd->addr & 0xFF;
	buf[2]     = cmd->data[0] & 0xFF;
	msgs.addr  = sen_i2c[id].addr;
	msgs.flags = 0;
	msgs.len   = 3;
	msgs.buf   = buf;

	if ((!is_fastboot[id]) || (fastboot_i2c_id[id] != sen_i2c[id].id) || (fastboot_i2c_addr[id] != sen_i2c[id].addr)) {
		i = 0;
		while(1){
			if (sen_i2c_transfer(id, &msgs, 1) == 0) {
				break;
			}

			i++;
			if (i == 5)
				return E_SYS;
		}
	} else {
		#if defined(__KERNEL__)
		isp_builtin_set_transfer_i2c(id, &msgs, 1);
		#endif
	}

	return E_OK;
}

static ER sen_read_reg_os08b10(CTL_SEN_ID id, CTL_SEN_CMD *cmd)
{
	struct i2c_msg  msgs[2];
	unsigned char   tmp[2], tmp2[2];
	int i;

	if (!i2c_valid[id]) {
		return E_NOSPT;
	}

	tmp[0]        = (cmd->addr >> 8) & 0xFF;
	tmp[1]        = cmd->addr & 0xFF;
	msgs[0].addr  = sen_i2c[id].addr;
	msgs[0].flags = 0;
	msgs[0].len   = 2;
	msgs[0].buf   = tmp;

	tmp2[0]       = 0;
	msgs[1].addr  = sen_i2c[id].addr;
	msgs[1].flags = 1;
	msgs[1].len   = 1;
	msgs[1].buf   = tmp2;

	if ((!is_fastboot[id]) || (fastboot_i2c_id[id] != sen_i2c[id].id) || (fastboot_i2c_addr[id] != sen_i2c[id].addr)) {
		i = 0;
		while(1){
			if (sen_i2c_transfer(id, msgs, 2) == 0)
				break;
			i++;
			if (i == 5)
				return E_SYS;
		}
	} else {
		#if defined(__KERNEL__)
		isp_builtin_set_transfer_i2c(id, msgs, 2);
		#endif
	}

	cmd->data[0] = tmp2[0];

	return E_OK;
}

static UINT32 sen_get_cmd_tab_os08b10(CTL_SEN_MODE mode, CTL_SEN_CMD **cmd_tab)
{
	switch (mode) {
	case CTL_SEN_MODE_1:
		*cmd_tab = os08b10_mode_1;
		return sizeof(os08b10_mode_1) / sizeof(CTL_SEN_CMD);

	case CTL_SEN_MODE_2:
		*cmd_tab = os08b10_mode_2;
		return sizeof(os08b10_mode_2) / sizeof(CTL_SEN_CMD);

	case CTL_SEN_MODE_3:
		*cmd_tab = os08b10_mode_3;
		return sizeof(os08b10_mode_3) / sizeof(CTL_SEN_CMD);

	default:
		DBG_ERR("sensor mode %d no cmd table\r\n", mode);
		*cmd_tab = NULL;
		return 0;
	}
}

static ER sen_chg_mode_os08b10(CTL_SEN_ID id, CTL_SENDRV_CHGMODE_OBJ chgmode_obj)
{
	ISP_SENSOR_CTRL sensor_ctrl = {0};
	CTL_SEN_CMD *p_cmd_list = NULL, cmd;
	CTL_SEN_FLIP flip = CTL_SEN_FLIP_NONE;
	UINT32 sensor_vd;
	UINT32 idx, cmd_num = 0;
	ER rt = E_OK;

	cur_sen_mode[id] = chgmode_obj.mode;

	if (is_fastboot[id]) {
		#if defined(__KERNEL__)
		ISP_BUILTIN_SENSOR_CTRL *p_sensor_ctrl_temp;

		p_sensor_ctrl_temp = isp_builtin_get_sensor_gain(id);
		sensor_ctrl.gain_ratio[0] = p_sensor_ctrl_temp->gain_ratio[0];
		sensor_ctrl.gain_ratio[1] = p_sensor_ctrl_temp->gain_ratio[1];
		p_sensor_ctrl_temp = isp_builtin_get_sensor_expt(id);
		sensor_ctrl.exp_time[0] = p_sensor_ctrl_temp->exp_time[0];
		sensor_ctrl.exp_time[1] = p_sensor_ctrl_temp->exp_time[1];
		sen_set_chgmode_fps_os08b10(id, isp_builtin_get_chgmode_fps(id));
		sen_set_cur_fps_os08b10(id, isp_builtin_get_chgmode_fps(id));
		sen_set_expt_os08b10(id, &sensor_ctrl);
		sen_set_gain_os08b10(id, &sensor_ctrl);
		#endif
		preset_ctrl[id].mode = ISP_SENSOR_PRESET_CHGMODE;

		return E_OK;
	}

	// get & set sensor cmd table
	cmd_num = sen_get_cmd_tab_os08b10(chgmode_obj.mode, &p_cmd_list);
	if (p_cmd_list == NULL) {
		DBG_ERR("%s: SenMode(%d) out of range!!! \r\n", __func__, chgmode_obj.mode);
		return E_SYS;
	}

	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_CHGFPS) {
		sensor_vd = sen_calc_chgmode_vd_os08b10(id, chgmode_obj.frame_rate);
	} else {
		DBG_WRN(" not support fps adjust \r\n");
		sen_set_cur_fps_os08b10(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
		sen_set_chgmode_fps_os08b10(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
		sensor_vd = mode_basic_param[cur_sen_mode[id]].signal_info.vd_period;
	}

	for (idx = 0; idx < cmd_num; idx++) {
		if (p_cmd_list[idx].addr == SEN_CMD_DELAY) {
			vos_util_delay_ms((p_cmd_list[idx].data[0] & 0xFF) | ((p_cmd_list[idx].data[1] & 0xFF) << 8));
		} else if (p_cmd_list[idx].addr == SEN_CMD_SETVD) {
			cmd = sen_set_cmd_info_os08b10(0x380E, 1, (sensor_vd >> 8) & 0xFF, 0x00);
			rt |= sen_write_reg_os08b10(id, &cmd);
			cmd = sen_set_cmd_info_os08b10(0x380F, 1, sensor_vd & 0xFF, 0x00);
			rt |= sen_write_reg_os08b10(id, &cmd);
		} else if (p_cmd_list[idx].addr == SEN_CMD_PRESET) {
			switch (preset_ctrl[id].mode) {
				default:
				case ISP_SENSOR_PRESET_DEFAULT:
					sensor_ctrl.gain_ratio[0] = sen_preset[id].gain_ratio;
					sensor_ctrl.exp_time[0] = sen_preset[id].expt_time;
					if (mode_basic_param[cur_sen_mode[id]].frame_num == 2) {
						sensor_ctrl.exp_time[1] = sen_preset[id].expt_time >> 3;
					}
					break;

				case ISP_SENSOR_PRESET_CHGMODE:
					memcpy(&sensor_ctrl, &sensor_ctrl_last[id], sizeof(ISP_SENSOR_CTRL));
					break;

				case ISP_SENSOR_PRESET_AE:
					sensor_ctrl.exp_time[0] = preset_ctrl[id].exp_time[0];
					sensor_ctrl.exp_time[1] = preset_ctrl[id].exp_time[1];
					sensor_ctrl.gain_ratio[0] = preset_ctrl[id].gain_ratio[0];
					sensor_ctrl.gain_ratio[1] = preset_ctrl[id].gain_ratio[1];
				break;
			}

			sen_set_gain_os08b10(id, &sensor_ctrl);
			sen_set_expt_os08b10(id, &sensor_ctrl);
		} else if (p_cmd_list[idx].addr == SEN_CMD_DIRECTION) {
			if (sen_direction[id].mirror) {
				flip |= CTL_SEN_FLIP_H;
			}
			if (sen_direction[id].flip) {
				flip |= CTL_SEN_FLIP_V;
			}
			sen_set_flip_os08b10(id, &flip);
		} else {
			cmd = sen_set_cmd_info_os08b10(p_cmd_list[idx].addr, p_cmd_list[idx].data_len, p_cmd_list[idx].data[0], p_cmd_list[idx].data[1]);
			rt |= sen_write_reg_os08b10(id, &cmd);
		}
	}

	preset_ctrl[id].mode = ISP_SENSOR_PRESET_CHGMODE;

	if (rt != E_OK) {
		DBG_ERR("write register error %d \r\n", (INT)rt);
		return rt;
	}

	return E_OK;
}

static ER sen_chg_fps_os08b10(CTL_SEN_ID id, UINT32 fps)
{
	CTL_SEN_CMD cmd;
	UINT32 sensor_vd;
	ER rt = E_OK;

	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_CHGFPS) {
		sensor_vd = sen_calc_chgmode_vd_os08b10(id, fps);
	} else {
		DBG_WRN(" not support fps adjust \r\n");
		sen_set_cur_fps_os08b10(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
		sen_set_chgmode_fps_os08b10(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
		sensor_vd = mode_basic_param[cur_sen_mode[id]].signal_info.vd_period;
	}

	cmd = sen_set_cmd_info_os08b10(0x380E, 1, (sensor_vd >> 8) & 0xFF, 0x00);
	rt |= sen_write_reg_os08b10(id, &cmd);
	cmd = sen_set_cmd_info_os08b10(0x380F, 1, sensor_vd & 0xFF, 0x00);
	rt |= sen_write_reg_os08b10(id, &cmd);

	return rt;
}

static ER sen_set_info_os08b10(CTL_SEN_ID id, CTL_SENDRV_CFGID drv_cfg_id, void *data)
{
	switch (drv_cfg_id) {
	case CTL_SENDRV_CFGID_SET_EXPT:
		sen_set_expt_os08b10(id, data);
		break;
	case CTL_SENDRV_CFGID_SET_GAIN:
		sen_set_gain_os08b10(id, data);
		break;
	case CTL_SENDRV_CFGID_FLIP_TYPE:
		sen_set_flip_os08b10(id, (CTL_SEN_FLIP *)(data));
		break;
	case CTL_SENDRV_CFGID_USER_DEFINE1:
		sen_set_preset_os08b10(id, (ISP_SENSOR_PRESET_CTRL *)(data));
		break;
	default:
		return E_NOSPT;
	}
	return E_OK;
}

static ER sen_get_info_os08b10(CTL_SEN_ID id, CTL_SENDRV_CFGID drv_cfg_id, void *data)
{
	ER rt = E_OK;

	switch (drv_cfg_id) {
	case CTL_SENDRV_CFGID_GET_EXPT:
		sen_get_expt_os08b10(id, data);
		break;
	case CTL_SENDRV_CFGID_GET_GAIN:
		sen_get_gain_os08b10(id, data);
		break;
	case CTL_SENDRV_CFGID_GET_ATTR_BASIC:
		sen_get_attr_basic_os08b10((CTL_SENDRV_GET_ATTR_BASIC_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_ATTR_SIGNAL:
		sen_get_attr_signal_os08b10((CTL_SENDRV_GET_ATTR_SIGNAL_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_ATTR_CMDIF:
		rt = sen_get_attr_cmdif_os08b10(id, (CTL_SENDRV_GET_ATTR_CMDIF_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_ATTR_IF:
		rt = sen_get_attr_if_os08b10((CTL_SENDRV_GET_ATTR_IF_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_FPS:
		sen_get_fps_os08b10(id, (CTL_SENDRV_GET_FPS_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_SPEED:
		sen_get_speed_os08b10(id, (CTL_SENDRV_GET_SPEED_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_MODE_BASIC:
		sen_get_mode_basic_os08b10((CTL_SENDRV_GET_MODE_BASIC_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_MODE_MIPI:
		sen_get_mode_mipi_os08b10((CTL_SENDRV_GET_MODE_MIPI_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_MODESEL:
		sen_get_modesel_os08b10((CTL_SENDRV_GET_MODESEL_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_MODE_ROWTIME:
		sen_get_rowtime_os08b10(id, (CTL_SENDRV_GET_MODE_ROWTIME_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_FLIP_TYPE:
		rt = sen_get_flip_os08b10(id, (CTL_SEN_FLIP *)(data));
		break;
	case CTL_SENDRV_CFGID_USER_DEFINE2:
		sen_get_min_expt_os08b10(id, data);
		break;

	default:
		rt = E_NOSPT;
	}
	return rt;
}

static UINT32 sen_calc_chgmode_vd_os08b10(CTL_SEN_ID id, UINT32 fps)
{
	UINT32 sensor_vd;

	if (1 > fps) {
		DBG_ERR("sensor fps can not small than (%d),change to dft sensor fps (%d) \r\n", fps, mode_basic_param[cur_sen_mode[id]].dft_fps);
		fps = mode_basic_param[cur_sen_mode[id]].dft_fps;
	}
	sensor_vd = (mode_basic_param[cur_sen_mode[id]].signal_info.vd_period) * (mode_basic_param[cur_sen_mode[id]].dft_fps) / fps;

	sen_set_chgmode_fps_os08b10(id, fps);
	sen_set_cur_fps_os08b10(id, fps);

	if (sensor_vd > MAX_VD_PERIOD) {
		DBG_ERR("sensor vd out of sensor driver range (%d) \r\n", sensor_vd);
		sensor_vd = MAX_VD_PERIOD;
		fps = (mode_basic_param[cur_sen_mode[id]].signal_info.vd_period) * (mode_basic_param[cur_sen_mode[id]].dft_fps) / sensor_vd;
		sen_set_chgmode_fps_os08b10(id, fps);
		sen_set_cur_fps_os08b10(id, fps);
	}

	if(sensor_vd < (mode_basic_param[cur_sen_mode[id]].signal_info.vd_period)) {
		DBG_ERR("sensor vd out of sensor driver range (%d) \r\n", sensor_vd);
		sensor_vd = mode_basic_param[cur_sen_mode[id]].signal_info.vd_period;
		sen_set_chgmode_fps_os08b10(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
		sen_set_cur_fps_os08b10(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
	}

	return sensor_vd;
}

static UINT32 sen_calc_exp_vd_os08b10(CTL_SEN_ID id, UINT32 fps)
{
	UINT32 sensor_vd;

	if (1 > fps) {
		DBG_ERR("sensor fps can not small than (%d),change to dft sensor fps (%d) \r\n", fps, mode_basic_param[cur_sen_mode[id]].dft_fps);
		fps = mode_basic_param[cur_sen_mode[id]].dft_fps;
	}
	sensor_vd = (mode_basic_param[cur_sen_mode[id]].signal_info.vd_period) * (mode_basic_param[cur_sen_mode[id]].dft_fps) / fps;

	if (sensor_vd > MAX_VD_PERIOD) {
		DBG_ERR("sensor vd out of sensor driver range (%d) \r\n", sensor_vd);
		sensor_vd = MAX_VD_PERIOD;

	}

	if(sensor_vd < (mode_basic_param[cur_sen_mode[id]].signal_info.vd_period)) {
		DBG_ERR("sensor vd out of sensor driver range (%d) \r\n", sensor_vd);
		sensor_vd = mode_basic_param[cur_sen_mode[id]].signal_info.vd_period;
	}

	return sensor_vd;
}

static void sen_set_gain_os08b10(CTL_SEN_ID id, void *param)
{
	ISP_SENSOR_CTRL *sensor_ctrl = (ISP_SENSOR_CTRL *)param;
	UINT32 data1[ISP_SEN_MFRAME_MAX_NUM] = {0};
	UINT32 data2[ISP_SEN_MFRAME_MAX_NUM] = {0};
	UINT32 frame_cnt, total_frame;
	CTL_SEN_CMD cmd;
	UINT32 a_gain = 0, d_gain = 0;
	UINT32 temp_gain[ISP_SEN_MFRAME_MAX_NUM] = {0};	
	static UINT32 last_hcg_reg1[CTL_SEN_ID_MAX] = {0};
	UINT32 hcg_reg1 = 0, hcg_change_flag = 0;
	ER rt = E_OK;

	sensor_ctrl_last[id].gain_ratio[0] = sensor_ctrl->gain_ratio[0];
	sensor_ctrl_last[id].gain_ratio[1] = sensor_ctrl->gain_ratio[1];

	// Calculate sensor gain
	if (mode_basic_param[cur_sen_mode[id]].frame_num == 0) {
		DBG_WRN("total_frame = 0, force to 1 \r\n");
		total_frame = 1;
	} else {
		total_frame = mode_basic_param[cur_sen_mode[id]].frame_num;
	}
	
	cmd = sen_set_cmd_info_os08b10(0x376C, 1, 0x0, 0x0);
	rt |= sen_read_reg_os08b10(id, &cmd);
	last_hcg_reg1[id] = cmd.data[0];

	for (frame_cnt = 0; frame_cnt < total_frame; frame_cnt++) {
		if (100 <= (compensation_ratio[id][frame_cnt])) {
			sensor_ctrl->gain_ratio[frame_cnt] = (sensor_ctrl->gain_ratio[frame_cnt]) * (compensation_ratio[id][frame_cnt]) / 100;
		}		
		if (sensor_ctrl->gain_ratio[frame_cnt] < (mode_basic_param[cur_sen_mode[id]].gain.min)) {
			sensor_ctrl->gain_ratio[frame_cnt] = (mode_basic_param[cur_sen_mode[id]].gain.min);
		} else if (sensor_ctrl->gain_ratio[frame_cnt] > (mode_basic_param[cur_sen_mode[id]].gain.max)) {
			sensor_ctrl->gain_ratio[frame_cnt] = (mode_basic_param[cur_sen_mode[id]].gain.max);
		}
		
		temp_gain[frame_cnt] = sensor_ctrl->gain_ratio[frame_cnt];

		if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_LINEAR) {
			if ((HCG_ENABLE) && (4000 <= (sensor_ctrl->gain_ratio[frame_cnt]))) {
				temp_gain[frame_cnt] /= 4;
				hcg_change_flag = 1;
			}
		}

		if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
			if ((HCG_ENABLE) && (4000 <= (sensor_ctrl->gain_ratio[0])) && (4000 <= (sensor_ctrl->gain_ratio[1]))) {
				temp_gain[frame_cnt] /= 4;
				hcg_change_flag = 1;
			}
		}

		if((temp_gain[frame_cnt]) <= 15500) {
			a_gain = (temp_gain[frame_cnt]) * 16 / 1000;
			d_gain = 0x400;
		} else {
			d_gain = (temp_gain[frame_cnt]) * 1024 / 15500;
			a_gain = 0xF8;
		}

		if (0x3FFF < d_gain) { 
			d_gain = 0x3FFF;
		}
		data1[frame_cnt] = a_gain;
		data2[frame_cnt] = d_gain;
	}

	if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_LINEAR) {
		if (hcg_change_flag) {
			hcg_reg1 = 0x02;
		} else {
			hcg_reg1 = 0x12;
		}
  	
		cmd = sen_set_cmd_info_os08b10(0x3208, 1, 0x00, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x3508, 1, (data1[0] >> 4) & 0x1F, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x3509, 1, (data1[0] & 0x0F) << 4, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x350A, 1, (data2[0] >> 10) & 0x0F, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x350B, 1, (data2[0] >> 2) & 0xFF, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x350C, 1, (data2[0] & 0x03) << 6, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x3208, 1, 0x10, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
  	
		cmd = sen_set_cmd_info_os08b10(0x3208, 1, 0x01, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x376c, 1, hcg_reg1, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x3208, 1, 0x11, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
   	
		if ((last_hcg_reg1[id] != hcg_reg1)) {
			cmd = sen_set_cmd_info_os08b10(0x320D, 1, 0x81, 0x0);
			rt |= sen_write_reg_os08b10(id, &cmd);	
		}
  	
		cmd = sen_set_cmd_info_os08b10(0x3209, 1, 0x01, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);	
		cmd = sen_set_cmd_info_os08b10(0x320A, 1, 0x01, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x320E, 1, 0xA0, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);

	}	else if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
		if (hcg_change_flag) {
			hcg_reg1 = 0x02;
		} else {
			hcg_reg1 = 0x32;
		}

		cmd = sen_set_cmd_info_os08b10(0x3208, 1, 0x00, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x3508, 1, (data1[0] >> 4) & 0x1F, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x3509, 1, (data1[0] & 0x0F) << 4, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
 		cmd = sen_set_cmd_info_os08b10(0x3548, 1, (data1[1] >> 4) & 0x1F, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x3549, 1, (data1[1] & 0x0F) << 4, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x350A, 1, (data2[0] >> 10) & 0x0F, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x350B, 1, (data2[0] >> 2) & 0xFF, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x350C, 1, (data2[0] & 0x03) << 6, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x354A, 1, (data2[1] >> 10) & 0x0F, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x354B, 1, (data2[1] >> 2) & 0xFF, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x354C, 1, (data2[1] & 0x03) << 6, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);		
		cmd = sen_set_cmd_info_os08b10(0x3208, 1, 0x10, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);

		cmd = sen_set_cmd_info_os08b10(0x3208, 1, 0x01, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);	
		cmd = sen_set_cmd_info_os08b10(0x376c, 1, hcg_reg1, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);	
		cmd = sen_set_cmd_info_os08b10(0x3208, 1, 0x11, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);

		if (last_hcg_reg1[id] != hcg_reg1) {
			cmd = sen_set_cmd_info_os08b10(0x320D, 1, 0x81, 0x0);
			rt |= sen_write_reg_os08b10(id, &cmd);	
		}

		cmd = sen_set_cmd_info_os08b10(0x3209, 1, 0x01, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);	
		cmd = sen_set_cmd_info_os08b10(0x320A, 1, 0x01, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x320E, 1, 0xA0, 0x0);
		rt |= sen_write_reg_os08b10(id, &cmd);			
	}

	if (rt != E_OK) {
		DBG_ERR("write register error %d \r\n", (INT)rt);
	}
}

static void sen_set_expt_os08b10(CTL_SEN_ID id, void *param)
{
	ISP_SENSOR_CTRL *sensor_ctrl = (ISP_SENSOR_CTRL *)param;
	UINT32 line[ISP_SEN_MFRAME_MAX_NUM];
	UINT32 frame_cnt, total_frame;
	CTL_SEN_CMD cmd;
	UINT32 expt_time = 0, sensor_vd = 0, chgmode_fps = 0, cur_fps = 0, clac_fps = 0, t_row = 0;
	UINT32 temp_line[ISP_SEN_MFRAME_MAX_NUM] = { 0 };
	ER rt = E_OK;

	sensor_ctrl_last[id].exp_time[0] = sensor_ctrl->exp_time[0];
	sensor_ctrl_last[id].exp_time[1] = sensor_ctrl->exp_time[1];

	if (mode_basic_param[cur_sen_mode[id]].frame_num == 0) {
		DBG_WRN("total_frame = 0, force to 1 \r\n");
		total_frame = 1;
	} else {
		total_frame = mode_basic_param[cur_sen_mode[id]].frame_num;
	}

	// Calculate exposure line
	for (frame_cnt = 0; frame_cnt < total_frame; frame_cnt++) {
		// Calculates the exposure setting
		t_row = 2 * sen_calc_rowtime_os08b10(id, cur_sen_mode[id]);
		if (0 == t_row) {
			DBG_WRN("t_row  = 0, must >= 1 \r\n");
			t_row = 1;
		}
		line[frame_cnt] = (sensor_ctrl->exp_time[frame_cnt]) * 10 / t_row;

		// Limit minimun exposure line
		if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_STAGGER_HDR) {		
			if (line[frame_cnt] < MIN_HDR_EXPOSURE_LINE) {
				line[frame_cnt] = MIN_HDR_EXPOSURE_LINE;
			}
		} else {
			if (line[frame_cnt] < MIN_EXPOSURE_LINE) {
				line[frame_cnt] = MIN_EXPOSURE_LINE;
			}
		}
	}

	// Write exposure line
	// Get fps
	chgmode_fps = sen_get_chgmode_fps_os08b10(id);

	// Calculate exposure time
	t_row = 2 * sen_calc_rowtime_os08b10(id, cur_sen_mode[id]);
	if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
		expt_time = (line[0] + line[1])  * t_row / 10;
		temp_line[0] = line[0];
		temp_line[1] = line[1];
	} else {
		expt_time = (line[0]) * t_row / 10;		
		temp_line[0] = line[0];
	}

	// Calculate fps
	if (0 == expt_time) {
		DBG_WRN("expt_time  = 0, must >= 1 \r\n");		
		expt_time = 1;
	}
	clac_fps = 100000000 / expt_time;

	cur_fps = (clac_fps < chgmode_fps) ? clac_fps : chgmode_fps;
	sen_set_cur_fps_os08b10(id, cur_fps);

	// Calculate new vd
	sensor_vd = sen_calc_exp_vd_os08b10(id, cur_fps);

	//Check max vts
	if (sensor_vd > MAX_VD_PERIOD) {
		DBG_ERR("max vts overflow\r\n");
		sensor_vd = MAX_VD_PERIOD;
	}

	cmd = sen_set_cmd_info_os08b10(0x380E, 1, (sensor_vd >> 8) & 0xFF, 0);
	rt |= sen_write_reg_os08b10(id, &cmd);

	cmd = sen_set_cmd_info_os08b10(0x380F, 1, sensor_vd & 0xFF, 0);
	rt |= sen_write_reg_os08b10(id, &cmd);

	if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_LINEAR) {
		//Check max exp line reg
		if (line[0] > MAX_EXPOSURE_LINE) {
			line[0] = MAX_EXPOSURE_LINE;
		}

		//Check max exp line
		if (line[0] > (sensor_vd - NON_EXPOSURE_LINE)) {
			line[0] = sensor_vd - NON_EXPOSURE_LINE;
		}
		compensation_ratio[id][0] = 100 * temp_line[0] / line[0];

		cmd = sen_set_cmd_info_os08b10(0x3501, 1, (line[0] >> 8) & 0xFF, 0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x3502, 1, line[0] & 0xFF , 0);
		rt |= sen_write_reg_os08b10(id, &cmd);
	} else if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
		//Check max exp line reg
		if (line[0] > MAX_EXPOSURE_LINE) {
			line[0] = MAX_EXPOSURE_LINE;
		}
		//Check max exp line reg
		if (line[1] > MAX_EXPOSURE_LINE) {
			line[1] = MAX_EXPOSURE_LINE;
		}

		if (line[0] > (sensor_vd - line[1] - NON_HDR_SHORT_EXPOSURE_LINE - NON_HDR_LONG_EXPOSURE_LINE)) {
			line[0] = sensor_vd - line[1] - NON_HDR_SHORT_EXPOSURE_LINE - NON_HDR_LONG_EXPOSURE_LINE;
		}
		compensation_ratio[id][0] = 100 * temp_line[0] / line[0];		
		compensation_ratio[id][1] = 100 * temp_line[1] / line[1];
	
		cmd = sen_set_cmd_info_os08b10(0x3501, 1, (line[0] >> 8) & 0xFF, 0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x3502, 1, line[0] & 0xFF , 0);
		rt |= sen_write_reg_os08b10(id, &cmd);

		cmd = sen_set_cmd_info_os08b10(0x3541, 1, (line[1] >> 8) & 0xFF, 0);
		rt |= sen_write_reg_os08b10(id, &cmd);
		cmd = sen_set_cmd_info_os08b10(0x3542, 1, line[1] & 0xFF , 0);
		rt |= sen_write_reg_os08b10(id, &cmd);
	}

	if (rt != E_OK) {
		DBG_ERR("write register error %d \r\n", (INT)rt);
	}
}

static void sen_set_preset_os08b10(CTL_SEN_ID id, ISP_SENSOR_PRESET_CTRL *ctrl)
{
	memcpy(&preset_ctrl[id], ctrl, sizeof(ISP_SENSOR_PRESET_CTRL));
}

static void sen_set_flip_os08b10(CTL_SEN_ID id, CTL_SEN_FLIP *flip)
{
	CTL_SEN_CMD cmd;
	ER rt = E_OK;
	UINT32 temp = 0;

	cmd = sen_set_cmd_info_os08b10(0x3820, 1, 0x0, 0x0);
	rt |= sen_read_reg_os08b10(id, &cmd);

	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_MIRROR) {
		if (*flip & CTL_SEN_FLIP_H) {
				cmd.data[0] |= (0x02);
		} else {
				cmd.data[0] &= (~0x02);				
		}
	} else {
		DBG_WRN("no support mirror \r\n");
	}

	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_FLIP) {
		if (*flip & CTL_SEN_FLIP_V) {
			cmd.data[0] |= 0x04;
		} else {
			cmd.data[0] &= (~0x04);				
		}
	} else {
		DBG_WRN("no support flip \r\n");
	}
	
	temp = cmd.data[0];

	cmd = sen_set_cmd_info_os08b10(0x0100, 1, 0x00, 0x0);
	rt |= sen_write_reg_os08b10(id, &cmd);
	
	cmd = sen_set_cmd_info_os08b10(0x3820, 1, temp, 0x0);
	rt |= sen_write_reg_os08b10(id, &cmd);

	cmd = sen_set_cmd_info_os08b10(0x0100, 1, 0x01, 0x0);
	rt |= sen_write_reg_os08b10(id, &cmd);

	if (rt != E_OK) {
		DBG_ERR("write register error %d \r\n", (INT)rt);
	}
}

static ER sen_get_flip_os08b10(CTL_SEN_ID id, CTL_SEN_FLIP *flip)
{
	CTL_SEN_CMD cmd;
	ER rt = E_OK;

	*flip = CTL_SEN_FLIP_NONE;

	cmd = sen_set_cmd_info_os08b10(0x3820, 1, 0x0, 0x0);
	rt |= sen_read_reg_os08b10(id, &cmd);

	*flip = CTL_SEN_FLIP_NONE;
	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_MIRROR) {
		if (cmd.data[0] & 0x02) {
			*flip |= CTL_SEN_FLIP_H;
		}
	} else {
		DBG_WRN("no support mirror \r\n");
	}

	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_FLIP) {
		if (cmd.data[0] & 0x04) {
			*flip |= CTL_SEN_FLIP_V;
		}
	} else {
		DBG_WRN("no support flip \r\n");
	}

	return rt;
}

#if defined(__FREERTOS)
void sen_get_gain_os08b10(CTL_SEN_ID id, void *param)
#else
static void sen_get_gain_os08b10(CTL_SEN_ID id, void *param)
#endif
{
	ISP_SENSOR_CTRL *sensor_ctrl = (ISP_SENSOR_CTRL *)param;

	sensor_ctrl->gain_ratio[0] = sensor_ctrl_last[id].gain_ratio[0];
	sensor_ctrl->gain_ratio[1] = sensor_ctrl_last[id].gain_ratio[1];
}

#if defined(__FREERTOS)
void sen_get_expt_os08b10(CTL_SEN_ID id, void *param)
#else
static void sen_get_expt_os08b10(CTL_SEN_ID id, void *param)
#endif
{
	ISP_SENSOR_CTRL *sensor_ctrl = (ISP_SENSOR_CTRL *)param;

	sensor_ctrl->exp_time[0] = sensor_ctrl_last[id].exp_time[0];
	sensor_ctrl->exp_time[1] = sensor_ctrl_last[id].exp_time[1];
}

static void sen_get_min_expt_os08b10(CTL_SEN_ID id, void *param)
{
	UINT32 *min_exp_time = (UINT32 *)param;
	UINT32 t_row;

	t_row = 2 * sen_calc_rowtime_os08b10(id, cur_sen_mode[id]);

	if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_LINEAR) {
	*min_exp_time = t_row * MIN_EXPOSURE_LINE / 10 + 1; // if linear and shdr mode is different,the value need to separate.
	} else if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
	*min_exp_time = t_row * MIN_HDR_EXPOSURE_LINE / 10 + 1; // if linear and shdr mode is different,the value need to separate.
	}
}

static void sen_get_mode_basic_os08b10(CTL_SENDRV_GET_MODE_BASIC_PARAM *mode_basic)
{
	UINT32 mode = mode_basic->mode;

	if (mode >= SEN_MAX_MODE) {
		mode = 0;
	}
	memcpy(mode_basic, &mode_basic_param[mode], sizeof(CTL_SENDRV_GET_MODE_BASIC_PARAM));
}

static void sen_get_attr_basic_os08b10(CTL_SENDRV_GET_ATTR_BASIC_PARAM *data)
{
	memcpy(data, &basic_param, sizeof(CTL_SENDRV_GET_ATTR_BASIC_PARAM));
}

static void sen_get_attr_signal_os08b10(CTL_SENDRV_GET_ATTR_SIGNAL_PARAM *data)
{
	memcpy(data, &signal_param, sizeof(CTL_SENDRV_GET_ATTR_SIGNAL_PARAM));
}

static ER sen_get_attr_cmdif_os08b10(CTL_SEN_ID id, CTL_SENDRV_GET_ATTR_CMDIF_PARAM *data)
{
	data->type = CTL_SEN_CMDIF_TYPE_I2C;
	memcpy(&data->info, &i2c, sizeof(CTL_SENDRV_I2C));
	data->info.i2c.ch = sen_i2c[id].id;
	data->info.i2c.w_addr_info[0].w_addr = sen_i2c[id].addr;
	data->info.i2c.cur_w_addr_info.w_addr_sel = data->info.i2c.w_addr_info[0].w_addr_sel;
	data->info.i2c.cur_w_addr_info.w_addr = data->info.i2c.w_addr_info[0].w_addr;
	return E_OK;
}

static ER sen_get_attr_if_os08b10(CTL_SENDRV_GET_ATTR_IF_PARAM *data)
{
	#if 1
	if (data->type == CTL_SEN_IF_TYPE_MIPI) {
		return E_OK;
	}
	return E_NOSPT;
	#else
	if (data->type == CTL_SEN_IF_TYPE_MIPI) {
		memcpy(&data->info.mipi, &mipi, sizeof(CTL_SENDRV_MIPI));
		return E_OK;
	}
	return E_NOSPT;
	#endif
}

static void sen_get_fps_os08b10(CTL_SEN_ID id, CTL_SENDRV_GET_FPS_PARAM *data)
{
	data->cur_fps = sen_get_cur_fps_os08b10(id);
	data->chg_fps = sen_get_chgmode_fps_os08b10(id);
}

static void sen_get_speed_os08b10(CTL_SEN_ID id, CTL_SENDRV_GET_SPEED_PARAM *data)
{
	UINT32 mode = data->mode;

	if (mode >= SEN_MAX_MODE) {
		mode = 0;
	}
	memcpy(data, &speed_param[mode], sizeof(CTL_SENDRV_GET_SPEED_PARAM));

	if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK ) {
		data->mclk_src = CTL_SEN_SIEMCLK_SRC_MCLK;
	} else if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK2) {
		data->mclk_src = CTL_SEN_SIEMCLK_SRC_MCLK2;
	} else if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK3) {
		data->mclk_src = CTL_SEN_SIEMCLK_SRC_MCLK3;
	} else if (sen_power[id].mclk == CTL_SEN_IGNORE) {
		data->mclk_src = CTL_SEN_SIEMCLK_SRC_IGNORE;
	} else {
		DBG_ERR("mclk source is fail \r\n");
	}
}

static void sen_get_mode_mipi_os08b10(CTL_SENDRV_GET_MODE_MIPI_PARAM *data)
{
	UINT32 mode = data->mode;

	if (mode >= SEN_MAX_MODE) {
		mode = 0;
	}
	memcpy(data, &mipi_param[mode], sizeof(CTL_SENDRV_GET_MODE_MIPI_PARAM));
}

static void sen_get_modesel_os08b10(CTL_SENDRV_GET_MODESEL_PARAM *data)
{
	if (data->if_type != CTL_SEN_IF_TYPE_MIPI) {
		DBG_ERR("if_type %d N.S. \r\n", data->if_type);
		return;
	}

	if (data->data_fmt != CTL_SEN_DATA_FMT_RGB) {
		DBG_ERR("data_fmt %d N.S. \r\n", data->data_fmt);
		return;
	}
	
	if (data->frame_num == 1) {
		if ((data->size.w <= 3840) && (data->size.h <= 2160)) {
			if (data->frame_rate <= 2500) {
				data->mode = CTL_SEN_MODE_3;
				return;
			} else if (data->frame_rate <= 3000) {
				data->mode = CTL_SEN_MODE_1;
				return;
			}
		}
	} else if (data->frame_num == 2) {
		if ((data->size.w <= 3840) && (data->size.h <= 2160)) {
			if (data->frame_rate <= 2500) {
				data->mode = CTL_SEN_MODE_2;
				return;
			}
		}		
	}

	DBG_ERR("fail (frame_rate%d,size%d*%d,if_type%d,data_fmt%d,frame_num%d) \r\n"
			, data->frame_rate, data->size.w, data->size.h, data->if_type, data->data_fmt, data->frame_num);
	data->mode = CTL_SEN_MODE_1;
}

static UINT32 sen_calc_rowtime_step_os08b10(CTL_SEN_ID id, CTL_SEN_MODE mode)
{
	UINT32 div_step = 0;

	if (mode >= SEN_MAX_MODE) {
		mode = cur_sen_mode[id];
	}

	if ((mode_basic_param[mode].mode_type == CTL_SEN_MODE_STAGGER_HDR) || (mode_basic_param[mode].mode_type == CTL_SEN_MODE_LINEAR))	{
		div_step = 1;
	}

	return div_step;
}

static UINT32 sen_calc_rowtime_os08b10(CTL_SEN_ID id, CTL_SEN_MODE mode)
{
	UINT32 row_time = 0;

	if (mode >= SEN_MAX_MODE) {
		mode = cur_sen_mode[id];
	}

	//Precision * 10
	if (mode_basic_param[mode].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
		row_time = 10 * (mode_basic_param[mode].signal_info.hd_period) / ((speed_param[mode].pclk) / 2 / 1000000);
	} else {	
		row_time = 10 * (mode_basic_param[mode].signal_info.hd_period) / ((speed_param[mode].pclk) / 1000000) / 2;
	}

	return row_time;
}

static void sen_get_rowtime_os08b10(CTL_SEN_ID id, CTL_SENDRV_GET_MODE_ROWTIME_PARAM *data)
{
	data->row_time_step = sen_calc_rowtime_step_os08b10(id, data->mode);
	data->row_time = sen_calc_rowtime_os08b10(id, data->mode) * (data->row_time_step);
}

static void sen_set_cur_fps_os08b10(CTL_SEN_ID id, UINT32 fps)
{
	cur_fps[id] = fps;
}

static UINT32 sen_get_cur_fps_os08b10(CTL_SEN_ID id)
{
	return cur_fps[id];
}

static void sen_set_chgmode_fps_os08b10(CTL_SEN_ID id, UINT32 fps)
{
	chgmode_fps[id] = fps;
}

static UINT32 sen_get_chgmode_fps_os08b10(CTL_SEN_ID id)
{
	return chgmode_fps[id];
}

#if defined(__FREERTOS)
void sen_get_i2c_id_os08b10(CTL_SEN_ID id, UINT32 *i2c_id)
{
	*i2c_id = sen_i2c[id].id;
}

void sen_get_i2c_addr_os08b10(CTL_SEN_ID id, UINT32 *i2c_addr)
{
	*i2c_addr = sen_i2c[id].addr;
}

int sen_init_os08b10(SENSOR_DTSI_INFO *info)
{
	CTL_SEN_REG_OBJ reg_obj;
	CHAR node_path[64];
	CHAR compatible[64];
	UINT32 id;
	ER rt = E_OK;

	for (id = 0; id < CTL_SEN_ID_MAX; id++ ) {
		is_fastboot[id] = 0;
		fastboot_i2c_id[id] = 0xFFFFFFFF;
		fastboot_i2c_addr[id] = 0x0;
	}

	sprintf(compatible, "nvt,sen_os08b10");
	if (sen_common_check_compatible(compatible)) {
		DBG_DUMP("compatible valid, using peri-dev.dtsi \r\n");
		sen_common_load_cfg_preset_compatible(compatible, &sen_preset);
		sen_common_load_cfg_direction_compatible(compatible, &sen_direction);
		sen_common_load_cfg_power_compatible(compatible, &sen_power);
		sen_common_load_cfg_i2c_compatible(compatible, &sen_i2c);
	} else if (info->addr != NULL) {
		DBG_DUMP("compatible not valid, using sensor.dtsi \r\n");
		sprintf(node_path, "/sensor/sen_cfg/sen_os08b10");
		sen_common_load_cfg_map(info->addr, node_path, &sen_map);
		sen_common_load_cfg_preset(info->addr, node_path, &sen_preset);
		sen_common_load_cfg_direction(info->addr, node_path, &sen_direction);
		sen_common_load_cfg_power(info->addr, node_path, &sen_power);
		sen_common_load_cfg_i2c(info->addr, node_path, &sen_i2c);
	} else {
		DBG_WRN("DTSI addr is NULL \r\n");
	}

	memset((void *)(&reg_obj), 0, sizeof(CTL_SEN_REG_OBJ));
	reg_obj.pwr_ctrl = sen_pwr_ctrl_os08b10;
	reg_obj.det_plug_in = NULL;
	reg_obj.drv_tab = sen_get_drv_tab_os08b10();
	rt = ctl_sen_reg_sendrv("nvt_sen_os08b10", &reg_obj);
	if (rt != E_OK) {
		DBG_WRN("register sensor driver fail \r\n");
	}

	return rt;
}

void sen_exit_os08b10(void)
{
	ctl_sen_unreg_sendrv("nvt_sen_os08b10");
}

#else
static int __init sen_init_os08b10(void)
{
	INT8 cfg_path[MAX_PATH_NAME_LENGTH+1] = { '\0' };
	CFG_FILE_FMT *pcfg_file;
	CTL_SEN_REG_OBJ reg_obj;
	UINT32 id;
	ER rt = E_OK;

	for (id = 0; id < ISP_BUILTIN_ID_MAX_NUM; id++ ) {
		is_fastboot[id] = kdrv_builtin_is_fastboot();
		fastboot_i2c_id[id] = isp_builtin_get_i2c_id(id);
		fastboot_i2c_addr[id] = isp_builtin_get_i2c_addr(id);
	}

	// Parsing cfc file if exist
	if ((strstr(sen_cfg_path, "null")) || (strstr(sen_cfg_path, "NULL"))) {
		DBG_WRN("cfg file no exist \r\n");
		cfg_path[0] = '\0';
	} else {
		if ((sen_cfg_path != NULL) && (strlen(sen_cfg_path) <= MAX_PATH_NAME_LENGTH)) {
			strncpy((char *)cfg_path, sen_cfg_path, MAX_PATH_NAME_LENGTH);
		}

		if ((pcfg_file = sen_common_open_cfg(cfg_path)) != NULL) {
			DBG_MSG("load %s success \r\n", sen_cfg_path);
			sen_common_load_cfg_map(pcfg_file, &sen_map);
			sen_common_load_cfg_preset(pcfg_file, &sen_preset);
			sen_common_load_cfg_direction(pcfg_file, &sen_direction);
			sen_common_load_cfg_power(pcfg_file, &sen_power);
			sen_common_load_cfg_i2c(pcfg_file, &sen_i2c);
			sen_common_close_cfg(pcfg_file);
		} else {
			DBG_WRN("load cfg fail \r\n");
		}
	}

	memset((void *)(&reg_obj), 0, sizeof(CTL_SEN_REG_OBJ));
	reg_obj.pwr_ctrl = sen_pwr_ctrl_os08b10;
	reg_obj.det_plug_in = NULL;
	reg_obj.drv_tab = sen_get_drv_tab_os08b10();
	rt = ctl_sen_reg_sendrv("nvt_sen_os08b10", &reg_obj);
	if (rt != E_OK) {
		DBG_WRN("register sensor driver fail \r\n");
	}

	return rt;
}

static void __exit sen_exit_os08b10(void)
{
	ctl_sen_unreg_sendrv("nvt_sen_os08b10");
}

module_init(sen_init_os08b10);
module_exit(sen_exit_os08b10);

MODULE_AUTHOR("Novatek Corp.");
MODULE_DESCRIPTION(SEN_OS08B10_MODULE_NAME);
MODULE_LICENSE("GPL");
#endif

