#ifndef __MODULE_PROC_H_
#define __MODULE_PROC_H__
#include "gyro_comm_main.h"

int nvt_gyro_comm_proc_init(GYRO_COMM_INFO *pdrv_info);
int nvt_gyro_comm_proc_remove(GYRO_COMM_INFO *pdrv_info);


#endif
