/**
    General Sensor API for project layer

    Sample module detailed description.

    @file       n4_param.c
    @ingroup    Predefined_group_name
    @note       Nothing (or anything need to be mentioned).

    Copyright   Novatek Microelectronics Corp. 2011.  All rights reserved.
*/
#if defined(__FREERTOS)
#include <string.h>
#else
#endif

#include "ad_std_drv_param_int.h"
#include "ad.h"
#include "ad_dbg_int.h"

