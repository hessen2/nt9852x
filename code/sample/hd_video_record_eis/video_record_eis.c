/**
	@brief Sample code of video record with eis.\n

	@file video_record_eis.c

	@author Janice Huang

	@ingroup mhdal


	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "hdal.h"
#include "hd_debug.h"
#include "vendor_videoprocess.h"
#include "vendor_videocapture.h"
#include "vendor_eis.h"
#include "vendor_vpe.h"
#include <pd_shm.h>
#include "vendor_isp.h"
#include <math.h>
// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#include <sys/ipc.h>
#include <sys/shm.h>
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(hd_video_record_with_vprc_eis, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

#define DEBUG_MENU 		1

#define CHKPNT			printf("\033[37mCHK: %s, %s: %d\033[0m\r\n",__FILE__,__func__,__LINE__)
#define DBGH(x)			printf("\033[0;35m%s=0x%08X\033[0m\r\n", #x, x)
#define DBGD(x)			printf("\033[0;35m%s=%d\033[0m\r\n", #x, x)
#define BIND_BSMUXER    1
#define GYRO_FILE_MAX  (64*1024*1024)
///////////////////////////////////////////////////////////////////////////////

//header
#define DBGINFO_BUFSIZE()	(0x200)

//RAW
#define VDO_RAW_BUFSIZE(w, h, pxlfmt)   (ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NRX: RAW compress: Only support 12bit mode
#define RAW_COMPRESS_RATIO 59
#define VDO_NRX_BUFSIZE(w, h)           (ALIGN_CEIL_4(ALIGN_CEIL_64(w) * 12 / 8 * RAW_COMPRESS_RATIO / 100 * (h)))
//CA for AWB
#define VDO_CA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 3) << 1)
//LA for AE
#define VDO_LA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 1) << 1)

//YUV
#define VDO_YUV_BUFSIZE(w, h, pxlfmt)	(ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NVX: YUV compress
#define YUV_COMPRESS_RATIO 75
#define VDO_NVX_BUFSIZE(w, h, pxlfmt)	(VDO_YUV_BUFSIZE(w, h, pxlfmt) * YUV_COMPRESS_RATIO / 100)

///////////////////////////////////////////////////////////////////////////////

#define SEN_OUT_FMT		HD_VIDEO_PXLFMT_RAW12
#define CAP_OUT_FMT		HD_VIDEO_PXLFMT_NRX12
#define CA_WIN_NUM_W		32
#define CA_WIN_NUM_H		32
#define LA_WIN_NUM_W		32
#define LA_WIN_NUM_H		32
#define VA_WIN_NUM_W		16
#define VA_WIN_NUM_H		16
#define YOUT_WIN_NUM_W	128
#define YOUT_WIN_NUM_H	128
#define ETH_8BIT_SEL		0 //0: 2bit out, 1:8 bit out
#define ETH_OUT_SEL		1 //0: full, 1: subsample 1/2

#define SEN_SEL_IMX290   0   //2M_SHDR
#define SEN_SEL_IMX415   1   //8M_SHDR
#define OUT_NO_EIS       1   //for demo,eis on/off compare


#define VDO_SIZE_W_8M      3840
#define VDO_SIZE_H_8M      2160
#define VDO_BITRATE_8M          (16 * 1024 * 1024)
#define VDO_BITRATE_RTSP_8M     (4 * 1024 * 1024)

#define VDO_SIZE_W_2M	   1920
#define VDO_SIZE_H_2M	   1080
#define VDO_BITRATE_2M          (8 * 1024 * 1024)
#define VDO_BITRATE_RTSP_2M     (2 * 1024 * 1024)

static UINT32 VDO_SIZE_W=0;
static UINT32 VDO_SIZE_H=0;

#define SUB_VDO_SIZE_W	1280
#define SUB_VDO_SIZE_H	720

#define GYRO_DATA_NUM   16
#define GYRO_DATA_SIZE (4+4*GYRO_DATA_NUM*6)

#define EIS_PATH0_2DLUT_SIZE VENDOR_EIS_LUT_9x9
#define ISP_EFFECT_ID 0

static UINT32 g_enc_poll = 1;
static UINT32 g_write_file = 0;
static UINT32 sensor_sel = SEN_SEL_IMX290;
static UINT32 vdo_br     = (VDO_BITRATE_2M);
static UINT32 vdo_br_rtsp     = (VDO_BITRATE_RTSP_2M);

static UINT32 g_capbind = 0;  //0:D2D, 1:direct, 2: one-buf, 0xff: no-bind
static UINT32 g_vcap_blk_size = 0;
static UINT32 g_show_gyro = 0;
#define VPRC_PI 3.1415926
static char   *g_shm = NULL;
static int    g_shmid = 0;
static UINT32 g_fps = 30;

static double normalized_default_2dlut[2][9][9] =
{
	{//X
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9}
	},
	{//Y
		{0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1},
		{0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2},
		{0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3},
		{0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4},
		{0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5},
		{0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6},
		{0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7},
		{0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8},
		{0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9}
	}
};

static void gen_default_2dlut(UINT32 *p_lut2d_buf, INT32 width, INT32 height, INT32 lut_size)
{
	INT32 i, j;
	UINT32 line_data_num = ((lut_size+3)>>2)<<2;

	for (j = 0; j < lut_size; j++) {
		for (i = 0; i < lut_size; i++) {
			p_lut2d_buf[j*line_data_num + i] =
				((UINT32)round(normalized_default_2dlut[1][j][i]*(double)height*4) << 16) +
				(UINT32)round(normalized_default_2dlut[0][j][i]*(double)width*4);
		}
	}
}
static VPET_2DLUT_PARAM lut2d_path0 = {
	0,
	{
		EIS_PATH0_2DLUT_SIZE,
		{0}
	}
};
///////////////////////////////////////////////////////////////////////////////

void init_share_memory(void)
{
	int g_shmid = 0;
    key_t key;
	PD_SHM_INFO  *p_pd_shm;

    // Segment key.
    key = PD_SHM_KEY;
    // Create the segment.
    if( ( g_shmid = shmget( key, PD_SHMSZ, IPC_CREAT | 0666 ) ) < 0 ) {
        perror( "shmget" );
        exit(1);
    }
    // Attach the segment to the data space.
    if( ( g_shm = shmat( g_shmid, NULL, 0 ) ) == (char *)-1 ) {
        perror( "shmat" );
        exit(1);
    }
    // Initialization.
    memset(g_shm, 0, PD_SHMSZ );
	p_pd_shm = (PD_SHM_INFO  *)g_shm;
	p_pd_shm->enc_path[0].w = VDO_SIZE_W;
	p_pd_shm->enc_path[0].h = VDO_SIZE_H;
	p_pd_shm->enc_path[1].w = SUB_VDO_SIZE_W;
	p_pd_shm->enc_path[1].h = SUB_VDO_SIZE_H;
}

void exit_share_memory(void)
{
	if (g_shm) {
		shmdt(g_shm);
		shmctl(g_shmid, IPC_RMID, NULL);
	}
}


static HD_RESULT mem_init(void)
{
	HD_RESULT              ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};
    UINT32 id;

	// config common pool (cap)
	id = 0;
	mem_cfg.pool_info[id].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[id].blk_size = DBGINFO_BUFSIZE()+VDO_RAW_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, CAP_OUT_FMT)
														+VDO_CA_BUF_SIZE(CA_WIN_NUM_W, CA_WIN_NUM_H)
														+VDO_LA_BUF_SIZE(LA_WIN_NUM_W, LA_WIN_NUM_H);
	mem_cfg.pool_info[id].blk_size += GYRO_DATA_SIZE;
	mem_cfg.pool_info[id].blk_cnt = 3;
	mem_cfg.pool_info[id].ddr_id = DDR_ID0;

	// config common pool (pre-process)
	id++;
	mem_cfg.pool_info[id].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[id].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
	mem_cfg.pool_info[id].blk_size += (vendor_eis_buf_query(VENDOR_EIS_LUT_65x65) + GYRO_DATA_SIZE + sizeof(VENDOR_EIS_DBG_CTX));
	mem_cfg.pool_info[id].blk_cnt = 3;
	mem_cfg.pool_info[id].ddr_id = DDR_ID0;

	// config common pool (main)
	id++;
	mem_cfg.pool_info[id].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[id].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
	mem_cfg.pool_info[id].blk_cnt = 3;
	if (g_capbind == 1) {
		//direct
		mem_cfg.pool_info[id].blk_cnt += 2; //direct will pre-new 1
	}
	mem_cfg.pool_info[id].ddr_id = DDR_ID0;
#if OUT_NO_EIS
	// config common pool (sub)
	id++;
	mem_cfg.pool_info[id].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[id].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(SUB_VDO_SIZE_W, SUB_VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
	mem_cfg.pool_info[id].blk_cnt = 3;
	mem_cfg.pool_info[id].ddr_id = DDR_ID0;
#endif
	// config common pool (display)
	id ++;
	mem_cfg.pool_info[id].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[id].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
	mem_cfg.pool_info[id].blk_cnt = 3;
	mem_cfg.pool_info[id].ddr_id = DDR_ID0;

    // gyro data
	id ++;
	mem_cfg.pool_info[id].type = HD_COMMON_MEM_USER_DEFINIED_POOL;
	mem_cfg.pool_info[id].blk_size = GYRO_FILE_MAX;
	mem_cfg.pool_info[id].blk_cnt = 1;
	mem_cfg.pool_info[id].ddr_id = DDR_ID0;

	ret = hd_common_mem_init(&mem_cfg);

	g_vcap_blk_size = mem_cfg.pool_info[0].blk_size;

	return ret;
}

static HD_RESULT mem_exit(void)
{
	HD_RESULT ret = HD_OK;
	hd_common_mem_uninit();
	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT get_cap_caps(HD_PATH_ID video_cap_ctrl, HD_VIDEOCAP_SYSCAPS *p_video_cap_syscaps)
{
	HD_RESULT ret = HD_OK;
	hd_videocap_get(video_cap_ctrl, HD_VIDEOCAP_PARAM_SYSCAPS, p_video_cap_syscaps);
	return ret;
}

static HD_RESULT set_cap_cfg(HD_PATH_ID *p_video_cap_ctrl)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOCAP_DRV_CONFIG cap_cfg = {0};
	HD_PATH_ID video_cap_ctrl = 0;
	HD_VIDEOCAP_CTRL iq_ctl = {0};

	if (sensor_sel == SEN_SEL_IMX290) {
		snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, "nvt_sen_imx290");
		printf("Using nvt_sen_imx290\n");
	} else if (sensor_sel == SEN_SEL_IMX415) {
		snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, "nvt_sen_imx415");
		printf("Using nvt_sen_imx415\n");
	} else {
		// MIPI interface
		cap_cfg.sen_cfg.sen_dev.if_type = HD_COMMON_VIDEO_IN_MIPI_CSI;
		cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux = 0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
	}
	cap_cfg.sen_cfg.sen_dev.if_type = HD_COMMON_VIDEO_IN_MIPI_CSI;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.serial_if_pinmux = 0xf04;//PIN_MIPI_LVDS_CFG_CLK2 | PIN_MIPI_LVDS_CFG_DAT0|PIN_MIPI_LVDS_CFG_DAT1 | PIN_MIPI_LVDS_CFG_DAT2 | PIN_MIPI_LVDS_CFG_DAT3
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = 0x10;//PIN_I2C_CFG_CH2
	cap_cfg.sen_cfg.sen_dev.pin_cfg.clk_lane_sel = HD_VIDEOCAP_SEN_CLANE_SEL_CSI0_USE_C2;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[0] = 0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[1] = 1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[4] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[5] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[6] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[7] = HD_VIDEOCAP_SEN_IGNORE;
	ret = hd_videocap_open(0, HD_VIDEOCAP_0_CTRL, &video_cap_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_DRV_CONFIG, &cap_cfg);
	iq_ctl.func = HD_VIDEOCAP_FUNC_AE | HD_VIDEOCAP_FUNC_AWB;
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_CTRL, &iq_ctl);

	*p_video_cap_ctrl = video_cap_ctrl;
	return ret;
}

static HD_RESULT set_cap_param(HD_PATH_ID video_cap_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	{//select sensor mode, manually or automatically
		HD_VIDEOCAP_IN video_in_param = {0};

		video_in_param.sen_mode = HD_VIDEOCAP_SEN_MODE_AUTO; //auto select sensor mode by the parameter of HD_VIDEOCAP_PARAM_OUT
		video_in_param.frc = HD_VIDEO_FRC_RATIO(g_fps,1);
		video_in_param.dim.w = p_dim->w;
		video_in_param.dim.h = p_dim->h;
		video_in_param.pxlfmt = SEN_OUT_FMT;
		video_in_param.out_frame_num = HD_VIDEOCAP_SEN_FRAME_NUM_1;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN, &video_in_param);
		//printf("set_cap_param MODE=%d\r\n", ret);
		if (ret != HD_OK) {
			return ret;
		}
	}
	#if 1 //no crop, full frame
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_OFF;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_OUT_CROP, &video_crop_param);
		//printf("set_cap_param CROP NONE=%d\r\n", ret);
	}
	#else //HD_CROP_ON
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_ON;
		video_crop_param.win.rect.x = 0;
		video_crop_param.win.rect.y = 0;
		video_crop_param.win.rect.w = 1920/2;
		video_crop_param.win.rect.h= 1080/2;
		video_crop_param.align.w = 4;
		video_crop_param.align.h = 4;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_OUT_CROP, &video_crop_param);
		//printf("set_cap_param CROP ON=%d\r\n", ret);
	}
	#endif
	{
		HD_VIDEOCAP_OUT video_out_param = {0};

		//without setting dim for no scaling, using original sensor out size
		video_out_param.pxlfmt = CAP_OUT_FMT;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		if (g_capbind == 0xff) //no-bind mode
			video_out_param.depth = 1;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_OUT, &video_out_param);
		//printf("set_cap_param OUT=%d\r\n", ret);
	}
	{
		HD_VIDEOCAP_FUNC_CONFIG video_path_param = {0};

		video_path_param.out_func = 0;
		if (g_capbind == 1) //direct mode
			video_path_param.out_func = HD_VIDEOCAP_OUTFUNC_DIRECT;
		if (g_capbind == 2) //one-buf mode
			video_path_param.out_func = HD_VIDEOCAP_OUTFUNC_ONEBUF;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_FUNC_CONFIG, &video_path_param);
		//printf("set_cap_param PATH_CONFIG=0x%X\r\n", ret);
	}
	{
		VENDOR_VIDEOCAP_GYRO_INFO vcap_gyro_info = {0};

		vcap_gyro_info.en = TRUE;
		vcap_gyro_info.data_num = GYRO_DATA_NUM;
		ret = vendor_videocap_set(video_cap_path, VENDOR_VIDEOCAP_PARAM_GYRO_INFO, &vcap_gyro_info);
		//printf("set_cap_param OUT=%d\r\n", ret);
	}
	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_proc_cfg(HD_OUT_ID ctrl, HD_PATH_ID *p_video_proc_ctrl, HD_VIDEOPROC_PIPE pipe, HD_DIM* p_max_dim)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOPROC_DEV_CONFIG video_cfg_param = {0};
	HD_VIDEOPROC_CTRL video_ctrl_param = {0};
	HD_PATH_ID video_proc_ctrl = 0;

	ret = hd_videoproc_open(0, ctrl, &video_proc_ctrl); //open this for device control
	if (ret != HD_OK)
		return ret;

	if (p_max_dim != NULL ) {
		video_cfg_param.pipe = pipe;
		if (pipe == HD_VIDEOPROC_PIPE_RAWALL) {
			video_cfg_param.isp_id = 0;
		} else { //HD_VIDEOPROC_PIPE_VPE
			video_cfg_param.isp_id = ISP_EFFECT_ID;
		}
		video_cfg_param.ctrl_max.func = 0;
		video_cfg_param.in_max.func = 0;
		video_cfg_param.in_max.dim.w = p_max_dim->w;
		video_cfg_param.in_max.dim.h = p_max_dim->h;
		video_cfg_param.in_max.pxlfmt = (pipe == HD_VIDEOPROC_PIPE_VPE) ? HD_VIDEO_PXLFMT_YUV420 : CAP_OUT_FMT;
		video_cfg_param.in_max.frc = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_DEV_CONFIG, &video_cfg_param);
		if (ret != HD_OK) {
			return HD_ERR_NG;
		}
	}
	if (pipe == HD_VIDEOPROC_PIPE_RAWALL) {
		HD_VIDEOPROC_FUNC_CONFIG video_path_param = {0};
		BOOL eis_func = TRUE;

		video_path_param.in_func = 0;
		if (g_capbind == 1)
			video_path_param.in_func |= HD_VIDEOPROC_INFUNC_DIRECT; //direct NOTE: enable direct
		if (g_capbind == 2)
			video_path_param.in_func |= HD_VIDEOPROC_INFUNC_ONEBUF;
		ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_FUNC_CONFIG, &video_path_param);
		//printf("set_proc_param PATH_CONFIG=0x%X\r\n", ret);

		vendor_videoproc_set(video_proc_ctrl, VENDOR_VIDEOPROC_PARAM_EIS_FUNC, &eis_func);
	}

	video_ctrl_param.func = 0;
	ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_CTRL, &video_ctrl_param);
	*p_video_proc_ctrl = video_proc_ctrl;

	return ret;
}

static HD_RESULT set_proc_param(HD_PATH_ID video_proc_path, HD_DIM* p_dim)
{
	HD_RESULT ret = HD_OK;

	if (p_dim != NULL) { //if videoproc is already binding to dest module, not require to setting this!
		HD_VIDEOPROC_OUT video_out_param = {0};
		video_out_param.func = 0;
		video_out_param.dim.w = p_dim->w;
		video_out_param.dim.h = p_dim->h;
		video_out_param.pxlfmt = HD_VIDEO_PXLFMT_YUV420;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		video_out_param.frc = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoproc_set(video_proc_path, HD_VIDEOPROC_PARAM_OUT, &video_out_param);
	}

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_enc_cfg(HD_PATH_ID video_enc_path, HD_DIM *p_max_dim, UINT32 max_bitrate)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOENC_PATH_CONFIG video_path_config = {0};

	if (p_max_dim != NULL) {

		//--- HD_VIDEOENC_PARAM_PATH_CONFIG ---
		video_path_config.max_mem.codec_type = HD_CODEC_TYPE_H264;
		video_path_config.max_mem.max_dim.w  = p_max_dim->w;
		video_path_config.max_mem.max_dim.h  = p_max_dim->h;
		video_path_config.max_mem.bitrate    = max_bitrate;
		video_path_config.max_mem.enc_buf_ms = 3000;
		video_path_config.max_mem.svc_layer  = HD_SVC_4X;
		video_path_config.max_mem.ltr        = TRUE;
		video_path_config.max_mem.rotate     = FALSE;
		video_path_config.max_mem.source_output   = FALSE;
		video_path_config.isp_id             = 0;
		ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_PATH_CONFIG, &video_path_config);
		if (ret != HD_OK) {
			printf("set_enc_path_config = %d\r\n", ret);
			return HD_ERR_NG;
		}
	}

	return ret;
}

static HD_RESULT set_enc_param(HD_PATH_ID video_enc_path, HD_DIM *p_dim, UINT32 enc_type, UINT32 bitrate)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOENC_IN  video_in_param = {0};
	HD_VIDEOENC_OUT video_out_param = {0};
	HD_H26XENC_RATE_CONTROL rc_param = {0};

	if (p_dim != NULL) {

		//--- HD_VIDEOENC_PARAM_IN ---
		video_in_param.dir           = HD_VIDEO_DIR_NONE;
		video_in_param.pxl_fmt = HD_VIDEO_PXLFMT_YUV420;
		video_in_param.dim.w   = p_dim->w;
		video_in_param.dim.h   = p_dim->h;
		video_in_param.frc     = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_IN, &video_in_param);
		if (ret != HD_OK) {
			printf("set_enc_param_in = %d\r\n", ret);
			return ret;
		}

		printf("enc_type=%d bitrate=%d\r\n", enc_type,bitrate);

		if (enc_type == 0) {

			//--- HD_VIDEOENC_PARAM_OUT_ENC_PARAM ---
			video_out_param.codec_type         = HD_CODEC_TYPE_H265;
			video_out_param.h26x.profile       = HD_H265E_MAIN_PROFILE;
			video_out_param.h26x.level_idc     = HD_H265E_LEVEL_5;
			video_out_param.h26x.gop_num       = 30;
			video_out_param.h26x.ltr_interval  = 0;
			video_out_param.h26x.ltr_pre_ref   = 0;
			video_out_param.h26x.gray_en       = 0;
			video_out_param.h26x.source_output = 0;
			video_out_param.h26x.svc_layer     = HD_SVC_DISABLE;
			video_out_param.h26x.entropy_mode  = HD_H265E_CABAC_CODING;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_ENC_PARAM, &video_out_param);
			if (ret != HD_OK) {
				printf("set_enc_param_out = %d\r\n", ret);
				return ret;
			}

			//--- HD_VIDEOENC_PARAM_OUT_RATE_CONTROL ---
			rc_param.rc_mode             = HD_RC_MODE_CBR;
			rc_param.cbr.bitrate         = bitrate;
			rc_param.cbr.frame_rate_base = g_fps;
			rc_param.cbr.frame_rate_incr = 1;
			rc_param.cbr.init_i_qp       = 26;
			rc_param.cbr.min_i_qp        = 10;
			rc_param.cbr.max_i_qp        = 45;
			rc_param.cbr.init_p_qp       = 26;
			rc_param.cbr.min_p_qp        = 10;
			rc_param.cbr.max_p_qp        = 45;
			rc_param.cbr.static_time     = 4;
			rc_param.cbr.ip_weight       = 0;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_RATE_CONTROL, &rc_param);
			if (ret != HD_OK) {
				printf("set_enc_rate_control = %d\r\n", ret);
				return ret;
			}
		} else if (enc_type == 1) {

			//--- HD_VIDEOENC_PARAM_OUT_ENC_PARAM ---
			video_out_param.codec_type         = HD_CODEC_TYPE_H264;
			video_out_param.h26x.profile       = HD_H264E_HIGH_PROFILE;
			video_out_param.h26x.level_idc     = HD_H264E_LEVEL_5_1;
			video_out_param.h26x.gop_num       = 30;
			video_out_param.h26x.ltr_interval  = 0;
			video_out_param.h26x.ltr_pre_ref   = 0;
			video_out_param.h26x.gray_en       = 0;
			video_out_param.h26x.source_output = 0;
			video_out_param.h26x.svc_layer     = HD_SVC_DISABLE;
			video_out_param.h26x.entropy_mode  = HD_H264E_CABAC_CODING;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_ENC_PARAM, &video_out_param);
			if (ret != HD_OK) {
				printf("set_enc_param_out = %d\r\n", ret);
				return ret;
			}

			//--- HD_VIDEOENC_PARAM_OUT_RATE_CONTROL ---
			rc_param.rc_mode             = HD_RC_MODE_CBR;
			rc_param.cbr.bitrate         = bitrate;
			rc_param.cbr.frame_rate_base = g_fps;
			rc_param.cbr.frame_rate_incr = 1;
			rc_param.cbr.init_i_qp       = 26;
			rc_param.cbr.min_i_qp        = 10;
			rc_param.cbr.max_i_qp        = 45;
			rc_param.cbr.init_p_qp       = 26;
			rc_param.cbr.min_p_qp        = 10;
			rc_param.cbr.max_p_qp        = 45;
			rc_param.cbr.static_time     = 4;
			rc_param.cbr.ip_weight       = 0;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_RATE_CONTROL, &rc_param);
			if (ret != HD_OK) {
				printf("set_enc_rate_control = %d\r\n", ret);
				return ret;
			}

		} else if (enc_type == 2) {

			//--- HD_VIDEOENC_PARAM_OUT_ENC_PARAM ---
			video_out_param.codec_type         = HD_CODEC_TYPE_JPEG;
			video_out_param.jpeg.retstart_interval = 0;
			video_out_param.jpeg.image_quality = 50;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_ENC_PARAM, &video_out_param);
			if (ret != HD_OK) {
				printf("set_enc_param_out = %d\r\n", ret);
				return ret;
			}

		} else {

			printf("not support enc_type\r\n");
			return HD_ERR_NG;
		}
	}

	return ret;
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#if (BIND_BSMUXER == 1)
#include "hd_bsmux_lib.h"
#include "hd_fileout_lib.h"
#include "sdio.h"
#include "FileSysTsk.h"

typedef struct _BSMUXER_STREAM {

	// (1)
	HD_PATH_ID bsmux_path;
	HD_VIDEOENC_BUFINFO bsmux_vencbufinfo;
	HD_AUDIOENC_BUFINFO bsmux_aencbufinfo;
	HD_BSMUX_VIDCODEC vidcodec;

	// (2)
	HD_PATH_ID fileout_path;

	// (3)
	CHAR drive;
	unsigned long filesys_pa;
	unsigned long filesys_va;
	unsigned long filesys_size;

} BSMUXER_STREAM;

INT32 bsmux_callback_func(CHAR *p_name, HD_BSMUX_CBINFO *cbinfo, UINT32 *param)
{
	HD_BSMUX_CB_EVENT event = cbinfo->cb_event;
	switch (event) {
	case HD_BSMUX_CB_EVENT_FOUTREADY: //ready ops buf
		{
			HD_PATH_ID fileout_path = cbinfo->id;
			HD_FILEOUT_BUF *fout_buf = (HD_FILEOUT_BUF *)cbinfo->out_data;
			if (hd_fileout_push_in_buf(fileout_path, fout_buf, -1) != HD_OK)
				printf("hd_fileout_push_in_buf fail\r\n");
		}
		break;
	default:
		break;
	}
	return 0;
}

INT32 fileout_callback_func(CHAR *p_name, HD_FILEOUT_CBINFO *cbinfo, UINT32 *param)
{
	HD_FILEOUT_CB_EVENT event = cbinfo->cb_event;
	switch (event) {
	case HD_FILEOUT_CB_EVENT_NAMING:
		{
			time_t time_sec = time(0);
			struct tm cur_time;
			localtime_r(&time_sec, &cur_time);
			cbinfo->fpath_size = 128;
			snprintf(cbinfo->p_fpath, cbinfo->fpath_size, "A:\\%04d%02d%02d-%02d%02d%02d_%02d.mp4",
				cur_time.tm_year+1900, cur_time.tm_mon+1, cur_time.tm_mday,
				cur_time.tm_hour, cur_time.tm_min, cur_time.tm_sec,(int)cbinfo->iport);
		}
		break;
	case HD_FILEOUT_CB_EVENT_CLOSED:
		break;
	case HD_FILEOUT_CB_EVENT_FS_ERR:
		printf("fs error\r\n");
		break;
	default:
		break;
	}
	return 0;
}

static HD_RESULT set_bsmuxer_config(BSMUXER_STREAM *p_stream)
{
	HD_BSMUX_VIDEOINFO  video_info = {0};
	HD_BSMUX_AUDIOINFO  audio_info = {0};
	HD_BSMUX_FILEINFO   file_info = {0};
	HD_BSMUX_BUFINFO    buf_info = {0};

	// (1) VIDEOINFO
	video_info.vidcodec      = p_stream->vidcodec; //user config
	video_info.vfr           =  g_fps;
	video_info.width         = VDO_SIZE_W;
	video_info.height        = VDO_SIZE_H;
	video_info.tbr           = 2*1024*1024; // 2 Mb/s
	video_info.gop           = 15;
	hd_bsmux_set(p_stream->bsmux_path, HD_BSMUX_PARAM_VIDEOINFO, (VOID *)&video_info);
	// (2) AUDIOINFO
	audio_info.codectype     = HD_BSMUX_AUDCODEC_PCM;
	audio_info.chs           = 2;
	audio_info.asr           = 16000;
	audio_info.adts_bytes    = 7;
	audio_info.aud_en        = 0; //disable
	hd_bsmux_set(p_stream->bsmux_path, HD_BSMUX_PARAM_AUDIOINFO, (VOID *)&audio_info);
	// (3) FILEINFO
	file_info.seamlessSec    = 300;
	file_info.rollbacksec    = 3;
	file_info.keepsec        = 5;
	file_info.filetype       = HD_BSMUX_FTYPE_MP4;
	file_info.recformat      = HD_BSMUX_RECFORMAT_VID_ONLY;
	file_info.revsec         = 5;
	file_info.seamlessSec_ms = HD_BSMUX_SET_MS(300, 0);
	file_info.rollbacksec_ms = HD_BSMUX_SET_MS(3, 0);
	file_info.keepsec_ms     = HD_BSMUX_SET_MS(5, 0);
	file_info.revsec_ms      = HD_BSMUX_SET_MS(5, 0);
	hd_bsmux_set(p_stream->bsmux_path, HD_BSMUX_PARAM_FILEINFO, (VOID *)&file_info);
	// (4) BUFINFO
	buf_info.videnc.phy_addr = p_stream->bsmux_vencbufinfo.buf_info.phy_addr;
	buf_info.videnc.buf_size = p_stream->bsmux_vencbufinfo.buf_info.buf_size;
	hd_bsmux_set(p_stream->bsmux_path, HD_BSMUX_PARAM_BUFINFO, (VOID *)&buf_info);
	// (5) CALLBACK
	hd_bsmux_set(p_stream->bsmux_path, HD_BSMUX_PARAM_REG_CALLBACK, (VOID *)bsmux_callback_func);
	hd_fileout_set(p_stream->fileout_path, HD_FILEOUT_PARAM_REG_CALLBACK, (VOID *)fileout_callback_func);

	return HD_OK;
}

static HD_RESULT init_filesys_module(BSMUXER_STREAM *p_stream)
{
	int    ret;
	FILE_TSK_INIT_PARAM     Param = {0};
	FS_HANDLE               StrgDXH;

	// (1) filesys
	p_stream->drive = 'A';
	p_stream->filesys_size = (ALIGN_CEIL_64(0x4000));  //for linux fs cmd
	if (hd_common_mem_alloc("FsLib", &p_stream->filesys_pa, (void **)&p_stream->filesys_va, p_stream->filesys_size, DDR_ID0) != HD_OK) {
		printf("%s mem alloc fail\r\n", __func__);
		return HD_ERR_NG;
	}
	FileSys_InstallID(FileSys_GetOPS_Linux());
	if (FST_STA_OK != FileSys_Init(FileSys_GetOPS_Linux())) {
		printf("FileSys_Init failed\r\n");
	}
	memset(&Param, 0, sizeof(FILE_TSK_INIT_PARAM));
	StrgDXH = (FS_HANDLE)sdio_getStorageObject(STRG_OBJ_FAT1);
	Param.FSParam.WorkBuf = p_stream->filesys_va;
	Param.FSParam.WorkBufSize = p_stream->filesys_size;
	strncpy(Param.FSParam.szMountPath, "/mnt/sd", sizeof(Param.FSParam.szMountPath) - 1); //only used by FsLinux
	Param.FSParam.szMountPath[sizeof(Param.FSParam.szMountPath) - 1] = '\0';
	Param.FSParam.MaxOpenedFileNum = 10;
	ret = FileSys_OpenEx(p_stream->drive, StrgDXH, &Param);
	if (FST_STA_OK != ret) {
		printf("FileSys_Open err %d\r\n", ret);
	}
	// call the function to wait init finish
	FileSys_WaitFinishEx(p_stream->drive);
	return HD_OK;
}

static HD_RESULT exit_filesys_module(BSMUXER_STREAM *p_stream)
{
	if (FileSys_CloseEx(p_stream->drive, 1000) != FST_STA_OK) {
		printf("%s fail\r\n", __func__);
	}
	if (p_stream->filesys_pa && p_stream->filesys_va) {
		if (hd_common_mem_free((UINT32)p_stream->filesys_pa, (void *)p_stream->filesys_va) != HD_OK) {
			printf("%s mem free fail\r\n", __func__);
		}
		p_stream->filesys_pa = 0;
		p_stream->filesys_va = 0;
	}
	return HD_OK;
}

static HD_RESULT init_bsmuxer_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_gfx_init()) != HD_OK)
		return ret;
	if ((ret = hd_bsmux_init()) != HD_OK)
		return ret;
	if ((ret = hd_fileout_init()) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT open_bsmuxer_module(BSMUXER_STREAM *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_bsmux_open(HD_BSMUX_IN(0, 0), HD_BSMUX_OUT(0, 0), &p_stream->bsmux_path)) != HD_OK)
		return ret;
	if ((ret = hd_fileout_open(HD_FILEOUT_IN(0, 0), HD_FILEOUT_OUT(0, 0), &p_stream->fileout_path)) != HD_OK)
		return ret;
	return HD_OK;
}
static HD_RESULT open_bsmuxer_module1(BSMUXER_STREAM *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_bsmux_open(HD_BSMUX_IN(0, 1), HD_BSMUX_OUT(0, 1), &p_stream->bsmux_path)) != HD_OK)
		return ret;
	if ((ret = hd_fileout_open(HD_FILEOUT_IN(0, 1), HD_FILEOUT_OUT(0, 1), &p_stream->fileout_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT start_bsmuxer_module(BSMUXER_STREAM *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_fileout_start(p_stream->fileout_path)) != HD_OK)
		return ret;
	if ((ret = hd_bsmux_start(p_stream->bsmux_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT stop_bsmuxer_module(BSMUXER_STREAM *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_bsmux_stop(p_stream->bsmux_path)) != HD_OK)
		return ret;
	if ((ret = hd_fileout_stop(p_stream->fileout_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT close_bsmuxer_module(BSMUXER_STREAM *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_bsmux_close(p_stream->bsmux_path)) != HD_OK)
		return ret;
	if ((ret = hd_fileout_close(p_stream->fileout_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT exit_bsmuxer_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_gfx_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_bsmux_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_fileout_uninit()) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT push_video_to_bsmuxer_module(BSMUXER_STREAM *p_stream, HD_VIDEOENC_BS *p_user_bs)
{
	HD_RESULT ret;
	if ((ret = hd_bsmux_push_in_buf_video(p_stream->bsmux_path, p_user_bs, -1)) != HD_OK)
		return ret;
	return HD_OK;
}
#endif

static HD_RESULT set_out_cfg(HD_PATH_ID *p_video_out_ctrl, UINT32 out_type, HD_VIDEOOUT_HDMI_ID hdmi_id)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOOUT_MODE videoout_mode = {0};
	HD_PATH_ID video_out_ctrl = 0;

	ret = hd_videoout_open(0, HD_VIDEOOUT_0_CTRL, &video_out_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}

	printf("out_type=%d\r\n", out_type);

	#if 1
	videoout_mode.output_type = HD_COMMON_VIDEO_OUT_LCD;
	videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
	videoout_mode.output_mode.lcd = HD_VIDEOOUT_LCD_0;
	if (out_type != 1) {
		printf("520 only support LCD\r\n");
	}
	#else
	switch(out_type){
	case 0:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_CVBS;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.cvbs= HD_VIDEOOUT_CVBS_NTSC;
	break;
	case 1:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_LCD;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.lcd = HD_VIDEOOUT_LCD_0;
	break;
	case 2:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_HDMI;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.hdmi= hdmi_id;
	break;
	default:
		printf("not support out_type\r\n");
	break;
	}
	#endif
	ret = hd_videoout_set(video_out_ctrl, HD_VIDEOOUT_PARAM_MODE, &videoout_mode);

	*p_video_out_ctrl=video_out_ctrl ;
	return ret;
}

static HD_RESULT get_out_caps(HD_PATH_ID video_out_ctrl,HD_VIDEOOUT_SYSCAPS *p_video_out_syscaps)
{
	HD_RESULT ret = HD_OK;
    HD_DEVCOUNT video_out_dev = {0};

	ret = hd_videoout_get(video_out_ctrl, HD_VIDEOOUT_PARAM_DEVCOUNT, &video_out_dev);
	if (ret != HD_OK) {
		return ret;
	}
	printf("##devcount %d\r\n", video_out_dev.max_dev_count);

	ret = hd_videoout_get(video_out_ctrl, HD_VIDEOOUT_PARAM_SYSCAPS, p_video_out_syscaps);
	if (ret != HD_OK) {
		return ret;
	}
	return ret;
}

static HD_RESULT set_out_param(HD_PATH_ID video_out_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOOUT_IN video_out_param={0};

	video_out_param.dim.w = p_dim->w;
	video_out_param.dim.h = p_dim->h;
	video_out_param.pxlfmt = HD_VIDEO_PXLFMT_YUV420;
	video_out_param.dir = HD_VIDEO_DIR_NONE;
	ret = hd_videoout_set(video_out_path, HD_VIDEOOUT_PARAM_IN, &video_out_param);
	if (ret != HD_OK) {
		return ret;
	}
	memset((void *)&video_out_param,0,sizeof(HD_VIDEOOUT_IN));
	ret = hd_videoout_get(video_out_path, HD_VIDEOOUT_PARAM_IN, &video_out_param);
	if (ret != HD_OK) {
		return ret;
	}
	printf("##video_out_param w:%d,h:%d %x %x\r\n", video_out_param.dim.w, video_out_param.dim.h, video_out_param.pxlfmt, video_out_param.dir);

	return ret;
}


///////////////////////////////////////////////////////////////////////////////

typedef struct _VIDEO_RECORD {

	// (1)
	HD_VIDEOCAP_SYSCAPS cap_syscaps;
	HD_PATH_ID cap_ctrl;
	HD_PATH_ID cap_path;

	HD_DIM  cap_dim;
	// only used when (g_capbind = 0xff)  //no-bind mode
	pthread_t  cap_thread_id;
	UINT32	cap_enter;
	UINT32	cap_exit;
	UINT32	cap_count;
	UINT32 	cap_loop;

	HD_DIM  proc0_max_dim;

	// (2)
	HD_VIDEOPROC_SYSCAPS proc0_syscaps;
	HD_PATH_ID proc0_ctrl;
	HD_PATH_ID proc0_path;

	HD_DIM  proc1_max_dim;
	HD_DIM  proc1_dim;

	// (3)
	HD_VIDEOPROC_SYSCAPS proc1_syscaps;
	HD_PATH_ID proc1_ctrl;
	HD_PATH_ID proc1_path;

	HD_DIM  enc_max_dim;
	HD_DIM  enc_dim;

	// (4)
	HD_VIDEOENC_SYSCAPS enc_syscaps;
	HD_PATH_ID enc_path;

	// (5) user pull
	pthread_t  enc_thread_id;
	UINT32     enc_exit;
	UINT32     flow_start;

	// (6) vout
	HD_VIDEOOUT_SYSCAPS out_syscaps;
	HD_PATH_ID out_ctrl;
	HD_PATH_ID out_path;
	HD_DIM  out_max_dim;
	HD_DIM  out_dim;

    HD_VIDEOOUT_HDMI_ID hdmi_id;
#if (BIND_BSMUXER == 1)
	BSMUXER_STREAM muxer_stream;
#endif

} VIDEO_RECORD;

static HD_RESULT init_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_init()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_init()) != HD_OK)
		return ret;
    if ((ret = hd_videoenc_init()) != HD_OK)
		return ret;
   if ((ret = hd_videoout_init()) != HD_OK)
		return ret;
	return HD_OK;
}

#if 1 //for IMX291
#if 1
//IMX291 with lens TRC-2191B6(IMX317)
double distor_pt_lut_4R[DISTOR_CURVE_TABLE_NUM] = {
0, 18.0243122612896, 36.0510039231625, 54.0824440742380, 72.1209811792078, 90.1689327668716, 108.228575118174, 126.302132954239, 144.391769124409, 162.499574294278, 180.627556633728, 198.777631504967, 216.951611150564, 235.151194381486, 253.377956265131, 271.633337813367, 289.918635670569, 308.234991801651, 326.583383180107, 344.964611476044, 363.379292744217, 381.827847112070, 400.310488467766, 418.827214148228, 437.377794627174, 455.961763203149, 474.578405687568, 493.226750092746, 511.905556319938, 530.613305847373, 549.348191418292, 568.108106728981, 586.890636116810, 605.693044248269, 624.512265807003, 643.344895181846, 662.187176154862, 681.034991589378, 699.883853118019, 718.728890830749, 737.564842962899, 756.386045583214, 775.186422281878, 793.959473858556, 812.698268010433, 831.395429020241, 850.043127444303, 868.633069800568, 887.156488256642, 905.604130317831, 923.966248515174, 942.232590093474, 960.392386699345, 978.434344069240, 996.346631717488, 1014.11687262433, 1031.73213292397, 1049.17891159257, 1066.44313013633, 1083.51012227953, 1100.36462365249, 1116.99076147972, 1133.37204426785, 1149.49135149374, 1165.33092329247, 1180.87235014540, 1196.09656256821, 1210.98382079891, 1225.51370448592, 1239.66510237604, 1253.41620200255, 1266.74447937323, 1279.62668865836, 1292.03885187880, 1303.95624859400, 1315.35340559006, 1326.20408656774, 1336.48128183051, 1346.15719797258, 1355.20324756694, 1363.59003885341, 1371.28736542665, 1378.26419592421, 1384.48866371455, 1389.92805658512, 1394.54880643034, 1398.31647893967, 1401.19576328565, 1403.15046181192, 1404.14347972124, 1404.13681476357, 1403.09154692408, 1400.96782811119, 1397.72487184459, 1393.32094294330, 1387.71334721371, 1380.85842113759, 1372.71152156014, 1363.22701537802, 1352.35826922741, 1340.05763917202, 1326.27646039112, 1310.96503686760, 1294.07263107601, 1275.54745367057, 1255.33665317321, 1233.38630566162, 1209.64140445729, 1184.04584981353, 1156.54243860351, 1127.07285400830, 1095.57765520491, 1061.99626705432, 1026.26696978950, 988.326888703502, 948.111983837415, 905.557039668468, 860.595654798034, 813.160231639676, 763.181966107179, 710.590837302589, 655.315597204248, 597.283760354830, 536.421593549378, 472.654105523336, 405.905036640590, 336.096848581505, 263.150714030957, 186.986506366370, 107.522789345752, 24.6768067957333, -61.6355277003971, -151.499641114664, -245.002311286354, -342.231677233991, -443.277249467305, -548.229920299177, -657.181974157624, -770.227097897749, -887.460391113704, -1008.97837645067, -1134.87900991681, -1265.26169119522, -1400.22727395591, -1539.87807616777, -1684.31789041053, -1833.65199418671, -1987.98716023361, -2147.43166683525, -2312.09530813433, -2482.08940444423, -2657.52681256095, -2838.52193607506, -3025.19073568368, -3217.65073950246, -3416.02105337752, -3620.42237119739, -3830.97698520506, -4047.80879630984, -4271.04332439939, -4500.80771865168, -4737.23076784691, -4980.44291067954, -5230.57624607019, -5487.76454347764,
-5752.14325321078, -6023.84951674060, -6303.02217701210, -6589.80178875632, -6884.33062880224, -7186.75270638882, -7497.21377347687, -7815.86133506109, -8142.84465948200, -8478.31478873792, -8822.42454879690, -9175.32855990874, -9537.18324691690, -9908.14684957050, -10288.3794328362, -10678.0428972105, -11077.3009890310, -11486.3193107891, -11905.2653314417, -12334.3083967229, -12773.6197394564, -13223.3724898672, -13683.7416858936, -14154.9042834992, -14637.0391669847, -15130.3271593004, -15634.9510323573, -16151.0955173399, -16678.9473150177, -17218.6951060571, -17770.5295613338, -18334.6433522443, -18911.2311610182, -19500.4896910300, -20102.6176771109, -20717.8158958612, -21346.2871759619, -21988.2364084868, -22643.8705572143, -23313.3986689399, -23997.0318837874, -24694.9834455212, -25407.4687118587, -26134.7051647815, -26876.9124208478, -27634.3122415044, -28407.1285433985, -29195.5874086897, -29999.9170953619, -30820.3480475355, -31657.1129057792, -32510.4465174219, -33380.5859468646, -34267.7704858929, -35172.2416639883, -36094.2432586402, -37034.0213056587, -37991.8241094854, -38967.9022535061, -39962.5086103628, -40975.8983522651, -42008.3289613028, -43060.0602397574, -44131.3543204143, -45222.4756768747, -46333.6911338676, -47465.2698775617, -48617.4834658774, -49790.6058387987, -50984.9133286854, -52200.6846705847, -53438.2010125435, -54697.7459259201, -55979.6054156963, -57284.0679307894, -58611.4243743641, -59961.9681141444, -61335.9949927258, -62733.8033378868, -64155.6939729015, -65601.9702268510, -67072.9379449358, -68568.9054987873, -70090.1837967801, -71637.0862943440, -73209.9290042758, -74809.0305070513, -76434.7119611372, -78087.2971133032, -79767.1123089339, -81474.4865023408, -83209.7512670741
};

double undistor_pt_lut[UNDISTOR_CURVE_TABLE_NUM] = {
0, 18.0235184490379, 36.0446579687234, 54.0610524949744, 72.0703621509529, 90.0702864733339, 108.058577409238, 126.033052006235, 143.991604722740, 161.932219292138, 179.852980081122, 197.752082890669, 215.627845156945, 233.478715518831, 251.303282728719, 269.100283893509, 286.868612043274, 304.607323035602, 322.315641814227, 339.992968050928, 357.638881209903, 375.253145083726, 392.835711859566, 410.386725783639, 427.906526500802, 445.395652154856, 462.854842343639, 480.285041031363, 497.687399529069, 515.063279662786, 532.414257257965, 549.742126078586, 567.048902369978, 584.336830166406, 601.608387538148, 618.866293968621, 636.113519070625, 653.353292872621, 670.589117931878, 687.824783562240, 705.064382501235, 722.312330385560, 739.573388457299, 756.852910553027, 774.157363297735, 791.492079919997, 808.863517677882, 826.278647022756, 843.745003935869, 861.270750596296, 878.864745769246, 896.536626578538, 914.296903665936, 932.157072159837, 950.129741398399, 968.228787006013, 986.469529744493, 1004.86894660114, 1023.44592090088, 1042.22153992785, 1061.21945073354, 1080.46628765810, 1100.00016097508, 1119.86959509757, 1140.10292498988
};

double distor_center[2] = {988.9514, 593.8084};
INT32 focal_length = 1170;
#endif

#if 0
//IMX291 with wide lens
double distor_pt_lut_4R[DISTOR_CURVE_TABLE_NUM] = {
0, 18.3690691854627, 36.7260059161746, 55.0587982752996, 73.3556064653146, 91.6048055218842, 109.795025563017, 127.915189284369, 145.954546460495, 163.902705262019, 181.749660248819, 199.485816948532, 217.102012976787, 234.589535700026, 251.940136482731, 269.146041597933, 286.199959912699, 303.095087488569, 319.825109260754, 336.384197979060, 352.767010608442, 368.968682397695, 384.984818831600, 400.811485685005, 416.445197397268, 431.882903982617, 447.121976686582, 462.160192591201, 476.995718362540, 491.627093323469, 506.053212023071, 520.273306461728, 534.286928118084, 548.093929911104, 561.694448217393, 575.088885051060, 588.277890500920, 601.262345507706, 614.043345052474, 626.622181816461, 639.000330362506, 651.179431878618, 663.161279515601, 674.947804342653, 686.541061937641, 697.943219622286, 709.156544346700, 720.183391222639, 731.026192700373, 741.687448380233, 752.169715446624, 762.475599709518, 772.607747236158, 782.568836553873, 792.361571403449, 801.988674021366, 811.452878928476, 820.756927202170, 829.903561208795, 838.895519773069, 847.735533761355, 856.426322055938, 864.970587897876, 873.371015576518, 881.630267444405, 889.750981236938, 897.735767676947, 905.587208345096, 913.307853797818, 920.900221915348, 928.366796463238, 935.710025851559, 942.932322076847, 950.036059832653, 957.023575775355, 963.897167932666, 970.659095243059, 977.311577214994, 983.856793695614, 990.296884739183, 996.633950566222, 1002.87005160491, 1009.00720860685, 1015.04740282998, 1020.99257628178, 1026.84463201644, 1032.60543448034, 1038.27680990033, 1043.86054670985, 1049.35839600834, 1054.77207204980, 1060.10325275647, 1065.35358025417, 1070.52466142608, 1075.61806848186, 1080.63533953944, 1085.57797921705, 1090.44745923310, 1095.24521901193, 1099.97266629354, 1104.63117774562, 1109.22209957631, 1113.74674814632, 1118.20641057922, 1122.60234536871, 1126.93578298193, 1131.20792645788, 1135.41995200023, 1139.57300956375, 1143.66822343379, 1147.70669279829, 1151.68949231184, 1155.61767265136, 1159.49226106312, 1163.31426190082, 1167.08465715444, 1170.80440696966, 1174.47445015781, 1178.09570469610, 1181.66906821818, 1185.19541849485, 1188.67561390499, 1192.11049389670, 1195.50087943861, 1198.84757346154, 1202.15136129033, 1205.41301106625, 1208.63327415975, 1211.81288557386, 1214.95256433830, 1218.05301389437, 1221.11492247084, 1224.13896345090, 1227.12579573033, 1230.07606406709, 1232.99039942238, 1235.86941929344, 1238.71372803817, 1241.52391719173, 1244.30056577534, 1247.04424059734, 1249.75549654684, 1252.43487687988, 1255.08291349850, 1257.70012722274, 1260.28702805576, 1262.84411544226, 1265.37187852034, 1267.87079636693, 1270.34133823701, 1272.78396379671,
1275.19912335045, 1277.58725806228, 1279.94880017159, 1282.28417320326, 1284.59379217243, 1286.87806378408, 1289.13738662744, 1291.37215136544, 1293.58274091933, 1295.76953064856, 1297.93288852601, 1300.07317530887, 1302.19074470500, 1304.28594353513, 1306.35911189092, 1308.41058328897, 1310.44068482088, 1312.44973729958, 1314.43805540185, 1316.40594780727, 1318.35371733365, 1320.28166106904, 1322.19007050036, 1324.07923163884, 1325.94942514226, 1327.80092643416, 1329.63400582003, 1331.44892860055, 1333.24595518211, 1335.02534118444, 1336.78733754568, 1338.53219062471, 1340.26014230110, 1341.97143007240, 1343.66628714921, 1345.34494254776, 1347.00762118030, 1348.65454394325, 1350.28592780316, 1351.90198588060, 1353.50292753204, 1355.08895842964, 1356.66028063921, 1358.21709269621, 1359.75958967994, 1361.28796328598, 1362.80240189682, 1364.30309065082, 1365.79021150958, 1367.26394332362, 1368.72446189656, 1370.17194004772, 1371.60654767329, 1373.02845180606, 1374.43781667367, 1375.83480375559, 1377.21957183871, 1378.59227707161, 1379.95307301764, 1381.30211070672, 1382.63953868592, 1383.96550306891, 1385.28014758428, 1386.58361362268, 1387.87604028298, 1389.15756441730, 1390.42832067502, 1391.68844154584, 1392.93805740181, 1394.17729653846, 1395.40628521492, 1396.62514769326, 1397.83400627683, 1399.03298134783, 1400.22219140401, 1401.40175309450, 1402.57178125498, 1403.73238894193, 1404.88368746622, 1406.02578642593, 1407.15879373842, 1408.28281567178, 1409.39795687550, 1410.50432041055, 1411.60200777877, 1412.69111895163, 1413.77175239839, 1414.84400511360, 1415.90797264411, 1416.96374911540, 1418.01142725741, 1419.05109842979, 1420.08285264668, 1421.10677860085, 1422.12296368744, 1423.13149402718, 1424.13245448907, 1425.12592871261, 1426.11199912966, 1427.09074698566, 1428.06225236061, 1429.02659418946, 1429.98385028221, 1430.93409734346, 1431.87741099168, 1432.81386577805
};

double undistor_pt_lut[UNDISTOR_CURVE_TABLE_NUM] = {
0, 18.3731261203322, 36.7584047536403, 55.1679474535147, 73.6138510116867, 92.1082302307248, 110.663250708924, 129.291161817559, 148.004330051857, 166.815272939967, 185.736693698862, 204.781516832596, 223.962924876734, 243.294396503210, 262.789746212444, 282.463165854514, 302.329268238734, 322.403133111332, 342.700355804485, 363.237098887013, 384.030147178039, 405.096966520380, 426.455766750949, 448.125569351677, 470.126280317253, 492.478768836198, 515.204952450691, 538.327889439183, 561.871879256034, 585.862571965621, 610.327087726982, 635.294147521429, 660.794216472659, 686.859661290257, 713.524923577300, 740.826710985962, 768.804208487677, 797.499312353579, 826.956889825432, 857.225067907315, 888.355555236855, 920.404001617046, 953.430400524517, 987.499540780722, 1022.68151460709, 1059.05229051896, 1096.69436098954, 1135.69747658822, 1176.15948043556, 1218.18725940476, 1261.89783164442, 1307.41959383695, 1354.89375631475, 1404.47599995679, 1456.33839597116, 1510.67163860872, 1567.68765204249, 1627.62264673145, 1690.74071842443, 1757.33810569432, 1827.74825106512, 1902.34784849740, 1981.56410909492, 2065.88354134619, 2155.86262754628
};

double distor_center[2] = {905.1868, 483.5410};
INT32 focal_length = 932;
#endif

#else //for IMX415

#if 0
//IMX415 #18 long pin with lens TRC-2191B6
double distor_pt_lut_4R[DISTOR_CURVE_TABLE_NUM] = {
0, 35.3013588583625, 70.6067258680976, 105.920082913640, 141.245359451805, 176.586406563608, 211.946971324855, 247.330671601740, 282.740971377718, 318.181156717894, 353.654312477199, 389.163299858586, 424.710734927514, 460.298968188979, 495.930065333322, 531.605789257096, 567.327583465226, 603.096556960725, 638.913470728218, 674.778725917525, 710.692353833563, 746.654007838816, 782.662957274622, 818.718083507550, 854.817878207091, 890.960443960950, 927.143497334161, 963.364374478308, 999.620039397082, 1035.90709497444, 1072.22179687163, 1108.56007039929, 1144.91753047095, 1181.28950474414, 1217.67106005532, 1254.05703225501, 1290.44205954922, 1326.82061945360, 1363.18706946634, 1399.53569156639, 1435.86074064289, 1472.15649696236, 1508.41732277977, 1544.63772319973, 1580.81241139413, 1616.93637828238, 1653.00496678066, 1689.01395072626, 1724.95961858333, 1760.83886203641, 1796.64926957778, 1832.38922519506, 1868.05801226521, 1903.65592276125, 1939.18437187790, 1974.64601818247, 2010.04488939713, 2045.38651391892, 2080.67805818378, 2115.92846998069, 2151.14862782234, 2186.35149647853, 2221.55228877851, 2256.76863378860, 2292.02075147123, 2327.33163393177, 2362.72723335932, 2398.23665676778, 2433.89236764341, 2469.73039460508, 2505.79054718364, 2542.11663882646, 2578.75671723352, 2615.76330213127, 2653.19363059051, 2691.10990999453, 2729.57957876383, 2768.67557494354, 2808.47661275996, 2849.06746725237, 2890.53926708634, 2932.98979565487, 2976.52380057366, 3021.25331167651, 3067.29796761748, 3114.78535118570, 3163.85133343936, 3214.64042676498, 3267.30614696824, 3322.01138450266, 3378.92878494240, 3438.24113880530, 3500.14178083263, 3564.83499883158, 3632.53645218694, 3703.47360014807, 3777.88613999748, 3856.02645520734, 3938.16007368999, 4024.56613624893, 4115.53787533639, 4211.38310422372, 4312.42471669101, 4419.00119734202, 4531.46714265081, 4650.19379284615, 4775.56957474024, 4908.00065560767, 5047.91150822116, 5195.74548715011, 5351.96541642835, 5517.05418869733, 5691.51537593096, 5875.87385184832, 6070.67642612065, 6276.49249047868, 6493.91467682669, 6723.55952746947, 6966.06817755855, 7222.10704986377, 7492.36856197661, 7777.57184605150, 8078.46348119127, 8395.81823858309, 8730.43983949115, 9083.16172621228, 9454.84784610076, 9846.39344876870, 10258.7258965681, 10692.8054884607, 11149.6262973827, 11630.2170212089, 12135.6418474249, 12667.0013316110, 13225.4332898464, 13812.1137051381, 14428.2576479824, 15075.1202111634, 15753.9974588969, 16466.2273904237, 17213.1909181604, 17996.3128605135, 18817.0629494615, 19676.9568530139, 20577.5572126514,
21520.4746958542, 22507.3690638244, 23539.9502545094, 24619.9794810317, 25749.2703456322, 26929.6899692319, 28163.1601367207, 29451.6584580762, 30797.2195454224, 32201.9362061311, 33667.9606520750, 35197.5057251372, 36792.8461390836, 38456.3197379054, 40190.3287707361, 41997.3411834515, 43879.8919270569, 45840.5842829688, 47882.0912052981, 50007.1566802390, 52218.5971026714, 54519.3026700834, 56912.2387939182, 59400.4475284536, 61987.0490173200, 64675.2429577618, 67468.3100827502, 70369.6136610528, 73382.6010153660, 76510.8050586171, 79757.8458485421, 83127.4321606452, 86623.3630796467, 90249.5296095250, 94009.9163022597, 97908.6029053814, 101949.766028434, 106137.680828459, 110476.722714602, 114971.369071951, 119626.201004719, 124445.905098865, 129435.275204265, 134599.214236545, 139942.735998675, 145470.967022430, 151189.148429828, 157102.637814656, 163216.911144174, 169537.564681119, 176070.316926112, 182821.010580565, 189795.614530210, 197000.225849340, 204441.071825880, 212124.512007387, 220057.040268093, 228245.286897086, 236696.020707747, 245416.151168545, 254412.730555285, 263692.956124941, 273264.172311154, 283133.872941513, 293309.703476729, 303799.463271797, 314611.107859269, 325752.751254720, 337232.668284541, 349059.296936141, 361241.240730684, 373787.271118447, 386706.329896929, 400007.531651794, 413700.166220772, 427793.701180610, 442297.784357196, 457222.246358943, 472577.103133555, 488372.558548269, 504619.006993698, 521327.036011348, 538507.428944960, 556171.167615739, 574329.435021606, 592993.618060569, 612175.310278318, 631886.314640156, 652138.646327358, 672944.535558091, 694316.430432969, 716266.999805374, 738809.136176641, 761955.958616206, 785720.815706838, 810117.288515043, 835159.193586772, 860860.585968504, 887235.762253847, 914299.263655737, 942065.879104354, 970550.648370851, 999768.865217024, 1029736.08057100, 1060468.10572905, 1091981.01558371, 1124291.15187817, 1157415.12648714, 1191369.82472437, 1226172.40867668, 1261840.32056493, 1298391.28613173
};

double undistor_pt_lut[UNDISTOR_CURVE_TABLE_NUM] = {
0, 35.3000210679325, 70.5960346673446, 105.884063288595, 141.160189852243, 176.420587735689, 211.661550008581, 246.879517619726, 282.071106295873, 317.233131934210, 352.362634295140, 387.456898829134, 422.513476500646, 457.530201502250, 492.505206782771, 527.436937343315, 562.324161284156, 597.165978612704, 631.961827847640, 666.711490476322, 701.415093341190, 736.073109045906, 770.686354482984, 805.255987591568, 839.783502456724, 874.270722859923, 908.719794384600, 943.133175170479, 977.513625396148, 1011.86419555105, 1046.18821353581, 1080.48927060382, 1114.77120612714, 1149.03809113651, 1183.29421054826, 1217.54404395077, 1251.79224477940, 1286.04361766220, 1320.30309366900, 1354.57570314441, 1388.86654575063, 1423.18075729042, 1457.52347282366, 1491.89978553577, 1526.31470076244, 1560.77308452693, 1595.27960590513, 1629.83867250486, 1664.45435833271, 1699.13032333224, 1733.86972391653, 1768.67511389688, 1803.54833533609, 1838.49039904196, 1873.50135467673, 1908.58015080387, 1943.72448563942, 1978.93064983184, 2014.19336327320, 2049.50560874878, 2084.85846616173, 2120.24095211023, 2155.63987072150, 2191.03968281752, 2226.42240163637
};

double distor_center[2] = {1983.0, 1076.4};
INT32 focal_length = 2261;
#endif

#if 0
//IMX415 wide-angle lens
double distor_pt_lut_4R[DISTOR_CURVE_TABLE_NUM] = {
0, 35.7782557534189, 71.5333417503821, 107.242408849948, 142.883028324902, 178.433264660323, 213.871742596451, 249.177708106689, 284.331083100627, 319.312513735972, 354.103412310666, 388.685992786113, 423.043300063349, 457.159233195835, 491.018562775080, 524.606942768590, 557.910917124180, 590.917921480723, 623.616280343877, 655.995200096682, 688.044758220128, 719.755889098519, 751.120366779535, 782.130785050086, 812.780535176969, 843.063781646763, 872.975436222797, 902.511130618990, 931.667188071301, 960.440594067914, 988.828966479327, 1016.83052530968, 1044.44406227094, 1071.66891036251, 1098.50491362005, 1124.95239717966, 1151.01213778630, 1176.68533485945, 1201.97358221342, 1226.87884051582, 1251.40341055409, 1275.54990736790, 1299.32123529369, 1322.72056395729, 1345.75130524099, 1368.41709124296, 1390.72175323890, 1412.66930164915, 1434.26390700798, 1455.50988192667, 1476.41166403696, 1496.97379989747, 1517.20092984200, 1537.09777374591, 1556.66911768395, 1575.91980145120, 1594.85470691698, 1613.47874718055, 1631.79685649632, 1649.81398093599, 1667.53506975459, 1684.96506742737, 1702.10890632469, 1718.97149999239, 1735.55773700566, 1751.87247536496, 1767.92053740351, 1783.70670517648, 1799.23571630311, 1814.51226023387, 1829.54097491591, 1844.32644383090, 1858.87319338076, 1873.18569059750, 1887.26834115486, 1901.12548766018, 1914.76140820623, 1928.18031516378, 1941.38635419654, 1954.38360348133, 1967.17607311723, 1979.76770470831, 1992.16237110561, 2004.36387629493, 2016.37595541759, 2028.20227491241, 2039.84643276785, 2051.31195887381, 2062.60231546364, 2073.72089763711, 2084.67103395622, 2095.45598710594, 2106.07895461262, 2116.54306961358, 2126.85140167148, 2137.00695762793, 2147.01268249092, 2156.87146035127, 2166.58611532375, 2176.15941250844, 2185.59405896892, 2194.89270472358, 2204.05794374699, 2213.09231497838, 2221.99830333472, 2230.77834072582, 2239.43480706946, 2247.97003130449, 2256.38629240004, 2264.68582035948, 2272.87079721740, 2280.94335802860, 2288.90559184767, 2296.75954269842, 2304.50721053199, 2312.15055217308, 2319.69148225349, 2327.13187413232, 2334.47356080251, 2341.71833578308, 2348.86795399679, 2355.92413263302, 2362.88855199541, 2369.76285633430, 2376.54865466367, 2383.24752156261, 2389.86099796118, 2396.39059191072, 2402.83777933853,
2409.20400478711, 2415.49068213791, 2421.69919531980, 2427.83089900226, 2433.88711927352, 2439.86915430386, 2445.77827499403, 2451.61572560929, 2457.38272439894, 2463.08046420184, 2468.71011303791, 2474.27281468595, 2479.76968924805, 2485.20183370068, 2490.57032243283, 2495.87620777140, 2501.12052049401, 2506.30427032966, 2511.42844644728, 2516.49401793251, 2521.50193425300, 2526.45312571243, 2531.34850389341, 2536.18896208968, 2540.97537572770, 2545.70860277794, 2550.38948415608, 2555.01884411440, 2559.59749062345, 2564.12621574445, 2568.60579599241, 2573.03699269038, 2577.42055231491, 2581.75720683301, 2586.04767403077, 2590.29265783389, 2594.49284862028, 2598.64892352496, 2602.76154673740, 2606.83136979157, 2610.85903184879, 2614.84515997366, 2618.79036940311, 2622.69526380894, 2626.56043555380, 2630.38646594093, 2634.17392545776, 2637.92337401355, 2641.63536117117, 2645.31042637326, 2648.94909916281, 2652.55189939843, 2656.11933746426, 2659.65191447493, 2663.15012247538, 2666.61444463596, 2670.04535544273, 2673.44332088318, 2676.80879862748, 2680.14223820537, 2683.44408117876, 2686.71476131021, 2689.95470472741, 2693.16433008365, 2696.34404871454, 2699.49426479094, 2702.61537546829, 2705.70777103238, 2708.77183504171, 2711.80794446641, 2714.81646982398, 2717.79777531178, 2720.75221893641, 2723.68015264013, 2726.58192242422, 2729.45786846958, 2732.30832525443, 2735.13362166937, 2737.93408112970, 2740.71002168519, 2743.46175612732, 2746.18959209408, 2748.89383217231, 2751.57477399774, 2754.23271035277, 2756.86792926197, 2759.48071408551, 2762.07134361032, 2764.64009213940, 2767.18722957890, 2769.71302152345, 2772.21772933942, 2774.70161024641, 2777.16491739690, 2779.60789995414, 2782.03080316827, 2784.43386845086, 2786.81733344768, 2789.18143210993, 2791.52639476398, 2793.85244817944, 2796.15981563587, 2798.44871698795, 2800.71936872934, 2802.97198405502, 2805.20677292242, 2807.42394211117, 2809.62369528152, 2811.80623303166, 2813.97175295363, 2816.12044968820, 2818.25251497848, 2820.36813772244, 2822.46750402432, 2824.55079724495, 2826.61819805103, 2828.66988446334, 2830.70603190398, 2832.72681324262, 2834.73239884177, 2836.72295660115, 2838.69865200110, 2840.65964814512, 2842.60610580155, 2844.53818344434, 2846.45603729310, 2848.35982135218, 2850.24968744908
};

double undistor_pt_lut[UNDISTOR_CURVE_TABLE_NUM] = {
0, 35.7860147272353, 71.5952367618351, 107.450701049393, 143.375313441834, 179.391914698693, 215.523343912655, 251.792501714473, 288.222413608328, 324.836293787963, 361.657609786615, 398.710148319776, 436.018082689326, 473.606042130631, 511.499183500998, 549.723265728698, 588.304727466810, 627.270768425885, 666.649434894166, 706.469709994521, 746.761609273801, 787.556282273868, 828.886120794821, 870.784874631034, 913.287775640607, 956.431671100125, 1000.25516740079, 1044.79878526084, 1090.10512776508, 1136.21906269735, 1183.18792080971, 1231.06171187549, 1279.89336060723, 1329.73896478925, 1380.65807828402, 1432.71402192908, 1485.97422575411, 1540.51060642712, 1596.39998439484, 1653.72454583089, 1712.57235526171, 1773.03792562615, 1835.22285356464, 1899.23652895776, 1965.19692917965, 2033.23151024371, 2103.47820905195, 2176.08657338505, 2251.21903917144, 2329.05237805803, 2409.77934250456, 2493.61054070574, 2580.77657982048, 2671.53052352360, 2766.15071913441, 2864.94406095645, 2968.24977054884, 3076.44379217823, 3189.94392362976, 3309.21583015072, 3434.78012423485, 3567.22073846375, 3707.19487571200, 3855.44489477316, 4012.81258545976
};

double distor_center[2] = {1972.0, 1163.4};
INT32 focal_length = 1870;
#endif

#if 1
//IMX415 wide-angle lens flat front glass
double distor_pt_lut_4R[DISTOR_CURVE_TABLE_NUM] = {
0, 34.8240949676785, 69.6344816257238, 104.417541759701, 139.159767244985, 173.847772520248, 208.468306619793, 243.008264890634, 277.454700514234, 311.794835943968, 346.016074357857, 380.106011212386, 414.052445967552, 447.843394036073, 481.467098991393, 514.912045050083, 548.166969824948, 581.220877325999, 614.063051167888, 646.683067924784, 679.070810557415, 711.216481822468, 743.110617562012, 774.744099760315, 806.108169247720, 837.194437926091, 867.994900387979, 898.501944802037, 928.708362940322, 958.607359228863, 988.192558711019, 1017.45801382361, 1046.39820989814, 1075.00806931339, 1103.28295424109, 1131.21866794253, 1158.81145459077, 1186.05799761009, 1212.95541654124, 1239.50126245719, 1265.69351196966, 1291.53055988099, 1317.01121054906, 1342.13466804440, 1366.90052518860, 1391.30875157114, 1415.35968064827, 1439.05399603210, 1462.39271708078, 1485.37718390213, 1508.00904188238, 1530.29022585013, 1552.22294398244, 1573.80966155591, 1595.05308464034, 1615.95614382672, 1636.52197807487, 1656.75391875883, 1676.65547398124, 1696.23031321993, 1715.48225236325, 1734.41523918243, 1753.03333928277, 1771.34072256810, 1789.34165024639, 1807.04046239822, 1824.44156612392, 1841.54942427979, 1858.36854480891, 1874.90347066747, 1891.15877034376, 1907.13902896317, 1922.84883996960, 1938.29279737097, 1953.47548853420, 1968.40148751318, 1983.07534889169, 1997.50160212190, 2011.68474633839, 2025.62924562661, 2039.33952472466, 2052.81996513664, 2066.07490163619, 2079.10861913861, 2091.92534992052, 2104.52927116613, 2116.92450281992, 2129.11510572588, 2141.10508003435, 2152.89836385783, 2164.49883215826, 2175.91029584864, 2187.13650109296, 2198.18112878892, 2209.04779421886, 2219.74004685509, 2230.26137030640, 2240.61518239366, 2250.80483534258, 2260.83361608307, 2270.70474664478, 2280.42138463934, 2289.98662382046, 2299.40349471349, 2308.67496530683, 2317.80394179796, 2326.79326938745, 2335.64573311493, 2344.36405873113, 2352.95091360101, 2361.40890763295, 2369.74059422976, 2377.94847125729, 2386.03498202708, 2394.00251628953, 2401.85341123464, 2409.58995249731, 2417.21437516489, 2424.72886478443, 2432.13555836771, 2439.43654539217, 2446.63386879585, 2453.72952596515, 2460.72546971376, 2467.62360925181, 2474.42581114398, 2481.13390025584, 2487.74966068744, 2494.27483669357,
2500.71113359005, 2507.06021864549, 2513.32372195826, 2519.50323731807, 2525.60032305211, 2531.61650285539, 2537.55326660518, 2543.41207115948, 2549.19434113929, 2554.90146969487, 2560.53481925591, 2566.09572226559, 2571.58548189879, 2577.00537276434, 2582.35664159166, 2587.64050790181, 2592.85816466310, 2598.01077893163, 2603.09949247670, 2608.12542239155, 2613.08966168955, 2617.99327988600, 2622.83732356597, 2627.62281693828, 2632.35076237590, 2637.02214094310, 2641.63791290953, 2646.19901825155, 2650.70637714106, 2655.16089042202, 2659.56344007512, 2663.91488967060, 2668.21608480973, 2672.46785355507, 2676.67100684976, 2680.82633892621, 2684.93462770431, 2688.99663517953, 2693.01310780105, 2696.98477684024, 2700.91235874970, 2704.79655551311, 2708.63805498605, 2712.43753122816, 2716.19564482672, 2719.91304321196, 2723.59036096423, 2727.22822011339, 2730.82723043036, 2734.38798971140, 2737.91108405492, 2741.39708813134, 2744.84656544596, 2748.26006859518, 2751.63813951611, 2754.98130972988, 2758.29010057871, 2761.56502345695, 2764.80658003629, 2768.01526248520, 2771.19155368285, 2774.33592742761, 2777.44884864028, 2780.53077356219, 2783.58214994835, 2786.60341725567, 2789.59500682655, 2792.55734206783, 2795.49083862525, 2798.39590455363, 2801.27294048272, 2804.12233977899, 2806.94448870345, 2809.73976656546, 2812.50854587284, 2815.25119247825, 2817.96806572198, 2820.65951857123, 2823.32589775598, 2825.96754390159, 2828.58479165813, 2831.17796982657, 2833.74740148197, 2836.29340409359, 2838.81628964225, 2841.31636473471, 2843.79393071547, 2846.24928377573, 2848.68271505990, 2851.09451076950, 2853.48495226459, 2855.85431616282, 2858.20287443618, 2860.53089450538, 2862.83863933209, 2865.12636750903, 2867.39433334786, 2869.64278696516, 2871.87197436629, 2874.08213752738, 2876.27351447542, 2878.44633936648, 2880.60084256213, 2882.73725070416, 2884.85578678754, 2886.95667023175, 2889.04011695046, 2891.10633941966, 2893.15554674427, 2895.18794472317, 2897.20373591286, 2899.20311968965, 2901.18629231044, 2903.15344697224, 2905.10477387022, 2907.04046025463, 2908.96069048635, 2910.86564609127, 2912.75550581347, 2914.63044566724, 2916.49063898793, 2918.33625648176, 2920.16746627446, 2921.98443395895, 2923.78732264194, 2925.57629298952, 2927.35150327180, 2929.11310940658
};

double undistor_pt_lut[UNDISTOR_CURVE_TABLE_NUM] = {
0, 34.8286750475413, 69.6710719051923, 104.540875990666, 139.451769367961, 174.417470754655, 209.451775714415, 244.568597206802, 279.782006668998, 315.106275808050, 350.555919287801, 386.145738501817, 421.890866632487, 457.806815207091, 493.909522374277, 530.215403139027, 566.741401811232, 603.505046942436, 640.524509047625, 677.818661434252, 715.407144489449, 753.310433809006, 791.549912588537, 830.147948739026, 869.127977236122, 908.514588265941, 948.333621790560, 988.612269224816, 1029.37918299361, 1070.66459482692, 1112.50044374983, 1154.92051483859, 1197.96058994371, 1241.65861172876, 1286.05486254338, 1331.19215984273, 1377.11607008820, 1423.87514332077, 1471.52117089327, 1520.10946918893, 1569.69919254878, 1620.35367908843, 1672.14083361832, 1725.13355250335, 1779.41019602556, 1835.05511466668, 1892.15923673099, 1950.82072591310, 2011.14571881607, 2073.24915408906, 2137.25570683442, 2203.30084430288, 2271.53202173595, 2342.11004063578, 2415.21059587774, 2491.02604310102, 2569.76742393118, 2651.66679408187, 2736.97990860050, 2825.98932991914, 2919.00803853304, 3016.38364382254, 3118.50331476492, 3225.79957838324, 3338.75716952268
};

double distor_center[2] = {1949.1, 1081.2};
INT32 focal_length = 2723;
#endif

#if 0
//IMX415 ultra-wide angle lens
double distor_pt_lut_4R[DISTOR_CURVE_TABLE_NUM] = {
0, 35.3857858573809, 70.6932392685877, 105.848543714592, 140.783240851515, 175.434518888411, 209.745323586465, 243.664324326094, 277.145768099897, 310.149252187826, 342.639442560454, 374.585760527520, 405.962055421875, 436.746276620556, 466.920154225845, 496.468894396130, 525.380892666076, 553.647466597751, 581.262607688532, 608.222751537264, 634.526564740955, 660.174746767238, 685.169845039011, 709.516081604961, 733.219189993705, 756.286261113591, 778.725597329878, 800.546574101828, 821.759508778697, 842.375536327493, 862.406491894159, 881.864800185177, 900.763371702800, 919.115505880445, 936.934801152053, 954.235071957435, 971.030272641284, 987.334428152412, 1003.16157139666, 1018.52568704563, 1033.44066155660, 1047.92023911872, 1061.97798320799, 1075.62724340849, 1088.88112714084, 1101.75247592941, 1114.25384583725, 1126.39749170146, 1138.19535480961, 1149.65905367114, 1160.79987755283, 1171.62878246587, 1182.15638931172, 1192.39298391464, 1202.34851868995, 1212.03261571807, 1221.45457101506, 1230.62335981040, 1239.54764266169, 1248.23577225404, 1256.69580074867, 1264.93548756095, 1272.96230746237, 1280.78345891428, 1288.40587255315, 1295.83621975785, 1303.08092123949, 1310.14615560287, 1317.03786783654, 1323.76177769539, 1330.32338794582, 1336.72799244897, 1342.98068406229, 1349.08636234372, 1355.04974104655, 1360.87535539608, 1366.56756914182, 1372.13058138145, 1377.56843315457, 1382.88501380607, 1388.08406712017, 1393.16919722768, 1398.14387428967, 1403.01143996170, 1407.77511264334, 1412.43799251838, 1417.00306639122, 1421.47321232555, 1425.85120409132, 1430.13971542649, 1434.34132411972, 1438.45851592064, 1442.49368828382, 1446.44915395289, 1450.32714439110, 1454.12981306423, 1457.85923858202, 1461.51742770390, 1465.10631821462, 1468.62778167537, 1472.08362605575, 1475.47559825159, 1478.80538649381, 1482.07462265288, 1485.28488444370, 1488.43769753519, 1491.53453756884, 1494.57683209046, 1497.56596239882, 1500.50326531506, 1503.39003487643, 1506.22752395778, 1509.01694582402, 1511.75947561676, 1514.45625177814, 1517.10837741450, 1519.71692160295, 1522.28292064305, 1524.80737925643, 1527.29127173648, 1529.73554305045, 1532.14110989610, 1534.50886171493, 1536.83966166399, 1539.13434754802, 1541.39373271388, 1543.61860690871, 1545.80973710364, 1547.96786828451, 1550.09372421100, 1552.18800814565, 1554.25140355410,
1556.28457477766, 1558.28816767970, 1560.26281026666, 1562.20911328507, 1564.12767079551, 1566.01906072439, 1567.88384539475, 1569.72257203673, 1571.53577327876, 1573.32396762015, 1575.08765988599, 1576.82734166503, 1578.54349173122, 1580.23657644974, 1581.90705016800, 1583.55535559229, 1585.18192415076, 1586.78717634317, 1588.37152207797, 1589.93536099735, 1591.47908279054, 1593.00306749607, 1594.50768579319, 1595.99329928312, 1597.46026076037, 1598.90891447456, 1600.33959638323, 1601.75263439577, 1603.14834860905, 1604.52705153496, 1605.88904832015, 1607.23463695833, 1608.56410849540, 1609.87774722773, 1611.17583089369, 1612.45863085892, 1613.72641229541, 1614.97943435465, 1616.21795033514, 1617.44220784441, 1618.65244895575, 1619.84891035990, 1621.03182351184, 1622.20141477285, 1623.35790554811, 1624.50151241980, 1625.63244727612, 1626.75091743622, 1627.85712577111, 1628.95127082097, 1630.03354690868, 1631.10414424994, 1632.16324905991, 1633.21104365665, 1634.24770656142, 1635.27341259587, 1636.28833297636, 1637.29263540540, 1638.28648416040, 1639.27004017972, 1640.24346114618, 1641.20690156818, 1642.16051285833, 1643.10444340984, 1644.03883867063, 1644.96384121539, 1645.87959081548, 1646.78622450680, 1647.68387665579, 1648.57267902351, 1649.45276082790, 1650.32424880429, 1651.18726726418, 1652.04193815245, 1652.88838110286, 1653.72671349211, 1654.55705049235, 1655.37950512227, 1656.19418829675, 1657.00120887522, 1657.80067370864, 1658.59268768523, 1659.37735377502, 1660.15477307308, 1660.92504484174, 1661.68826655157, 1662.44453392134, 1663.19394095691, 1663.93657998904, 1664.67254171035, 1665.40191521113, 1666.12478801440, 1666.84124610990, 1667.55137398735, 1668.25525466875, 1668.95296973990, 1669.64459938110, 1670.33022239712, 1671.00991624633, 1671.68375706918, 1672.35181971588, 1673.01417777348, 1673.67090359221, 1674.32206831123, 1674.96774188363, 1675.60799310096, 1676.24288961707, 1676.87249797137, 1677.49688361158, 1678.11611091586, 1678.73024321446, 1679.33934281082, 1679.94347100219, 1680.54268809971, 1681.13705344810, 1681.72662544480, 1682.31146155865, 1682.89161834828, 1683.46715147984, 1684.03811574452, 1684.60456507555, 1685.16655256483, 1685.72413047923, 1686.27735027641, 1686.82626262037
};

double undistor_pt_lut[UNDISTOR_CURVE_TABLE_NUM] = {
0, 35.4124260786088, 70.9036194776405, 106.549472063263, 142.423554019474, 178.597966729252, 215.144165471452, 252.133769857071, 289.639379036224, 327.735408445937, 366.498965216792, 406.010780326081, 446.356217223760, 487.626379050206, 529.919339840931, 573.341529456289, 618.009307635892, 664.050769899678, 711.607837461782, 760.838695512797, 811.920660005993, 865.053573606716, 920.463858316666, 978.409387663446, 1039.18538832621, 1103.13164399963, 1170.64135940011, 1242.17215859234, 1318.25985245924, 1399.53583477726, 1486.74928460968, 1580.79581002276, 1682.75483539207, 1793.93902466707, 1915.96052930859, 2050.82115713239, 2201.03719689689, 2369.81551474287, 2561.30731130535, 2780.98268044155, 3036.19885776947, 3337.09004688455, 3698.01326246152, 4140.00266262533, 4695.16100686418, 5415.04216160261, 6388.01172487697, 7779.24232151261, 9937.33152757382, 13746.7761766212, 22300.6110710427, 59245.0711222065, -89398.1977489947, -25385.2489567361, -14753.5609170866, -10375.1338710591, -7984.65980196399, -6477.46026861701, -5439.48931607593, -4680.56637448282, -4101.09191660678, -3643.85595059595, -3273.65454736512, -2967.63636232492, -2710.32094033182
};

double distor_center[2] = {1974.8, 1110.3};
INT32 focal_length = 1242;
#endif

#endif

#define GYRO_FXPT_PREC (1 << 16)
static HD_RESULT init_eis(void)
{
	VENDOR_EIS_OPEN_CFG test_open_obj = {0};
	UINT32 i;
	HD_RESULT ret;

	test_open_obj.cam_intrins.distor_center[0] = distor_center[0]; //pixel
	test_open_obj.cam_intrins.distor_center[1] = distor_center[1]; //pixel
	for (i = 0; i < DISTOR_CURVE_TABLE_NUM; i++) {
		test_open_obj.cam_intrins.distor_curve[i] = distor_pt_lut_4R[i];
	}
	for (i = 0; i < UNDISTOR_CURVE_TABLE_NUM; i++) {
		test_open_obj.cam_intrins.undistor_curve[i] = undistor_pt_lut[i];
	}
	test_open_obj.cam_intrins.calib_img_size.w = VDO_SIZE_W; //pixel
	test_open_obj.cam_intrins.calib_img_size.h = VDO_SIZE_H; //pixel

	test_open_obj.cam_intrins.focal_length = focal_length; //pixel

    test_open_obj.imu_sync_shift_exposure_time_threshold = 15000; //us
    test_open_obj.imu_sync_shift_exposure_time_precent = 30;

	test_open_obj.imu_type = VENDOR_EIS_GYROSCOPE; //
#if 1
	//gyro board pin up, chip facing forward
	test_open_obj.gyro.axes_mapping[0].axis = VENDOR_EIS_CAM_X; //Ex. axis = CAM_Z, sign = -1;
	test_open_obj.gyro.axes_mapping[0].sign = 1;
	test_open_obj.gyro.axes_mapping[1].axis = VENDOR_EIS_CAM_Y;
	test_open_obj.gyro.axes_mapping[1].sign = 1;
	test_open_obj.gyro.axes_mapping[2].axis = VENDOR_EIS_CAM_Z;
	test_open_obj.gyro.axes_mapping[2].sign = 1;
#else
	//gyro board pin up, chip facing backward
	test_open_obj.gyro.axes_mapping[0].axis = VENDOR_EIS_CAM_X; //Ex. axis = CAM_Z, sign = -1;
	test_open_obj.gyro.axes_mapping[0].sign = -1;
	test_open_obj.gyro.axes_mapping[1].axis = VENDOR_EIS_CAM_Y;
	test_open_obj.gyro.axes_mapping[1].sign = 1;
	test_open_obj.gyro.axes_mapping[2].axis = VENDOR_EIS_CAM_Z;
	test_open_obj.gyro.axes_mapping[2].sign = -1;
#endif
	test_open_obj.gyro.sampling_rate = g_fps*GYRO_DATA_NUM; //Ex. 1000 Hz
	test_open_obj.gyro.unit_conv = 1000.0/32768.0/180.0*VPRC_PI;  //Ex. gyro_value*unit_conv = rad/s

	if (vendor_isp_init() == HD_ERR_NG) {
		return -1;
	}

	ret = vendor_eis_open(&test_open_obj);

	{
		VENDOR_EIS_PATH_INFO test_path_obj = {0};
		BOOL lib_set_status = FALSE;
		INT32 frame_rate;
		INT32 gyro_sample_per_frame;
		INT32 crop_percent;
		BOOL switch_val;
		VENDOR_EIS_IMG_ROTATE_SEL rot_sel;

		test_path_obj.path_id = 0;
		test_path_obj.frame_latency = 1;
		test_path_obj.frame_size.w = VDO_SIZE_W;
		test_path_obj.frame_size.h = VDO_SIZE_H;
		test_path_obj.lut2d_size_sel = lut2d_path0.lut2d.lut_sz;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_PATH_INFO, &test_path_obj);
		printf("Set EIS_RSC_PARAM_PATH_INFO: %s\r\n", lib_set_status ? "Success" : "Failed");

#if 0
		test_path_obj.path_id = 1;
		test_path_obj.frame_latency = 1;
		test_path_obj.frame_size.w = VDO_SIZE_W;
		test_path_obj.frame_size.h = VDO_SIZE_H;
		test_path_obj.lut2d_size_sel = VENDOR_EIS_LUT_65x65;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_PATH_INFO, &test_path_obj);
		printf("Set EIS_RSC_PARAM_PATH_INFO: %s\r\n", lib_set_status ? "Success" : "Failed");
#endif
		switch_val = FALSE;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_FRAME_CNT_RESET, &switch_val);
		printf("Set EIS_RSC_PARAM_FRAME_CNT_RESET: %s\r\n", lib_set_status ? "Success" : "Failed");

		frame_rate = 30;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_FRAME_RATE, &frame_rate);
		printf("Set EIS_RSC_PARAM_FRAME_RATE: %s\r\n", lib_set_status ? "Success" : "Failed");

		gyro_sample_per_frame = GYRO_DATA_NUM;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_IMU_SAMPLE_NUM_PER_FRAME, &gyro_sample_per_frame);
		printf("Set EIS_RSC_PARAM_IMU_SAMPLE_NUM_PER_FRAME: %s\r\n", lib_set_status ? "Success" : "Failed");

		rot_sel = VENDOR_EIS_ROT_0;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_ROTATE_TYPE, &rot_sel);
		printf("Set EIS_RSC_PARAM_ROTATE_TYPE: %s\r\n", lib_set_status ? "Success" : "Failed");

		crop_percent = 20;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_COMPEN_CROP_PERCENT, &crop_percent);
		printf("Set EIS_RSC_PARAM_COMPEN_CROP_PERCENT: %s\r\n", lib_set_status ? "Success" : "Failed");
	}

	return ret;
}
static HD_RESULT init_vpe(void)
{
	HD_RESULT ret;
	VPET_DCE_CTL_PARAM dce_ctl = {0};

	if ((ret = vendor_vpe_init()) != HD_OK) {
		printf("vendor_vpe_init failed!(%d)\r\n", ret);
		return ret;
	}

	dce_ctl.id = ISP_EFFECT_ID;
	dce_ctl.dce_ctl.enable = 1;
	dce_ctl.dce_ctl.dce_mode = 1; // 0:GDC, 1:2DLUT
	dce_ctl.dce_ctl.lens_radius = 0;
	vendor_vpe_set_cmd(VPET_ITEM_DCE_CTL_PARAM, &dce_ctl);

	lut2d_path0.id = ISP_EFFECT_ID;
	gen_default_2dlut(lut2d_path0.lut2d.lut, VDO_SIZE_W, VDO_SIZE_H, 9);
	vendor_vpe_set_cmd(VPET_ITEM_2DLUT_PARAM, &lut2d_path0);
	return ret;
}
static HD_RESULT open_module(VIDEO_RECORD *p_stream, HD_DIM* p_proc0_max_dim, HD_DIM* p_proc1_max_dim)
{
	HD_RESULT ret;
	// set videocap config
	ret = set_cap_cfg(&p_stream->cap_ctrl);
	if (ret != HD_OK) {
		printf("set cap-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set 1st videoproc config
	ret = set_proc_cfg(HD_VIDEOPROC_0_CTRL, &p_stream->proc0_ctrl, HD_VIDEOPROC_PIPE_RAWALL, p_proc0_max_dim);
	if (ret != HD_OK) {
		printf("set proc-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set 2nd videoproc config
	ret = set_proc_cfg(HD_VIDEOPROC_1_CTRL, &p_stream->proc1_ctrl, HD_VIDEOPROC_PIPE_VPE, p_proc1_max_dim);
	if (ret != HD_OK) {
		printf("set proc-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}

	if ((ret = hd_videocap_open(HD_VIDEOCAP_0_IN_0, HD_VIDEOCAP_0_OUT_0, &p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_0, &p_stream->proc0_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_1_IN_0, HD_VIDEOPROC_1_OUT_0, &p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoenc_open(HD_VIDEOENC_0_IN_0, HD_VIDEOENC_0_OUT_0, &p_stream->enc_path)) != HD_OK)
		return ret;

	return HD_OK;
}

static HD_RESULT open_module_2(VIDEO_RECORD *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_1,  &p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoenc_open(HD_VIDEOENC_0_IN_1, HD_VIDEOENC_0_OUT_1,  &p_stream->enc_path)) != HD_OK)
		return ret;

	return HD_OK;
}
static HD_RESULT open_module_3(VIDEO_RECORD *p_stream,UINT32 out_type)
{
	HD_RESULT ret;
	// set videoout config
	ret = set_out_cfg(&p_stream->out_ctrl, out_type, p_stream->hdmi_id);
	if (ret != HD_OK) {
		printf("set out-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}

	if ((ret = hd_videoproc_open(HD_VIDEOPROC_1_IN_0, HD_VIDEOPROC_1_OUT_2,  &p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoout_open(HD_VIDEOOUT_0_IN_0, HD_VIDEOOUT_0_OUT_0,  &p_stream->out_path)) != HD_OK)
		return ret;

	return HD_OK;
}

static HD_RESULT close_module(VIDEO_RECORD *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_close(p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_close(p_stream->proc0_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_close(p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoenc_close(p_stream->enc_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT close_module_2(VIDEO_RECORD *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videoproc_close(p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoenc_close(p_stream->enc_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT close_module_3(VIDEO_RECORD *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videoproc_close(p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoout_close(p_stream->out_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT exit_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoenc_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoout_uninit()) != HD_OK)
		return ret;
	return HD_OK;
}
////for save gyro data
//#define TMP_SIZE 2048
//static char write_buf[TMP_SIZE] = {0};
// only used when (g_capbind = 0xff)  //no-bind mode

typedef struct {
	UINT32 *data_num;				//output gyro data number
	UINT64 *t_diff_crop;			//time for crop_start to crop_end
	UINT64 *t_diff_crp_end_to_vd;	//time for crop_end to next vd
	INT32 *agyro_x;
	INT32 *agyro_y;
	INT32 *agyro_z;
	INT32 *ags_x;
	INT32 *ags_y;
	INT32 *ags_z;
} USR_SIE_EXT_GYRO_DATA;


#if 0
INT32 vcap_get_gyro_data(UINT32 rsv1)
{
    INT32 rt = E_OK;
    USR_SIE_EXT_GYRO_DATA gyro_data;
    UINT32 buf_ofs = 0;

	gyro_data.data_num = (UINT32 *)(rsv1);

	buf_ofs += sizeof(*gyro_data.data_num);
	gyro_data.t_diff_crop = (UINT64 *)(rsv1 + buf_ofs);

	buf_ofs += sizeof(*gyro_data.t_diff_crop);
	gyro_data.t_diff_crp_end_to_vd = (UINT64 *)(rsv1 + buf_ofs);

	buf_ofs += sizeof(*gyro_data.t_diff_crp_end_to_vd);
	gyro_data.agyro_x =  (INT32 *)(rsv1 + buf_ofs);
	buf_ofs += (*gyro_data.data_num) * sizeof(INT32);
	gyro_data.agyro_y =  (INT32 *)(rsv1 + buf_ofs);
	buf_ofs += (*gyro_data.data_num) * sizeof(INT32);
	gyro_data.agyro_z =  (INT32 *)(rsv1 + buf_ofs);
	buf_ofs += (*gyro_data.data_num) * sizeof(INT32);
	gyro_data.ags_x = (INT32 *)(rsv1 + buf_ofs);
	buf_ofs += (*gyro_data.data_num) * sizeof(INT32);
	gyro_data.ags_y = (INT32 *)(rsv1 + buf_ofs);
	buf_ofs += (*gyro_data.data_num) * sizeof(INT32);
	gyro_data.ags_z = (INT32 *)(rsv1 + buf_ofs);

    return rt;
}
#endif
UINT64 vcap_ts = 0;
static void *capture_thread(void *arg)
{
	VIDEO_RECORD* p_stream0 = (VIDEO_RECORD *)arg;
	HD_RESULT ret = HD_OK;
	HD_VIDEO_FRAME video_frame = {0};
	char file_path_main[32] = {0};
	FILE *f_out_main;
	UINT32 phy_addr_main, vir_addr_main;
    UINT64 last_frame =0;
    char *ptr = 0;
	UINT32 pa   = 0;
	void  *va   = NULL;
    UINT32 keep_pos =0;
    static UINT32 checked =0;
	HD_COMMON_MEM_VB_BLK      blk;
	ISPT_SENSOR_EXPT sensor_expt = {0};

	 #define PHY2VIRT_RSV1(pa) (vir_addr_main + ((pa) - phy_addr_main))

    snprintf(file_path_main, 32, "/mnt/sd/gyro_dump.txt");
	if ((f_out_main = fopen(file_path_main, "wb")) == NULL) {
		HD_VIDEOENC_ERR("open file (%s) fail....\r\n", file_path_main);
	} else {
		printf("\r\ndump gyro to file (%s) ....\r\n", file_path_main);
	}

	p_stream0->cap_exit = 0;
	p_stream0->cap_loop = 0;
	p_stream0->cap_count = 0;

	blk = hd_common_mem_get_block(HD_COMMON_MEM_USER_DEFINIED_POOL, GYRO_FILE_MAX, DDR_ID0);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail\r\n");
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		printf("not get buffer, pa=%08x\r\n", (int)pa);
	}
	va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, GYRO_FILE_MAX);

	//------ wait flow_start ------
	while (p_stream0->cap_enter == 0) usleep(100);

	//--------- pull data test ---------
	while (p_stream0->cap_exit == 0) {

		//printf("cap_pull ....\r\n");
		ret = hd_videocap_pull_out_buf(p_stream0->cap_path, &video_frame, -1); // -1 = blocking mode
		if (ret != HD_OK) {
			if (ret != HD_ERR_UNDERRUN)
			printf("cap_pull error=%d !!\r\n\r\n", ret);
    			goto skip;
		}

        if(checked<1) {
            printf("1cap ts=%llu count = %llu \r\n",video_frame.timestamp,video_frame.count);
            vcap_ts = video_frame.timestamp;
            checked++;
        }
		//printf("cap frame.count = %llu last_frame %llu\r\n", video_frame.count,last_frame);
		if((last_frame)&&(video_frame.count-last_frame!=1)){
            printf("###################ERR: drop frame %llu %llu ret %d ts:%llu ############\r\n",video_frame.count,last_frame,ret,video_frame.timestamp);

		}
		last_frame = video_frame.count;
///////////////////////

			phy_addr_main = hd_common_mem_blk2pa(video_frame.blk); // Get physical addr
			if (phy_addr_main == 0) {
				printf("hd_common_mem_blk2pa error !!\r\n\r\n");
				goto skip;
			}
			vir_addr_main = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, phy_addr_main, g_vcap_blk_size);
			if (vir_addr_main == 0) {
				printf("memory map error !!\r\n\r\n");
				goto skip;
			}
            vendor_isp_get_common(ISPT_ITEM_SENSOR_EXPT, &sensor_expt);
            //printf("exposure time (us) = %d, %d \n", sensor_expt.time[0], sensor_expt.time[1]);
            if (!sensor_expt.time[0]) {
                printf("no exposure time (us) = %d, %d \n");
            }

			{
				UINT32 rsv1 = PHY2VIRT_RSV1(video_frame.phy_addr[HD_VIDEO_MAX_PLANE-1]);

				if (rsv1) {
                    USR_SIE_EXT_GYRO_DATA gyro_data;
                    UINT32 buf_ofs = 0;
					UINT32 data_num;
					INT32 *agyro_x;
					INT32 *agyro_y;
					INT32 *agyro_z;
					INT32 *ags_x;
					INT32 *ags_y;
					INT32 *ags_z;
                	UINT64 *t_diff_crop;
                	UINT64 *t_diff_crp_end_to_vd;

                	gyro_data.data_num = (UINT32 *)(rsv1);
                    data_num = *gyro_data.data_num;
                	buf_ofs += sizeof(*gyro_data.data_num);
                	t_diff_crop = (UINT64 *)(rsv1 + buf_ofs);
                	buf_ofs += sizeof(*gyro_data.t_diff_crop);
                	t_diff_crp_end_to_vd = (UINT64 *)(rsv1 + buf_ofs);

                	buf_ofs += sizeof(*gyro_data.t_diff_crp_end_to_vd);
                	agyro_x =  (INT32 *)(rsv1 + buf_ofs);
                	buf_ofs += (*gyro_data.data_num) * sizeof(INT32);
                	agyro_y =  (INT32 *)(rsv1 + buf_ofs);
                	buf_ofs += (*gyro_data.data_num) * sizeof(INT32);
                	agyro_z =  (INT32 *)(rsv1 + buf_ofs);

                	buf_ofs += (*gyro_data.data_num) * sizeof(INT32);
                	ags_x = (INT32 *)(rsv1 + buf_ofs);
                	buf_ofs += (*gyro_data.data_num) * sizeof(INT32);
                	ags_y = (INT32 *)(rsv1 + buf_ofs);
                	buf_ofs += (*gyro_data.data_num) * sizeof(INT32);
                	ags_z = (INT32 *)(rsv1 + buf_ofs);
                    {
                    UINT32 i = 0;
                    UINT32 pos=0;

                    ptr = va+keep_pos;
                    for(i=0;i<data_num;i++) {
                        sprintf(ptr+pos,"%lu %ld %ld %d %d %d %d %d %d %d\r\n",(unsigned long)video_frame.timestamp,(unsigned long)(*t_diff_crop),(unsigned long)(*t_diff_crp_end_to_vd),(int)sensor_expt.time[0],(int)*(agyro_x+i),(int)*(agyro_y+i),(int)*(agyro_z+i), \
                            (int)*(ags_x+i),(int)*(ags_y+i),(int)*(ags_z+i));
                        pos=strlen(ptr);
                    }
                    keep_pos += pos;
					if(g_show_gyro) {
                        printf("%s",ptr);
					}
                    //printf("##keep_pos %d %d ptr %lx %lx\r\n",keep_pos,pos,ptr,va);

                    }
				}
			}

///////////////////////
		//printf("proc_push ....\r\n");
		ret = hd_videoproc_push_in_buf(p_stream0->proc0_path, &video_frame, NULL, 0); // only support non-blocking mode now
		if (ret != HD_OK) {
			printf("proc_push error=%d !!\r\n\r\n", ret);
    			goto skip;
		}

		//printf("cap_release ....\r\n");
		ret = hd_videocap_release_out_buf(p_stream0->cap_path, &video_frame);
		if (ret != HD_OK) {
			printf("cap_release error=%d !!\r\n\r\n", ret);
    			goto skip;
		}
		hd_common_mem_munmap((void *)vir_addr_main, g_vcap_blk_size);

		p_stream0->cap_count ++;
		//printf("capture count = %d\r\n", p_stream0->cap_count);
skip:

		p_stream0->cap_loop++;
		usleep(100); //sleep for getchar()
	}


	if (f_out_main) fwrite(va, 1, keep_pos, f_out_main);
	if (f_out_main) fflush(f_out_main);
	// close output file
	if (f_out_main) fclose(f_out_main);
    hd_common_mem_release_block(blk);
	return 0;
}
static void *encode_thread(void *arg)
{
	VIDEO_RECORD* p_stream0 = (VIDEO_RECORD *)arg;
	VIDEO_RECORD* p_stream1 = p_stream0 + 1;
	HD_RESULT ret = HD_OK;
	HD_VIDEOENC_BS  data_pull;
	UINT32 j;
    static UINT32 checked = 0;
	UINT32 vir_addr_main=0;
	HD_VIDEOENC_BUFINFO phy_buf_main;
	char file_path_main[32] = "/mnt/sd/dump_bs_main.dat";
	FILE *f_out_main=NULL;
	#define PHY2VIRT_MAIN(pa) (vir_addr_main + (pa - phy_buf_main.buf_info.phy_addr))
#if OUT_NO_EIS
	UINT32 vir_addr_sub=0;
	HD_VIDEOENC_BUFINFO phy_buf_sub;
#endif
	char file_path_sub[32]  = "/mnt/sd/dump_bs_sub.dat";
	FILE *f_out_sub=NULL;
	#define PHY2VIRT_SUB(pa) (vir_addr_sub + (pa - phy_buf_sub.buf_info.phy_addr))
	HD_VIDEOENC_POLL_LIST poll_list[2];
#if OUT_NO_EIS
	UINT32 poll_num = 2;
#else
	UINT32 poll_num = 1;
#endif
	//------ wait flow_start ------
	while (p_stream0->flow_start == 0) sleep(1);

	// query physical address of bs buffer ( this can ONLY query after hd_videoenc_start() is called !! )
	hd_videoenc_get(p_stream0->enc_path, HD_VIDEOENC_PARAM_BUFINFO, &phy_buf_main);
#if OUT_NO_EIS
	hd_videoenc_get(p_stream1->enc_path, HD_VIDEOENC_PARAM_BUFINFO, &phy_buf_sub);
#endif
	// mmap for bs buffer (just mmap one time only, calculate offset to virtual address later)
	vir_addr_main = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, phy_buf_main.buf_info.phy_addr, phy_buf_main.buf_info.buf_size);
#if OUT_NO_EIS
	vir_addr_sub  = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, phy_buf_sub.buf_info.phy_addr, phy_buf_sub.buf_info.buf_size);
#endif
    if (g_write_file) {
    	//----- open output files -----
    	if ((f_out_main = fopen(file_path_main, "wb")) == NULL) {
    		HD_VIDEOENC_ERR("open file (%s) fail....\r\n", file_path_main);
    	} else {
    		printf("\r\ndump main bitstream to file (%s) ....\r\n", file_path_main);
    	}
    	if ((f_out_sub = fopen(file_path_sub, "wb")) == NULL) {
    		HD_VIDEOENC_ERR("open file (%s) fail....\r\n", file_path_sub);
    	} else {
    		printf("\r\ndump sub  bitstream to file (%s) ....\r\n", file_path_sub);
    	}
    }
	printf("\r\nif you want to stop, enter \"q\" to exit !!\r\n\r\n");

	//--------- pull data test ---------
	poll_list[0].path_id = p_stream0->enc_path;
	poll_list[1].path_id = p_stream1->enc_path;


	//--------- pull data test ---------
	while (p_stream0->enc_exit == 0) {
		if (g_enc_poll && HD_OK == hd_videoenc_poll_list(poll_list, poll_num, -1)) {    // multi path poll_list , -1 = blocking mode
			if (TRUE == poll_list[0].revent.event) {
				//pull data
				ret = hd_videoenc_pull_out_buf(p_stream0->enc_path, &data_pull, -1); // -1 = blocking mode

                if(checked<1) {
                    printf("1venc %llu\r\n",data_pull.timestamp);
                    checked ++;
                    if(data_pull.timestamp!=vcap_ts) {
                        printf("ERR: gyro and venc not sync\r\n");
                    }
                }
				if (ret == HD_OK) {
					for (j=0; j< data_pull.pack_num; j++) {
						UINT8 *ptr = (UINT8 *)PHY2VIRT_MAIN(data_pull.video_pack[j].phy_addr);
						UINT32 len = data_pull.video_pack[j].size;
						if (g_write_file) {
							if (f_out_main) fwrite(ptr, 1, len, f_out_main);
							if (f_out_main) fflush(f_out_main);
						}
					}
#if (BIND_BSMUXER == 1)
			push_video_to_bsmuxer_module(&(p_stream0->muxer_stream), &data_pull);
#endif

					// release data
					ret = hd_videoenc_release_out_buf(p_stream0->enc_path, &data_pull);
				}
			}
#if OUT_NO_EIS
			if (TRUE == poll_list[1].revent.event) {
				//pull data
				ret = hd_videoenc_pull_out_buf(p_stream1->enc_path, &data_pull, -1); // -1 = blocking mode
				if (ret == HD_OK) {
					for (j=0; j< data_pull.pack_num; j++) {
						UINT8 *ptr = (UINT8 *)PHY2VIRT_SUB(data_pull.video_pack[j].phy_addr);
						UINT32 len = data_pull.video_pack[j].size;
						if (g_write_file) {
							if (f_out_sub) fwrite(ptr, 1, len, f_out_sub);
							if (f_out_sub) fflush(f_out_sub);
						}
					}
#if (BIND_BSMUXER == 1)
			push_video_to_bsmuxer_module(&(p_stream1->muxer_stream), &data_pull);
#endif
					// release data
					ret = hd_videoenc_release_out_buf(p_stream1->enc_path, &data_pull);
				}
			}
#endif
		} else {
			usleep(100000);
		}
	}

	// mummap for bs buffer
	hd_common_mem_munmap((void *)vir_addr_main, phy_buf_main.buf_info.buf_size);
#if OUT_NO_EIS
	hd_common_mem_munmap((void *)vir_addr_sub, phy_buf_sub.buf_info.buf_size);
#endif
	// close output file
	if (f_out_main) fclose(f_out_main);
	if (f_out_sub) fclose(f_out_sub);

	return 0;
}

MAIN(argc, argv)
{
	HD_RESULT ret;
	INT key;
	VIDEO_RECORD stream[3] = {0}; //0: main stream, 1: sub stream,2: liveview
	UINT32 enc_type = 0;
	HD_DIM main_dim;
	HD_DIM sub_dim;
	UINT32 out_type = 1;
	PD_SHM_INFO  *p_pd_shm;
	AET_CFG_INFO cfg_info = {0};
	AET_EXPT_BOUND expt_bound = {0};
	UINT32       calibration = 0;

	// query program options

	if (argc == 1 || argc > 6) {
		printf("Usage: <enc-type> <cap_out_bind> <cap_out_fmt> <prc_out_bind> <prc_out_fmt>.\r\n");
		printf("Help:\r\n");
		printf("  <enc_type> : 0(H265), 1(H264), 2(MJPG)\r\n");
		printf("  <cap_out_bind>: 0(D2D Mode), 1(Direct Mode), 2(OneBuf Mode)\r\n");
		printf("  <sensor> :  0(IMX290) , 1(IMX415)\r\n");
		printf("  <fps>  : 30,60\r\n");
		printf("  <calibration mode>  :  0, 1\r\n");
		return 0;
	}


	if (argc > 1) {
		enc_type = atoi(argv[1]);
		printf("enc_type %d\r\n", enc_type);
		if(enc_type > 2) {
			printf("error: not support enc_type!\r\n");
			return 0;
		}
	}

	if (argc > 2) {
		if (argv[2][0] == 'x') {
			g_capbind = 0xff;
			printf("CAP-BIND x\r\n");
		} else {
			g_capbind = atoi(argv[2]);
			printf("CAP-BIND %d\r\n", g_capbind);
			if(g_capbind > 3) {
				printf("error: not support CAP-BIND! (%d) \r\n", g_capbind);
				return 0;
			}
		}
	}
	if (argc > 3) {
		sensor_sel = atoi(argv[3]);
		printf("sensor_sel %d\r\n", sensor_sel);
		if(sensor_sel > 2) {
			printf("error: not support sensor_sel!\r\n");
			return 0;
		}
	}
	if (argc > 4) {
		g_fps = atoi(argv[4]);
		printf("g_fps %d\r\n", g_fps);
	}
	if (argc > 5) {
		calibration = atoi(argv[5]);
		printf("calibration %d\r\n", calibration);
	}

    if (sensor_sel == SEN_SEL_IMX415) {
		VDO_SIZE_W = VDO_SIZE_W_8M;
		VDO_SIZE_H = VDO_SIZE_H_8M;
		vdo_br = VDO_BITRATE_8M;
        vdo_br_rtsp = VDO_BITRATE_RTSP_8M;
	} else {
		VDO_SIZE_W = VDO_SIZE_W_2M;
		VDO_SIZE_H = VDO_SIZE_H_2M;
		vdo_br =VDO_BITRATE_2M;
        vdo_br_rtsp = VDO_BITRATE_RTSP_2M;

	}

     printf("###main %d*%d vdo_br=%d vdo_br_rtsp=%d\r\n",VDO_SIZE_W,VDO_SIZE_H,vdo_br,vdo_br_rtsp);
	if (vendor_isp_init() != HD_ERR_NG) {
		cfg_info.id = 0;
    if (sensor_sel == SEN_SEL_IMX415) {
		strncpy(cfg_info.path, "/mnt/app/isp/isp_imx415_0_528.cfg", CFG_NAME_LENGTH);
    }else{
		strncpy(cfg_info.path, "/mnt/app/isp/isp_imx290_0.cfg", CFG_NAME_LENGTH);
    }
		printf("sensor 1 load %s \n", cfg_info.path);
		vendor_isp_set_ae(AET_ITEM_RLD_CONFIG, &cfg_info);
		expt_bound.id = 0;
		expt_bound.bound.l = (UINT32)500;
		expt_bound.bound.h = (UINT32)8000;
		vendor_isp_set_ae(AET_ITEM_EXPT_BOUND, &expt_bound);
		vendor_isp_set_awb(AWBT_ITEM_RLD_CONFIG, &cfg_info);
		vendor_isp_set_iq(IQT_ITEM_RLD_CONFIG, &cfg_info);
		vendor_isp_uninit();
	}


	// init hdal
	ret = hd_common_init(0);
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
		goto exit;
	}

	// init memory
	ret = mem_init();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
		goto exit;
	}

	init_share_memory();

	// init all modules
	ret = init_module();
	if (ret != HD_OK) {
		printf("init fail=%d\n", ret);
		goto exit;
	}
#if (BIND_BSMUXER == 1)
	init_filesys_module(&(stream[0].muxer_stream));
	ret = init_bsmuxer_module();
	if (ret != HD_OK) {
		printf("init bsmuxer fail=%d\n", ret);
		goto exit;
	}
#endif

	// init EIS lib
	ret = init_eis();
	if (ret != HD_OK) {
		printf("eis init fail=%d\n", ret);
		goto exit;
	}
	ret = init_vpe();
	if (ret != HD_OK) {
		printf("vpe init fail=%d\n", ret);
		goto exit;
	}

	// open video_record modules (pre-process)
	stream[0].proc0_max_dim.w = VDO_SIZE_W; //assign by user
	stream[0].proc0_max_dim.h = VDO_SIZE_H; //assign by user
	// open video_record modules (main)
	stream[0].proc1_max_dim.w = VDO_SIZE_W; //assign by user
	stream[0].proc1_max_dim.h = VDO_SIZE_H; //assign by user
	ret = open_module(&stream[0], &stream[0].proc0_max_dim, &stream[0].proc1_max_dim);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}
#if (BIND_BSMUXER == 1)
	ret = open_bsmuxer_module(&(stream[0].muxer_stream));
	if (ret != HD_OK) {
		printf("open bsmuxer fail=%d\n", ret);
		goto exit;
	}
#endif

	// open video_record modules (sub)
	ret = open_module_2(&stream[1]);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}
#if (BIND_BSMUXER == 1)
	ret = open_bsmuxer_module1(&(stream[1].muxer_stream));
	if (ret != HD_OK) {
		printf("open bsmuxer1 fail=%d\n", ret);
		goto exit;
	}
#endif

	// open vout modules (display)
	ret = open_module_3(&stream[2], out_type);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}

	// get videocap capability
	ret = get_cap_caps(stream[0].cap_ctrl, &stream[0].cap_syscaps);
	if (ret != HD_OK) {
		printf("get cap-caps fail=%d\n", ret);
		goto exit;
	}

	// get videoout capability
	ret = get_out_caps(stream[2].out_ctrl, &stream[2].out_syscaps);
	if (ret != HD_OK) {
		printf("get out-caps fail=%d\n", ret);
		goto exit;
	}

    if (sensor_sel == SEN_SEL_IMX415) {
    	stream[2].out_dim.w = VDO_SIZE_W/6; //using vpe/8 and ide scale
    	stream[2].out_dim.h = VDO_SIZE_H/6; //using vpe/8 and ide scale
    } else {
	    stream[2].out_max_dim = stream[2].out_syscaps.output_dim;
    }
	// set videocap parameter
	stream[0].cap_dim.w = VDO_SIZE_W; //assign by user
	stream[0].cap_dim.h = VDO_SIZE_H; //assign by user
	ret = set_cap_param(stream[0].cap_path, &stream[0].cap_dim);
	if (ret != HD_OK) {
		printf("set cap fail=%d\n", ret);
		goto exit;
	}

	// assign parameter by program options
	main_dim.w = VDO_SIZE_W;
	main_dim.h = VDO_SIZE_H;
	sub_dim.w = SUB_VDO_SIZE_W;
	sub_dim.h = SUB_VDO_SIZE_H;

	// set videoproc parameter (pre-process)
	stream[0].proc1_dim.w = main_dim.w;
	stream[0].proc1_dim.h = main_dim.h;
	ret = set_proc_param(stream[0].proc0_path, &stream[0].proc1_dim);
	if (ret != HD_OK) {
		printf("set proc fail=%d\n", ret);
		goto exit;
	}

	// set videoproc parameter (main)
	stream[0].proc1_dim.w = main_dim.w;
	stream[0].proc1_dim.h = main_dim.h;
	ret = set_proc_param(stream[0].proc1_path, NULL);
	if (ret != HD_OK) {
		printf("set proc fail=%d\n", ret);
		goto exit;
	}

	stream[1].proc1_dim.w = sub_dim.w;
	stream[1].proc1_dim.h = sub_dim.h;
	// set videoproc parameter (sub)
	ret = set_proc_param(stream[1].proc1_path, NULL);
	if (ret != HD_OK) {
		printf("set proc fail=%d\n", ret);
		goto exit;
	}

    //vpe scale ratio is 1/8,ide scale ratio is 1/2
    if (sensor_sel == SEN_SEL_IMX415) {
	// set videoout parameter (main)
	stream[2].out_dim.w = VDO_SIZE_W/6; //using vpe/8 and ide scale
	stream[2].out_dim.h = VDO_SIZE_H/6; //using vpe/8 and ide scale

    } else {
	// set videoout parameter (main)
	stream[2].out_dim.w = stream[2].out_max_dim.w; //using device max dim.w
	stream[2].out_dim.h = stream[2].out_max_dim.h; //using device max dim.h
    }
	ret = set_out_param(stream[2].out_path, &stream[2].out_dim);
	if (ret != HD_OK) {
		printf("set out fail=%d\n", ret);
		goto exit;
	}

	// set videoenc config (main)
	stream[0].enc_max_dim.w = main_dim.w;
	stream[0].enc_max_dim.h = main_dim.h;
	ret = set_enc_cfg(stream[0].enc_path, &stream[0].enc_max_dim, vdo_br);
	if (ret != HD_OK) {
		printf("set enc-cfg fail=%d\n", ret);
		goto exit;
	}

	// set videoenc parameter (main)
	stream[0].enc_dim.w = main_dim.w;
	stream[0].enc_dim.h = main_dim.h;
	ret = set_enc_param(stream[0].enc_path, &stream[0].enc_dim, enc_type, vdo_br);
	if (ret != HD_OK) {
		printf("set enc fail=%d\n", ret);
		goto exit;
	}

	// set videoenc config (sub)
	stream[1].enc_max_dim.w = sub_dim.w;
	stream[1].enc_max_dim.h = sub_dim.h;
	ret = set_enc_cfg(stream[1].enc_path, &stream[1].enc_max_dim, vdo_br);
	if (ret != HD_OK) {
		printf("set enc-cfg fail=%d\n", ret);
		goto exit;
	}

	// set videoenc parameter (sub)
	stream[1].enc_dim.w = sub_dim.w;
	stream[1].enc_dim.h = sub_dim.h;
	ret = set_enc_param(stream[1].enc_path, &stream[1].enc_dim, enc_type, vdo_br);
	if (ret != HD_OK) {
		printf("set enc fail=%d\n", ret);
		goto exit;
	}

	if (g_capbind == 0xff) { //no-bind mode
		// create capture_thread (pull_out frame, push_in frame then pull_out frame)
		ret = pthread_create(&stream[0].cap_thread_id, NULL, capture_thread, (void *)stream);
		if (ret < 0) {
			printf("create capture_thread failed");
			goto exit;
		}
	} else {
    	// bind video_record modules (main)
	    hd_videocap_bind(HD_VIDEOCAP_0_OUT_0, HD_VIDEOPROC_0_IN_0);
    }
	if (calibration) {
		hd_videoproc_bind(HD_VIDEOPROC_0_OUT_0, HD_VIDEOENC_0_IN_0);
	} else {
		hd_videoproc_bind(HD_VIDEOPROC_0_OUT_0, HD_VIDEOPROC_1_IN_0);
	}
	hd_videoproc_bind(HD_VIDEOPROC_1_OUT_0, HD_VIDEOENC_0_IN_0);
    if (calibration) {
        hd_videoproc_bind(HD_VIDEOPROC_0_OUT_1, HD_VIDEOOUT_0_IN_0);
    } else {
#if OUT_NO_EIS
	// bind video_record modules (sub)
	hd_videoproc_bind(HD_VIDEOPROC_0_OUT_1, HD_VIDEOENC_0_IN_1);
#endif
    }
	// bind vout modules (display)
	hd_videoproc_bind(HD_VIDEOPROC_1_OUT_2, HD_VIDEOOUT_0_IN_0);

	// create encode_thread (pull_out bitstream)
	ret = pthread_create(&stream[0].enc_thread_id, NULL, encode_thread, (void *)stream);
	if (ret < 0) {
		printf("create encode thread failed");
		goto exit;
	}

	if (g_capbind == 1) {
		//direct NOTE: ensure videocap start after 1st videoproc phy path start
		hd_videoproc_start(stream[0].proc0_path);
		hd_videocap_start(stream[0].cap_path);
	} else {
		hd_videocap_start(stream[0].cap_path);
		hd_videoproc_start(stream[0].proc0_path);
	}
	// start video_record modules (main)
	hd_videoproc_start(stream[0].proc1_path);
#if OUT_NO_EIS
	// start video_record modules (sub)
	hd_videoproc_start(stream[1].proc1_path);
#endif
	// start vout modules (display)
	hd_videoproc_start(stream[2].proc1_path);

	// just wait ae/awb stable for auto-test, if don't care, user can remove it
	sleep(1);
	hd_videoenc_start(stream[0].enc_path);
#if OUT_NO_EIS
	hd_videoenc_start(stream[1].enc_path);
#endif
#if (BIND_BSMUXER == 1)
	if (enc_type == 0) {
		stream[0].muxer_stream.vidcodec = HD_BSMUX_VIDCODEC_H265;
		stream[1].muxer_stream.vidcodec = HD_BSMUX_VIDCODEC_H265;
	}else if (enc_type == 1) {
		stream[0].muxer_stream.vidcodec = HD_BSMUX_VIDCODEC_H264;
		stream[1].muxer_stream.vidcodec = HD_BSMUX_VIDCODEC_H264;
    }else {
		printf("muxer vidcodec not supported\r\n");
    }
	hd_videoenc_get(stream[0].enc_path, HD_VIDEOENC_PARAM_BUFINFO, &(stream[0].muxer_stream.bsmux_vencbufinfo));
	set_bsmuxer_config(&(stream[0].muxer_stream));
	start_bsmuxer_module(&(stream[0].muxer_stream));
#if OUT_NO_EIS
	hd_videoenc_get(stream[1].enc_path, HD_VIDEOENC_PARAM_BUFINFO, &(stream[1].muxer_stream.bsmux_vencbufinfo));
	set_bsmuxer_config(&(stream[1].muxer_stream));
	start_bsmuxer_module(&(stream[1].muxer_stream));
#endif
#endif


	hd_videoout_start(stream[2].out_path);

	if (g_capbind == 0xff) { //no-bind mode
		// start capture_thread
		stream[0].cap_enter = 1;
	}

	// let encode_thread start to work
	stream[0].flow_start= 1;

	// query user key
	printf("Enter q to exit\n");
	while (1) {
		key = GETCHAR();
		if (key == 'q' || key == 0x3) {
        	if (g_capbind == 0xff) { //no-bind mode
        		// stop capture_thread
        		stream[0].cap_exit = 1;
        	}

			// let encode_thread stop loop and exit
			stream[0].enc_exit = 1;
			// quit program
			break;
		}
        if (key == 'c') {
			printf("start rtsp\r\n");
            g_enc_poll = 0;
        	hd_videoenc_stop(stream[0].enc_path);
#if OUT_NO_EIS
        	hd_videoenc_stop(stream[1].enc_path);
#endif
            usleep(500000);
        	ret = set_enc_param(stream[0].enc_path, &stream[0].enc_dim, enc_type, vdo_br_rtsp);
#if OUT_NO_EIS
        	ret = set_enc_param(stream[1].enc_path, &stream[1].enc_dim, enc_type, vdo_br_rtsp);
#endif
            usleep(500000);
        	hd_videoenc_start(stream[0].enc_path);
#if OUT_NO_EIS
        	hd_videoenc_start(stream[1].enc_path);
#endif
            usleep(500000);

			system("nvtrtspd_ipc &");
		}

        if (key == '4') {
            hd_videoenc_stop(stream[0].enc_path);
            hd_videoenc_stop(stream[1].enc_path);
            printf("stop encode\r\n");
        }
        if (key == '5') {
            hd_videoenc_start(stream[0].enc_path);
            hd_videoenc_start(stream[1].enc_path);
            printf("start encode\r\n");
        }
        if (key == '6') {
        	ret = set_enc_param(stream[0].enc_path, &stream[0].enc_dim, enc_type, vdo_br_rtsp);
        	ret = set_enc_param(stream[1].enc_path, &stream[1].enc_dim, enc_type, vdo_br_rtsp);
            printf("encode bitrate\r\n");
        }
		if (key == 'm') {
			g_show_gyro =1;
		}

		#if (DEBUG_MENU == 1)
		if (key == 'd') {
			// enter debug menu
			hd_debug_run_menu();
			printf("\r\nEnter q to exit, Enter d to debug\r\n");
		}
		#endif
	}

	// notify child process
	p_pd_shm = (PD_SHM_INFO  *)g_shm;
	p_pd_shm->exit = 1;
	sleep(1);

	// destroy encode thread
	pthread_join(stream[0].enc_thread_id, NULL);

	if (g_capbind == 1) {
		//direct NOTE: ensure videocap stop after all videoproc path stop
		hd_videoproc_stop(stream[0].proc0_path);
		hd_videocap_stop(stream[0].cap_path);
	} else {
		hd_videocap_stop(stream[0].cap_path);
		hd_videoproc_stop(stream[0].proc0_path);
	}
    // stop video_record modules (main)
	hd_videoproc_stop(stream[0].proc1_path);
#if OUT_NO_EIS
    // stop video_record modules (sub)
	hd_videoproc_stop(stream[1].proc1_path);
#endif
    // stop vout modules (dispaly)
	hd_videoproc_stop(stream[2].proc1_path);
	hd_videoenc_stop(stream[0].enc_path);
#if OUT_NO_EIS
	hd_videoenc_stop(stream[1].enc_path);
#endif
#if (BIND_BSMUXER == 1)
	stop_bsmuxer_module(&(stream[0].muxer_stream));
#if OUT_NO_EIS
	stop_bsmuxer_module(&(stream[1].muxer_stream));
#endif
#endif
	hd_videoout_stop(stream[2].out_path);

	if (g_capbind == 0xff) { //no-bind mode
		// destroy capture_thread
		pthread_join(stream[0].cap_thread_id, NULL);  //NOTE: before destory, call stop to breaking pull(-1)
	} else {
	    // unbind video_record modules (main)
	    hd_videocap_unbind(HD_VIDEOCAP_0_OUT_0);
    }
	hd_videoproc_unbind(HD_VIDEOPROC_0_OUT_0);
	hd_videoproc_unbind(HD_VIDEOPROC_1_OUT_0);
#if OUT_NO_EIS
	// unbind video_record modules (sub)
	hd_videoproc_unbind(HD_VIDEOPROC_1_OUT_1);
#endif
	// unbind vout modules (display)
	hd_videoproc_unbind(HD_VIDEOPROC_1_OUT_2);

exit:
	// close video_record modules (main)
	ret = close_module(&stream[0]);
	if (ret != HD_OK) {
		printf("close fail=%d\n", ret);
	}

	// close video_record modules (sub)
	ret = close_module_2(&stream[1]);
	if (ret != HD_OK) {
		printf("close fail=%d\n", ret);
	}
	// close vout modules (display)
	ret = close_module_3(&stream[2]);
	if (ret != HD_OK) {
		printf("close fail=%d\n", ret);
	}
#if (BIND_BSMUXER == 1)
	ret = close_bsmuxer_module(&(stream[0].muxer_stream));
	if (ret != HD_OK) {
		printf("close bsmuxer fail=%d\n", ret);
		goto exit;
	}
	ret = close_bsmuxer_module(&(stream[1].muxer_stream));
	if (ret != HD_OK) {
		printf("close bsmuxer1 fail=%d\n", ret);
		goto exit;
	}
#endif

	vendor_vpe_uninit();
	ret = vendor_eis_close();
	if (ret != HD_OK) {
		printf("eis close fail=%d\n", ret);
	}
    if (vendor_isp_uninit() == HD_ERR_NG) {
		return -1;
	}

	// uninit all modules
	ret = exit_module();
	if (ret != HD_OK) {
		printf("exit fail=%d\n", ret);
	}
#if (BIND_BSMUXER == 1)
	ret = exit_bsmuxer_module();
	if (ret != HD_OK) {
		printf("exit bsmuxer fail=%d\n", ret);
		goto exit;
	}
	exit_filesys_module(&(stream[0].muxer_stream));
#endif

	// uninit memory
	ret = mem_exit();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
	}

	// uninit hdal
	ret = hd_common_uninit();
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
	}
	exit_share_memory();

	return 0;
}
