/*
    Copyright   Novatek Microelectronics Corp. 2017.  All rights reserved.

    @file       ImageApp_MovieMulti_FileManage.h
    @ingroup    mIImageApp

    @note       Nothing.

    @date       2017/06/14
*/
#ifndef IA_MOVIEMULTI_FILE_MANAGE_H
#define IA_MOVIEMULTI_FILE_MANAGE_H
#include "ImageApp/ImageApp_MovieMulti.h"

#define IAMOVIEMULTI_FM_FDB_CNT   2
#define IAMOVIEMULTI_FM_DRV_FIRST  'A'
#define IAMOVIEMULTI_FM_DRV_LAST   (IAMOVIEMULTI_FM_DRV_FIRST + IAMOVIEMULTI_FM_FDB_CNT - 1)

#define IAMOVIEMULTI_FM_RESV_SIZE      10*1024*1024ULL
#define IAMOVIEMULTI_FM_RO_PCT         30
#define IAMOVIEMULTI_FM_RO_NUM         80
#define IAMOVIEMULTI_FM_CHK_PCT        1
#define IAMOVIEMULTI_FM_CHK_NUM        2

typedef enum {
	IAMOVIEMULTI_FM_ROINFO_BASE = 0,
	IAMOVIEMULTI_FM_ROINFO_PCT,
	IAMOVIEMULTI_FM_ROINFO_NUM,
	IAMOVIEMULTI_FM_ROINFO_TYPE,
	IAMOVIEMULTI_FM_ROINFO_MAX,
	ENUM_DUMMY4WORD(IAMOVIEMULTI_FM_ROINFO)
} IAMOVIEMULTI_FM_ROINFO;

INT32 iamoviemulti_fm_open(CHAR drive);
INT32 iamoviemulti_fm_close(CHAR drive);
INT32 iamoviemulti_fm_refresh(CHAR drive);
INT32 iamoviemulti_fm_config(MOVIE_CFG_FDB_INFO *p_cfg);

INT32 iamoviemulti_fm_is_loop_del(CHAR drive);
INT32 iamoviemulti_fm_is_add_to_last(CHAR drive);
INT32 iamoviemulti_fm_set_loop_del(CHAR drive, INT32 is_loop_del);
INT32 iamoviemulti_fm_set_add_to_last(CHAR drive, INT32 is_add_to_last);

INT32 iamoviemulti_fm_chk_time(UINT16 cre_date, UINT16 cre_time, UINT64 *time);
INT32 iamoviemulti_fm_chk_fdb(CHAR drive);
INT32 iamoviemulti_fm_add_file(CHAR *p_path); //filedb add
INT32 iamoviemulti_fm_del_file(CHAR drive, UINT64 need_size); //fs del + filedb del
INT32 iamoviemulti_fm_move_file(CHAR *p_old_path, CHAR *p_new_folder, CHAR *p_new_path); //fs move + filedb move
INT32 iamoviemulti_fm_set_ro_file(CHAR *p_path, BOOL is_ro);
INT32 iamoviemulti_fm_chk_ro_file(CHAR drive);
INT32 iamoviemulti_fm_chk_ro_size(CHAR drive, UINT64 remain_size);
INT32 iamoviemulti_fm_del_ro_file(CHAR drive, UINT64 *size);
INT32 iamoviemulti_fm_set_time_val(CHAR drive, INT32 value);
INT32 iamoviemulti_fm_get_time_val(CHAR drive, INT32 *value);
INT32 iamoviemulti_fm_set_file_del_max(CHAR drive, INT32 value);
INT32 iamoviemulti_fm_get_file_del_max(CHAR drive, INT32 *value);
INT32 iamoviemulti_fm_set_rofile_info(CHAR drive, INT32 value, INT32 type);
INT32 iamoviemulti_fm_get_rofile_info(CHAR drive, INT32 *value, INT32 type);

INT32 iamoviemulti_fm_set_resv_size(CHAR drive, UINT64 size, UINT32 is_loop); //1: loop recording on, 0: loop off
INT32 iamoviemulti_fm_get_resv_size(CHAR drive, UINT64 *p_size);
INT32 iamoviemulti_fm_get_remain_size(CHAR drive, UINT64 *p_size); //get fout remain size
INT32 iamoviemulti_fm_get_filedb_num(CHAR drive);

INT32 ImageApp_MovieMulti_FMOpen(CHAR drive);
INT32 ImageApp_MovieMulti_FMClose(CHAR drive);
INT32 ImageApp_MovieMulti_FMRefresh(CHAR drive);
INT32 ImageApp_MovieMulti_FMConfig(MOVIE_CFG_FDB_INFO *p_cfg);
void ImageApp_MovieMulti_FMSetSortBySN(CHAR *pDelimStr, UINT32 nDelimCount, UINT32 nNumOfSn);

#endif




