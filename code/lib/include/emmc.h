#ifndef _EMMC_H
#define _EMMC_H

#include <strg_def.h>

extern PSTORAGE_OBJ emmc_getStorageObject(STRG_OBJ_ID strgObjID);

#endif