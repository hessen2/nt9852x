/*
	@file       nvtboot.c
	@ingroup
	@note
	Copyright   Novatek Microelectronics Corp. 2021.  All rights reserved.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
*/

#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/time.h>
#include <linux/random.h>
#include <linux/usb.h>
#include <linux/fs.h>

#include "nvtboot.h"

static struct file*         gImageFileHandle;

//****************************************************
char gcImageFileName[U_UPDATE_TOTAL][64] = {
/*U_WRLOAD_IDX*/	WRLOADER_PATH, 	// Wrloader Name		<Run>
/*U_FWIMAG_IDX*/	FW_IMAGE_PATH, 	// FW Image Name 		<Run><Write-to-Flash>
/*U_LDIMAG_IDX*/	LD_IMAGE_PATH, 	// Loader Image Name	<Write-to-Flash>
};
//****************************************************

static bool write_loader_updated = false;

static void usb_nvtboot_cxin_blocking_completion(struct urb *urb)
{
	struct usb_nvtboot *nvtboot = urb->context;

	nvtboot->status = urb->status;
	if(nvtboot->status)
		printk("[%s]: err sts = %d\n",__func__,nvtboot->status);

	complete(&nvtboot->cx_done_in);
}
static void usb_nvtboot_cxout_blocking_completion(struct urb *urb)
{
	struct usb_nvtboot *nvtboot = urb->context;

	nvtboot->status = urb->status;
	if(nvtboot->status)
		printk("[%s]: err sts = %d\n",__func__,nvtboot->status);

	complete(&nvtboot->cx_done_out);
}

static void usb_nvtboot_bulkout_blocking_completion(struct urb *urb)
{
	struct usb_nvtboot *nvtboot = urb->context;

	nvtboot->status = urb->status;
	if(nvtboot->status)
		printk("[%s]: err sts = %d\n",__func__,nvtboot->status);

	complete(&nvtboot->bulk_done_out);
}

static void usb_nvtboot_setup_BulkOUT(struct usb_nvtboot *nvtboot, void *buf, size_t length,int epn)
{

	usb_fill_bulk_urb(nvtboot->bulk_urb_out,
		          nvtboot->usbdev,
		          usb_sndbulkpipe(nvtboot->usbdev, epn),
		          (void *)buf,
		          length,
		          usb_nvtboot_bulkout_blocking_completion,
		          nvtboot);
}

static int usb_nvtboot_cxin_issue_urb_blocking(struct usb_nvtboot *nvtboot)
{
	int 	ret;

	init_completion(&nvtboot->cx_done_in);

	nvtboot->cx_urb_in->actual_length = 0;
	ret = usb_submit_urb(nvtboot->cx_urb_in, GFP_KERNEL);
	if( ret ) {
		printk("[%s]: usb_submit_urb failed %d\n",__func__,ret);
		return ret;
	}

	if (!wait_for_completion_timeout(&nvtboot->cx_done_in, msecs_to_jiffies(8000))) {
		printk("[%s]: timeout",__func__);
		return -ETIMEDOUT;
	}

	return 0;
}

static int usb_nvtboot_cxout_issue_urb_blocking(struct usb_nvtboot *nvtboot)
{
	int 	ret;

	init_completion(&nvtboot->cx_done_out);

	nvtboot->cx_urb_out->actual_length = 0;
	ret = usb_submit_urb(nvtboot->cx_urb_out, GFP_KERNEL);
	if( ret ) {
		printk("[%s]: usb_submit_urb failed %d\n",__func__,ret);
		return ret;
	}

	if (!wait_for_completion_timeout(&nvtboot->cx_done_out, msecs_to_jiffies(8000))) {
		printk("[%s]: timeout",__func__);
		return -ETIMEDOUT;
	}

	return 0;
}

static int usb_nvtboot_bulkout_issue_urb_blocking(struct usb_nvtboot *nvtboot)
{
	int 	ret;

	init_completion(&nvtboot->bulk_done_out);

	nvtboot->bulk_urb_out->actual_length = 0;
	ret = usb_submit_urb(nvtboot->bulk_urb_out, GFP_KERNEL);
	if( ret ) {
		printk("[%s]: usb_submit_urb failed %d\n",__func__,ret);
		return ret;
	}

	if (!wait_for_completion_timeout(&nvtboot->bulk_done_out, msecs_to_jiffies(8000))) {
		printk("[%s]: timeout",__func__);
		return -ETIMEDOUT;
	}

	return 0;
}

int nvtboot_write_reg(struct usb_nvtboot *nvtboot, struct usb_ctrlrequest *req, u8 *buf, u32 address, u32 data)
{
	u32 *buf32;
	int 	ret;

	buf32 = (u32 *)buf;
	buf32[0] = address;
	buf32[1] = data;

	req->bRequestType = USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE;
	req->bRequest     = 0x03;
	req->wValue       = 0x0000;
	req->wIndex       = 0x0000;
	req->wLength      = 8;

	usb_fill_control_urb(nvtboot->cx_urb_out,
			     nvtboot->usbdev,
			     usb_sndctrlpipe(nvtboot->usbdev, 0),
			     (unsigned char *) req,
			     (void *)buf,
			     8,
			     usb_nvtboot_cxout_blocking_completion,
			     nvtboot);

	ret = usb_nvtboot_cxout_issue_urb_blocking(nvtboot);
	if(ret) {
		printk("cx out failed\n");
		return -1;
	}

	return 0;
}

int nvtboot_read_reg(struct usb_nvtboot *nvtboot, struct usb_ctrlrequest *req, u8 *buf, u32 address, u32 *data)
{
	u32 	*buf32;
	int 	ret;

	buf32 = (u32 *)buf;

	req->bRequestType = USB_DIR_IN | USB_TYPE_VENDOR | USB_RECIP_DEVICE;
	req->bRequest     = 0x04;
	req->wValue       = address&0xFFFF;
	req->wIndex       = (address>>16)&0xFFFF;
	req->wLength      = 4;

	usb_fill_control_urb(nvtboot->cx_urb_in,
			     nvtboot->usbdev,
			     usb_rcvctrlpipe(nvtboot->usbdev, 0),
			     (unsigned char *) req,
			     (void *)buf,
			     4,
			     usb_nvtboot_cxin_blocking_completion,
			     nvtboot);

	ret = usb_nvtboot_cxin_issue_urb_blocking(nvtboot);
	if(ret) {
		printk("cx in failed\n");
		return -1;
	}


	*data = *buf32;
	//printk("read data is 0x%08X\n",*data);

	return 0;
}


int nvtboot_read_result(struct usb_nvtboot *nvtboot, struct usb_ctrlrequest *req, u8 *buf, u8 *data)
{
	int 	ret;

	req->bRequestType = USB_DIR_IN | USB_TYPE_VENDOR | USB_RECIP_DEVICE;
	req->bRequest     = 0x03;
	req->wValue       = 0;
	req->wIndex       = 0;
	req->wLength      = 1;

	usb_fill_control_urb(nvtboot->cx_urb_in,
			     nvtboot->usbdev,
			     usb_rcvctrlpipe(nvtboot->usbdev, 0),
			     (unsigned char *) req,
			     (void *)buf,
			     4,
			     usb_nvtboot_cxin_blocking_completion,
			     nvtboot);

	ret = usb_nvtboot_cxin_issue_urb_blocking(nvtboot);
	if(ret) {
		printk("cx in failed\n");
		return -1;
	}


	*data = *buf;
	//printk("read data is 0x%08X\n",*data);

	return 0;
}


int nvtboot_finish(struct usb_nvtboot *nvtboot, struct usb_ctrlrequest *req, u32 address)
{
	int 	ret;

	req->bRequestType = USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE;
	req->bRequest     = 0x02;
	req->wValue       = address&0xFFFF;
	req->wIndex       = (address>>16)&0xFFFF;
	req->wLength      = 0;

	usb_fill_control_urb(nvtboot->cx_urb_out,
			     nvtboot->usbdev,
			     usb_sndctrlpipe(nvtboot->usbdev, 0),
			     (unsigned char *) req,
			     (void *)NULL,
			     0,
			     usb_nvtboot_cxout_blocking_completion,
			     nvtboot);

	ret = usb_nvtboot_cxout_issue_urb_blocking(nvtboot);
	if(ret) {
		printk("cx out failed\n");
		return -1;
	}

	return 0;
}

#if 1
#endif

static int nvtboot_thread(void * __nvttst)
{
	struct usb_nvtboot *nvtboot = (struct usb_nvtboot *)__nvttst;

	struct usb_ctrlrequest    *ctrlreq1, *ctrlreq2;
	u32 	*commandbuf;
	u8 		*image_buffer;
	int 	ret;
	//u32     data;
	int 	bufsize = 65536;
	struct  kstat stat;
	int 	error = 0;
	loff_t 	logstrs_size;

	nvtboot->cx_urb_in = NULL;
	nvtboot->cx_urb_out = NULL;
	nvtboot->bulk_urb_out = NULL;
	commandbuf = NULL;
	image_buffer = NULL;
	ctrlreq1 = NULL;
	ctrlreq2 = NULL;

	nvtboot->cx_urb_in = usb_alloc_urb(0, GFP_KERNEL);
	if (!nvtboot->cx_urb_in)
		goto fail1;

	nvtboot->cx_urb_out = usb_alloc_urb(0, GFP_KERNEL);
	if (!nvtboot->cx_urb_out)
		goto fail1;

	nvtboot->bulk_urb_out = usb_alloc_urb(0, GFP_KERNEL);
	if (!nvtboot->bulk_urb_out)
		goto fail1;

	ctrlreq1 = kmalloc(sizeof(struct usb_ctrlrequest), GFP_KERNEL);
	if(!ctrlreq1) {
		printk("[%s]: ctrlreq1 no mem\n",__func__);
		goto fail2;
	}
	ctrlreq2 = kmalloc(sizeof(struct usb_ctrlrequest), GFP_KERNEL);
	if(!ctrlreq2) {
		printk("[%s]: ctrlreq2 no mem\n",__func__);
		goto fail2;
	}

	commandbuf = kmalloc(32, GFP_KERNEL);
	if(!commandbuf) {
		printk("[%s]: commandbuf no mem\n",__func__);
		goto fail2;
	}
	image_buffer = kmalloc(bufsize, GFP_KERNEL);
	if(!image_buffer) {
		printk("[%s]: image-buf no mem\n",__func__);
		goto fail2;
	}
#if 0
	//check if write loader has been updated
	ret = nvtboot_read_reg(nvtboot, ctrlreq1, image_buffer, 0xF029001C, &data);
	if(ret) {
		printk("nvtboot_read_reg fail(%d), line%d\n", ret, __LINE__);
		goto fail3;
	}
#endif
	if(write_loader_updated == false) {
		printk("Update write_loader begin\n");
		//******************************************************
		// Start Sending USB WriteLoader Imager
		//******************************************************

		gImageFileHandle = filp_open(gcImageFileName[U_WRLOAD_IDX], O_RDONLY, 0);
		if (IS_ERR(gImageFileHandle)) {
			printk("unable to open backing file: %s\n", gcImageFileName[U_WRLOAD_IDX]);
			goto fail3;
		}

		error = vfs_stat(gcImageFileName[U_WRLOAD_IDX], &stat);
		if (error) {
			printk("%s: Failed to stat file %s \n", __FUNCTION__, gcImageFileName[U_WRLOAD_IDX]);
			goto fail3;
		}

		logstrs_size = stat.size;

		if(stat.size > bufsize) {
			printk("%s: file %s size is 0x%08X exceed buffer size\n", __FUNCTION__, gcImageFileName[U_WRLOAD_IDX], (int)stat.size);
			logstrs_size = bufsize;
			goto fail3;
		}

		if (kernel_read(gImageFileHandle, (char *)(image_buffer), logstrs_size, &gImageFileHandle->f_pos) != logstrs_size) {
			printk("%s: Failed to read file %s\n", __FUNCTION__, gcImageFileName[U_WRLOAD_IDX]);
			goto fail3;
		}

		filp_close(gImageFileHandle, NULL);
		//printk("%s: file %s size is 0x%08X\n", __FUNCTION__, gcImageFileName[0], (int)stat.size);


	    /*	BULK OUT */
		commandbuf[0] = USB_WRITE_ADDR;
		commandbuf[1] = stat.size;
		usb_nvtboot_setup_BulkOUT(nvtboot, commandbuf, 8, 1);
		ret = usb_nvtboot_bulkout_issue_urb_blocking(nvtboot);
		if(ret) {
			printk("bulk out error 1\n");
			goto fail3;
		}

		usb_nvtboot_setup_BulkOUT(nvtboot, image_buffer, stat.size,1);
		ret = usb_nvtboot_bulkout_issue_urb_blocking(nvtboot);
		if(ret) {
			printk("bulk out error 2\n");
			goto fail3;
		}
		#if 0
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, 0xF029001C, 0xAA);
		if(ret) {
			printk("nvtboot_write_reg error 3\n");
			goto fail3;
		}
		#endif
		write_loader_updated = true;
		nvtboot_finish(nvtboot, ctrlreq2, USB_WRITE_ADDR);


		printk("Update write_loader done\n");
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	    // Write loader will re-enumeration to high speed, so nvtboot_probe will be invoked again
	    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	} else {
		u32 image_size;

		write_loader_updated = false;
		#if 0
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, 0xF029001C, 0x00);
		if(ret) {
			goto fail3;
		}
		#endif
#if UPDATE_LOADER_ENABLE
		printk("Update loader begin\n");
		//******************************************************
		// Start Sending Loader Imager, This is writing to flash.
		//
		//******************************************************
		gImageFileHandle = filp_open(gcImageFileName[U_LDIMAG_IDX], O_RDONLY, 0);
		if (IS_ERR(gImageFileHandle)) {
			printk("unable to open backing file: %s\n", gcImageFileName[U_LDIMAG_IDX]);
			goto fail3;
		}

		error = vfs_stat(gcImageFileName[U_LDIMAG_IDX], &stat);
		if (error) {
			printk("%s: Failed to stat file %s \n", __FUNCTION__, gcImageFileName[U_LDIMAG_IDX]);
			goto fail3;
		}

		printk("%s: file %s size is 0x%08X\n", __FUNCTION__, gcImageFileName[U_LDIMAG_IDX], (int)stat.size);

		logstrs_size = stat.size;

		if(stat.size > bufsize) {
			printk("%s: file %s size is 0x%08X exceed buffer size\n", __FUNCTION__, gcImageFileName[U_LDIMAG_IDX], (int)stat.size);
			logstrs_size = bufsize;
			goto fail3;
		}
		if (kernel_read(gImageFileHandle, (char *)(image_buffer), logstrs_size, &gImageFileHandle->f_pos) != logstrs_size) {
			printk("%s: Failed to read file %s\n", __FUNCTION__, gcImageFileName[U_LDIMAG_IDX]);
			goto fail3;
		}

		filp_close(gImageFileHandle, NULL);

	    /*	BULK OUT */
		commandbuf[0] = LOADER_ADDR;
		commandbuf[1] = stat.size;
		usb_nvtboot_setup_BulkOUT(nvtboot, commandbuf, 8, 1);
		ret = usb_nvtboot_bulkout_issue_urb_blocking(nvtboot);
		if(ret) {
			printk("bulk out error 1\n");
			goto fail3;
		}

		usb_nvtboot_setup_BulkOUT(nvtboot, image_buffer, stat.size,1);
		ret = usb_nvtboot_bulkout_issue_urb_blocking(nvtboot);
		if(ret) {
			printk("bulk out error 2\n");
			goto fail3;
		}

		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR, 0xABCD0043);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR+4, 0xABCD0001);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR+8, LOADER_ADDR);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR+0xC, stat.size);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		printk("Update loader done\n");
#else
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR, 0x0);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR+4, 0x0);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR+8, 0x0);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR+0xC, 0);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
#endif

		//******************************************************
		// Start Sending FW Image.
		//******************************************************
		printk("Update FW begin\n");
		gImageFileHandle = filp_open(gcImageFileName[U_FWIMAG_IDX], O_RDONLY, 0);
		if (IS_ERR(gImageFileHandle)) {
			printk("unable to open backing file: %s\n", gcImageFileName[U_FWIMAG_IDX]);
			goto fail3;
		}

		error = vfs_stat(gcImageFileName[U_FWIMAG_IDX], &stat);
		if (error) {
			printk("%s: Failed to stat file %s \n", __FUNCTION__, gcImageFileName[U_FWIMAG_IDX]);
			goto fail3;
		}

		printk("%s: file %s size is 0x%08X\n", __FUNCTION__, gcImageFileName[U_FWIMAG_IDX], (int)stat.size);


		commandbuf[0] = FW_ADDR;
		commandbuf[1] = stat.size;
		usb_nvtboot_setup_BulkOUT(nvtboot, commandbuf, 8, 1);
		ret = usb_nvtboot_bulkout_issue_urb_blocking(nvtboot);
		if(ret) {
			printk("bulk out error 4\n");
			goto fail3;
		}


		image_size = stat.size;

		while(image_size) {

			if(image_size >= bufsize)
				logstrs_size = bufsize;
			else
				logstrs_size = image_size;

			if (kernel_read(gImageFileHandle, (char *)(image_buffer), logstrs_size, &gImageFileHandle->f_pos) != logstrs_size) {
				printk("%s: Failed to read file %s\n", __FUNCTION__, gcImageFileName[U_FWIMAG_IDX]);
				goto fail3;
			}

			usb_nvtboot_setup_BulkOUT(nvtboot, image_buffer, logstrs_size, 1);
			ret = usb_nvtboot_bulkout_issue_urb_blocking(nvtboot);
			if(ret) {
				printk("bulk out error 5, remain=0x%X\n", image_size);
				break;
			}

			image_size -= logstrs_size;
		}


		filp_close(gImageFileHandle, NULL);

		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR+0x10, 0xABCD0043);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR+0x14, 0xABCD0002);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR+0x18, FW_ADDR);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		ret = nvtboot_write_reg(nvtboot, ctrlreq2, (u8 *)commandbuf, CONFIG_ADDR+0x1C, stat.size);
		if(ret) {
			printk("nvtboot_write_reg error %d\n", __LINE__);
			goto fail3;
		}
		nvtboot_finish(nvtboot, ctrlreq2, 0x00000000);

		printk("Update FW done\n");

		#if 0
		data = 0;
		nvtboot_read_result(nvtboot, ctrlreq1, (u8 *)commandbuf, (u8 *)&data);
		if (data == 0x5)
			printk("nvtboot pass\r\n");
		else
			printk("nvtboot failed err-code=%d\r\n", data);
		#endif

	}

fail3:


fail2:

	if(ctrlreq1)
		kfree(ctrlreq1);

	if(ctrlreq2)
		kfree(ctrlreq2);

	if(commandbuf)
		kfree(commandbuf);

	if(image_buffer)
		kfree(image_buffer);

fail1:

	if(nvtboot->cx_urb_in)
		usb_free_urb(nvtboot->cx_urb_in);

	if(nvtboot->cx_urb_out)
		usb_free_urb(nvtboot->cx_urb_out);

	while(!kthread_should_stop())
	{
		msleep(500);
	}

	complete(&nvtboot->cx_close);
	//printk("[%s]: thread closed.\n",__func__);
	return 0;
}

#if 1
#endif

static int nvtboot_probe(struct usb_interface *intf, const struct usb_device_id *id)
{
	struct usb_device *dev = interface_to_usbdev(intf);
	struct usb_nvtboot *nvtboot;
	struct task_struct *th;

	/*
	printk("match_flags        0x%X\n",id->match_flags);
	printk("idVendor           0x%X\n",id->idVendor);
	printk("idProduct          0x%X\n",id->idProduct);
	printk("bcdDevice_lo       0x%X\n",id->bcdDevice_lo);
	printk("bcdDevice_hi       0x%X\n",id->bcdDevice_hi);
	printk("bDeviceClass       0x%X\n",id->bDeviceClass);
	printk("bDeviceSubClass    0x%X\n",id->bDeviceSubClass);
	printk("bDeviceProtocol    0x%X\n",id->bDeviceProtocol);
	printk("bInterfaceClass    0x%X\n",id->bInterfaceClass);
	printk("bInterfaceSubClass 0x%X\n",id->bInterfaceSubClass);
	printk("bInterfaceProtocol 0x%X\n",id->bInterfaceProtocol);
	printk("bInterfaceNumber   0x%X\n",id->bInterfaceNumber);
	*/

	nvtboot = kzalloc(sizeof(struct usb_nvtboot), GFP_KERNEL);
	if(!nvtboot)
		return -ENOMEM;

	if(dev->manufacturer)
		strlcpy(nvtboot->name, dev->manufacturer, sizeof(nvtboot->name));

	if (dev->product) {
		if (dev->manufacturer)
			strlcat(nvtboot->name, " ", sizeof(nvtboot->name));
		strlcat(nvtboot->name, dev->product, sizeof(nvtboot->name));
	}

	nvtboot->cx_thread    = NULL;
	nvtboot->inf = intf;
	nvtboot->usbdev = dev;

	init_completion(&nvtboot->cx_close);

	usb_set_intfdata(intf, nvtboot);

	th = kthread_run(nvtboot_thread, nvtboot, "nvtboot_thread");
	if (IS_ERR(th)) {
		printk("+%s[%d]:  thread open error\n",__func__,__LINE__);
		goto fail1;
	}
	nvtboot->cx_thread = th;

	printk("%s:device-name = %s\n",__func__,nvtboot->name);
	return 0;

fail1:
	kfree(nvtboot);
	return -ENOMEM;

}

static void nvtboot_disconnect(struct usb_interface *intf)
{
	struct usb_nvtboot *nvtboot = usb_get_intfdata(intf);

	if (nvtboot->cx_thread)
		kthread_stop(nvtboot->cx_thread);
	else
		complete(&nvtboot->cx_close);

	if(!wait_for_completion_timeout(&nvtboot->cx_close, msecs_to_jiffies(5000)))
		printk("[%s]:wait thread close timeout\n",__func__);

	kfree(nvtboot);
}

static struct usb_device_id nvtboot_id_table [] = {
	{
		.match_flags =    USB_DEVICE_ID_MATCH_VENDOR |
				  		USB_DEVICE_ID_MATCH_PRODUCT | USB_DEVICE_ID_MATCH_DEV_LO,
		.idVendor    =    0x0603,
		.idProduct   =    0x8610,
		.bcdDevice_lo=    1
	},
	{ }	/* Terminating entry */
};

MODULE_DEVICE_TABLE (usb, nvtboot_id_table);

static struct usb_driver usb_nvtboot_driver = {
	.name		="nvtboot",
	.probe		= nvtboot_probe,
	.disconnect	= nvtboot_disconnect,
	.id_table	= nvtboot_id_table,
};

module_usb_driver(usb_nvtboot_driver);

MODULE_AUTHOR("Ben Wang <Ben_Wang@novatek.com.tw>");
MODULE_DESCRIPTION("Novatek USB BOOT");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.00.000");
