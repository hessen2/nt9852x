/*
	@file       nvtboot.h
	@ingroup
	@note
	Copyright   Novatek Microelectronics Corp. 2021.  All rights reserved.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
*/
#ifndef _NVTBOOT_H
#define _NVTBOOT_H



#define UPDATE_LOADER_ENABLE  0


#define U_WRLOAD_IDX 	0
#define U_FWIMAG_IDX 	1
#define U_LDIMAG_IDX 	2

#define U_UPDATE_TOTAL	3

#define WRLOADER_PATH "/mnt/sd/Wrldr528.bin" 	// Write-loader Name
#define FW_IMAGE_PATH "/mnt/sd/FW98528A.bin" 	// FW Image Name
#define LD_IMAGE_PATH "/mnt/sd/LD98528A.bin" 	// Loader Image Name

//must be the same with NvtUSBD.ini
#define USB_WRITE_ADDR 0xF07C0000
#define FW_ADDR        0x02000000
#define CONFIG_ADDR    0x05000000
#define LOADER_ADDR    0x05800000

struct usb_nvtboot {

	char name[128];
	struct usb_interface 	*inf;
	struct usb_device 		*usbdev;

	struct task_struct 		*cx_thread;
	struct urb 				*cx_urb_in,*cx_urb_out;
	struct completion	     cx_done_in,cx_done_out;
	struct completion	     cx_close;
	struct urb 				*bulk_urb_out;
	struct completion	     bulk_done_out;
	int			         	 status;
};




#endif