/**
    @file       gpio_Irq.c
    @ingroup
    @note
    Copyright   Novatek Microelectronics Corp. 2021.  All rights reserved.
**/

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <plat/nvt-gpio.h>
#include <linux/delay.h>

#define RED   "\x1B[31m"
#define RESET "\x1B[0m"

#define BUTTON C_GPIO(5)
#define MY_GPIO_INT_NAME "my_button_int"
#define MY_DEV_NAME "my_device"

static int button_irq = 0;

static irqreturn_t gpio_irq_test_isr(int irq, void *data)
{
	printk("START "RED"button_isr !!!"RESET", func=%s, line=%d\n", __FUNCTION__, __LINE__);
	mdelay(1000);
	printk("END "RED"button_isr !!!"RESET", func=%s, line=%d\n", __FUNCTION__, __LINE__);
	return IRQ_HANDLED;
}

static int __init gpio_irq_test_init(void)
{
	int ret = 0;

	ret = gpio_is_valid(BUTTON);
	if (!ret) {
		return -1;
	}
	ret = gpio_request(BUTTON,"BUTTON");
	if (ret < 0) {
		return -1;
	}

	gpio_direction_input(BUTTON);

	button_irq = gpio_to_irq(BUTTON);
	if (button_irq < 0) {
		return -1;
	}
	ret = request_irq( button_irq, gpio_irq_test_isr, IRQF_TRIGGER_FALLING, MY_GPIO_INT_NAME, MY_DEV_NAME);
	if (ret) {
		return -1;
	}
	printk(" Function %s sucessfully. \n", __FUNCTION__);

	return 0;
}

static void __exit gpio_irq_test_exit(void)
{
	free_irq(button_irq, MY_DEV_NAME);
	gpio_free(BUTTON);
}

module_init(gpio_irq_test_init);
module_exit(gpio_irq_test_exit);
MODULE_LICENSE("GPL");