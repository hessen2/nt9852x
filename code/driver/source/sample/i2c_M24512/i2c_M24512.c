/**
    @file       i2c_M24512.c
    @ingroup
    @note
    Copyright   Novatek Microelectronics Corp. 2021.  All rights reserved.
**/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/errno.h>
#include <linux/err.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/slab.h>

#define EEPROM_MAJOR	100
#define EEPROM_MINOR	0
#define EEPROM_NAME		"EEPROM_M24512"

struct eeprom_data {
	u8 addr[2];
	u8 data;
	struct i2c_msg i2c_msg[2];
	struct i2c_client *i2c_client;
	struct class *i2c_class;
	struct device *i2c_dev;
	struct cdev	*p_cdev;
};
struct eeprom_data *ee_dev;

static int nvt_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id);
static int nvt_i2c_remove(struct i2c_client *client);

static int i2c_eeprome_open(struct inode *inode, struct file *file)
{
    file->private_data = ee_dev;
    return 0;
}

static loff_t i2c_eeprom_llseek(struct file *file, loff_t offset, int whence)
{
	loff_t ret = -EINVAL;

	switch (whence) {
	case SEEK_END:
	case SEEK_DATA:
	case SEEK_HOLE:
		return -EINVAL;
	case SEEK_CUR:
		if (offset == 0)
			return file->f_pos;

		offset += file->f_pos;
		break;
	case SEEK_SET:
		file->f_pos = offset;
		break;
	}

	if (offset != file->f_pos) {
		file->f_pos = offset;
		file->f_version = 0;
		ret = offset;
	}

	return ret;
}

static int i2c_eeprom_write (struct file *filp, const char __user *buf,
							size_t count, loff_t *f_pos)
{
	struct eeprom_data *pData = (struct eeprom_data *)filp->private_data;
	int ret = 0;

	if (copy_from_user(&pData->data, buf, count)) {
		printk(" Copy from userspace fail. func=%s \n", __FUNCTION__);
		return -EFAULT;
	}

	pData->addr[0]  = (char)(*f_pos >> 8);
	pData->addr[1]  = (char)(*f_pos);
	pData->i2c_msg[0].addr  = pData->i2c_client->addr;
	pData->i2c_msg[0].flags = 0;
	pData->i2c_msg[0].len   = count+2;
	pData->i2c_msg[0].buf   = &pData->addr[0];

	ret = i2c_transfer(pData->i2c_client->adapter, &pData->i2c_msg[0], 1);
	msleep(3);

	if ( ret < 0) {
		printk("Wrtie fail, i2c transfer failed. func=%s, \n", __FUNCTION__);
		return -EIO;
	}
	*f_pos +=count;
	return 0;
}

static int i2c_eeprom_read(struct file *filp, char __user *buf, size_t count,
							loff_t *f_pos)
{
	struct eeprom_data *pData = (struct eeprom_data *)filp->private_data;
	int ret = 0;

	pData->addr[0]  = (char)(*f_pos >> 8);
	pData->addr[1]  = (char)(*f_pos);
	pData->i2c_msg[0].addr  = pData->i2c_client->addr;
	pData->i2c_msg[0].flags = 0;
	pData->i2c_msg[0].len   = 2;
	pData->i2c_msg[0].buf   = &pData->addr[0];

	pData->i2c_msg[1].addr  = pData->i2c_client->addr;
	pData->i2c_msg[1].flags = I2C_M_RD;
	pData->i2c_msg[1].len   = 1;
	pData->i2c_msg[1].buf   = &pData->addr[0];

	ret = i2c_transfer(pData->i2c_client->adapter, &pData->i2c_msg[0], 2);
	if ( ret < 0) {
		printk(" Read fail, i2c transfer failed. func=%s\n", __FUNCTION__);
		return -EIO;
	}
	msleep(3);
	if (copy_to_user( buf , &pData->addr[0] , 1)) {
		printk("Copy to user fail. func=%s \n", __FUNCTION__);
		return -EFAULT;
	}

	*f_pos +=count;
	return 0;
}

struct file_operations i2c_eeprom_ops =
{
	.owner  = THIS_MODULE,
	.open   = i2c_eeprome_open,
	.read   = i2c_eeprom_read,
	.write  = i2c_eeprom_write,
	.llseek = i2c_eeprom_llseek,
};

static const struct i2c_device_id nvt_i2c_id[] = {
	{ "nvt_i2c_M24512", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, nvt_i2c_id);

static const struct of_device_id nvt_i2c_of_match[] = {
	{ .compatible = "nvt,nvt_i2c_M24512" },
	{}
};
MODULE_DEVICE_TABLE(of, nvt_i2c_of_match);

static struct i2c_driver nvt_Irn_i2c_driver = {
	.id_table = nvt_i2c_id,
	.probe    = nvt_i2c_probe,
	.remove   = nvt_i2c_remove,
	.driver = {
		.name   = "nvt_i2c_M24512",
		.owner  = THIS_MODULE,
		.of_match_table = nvt_i2c_of_match,
	},
};

static int nvt_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int ret = 0;

	ee_dev = kmalloc(sizeof(struct eeprom_data), GFP_KERNEL);
	if (!ee_dev) {
		printk(" Kmalloc eeprom_data fail. func=%s \n", __FUNCTION__);
        ret = -ENOMEM;
	}
	memset(ee_dev, 0, sizeof(struct eeprom_data));
	ee_dev->i2c_client = client;
	printk("Slave addr:%#x\n",client->addr);

	ee_dev->p_cdev = cdev_alloc();
	cdev_init(ee_dev->p_cdev, &i2c_eeprom_ops);
	ee_dev->p_cdev->owner = THIS_MODULE;
	ee_dev->p_cdev->dev=MKDEV(EEPROM_MAJOR, EEPROM_MINOR);
	ret = register_chrdev_region( ee_dev->p_cdev->dev, 1, EEPROM_NAME);
	if ( ret != 0 ) {
		printk("I2C EEPROM device driver register region failed. \n");
		return -EINVAL;
	}

	ret = cdev_add(ee_dev->p_cdev, ee_dev->p_cdev->dev, 1);
	if ( ret != 0 ) {
		printk("Add I2C EEPROM character device failed. \n");
		return -EINVAL;
	}
	ee_dev->i2c_class = class_create(THIS_MODULE ,EEPROM_NAME);
	ee_dev->i2c_dev = device_create(ee_dev->i2c_class, NULL, \
	                                ee_dev->p_cdev->dev, NULL, EEPROM_NAME);
	if(ee_dev->i2c_dev == NULL) {
		printk("failed in creating device.\n");
		return -EINVAL;
	}

	printk("Add I2C EEPROM device driver success. \n");
	return 0;
}

static int nvt_i2c_remove(struct i2c_client *client)
{
	cdev_del(ee_dev->p_cdev);
	unregister_chrdev_region(ee_dev->p_cdev->dev,1);
	device_destroy(ee_dev->i2c_class, ee_dev->p_cdev->dev);
	class_destroy(ee_dev->i2c_class);
	kfree(ee_dev);
	printk("I2C EEPROM exit. \n");
	return 0;
}

module_i2c_driver(nvt_Irn_i2c_driver);

MODULE_DESCRIPTION("I2C device driver test. ");
MODULE_LICENSE("GPL");