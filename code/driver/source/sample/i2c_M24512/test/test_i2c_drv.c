/**
    @file       i2c_M24512.c
    @ingroup
    @note
    Copyright   Novatek Microelectronics Corp. 2021.  All rights reserved.
**/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#define DEV_NAME 	"/dev/EEPROM_M24512"

int main(int argc, char *argv[])
{
	int	fd, i=0, j=0;
	off_t ret_l = 0;
	ssize_t ret_w = 0, ret_r = 0;
	char ch_tx = 0x56;
	char ch_rx = 0x00;
	fd = open(DEV_NAME,O_RDWR);
	if(fd<0) {
		printf("USP_ opne device fail.");
		goto error;
	}

	printf(" ====================START===========================\n");
	printf(" At address 0x30, Write Data is : \n");
	ret_l = lseek(fd,0x30,SEEK_SET);
	if(ret_l == -1) {
		printf("lseek fail. ");
		goto error;
	}
	for(i=0; i<0x30; i++) {
		ret_w = write(fd, &ch_tx, 1);
		if(ret_w == -1) {
			printf("write fail. ");
			goto error;
		}
		printf(" %02X", ch_tx);
		if( (i==0xf) || (i==0x1f) || (i==0x2f))
			printf("\n");
	}
	printf("\n");

	printf(" At address 0x30, Read Data is :\n");
	ret_l = lseek(fd,0x30,SEEK_SET);
	if(ret_l == -1) {
		printf("lseek fail. ");
		goto error;
	}
	for(i=0; i<0x30; i++) {
		ret_r = read(fd, &ch_rx, 1);
		if(ret_r == -1) {
			printf("read fail. ");
			goto error;
		}
		printf(" %02X", ch_rx);
		if( (i==0xf) || (i==0x1f) || (i==0x2f))
			printf("\n");
	}
	printf("\n");
	printf(" =====================END============================\n");

	if(ch_tx == ch_rx)
		printf(" Read&WRITE is the SAME. \n");
	else
		printf(" Read&WRITE is NOT the SAME. \n");

	printf(" ====================START===========================\n");
	printf(" Address: 0x30, Read Data is \n");
	ret_l = lseek(fd,0x30,SEEK_SET);
	if(ret_l == -1) {
		printf("lseek fail. ");
		goto error;
	}
	for (i=0; i<0x100; i=i+0x10) {
		for(j=0; j<0x10; j=j+0x01) {
			ret_r = read(fd, &ch_rx, 1);
			if(ret_r == -1) {
				printf("read fail. ");
				goto error;
			}
			printf(" %02X", ch_rx);
		}
		printf(" \n", ch_rx);
	}
	printf(" =====================END============================\n");

	close(fd);
	return 0;

error:
	close(fd);
	return -EINVAL;
}