I2C eeprom M24512 driver

Required properties:
- compatible: Should be "nvt,nvt_i2c_M24512".
- here use i2c bus 1 for an example.

Should add below content in file \configs\cfg_gen\nvt-na51055-peri-dev.dtsi
Example:
&i2c1 {
	#address-cells = <1>;
	#size-cells = <0>;
	nvt_i2c_M24512@50 {
		compatible = "nvt,nvt_i2c_M24512";
		reg = <0x50>;
	};
};


Usage of this driver:
insmod /lib/modules/<kernel_ver>/extra/sample/i2c_test/nct_i2c_M24512.ko

Usage in user space:
Before calling read or write function, you should call lseek and then
assign value to cRx for offset to assign the address of eeprom that
you would like to read or write.

Example:
lseek(fd, 0x50, SEEK_SET);
cRx = 0;
read (fd, &cRx, 1)
It means you want to read from address 0x50.

lseek(fd, 0x50, SEEK_SET);
cRx = 2;
read (fd, &cRx, 1)
It means you want to read from address 0x52.