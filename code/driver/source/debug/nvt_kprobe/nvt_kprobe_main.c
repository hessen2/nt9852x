/*
 * nvt_kprobe_main.c - To add probe point into specific function
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * usage:
 *	modprobe nvt_data_breakpoint ksym=<ksym_name> wp_chk_val=<Value>
 *	modprobe nvt_data_breakpoint wp_addr=<Address> wp_chk_val=<Value>
 *
 * Modified by: Wayne Lin <Wayne_Lin@novatek.com.tw>
 */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/init.h>		/* Needed for the macros */
#include <linux/kallsyms.h>

#include <linux/perf_event.h>
#include <linux/hw_breakpoint.h>
#include <linux/kprobes.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/dma-mapping.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <mach/nvt-io.h>

#define MAX_SYMBOL_LEN 64
/* MMC isr symbol */
static char symbol[MAX_SYMBOL_LEN] = "nvt_version_show";
module_param_string(symbol, symbol, sizeof(symbol), 0644);
static long long times = 0;
/* For each probe you need to allocate a kprobe structure */
static struct kprobe kp = {
	.symbol_name = symbol,
};

static int __kprobes handler_pre(struct kprobe *p, struct pt_regs *regs)
{
	times++;
	if (times >= LLONG_MAX - 1) {
			times = 0;
	}

	pr_info("<ztex><%s> pre_handler: p->addr = 0x%p, times=%lld, nmissed=%lu\n",
				p->symbol_name, p->addr, times, p->nmissed);
	dump_stack();

	return 0;
}

static void handler_post (struct kprobe *p, struct pt_regs *regs, unsigned long flags)
{
	pr_info("%s %d\n", __func__, __LINE__);
	return;
}

static int handler_fault (struct kprobe *p, struct pt_regs *regs, int trapnr)
{
	pr_info("%s %d\n", __func__, __LINE__);
	return 0;
}

static int nvt_kprobe_module_init(void)
{
	int ret;

	kp.pre_handler = handler_pre;
	kp.post_handler = handler_post;
	kp.fault_handler = handler_fault;

	ret = register_kprobe(&kp);
	if (ret < 0) {
		pr_err("register_kprobe failed, returned %d\n", ret);
		return ret;
	}

	pr_info("%s: Register kprobe successfully\n", __func__);
	return 0;
}

static void __exit nvt_kprobe_module_exit(void)
{
	unregister_kprobe(&kp);
	pr_info("%s: Remove kprobe\n", __func__);
}

module_init(nvt_kprobe_module_init);
module_exit(nvt_kprobe_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("NOVATEK");
MODULE_DESCRIPTION("Novatek KPROBE driver");
