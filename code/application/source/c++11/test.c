#include <iostream>

using namespace std;

int
main(void)
{
	int Array[5];

	for (int &i : Array) {
		cout << "Next element:";
		cin >> i;
	}

	cout << "Elements in array =" << endl;
	for (int i : Array) {
		cout << i << endl;
	}

	return 0;
}
