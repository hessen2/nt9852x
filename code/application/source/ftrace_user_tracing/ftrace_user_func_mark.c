#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <linux/unistd.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <linux/kernel.h>       /* for struct sysinfo */
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <ctype.h>

static void sysfs_write(char *path, char *s)
{
	char buf[80];
	int len;
	int fd = open(path, O_WRONLY);

	if (fd < 0) {
		strerror_r(errno, buf, sizeof(buf));
		return;
	}
	len = write(fd, s, strlen(s) + 1);
	if (len < 0) {
		strerror_r(errno, buf, sizeof(buf));
	}

	close(fd);
}

static ssize_t sysfs_read(char *path, char *s, int num_bytes)
{
	char buf[80];
	ssize_t count = 0;
	int fd = open(path, O_RDONLY);

	if (fd < 0) {
		strerror_r(errno, buf, sizeof(buf));
		return -1;
	}

	if ((count = read(fd, s, (num_bytes -1))) < 0) {
		strerror_r(errno, buf, sizeof(buf));
	} else {
		if ((count >= 1) && (s[count-1] == '\n')) {
			s[count-1] = '\0';
		} else {
			s[count] = '\0';
		}
	}

	close(fd);

	return count;
}

int main()
{
	int count = 0;
	/* Clean trace buffer */
	sysfs_write("/sys/kernel/debug/tracing/trace", "0");
	/* Add marker */
	sysfs_write("/sys/kernel/debug/tracing/trace_marker", "MyFunc_Start\n");

	while (count < 50) {
		usleep(100*1000);
		/* Add marker */
		sysfs_write("/sys/kernel/debug/tracing/trace_marker", "MyFunc_temp\n");
		count++;
	}
	/* Add marker */
	sysfs_write("/sys/kernel/debug/tracing/trace_marker", "MyFunc_end\n");

	return 0;
}