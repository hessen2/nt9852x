root@NVTEVM:~$ ftrace_user_func_mark
root@NVTEVM:~$ cat /sys/kernel/debug/tracing/trace
# tracer: nop
#
# entries-in-buffer/entries-written: 52/52   #P:1
#
#                              _-----=> irqs-off
#                             / _----=> need-resched
#                            | / _---=> hardirq/softirq
#                            || / _--=> preempt-depth
#                            ||| /     delay
#           TASK-PID   CPU#  ||||    TIMESTAMP  FUNCTION
#              | |       |   ||||       |         |
           <...>-676   [000] ....     9.461738: tracing_mark_write: MyFunc_Start
           <...>-676   [000] ....     9.570275: tracing_mark_write: MyFunc_temp				// 0.1 sec. (it can meet code behavior)
           <...>-676   [000] ....     9.680270: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....     9.790264: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....     9.900262: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    10.010258: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    10.120263: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    10.230264: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    10.340262: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    10.450262: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    10.560267: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    10.670262: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    10.780267: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    10.890263: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    11.000259: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    11.110263: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    11.220266: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    11.330262: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    11.440262: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    11.550261: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    11.660266: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    11.770269: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    11.880265: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    11.990264: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    12.100262: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    12.210266: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    12.320263: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    12.430263: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    12.540261: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    12.650266: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    12.760262: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    12.870268: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    12.980265: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    13.090264: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    13.200266: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    13.310262: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    13.420264: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    13.530264: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    13.640263: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    13.750267: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    13.860270: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    13.970266: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    14.080263: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    14.190268: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    14.300265: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    14.410266: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    14.520264: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    14.630264: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    14.740270: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    14.850267: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    14.960270: tracing_mark_write: MyFunc_temp
           <...>-676   [000] ....    14.960325: tracing_mark_write: MyFunc_end
