/**
	@brief Vendor ai implementation version.

	@file net_flow_ai_version.h

	@ingroup net_flow_ai

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2019.  All rights reserved.
*/
#ifndef _KFLOW_AI_VERSION_H_
#define _KFLOW_AI_VERSION_H_


#define KFLOW_AI_IMPL_VERSION      "02.18.2211240" //implementation version

#endif
