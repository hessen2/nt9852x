/**
	@brief Header file of Custom NN Neural Network functions operating using CPU.

	@file net_cust_version.h

	@ingroup custnn

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2019.  All rights reserved.
*/
#ifndef _CUSTNN_VER_H_
#define _CUSTNN_VER_H_

#define AI_CUST_IMPL_VERSION    "01.00.2201270"

#endif  /* _CUSTNN_VER_H_ */
