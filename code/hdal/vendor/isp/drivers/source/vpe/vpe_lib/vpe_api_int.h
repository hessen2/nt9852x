#ifndef _VPE_API_INT_H_
#define _VPE_API_INT_H_

#include "ctl_vpe_isp.h"
#include "vpe_alg.h"

extern INT32 vpe_api_cb_flow(ISP_ID id, ISP_EVENT evt, UINT32 frame_cnt, void *param);

#endif

