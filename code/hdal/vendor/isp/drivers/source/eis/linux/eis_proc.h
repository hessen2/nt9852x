#ifndef _EIS_PROC_H_
#define _EIS_PROC_H_
#include "eis_main.h"

int nvt_eis_proc_init(PEIS_DRV_INFO pdrv_info);
int nvt_eis_proc_remove(PEIS_DRV_INFO pdrv_info);

#endif
