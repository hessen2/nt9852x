#include <linux/module.h>
#include "kdrv_videoprocess/kdrv_vpe_if.h"

EXPORT_SYMBOL(kdrv_vpe_if_trigger);
EXPORT_SYMBOL(kdrv_vpe_if_get);
EXPORT_SYMBOL(kdrv_vpe_if_set);
EXPORT_SYMBOL(kdrv_vpe_if_open);
EXPORT_SYMBOL(kdrv_vpe_if_close);
EXPORT_SYMBOL(kdrv_vpe_if_is_init);
EXPORT_SYMBOL(kdrv_vpe_if_register);