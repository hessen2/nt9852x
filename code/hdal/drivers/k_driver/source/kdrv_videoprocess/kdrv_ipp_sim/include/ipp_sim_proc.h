#ifndef __IPP_SIM_PROC_H_
#define __IPP_SIM_PROC_H__

#include "ipp_sim_main.h"

int nvt_ipp_sim_proc_init(PIPP_SIM_DRV_INFO pdrv_info);
int nvt_ipp_sim_proc_remove(PIPP_SIM_DRV_INFO pdrv_info);


#endif