#ifndef __IFE2_PROC_H_
#define __IFE2_PROC_H__
#include "ife2_main.h"

int nvt_ife2_proc_init(PIFE2_DRV_INFO pdrv_info);
int nvt_ife2_proc_remove(PIFE2_DRV_INFO pdrv_info);


#endif