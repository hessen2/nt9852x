#ifndef __VPE_PROC_H__
#define __VPE_PROC_H__

#include "vpe_main.h"

int nvt_vpe_proc_init(PVPE_DRV_INFO pdrv_info);
int nvt_vpe_proc_remove(PVPE_DRV_INFO pdrv_info);


#endif