
/*
    VPE module driver

    NT98528 VPE internal header file.

    @file       vpe_compression_base.h
    @ingroup    mIIPPVPE
    @note       Nothing

    Copyright   Novatek Microelectronics Corp. 2019.  All rights reserved.
*/


#ifndef _VPE_COMPRESSION_REG_
#define _VPE_COMPRESSION_REG_

#ifdef __cplusplus
extern "C" {
#endif

#include "vpe_platform.h"

extern VOID vpe_set_compression_coefs_reg(VOID);

#ifdef __cplusplus
}
#endif


#endif // _VPE_COMPRESSION_REG_
