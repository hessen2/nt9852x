#ifndef __IPE_PROC_H_
#define __IPE_PROC_H__
#include "ipe_main.h"

int nvt_ipe_proc_init(PIPE_DRV_INFO pdrv_info);
int nvt_ipe_proc_remove(PIPE_DRV_INFO pdrv_info);


#endif