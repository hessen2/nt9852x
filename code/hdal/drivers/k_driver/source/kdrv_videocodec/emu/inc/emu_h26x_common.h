#ifndef _EMU_H26X_COMMON_H_
#define _EMU_H26X_COMMON_H_
//#include "emulation.h"


//#if (EMU_H26X == ENABLE || AUTOTEST_H26X == ENABLE)

#include "emu_h26x_job.h"
#include "emu_h26x_mem.h"
#include "emu_h26x_file.h"
#include "kdrv_vdocdc_dbg.h"


//#endif // (EMU_H26X == ENABLE || AUTOTEST_H26X == ENABLE)

#endif
