#ifndef __MODULE_PROC_H_
#define __MODULE_PROC_H__
#include "ctl_vpe_main.h"

int nvt_ctl_vpe_proc_init(PCTL_VPE_DRV_INFO pdrv_info);
int nvt_ctl_vpe_proc_remove(PCTL_VPE_DRV_INFO pdrv_info);


#endif