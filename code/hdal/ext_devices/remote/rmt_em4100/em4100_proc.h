int em4100_proc_init(void);
int em4100_proc_exit(void);

int em4100_proc_loop(unsigned char argc, char **argv);
int em4100_proc_close(unsigned char argc, char **argv);
int em4100_proc_sleep(unsigned char argc, char **argv);
int em4100_proc_mode(unsigned char argc, char **argv);
int em4100_proc_length(unsigned char argc, char **argv);
int em4100_proc_read_data(unsigned char argc, char **argv);
int em4100_proc_read_data2(unsigned char argc, char **argv);
