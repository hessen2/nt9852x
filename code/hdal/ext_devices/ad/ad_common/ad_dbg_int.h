#ifndef _AD_DBG_INT_H_
#define _AD_DBG_INT_H_
#define __MODULE__ AD_COMM
#define __DBGLVL__ 1 // 0=FATAL, 1=ERR, 2=WRN, 3=UNIT, 4=FUNC, 5=IND, 6=MSG, 7=VALUE, 8=USER
#include <kwrap/debug.h>

#endif //_AD_DBG_INT_H_

