#if defined(__KERNEL__)
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <kdrv_builtin/kdrv_builtin.h>
#include "isp_builtin.h"
#else
#include "plat/gpio.h"
#endif
#include "kwrap/error_no.h"
#include "kwrap/type.h"
#include "kwrap/util.h"
#include <kwrap/verinfo.h>
#include "kflow_videocapture/ctl_sen.h"
#include "isp_api.h"

#include "sen_cfg.h"
#include "sen_common.h"
#include "sen_inc.h"

//=============================================================================
//Module parameter : Set module parameters when insert the module
//=============================================================================
#if defined(__KERNEL__)
char *sen_cfg_path = "null";
module_param_named(sen_cfg_path, sen_cfg_path, charp, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(sen_cfg_path, "Path of cfg file");

#ifdef DEBUG
unsigned int sen_debug_level = THIS_DBGLVL;
module_param_named(sen_debug_level, sen_debug_level, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(sen_debug_level, "Debug message level");
#endif
#endif

//=============================================================================
// version
//=============================================================================
VOS_MODULE_VERSION(nvt_sen_imx678, 1, 53, 000, 00);

//=============================================================================
// information
//=============================================================================
#define SEN_IMX678_MODULE_NAME     "sen_imx678"
#define SEN_MAX_MODE               3
#define MAX_VD_PERIOD              0xFFFFF
#define MAX_EXPOSURE_LINE          0xFFFFF
#define HCG_ENABLE                 99
#define BRL_ALL_PIXEL              2200
#define BRL_1080P_PIXEL            1100
#define BEYOND_FSC                 1
#define HCG_RATIO                  27

#define SEN_I2C_ADDR 0x34>>1
#define SEN_I2C_COMPATIBLE "nvt,sen_imx678"

#include "sen_i2c.c"

//=============================================================================
// function declaration
//=============================================================================
static CTL_SEN_DRV_TAB *sen_get_drv_tab_imx678(void);
static void sen_pwr_ctrl_imx678(CTL_SEN_ID id, CTL_SEN_PWR_CTRL_FLAG flag, CTL_SEN_CLK_CB clk_cb);
static ER sen_open_imx678(CTL_SEN_ID id);
static ER sen_close_imx678(CTL_SEN_ID id);
static ER sen_sleep_imx678(CTL_SEN_ID id);
static ER sen_wakeup_imx678(CTL_SEN_ID id);
static ER sen_write_reg_imx678(CTL_SEN_ID id, CTL_SEN_CMD *cmd);
static ER sen_read_reg_imx678(CTL_SEN_ID id, CTL_SEN_CMD *cmd);
static ER sen_chg_mode_imx678(CTL_SEN_ID id, CTL_SENDRV_CHGMODE_OBJ chgmode_obj);
static ER sen_chg_fps_imx678(CTL_SEN_ID id, UINT32 fps);
static ER sen_set_info_imx678(CTL_SEN_ID id, CTL_SENDRV_CFGID drv_cfg_id, void *data);
static ER sen_get_info_imx678(CTL_SEN_ID id, CTL_SENDRV_CFGID drv_cfg_id, void *data);
static UINT32 sen_calc_chgmode_vd_imx678(CTL_SEN_ID id, UINT32 fps);
static UINT32 sen_calc_exp_vd_imx678(CTL_SEN_ID id, UINT32 fps);
static void sen_set_gain_imx678(CTL_SEN_ID id, void *param);
static void sen_set_expt_imx678(CTL_SEN_ID id, void *param);
static void sen_set_preset_imx678(CTL_SEN_ID id, ISP_SENSOR_PRESET_CTRL *ctrl);
static void sen_set_flip_imx678(CTL_SEN_ID id, CTL_SEN_FLIP *flip);
static ER sen_get_flip_imx678(CTL_SEN_ID id, CTL_SEN_FLIP *flip);
#if defined(__FREERTOS)
void sen_get_gain_imx678(CTL_SEN_ID id, void *param);
void sen_get_expt_imx678(CTL_SEN_ID id, void *param);
#else
static void sen_get_gain_imx678(CTL_SEN_ID id, void *param);
static void sen_get_expt_imx678(CTL_SEN_ID id, void *param);
#endif
static void sen_get_min_expt_imx678(CTL_SEN_ID id, void *param);
static void sen_get_mode_basic_imx678(CTL_SENDRV_GET_MODE_BASIC_PARAM *mode_basic);
static void sen_get_attr_basic_imx678(CTL_SEN_ID id, CTL_SENDRV_GET_ATTR_BASIC_PARAM *data);
static void sen_get_attr_signal_imx678(CTL_SENDRV_GET_ATTR_SIGNAL_PARAM *data);
static ER sen_get_attr_cmdif_imx678(CTL_SEN_ID id, CTL_SENDRV_GET_ATTR_CMDIF_PARAM *data);
static ER sen_get_attr_if_imx678(CTL_SENDRV_GET_ATTR_IF_PARAM *data);
static void sen_get_fps_imx678(CTL_SEN_ID id, CTL_SENDRV_GET_FPS_PARAM *data);
static void sen_get_speed_imx678(CTL_SEN_ID id, CTL_SENDRV_GET_SPEED_PARAM *data);
static void sen_get_mode_mipi_imx678(CTL_SENDRV_GET_MODE_MIPI_PARAM *data);
static void sen_get_modesel_imx678(CTL_SENDRV_GET_MODESEL_PARAM *data);
static UINT32 sen_calc_rowtime_imx678(CTL_SEN_ID id, CTL_SEN_MODE mode);
static UINT32 sen_calc_rowtime_step_imx678(CTL_SEN_ID id, CTL_SEN_MODE mode);
static void sen_get_rowtime_imx678(CTL_SEN_ID id, CTL_SENDRV_GET_MODE_ROWTIME_PARAM *data);
static void sen_set_cur_fps_imx678(CTL_SEN_ID id, UINT32 fps);
static UINT32 sen_get_cur_fps_imx678(CTL_SEN_ID id);
static void sen_set_chgmode_fps_imx678(CTL_SEN_ID id, UINT32 fps);
static UINT32 sen_get_chgmode_fps_imx678(CTL_SEN_ID id);
static ER sen_get_min_shr_imx678(CTL_SEN_MODE mode, UINT32 *min_shr, UINT32 *min_exp);
//=============================================================================
// global variable
//=============================================================================
static UINT32 sen_map = SEN_PATH_1;

static SEN_PRESET sen_preset[CTL_SEN_ID_MAX] = {
	{10000, 1000},
	{10000, 1000},
	{10000, 1000},
	{10000, 1000},
	{10000, 1000},
	{10000, 1000},
	{10000, 1000},
	{10000, 1000}
};

static SEN_DIRECTION sen_direction[CTL_SEN_ID_MAX] = {
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE},
	{FALSE, FALSE}
};

static SEN_POWER sen_power[CTL_SEN_ID_MAX] = {
	//C_GPIO:+0x0; P_GPIO:+0x20; S_GPIO:+0x40; L_GPIO:0x60
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1},
	{CTL_SEN_CLK_SEL_SIEMCLK, CTL_SEN_IGNORE, 0x44, 1, 1}
};
	
static SEN_I2C sen_i2c[CTL_SEN_ID_MAX] = {
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_1, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR},
	{SEN_I2C_ID_2, SEN_I2C_ADDR}
};

static CTL_SENDRV_GET_ATTR_BASIC_PARAM basic_param = {
	SEN_IMX678_MODULE_NAME,
	CTL_SEN_VENDOR_OTHERS,
	SEN_MAX_MODE,
	CTL_SEN_SUPPORT_PROPERTY_MIRROR|CTL_SEN_SUPPORT_PROPERTY_FLIP|CTL_SEN_SUPPORT_PROPERTY_CHGFPS,
	0
};

static CTL_SENDRV_GET_ATTR_SIGNAL_PARAM signal_param = {
	CTL_SEN_SIGNAL_MASTER,
	{CTL_SEN_ACTIVE_HIGH, CTL_SEN_ACTIVE_HIGH, CTL_SEN_PHASE_RISING, CTL_SEN_PHASE_RISING, CTL_SEN_PHASE_RISING}
};

static CTL_SENDRV_I2C i2c = {
	{
		{CTL_SEN_I2C_W_ADDR_DFT,     0x34},
		{CTL_SEN_I2C_W_ADDR_OPTION1, 0xFF},
		{CTL_SEN_I2C_W_ADDR_OPTION2, 0xFF},
		{CTL_SEN_I2C_W_ADDR_OPTION3, 0xFF},
		{CTL_SEN_I2C_W_ADDR_OPTION4, 0xFF},
		{CTL_SEN_I2C_W_ADDR_OPTION5, 0xFF}
	}
};

static CTL_SENDRV_GET_SPEED_PARAM speed_param[SEN_MAX_MODE] = {
	{
		CTL_SEN_MODE_1,
		CTL_SEN_SIEMCLK_SRC_DFT,
		24000000,
		74250000,
		240000000
	},
	{
		CTL_SEN_MODE_2,
		CTL_SEN_SIEMCLK_SRC_DFT,
		24000000,
		74250000,
		288000000
	},
	{
		CTL_SEN_MODE_3,
		CTL_SEN_SIEMCLK_SRC_DFT,
		24000000,
		74250000,
		288000000
	}
};

static CTL_SENDRV_GET_MODE_MIPI_PARAM mipi_param[SEN_MAX_MODE] = {
	{
		CTL_SEN_MODE_1,
		CTL_SEN_CLKLANE_1,
		CTL_SEN_DATALANE_4,
		{ {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0} },
		0,
		{0, 0, 0, 0},
		SEN_BIT_OFS_NONE
	},
	{
		CTL_SEN_MODE_2,
		CTL_SEN_CLKLANE_1,
		CTL_SEN_DATALANE_4,
		{ {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0} },
		0,
		{1, 0, 0, 0},
		SEN_BIT_OFS_0|SEN_BIT_OFS_1
	},
	{
		CTL_SEN_MODE_3,
		CTL_SEN_CLKLANE_1,
		CTL_SEN_DATALANE_4,
		{ {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0}, {CTL_SEN_MIPI_MANUAL_NONE, 0} },
		0,
		{1, 0, 0, 0},
		SEN_BIT_OFS_0|SEN_BIT_OFS_1
	}
};

static CTL_SENDRV_GET_MODE_BASIC_PARAM mode_basic_param[SEN_MAX_MODE] = {
	{
		CTL_SEN_MODE_1,
		CTL_SEN_IF_TYPE_MIPI,
		CTL_SEN_DATA_FMT_RGB,
		CTL_SEN_MODE_LINEAR,
		3000,
		1,
		CTL_SEN_STPIX_R,
		CTL_SEN_PIXDEPTH_12BIT,
		CTL_SEN_FMT_POGRESSIVE,
		{3856, 2180},
		{{8, 12, 3840, 2160}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}},
		{3840, 2160},
		{0, 660, 0, 3750},
		CTL_SEN_RATIO(16, 9),
		{1000, 10748700},
		100
	},
	{
		CTL_SEN_MODE_2,
		CTL_SEN_IF_TYPE_MIPI,
		CTL_SEN_DATA_FMT_RGB,
		CTL_SEN_MODE_STAGGER_HDR,
		3000,
		2,
		CTL_SEN_STPIX_R,
		CTL_SEN_PIXDEPTH_10BIT,
		CTL_SEN_FMT_POGRESSIVE,
		{3856, 2180},
		{{8, 12, 3840, 2160}, {8, 12, 3840, 2160}, {0, 0, 0, 0}, {0, 0, 0, 0}},
		{3840, 2160},
		{0, 550, 0, 2250},
		CTL_SEN_RATIO(16, 9),
		{1000, 10748700},
		100
	},
	{
		CTL_SEN_MODE_3,
		CTL_SEN_IF_TYPE_MIPI,
		CTL_SEN_DATA_FMT_RGB,
		CTL_SEN_MODE_STAGGER_HDR,
		3000,
		2,
		CTL_SEN_STPIX_R,
		CTL_SEN_PIXDEPTH_10BIT,
		CTL_SEN_FMT_POGRESSIVE,
		{1928, 1090},
		{{4, 6, 1920, 1080}, {4, 6, 1920, 1080}, {0, 0, 0, 0}, {0, 0, 0, 0}},
		{1920, 1080},
		{0, 550, 0, 2250},
		CTL_SEN_RATIO(16, 9),
		{1000, 10748700},
		200
	}
};

static CTL_SEN_CMD imx678_mode_1[] = {
	{0x3000, 1, {0x01, 0x0}}, // standby
	{0x3002, 1, {0x01, 0x0}}, //Master mode stop
	{0x3014, 1, {0x04, 0x0}}, 	
	{0x3015, 1, {0x03, 0x0}}, 
	{0x3028, 1, {0xA6, 0x0}}, 	
	{0x3029, 1, {0x0E, 0x0}},
	{0x302C, 1, {0x94, 0x0}},
	{0x302D, 1, {0x02, 0x0}},
	{0x3050, 1, {0x03, 0x0}},
	{0x30A4, 1, {0xAA, 0x0}},
	{0x30A6, 1, {0x00, 0x0}},
	{0x30CC, 1, {0x00, 0x0}},
	{0x30CD, 1, {0x00, 0x0}},
	{0x3460, 1, {0x22, 0x0}},
	{0x355A, 1, {0x64, 0x0}},
	{0x3A02, 1, {0x7A, 0x0}},
	{0x3A10, 1, {0xEC, 0x0}},
	{0x3A12, 1, {0x71, 0x0}},
	{0x3A14, 1, {0xDE, 0x0}},
	{0x3A20, 1, {0x2B, 0x0}},	
	{0x3A24, 1, {0x22, 0x0}},	
	{0x3A25, 1, {0x25, 0x0}},
	{0x3A26, 1, {0x2A, 0x0}},	
	{0x3A27, 1, {0x2C, 0x0}},	
	{0x3A28, 1, {0x39, 0x0}},
	{0x3A29, 1, {0x38, 0x0}},	
	{0x3A30, 1, {0x04, 0x0}},
	{0x3A31, 1, {0x04, 0x0}},
	{0x3A32, 1, {0x03, 0x0}},
	{0x3A33, 1, {0x03, 0x0}},
	{0x3A34, 1, {0x09, 0x0}},
	{0x3A35, 1, {0x06, 0x0}},
	{0x3A38, 1, {0xCD, 0x0}},
	{0x3A3A, 1, {0x4C, 0x0}},
	{0x3A3C, 1, {0xB9, 0x0}},
	{0x3A3E, 1, {0x30, 0x0}},
	{0x3A40, 1, {0x2C, 0x0}},
	{0x3A42, 1, {0x39, 0x0}},
	{0x3A4E, 1, {0x00, 0x0}},
	{0x3A52, 1, {0x00, 0x0}},
	{0x3A56, 1, {0x00, 0x0}},
	{0x3A5A, 1, {0x00, 0x0}},
	{0x3A5E, 1, {0x00, 0x0}},
	{0x3A62, 1, {0x00, 0x0}},
	{0x3A6E, 1, {0xA0, 0x0}},
	{0x3A70, 1, {0x50, 0x0}},
	{0x3A8C, 1, {0x04, 0x0}},
	{0x3A8D, 1, {0x03, 0x0}},
	{0x3A8E, 1, {0x09, 0x0}},
	{0x3A90, 1, {0x38, 0x0}},
	{0x3A91, 1, {0x42, 0x0}},
	{0x3A92, 1, {0x3C, 0x0}},
	{0x3B0E, 1, {0xF3, 0x0}},
	{0x3B12, 1, {0xE5, 0x0}},
	{0x3B27, 1, {0xC0, 0x0}},
	{0x3B2E, 1, {0xEF, 0x0}},
	{0x3B30, 1, {0x6A, 0x0}},
	{0x3B32, 1, {0xF6, 0x0}},
	{0x3B36, 1, {0xE1, 0x0}},
	{0x3B3A, 1, {0xE8, 0x0}},
	{0x3B5A, 1, {0x17, 0x0}},
	{0x3B5E, 1, {0xEF, 0x0}},
	{0x3B60, 1, {0x6A, 0x0}},
	{0x3B62, 1, {0xF6, 0x0}},
	{0x3B66, 1, {0xE1, 0x0}},
	{0x3B6A, 1, {0xE8, 0x0}},
	{0x3B88, 1, {0xEC, 0x0}},
	{0x3B8A, 1, {0xED, 0x0}},
	{0x3B94, 1, {0x71, 0x0}},
	{0x3B96, 1, {0x72, 0x0}},
	{0x3B98, 1, {0xDE, 0x0}},
	{0x3B9A, 1, {0xDF, 0x0}},
	{0x3C0F, 1, {0x06, 0x0}},
	{0x3C10, 1, {0x06, 0x0}},
	{0x3C11, 1, {0x06, 0x0}},
	{0x3C12, 1, {0x06, 0x0}},
	{0x3C13, 1, {0x06, 0x0}},
	{0x3C18, 1, {0x20, 0x0}},
	{0x3C3A, 1, {0x7A, 0x0}},
	{0x3C40, 1, {0xF4, 0x0}},
	{0x3C48, 1, {0xE6, 0x0}},
	{0x3C54, 1, {0xCE, 0x0}},
	{0x3C56, 1, {0xD0, 0x0}},
	{0x3C6C, 1, {0x53, 0x0}},
	{0x3C6E, 1, {0x55, 0x0}},
	{0x3C70, 1, {0xC0, 0x0}},
	{0x3C72, 1, {0xC2, 0x0}},
	{0x3C7E, 1, {0xCE, 0x0}},
	{0x3C8C, 1, {0xCF, 0x0}},
	{0x3C8E, 1, {0xEB, 0x0}},
	{0x3C98, 1, {0x54, 0x0}},
	{0x3C9A, 1, {0x70, 0x0}},
	{0x3C9C, 1, {0xC1, 0x0}},
	{0x3C9E, 1, {0xDD, 0x0}},
	{0x3CB0, 1, {0x7A, 0x0}},
	{0x3CB2, 1, {0xBA, 0x0}},
	{0x3CC8, 1, {0xBC, 0x0}},
	{0x3CCA, 1, {0x7C, 0x0}},
	{0x3CD4, 1, {0xEA, 0x0}},
	{0x3CD5, 1, {0x01, 0x0}},
	{0x3CD6, 1, {0x4A, 0x0}},
	{0x3CD8, 1, {0x00, 0x0}},
	{0x3CD9, 1, {0x00, 0x0}},
	{0x3CDA, 1, {0xFF, 0x0}},
	{0x3CDB, 1, {0x03, 0x0}},
	{0x3CDC, 1, {0x00, 0x0}},
	{0x3CDD, 1, {0x00, 0x0}},
	{0x3CDE, 1, {0xFF, 0x0}}, 	
	{0x3CDF, 1, {0x03, 0x0}}, 
	{0x3CE4, 1, {0x4C, 0x0}}, 	
	{0x3CE6, 1, {0xEC, 0x0}},
	{0x3CE7, 1, {0x01, 0x0}},
	{0x3CE8, 1, {0xFF, 0x0}},
	{0x3CE9, 1, {0x03, 0x0}},
	{0x3CEA, 1, {0x00, 0x0}},
	{0x3CEB, 1, {0x00, 0x0}},
	{0x3CEC, 1, {0xFF, 0x0}},
	{0x3CED, 1, {0x03, 0x0}},
	{0x3CEE, 1, {0x00, 0x0}},
	{0x3CEF, 1, {0x00, 0x0}},
	{0x3E28, 1, {0x82, 0x0}},
	{0x3E2A, 1, {0x80, 0x0}},
	{0x3E30, 1, {0x85, 0x0}},
	{0x3E32, 1, {0x7D, 0x0}},
	{0x3E5C, 1, {0xCE, 0x0}},	
	{0x3E5E, 1, {0xD3, 0x0}},	
	{0x3E70, 1, {0x53, 0x0}},
	{0x3E72, 1, {0x58, 0x0}},	
	{0x3E74, 1, {0xC0, 0x0}},	
	{0x3E76, 1, {0xC5, 0x0}},
	{0x3E78, 1, {0xC0, 0x0}},	
	{0x3E79, 1, {0x01, 0x0}},
	{0x3E7A, 1, {0xD4, 0x0}},
	{0x3E7B, 1, {0x01, 0x0}},
	{0x3EB4, 1, {0x0B, 0x0}},
	{0x3EB5, 1, {0x02, 0x0}},
	{0x3EB6, 1, {0x4D, 0x0}},
	{0x3EEC, 1, {0xF3, 0x0}},
	{0x3EEE, 1, {0xE7, 0x0}},
	{0x3F01, 1, {0x01, 0x0}},
	{0x3F24, 1, {0x10, 0x0}},
	{0x3F28, 1, {0x2D, 0x0}},
	{0x3F2A, 1, {0x2D, 0x0}},
	{0x3F2C, 1, {0x2D, 0x0}},
	{0x3F2E, 1, {0x2D, 0x0}},
	{0x3F30, 1, {0x23, 0x0}},
	{0x3F38, 1, {0x2D, 0x0}},
	{0x3F3A, 1, {0x2D, 0x0}},
	{0x3F3C, 1, {0x2D, 0x0}},
	{0x3F3E, 1, {0x28, 0x0}},
	{0x3F40, 1, {0x1E, 0x0}},
	{0x3F48, 1, {0x2D, 0x0}},
	{0x3F4A, 1, {0x2D, 0x0}},
	{0x4004, 1, {0xE4, 0x0}},
	{0x4006, 1, {0xFF, 0x0}},
	{0x4018, 1, {0x69, 0x0}},
	{0x401A, 1, {0x84, 0x0}},
	{0x401C, 1, {0xD6, 0x0}},
	{0x401E, 1, {0xF1, 0x0}},
	{0x4038, 1, {0xDE, 0x0}},
	{0x403A, 1, {0x00, 0x0}},
	{0x403B, 1, {0x01, 0x0}},
	{0x404C, 1, {0x63, 0x0}},
	{0x404E, 1, {0x85, 0x0}},
	{0x4050, 1, {0xD0, 0x0}},
	{0x4052, 1, {0xF2, 0x0}},
	{0x4108, 1, {0xDD, 0x0}},
	{0x410A, 1, {0xF7, 0x0}},
	{0x411C, 1, {0x62, 0x0}},
	{0x411E, 1, {0x7C, 0x0}},
	{0x4120, 1, {0xCF, 0x0}},
	{0x4122, 1, {0xE9, 0x0}},
	{0x4138, 1, {0xE6, 0x0}},
	{0x413A, 1, {0xF1, 0x0}},
	{0x414C, 1, {0x6B, 0x0}},
	{0x414E, 1, {0x76, 0x0}},
	{0x4150, 1, {0xD8, 0x0}},
	{0x4152, 1, {0xE3, 0x0}},
	{0x417E, 1, {0x03, 0x0}},
	{0x417F, 1, {0x01, 0x0}},
	{0x4186, 1, {0xE0, 0x0}},
	{0x4190, 1, {0xF3, 0x0}},
	{0x4192, 1, {0xF7, 0x0}},
	{0x419C, 1, {0x78, 0x0}},
	{0x419E, 1, {0x7C, 0x0}},
	{0x41A0, 1, {0xE5, 0x0}},
	{0x41A2, 1, {0xE9, 0x0}},
	{0x41C8, 1, {0xE2, 0x0}},
	{0x41CA, 1, {0xFD, 0x0}},
	{0x41DC, 1, {0x67, 0x0}},
	{0x41DE, 1, {0x82, 0x0}},
	{0x41E0, 1, {0xD4, 0x0}},
	{0x41E2, 1, {0xEF, 0x0}},
	{0x4200, 1, {0xDE, 0x0}},
	{0x4202, 1, {0xDA, 0x0}},
	{0x4218, 1, {0x63, 0x0}},
	{0x421A, 1, {0x5F, 0x0}},
	{0x421C, 1, {0xD0, 0x0}},
	{0x421E, 1, {0xCC, 0x0}},
	{0x425A, 1, {0x82, 0x0}},
	{0x425C, 1, {0xEF, 0x0}},
	{0x4348, 1, {0xFE, 0x0}},
	{0x4349, 1, {0x06, 0x0}},
	{0x4352, 1, {0xCE, 0x0}},
	{0x4420, 1, {0x0B, 0x0}},
	{0x4421, 1, {0x02, 0x0}},
	{0x4422, 1, {0x4D, 0x0}},
	{0x4426, 1, {0xF5, 0x0}},
	{0x442A, 1, {0xE7, 0x0}},
	{0x4432, 1, {0xF5, 0x0}},
	{0x4436, 1, {0xE7, 0x0}},
	{0x4466, 1, {0xB4, 0x0}},
	{0x446E, 1, {0x32, 0x0}}, 	
	{0x449F, 1, {0x1C, 0x0}}, 
	{0x44A4, 1, {0x2C, 0x0}}, 	
	{0x44A6, 1, {0x2C, 0x0}},
	{0x44A8, 1, {0x2C, 0x0}},
	{0x44AA, 1, {0x2C, 0x0}},
	{0x44B4, 1, {0x2C, 0x0}},
	{0x44B6, 1, {0x2C, 0x0}},
	{0x44B8, 1, {0x2C, 0x0}},
	{0x44BA, 1, {0x2C, 0x0}},
	{0x44C4, 1, {0x2C, 0x0}},
	{0x44C6, 1, {0x2C, 0x0}},
	{0x44C8, 1, {0x2C, 0x0}},
	{0x4506, 1, {0xF3, 0x0}},
	{0x450E, 1, {0xE5, 0x0}},
	{0x4516, 1, {0xF3, 0x0}},
	{0x4522, 1, {0xE5, 0x0}},
	{0x4524, 1, {0xF3, 0x0}},	
	{0x452C, 1, {0xE5, 0x0}},	
	{0x453C, 1, {0x22, 0x0}},
	{0x453D, 1, {0x1B, 0x0}},	
	{0x453E, 1, {0x1B, 0x0}},	
	{0x453F, 1, {0x15, 0x0}},
	{0x4540, 1, {0x15, 0x0}},	
	{0x4541, 1, {0x15, 0x0}},
	{0x4542, 1, {0x15, 0x0}},
	{0x4543, 1, {0x15, 0x0}},
	{0x4544, 1, {0x15, 0x0}},
	{0x4548, 1, {0x00, 0x0}},
	{0x4549, 1, {0x01, 0x0}},
	{0x454A, 1, {0x01, 0x0}},
	{0x454B, 1, {0x06, 0x0}},
	{0x454C, 1, {0x06, 0x0}},
	{0x454D, 1, {0x06, 0x0}},
	{0x454E, 1, {0x06, 0x0}},
	{0x454F, 1, {0x06, 0x0}},
	{0x4550, 1, {0x06, 0x0}},
	{0x4554, 1, {0x55, 0x0}},
	{0x4555, 1, {0x02, 0x0}},
	{0x4556, 1, {0x42, 0x0}},
	{0x4557, 1, {0x05, 0x0}},
	{0x4558, 1, {0xFD, 0x0}},
	{0x4559, 1, {0x05, 0x0}},
	{0x455A, 1, {0x94, 0x0}},
	{0x455B, 1, {0x06, 0x0}},
	{0x455D, 1, {0x06, 0x0}},
	{0x455E, 1, {0x49, 0x0}},
	{0x455F, 1, {0x07, 0x0}},
	{0x4560, 1, {0x7F, 0x0}},
	{0x4561, 1, {0x07, 0x0}},
	{0x4562, 1, {0xA5, 0x0}},
	{0x4564, 1, {0x55, 0x0}},
	{0x4565, 1, {0x02, 0x0}},
	{0x4566, 1, {0x42, 0x0}},
	{0x4567, 1, {0x05, 0x0}},
	{0x4568, 1, {0xFD, 0x0}},
	{0x4569, 1, {0x05, 0x0}},
	{0x456A, 1, {0x94, 0x0}},
	{0x456B, 1, {0x06, 0x0}},
	{0x456D, 1, {0x06, 0x0}},
	{0x456E, 1, {0x49, 0x0}},
	{0x456F, 1, {0x07, 0x0}},
	{0x4572, 1, {0xA5, 0x0}},
	{0x460C, 1, {0x7D, 0x0}},
	{0x460E, 1, {0xB1, 0x0}},
	{0x4614, 1, {0xA8, 0x0}},
	{0x4616, 1, {0xB2, 0x0}},
	{0x461C, 1, {0x7E, 0x0}},
	{0x461E, 1, {0xA7, 0x0}},
	{0x4624, 1, {0xA8, 0x0}},
	{0x4626, 1, {0xB2, 0x0}},
	{0x462C, 1, {0x7E, 0x0}},
	{0x462E, 1, {0x8A, 0x0}},
	{0x4630, 1, {0x94, 0x0}},
	{0x4632, 1, {0xA7, 0x0}},
	{0x4634, 1, {0xFB, 0x0}},
	{0x4636, 1, {0x2F, 0x0}},
	{0x4638, 1, {0x81, 0x0}},
	{0x4639, 1, {0x01, 0x0}},
	{0x463A, 1, {0xB5, 0x0}},
	{0x463B, 1, {0x01, 0x0}},
	{0x463C, 1, {0x26, 0x0}},
	{0x463E, 1, {0x30, 0x0}},
	{0x4640, 1, {0xAC, 0x0}},
	{0x4641, 1, {0x01, 0x0}},
	{0x4642, 1, {0xB6, 0x0}},
	{0x4643, 1, {0x01, 0x0}},
	{0x4644, 1, {0xFC, 0x0}},
	{0x4646, 1, {0x25, 0x0}},
	{0x4648, 1, {0x82, 0x0}},
	{0x4649, 1, {0x01, 0x0}},
	{0x464A, 1, {0xAB, 0x0}},
	{0x464B, 1, {0x01, 0x0}},
	{0x464C, 1, {0x26, 0x0}},
	{0x464E, 1, {0x30, 0x0}},
	{0x4654, 1, {0xFC, 0x0}},
	{0x4656, 1, {0x08, 0x0}},
	{0x4658, 1, {0x12, 0x0}},
	{0x465A, 1, {0x25, 0x0}},
	{0x4662, 1, {0xFC, 0x0}},
	{0x46A2, 1, {0xFB, 0x0}},
	{0x46D6, 1, {0xF3, 0x0}},
	{0x46E6, 1, {0x00, 0x0}},
	{0x46E8, 1, {0xFF, 0x0}},
	{0x46E9, 1, {0x03, 0x0}},
	{0x46EC, 1, {0x7A, 0x0}}, 	
	{0x46EE, 1, {0xE5, 0x0}}, 
	{0x46F4, 1, {0xEE, 0x0}}, 	
	{0x46F6, 1, {0xF2, 0x0}},
	{0x470C, 1, {0xFF, 0x0}},
	{0x470D, 1, {0x03, 0x0}},
	{0x470E, 1, {0x00, 0x0}},
	{0x4714, 1, {0xE0, 0x0}},
	{0x4716, 1, {0xE4, 0x0}},
	{0x471E, 1, {0xED, 0x0}},
	{0x472E, 1, {0x00, 0x0}},
	{0x4730, 1, {0xFF, 0x0}},
	{0x4731, 1, {0x03, 0x0}},
	{0x4734, 1, {0x7B, 0x0}},
	{0x4736, 1, {0xDF, 0x0}},
	{0x4754, 1, {0x7D, 0x0}},
	{0x4756, 1, {0x8B, 0x0}},
	{0x4758, 1, {0x93, 0x0}},	
	{0x475A, 1, {0xB1, 0x0}},	
	{0x475C, 1, {0xFB, 0x0}},
	{0x475E, 1, {0x09, 0x0}},	
	{0x4760, 1, {0x11, 0x0}},	
	{0x4762, 1, {0x2F, 0x0}},
	{0x4766, 1, {0xCC, 0x0}},	
	{0x4776, 1, {0xCB, 0x0}},
	{0x477E, 1, {0x4A, 0x0}},
	{0x478E, 1, {0x49, 0x0}},
	{0x4794, 1, {0x7C, 0x0}},
	{0x4796, 1, {0x8F, 0x0}},
	{0x4798, 1, {0xB3, 0x0}},
	{0x4799, 1, {0x00, 0x0}},
	{0x479A, 1, {0xCC, 0x0}},
	{0x479C, 1, {0xC1, 0x0}},
	{0x479E, 1, {0xCB, 0x0}},
	{0x47A4, 1, {0x7D, 0x0}},
	{0x47A6, 1, {0x8E, 0x0}},
	{0x47A8, 1, {0xB4, 0x0}},
	{0x47A9, 1, {0x00, 0x0}},
	{0x47AA, 1, {0xC0, 0x0}},
	{0x47AC, 1, {0xFA, 0x0}},
	{0x47AE, 1, {0x0D, 0x0}},
	{0x47B0, 1, {0x31, 0x0}},
	{0x47B1, 1, {0x01, 0x0}},
	{0x47B2, 1, {0x4A, 0x0}},
	{0x47B3, 1, {0x01, 0x0}},
	{0x47B4, 1, {0x3F, 0x0}},
	{0x47B6, 1, {0x49, 0x0}},
	{0x47BC, 1, {0xFB, 0x0}},
	{0x47BE, 1, {0x0C, 0x0}},
	{0x47C0, 1, {0x32, 0x0}},
	{0x47C1, 1, {0x01, 0x0}},
	{0x47C2, 1, {0x3E, 0x0}},
	{0x47C3, 1, {0x01, 0x0}},	
	{SEN_CMD_SETVD, 1, {0x0, 0x0}},
	{SEN_CMD_PRESET, 1, {0x0, 0x0}},
	{SEN_CMD_DIRECTION, 1, {0x0, 0x0}},
	{0x3000, 1, {0x00, 0x0}},  // standby cancel
	{SEN_CMD_DELAY, 1, {24, 0x0}},
	{0x3002, 1, {0x00, 0x0}},  //Master mode start
};

static CTL_SEN_CMD imx678_mode_2[] = {
	{0x3000, 1, {0x01, 0x0}}, // standby
	{0x3002, 1, {0x01, 0x0}}, //Master mode stop
	{0x3014, 1, {0x04, 0x0}},
	{0x3015, 1, {0x03, 0x0}},
	{0x301A, 1, {0x01, 0x0}},
	{0x301C, 1, {0x01, 0x0}},
	{0x301D, 1, {0x01, 0x0}},
	{0x3022, 1, {0x00, 0x0}},
	{0x3023, 1, {0x00, 0x0}},
	{0x302C, 1, {0x26, 0x0}},
	{0x302D, 1, {0x02, 0x0}},
	{0x3050, 1, {0x0E, 0x0}},
	{0x3051, 1, {0x11, 0x0}},
	{0x3054, 1, {0x05, 0x0}},
	{0x3060, 1, {0x13, 0x0}},
	{0x3061, 1, {0x00, 0x0}},
	{0x30A4, 1, {0xAA, 0x0}},
	{0x30A6, 1, {0x00, 0x0}},
	{0x30CC, 1, {0x00, 0x0}},
	{0x30CD, 1, {0x00, 0x0}},
	{0x3460, 1, {0x22, 0x0}},
	{0x355A, 1, {0x64, 0x0}},
	{0x3A02, 1, {0x7A, 0x0}},
	{0x3A10, 1, {0xEC, 0x0}},
	{0x3A12, 1, {0x71, 0x0}},
	{0x3A14, 1, {0xDE, 0x0}},
	{0x3A20, 1, {0x2B, 0x0}},
	{0x3A24, 1, {0x22, 0x0}},
	{0x3A25, 1, {0x25, 0x0}},
	{0x3A26, 1, {0x2A, 0x0}},
	{0x3A27, 1, {0x2C, 0x0}},
	{0x3A28, 1, {0x39, 0x0}},
	{0x3A29, 1, {0x38, 0x0}},
	{0x3A30, 1, {0x04, 0x0}},
	{0x3A31, 1, {0x04, 0x0}},
	{0x3A32, 1, {0x03, 0x0}},
	{0x3A33, 1, {0x03, 0x0}},
	{0x3A34, 1, {0x09, 0x0}},
	{0x3A35, 1, {0x06, 0x0}},
	{0x3A38, 1, {0xCD, 0x0}},
	{0x3A3A, 1, {0x4C, 0x0}},
	{0x3A3C, 1, {0xB9, 0x0}},
	{0x3A3E, 1, {0x30, 0x0}},
	{0x3A40, 1, {0x2C, 0x0}},
	{0x3A42, 1, {0x39, 0x0}},
	{0x3A4E, 1, {0x00, 0x0}},
	{0x3A52, 1, {0x00, 0x0}},
	{0x3A56, 1, {0x00, 0x0}},
	{0x3A5A, 1, {0x00, 0x0}},
	{0x3A5E, 1, {0x00, 0x0}},
	{0x3A62, 1, {0x00, 0x0}},
	{0x3A6E, 1, {0xA0, 0x0}},
	{0x3A70, 1, {0x50, 0x0}},
	{0x3A8C, 1, {0x04, 0x0}},
	{0x3A8D, 1, {0x03, 0x0}},
	{0x3A8E, 1, {0x09, 0x0}},
	{0x3A90, 1, {0x38, 0x0}},
	{0x3A91, 1, {0x42, 0x0}},
	{0x3A92, 1, {0x3C, 0x0}},
	{0x3B0E, 1, {0xF3, 0x0}},
	{0x3B12, 1, {0xE5, 0x0}},
	{0x3B27, 1, {0xC0, 0x0}},
	{0x3B2E, 1, {0xEF, 0x0}},
	{0x3B30, 1, {0x6A, 0x0}},
	{0x3B32, 1, {0xF6, 0x0}},
	{0x3B36, 1, {0xE1, 0x0}},
	{0x3B3A, 1, {0xE8, 0x0}},
	{0x3B5A, 1, {0x17, 0x0}},
	{0x3B5E, 1, {0xEF, 0x0}},
	{0x3B60, 1, {0x6A, 0x0}},
	{0x3B62, 1, {0xF6, 0x0}},
	{0x3B66, 1, {0xE1, 0x0}},
	{0x3B6A, 1, {0xE8, 0x0}},
	{0x3B88, 1, {0xEC, 0x0}},
	{0x3B8A, 1, {0xED, 0x0}},
	{0x3B94, 1, {0x71, 0x0}},
	{0x3B96, 1, {0x72, 0x0}},
	{0x3B98, 1, {0xDE, 0x0}},
	{0x3B9A, 1, {0xDF, 0x0}},
	{0x3C0F, 1, {0x06, 0x0}},
	{0x3C10, 1, {0x06, 0x0}},
	{0x3C11, 1, {0x06, 0x0}},
	{0x3C12, 1, {0x06, 0x0}},
	{0x3C13, 1, {0x06, 0x0}},
	{0x3C18, 1, {0x20, 0x0}},
	{0x3C3A, 1, {0x7A, 0x0}},
	{0x3C40, 1, {0xF4, 0x0}},
	{0x3C48, 1, {0xE6, 0x0}},
	{0x3C54, 1, {0xCE, 0x0}},
	{0x3C56, 1, {0xD0, 0x0}},
	{0x3C6C, 1, {0x53, 0x0}},
	{0x3C6E, 1, {0x55, 0x0}},
	{0x3C70, 1, {0xC0, 0x0}},
	{0x3C72, 1, {0xC2, 0x0}},
	{0x3C7E, 1, {0xCE, 0x0}},
	{0x3C8C, 1, {0xCF, 0x0}},
	{0x3C8E, 1, {0xEB, 0x0}},
	{0x3C98, 1, {0x54, 0x0}},
	{0x3C9A, 1, {0x70, 0x0}},
	{0x3C9C, 1, {0xC1, 0x0}},
	{0x3C9E, 1, {0xDD, 0x0}},
	{0x3CB0, 1, {0x7A, 0x0}},
	{0x3CB2, 1, {0xBA, 0x0}},
	{0x3CC8, 1, {0xBC, 0x0}},
	{0x3CCA, 1, {0x7C, 0x0}},
	{0x3CD4, 1, {0xEA, 0x0}},
	{0x3CD5, 1, {0x01, 0x0}},
	{0x3CD6, 1, {0x4A, 0x0}},
	{0x3CD8, 1, {0x00, 0x0}},
	{0x3CD9, 1, {0x00, 0x0}},
	{0x3CDA, 1, {0xFF, 0x0}},
	{0x3CDB, 1, {0x03, 0x0}},
	{0x3CDC, 1, {0x00, 0x0}},
	{0x3CDD, 1, {0x00, 0x0}},
	{0x3CDE, 1, {0xFF, 0x0}},
	{0x3CDF, 1, {0x03, 0x0}},
	{0x3CE4, 1, {0x4C, 0x0}},
	{0x3CE6, 1, {0xEC, 0x0}},
	{0x3CE7, 1, {0x01, 0x0}},
	{0x3CE8, 1, {0xFF, 0x0}},
	{0x3CE9, 1, {0x03, 0x0}},
	{0x3CEA, 1, {0x00, 0x0}},
	{0x3CEB, 1, {0x00, 0x0}},
	{0x3CEC, 1, {0xFF, 0x0}},
	{0x3CED, 1, {0x03, 0x0}},
	{0x3CEE, 1, {0x00, 0x0}},
	{0x3CEF, 1, {0x00, 0x0}},
	{0x3E28, 1, {0x82, 0x0}},
	{0x3E2A, 1, {0x80, 0x0}},
	{0x3E30, 1, {0x85, 0x0}},
	{0x3E32, 1, {0x7D, 0x0}},
	{0x3E5C, 1, {0xCE, 0x0}},
	{0x3E5E, 1, {0xD3, 0x0}},
	{0x3E70, 1, {0x53, 0x0}},
	{0x3E72, 1, {0x58, 0x0}},
	{0x3E74, 1, {0xC0, 0x0}},
	{0x3E76, 1, {0xC5, 0x0}},
	{0x3E78, 1, {0xC0, 0x0}},
	{0x3E79, 1, {0x01, 0x0}},
	{0x3E7A, 1, {0xD4, 0x0}},
	{0x3E7B, 1, {0x01, 0x0}},
	{0x3EB4, 1, {0x0B, 0x0}},
	{0x3EB5, 1, {0x02, 0x0}},
	{0x3EB6, 1, {0x4D, 0x0}},
	{0x3EEC, 1, {0xF3, 0x0}},
	{0x3EEE, 1, {0xE7, 0x0}},
	{0x3F01, 1, {0x01, 0x0}},
	{0x3F24, 1, {0x10, 0x0}},
	{0x3F28, 1, {0x2D, 0x0}},
	{0x3F2A, 1, {0x2D, 0x0}},
	{0x3F2C, 1, {0x2D, 0x0}},
	{0x3F2E, 1, {0x2D, 0x0}},
	{0x3F30, 1, {0x23, 0x0}},
	{0x3F38, 1, {0x2D, 0x0}},
	{0x3F3A, 1, {0x2D, 0x0}},
	{0x3F3C, 1, {0x2D, 0x0}},
	{0x3F3E, 1, {0x28, 0x0}},
	{0x3F40, 1, {0x1E, 0x0}},
	{0x3F48, 1, {0x2D, 0x0}},
	{0x3F4A, 1, {0x2D, 0x0}},
	{0x4004, 1, {0xE4, 0x0}},
	{0x4006, 1, {0xFF, 0x0}},
	{0x4018, 1, {0x69, 0x0}},
	{0x401A, 1, {0x84, 0x0}},
	{0x401C, 1, {0xD6, 0x0}},
	{0x401E, 1, {0xF1, 0x0}},
	{0x4038, 1, {0xDE, 0x0}},
	{0x403A, 1, {0x00, 0x0}},
	{0x403B, 1, {0x01, 0x0}},
	{0x404C, 1, {0x63, 0x0}},
	{0x404E, 1, {0x85, 0x0}},
	{0x4050, 1, {0xD0, 0x0}},
	{0x4052, 1, {0xF2, 0x0}},
	{0x4108, 1, {0xDD, 0x0}},
	{0x410A, 1, {0xF7, 0x0}},
	{0x411C, 1, {0x62, 0x0}},
	{0x411E, 1, {0x7C, 0x0}},
	{0x4120, 1, {0xCF, 0x0}},
	{0x4122, 1, {0xE9, 0x0}},
	{0x4138, 1, {0xE6, 0x0}},
	{0x413A, 1, {0xF1, 0x0}},
	{0x414C, 1, {0x6B, 0x0}},
	{0x414E, 1, {0x76, 0x0}},
	{0x4150, 1, {0xD8, 0x0}},
	{0x4152, 1, {0xE3, 0x0}},
	{0x417E, 1, {0x03, 0x0}},
	{0x417F, 1, {0x01, 0x0}},
	{0x4186, 1, {0xE0, 0x0}},
	{0x4190, 1, {0xF3, 0x0}},
	{0x4192, 1, {0xF7, 0x0}},
	{0x419C, 1, {0x78, 0x0}},
	{0x419E, 1, {0x7C, 0x0}},
	{0x41A0, 1, {0xE5, 0x0}},
	{0x41A2, 1, {0xE9, 0x0}},
	{0x41C8, 1, {0xE2, 0x0}},
	{0x41CA, 1, {0xFD, 0x0}},
	{0x41DC, 1, {0x67, 0x0}},
	{0x41DE, 1, {0x82, 0x0}},
	{0x41E0, 1, {0xD4, 0x0}},
	{0x41E2, 1, {0xEF, 0x0}},
	{0x4200, 1, {0xDE, 0x0}},
	{0x4202, 1, {0xDA, 0x0}},
	{0x4218, 1, {0x63, 0x0}},
	{0x421A, 1, {0x5F, 0x0}},
	{0x421C, 1, {0xD0, 0x0}},
	{0x421E, 1, {0xCC, 0x0}},
	{0x425A, 1, {0x82, 0x0}},
	{0x425C, 1, {0xEF, 0x0}},
	{0x4348, 1, {0xFE, 0x0}},
	{0x4349, 1, {0x06, 0x0}},
	{0x4352, 1, {0xCE, 0x0}},
	{0x4420, 1, {0x0B, 0x0}},
	{0x4421, 1, {0x02, 0x0}},
	{0x4422, 1, {0x4D, 0x0}},
	{0x4426, 1, {0xF5, 0x0}},
	{0x442A, 1, {0xE7, 0x0}},
	{0x4432, 1, {0xF5, 0x0}},
	{0x4436, 1, {0xE7, 0x0}},
	{0x4466, 1, {0xB4, 0x0}},
	{0x446E, 1, {0x32, 0x0}},
	{0x449F, 1, {0x1C, 0x0}},
	{0x44A4, 1, {0x2C, 0x0}},
	{0x44A6, 1, {0x2C, 0x0}},
	{0x44A8, 1, {0x2C, 0x0}},
	{0x44AA, 1, {0x2C, 0x0}},
	{0x44B4, 1, {0x2C, 0x0}},
	{0x44B6, 1, {0x2C, 0x0}},
	{0x44B8, 1, {0x2C, 0x0}},
	{0x44BA, 1, {0x2C, 0x0}},
	{0x44C4, 1, {0x2C, 0x0}},
	{0x44C6, 1, {0x2C, 0x0}},
	{0x44C8, 1, {0x2C, 0x0}},
	{0x4506, 1, {0xF3, 0x0}},
	{0x450E, 1, {0xE5, 0x0}},
	{0x4516, 1, {0xF3, 0x0}},
	{0x4522, 1, {0xE5, 0x0}},
	{0x4524, 1, {0xF3, 0x0}},
	{0x452C, 1, {0xE5, 0x0}},
	{0x453C, 1, {0x22, 0x0}},
	{0x453D, 1, {0x1B, 0x0}},
	{0x453E, 1, {0x1B, 0x0}},
	{0x453F, 1, {0x15, 0x0}},
	{0x4540, 1, {0x15, 0x0}},
	{0x4541, 1, {0x15, 0x0}},
	{0x4542, 1, {0x15, 0x0}},
	{0x4543, 1, {0x15, 0x0}},
	{0x4544, 1, {0x15, 0x0}},
	{0x4548, 1, {0x00, 0x0}},
	{0x4549, 1, {0x01, 0x0}},
	{0x454A, 1, {0x01, 0x0}},
	{0x454B, 1, {0x06, 0x0}},
	{0x454C, 1, {0x06, 0x0}},
	{0x454D, 1, {0x06, 0x0}},
	{0x454E, 1, {0x06, 0x0}},
	{0x454F, 1, {0x06, 0x0}},
	{0x4550, 1, {0x06, 0x0}},
	{0x4554, 1, {0x55, 0x0}},
	{0x4555, 1, {0x02, 0x0}},
	{0x4556, 1, {0x42, 0x0}},
	{0x4557, 1, {0x05, 0x0}},
	{0x4558, 1, {0xFD, 0x0}},
	{0x4559, 1, {0x05, 0x0}},
	{0x455A, 1, {0x94, 0x0}},
	{0x455B, 1, {0x06, 0x0}},
	{0x455D, 1, {0x06, 0x0}},
	{0x455E, 1, {0x49, 0x0}},
	{0x455F, 1, {0x07, 0x0}},
	{0x4560, 1, {0x7F, 0x0}},
	{0x4561, 1, {0x07, 0x0}},
	{0x4562, 1, {0xA5, 0x0}},
	{0x4564, 1, {0x55, 0x0}},
	{0x4565, 1, {0x02, 0x0}},
	{0x4566, 1, {0x42, 0x0}},
	{0x4567, 1, {0x05, 0x0}},
	{0x4568, 1, {0xFD, 0x0}},
	{0x4569, 1, {0x05, 0x0}},
	{0x456A, 1, {0x94, 0x0}},
	{0x456B, 1, {0x06, 0x0}},
	{0x456D, 1, {0x06, 0x0}},
	{0x456E, 1, {0x49, 0x0}},
	{0x456F, 1, {0x07, 0x0}},
	{0x4572, 1, {0xA5, 0x0}},
	{0x460C, 1, {0x7D, 0x0}},
	{0x460E, 1, {0xB1, 0x0}},
	{0x4614, 1, {0xA8, 0x0}},
	{0x4616, 1, {0xB2, 0x0}},
	{0x461C, 1, {0x7E, 0x0}},
	{0x461E, 1, {0xA7, 0x0}},
	{0x4624, 1, {0xA8, 0x0}},
	{0x4626, 1, {0xB2, 0x0}},
	{0x462C, 1, {0x7E, 0x0}},
	{0x462E, 1, {0x8A, 0x0}},
	{0x4630, 1, {0x94, 0x0}},
	{0x4632, 1, {0xA7, 0x0}},
	{0x4634, 1, {0xFB, 0x0}},
	{0x4636, 1, {0x2F, 0x0}},
	{0x4638, 1, {0x81, 0x0}},
	{0x4639, 1, {0x01, 0x0}},
	{0x463A, 1, {0xB5, 0x0}},
	{0x463B, 1, {0x01, 0x0}},
	{0x463C, 1, {0x26, 0x0}},
	{0x463E, 1, {0x30, 0x0}},
	{0x4640, 1, {0xAC, 0x0}},
	{0x4641, 1, {0x01, 0x0}},
	{0x4642, 1, {0xB6, 0x0}},
	{0x4643, 1, {0x01, 0x0}},
	{0x4644, 1, {0xFC, 0x0}},
	{0x4646, 1, {0x25, 0x0}},
	{0x4648, 1, {0x82, 0x0}},
	{0x4649, 1, {0x01, 0x0}},
	{0x464A, 1, {0xAB, 0x0}},
	{0x464B, 1, {0x01, 0x0}},
	{0x464C, 1, {0x26, 0x0}},
	{0x464E, 1, {0x30, 0x0}},
	{0x4654, 1, {0xFC, 0x0}},
	{0x4656, 1, {0x08, 0x0}},
	{0x4658, 1, {0x12, 0x0}},
	{0x465A, 1, {0x25, 0x0}},
	{0x4662, 1, {0xFC, 0x0}},
	{0x46A2, 1, {0xFB, 0x0}},
	{0x46D6, 1, {0xF3, 0x0}},
	{0x46E6, 1, {0x00, 0x0}},
	{0x46E8, 1, {0xFF, 0x0}},
	{0x46E9, 1, {0x03, 0x0}},
	{0x46EC, 1, {0x7A, 0x0}},
	{0x46EE, 1, {0xE5, 0x0}},
	{0x46F4, 1, {0xEE, 0x0}},
	{0x46F6, 1, {0xF2, 0x0}},
	{0x470C, 1, {0xFF, 0x0}},
	{0x470D, 1, {0x03, 0x0}},
	{0x470E, 1, {0x00, 0x0}},
	{0x4714, 1, {0xE0, 0x0}},
	{0x4716, 1, {0xE4, 0x0}},
	{0x471E, 1, {0xED, 0x0}},
	{0x472E, 1, {0x00, 0x0}},
	{0x4730, 1, {0xFF, 0x0}},
	{0x4731, 1, {0x03, 0x0}},
	{0x4734, 1, {0x7B, 0x0}},
	{0x4736, 1, {0xDF, 0x0}},
	{0x4754, 1, {0x7D, 0x0}},
	{0x4756, 1, {0x8B, 0x0}},
	{0x4758, 1, {0x93, 0x0}},
	{0x475A, 1, {0xB1, 0x0}},
	{0x475C, 1, {0xFB, 0x0}},
	{0x475E, 1, {0x09, 0x0}},
	{0x4760, 1, {0x11, 0x0}},
	{0x4762, 1, {0x2F, 0x0}},
	{0x4766, 1, {0xCC, 0x0}},
	{0x4776, 1, {0xCB, 0x0}},
	{0x477E, 1, {0x4A, 0x0}},
	{0x478E, 1, {0x49, 0x0}},
	{0x4794, 1, {0x7C, 0x0}},
	{0x4796, 1, {0x8F, 0x0}},
	{0x4798, 1, {0xB3, 0x0}},
	{0x4799, 1, {0x00, 0x0}},
	{0x479A, 1, {0xCC, 0x0}},
	{0x479C, 1, {0xC1, 0x0}},
	{0x479E, 1, {0xCB, 0x0}},
	{0x47A4, 1, {0x7D, 0x0}},
	{0x47A6, 1, {0x8E, 0x0}},
	{0x47A8, 1, {0xB4, 0x0}},
	{0x47A9, 1, {0x00, 0x0}},
	{0x47AA, 1, {0xC0, 0x0}},
	{0x47AC, 1, {0xFA, 0x0}},
	{0x47AE, 1, {0x0D, 0x0}},
	{0x47B0, 1, {0x31, 0x0}},
	{0x47B1, 1, {0x01, 0x0}},
	{0x47B2, 1, {0x4A, 0x0}},
	{0x47B3, 1, {0x01, 0x0}},
	{0x47B4, 1, {0x3F, 0x0}},
	{0x47B6, 1, {0x49, 0x0}},
	{0x47BC, 1, {0xFB, 0x0}},
	{0x47BE, 1, {0x0C, 0x0}},
	{0x47C0, 1, {0x32, 0x0}},
	{0x47C1, 1, {0x01, 0x0}},
	{0x47C2, 1, {0x3E, 0x0}},
	{0x47C3, 1, {0x01, 0x0}},
	{SEN_CMD_SETVD, 1, {0x0, 0x0}},
	{SEN_CMD_PRESET, 1, {0x0, 0x0}},
	{SEN_CMD_DIRECTION, 1, {0x0, 0x0}},
	{0x3000, 1, {0x00, 0x0}},  // standby cancel
	{SEN_CMD_DELAY, 1, {24, 0x0}},
	{0x3002, 1, {0x00, 0x0}},  //Master mode start
};

static CTL_SEN_CMD imx678_mode_3[] = {
	{0x3000, 1, {0x01, 0x0}}, // standby
	{0x3002, 1, {0x01, 0x0}}, //Master mode stop
	{0x3014, 1, {0x04, 0x0}},
	{0x3015, 1, {0x03, 0x0}},
	{0x301A, 1, {0x01, 0x0}},
	{0x301B, 1, {0x01, 0x0}},
	{0x301C, 1, {0x01, 0x0}},
	{0x3022, 1, {0x00, 0x0}},
	{0x302C, 1, {0x26, 0x0}},
	{0x302D, 1, {0x02, 0x0}},
	{0x3050, 1, {0x0E, 0x0}},
	{0x3051, 1, {0x11, 0x0}},
	{0x3054, 1, {0x05, 0x0}},
	{0x3060, 1, {0x15, 0x0}},
	{0x3061, 1, {0x00, 0x0}},
	{0x30A6, 1, {0x00, 0x0}},
	{0x3460, 1, {0x22, 0x0}},
	{0x355A, 1, {0x64, 0x0}},
	{0x3A02, 1, {0x7A, 0x0}},
	{0x3A10, 1, {0xEC, 0x0}},
	{0x3A12, 1, {0x71, 0x0}},
	{0x3A14, 1, {0xDE, 0x0}},
	{0x3A20, 1, {0x2B, 0x0}},
	{0x3A24, 1, {0x22, 0x0}},
	{0x3A25, 1, {0x25, 0x0}},
	{0x3A26, 1, {0x2A, 0x0}},
	{0x3A27, 1, {0x2C, 0x0}},
	{0x3A28, 1, {0x39, 0x0}},
	{0x3A29, 1, {0x38, 0x0}},
	{0x3A30, 1, {0x04, 0x0}},
	{0x3A31, 1, {0x04, 0x0}},
	{0x3A32, 1, {0x03, 0x0}},
	{0x3A33, 1, {0x03, 0x0}},
	{0x3A34, 1, {0x09, 0x0}},
	{0x3A35, 1, {0x06, 0x0}},
	{0x3A38, 1, {0xCD, 0x0}},
	{0x3A3A, 1, {0x4C, 0x0}},
	{0x3A3C, 1, {0xB9, 0x0}},
	{0x3A3E, 1, {0x30, 0x0}},
	{0x3A40, 1, {0x2C, 0x0}},
	{0x3A42, 1, {0x39, 0x0}},
	{0x3A4E, 1, {0x00, 0x0}},
	{0x3A52, 1, {0x00, 0x0}},
	{0x3A56, 1, {0x00, 0x0}},
	{0x3A5A, 1, {0x00, 0x0}},
	{0x3A5E, 1, {0x00, 0x0}},
	{0x3A62, 1, {0x00, 0x0}},
	{0x3A6E, 1, {0xA0, 0x0}},
	{0x3A70, 1, {0x50, 0x0}},
	{0x3A8C, 1, {0x04, 0x0}},
	{0x3A8D, 1, {0x03, 0x0}},
	{0x3A8E, 1, {0x09, 0x0}},
	{0x3A90, 1, {0x38, 0x0}},
	{0x3A91, 1, {0x42, 0x0}},
	{0x3A92, 1, {0x3C, 0x0}},
	{0x3B0E, 1, {0xF3, 0x0}},
	{0x3B12, 1, {0xE5, 0x0}},
	{0x3B27, 1, {0xC0, 0x0}},
	{0x3B2E, 1, {0xEF, 0x0}},
	{0x3B30, 1, {0x6A, 0x0}},
	{0x3B32, 1, {0xF6, 0x0}},
	{0x3B36, 1, {0xE1, 0x0}},
	{0x3B3A, 1, {0xE8, 0x0}},
	{0x3B5A, 1, {0x17, 0x0}},
	{0x3B5E, 1, {0xEF, 0x0}},
	{0x3B60, 1, {0x6A, 0x0}},
	{0x3B62, 1, {0xF6, 0x0}},
	{0x3B66, 1, {0xE1, 0x0}},
	{0x3B6A, 1, {0xE8, 0x0}},
	{0x3B88, 1, {0xEC, 0x0}},
	{0x3B8A, 1, {0xED, 0x0}},
	{0x3B94, 1, {0x71, 0x0}},
	{0x3B96, 1, {0x72, 0x0}},
	{0x3B98, 1, {0xDE, 0x0}},
	{0x3B9A, 1, {0xDF, 0x0}},
	{0x3C0F, 1, {0x06, 0x0}},
	{0x3C10, 1, {0x06, 0x0}},
	{0x3C11, 1, {0x06, 0x0}},
	{0x3C12, 1, {0x06, 0x0}},
	{0x3C13, 1, {0x06, 0x0}},
	{0x3C18, 1, {0x20, 0x0}},
	{0x3C3A, 1, {0x7A, 0x0}},
	{0x3C40, 1, {0xF4, 0x0}},
	{0x3C48, 1, {0xE6, 0x0}},
	{0x3C54, 1, {0xCE, 0x0}},
	{0x3C56, 1, {0xD0, 0x0}},
	{0x3C6C, 1, {0x53, 0x0}},
	{0x3C6E, 1, {0x55, 0x0}},
	{0x3C70, 1, {0xC0, 0x0}},
	{0x3C72, 1, {0xC2, 0x0}},
	{0x3C7E, 1, {0xCE, 0x0}},
	{0x3C8C, 1, {0xCF, 0x0}},
	{0x3C8E, 1, {0xEB, 0x0}},
	{0x3C98, 1, {0x54, 0x0}},
	{0x3C9A, 1, {0x70, 0x0}},
	{0x3C9C, 1, {0xC1, 0x0}},
	{0x3C9E, 1, {0xDD, 0x0}},
	{0x3CB0, 1, {0x7A, 0x0}},
	{0x3CB2, 1, {0xBA, 0x0}},
	{0x3CC8, 1, {0xBC, 0x0}},
	{0x3CCA, 1, {0x7C, 0x0}},
	{0x3CD4, 1, {0xEA, 0x0}},
	{0x3CD5, 1, {0x01, 0x0}},
	{0x3CD6, 1, {0x4A, 0x0}},
	{0x3CD8, 1, {0x00, 0x0}},
	{0x3CD9, 1, {0x00, 0x0}},
	{0x3CDA, 1, {0xFF, 0x0}},
	{0x3CDB, 1, {0x03, 0x0}},
	{0x3CDC, 1, {0x00, 0x0}},
	{0x3CDD, 1, {0x00, 0x0}},
	{0x3CDE, 1, {0xFF, 0x0}},
	{0x3CDF, 1, {0x03, 0x0}},
	{0x3CE4, 1, {0x4C, 0x0}},
	{0x3CE6, 1, {0xEC, 0x0}},
	{0x3CE7, 1, {0x01, 0x0}},
	{0x3CE8, 1, {0xFF, 0x0}},
	{0x3CE9, 1, {0x03, 0x0}},
	{0x3CEA, 1, {0x00, 0x0}},
	{0x3CEB, 1, {0x00, 0x0}},
	{0x3CEC, 1, {0xFF, 0x0}},
	{0x3CED, 1, {0x03, 0x0}},
	{0x3CEE, 1, {0x00, 0x0}},
	{0x3CEF, 1, {0x00, 0x0}},
	{0x3E28, 1, {0x82, 0x0}},
	{0x3E2A, 1, {0x80, 0x0}},
	{0x3E30, 1, {0x85, 0x0}},
	{0x3E32, 1, {0x7D, 0x0}},
	{0x3E5C, 1, {0xCE, 0x0}},
	{0x3E5E, 1, {0xD3, 0x0}},
	{0x3E70, 1, {0x53, 0x0}},
	{0x3E72, 1, {0x58, 0x0}},
	{0x3E74, 1, {0xC0, 0x0}},
	{0x3E76, 1, {0xC5, 0x0}},
	{0x3E78, 1, {0xC0, 0x0}},
	{0x3E79, 1, {0x01, 0x0}},
	{0x3E7A, 1, {0xD4, 0x0}},
	{0x3E7B, 1, {0x01, 0x0}},
	{0x3EB4, 1, {0x0B, 0x0}},
	{0x3EB5, 1, {0x02, 0x0}},
	{0x3EB6, 1, {0x4D, 0x0}},
	{0x3EEC, 1, {0xF3, 0x0}},
	{0x3EEE, 1, {0xE7, 0x0}},
	{0x3F01, 1, {0x01, 0x0}},
	{0x3F24, 1, {0x10, 0x0}},
	{0x3F28, 1, {0x2D, 0x0}},
	{0x3F2A, 1, {0x2D, 0x0}},
	{0x3F2C, 1, {0x2D, 0x0}},
	{0x3F2E, 1, {0x2D, 0x0}},
	{0x3F30, 1, {0x23, 0x0}},
	{0x3F38, 1, {0x2D, 0x0}},
	{0x3F3A, 1, {0x2D, 0x0}},
	{0x3F3C, 1, {0x2D, 0x0}},
	{0x3F3E, 1, {0x28, 0x0}},
	{0x3F40, 1, {0x1E, 0x0}},
	{0x3F48, 1, {0x2D, 0x0}},
	{0x3F4A, 1, {0x2D, 0x0}},
	{0x4004, 1, {0xE4, 0x0}},
	{0x4006, 1, {0xFF, 0x0}},
	{0x4018, 1, {0x69, 0x0}},
	{0x401A, 1, {0x84, 0x0}},
	{0x401C, 1, {0xD6, 0x0}},
	{0x401E, 1, {0xF1, 0x0}},
	{0x4038, 1, {0xDE, 0x0}},
	{0x403A, 1, {0x00, 0x0}},
	{0x403B, 1, {0x01, 0x0}},
	{0x404C, 1, {0x63, 0x0}},
	{0x404E, 1, {0x85, 0x0}},
	{0x4050, 1, {0xD0, 0x0}},
	{0x4052, 1, {0xF2, 0x0}},
	{0x4108, 1, {0xDD, 0x0}},
	{0x410A, 1, {0xF7, 0x0}},
	{0x411C, 1, {0x62, 0x0}},
	{0x411E, 1, {0x7C, 0x0}},
	{0x4120, 1, {0xCF, 0x0}},
	{0x4122, 1, {0xE9, 0x0}},
	{0x4138, 1, {0xE6, 0x0}},
	{0x413A, 1, {0xF1, 0x0}},
	{0x414C, 1, {0x6B, 0x0}},
	{0x414E, 1, {0x76, 0x0}},
	{0x4150, 1, {0xD8, 0x0}},
	{0x4152, 1, {0xE3, 0x0}},
	{0x417E, 1, {0x03, 0x0}},
	{0x417F, 1, {0x01, 0x0}},
	{0x4186, 1, {0xE0, 0x0}},
	{0x4190, 1, {0xF3, 0x0}},
	{0x4192, 1, {0xF7, 0x0}},
	{0x419C, 1, {0x78, 0x0}},
	{0x419E, 1, {0x7C, 0x0}},
	{0x41A0, 1, {0xE5, 0x0}},
	{0x41A2, 1, {0xE9, 0x0}},
	{0x41C8, 1, {0xE2, 0x0}},
	{0x41CA, 1, {0xFD, 0x0}},
	{0x41DC, 1, {0x67, 0x0}},
	{0x41DE, 1, {0x82, 0x0}},
	{0x41E0, 1, {0xD4, 0x0}},
	{0x41E2, 1, {0xEF, 0x0}},
	{0x4200, 1, {0xDE, 0x0}},
	{0x4202, 1, {0xDA, 0x0}},
	{0x4218, 1, {0x63, 0x0}},
	{0x421A, 1, {0x5F, 0x0}},
	{0x421C, 1, {0xD0, 0x0}},
	{0x421E, 1, {0xCC, 0x0}},
	{0x425A, 1, {0x82, 0x0}},
	{0x425C, 1, {0xEF, 0x0}},
	{0x4348, 1, {0xFE, 0x0}},
	{0x4349, 1, {0x06, 0x0}},
	{0x4352, 1, {0xCE, 0x0}},
	{0x4420, 1, {0x0B, 0x0}},
	{0x4421, 1, {0x02, 0x0}},
	{0x4422, 1, {0x4D, 0x0}},
	{0x4426, 1, {0xF5, 0x0}},
	{0x442A, 1, {0xE7, 0x0}},
	{0x4432, 1, {0xF5, 0x0}},
	{0x4436, 1, {0xE7, 0x0}},
	{0x4466, 1, {0xB4, 0x0}},
	{0x446E, 1, {0x32, 0x0}},
	{0x449F, 1, {0x1C, 0x0}},
	{0x44A4, 1, {0x2C, 0x0}},
	{0x44A6, 1, {0x2C, 0x0}},
	{0x44A8, 1, {0x2C, 0x0}},
	{0x44AA, 1, {0x2C, 0x0}},
	{0x44B4, 1, {0x2C, 0x0}},
	{0x44B6, 1, {0x2C, 0x0}},
	{0x44B8, 1, {0x2C, 0x0}},
	{0x44BA, 1, {0x2C, 0x0}},
	{0x44C4, 1, {0x2C, 0x0}},
	{0x44C6, 1, {0x2C, 0x0}},
	{0x44C8, 1, {0x2C, 0x0}},
	{0x4506, 1, {0xF3, 0x0}},
	{0x450E, 1, {0xE5, 0x0}},
	{0x4516, 1, {0xF3, 0x0}},
	{0x4522, 1, {0xE5, 0x0}},
	{0x4524, 1, {0xF3, 0x0}},
	{0x452C, 1, {0xE5, 0x0}},
	{0x453C, 1, {0x22, 0x0}},
	{0x453D, 1, {0x1B, 0x0}},
	{0x453E, 1, {0x1B, 0x0}},
	{0x453F, 1, {0x15, 0x0}},
	{0x4540, 1, {0x15, 0x0}},
	{0x4541, 1, {0x15, 0x0}},
	{0x4542, 1, {0x15, 0x0}},
	{0x4543, 1, {0x15, 0x0}},
	{0x4544, 1, {0x15, 0x0}},
	{0x4548, 1, {0x00, 0x0}},
	{0x4549, 1, {0x01, 0x0}},
	{0x454A, 1, {0x01, 0x0}},
	{0x454B, 1, {0x06, 0x0}},
	{0x454C, 1, {0x06, 0x0}},
	{0x454D, 1, {0x06, 0x0}},
	{0x454E, 1, {0x06, 0x0}},
	{0x454F, 1, {0x06, 0x0}},
	{0x4550, 1, {0x06, 0x0}},
	{0x4554, 1, {0x55, 0x0}},
	{0x4555, 1, {0x02, 0x0}},
	{0x4556, 1, {0x42, 0x0}},
	{0x4557, 1, {0x05, 0x0}},
	{0x4558, 1, {0xFD, 0x0}},
	{0x4559, 1, {0x05, 0x0}},
	{0x455A, 1, {0x94, 0x0}},
	{0x455B, 1, {0x06, 0x0}},
	{0x455D, 1, {0x06, 0x0}},
	{0x455E, 1, {0x49, 0x0}},
	{0x455F, 1, {0x07, 0x0}},
	{0x4560, 1, {0x7F, 0x0}},
	{0x4561, 1, {0x07, 0x0}},
	{0x4562, 1, {0xA5, 0x0}},
	{0x4564, 1, {0x55, 0x0}},
	{0x4565, 1, {0x02, 0x0}},
	{0x4566, 1, {0x42, 0x0}},
	{0x4567, 1, {0x05, 0x0}},
	{0x4568, 1, {0xFD, 0x0}},
	{0x4569, 1, {0x05, 0x0}},
	{0x456A, 1, {0x94, 0x0}},
	{0x456B, 1, {0x06, 0x0}},
	{0x456D, 1, {0x06, 0x0}},
	{0x456E, 1, {0x49, 0x0}},
	{0x456F, 1, {0x07, 0x0}},
	{0x4572, 1, {0xA5, 0x0}},
	{0x460C, 1, {0x7D, 0x0}},
	{0x460E, 1, {0xB1, 0x0}},
	{0x4614, 1, {0xA8, 0x0}},
	{0x4616, 1, {0xB2, 0x0}},
	{0x461C, 1, {0x7E, 0x0}},
	{0x461E, 1, {0xA7, 0x0}},
	{0x4624, 1, {0xA8, 0x0}},
	{0x4626, 1, {0xB2, 0x0}},
	{0x462C, 1, {0x7E, 0x0}},
	{0x462E, 1, {0x8A, 0x0}},
	{0x4630, 1, {0x94, 0x0}},
	{0x4632, 1, {0xA7, 0x0}},
	{0x4634, 1, {0xFB, 0x0}},
	{0x4636, 1, {0x2F, 0x0}},
	{0x4638, 1, {0x81, 0x0}},
	{0x4639, 1, {0x01, 0x0}},
	{0x463A, 1, {0xB5, 0x0}},
	{0x463B, 1, {0x01, 0x0}},
	{0x463C, 1, {0x26, 0x0}},
	{0x463E, 1, {0x30, 0x0}},
	{0x4640, 1, {0xAC, 0x0}},
	{0x4641, 1, {0x01, 0x0}},
	{0x4642, 1, {0xB6, 0x0}},
	{0x4643, 1, {0x01, 0x0}},
	{0x4644, 1, {0xFC, 0x0}},
	{0x4646, 1, {0x25, 0x0}},
	{0x4648, 1, {0x82, 0x0}},
	{0x4649, 1, {0x01, 0x0}},
	{0x464A, 1, {0xAB, 0x0}},
	{0x464B, 1, {0x01, 0x0}},
	{0x464C, 1, {0x26, 0x0}},
	{0x464E, 1, {0x30, 0x0}},
	{0x4654, 1, {0xFC, 0x0}},
	{0x4656, 1, {0x08, 0x0}},
	{0x4658, 1, {0x12, 0x0}},
	{0x465A, 1, {0x25, 0x0}},
	{0x4662, 1, {0xFC, 0x0}},
	{0x46A2, 1, {0xFB, 0x0}},
	{0x46D6, 1, {0xF3, 0x0}},
	{0x46E6, 1, {0x00, 0x0}},
	{0x46E8, 1, {0xFF, 0x0}},
	{0x46E9, 1, {0x03, 0x0}},
	{0x46EC, 1, {0x7A, 0x0}},
	{0x46EE, 1, {0xE5, 0x0}},
	{0x46F4, 1, {0xEE, 0x0}},
	{0x46F6, 1, {0xF2, 0x0}},
	{0x470C, 1, {0xFF, 0x0}},
	{0x470D, 1, {0x03, 0x0}},
	{0x470E, 1, {0x00, 0x0}},
	{0x4714, 1, {0xE0, 0x0}},
	{0x4716, 1, {0xE4, 0x0}},
	{0x471E, 1, {0xED, 0x0}},
	{0x472E, 1, {0x00, 0x0}},
	{0x4730, 1, {0xFF, 0x0}},
	{0x4731, 1, {0x03, 0x0}},
	{0x4734, 1, {0x7B, 0x0}},
	{0x4736, 1, {0xDF, 0x0}},
	{0x4754, 1, {0x7D, 0x0}},
	{0x4756, 1, {0x8B, 0x0}},
	{0x4758, 1, {0x93, 0x0}},
	{0x475A, 1, {0xB1, 0x0}},
	{0x475C, 1, {0xFB, 0x0}},
	{0x475E, 1, {0x09, 0x0}},
	{0x4760, 1, {0x11, 0x0}},
	{0x4762, 1, {0x2F, 0x0}},
	{0x4766, 1, {0xCC, 0x0}},
	{0x4776, 1, {0xCB, 0x0}},
	{0x477E, 1, {0x4A, 0x0}},
	{0x478E, 1, {0x49, 0x0}},
	{0x4794, 1, {0x7C, 0x0}},
	{0x4796, 1, {0x8F, 0x0}},
	{0x4798, 1, {0xB3, 0x0}},
	{0x4799, 1, {0x00, 0x0}},
	{0x479A, 1, {0xCC, 0x0}},
	{0x479C, 1, {0xC1, 0x0}},
	{0x479E, 1, {0xCB, 0x0}},
	{0x47A4, 1, {0x7D, 0x0}},
	{0x47A6, 1, {0x8E, 0x0}},
	{0x47A8, 1, {0xB4, 0x0}},
	{0x47A9, 1, {0x00, 0x0}},
	{0x47AA, 1, {0xC0, 0x0}},
	{0x47AC, 1, {0xFA, 0x0}},
	{0x47AE, 1, {0x0D, 0x0}},
	{0x47B0, 1, {0x31, 0x0}},
	{0x47B1, 1, {0x01, 0x0}},
	{0x47B2, 1, {0x4A, 0x0}},
	{0x47B3, 1, {0x01, 0x0}},
	{0x47B4, 1, {0x3F, 0x0}},
	{0x47B6, 1, {0x49, 0x0}},
	{0x47BC, 1, {0xFB, 0x0}},
	{0x47BE, 1, {0x0C, 0x0}},
	{0x47C0, 1, {0x32, 0x0}},
	{0x47C1, 1, {0x01, 0x0}},
	{0x47C2, 1, {0x3E, 0x0}},
	{0x47C3, 1, {0x01, 0x0}},
	{SEN_CMD_SETVD, 1, {0x0, 0x0}},
	{SEN_CMD_PRESET, 1, {0x0, 0x0}},
	{SEN_CMD_DIRECTION, 1, {0x0, 0x0}},
	{0x3000, 1, {0x00, 0x0}},  // standby cancel
	{SEN_CMD_DELAY, 1, {24, 0x0}},
	{0x3002, 1, {0x00, 0x0}},  //Master mode start
};

static UINT32 cur_sen_mode[CTL_SEN_ID_MAX] = {CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1, CTL_SEN_MODE_1};
static UINT32 cur_fps[CTL_SEN_ID_MAX] = {0};
static UINT32 chgmode_fps[CTL_SEN_ID_MAX] = {0};
static UINT32 power_ctrl_mclk[CTL_SEN_CLK_SEL_MAX] = {0};
static UINT32 reset_ctrl_count[CTL_SEN_ID_MAX] = {0};
static UINT32 pwdn_ctrl_count[CTL_SEN_ID_MAX] = {0};
static ISP_SENSOR_CTRL sensor_ctrl_last[CTL_SEN_ID_MAX] = {0};
static ISP_SENSOR_PRESET_CTRL preset_ctrl[CTL_SEN_ID_MAX] = {0};
static UINT32 compensation_ratio[CTL_SEN_ID_MAX][ISP_SEN_MFRAME_MAX_NUM] = {0};
static UINT32 each_frame_gain_flag[CTL_SEN_ID_MAX] = {0};
static INT32 is_fastboot[CTL_SEN_ID_MAX];
static UINT32 fastboot_i2c_id[CTL_SEN_ID_MAX];
static UINT32 fastboot_i2c_addr[CTL_SEN_ID_MAX];
static BOOL i2c_valid[CTL_SEN_ID_MAX];

static CTL_SEN_DRV_TAB imx678_sen_drv_tab = {
	sen_open_imx678,
	sen_close_imx678,
	sen_sleep_imx678,
	sen_wakeup_imx678,
	sen_write_reg_imx678,
	sen_read_reg_imx678,
	sen_chg_mode_imx678,
	sen_chg_fps_imx678,
	sen_set_info_imx678,
	sen_get_info_imx678,
};

static CTL_SEN_DRV_TAB *sen_get_drv_tab_imx678(void)
{
	return &imx678_sen_drv_tab;
}

static void sen_pwr_ctrl_imx678(CTL_SEN_ID id, CTL_SEN_PWR_CTRL_FLAG flag, CTL_SEN_CLK_CB clk_cb)
{
	UINT32 i = 0;
	UINT32 reset_count = 0, pwdn_count = 0;
	DBG_IND("enter flag %d \r\n", flag);

	if ((flag == CTL_SEN_PWR_CTRL_TURN_ON) && ((!is_fastboot[id]) || (fastboot_i2c_id[id] != sen_i2c[id].id) || (fastboot_i2c_addr[id] != sen_i2c[id].addr))) {

		if (sen_power[id].pwdn_pin != CTL_SEN_IGNORE) {
			for ( i = 0; i < CTL_SEN_ID_MAX ; i++ ) {
				if ( pwdn_ctrl_count[i] == (sen_power[id].pwdn_pin)) {
					pwdn_count++;
				}
			}
			pwdn_ctrl_count[id] = (sen_power[id].pwdn_pin);

			if (!pwdn_count) {
				gpio_direction_output((sen_power[id].pwdn_pin), 0);
				gpio_set_value((sen_power[id].pwdn_pin), 0);
				gpio_set_value((sen_power[id].pwdn_pin), 1);
			}
		}

		if (clk_cb != NULL) {
			if (sen_power[id].mclk != CTL_SEN_IGNORE) {
				if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK ) {
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK] += 1;
				} else if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK2) {
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK2] += 1;
				} else { //CTL_SEN_CLK_SEL_SIEMCLK3
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK3] += 1;
				}
				if (1 == (power_ctrl_mclk[sen_power[id].mclk])) {
					clk_cb(sen_power[id].mclk, TRUE);
				}
			}
		}

		if (sen_power[id].rst_pin != CTL_SEN_IGNORE) {
			vos_util_delay_ms(sen_power[id].stable_time);
			for ( i = 0; i < CTL_SEN_ID_MAX ; i++ ) {
				if ( reset_ctrl_count[i] == (sen_power[id].rst_pin)) {
					reset_count++;
				}
			}
			reset_ctrl_count[id] = (sen_power[id].rst_pin);

			if (!reset_count) {
				gpio_direction_output((sen_power[id].rst_pin), 0);
				gpio_set_value((sen_power[id].rst_pin), 0);
				vos_util_delay_ms(sen_power[id].rst_time);
				gpio_set_value((sen_power[id].rst_pin), 1);
				vos_util_delay_ms(sen_power[id].stable_time);
			}
		}
	}

	if (flag == CTL_SEN_PWR_CTRL_TURN_OFF) {
		
		if (sen_power[id].pwdn_pin != CTL_SEN_IGNORE) {
			pwdn_ctrl_count[id] = 0;

			for ( i = 0; i < CTL_SEN_ID_MAX ; i++ ) {
				if ( pwdn_ctrl_count[i] == (sen_power[id].pwdn_pin)) {
					pwdn_count++;
				}
			}

			if (!pwdn_count) {
				gpio_direction_output((sen_power[id].pwdn_pin), 0);
				gpio_set_value((sen_power[id].pwdn_pin), 0);
			}
		}

		if (sen_power[id].rst_pin != CTL_SEN_IGNORE) {
			reset_ctrl_count[id] = 0;

			for ( i = 0; i < CTL_SEN_ID_MAX ; i++ ) {
				if ( reset_ctrl_count[i] == (sen_power[id].rst_pin)) {
					reset_count++;
				}
			}

			if (!reset_count) {
				gpio_direction_output((sen_power[id].rst_pin), 0);
				gpio_set_value((sen_power[id].rst_pin), 0);
				vos_util_delay_ms(sen_power[id].stable_time);
			}
		}
		
		if (clk_cb != NULL) {
			if (sen_power[id].mclk != CTL_SEN_IGNORE) {			
				if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK ) {
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK] -= 1;
				} else if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK2) {
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK2] -= 1;
				} else { //CTL_SEN_CLK_SEL_SIEMCLK3
					power_ctrl_mclk[CTL_SEN_CLK_SEL_SIEMCLK3] -= 1;
				}
				if (!power_ctrl_mclk[sen_power[id].mclk]) {
					clk_cb(sen_power[id].mclk, FALSE);
				}
			}
		}
	}
}

static CTL_SEN_CMD sen_set_cmd_info_imx678(UINT32 addr, UINT32 data_length, UINT32 data0, UINT32 data1)
{
	CTL_SEN_CMD cmd;

	cmd.addr = addr;
	cmd.data_len = data_length;
	cmd.data[0] = data0;
	cmd.data[1] = data1;
	return cmd;
}

#if defined(__KERNEL__)
static void sen_load_cfg_from_compatible_imx678(struct device_node *of_node)
{
	DBG_DUMP("compatible valid, using peri-dev.dtsi \r\n");
	sen_common_load_cfg_preset_compatible(of_node, &sen_preset);
	sen_common_load_cfg_direction_compatible(of_node, &sen_direction);
	sen_common_load_cfg_power_compatible(of_node, &sen_power);
	sen_common_load_cfg_i2c_compatible(of_node, &sen_i2c);
}
#endif

static ER sen_open_imx678(CTL_SEN_ID id)
{
	ER rt = E_OK;

	#if defined(__KERNEL__)
	sen_i2c_reg_cb(sen_load_cfg_from_compatible_imx678);
	#endif

	preset_ctrl[id].mode = ISP_SENSOR_PRESET_DEFAULT;
	i2c_valid[id] = TRUE;
	if ((!is_fastboot[id]) || (fastboot_i2c_id[id] != sen_i2c[id].id) || (fastboot_i2c_addr[id] != sen_i2c[id].addr)) {
		rt = sen_i2c_init_driver(id, &sen_i2c[id]);

		if (rt != E_OK) {
			i2c_valid[id] = FALSE;

			DBG_ERR("init. i2c driver fail (%d) \r\n", id);
		}
	}

	return rt;
}

static ER sen_close_imx678(CTL_SEN_ID id)
{
	if ((!is_fastboot[id]) || (fastboot_i2c_id[id] != sen_i2c[id].id) || (fastboot_i2c_addr[id] != sen_i2c[id].addr)) {
		if (i2c_valid[id]) {
			sen_i2c_remove_driver(id);
		}
	} else {
		is_fastboot[id] = 0;
		#if defined(__KERNEL__)
		isp_builtin_uninit_i2c(id);
		#endif
	}

	i2c_valid[id] = FALSE;

	return E_OK;
}

static ER sen_sleep_imx678(CTL_SEN_ID id)
{
	DBG_IND("enter \r\n");
	return E_OK;
}

static ER sen_wakeup_imx678(CTL_SEN_ID id)
{
	DBG_IND("enter \r\n");
	return E_OK;
}

static ER sen_write_reg_imx678(CTL_SEN_ID id, CTL_SEN_CMD *cmd)
{
	struct i2c_msg msgs;
	unsigned char buf[3];
	int i;

	if (!i2c_valid[id]) {
		return E_NOSPT;
	}

	buf[0]     = (cmd->addr >> 8) & 0xFF;
	buf[1]     = cmd->addr & 0xFF;
	buf[2]     = cmd->data[0] & 0xFF;
	msgs.addr  = sen_i2c[id].addr;
	msgs.flags = 0;
	msgs.len   = 3;
	msgs.buf   = buf;

	if ((!is_fastboot[id]) || (fastboot_i2c_id[id] != sen_i2c[id].id) || (fastboot_i2c_addr[id] != sen_i2c[id].addr)) {
		i = 0;
		while(1){
			if (sen_i2c_transfer(id, &msgs, 1) == 0)
				break;
			i++;
			if (i == 5)
				return E_SYS;
		}
	} else {
		#if defined(__KERNEL__)
		isp_builtin_set_transfer_i2c(id, &msgs, 1);
		#endif
	}

	return E_OK;
}

static ER sen_read_reg_imx678(CTL_SEN_ID id, CTL_SEN_CMD *cmd)
{
	struct i2c_msg  msgs[2];
	unsigned char   tmp[2], tmp2[2];
	int i;

	if (!i2c_valid[id]) {
		return E_NOSPT;
	}

	tmp[0]        = (cmd->addr >> 8) & 0xFF;
	tmp[1]        = cmd->addr & 0xFF;
	msgs[0].addr  = sen_i2c[id].addr;
	msgs[0].flags = 0;
	msgs[0].len   = 2;
	msgs[0].buf   = tmp;

	tmp2[0]       = 0;
	msgs[1].addr  = sen_i2c[id].addr;
	msgs[1].flags = 1;
	msgs[1].len   = 1;
	msgs[1].buf   = tmp2;

	if ((!is_fastboot[id]) || (fastboot_i2c_id[id] != sen_i2c[id].id) || (fastboot_i2c_addr[id] != sen_i2c[id].addr)) {
		i = 0;
		while(1){
			if (sen_i2c_transfer(id, msgs, 2) == 0)
				break;
			i++;
			if (i == 5)
				return E_SYS;
		}
	} else {
		#if defined(__KERNEL__)
		isp_builtin_set_transfer_i2c(id, msgs, 2);
		#endif
	}

	cmd->data[0] = tmp2[0];

	return E_OK;
}

static UINT32 sen_get_cmd_tab_imx678(CTL_SEN_MODE mode, CTL_SEN_CMD **cmd_tab)
{
	switch (mode) {
	case CTL_SEN_MODE_1:
		*cmd_tab = imx678_mode_1;
		return sizeof(imx678_mode_1) / sizeof(CTL_SEN_CMD);

	case CTL_SEN_MODE_2:
		*cmd_tab = imx678_mode_2;
		return sizeof(imx678_mode_2) / sizeof(CTL_SEN_CMD);

	case CTL_SEN_MODE_3:
		*cmd_tab = imx678_mode_3;
		return sizeof(imx678_mode_3) / sizeof(CTL_SEN_CMD);

	default:
		DBG_ERR("sensor mode %d no cmd table\r\n", mode);
		*cmd_tab = NULL;
		return 0;
	}
}

static ER sen_chg_mode_imx678(CTL_SEN_ID id, CTL_SENDRV_CHGMODE_OBJ chgmode_obj)
{
	ISP_SENSOR_CTRL sensor_ctrl = {0};
	CTL_SEN_CMD *p_cmd_list = NULL, cmd;
	CTL_SEN_FLIP flip = CTL_SEN_FLIP_NONE;
	UINT32 sensor_vd;
	UINT32 idx, cmd_num = 0;
	ER rt = E_OK;
	
	if (cur_sen_mode[id] != chgmode_obj.mode) {
		gpio_direction_output((sen_power[id].rst_pin), 0);
		gpio_set_value((sen_power[id].rst_pin), 0);
		vos_util_delay_ms(sen_power[id].rst_time);
		gpio_set_value((sen_power[id].rst_pin), 1);
		vos_util_delay_ms(sen_power[id].stable_time);
	}

	cur_sen_mode[id] = chgmode_obj.mode;

	if (is_fastboot[id]) {
		#if defined(__KERNEL__)
		ISP_BUILTIN_SENSOR_CTRL *p_sensor_ctrl_temp;

		p_sensor_ctrl_temp = isp_builtin_get_sensor_gain(id);
		sensor_ctrl.gain_ratio[0] = p_sensor_ctrl_temp->gain_ratio[0];
		sensor_ctrl.gain_ratio[1] = p_sensor_ctrl_temp->gain_ratio[1];
		p_sensor_ctrl_temp = isp_builtin_get_sensor_expt(id);
		sensor_ctrl.exp_time[0] = p_sensor_ctrl_temp->exp_time[0];
		sensor_ctrl.exp_time[1] = p_sensor_ctrl_temp->exp_time[1];
		sen_set_chgmode_fps_imx678(id, isp_builtin_get_chgmode_fps(id));
		sen_set_cur_fps_imx678(id, isp_builtin_get_chgmode_fps(id));
		sen_set_gain_imx678(id, &sensor_ctrl);
		sen_set_expt_imx678(id, &sensor_ctrl);
		#endif
		preset_ctrl[id].mode = ISP_SENSOR_PRESET_CHGMODE;

		return E_OK;
	}

	// get & set sensor cmd table
	cmd_num = sen_get_cmd_tab_imx678(chgmode_obj.mode, &p_cmd_list);
	if (p_cmd_list == NULL) {
		DBG_ERR("%s: SenMode(%d) out of range!!! \r\n", __func__, chgmode_obj.mode);
		return E_SYS;
	}

	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_CHGFPS) {
		sensor_vd = sen_calc_chgmode_vd_imx678(id, chgmode_obj.frame_rate);
	} else {
		DBG_WRN(" not support fps adjust \r\n");
		sen_set_cur_fps_imx678(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
		sen_set_chgmode_fps_imx678(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
		sensor_vd = mode_basic_param[cur_sen_mode[id]].signal_info.vd_period;
	}

	for (idx = 0; idx < cmd_num; idx++) {
		if (p_cmd_list[idx].addr == SEN_CMD_DELAY) {
			vos_util_delay_ms((p_cmd_list[idx].data[0] & 0xFF) | ((p_cmd_list[idx].data[1] & 0xFF) << 8));
		} else if (p_cmd_list[idx].addr == SEN_CMD_SETVD) {
			cmd = sen_set_cmd_info_imx678(0x302A, 1, (sensor_vd >> 16) & 0x0F, 0x00);
			rt |= sen_write_reg_imx678(id, &cmd);
			cmd = sen_set_cmd_info_imx678(0x3029, 1, (sensor_vd >> 8) & 0xFF, 0x00);
			rt |= sen_write_reg_imx678(id, &cmd);			
			cmd = sen_set_cmd_info_imx678(0x3028, 1, sensor_vd & 0xFF, 0x00);
			rt |= sen_write_reg_imx678(id, &cmd);
		} else if (p_cmd_list[idx].addr == SEN_CMD_PRESET) {
			switch (preset_ctrl[id].mode) {
				default:
				case ISP_SENSOR_PRESET_DEFAULT:
					sensor_ctrl.gain_ratio[0] = sen_preset[id].gain_ratio;
					sensor_ctrl.exp_time[0] = sen_preset[id].expt_time;
					if (mode_basic_param[cur_sen_mode[id]].frame_num == 2) {
						sensor_ctrl.exp_time[1] = sen_preset[id].expt_time >> 3;
					}
					break;

				case ISP_SENSOR_PRESET_CHGMODE:
					memcpy(&sensor_ctrl, &sensor_ctrl_last[id], sizeof(ISP_SENSOR_CTRL));
					break;

				case ISP_SENSOR_PRESET_AE:
					sensor_ctrl.exp_time[0] = preset_ctrl[id].exp_time[0];
					sensor_ctrl.exp_time[1] = preset_ctrl[id].exp_time[1];
					sensor_ctrl.gain_ratio[0] = preset_ctrl[id].gain_ratio[0];
					sensor_ctrl.gain_ratio[1] = preset_ctrl[id].gain_ratio[1];
				break;
			}
			sen_set_gain_imx678(id, &sensor_ctrl);
			sen_set_expt_imx678(id, &sensor_ctrl);
		} else if (p_cmd_list[idx].addr == SEN_CMD_DIRECTION) {
			if (sen_direction[id].mirror) {
				flip |= CTL_SEN_FLIP_H;
			}
			if (sen_direction[id].flip) {
				flip |= CTL_SEN_FLIP_V;
			}
			sen_set_flip_imx678(id, &flip);
		} else {
			cmd = sen_set_cmd_info_imx678(p_cmd_list[idx].addr, p_cmd_list[idx].data_len, p_cmd_list[idx].data[0], p_cmd_list[idx].data[1]);
			rt |= sen_write_reg_imx678(id, &cmd);
		}
	}

	preset_ctrl[id].mode = ISP_SENSOR_PRESET_CHGMODE;

	if (rt != E_OK) {
		DBG_ERR("write register error %d \r\n", (INT)rt);
		return rt;
	}

	return E_OK;
}

static ER sen_chg_fps_imx678(CTL_SEN_ID id, UINT32 fps)
{
	CTL_SEN_CMD cmd;
	UINT32 sensor_vd;
	ER rt = E_OK;

	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_CHGFPS) {
		sensor_vd = sen_calc_chgmode_vd_imx678(id, fps);
	} else {
		DBG_WRN(" not support fps adjust \r\n");
		sen_set_cur_fps_imx678(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
		sen_set_chgmode_fps_imx678(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
		sensor_vd = mode_basic_param[cur_sen_mode[id]].signal_info.vd_period;
	}

	cmd = sen_set_cmd_info_imx678(0x302A, 1, (sensor_vd >> 16) & 0x0F, 0x00);
	rt |= sen_write_reg_imx678(id, &cmd);
	cmd = sen_set_cmd_info_imx678(0x3029, 1, (sensor_vd >> 8) & 0xFF, 0x00);
	rt |= sen_write_reg_imx678(id, &cmd);
	cmd = sen_set_cmd_info_imx678(0x3028, 1, sensor_vd & 0xFF, 0x00);
	rt |= sen_write_reg_imx678(id, &cmd);

	return rt;
}

static ER sen_set_info_imx678(CTL_SEN_ID id, CTL_SENDRV_CFGID drv_cfg_id, void *data)
{
	switch (drv_cfg_id) {
	case CTL_SENDRV_CFGID_SET_EXPT:
		sen_set_expt_imx678(id, data);
		break;
	case CTL_SENDRV_CFGID_SET_GAIN:
		sen_set_gain_imx678(id, data);
		break;
	case CTL_SENDRV_CFGID_FLIP_TYPE:
		sen_set_flip_imx678(id, (CTL_SEN_FLIP *)(data));
		break;
	case CTL_SENDRV_CFGID_USER_DEFINE1:
		sen_set_preset_imx678(id, (ISP_SENSOR_PRESET_CTRL *)(data));
		break;
	default:
		return E_NOSPT;
	}
	return E_OK;
}

static ER sen_get_info_imx678(CTL_SEN_ID id, CTL_SENDRV_CFGID drv_cfg_id, void *data)
{
	ER rt = E_OK;

	switch (drv_cfg_id) {
	case CTL_SENDRV_CFGID_GET_EXPT:
		sen_get_expt_imx678(id, data);
		break;
	case CTL_SENDRV_CFGID_GET_GAIN:
		sen_get_gain_imx678(id, data);
		break;
	case CTL_SENDRV_CFGID_GET_ATTR_BASIC:
		sen_get_attr_basic_imx678(id, (CTL_SENDRV_GET_ATTR_BASIC_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_ATTR_SIGNAL:
		sen_get_attr_signal_imx678((CTL_SENDRV_GET_ATTR_SIGNAL_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_ATTR_CMDIF:
		rt = sen_get_attr_cmdif_imx678(id, (CTL_SENDRV_GET_ATTR_CMDIF_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_ATTR_IF:
		rt = sen_get_attr_if_imx678((CTL_SENDRV_GET_ATTR_IF_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_FPS:
		sen_get_fps_imx678(id, (CTL_SENDRV_GET_FPS_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_SPEED:
		sen_get_speed_imx678(id, (CTL_SENDRV_GET_SPEED_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_MODE_BASIC:
		sen_get_mode_basic_imx678((CTL_SENDRV_GET_MODE_BASIC_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_MODE_MIPI:
		sen_get_mode_mipi_imx678((CTL_SENDRV_GET_MODE_MIPI_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_MODESEL:
		sen_get_modesel_imx678((CTL_SENDRV_GET_MODESEL_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_GET_MODE_ROWTIME:
		sen_get_rowtime_imx678(id, (CTL_SENDRV_GET_MODE_ROWTIME_PARAM *)(data));
		break;
	case CTL_SENDRV_CFGID_FLIP_TYPE:
		rt = sen_get_flip_imx678(id, (CTL_SEN_FLIP *)(data));
		break;
	case CTL_SENDRV_CFGID_USER_DEFINE2:
		sen_get_min_expt_imx678(id, data);
		break;
	default:
		rt = E_NOSPT;
	}
	return rt;
}

static UINT32 sen_calc_chgmode_vd_imx678(CTL_SEN_ID id, UINT32 fps)
{
	UINT32 sensor_vd;

	if (1 > fps) {
		DBG_ERR("sensor fps can not small than (%d),change to dft sensor fps (%d) \r\n", fps, mode_basic_param[cur_sen_mode[id]].dft_fps);	
		fps = mode_basic_param[cur_sen_mode[id]].dft_fps;
	}
	sensor_vd = (mode_basic_param[cur_sen_mode[id]].signal_info.vd_period) * (mode_basic_param[cur_sen_mode[id]].dft_fps) / fps;

	sen_set_chgmode_fps_imx678(id, fps);
	sen_set_cur_fps_imx678(id, fps);

	if (sensor_vd > MAX_VD_PERIOD) {
		DBG_ERR("sensor vd out of sensor driver range (%d) \r\n", sensor_vd);
		sensor_vd = MAX_VD_PERIOD;
		fps = (mode_basic_param[cur_sen_mode[id]].signal_info.vd_period) * (mode_basic_param[cur_sen_mode[id]].dft_fps) / sensor_vd;
		sen_set_chgmode_fps_imx678(id, fps);
		sen_set_cur_fps_imx678(id, fps);
	}

	if(sensor_vd < (mode_basic_param[cur_sen_mode[id]].signal_info.vd_period)) {
		DBG_ERR("sensor vd out of sensor driver range (%d) \r\n", sensor_vd);
		sensor_vd = mode_basic_param[cur_sen_mode[id]].signal_info.vd_period;
		sen_set_chgmode_fps_imx678(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
		sen_set_cur_fps_imx678(id, mode_basic_param[cur_sen_mode[id]].dft_fps);
	}

	return sensor_vd;
}

static UINT32 sen_calc_exp_vd_imx678(CTL_SEN_ID id, UINT32 fps)
{
	UINT32 sensor_vd;

	if (1 > fps) {
		DBG_ERR("sensor fps can not small than (%d),change to dft sensor fps (%d) \r\n", fps, mode_basic_param[cur_sen_mode[id]].dft_fps);	
		fps = mode_basic_param[cur_sen_mode[id]].dft_fps;
	}
	sensor_vd = (mode_basic_param[cur_sen_mode[id]].signal_info.vd_period) * (mode_basic_param[cur_sen_mode[id]].dft_fps) / fps;

	if (sensor_vd > MAX_VD_PERIOD) {
		DBG_ERR("sensor vd out of sensor driver range (%d) \r\n", sensor_vd);
		sensor_vd = MAX_VD_PERIOD;
	
	}

	if(sensor_vd < (mode_basic_param[cur_sen_mode[id]].signal_info.vd_period)) {
		DBG_ERR("sensor vd out of sensor driver range (%d) \r\n", sensor_vd);
		sensor_vd = mode_basic_param[cur_sen_mode[id]].signal_info.vd_period;
	}

	return sensor_vd;
}

static void sen_set_gain_imx678(CTL_SEN_ID id, void *param)
{
	ISP_SENSOR_CTRL *sensor_ctrl = (ISP_SENSOR_CTRL *)param;
	UINT32 data1[ISP_SEN_MFRAME_MAX_NUM] = {0};
	UINT32 data2[ISP_SEN_MFRAME_MAX_NUM] = {0};	
	UINT32 frame_cnt, total_frame;
	static UINT32 pre_gain[CTL_SEN_ID_MAX] = {0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF};	
	UINT32 data = 0, temp_gain_ratio = 0;
	CTL_SEN_CMD cmd;
	ER rt = E_OK;

	sensor_ctrl_last[id].gain_ratio[0] = sensor_ctrl->gain_ratio[0];
	sensor_ctrl_last[id].gain_ratio[1] = sensor_ctrl->gain_ratio[1];

	// Calculate sensor gain
	if (mode_basic_param[cur_sen_mode[id]].frame_num == 0) {
		DBG_WRN("total_frame = 0, force to 1 \r\n");
		total_frame = 1;
	} else {
		total_frame = mode_basic_param[cur_sen_mode[id]].frame_num;
	}

	for (frame_cnt = 0; frame_cnt < total_frame; frame_cnt++) {
		if (100 <= (compensation_ratio[id][frame_cnt])) {
			sensor_ctrl->gain_ratio[frame_cnt] = (sensor_ctrl->gain_ratio[frame_cnt]) * (compensation_ratio[id][frame_cnt]) / 100;
		}
		if (sensor_ctrl->gain_ratio[frame_cnt] < (mode_basic_param[cur_sen_mode[id]].gain.min)) {
			sensor_ctrl->gain_ratio[frame_cnt] = (mode_basic_param[cur_sen_mode[id]].gain.min);
		} else if (sensor_ctrl->gain_ratio[frame_cnt] > (mode_basic_param[cur_sen_mode[id]].gain.max)) {
			sensor_ctrl->gain_ratio[frame_cnt] = (mode_basic_param[cur_sen_mode[id]].gain.max);			
		}
		
		temp_gain_ratio = sensor_ctrl->gain_ratio[frame_cnt];

		if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
			if (HCG_ENABLE == 0) {
				data2[frame_cnt] = 0;
			} else if (HCG_ENABLE == 1 || ((sensor_ctrl->gain_ratio[0] >= 10000) && (sensor_ctrl->gain_ratio[1] >= 10000))) { // 0 = long exp frame
				temp_gain_ratio = sensor_ctrl->gain_ratio[frame_cnt] * 10 / HCG_RATIO;
				data2[frame_cnt] = 1;
			} else {
				data2[frame_cnt] = 0;
			}
		} else if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_LINEAR) {
			if (HCG_ENABLE == 0) {
				data2[frame_cnt] = 0;
			} else if (HCG_ENABLE == 1 || (sensor_ctrl->gain_ratio[0] >= 10000)) { // 0 = long exp frame
				temp_gain_ratio = sensor_ctrl->gain_ratio[frame_cnt] * 10 / HCG_RATIO;
				data2[frame_cnt] = 1;
			} else {
				data2[frame_cnt] = 0;
			}
		}

		data1[frame_cnt] = (6 * sen_common_calc_log_2(temp_gain_ratio, 1000)) / 30;
		if (data1[frame_cnt] > (0xF0)) {
			DBG_ERR("gain overflow gain_ratio = %d data[0] = 0x%.8x \r\n", temp_gain_ratio, data1[frame_cnt]);
			data1[frame_cnt] = 0xF0;
		}
	}

	if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_LINEAR) {
		cmd = sen_set_cmd_info_imx678(0x3071, 1, (data1[0] >> 8) & 0x07, 0x0);
		rt |= sen_write_reg_imx678(id, &cmd);
		cmd = sen_set_cmd_info_imx678(0x3070, 1, data1[0] & 0xFF, 0x0);
		rt |= sen_write_reg_imx678(id, &cmd);

		if (pre_gain[id] != data2[0]) {
			cmd = sen_set_cmd_info_imx678(0x3030, 1, 0x0, 0x0);
			rt |= sen_read_reg_imx678(id, &cmd);
			data = cmd.data[0];

			if (data2[0] == 1) {
				data = data | 0x01;
			} else {
				data = data & (~0x01);
			}						
			cmd = sen_set_cmd_info_imx678(0x3030, 1, data, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
			pre_gain[id] = data;
		}				
	} else if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
		if(!each_frame_gain_flag[id]) {
			cmd = sen_set_cmd_info_imx678(0x3400, 1, 0x0, 0x0);
			rt |= sen_read_reg_imx678(id, &cmd);
			data = cmd.data[0];
			data = data & (~0x01);
			cmd = sen_set_cmd_info_imx678(0x3400, 1, data, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
			each_frame_gain_flag[id] = 1;
		}   
		cmd = sen_set_cmd_info_imx678(0x3071, 1, (data1[0] >> 8) & 0x07, 0x0);
		rt |= sen_write_reg_imx678(id, &cmd);
		cmd = sen_set_cmd_info_imx678(0x3070, 1, data1[0] & 0xFF, 0x0);
		rt |= sen_write_reg_imx678(id, &cmd);
		cmd = sen_set_cmd_info_imx678(0x3073, 1, (data1[1] >> 8) & 0x07, 0x0);
		rt |= sen_write_reg_imx678(id, &cmd);
		cmd = sen_set_cmd_info_imx678(0x3072, 1, data1[1] & 0xFF, 0x0);
		rt |= sen_write_reg_imx678(id, &cmd);		
		if (pre_gain[id] != data2[0]) {
			cmd = sen_set_cmd_info_imx678(0x3030, 1, 0x0, 0x0);
			rt |= sen_read_reg_imx678(id, &cmd);
			data = cmd.data[0];
			if (data2[0] == 1) {
				data = data | 0x01;
			} else {
				data = data & (~0x01);
			}
			cmd = sen_set_cmd_info_imx678(0x3030, 1, data, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
			
			cmd = sen_set_cmd_info_imx678(0x3031, 1, 0x0, 0x0);
			rt |= sen_read_reg_imx678(id, &cmd);
			data = cmd.data[0];
			if (data2[0] == 1) {
				data = data | 0x01;
			} else {
				data = data & (~0x01);
			}
			cmd = sen_set_cmd_info_imx678(0x3031, 1, data, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
			
			pre_gain[id] = data;
		}
	}

	if (rt != E_OK) {
		DBG_ERR("write register error %d \r\n", (INT)rt);
	}
}

static ER sen_get_min_shr_imx678(CTL_SEN_MODE mode, UINT32 *min_shr, UINT32 *min_exp)
{
	ER rt = E_OK;

	switch (mode) {
	case CTL_SEN_MODE_1:
		*min_shr = 3; 
		*min_exp = 1;
		break;
	case CTL_SEN_MODE_2:
		*min_shr = 5;//dont't care 
		*min_exp = 2;
		break;		
	case CTL_SEN_MODE_3:
		*min_shr = 5;//dont't care 
		*min_exp = 4;
		break;
	default:
		DBG_ERR("sensor mode %d no cmd table\r\n", mode);
		break;
	}
	return rt;	
}

static void sen_set_expt_imx678(CTL_SEN_ID id, void *param)
{
	ISP_SENSOR_CTRL *sensor_ctrl = (ISP_SENSOR_CTRL *)param;
	UINT32 line[ISP_SEN_MFRAME_MAX_NUM];
	UINT32 frame_cnt, total_frame;
	CTL_SEN_CMD cmd;
	UINT32 expt_time = 0, sensor_vd = 0, chgmode_fps = 0, cur_fps = 0, clac_fps = 0, t_row = 0;
	UINT32 min_shr = 0, min_exp_line = 0;
	UINT32 real_short_exp = 0, real_long_exp = 0, shr0 = 0, rhs1 = 0, shr1 = 0, fsc = 0, temp_max_short_line = 0;
	UINT32 temp_line[ISP_SEN_MFRAME_MAX_NUM] = {0};		
	ER rt = E_OK;

	sensor_ctrl_last[id].exp_time[0] = sensor_ctrl->exp_time[0];
	sensor_ctrl_last[id].exp_time[1] = sensor_ctrl->exp_time[1];

	if (mode_basic_param[cur_sen_mode[id]].frame_num == 0) {
		DBG_WRN("total_frame = 0, force to 1 \r\n");
		total_frame = 1;
	} else {
		total_frame = mode_basic_param[cur_sen_mode[id]].frame_num;
	}

	// Calculate exposure line
	for (frame_cnt = 0; frame_cnt < total_frame; frame_cnt++) {
		// Calculates the exposure setting
		t_row = sen_calc_rowtime_imx678(id, cur_sen_mode[id]);
		if (0 == t_row) {
			DBG_WRN("t_row  = 0, must >= 1 \r\n");
			t_row = 1;
		}
		line[frame_cnt] = (sensor_ctrl->exp_time[frame_cnt]) * 10 / t_row;

		sen_get_min_shr_imx678(cur_sen_mode[id], &min_shr, &min_exp_line);

		// Limit minimun exposure line
		if (line[frame_cnt] < min_exp_line) {
			line[frame_cnt] = min_exp_line;
		}
	}

	// Write exposure line
	// Get fps
	chgmode_fps = sen_get_chgmode_fps_imx678(id);

	// Calculate exposure time
	t_row = sen_calc_rowtime_imx678(id, cur_sen_mode[id]);
	if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
		expt_time = (line[0] + line[1]) * t_row / 10;
		temp_line[0] = line[0];
		temp_line[1] = line[1];
	} else {
		expt_time = (line[0]) * t_row / 10;
		temp_line[0] = line[0];
	}

	// Calculate fps
	if (0 == expt_time) {
		DBG_WRN("expt_time  = 0, must >= 1 \r\n");		
		expt_time = 1;
	}
	clac_fps = 100000000 / expt_time;

	cur_fps = (clac_fps < chgmode_fps) ? clac_fps : chgmode_fps;
	sen_set_cur_fps_imx678(id, cur_fps);

	// Calculate new vd
	sensor_vd = sen_calc_exp_vd_imx678(id, cur_fps);

	//Check max vts
	if (sensor_vd > MAX_VD_PERIOD) {
		DBG_ERR("max vts overflow\r\n");
		sensor_vd = MAX_VD_PERIOD;
	}

	cmd = sen_set_cmd_info_imx678(0x302A, 1, (sensor_vd >> 16 ) & 0x0F, 0);
	rt |= sen_write_reg_imx678(id, &cmd);	
	cmd = sen_set_cmd_info_imx678(0x3029, 1, (sensor_vd >> 8) & 0xFF, 0);
	rt |= sen_write_reg_imx678(id, &cmd);
	cmd = sen_set_cmd_info_imx678(0x3028, 1, sensor_vd & 0xFF, 0);
	rt |= sen_write_reg_imx678(id, &cmd);

	if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_LINEAR) {
		//Check max exp line reg
		if (line[0] > MAX_EXPOSURE_LINE) {
			DBG_ERR("max line overflow \r\n");
			line[0] = MAX_EXPOSURE_LINE;
		}

		//calculate shr
		if (line[0] > (sensor_vd - min_shr)) {
			shr0 = min_shr;
		} else {
			shr0 = sensor_vd - line[0];
		}
		compensation_ratio[id][0] = 100 * temp_line[0] / (sensor_vd - shr0);	

		// set exposure line to sensor
		cmd = sen_set_cmd_info_imx678(0x3052, 1, (shr0 >> 16) & 0x0F , 0);
		rt |= sen_write_reg_imx678(id, &cmd);			
		cmd = sen_set_cmd_info_imx678(0x3051, 1, (shr0 >> 8) & 0xFF, 0);
		rt |= sen_write_reg_imx678(id, &cmd);
		cmd = sen_set_cmd_info_imx678(0x3050, 1, shr0 & 0xFF , 0);
		rt |= sen_write_reg_imx678(id, &cmd);
	} else if (mode_basic_param[cur_sen_mode[id]].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
		if ((mode_basic_param[cur_sen_mode[id]].frame_num) == 2) {
			if (!BEYOND_FSC) {
				temp_max_short_line = (100000000 / cur_fps ) * 10 / 17 / t_row; //max support 1:16

				fsc = sensor_vd * 2;

				real_long_exp = line[0]; 
				real_short_exp = line[1];
				if (mode_basic_param[cur_sen_mode[id]].valid_size.w > 1920) {
					rhs1 = (BRL_ALL_PIXEL * 2) - 1;
				} else { // binning mode
					rhs1 = (BRL_1080P_PIXEL * 2) - 1;					
				}

				if (rhs1 > (5 + temp_max_short_line)) {
					rhs1 = 5 + temp_max_short_line;//min shr1 = 5
				}

				if (mode_basic_param[cur_sen_mode[id]].valid_size.w > 1920) {
					if (rhs1 > (fsc - (BRL_ALL_PIXEL * 2) -1)) {
						rhs1 = fsc - (BRL_ALL_PIXEL * 2) - 1; 
					}

					if (rhs1 < (5 + 2)) {//min rhs1 = shr1+2 and should be 2n+1,so min rhs1 = 7
						rhs1 = 5 + 2;//min shr1 = 5, min rhs1 = shr1 + 2;
					}
      		
					if (0 != (( rhs1 - 1) % 2)) {
						rhs1 = (rhs1 - 1) / 2 * 2 + 1;
					}					
				} else { // binning mode
					if (rhs1 > (fsc - (BRL_1080P_PIXEL * 2) -1)) {
						rhs1 = fsc - (BRL_1080P_PIXEL * 2) - 1; 
					}
					
					if (rhs1 < (5 + 4)) {//min rhs1 = shr1+2 and should be 4n+1,so min rhs1 = 9
						rhs1 = 5 + 4;//min shr1 = 5, min rhs1 = shr1 + 2;
					}
      		
					if (0 != (( rhs1 - 1) % 4)) {
						rhs1 = (rhs1 - 1) / 4 * 4 + 1;
					}
				}
      		
				if (real_short_exp > rhs1) {
					shr1 = 5;
				} else {
					shr1 = rhs1 - real_short_exp;
				}
      	
 				if (shr1 < 5) { //shr1 min
				    shr1 = 5;
				}
      		
				if (shr1 >= (rhs1 - 2)) { //shr1 max
					shr1 = rhs1 - 2;
				}
				
				if (0 != ((shr1 - 1) % 2)) {
					shr1 = (shr1 - 1) / 2 * 2 + 1;
				}
			} else {						
				fsc = sensor_vd * 2;
				real_long_exp = line[0]; 
				real_short_exp = line[1];
      	
 				if (shr1 < 5) { //shr1 min
				    shr1 = 5;
				}
      	
				rhs1 = shr1 + real_short_exp;

				if (mode_basic_param[cur_sen_mode[id]].valid_size.w > 1920) {         	
					if (0 != ((rhs1 - 1) % 2)) {
						rhs1 = (rhs1 - 1) / 2 * 2 + 1;  
					}
					
					if (rhs1 < (shr1 + 2)) {
						rhs1 = shr1 + 2;  
					}
					
					if (rhs1 > ((BRL_ALL_PIXEL * 2) -1)) {
						rhs1 = (BRL_ALL_PIXEL * 2) - 1;
					}					
				} else { // binning mode 
					if (0 != ((rhs1 - 1) % 4)) {
						rhs1 = (rhs1 - 1) / 4 * 4 + 1;  
					}
					
					if (rhs1 < (shr1 + 4)) {
						rhs1 = shr1 + 4;  
					}

					if (rhs1 > ((BRL_1080P_PIXEL * 2) -1)) {
						rhs1 = (BRL_1080P_PIXEL * 2) - 1;
					}										
				}
			}

			if (real_long_exp > (fsc - (rhs1 + 5))) {
				shr0 = rhs1 + 5;
			} else {
				shr0 = fsc - real_long_exp;
			}

			if (shr0 < (rhs1 + 5)) {
				shr0 = rhs1 + 5;
			}
      
			if (0 != (shr0 % 2)) {
				shr0 = shr0 / 2 * 2 + 2;
			}
      	
			if (shr0 > (fsc - 2)) {
				shr0 = fsc - 2;
			}

			compensation_ratio[id][0] = 100 * temp_line[0] / (fsc - shr0);
			compensation_ratio[id][1] = 100 * temp_line[1] / (rhs1 - shr1);

			cmd = sen_set_cmd_info_imx678(0x3052, 1, (shr0 >> 16) & 0x0F, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
			cmd = sen_set_cmd_info_imx678(0x3051, 1, (shr0 >> 8) & 0xFF, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
			cmd = sen_set_cmd_info_imx678(0x3050, 1, shr0 & 0xFF, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
			cmd = sen_set_cmd_info_imx678(0x3062, 1, (rhs1 >> 16)& 0x0F, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);			
			cmd = sen_set_cmd_info_imx678(0x3061, 1, (rhs1 >> 8)& 0xFF, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);			
			cmd = sen_set_cmd_info_imx678(0x3060, 1, rhs1 & 0xFF, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
			cmd = sen_set_cmd_info_imx678(0x3056, 1, (shr1 >> 16)& 0x0F, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
			cmd = sen_set_cmd_info_imx678(0x3055, 1, (shr1 >> 8)& 0xFF, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
			cmd = sen_set_cmd_info_imx678(0x3054, 1, shr1 & 0xFF, 0x0);
			rt |= sen_write_reg_imx678(id, &cmd);
		}
	}

	if (rt != E_OK) {
		DBG_ERR("write register error %d \r\n", (INT)rt);
	}
}

static void sen_set_preset_imx678(CTL_SEN_ID id, ISP_SENSOR_PRESET_CTRL *ctrl)
{
	memcpy(&preset_ctrl[id], ctrl, sizeof(ISP_SENSOR_PRESET_CTRL));
}

static void sen_set_flip_imx678(CTL_SEN_ID id, CTL_SEN_FLIP *flip)
{
	CTL_SEN_CMD cmd;
	UINT32 dir_reg = 0;
	ER rt = E_OK;

	cmd = sen_set_cmd_info_imx678(0x3020, 1, 0x0, 0x0);
	rt |= sen_read_reg_imx678(id, &cmd);
	
	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_MIRROR) {
		if (*flip & CTL_SEN_FLIP_H) {
			cmd.data[0] |= 0x1;
		} else {
			cmd.data[0] &= (~0x01);
		}
		dir_reg = cmd.data[0];
		cmd = sen_set_cmd_info_imx678(0x3020, 1, dir_reg, 0x0);
		rt |= sen_write_reg_imx678(id, &cmd);		
	} else {
		DBG_WRN("no support mirror \r\n");
	}

	cmd = sen_set_cmd_info_imx678(0x3021, 1, 0x0, 0x0);
	rt |= sen_read_reg_imx678(id, &cmd);
	
	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_FLIP) {
		if (*flip & CTL_SEN_FLIP_V) {
			cmd.data[0] |= 0x1;
			dir_reg = cmd.data[0];	
		} else {
			cmd.data[0] &= (~0x01);
			dir_reg = cmd.data[0];	
		}
		cmd = sen_set_cmd_info_imx678(0x3021, 1, dir_reg, 0x0);	
		rt |= sen_write_reg_imx678(id, &cmd);	
	} else {
		DBG_WRN("no support flip \r\n");
	}


	if (rt != E_OK) {
		DBG_ERR("write register error %d \r\n", (INT)rt);
	}
}

static ER sen_get_flip_imx678(CTL_SEN_ID id, CTL_SEN_FLIP *flip)
{
	CTL_SEN_CMD cmd;
	ER rt = E_OK;

	cmd = sen_set_cmd_info_imx678(0x3020, 1, 0x0, 0x0);
	rt |= sen_read_reg_imx678(id, &cmd);

	*flip = CTL_SEN_FLIP_NONE;
	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_MIRROR) {
		if (cmd.data[0] & 0x1) {
			*flip |= CTL_SEN_FLIP_H;
		}
	} else {
		DBG_WRN("no support mirror \r\n");
	}

	cmd = sen_set_cmd_info_imx678(0x3021, 1, 0x0, 0x0);
	rt |= sen_read_reg_imx678(id, &cmd);

	if (basic_param.property & CTL_SEN_SUPPORT_PROPERTY_FLIP) {
		if (cmd.data[0] & 0x01) {
			*flip |= CTL_SEN_FLIP_V;
		}
	} else {
		DBG_WRN("no support flip \r\n");
	}

	return rt;
}

#if defined(__FREERTOS)
void sen_get_gain_imx678(CTL_SEN_ID id, void *param)
#else
static void sen_get_gain_imx678(CTL_SEN_ID id, void *param)
#endif
{
	ISP_SENSOR_CTRL *sensor_ctrl = (ISP_SENSOR_CTRL *)param;

	sensor_ctrl->gain_ratio[0] = sensor_ctrl_last[id].gain_ratio[0];
	sensor_ctrl->gain_ratio[1] = sensor_ctrl_last[id].gain_ratio[1];
}

#if defined(__FREERTOS)
void sen_get_expt_imx678(CTL_SEN_ID id, void *param)
#else
static void sen_get_expt_imx678(CTL_SEN_ID id, void *param)
#endif
{
	ISP_SENSOR_CTRL *sensor_ctrl = (ISP_SENSOR_CTRL *)param;

	sensor_ctrl->exp_time[0] = sensor_ctrl_last[id].exp_time[0];
	sensor_ctrl->exp_time[1] = sensor_ctrl_last[id].exp_time[1];
}

static void sen_get_min_expt_imx678(CTL_SEN_ID id, void *param)
{
	UINT32 *min_exp_time = (UINT32 *)param;
	UINT32 t_row = 0, min_shr = 0, min_exp_line = 0;

	sen_get_min_shr_imx678(cur_sen_mode[id], &min_shr, &min_exp_line);

	t_row = sen_calc_rowtime_imx678(id, cur_sen_mode[id]);
	*min_exp_time = t_row * min_exp_line / 10 + 1;
}

static void sen_get_mode_basic_imx678(CTL_SENDRV_GET_MODE_BASIC_PARAM *mode_basic)
{
	UINT32 mode = mode_basic->mode;
	
	if (mode >= SEN_MAX_MODE) {
		mode = 0;
	}
	memcpy(mode_basic, &mode_basic_param[mode], sizeof(CTL_SENDRV_GET_MODE_BASIC_PARAM));
}

static void sen_get_attr_basic_imx678(CTL_SEN_ID id, CTL_SENDRV_GET_ATTR_BASIC_PARAM *data)
{
	memcpy(data, &basic_param, sizeof(CTL_SENDRV_GET_ATTR_BASIC_PARAM));
}

static void sen_get_attr_signal_imx678(CTL_SENDRV_GET_ATTR_SIGNAL_PARAM *data)
{
	memcpy(data, &signal_param, sizeof(CTL_SENDRV_GET_ATTR_SIGNAL_PARAM));
}

static ER sen_get_attr_cmdif_imx678(CTL_SEN_ID id, CTL_SENDRV_GET_ATTR_CMDIF_PARAM *data)
{
	data->type = CTL_SEN_CMDIF_TYPE_I2C;
	memcpy(&data->info, &i2c, sizeof(CTL_SENDRV_I2C));
	data->info.i2c.ch = sen_i2c[id].id;
	data->info.i2c.w_addr_info[0].w_addr = sen_i2c[id].addr;
	data->info.i2c.cur_w_addr_info.w_addr_sel = data->info.i2c.w_addr_info[0].w_addr_sel;
	data->info.i2c.cur_w_addr_info.w_addr = data->info.i2c.w_addr_info[0].w_addr;
	return E_OK;
}

static ER sen_get_attr_if_imx678(CTL_SENDRV_GET_ATTR_IF_PARAM *data)
{
	#if 1
	if (data->type == CTL_SEN_IF_TYPE_MIPI) {
		return E_OK;
	}
	return E_NOSPT;
	#else
	if (data->type == CTL_SEN_IF_TYPE_MIPI) {
		memcpy(&data->info.mipi, &mipi, sizeof(CTL_SENDRV_MIPI));
		return E_OK;
	}
	return E_NOSPT;
	#endif
}

static void sen_get_fps_imx678(CTL_SEN_ID id, CTL_SENDRV_GET_FPS_PARAM *data)
{
	data->cur_fps = sen_get_cur_fps_imx678(id);
	data->chg_fps = sen_get_chgmode_fps_imx678(id);
}

static void sen_get_speed_imx678(CTL_SEN_ID id, CTL_SENDRV_GET_SPEED_PARAM *data)
{
	UINT32 mode = data->mode;
	
	if (mode >= SEN_MAX_MODE) {
		mode = 0;
	}
	memcpy(data, &speed_param[mode], sizeof(CTL_SENDRV_GET_SPEED_PARAM));

	if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK ) {
		data->mclk_src = CTL_SEN_SIEMCLK_SRC_MCLK;
	} else if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK2) {
		data->mclk_src = CTL_SEN_SIEMCLK_SRC_MCLK2;
	} else if (sen_power[id].mclk == CTL_SEN_CLK_SEL_SIEMCLK3) {
		data->mclk_src = CTL_SEN_SIEMCLK_SRC_MCLK3;		
	} else if (sen_power[id].mclk == CTL_SEN_IGNORE) {
		data->mclk_src = CTL_SEN_SIEMCLK_SRC_IGNORE;
	} else {
		DBG_ERR("mclk source is fail \r\n");
	}
}

static void sen_get_mode_mipi_imx678(CTL_SENDRV_GET_MODE_MIPI_PARAM *data)
{
	UINT32 mode = data->mode;
	
	if (mode >= SEN_MAX_MODE) {
		mode = 0;
	}
	memcpy(data, &mipi_param[mode], sizeof(CTL_SENDRV_GET_MODE_MIPI_PARAM));
}

static void sen_get_modesel_imx678(CTL_SENDRV_GET_MODESEL_PARAM *data)
{
	if (data->if_type != CTL_SEN_IF_TYPE_MIPI) {
		DBG_ERR("if_type %d N.S. \r\n", data->if_type);
		return;
	}

	if (data->data_fmt != CTL_SEN_DATA_FMT_RGB) {
		DBG_ERR("data_fmt %d N.S. \r\n", data->data_fmt);
		return;
	}
	
	if (data->frame_num == 1) {
		if ((data->size.w <= 3840) && (data->size.h <= 2160)) {
			if (data->frame_rate <= 3000) {
				data->mode = CTL_SEN_MODE_1;
				return;
			}
		}
	} else if (data->frame_num == 2) {
		if ((data->size.w <= 3840) && (data->size.h <= 2160)) {
			if ((data->size.w <= 1920) && (data->size.h <= 1080)) {	
				if (data->frame_rate <= 3000) {
					data->mode = CTL_SEN_MODE_3;
					return;
				}
			} else {
				if (data->frame_rate <= 3000) {
					data->mode = CTL_SEN_MODE_2;
					return;
				}
			}
		}
	} 					

	DBG_ERR("fail (frame_rate%d,size%d*%d,if_type%d,data_fmt%d,frame_num%d) \r\n"
			, data->frame_rate, data->size.w, data->size.h, data->if_type, data->data_fmt, data->frame_num);
	data->mode = CTL_SEN_MODE_1;
}

static UINT32 sen_calc_rowtime_step_imx678(CTL_SEN_ID id, CTL_SEN_MODE mode)
{
	UINT32 div_step = 1;

	if (mode >= SEN_MAX_MODE) {
		mode = cur_sen_mode[id];
	}

	if (mode_basic_param[mode].mode_type == CTL_SEN_MODE_STAGGER_HDR) {
		div_step = 2;
	} else {
		div_step = 1;
	}

	return div_step;
}

static UINT32 sen_calc_rowtime_imx678(CTL_SEN_ID id, CTL_SEN_MODE mode)
{
	UINT32 row_time = 0;

	if (mode >= SEN_MAX_MODE) {
		mode = cur_sen_mode[id];
	}

	//Precision * 10
	row_time = 1000 * (mode_basic_param[mode].signal_info.hd_period) / ((speed_param[mode].pclk) / 10000);

	return row_time;
}

static void sen_get_rowtime_imx678(CTL_SEN_ID id, CTL_SENDRV_GET_MODE_ROWTIME_PARAM *data)
{
	data->row_time_step = sen_calc_rowtime_step_imx678(id, data->mode);	
	data->row_time = sen_calc_rowtime_imx678(id, data->mode) * (data->row_time_step);
}

static void sen_set_cur_fps_imx678(CTL_SEN_ID id, UINT32 fps)
{
	cur_fps[id] = fps;
}

static UINT32 sen_get_cur_fps_imx678(CTL_SEN_ID id)
{
	return cur_fps[id];
}

static void sen_set_chgmode_fps_imx678(CTL_SEN_ID id, UINT32 fps)
{
	chgmode_fps[id] = fps;
}

static UINT32 sen_get_chgmode_fps_imx678(CTL_SEN_ID id)
{
	return chgmode_fps[id];
}

#if defined(__FREERTOS)
void sen_get_i2c_id_imx678(CTL_SEN_ID id, UINT32 *i2c_id)
{
	*i2c_id = sen_i2c[id].id;
}

void sen_get_i2c_addr_imx678(CTL_SEN_ID id, UINT32 *i2c_addr)
{
	*i2c_addr = sen_i2c[id].addr;
}
int sen_init_imx678(SENSOR_DTSI_INFO *info)
{
	CTL_SEN_REG_OBJ reg_obj;
	CHAR node_path[64];
	CHAR compatible[64];
	UINT32 id;
	ER rt = E_OK;

	for (id = 0; id < CTL_SEN_ID_MAX; id++ ) {
		is_fastboot[id] = 0;
		fastboot_i2c_id[id] = 0xFFFFFFFF;
		fastboot_i2c_addr[id] = 0x0;
	}

	sprintf(compatible, "nvt,sen_imx678");
	if (sen_common_check_compatible(compatible)) {
		DBG_DUMP("compatible valid, using peri-dev.dtsi \r\n");
		sen_common_load_cfg_preset_compatible(compatible, &sen_preset);
		sen_common_load_cfg_direction_compatible(compatible, &sen_direction);
		sen_common_load_cfg_power_compatible(compatible, &sen_power);
		sen_common_load_cfg_i2c_compatible(compatible, &sen_i2c);
	} else if (info->addr != NULL) {
		DBG_DUMP("compatible not valid, using sensor.dtsi \r\n");
		sprintf(node_path, "/sensor/sen_cfg/sen_imx678");
		sen_common_load_cfg_map(info->addr, node_path, &sen_map);
		sen_common_load_cfg_preset(info->addr, node_path, &sen_preset);
		sen_common_load_cfg_direction(info->addr, node_path, &sen_direction);
		sen_common_load_cfg_power(info->addr, node_path, &sen_power);
		sen_common_load_cfg_i2c(info->addr, node_path, &sen_i2c);
	} else {
		DBG_WRN("DTSI addr is NULL \r\n");
	}

	memset((void *)(&reg_obj), 0, sizeof(CTL_SEN_REG_OBJ));
	reg_obj.pwr_ctrl = sen_pwr_ctrl_imx678;
	reg_obj.det_plug_in = NULL;
	reg_obj.drv_tab = sen_get_drv_tab_imx678();
	rt = ctl_sen_reg_sendrv("nvt_sen_imx678", &reg_obj);
	if (rt != E_OK) {
		DBG_WRN("register sensor driver fail \r\n");
	}

	return rt;
}

void sen_exit_imx678(void)
{
	ctl_sen_unreg_sendrv("nvt_sen_imx678");
}

#else
static int __init sen_init_imx678(void)
{
	INT8 cfg_path[MAX_PATH_NAME_LENGTH+1] = { '\0' };
	CFG_FILE_FMT *pcfg_file;
	CTL_SEN_REG_OBJ reg_obj;
	UINT32 id;
	ER rt = E_OK;

	for (id = 0; id < ISP_BUILTIN_ID_MAX_NUM; id++ ) {
		is_fastboot[id] = kdrv_builtin_is_fastboot();
		fastboot_i2c_id[id] = isp_builtin_get_i2c_id(id);
		fastboot_i2c_addr[id] = isp_builtin_get_i2c_addr(id);
	}

	// Parsing cfc file if exist
	if ((strstr(sen_cfg_path, "null")) || (strstr(sen_cfg_path, "NULL"))) {
		DBG_WRN("cfg file no exist \r\n");
		cfg_path[0] = '\0';
	} else {
		if ((sen_cfg_path != NULL) && (strlen(sen_cfg_path) <= MAX_PATH_NAME_LENGTH)) {
			strncpy((char *)cfg_path, sen_cfg_path, MAX_PATH_NAME_LENGTH);
		}

		if ((pcfg_file = sen_common_open_cfg(cfg_path)) != NULL) {
			DBG_MSG("load %s success \r\n", sen_cfg_path);
			sen_common_load_cfg_map(pcfg_file, &sen_map);
			sen_common_load_cfg_preset(pcfg_file, &sen_preset);
			sen_common_load_cfg_direction(pcfg_file, &sen_direction);
			sen_common_load_cfg_power(pcfg_file, &sen_power);
			sen_common_load_cfg_i2c(pcfg_file, &sen_i2c);
			sen_common_close_cfg(pcfg_file);
		} else {
			DBG_WRN("load cfg fail \r\n");
		}
	}

	memset((void *)(&reg_obj), 0, sizeof(CTL_SEN_REG_OBJ));
	reg_obj.pwr_ctrl = sen_pwr_ctrl_imx678;
	reg_obj.det_plug_in = NULL;
	reg_obj.drv_tab = sen_get_drv_tab_imx678();
	rt = ctl_sen_reg_sendrv("nvt_sen_imx678", &reg_obj);
	if (rt != E_OK) {
		DBG_WRN("register sensor driver fail \r\n");
	}

	return rt;
}

static void __exit sen_exit_imx678(void)
{
	ctl_sen_unreg_sendrv("nvt_sen_imx678");
}

module_init(sen_init_imx678);
module_exit(sen_exit_imx678);

MODULE_AUTHOR("Novatek Corp.");
MODULE_DESCRIPTION(SEN_IMX678_MODULE_NAME);
MODULE_LICENSE("GPL");
#endif

