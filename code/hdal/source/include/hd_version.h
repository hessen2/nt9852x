/**
	@brief hdal body implementation version\n

	@file hd_version.h

	@ingroup mhdal

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/
#ifndef _HD_VERSION_H_
#define _HD_VERSION_H_

#define HDAL_IMPL_VERSION 0x010003 //implementation version

#endif

