/**
	@brief Sample code of video liveview with pip view (picture in picture).\n

	@file video_liveview_with_pip.c

	@author Janice Huang

	@ingroup mhdal

	@note This file is modified from video_liveview_with_2cap.c.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include "hdal.h"
#include "hd_debug.h"

#include "vendor_ai/vendor_ai.h"
#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include "net_flow_user_sample/net_layer_sample.h"
#include "vendor_common.h"

// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(hd_video_liveview_with_vcap_2dev, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

#define YUV_BLK_SIZE      (DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420))
#define DEBUG_MENU 		1

///////////////////////////////////////////////////////////////////////////////

//header
#define DBGINFO_BUFSIZE()	(0x200)

//RAW
#define VDO_RAW_BUFSIZE(w, h, pxlfmt)   (ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NRX: RAW compress: Only support 12bit mode
#define RAW_COMPRESS_RATIO ((7/12)*100)
#define VDO_NRX_BUFSIZE(w, h)           (ALIGN_CEIL_4(ALIGN_CEIL_64(w) * 12 / 8 * RAW_COMPRESS_RATIO / 100 * (h)))
//CA for AWB
#define VDO_CA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 3) << 1)
//LA for AE
#define VDO_LA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 1) << 1)

//YUV
#define VDO_YUV_BUFSIZE(w, h, pxlfmt)	(ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NVX: YUV compress
#define YUV_COMPRESS_RATIO 75
#define VDO_NVX_BUFSIZE(w, h, pxlfmt)	(VDO_YUV_BUFSIZE(w, h, pxlfmt) * YUV_COMPRESS_RATIO / 100)

///////////////////////////////////////////////////////////////////////////////

#define SEN_OUT_FMT		HD_VIDEO_PXLFMT_RAW12
#define CAP_OUT_FMT		HD_VIDEO_PXLFMT_RAW12
#define CA_WIN_NUM_W		32
#define CA_WIN_NUM_H		32
#define LA_WIN_NUM_W		32
#define LA_WIN_NUM_H		32
#define VA_WIN_NUM_W		16
#define VA_WIN_NUM_H		16
#define YOUT_WIN_NUM_W	128
#define YOUT_WIN_NUM_H	128
#define ETH_8BIT_SEL		0 //0: 2bit out, 1:8 bit out
#define ETH_OUT_SEL		1 //0: full, 1: subsample 1/2

#define VDO_SIZE_W     1920
#define VDO_SIZE_H     1080
#define VDOOUT0_TYPE   1 //1 for LCD, 2 for HDMI, 0 for TV
#define VDOOUT1_TYPE   2 //1 for LCD, 2 for HDMI, 0 for TV

#define VDOOUT_HDMI  HD_VIDEOOUT_HDMI_1920X1080I60
#define	VDO_FRAME_FORMAT	HD_VIDEO_PXLFMT_YUV420

// nn -----------------------------------------------------------
#define	NET_NUM_SENSOR1		2		// MUST BE 0, 1 or 2
#define	NET_NUM_SENSOR2		2		// MUST BE 0, 1 or 2
#define NN_USE_DSP			FALSE

#define	NET_NUM				(NET_NUM_SENSOR1+NET_NUM_SENSOR2)
#define MAX_FRAME_WIDTH		1280
#define MAX_FRAME_HEIGHT	720
#define MAX_OBJ_NUM			1024
#define YUV_OUT_BUF_SIZE	(3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT)

#define MBYTE				(1024 * 1024)
#define NN_TOTAL_MEM_SIZE	(200 * MBYTE)
#define VENDOR_AI_CFG  		0x000f0000  //ai project config


#if NET_NUM_SENSOR1 > 0
static CHAR model_name_sen1[NET_NUM_SENSOR1][256] = { "para/nvt_model.bin" , "para/nvt_model1.bin" };
static UINT32 mem_size_sen1[NET_NUM_SENSOR1]	  = { 30*MBYTE , 30*MBYTE };
#endif
#if NET_NUM_SENSOR2 > 0
static CHAR model_name_sen2[NET_NUM_SENSOR2][256] = { "para/nvt_model.bin" , "para/nvt_model1.bin" };
static UINT32 mem_size_sen2[NET_NUM_SENSOR2]	  = { 30*MBYTE , 30*MBYTE };
#endif
static CHAR g_label_name[] = "accuracy/labels.txt";


typedef struct _VIDEO_LIVEVIEW {
	// (1)
	HD_VIDEOCAP_SYSCAPS cap_syscaps;
	HD_PATH_ID cap_ctrl;
	HD_PATH_ID cap_path;

	HD_DIM  cap_dim;
	HD_DIM  proc_max_dim;

	// (2)
	HD_VIDEOPROC_SYSCAPS proc_syscaps;
	HD_PATH_ID proc_ctrl;
	HD_PATH_ID proc_path;

	HD_DIM  out_max_dim;
	HD_DIM  out_dim;

	// nn
	HD_PATH_ID proc_ctrl_alg;
	HD_PATH_ID proc_path_alg;
	HD_DIM	proc_dim_alg;
	HD_DIM	proc_max_dim_alg;

	// (3)
	HD_VIDEOOUT_SYSCAPS out_syscaps;
	HD_PATH_ID out_ctrl;
	HD_PATH_ID out_path;

	HD_DIM  out1_max_dim;
	HD_DIM  out1_dim;

	#if 0
	// (4)
	HD_VIDEOOUT_SYSCAPS out1_syscaps;
	HD_PATH_ID out1_ctrl;
	HD_PATH_ID out1_path;
	#endif

	// (5) user pull
	pthread_t  aquire_thread_id;
	UINT32     proc_exit;
	UINT32     flow_start;
	UINT32 	   copy_new_buf;
	INT32      wait_ms;
    HD_URECT   pip_rect;
} VIDEO_LIVEVIEW;


#if NET_NUM > 0
typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	VENDOR_AIS_IMG_PARM	src_img;
	VENDOR_AIS_FLOW_MEM_PARM mem;
	UINT32 run_id;
	VIDEO_LIVEVIEW *stream;
	UINT8 sensor_id;
	UINT8 net_id;
} VENDOR_AIS_NETWORK_PARM;


static VENDOR_AIS_FLOW_MEM_PARM g_mem = {0};
static HD_COMMON_MEM_VB_BLK g_blk_info[2];
static HD_VIDEO_FRAME yuv_out_buffer;
static VOID *yuv_out_buffer_va = NULL;
#if DYNAMIC_LABEL
static CHAR* g_class_labels[4] = {NULL};
#else
static CHAR g_class_labels[MAX_CLASS_NUM*VENDOR_AIS_LBL_LEN];
#endif
static BOOL show_net_results[4] = { 0, 0, 0, 0 };
#endif

//static struct timeval tstart[2], tend[2];


///////////////////////////////////////////////////////////////////////////////


static HD_RESULT mem_init(void)
{
	HD_RESULT              		ret;
	HD_COMMON_MEM_INIT_CONFIG 	mem_cfg   = {0};
	HD_COMMON_MEM_DDR_ID		ddr_id	  = DDR_ID0;

	#if NET_NUM > 0
	HD_COMMON_MEM_DDR_ID	  	ddr_id_nn = DDR_ID1;
	HD_COMMON_MEM_VB_BLK      	blk;
	UINT32                    	pa, va;
	#endif

	// config common pool (cap)
	mem_cfg.pool_info[0].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[0].blk_size = DBGINFO_BUFSIZE() + VDO_RAW_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, CAP_OUT_FMT)
        											  + VDO_CA_BUF_SIZE(CA_WIN_NUM_W, CA_WIN_NUM_H)
        											  + VDO_LA_BUF_SIZE(LA_WIN_NUM_W, LA_WIN_NUM_H);
	mem_cfg.pool_info[0].blk_cnt = 4;
	mem_cfg.pool_info[0].ddr_id = ddr_id;
	
	// config common pool (main)
	mem_cfg.pool_info[1].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[1].blk_size = YUV_BLK_SIZE;
	mem_cfg.pool_info[1].blk_cnt = 6+1; //for side by side copy
	mem_cfg.pool_info[1].ddr_id = ddr_id;

	// config common pool (alg)
	mem_cfg.pool_info[2].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[2].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, VDO_FRAME_FORMAT);
	mem_cfg.pool_info[2].blk_cnt = 3;
	mem_cfg.pool_info[2].ddr_id = ddr_id;

	#if NET_NUM > 0
	// nn mem pool ---
	mem_cfg.pool_info[3].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[3].blk_size = NN_TOTAL_MEM_SIZE;
	mem_cfg.pool_info[3].blk_cnt = 1;
	mem_cfg.pool_info[3].ddr_id = ddr_id_nn;
	mem_cfg.pool_info[4].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[4].blk_size = YUV_OUT_BUF_SIZE;
	mem_cfg.pool_info[4].blk_cnt = 1;
	mem_cfg.pool_info[4].ddr_id = ddr_id_nn;
	// - end nn mem pool
	#endif

	ret = hd_common_mem_init(&mem_cfg);

	#if NET_NUM > 0
	// nn get block ---
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, NN_TOTAL_MEM_SIZE, ddr_id_nn);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail, 1\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		printf("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, NN_TOTAL_MEM_SIZE);
	g_blk_info[0] = blk;

	// Release buffer 
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = NN_TOTAL_MEM_SIZE;

	// Allocate scale out buffer 
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, YUV_OUT_BUF_SIZE, ddr_id_nn);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail, 2\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	yuv_out_buffer.phy_addr[0] = hd_common_mem_blk2pa(blk);
	if (yuv_out_buffer.phy_addr[0] == 0) {
		printf("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			printf("yuv_out_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	yuv_out_buffer_va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
						  yuv_out_buffer.phy_addr[0],
						  YUV_OUT_BUF_SIZE);
	g_blk_info[1] = blk;
	printf("Allocate yuv_out_buffer pa(%#lx) va(%p)\n", yuv_out_buffer.phy_addr[0], yuv_out_buffer_va);

exit:
	#endif
	
	return ret;
}

static HD_RESULT mem_exit(void)
{
	#if NET_NUM > 0
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		printf("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release scale out buffer */
	if (yuv_out_buffer_va) {
		ret = hd_common_mem_munmap(yuv_out_buffer_va, YUV_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (yuv_out_buffer_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	ret = hd_common_mem_release_block(g_blk_info[1]);
	if (ret != HD_OK) {
		printf("mem_uninit : (yuv_out_buffer.phy_addr[0])hd_common_mem_release_block fail.\r\n");
		return ret;
	}
	#endif
	
	return hd_common_mem_uninit();
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT get_cap_caps(HD_PATH_ID video_cap_ctrl, HD_VIDEOCAP_SYSCAPS *p_video_cap_syscaps)
{
	HD_RESULT ret = HD_OK;
	hd_videocap_get(video_cap_ctrl, HD_VIDEOCAP_PARAM_SYSCAPS, p_video_cap_syscaps);
	return ret;
}

static HD_RESULT get_cap_sysinfo(HD_PATH_ID video_cap_ctrl)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOCAP_SYSINFO sys_info = {0};

	hd_videocap_get(video_cap_ctrl, HD_VIDEOCAP_PARAM_SYSINFO, &sys_info);
	printf("sys_info.devid =0x%X, cur_fps[0]=%d/%d, vd_count=%llu, output_started=%d, cur_dim(%dx%d)\r\n",
		sys_info.dev_id, GET_HI_UINT16(sys_info.cur_fps[0]), GET_LO_UINT16(sys_info.cur_fps[0]), sys_info.vd_count, sys_info.output_started, sys_info.cur_dim.w, sys_info.cur_dim.h);
	return ret;
}

static HD_RESULT set_cap_cfg(HD_PATH_ID *p_video_cap_ctrl)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOCAP_DRV_CONFIG cap_cfg = {0};
	HD_PATH_ID video_cap_ctrl = 0;
	HD_VIDEOCAP_CTRL iq_ctl = {0};
	snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, "nvt_sen_imx290");
	cap_cfg.sen_cfg.sen_dev.if_type = HD_COMMON_VIDEO_IN_MIPI_CSI;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.serial_if_pinmux = 0x301;//PIN_MIPI_LVDS_CFG_CLK0 | PIN_MIPI_LVDS_CFG_DAT0|PIN_MIPI_LVDS_CFG_DAT1
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = 0x10;//PIN_I2C_CFG_CH2
	cap_cfg.sen_cfg.sen_dev.pin_cfg.clk_lane_sel = HD_VIDEOCAP_SEN_CLANE_SEL_CSI0_USE_C0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[0] = 0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[1] = 1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[4] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[5] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[6] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[7] = HD_VIDEOCAP_SEN_IGNORE;

	ret = hd_videocap_open(0, HD_VIDEOCAP_0_CTRL, &video_cap_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_DRV_CONFIG, &cap_cfg);
	iq_ctl.func = HD_VIDEOCAP_FUNC_AE | HD_VIDEOCAP_FUNC_AWB;
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_CTRL, &iq_ctl);

	*p_video_cap_ctrl = video_cap_ctrl;
	return ret;
}
static HD_RESULT set_cap2_cfg(HD_PATH_ID *p_video_cap_ctrl)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOCAP_DRV_CONFIG cap_cfg = {0};
	HD_PATH_ID video_cap_ctrl = 0;
	HD_VIDEOCAP_CTRL iq_ctl = {0};
	snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, "nvt_sen_imx290");
	cap_cfg.sen_cfg.sen_dev.if_type = HD_COMMON_VIDEO_IN_MIPI_CSI;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =   0x20; //PIN_SENSOR2_CFG_MIPI
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.serial_if_pinmux = 0xC02;//PIN_MIPI_LVDS_CFG_CLK1 | PIN_MIPI_LVDS_CFG_DAT2 | PIN_MIPI_LVDS_CFG_DAT3
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = 0x40;//PIN_I2C_CFG_CH3
	cap_cfg.sen_cfg.sen_dev.pin_cfg.clk_lane_sel =  HD_VIDEOCAP_SEN_CLANE_SEL_CSI1_USE_C1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[0] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[1] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = 0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = 1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[4] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[5] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[6] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[7] = HD_VIDEOCAP_SEN_IGNORE;
	ret = hd_videocap_open(0, HD_VIDEOCAP_1_CTRL, &video_cap_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_DRV_CONFIG, &cap_cfg);
	iq_ctl.func = HD_VIDEOCAP_FUNC_AE | HD_VIDEOCAP_FUNC_AWB;
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_CTRL, &iq_ctl);

	*p_video_cap_ctrl = video_cap_ctrl;
	return ret;
}

static HD_RESULT set_cap_param(HD_PATH_ID video_cap_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	{//select sensor mode, manually or automatically
		HD_VIDEOCAP_IN video_in_param = {0};

		video_in_param.sen_mode = HD_VIDEOCAP_SEN_MODE_AUTO; //auto select sensor mode by the parameter of HD_VIDEOCAP_PARAM_OUT
		video_in_param.frc = HD_VIDEO_FRC_RATIO(25,1);
		video_in_param.dim.w = p_dim->w;
		video_in_param.dim.h = p_dim->h;
		video_in_param.pxlfmt = SEN_OUT_FMT;
		video_in_param.out_frame_num = HD_VIDEOCAP_SEN_FRAME_NUM_1;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN, &video_in_param);
		//printf("set_cap_param MODE=%d\r\n", ret);
		if (ret != HD_OK) {
			return ret;
		}
	}
	#if 1 //no crop, full frame
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_OFF;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN_CROP, &video_crop_param);
		//printf("set_cap_param CROP NONE=%d\r\n", ret);
	}
	#else //HD_CROP_ON
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_ON;
		video_crop_param.win.rect.x = 0;
		video_crop_param.win.rect.y = 0;
		video_crop_param.win.rect.w = 1920/2;
		video_crop_param.win.rect.h= 1080/2;
		video_crop_param.align.w = 4;
		video_crop_param.align.h = 4;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN_CROP, &video_crop_param);
		//printf("set_cap_param CROP ON=%d\r\n", ret);
	}
	#endif
	{
		HD_VIDEOCAP_OUT video_out_param = {0};

		//without setting dim for no scaling, using original sensor out size
		video_out_param.pxlfmt = CAP_OUT_FMT;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_OUT, &video_out_param);
		//printf("set_cap_param OUT=%d\r\n", ret);
	}

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_proc_cfg(HD_PATH_ID *p_video_proc_ctrl, HD_DIM* p_max_dim, HD_OUT_ID _out_id)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOPROC_DEV_CONFIG video_cfg_param = {0};
	HD_VIDEOPROC_CTRL video_ctrl_param = {0};
	HD_PATH_ID video_proc_ctrl = 0;

	ret = hd_videoproc_open(0, _out_id, &video_proc_ctrl); //open this for device control
	if (ret != HD_OK)
		return ret;

	if (p_max_dim != NULL ) {
		video_cfg_param.pipe = HD_VIDEOPROC_PIPE_RAWALL;
		if ((HD_CTRL_ID)_out_id == HD_VIDEOPROC_0_CTRL) {
			video_cfg_param.isp_id = 0;
		} else {
			video_cfg_param.isp_id = 1;
		}
		video_cfg_param.ctrl_max.func = 0;
		video_cfg_param.in_max.func = 0;
		video_cfg_param.in_max.dim.w = p_max_dim->w;
		video_cfg_param.in_max.dim.h = p_max_dim->h;
		video_cfg_param.in_max.pxlfmt = CAP_OUT_FMT;
		video_cfg_param.in_max.frc = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_DEV_CONFIG, &video_cfg_param);
		if (ret != HD_OK) {
			return HD_ERR_NG;
		}
	}

	video_ctrl_param.func = 0;
	ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_CTRL, &video_ctrl_param);

	*p_video_proc_ctrl = video_proc_ctrl;

	return ret;
}

static HD_RESULT set_proc_param(HD_PATH_ID video_proc_path, HD_DIM* p_dim)
{
	HD_RESULT ret = HD_OK;

	if (p_dim != NULL) { //if videoproc is already binding to dest module, not require to setting this!
		HD_VIDEOPROC_OUT video_out_param = {0};
		video_out_param.func = 0;
		video_out_param.dim.w = p_dim->w;
		video_out_param.dim.h = p_dim->h;
		video_out_param.pxlfmt = HD_VIDEO_PXLFMT_YUV420;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		video_out_param.frc = HD_VIDEO_FRC_RATIO(1,1);
		video_out_param.depth = 1;
		ret = hd_videoproc_set(video_proc_path, HD_VIDEOPROC_PARAM_OUT, &video_out_param);
	}

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_out_cfg(HD_CTRL_ID ctrl_id,HD_PATH_ID *p_video_out_ctrl, UINT32 out_type)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOOUT_MODE videoout_mode = {0};
	HD_PATH_ID video_out_ctrl = 0;

	ret = hd_videoout_open(0, ctrl_id, &video_out_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}

	switch(out_type){
	case 0:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_CVBS;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.cvbs= HD_VIDEOOUT_CVBS_NTSC;
	break;
	case 1:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_LCD;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.lcd = HD_VIDEOOUT_LCD_0;
	break;
	case 2:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_HDMI;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.hdmi= VDOOUT_HDMI;
	break;
	default:
		printf("not support out_type\r\n");
	break;
	}
	ret = hd_videoout_set(video_out_ctrl, HD_VIDEOOUT_PARAM_MODE, &videoout_mode);

	*p_video_out_ctrl=video_out_ctrl ;
	return ret;
}

static HD_RESULT get_out_caps(HD_PATH_ID video_out_ctrl,HD_VIDEOOUT_SYSCAPS *p_video_out_syscaps)
{
	HD_RESULT ret = HD_OK;
    HD_DEVCOUNT video_out_dev = {0};

	ret = hd_videoout_get(video_out_ctrl, HD_VIDEOOUT_PARAM_DEVCOUNT, &video_out_dev);
	if (ret != HD_OK) {
		return ret;
	}
	printf("##devcount %d\r\n", video_out_dev.max_dev_count);

	ret = hd_videoout_get(video_out_ctrl, HD_VIDEOOUT_PARAM_SYSCAPS, p_video_out_syscaps);
	if (ret != HD_OK) {
		return ret;
	}
	return ret;
}

static HD_RESULT set_out_param(HD_PATH_ID video_out_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOOUT_IN video_out_param={0};

	video_out_param.dim.w = p_dim->w;
	video_out_param.dim.h = p_dim->h;
	video_out_param.pxlfmt = HD_VIDEO_PXLFMT_YUV420;
	video_out_param.dir = HD_VIDEO_DIR_NONE;
	ret = hd_videoout_set(video_out_path, HD_VIDEOOUT_PARAM_IN, &video_out_param);
	if (ret != HD_OK) {
		return ret;
	}
	memset((void *)&video_out_param,0,sizeof(HD_VIDEOOUT_IN));
	ret = hd_videoout_get(video_out_path, HD_VIDEOOUT_PARAM_IN, &video_out_param);
	if (ret != HD_OK) {
		return ret;
	}
	printf("##video_out_param w:%d,h:%d %x %x\r\n", video_out_param.dim.w, video_out_param.dim.h, video_out_param.pxlfmt, video_out_param.dir);

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT init_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_init()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_init()) != HD_OK)
		return ret;
	if ((ret = hd_videoout_init()) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT open_module(VIDEO_LIVEVIEW *p_stream, HD_DIM* p_proc_max_dim, UINT32 out_type)
{
	HD_RESULT ret;
	
	// set videocap config
	ret = set_cap_cfg(&p_stream->cap_ctrl);
	if (ret != HD_OK) {
		printf("set cap-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set videoproc config
	ret = set_proc_cfg(&p_stream->proc_ctrl, p_proc_max_dim, HD_VIDEOPROC_0_CTRL);
	if (ret != HD_OK) {
		printf("set proc-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set videoproc config for nn
	ret = set_proc_cfg(&p_stream->proc_ctrl_alg, p_proc_max_dim, HD_VIDEOPROC_0_CTRL);
	if (ret != HD_OK) {
		printf("set proc-cfg alg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set videoout config
	ret = set_out_cfg(HD_VIDEOOUT_0_CTRL,&p_stream->out_ctrl, out_type);
	if (ret != HD_OK) {
		printf("set out-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	if ((ret = hd_videocap_open(HD_VIDEOCAP_0_IN_0, HD_VIDEOCAP_0_OUT_0, &p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_0, &p_stream->proc_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_1, &p_stream->proc_path_alg)) != HD_OK)
		return ret;
	if ((ret = hd_videoout_open(HD_VIDEOOUT_0_IN_0, HD_VIDEOOUT_0_OUT_0, &p_stream->out_path)) != HD_OK)
		return ret;

	return HD_OK;
}

static HD_RESULT open_module_2(VIDEO_LIVEVIEW *p_stream, HD_DIM* p_proc_max_dim, UINT32 out_type)
{
	HD_RESULT ret;

	// set videocap config
	ret = set_cap2_cfg(&p_stream->cap_ctrl);
	if (ret != HD_OK) {
		printf("set cap-cfg2 fail=%d\n", ret);
		return HD_ERR_NG;
	}

	// set videoproc config
	ret = set_proc_cfg(&p_stream->proc_ctrl, p_proc_max_dim, HD_VIDEOPROC_1_CTRL);
	if (ret != HD_OK) {
		printf("set proc-cfg2 fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set videoproc config for nn
	ret = set_proc_cfg(&p_stream->proc_ctrl_alg, p_proc_max_dim, HD_VIDEOPROC_1_CTRL);
	if (ret != HD_OK) {
		printf("set proc-cfg2 alg fail=%d\n", ret);
		return HD_ERR_NG;
	}	

	if((ret = hd_videocap_open(HD_VIDEOCAP_1_IN_0, HD_VIDEOCAP_1_OUT_0, &p_stream->cap_path)) != HD_OK)
        return ret;
    if ((ret = hd_videoproc_open(HD_VIDEOPROC_1_IN_0, HD_VIDEOPROC_1_OUT_0, &p_stream->proc_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_1_IN_0, HD_VIDEOPROC_1_OUT_1, &p_stream->proc_path_alg)) != HD_OK)
		return ret;

	return HD_OK;
}

static HD_RESULT close_module(VIDEO_LIVEVIEW *p_stream)
{
	HD_RESULT ret;

	if ((ret = hd_videocap_close(p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_close(p_stream->proc_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoout_close(p_stream->out_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT exit_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoout_uninit()) != HD_OK)
		return ret;
	return HD_OK;
}
static HD_RESULT yuv_copy(HD_VIDEO_FRAME *p_video_frame_dst,HD_VIDEO_FRAME *p_video_frame_src,HD_URECT *p_rect)
{
    HD_GFX_COPY param;
    HD_RESULT ret=0;
    //copy vout0 to common buffer
	memset(&param, 0, sizeof(HD_GFX_COPY));
	param.src_img.dim.w            = p_video_frame_src->dim.w;
	param.src_img.dim.h            = p_video_frame_src->dim.h;;
	param.src_img.format           = p_video_frame_src->pxlfmt;
	param.src_img.p_phy_addr[0]    = p_video_frame_src->phy_addr[0];//src_pa;
	param.src_img.p_phy_addr[1]    = p_video_frame_src->phy_addr[1];//src_pa;
	param.src_img.lineoffset[0]    = p_video_frame_src->loff[0];
	param.src_img.lineoffset[1]    = p_video_frame_src->loff[1];
	param.dst_img.dim.w            = p_video_frame_dst->dim.w;
	param.dst_img.dim.h            = p_video_frame_dst->dim.h;
	param.dst_img.format           = p_video_frame_dst->pxlfmt;
	param.dst_img.p_phy_addr[0]    = p_video_frame_dst->phy_addr[0];//dst_pa;
	param.dst_img.p_phy_addr[1]    = p_video_frame_dst->phy_addr[1];//dst_pa + 1920 * 1080;
	param.dst_img.lineoffset[0]    = p_video_frame_dst->loff[0];
	param.dst_img.lineoffset[1]    = p_video_frame_dst->loff[1];
	param.src_region.x             = 0;
	param.src_region.y             = 0;
	param.src_region.w             = p_video_frame_src->dim.w;
	param.src_region.h             = p_video_frame_src->dim.h;
	param.dst_pos.x                = p_rect->x;
	param.dst_pos.y                = p_rect->y;
	param.colorkey                 = 0;
	param.alpha                    = 255;

	ret = hd_gfx_copy(&param);
	if(ret != HD_OK){
		printf("hd_gfx_copy fail=%d\n", ret);
	}
    return ret;

}

static void *aquire_yuv_thread(void *arg)
{
	VIDEO_LIVEVIEW* p_stream0 = (VIDEO_LIVEVIEW *)arg;
	VIDEO_LIVEVIEW* p_stream1 = p_stream0+1;
	HD_RESULT ret = HD_OK;
	HD_VIDEO_FRAME *p_video_frame_dst=0;
	HD_VIDEO_FRAME *p_video_frame_src=0;
	HD_VIDEO_FRAME video_frame = {0};
	HD_VIDEO_FRAME video_frame1 = {0};
	HD_VIDEO_FRAME video_frame_new = {0};
	UINT32 phy_addr_main=0;
	UINT32 blk_size = YUV_BLK_SIZE;
	HD_COMMON_MEM_VB_BLK blk =0;

	printf("\r\nif you want to stop, enter \"q\" to exit !! \r\n\r\n");

	//--------- pull data test ---------
	while (p_stream0->proc_exit == 0) {
		ret = hd_videoproc_pull_out_buf(p_stream0->proc_path, &video_frame, p_stream0->wait_ms); // -1 = blocking mode, 0 = non-blocking mode, >0 = blocking-timeout mode
		if (ret != HD_OK) {
		    printf("0pull_out(%d) error = %d!!\r\n", p_stream0->wait_ms, ret);
			goto rel_none;
		}
		ret = hd_videoproc_pull_out_buf(p_stream1->proc_path, &video_frame1, p_stream1->wait_ms); // -1 = blocking mode, 0 = non-blocking mode, >0 = blocking-timeout mode
		if (ret != HD_OK) {
		    printf("1pull_out(%d) error = %d!!\r\n", p_stream1->wait_ms, ret);
			goto rel_0;
		}

        if(p_stream0->copy_new_buf) {

    		//--- Get memory ---
    		blk = hd_common_mem_get_block(HD_COMMON_MEM_COMMON_POOL, blk_size, DDR_ID0); // Get block from mem pool
    		if (blk == HD_COMMON_MEM_VB_INVALID_BLK) {
    			printf("get block fail (0x%x).. try again later.....\r\n", blk);
    			goto rel_1;
    		}

            phy_addr_main = hd_common_mem_blk2pa(blk); // Get physical addr
			if (phy_addr_main == 0) {
				printf("blk2pa fail, blk = 0x%x\r\n", blk);
        		goto rel_new;
			}

			video_frame_new.sign        = MAKEFOURCC('V','F','R','M');
			video_frame_new.p_next      = NULL;
			video_frame_new.ddr_id      = DDR_ID0;
			video_frame_new.pxlfmt      = HD_VIDEO_PXLFMT_YUV420;
			video_frame_new.dim.w       = p_stream0->out_max_dim.w;
			video_frame_new.dim.h       = p_stream0->out_max_dim.h;
			video_frame_new.count       = 0;
			video_frame_new.timestamp   = hd_gettime_us();
			video_frame_new.loff[0]     = p_stream0->out_max_dim.w; // Y
			video_frame_new.loff[1]     = p_stream0->out_max_dim.w; // UV
			video_frame_new.phy_addr[0] = phy_addr_main;                          // Y
			video_frame_new.phy_addr[1] = phy_addr_main+p_stream0->out_max_dim.w*p_stream0->out_max_dim.h;  // UV pack
			video_frame_new.blk         = blk;

            p_video_frame_src = &video_frame;
            p_video_frame_dst = &video_frame_new;

            ret = yuv_copy(p_video_frame_dst,p_video_frame_src,&p_stream0->pip_rect);
			if (ret != HD_OK) {
				printf("vout0 yuv_copy fail %d\r\n", ret);
        		goto rel_new;
			}
            p_video_frame_src = &video_frame1;
            p_video_frame_dst = &video_frame_new;

        } else {
           p_video_frame_src = &video_frame1;
           p_video_frame_dst = &video_frame;

        }

        ret = yuv_copy(p_video_frame_dst,p_video_frame_src,&p_stream1->pip_rect);
		if (ret != HD_OK) {
			printf("vout1 yuv_copy fail %d\r\n", ret);
    		goto rel_new;
		}

        ret = hd_videoout_push_in_buf(p_stream0->out_path, p_video_frame_dst, NULL, 0);
        ret = hd_videoout_push_in_buf(p_stream1->out_path, &video_frame1, NULL, 0);
		goto rel_new;

rel_new:
        if(p_stream0->copy_new_buf) {
			ret = hd_common_mem_release_block(blk);
    		if (ret != HD_OK) {
    			printf("release new_buf error %d!!\r\n\r\n",ret);
    		}
        }
rel_1:
		ret = hd_videoproc_release_out_buf(p_stream1->proc_path, &video_frame1);
		if (ret != HD_OK) {
			printf("release_out1 error %d!!\r\n\r\n",ret);
		}
rel_0:
		ret = hd_videoproc_release_out_buf(p_stream0->proc_path, &video_frame);
		if (ret != HD_OK) {
			printf("release_out0 error %d!!\r\n\r\n",ret);
		}
rel_none:
		usleep(1000); //delay 1 ms
	}
    printf("exit flow\r\n");
	return 0;
}

#if NET_NUM > 0
static HD_RESULT close_module_ext(VIDEO_LIVEVIEW *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videoproc_close(p_stream->proc_path_alg)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT open_module_extend(VIDEO_LIVEVIEW *p_stream, HD_DIM* p_proc_max_dim, HD_IN_ID in_id, HD_OUT_ID out_id, UINT32 out_type)
{
	HD_RESULT ret;
	if ((ret = hd_videoproc_open(in_id, out_id, &p_stream->proc_path_alg)) != HD_OK) {
	//if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, out_id, &p_stream->proc_path_alg)) != HD_OK) {
		printf("open_module_extend fail!\n\r");
		return ret;
	}
	return HD_OK;
}

static HD_RESULT set_proc_param_extend(HD_PATH_ID video_proc_path, HD_PATH_ID src_path, HD_DIM* p_dim, UINT32 dir)
{
	HD_RESULT ret = HD_OK;

	if (p_dim != NULL) { //if videoproc is already binding to dest module, not require to setting this!
		HD_VIDEOPROC_OUT_EX video_out_param = {0};
		video_out_param.src_path = src_path;
		video_out_param.dim.w = p_dim->w;
		video_out_param.dim.h = p_dim->h;
		video_out_param.pxlfmt = VDO_FRAME_FORMAT;
		video_out_param.dir = dir;
		video_out_param.depth = 1; //set 1 to allow pull

		ret = hd_videoproc_set(video_proc_path, HD_VIDEOPROC_PARAM_OUT_EX, &video_out_param);
	}

	return ret;
}


#if NN_USE_DSP
static HD_RESULT dsp_init(VOID)
{
	UINT32 dsp_running = 0;
	CUSTNN_IPC_INIT init = {0};
	NVTIPC_ER ipc_ret;
	HD_RESULT ret = HD_OK;

	// init ipc module
	ipc_ret = nvt_ipc_init();
	if (ipc_ret != NVTIPC_ER_OK) {
		DBG_DUMP("nvt_ipc_init fail=%d\n", ipc_ret);
		return HD_ERR_INIT;
	}

	// init dsp module
	ret = vendor_dsp_init();
	if (ret != HD_OK) {
		DBG_DUMP("vendor_dsp_init fail=%d\n", ret);
		return ret;
	}

	// init share memory
	init.size = custnn_get_buf_size();
	ret = vendor_dsp_init_share_mem(init.size, &init.pa, &init.va);
	if (ret != HD_OK) {
		DBG_DUMP("vendor_dsp_init_share_mem fail=%d\n", ret);
		return ret;
	}

	ret = vendor_dsp_is_running(DSP_CORE_ID_1, &dsp_running);
	if (ret != HD_OK) {
		DBG_DUMP("vendor_dsp_is_running fail=%d\r\n", ret);
		return ret;
	}

	if (!dsp_running) {
		DBG_DUMP("load dsp...\n");
		vendor_dsp_load(DSP_CORE_ID_1);
		sleep(2);
	}
	else {
		DBG_DUMP("dsp is running...\n");
	}

	if (custnn_init(&init, CUSTNN_SENDTO_DSP1) != CUSTNN_STA_OK) {
		DBG_DUMP("custnn_init fail\n");
		return HD_ERR_INIT;
	};

	return ret;
}

static HD_RESULT dsp_uninit(VOID)
{
	vendor_dsp_close(DSP_CORE_ID_1);
	vendor_dsp_uninit_share_mem();
	vendor_dsp_uninit();
	nvt_ipc_uninit();

	return HD_OK;
}
#endif // NN_USE_DSP

#if DYNAMIC_LABEL
HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, UINT32 label_num, const CHAR *filename)
#else
HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, const CHAR *filename)
#endif
{
	FILE *fd;
	CHAR *p_line = (CHAR *)addr;
#if DYNAMIC_LABEL
	UINT32 label_idx = 0;
#endif

	fd = fopen(filename, "r");
	if (!fd) {
		DBG_ERR("cannot read %s\r\n", filename);
		return HD_ERR_NG;
	}

	DBG_DUMP("open %s ok\r\n", filename);

	while (fgets(p_line, line_len, fd) != NULL) {
		p_line[strlen(p_line) - 1] = '\0'; // remove newline character
		p_line += line_len;
#if DYNAMIC_LABEL
		label_idx++;
		if (label_idx == label_num) {
			break;
		}
#endif
	}

	if (fd) {
		fclose(fd);
	}

	return HD_OK;
}

static UINT32 ai_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;
	//DBG_DUMP("model addr = %08x\r\n", (int)model_addr);

	fd = fopen(p_filename, "rb");
	if (!fd) {
		DBG_DUMP("cannot read %s\r\n", p_filename);
		return 0;
	}

	fseek ( fd, 0, SEEK_END );
	file_size = ALIGN_CEIL_4( ftell(fd) );
	fseek ( fd, 0, SEEK_SET );

	read_size = fread ((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
		DBG_DUMP("size mismatch, real = %d, idea = %d\r\n", (int)read_size, (int)file_size);
	}
	fclose(fd);
	return read_size;
}

#if 0
static BOOL net_sample_write_file(CHAR *filepath, UINT32 addr, UINT32 size)
{
	FILE *fsave = NULL;

	fsave = fopen(filepath, "wb");
	if (fsave == NULL) {
		printf("fopen fail\n");
		return FALSE;
	}
	fwrite((UINT8 *)addr, size, 1, fsave);

	return TRUE;
}

static void *nn_thread(void *arg)
{
	UINT32 i, va;
	HD_RESULT ret;

	VENDOR_AIS_NETWORK_PARM *p_net_parm = (VENDOR_AIS_NETWORK_PARM*)arg;
	VIDEO_LIVEVIEW *stream = p_net_parm->stream;
	HD_VIDEO_FRAME video_frame = {0};

	while(stream->proc_exit==0) {
		ret = hd_videoproc_pull_out_buf(stream->proc_path_alg, &video_frame, -1);  // -1 = blocking mode, 0 = non-blocking mode, >0 = blocking-timeout mode
		if(ret != HD_OK) {
			printf("hd_videoproc_pull_out_buf fail (%d)\n\r", ret);
			return 0;
		}

		printf("\n\rVideo frame info, sensor %d, net %d\n\r", p_net_parm->sensor_id, p_net_parm->net_id);
		printf("  ddr_id %d\n\r", video_frame.ddr_id);
		printf("  pxlfmt %d\n\r", video_frame.pxlfmt);
		printf("  plane-0 width %d, height %d\n\r", video_frame.dim.w, video_frame.dim.h);	
		for(i=0; i<HD_VIDEO_MAX_PLANE; i++) {
			printf("  plane %d width %d, height %d\n\r", i, video_frame.pw[i], video_frame.ph[i]);
		}

		va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, video_frame.phy_addr[0], video_frame.pw[0]*video_frame.ph[0]*3/2);

		if(p_net_parm->sensor_id==1) {
			if(p_net_parm->net_id==1) {
				net_sample_write_file("net_sample_write_yuv_s1n1.raw", va, video_frame.pw[0]*video_frame.ph[0]*3/2);
			} else if(p_net_parm->net_id==2) {
				net_sample_write_file("net_sample_write_yuv_s1n2.raw", va, video_frame.pw[0]*video_frame.ph[0]*3/2);
			} else {
				printf("wrong net id %d of sensor %d\n\r", p_net_parm->net_id, p_net_parm->sensor_id);
			}
		} else if(p_net_parm->sensor_id==2) {
			if(p_net_parm->net_id==1) {
				net_sample_write_file("net_sample_write_yuv_s2n1.raw", va, video_frame.pw[0]*video_frame.ph[0]*3/2);
			} else if(p_net_parm->net_id==2) {
				net_sample_write_file("net_sample_write_yuv_s2n2.raw", va, video_frame.pw[0]*video_frame.ph[0]*3/2);
			} else {
				printf("wrong net id %d of sensor %d\n\r", p_net_parm->net_id, p_net_parm->sensor_id);
			}
		} else {
			printf("wrong sensor id %d\n\r", p_net_parm->sensor_id);
		}
		
		ret = hd_common_mem_munmap((void *)va, video_frame.pw[0]*video_frame.ph[0]*3/2);
		if(ret != HD_OK) {
			printf("memory unmap fail!\n\r");
			return 0;
		}
		ret = hd_videoproc_release_out_buf(stream->proc_path_alg, &video_frame);
		if(ret != HD_OK) {
			printf("hd_videoproc_release_out_buf fail (%d)\n\r", ret);
			return 0;
		}
	}

	return 0;
}
#else


static void *nn_thread(void *arg)
{
	#if NET_NUM > 0
	UINT32 i, m, n;
	VENDOR_AIS_RESULT_INFO *p_net_rslt;
	#endif

	HD_RESULT ret;
	UINT32 model_size, req_size=0;

	VENDOR_AIS_NETWORK_PARM *p_net_parm = (VENDOR_AIS_NETWORK_PARM*)arg;
	VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem = &p_net_parm->mem;
	VENDOR_AIS_IMG_PARM src_img;
	VIDEO_LIVEVIEW *stream = p_net_parm->stream;
	VENDOR_AIS_FLOW_MAP_MEM_PARM mem_manager;
	VENDOR_AIS_FLOW_MEM_PARM model_mem, rslt_mem;
	HD_VIDEO_FRAME video_frame = {0};
#if DYNAMIC_LABEL
	UINT32 label_num = 0;
#endif

	// allocate memory (parm, model, io buffer)
	model_size      = vendor_ais_auto_alloc_mem(p_tot_mem, &mem_manager);
	model_mem.pa    = p_tot_mem->pa;
	model_mem.va    = p_tot_mem->va;    // = load_addr
	model_mem.size  = model_size;
	rslt_mem.pa     = model_mem.pa + model_mem.size;
	rslt_mem.va     = model_mem.va + model_mem.size;
	rslt_mem.size   = sizeof(VENDOR_AIS_RESULT_INFO) + MAX_OBJ_NUM * sizeof(VENDOR_AIS_RESULT);
	req_size        = model_mem.size + rslt_mem.size;
	if (req_size > p_tot_mem->size) {
		printf("memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)req_size);
		return 0;
	}
	if (model_size > p_tot_mem->size) {
		printf("memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)model_size);
		return 0;
	}
	memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer

	// map user/kernel memory
	ret = vendor_ais_net_gen_init(mem_manager, p_net_parm->run_id);		// run -> quit (ok) -> run -> quit (exception!?)
	if (ret != HD_OK) {
		printf("net gen init fail=%d\r\n", ret);
		goto gen_init_fail;
	} else {
		printf("[ai sample] init done: %s!\n", p_net_parm->p_model_path);
	}
#if DYNAMIC_LABEL
	vendor_ais_get_label_num(&label_num, model_mem, p_net_parm->run_id);
	if (label_num > 0) {
		g_class_labels[p_net_parm->run_id] = (CHAR*)malloc(label_num*VENDOR_AIS_LBL_LEN);//alloc label buffer
		ret = ai_read_label((UINT32)g_class_labels[p_net_parm->run_id], VENDOR_AIS_LBL_LEN, label_num, g_label_name);
		if (ret != HD_OK) {
			DBG_ERR("ai_read_label fail=%d\n", ret);
			goto gen_init_fail;
		}
	}
#endif
	// nn processing
	while(stream->proc_exit==0) {
		ret = hd_videoproc_pull_out_buf(stream->proc_path_alg, &video_frame, -1);  // -1 = blocking mode, 0 = non-blocking mode, >0 = blocking-timeout mode
		if(ret != HD_OK) {
			printf("hd_videoproc_pull_out_buf fail (%d)\n\r", ret);
			return 0;
		}

		src_img.width   	= video_frame.dim.w;
		src_img.height  	= video_frame.dim.h;
		src_img.channel 	= 2;
		src_img.line_ofs	= video_frame.loff[0];
		src_img.fmt 		= VDO_FRAME_FORMAT;
		src_img.pa			= video_frame.phy_addr[0];
		src_img.va			= (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, video_frame.phy_addr[0], video_frame.pw[0]*video_frame.ph[0]*3/2);
		src_img.fmt_type    = VENDOR_COMMON_PXLFMT_YUV420_TYPE;
		
		if (vendor_ais_net_input_init(&src_img, p_net_parm->run_id) != HD_OK) {
			printf("vendor_ais_net_input_init fail: sensor %d, net %d, run_id %d\n\r", p_net_parm->sensor_id, p_net_parm->net_id, p_net_parm->run_id);
			goto gen_init_fail;
		}

		memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
		hd_common_mem_flush_cache((VOID *)mem_manager.user_buff.va, mem_manager.user_buff.size);


//		gettimeofday(&tstart[p_net_parm->net_id-1], NULL);
#if USE_NEON
		vendor_ais_proc_net(model_mem, rslt_mem, &src_img, p_net_parm->run_id);
#else
		vendor_ais_proc_net(model_mem, rslt_mem, p_net_parm->run_id);
#endif
//		gettimeofday(&tend[p_net_parm->net_id-1], NULL);

		p_net_rslt = vendor_ais_get_results(p_net_parm->run_id);

		// show results
		if(p_net_rslt != NULL) {
			for(i=0; i<4; i++) {
				if(show_net_results[i] && p_net_parm->sensor_id==(i/2+1) && p_net_parm->net_id==((i%2)+1)) {
					printf("\r\nClassification results, sensor %d, net %d:\r\n", p_net_parm->sensor_id, p_net_parm->net_id);
					for(m=0; m < p_net_rslt->result_num; m++){
						VENDOR_AIS_RESULT *p_rslt = &p_net_rslt->p_result[m];
						for(n=0; n < TOP_N; n++) {
						#if DYNAMIC_LABEL
							printf("%ld. no=%ld, label=%s, score=%f\r\n", n + 1, p_rslt->no[n]
								, &g_class_labels[p_net_parm->run_id][p_rslt->no[n] * VENDOR_AIS_LBL_LEN], p_rslt->score[n]);
						#else
							printf("%ld. no=%ld, label=%s, score=%f\r\n", n + 1, p_rslt->no[n],
								&g_class_labels[p_rslt->no[n] * VENDOR_AIS_LBL_LEN], p_rslt->score[n]);
						#endif
						}
					}
				}
			}
		}


		ret = hd_common_mem_munmap((void *)src_img.va, video_frame.pw[0]*video_frame.ph[0]*3/2);
		if (ret != HD_OK) {
			printf("nn_thread_api : (src_img.va)hd_common_mem_munmap fail\r\n");
			goto gen_init_fail;
		}

		vendor_ais_net_input_uninit(p_net_parm->run_id);

		ret = hd_videoproc_release_out_buf(stream->proc_path_alg, &video_frame);
		if(ret != HD_OK) {
			printf("hd_videoproc_release_out_buf fail (%d)\n\r", ret);
			return 0;
		}
	} // end while

gen_init_fail:
 	ret = vendor_ais_net_gen_uninit(p_net_parm->run_id);
	if(ret != HD_OK) {
		printf("vendor_ais_net_gen_uninit fail (%d)\n\r", ret);
	}
#if DYNAMIC_LABEL
	if (g_class_labels[p_net_parm->run_id] != NULL) {
		free(g_class_labels[p_net_parm->run_id]);
		g_class_labels[p_net_parm->run_id] = NULL;
	}
#endif

	return 0;
}
#endif

#endif


MAIN(argc, argv)
{
	HD_RESULT ret;
	INT key;
	VIDEO_LIVEVIEW stream[2] = {0}; //0: shdr main stream, 1: shdr sub stream
    HD_DIM  sub_dim;
    HD_DIM  main_dim;
    INT display = 2;  //default is pip view

	#if NET_NUM > 0
	INT i, req_mem_size=0;
	UINT32 file_size, pa_mark, va_mark, id_cnt;
	#endif

	#if NET_NUM_SENSOR1 > 0
	VENDOR_AIS_FLOW_MEM_PARM mem_parm_sen1[NET_NUM_SENSOR1] = {0};
	VENDOR_AIS_NETWORK_PARM net_parm_sen1[NET_NUM_SENSOR1] = {0};
	VIDEO_LIVEVIEW stream0_extpath[NET_NUM_SENSOR1] = { 0 };
	for(i=0; i<NET_NUM_SENSOR1; i++) req_mem_size += mem_size_sen1[i];
	#endif
	
	#if NET_NUM_SENSOR2 > 0
	VENDOR_AIS_FLOW_MEM_PARM mem_parm_sen2[NET_NUM_SENSOR2] = {0};
	VENDOR_AIS_NETWORK_PARM net_parm_sen2[NET_NUM_SENSOR2] = {0};
	VIDEO_LIVEVIEW stream1_extpath[NET_NUM_SENSOR2] = { 0 };
	for(i=0; i<NET_NUM_SENSOR2; i++) req_mem_size += mem_size_sen1[i];
	#endif

	#if NET_NUM > 0
	if(req_mem_size > NN_TOTAL_MEM_SIZE) {
		printf("Required total memory %d is larger than allowed %d\n\r", req_mem_size, NN_TOTAL_MEM_SIZE);
		return 0;
	}
	#endif
	
	if (argc >= 2) {
        display = atoi(argv[1]);
        switch(display)
        {
            case 0:
                    //cap0 ->vout0
            break;
            case 1:
                    //cap1->vout0
            break;
            case 2:
                    //cap0+cap1 ->vout0 (pip)
                stream[0].copy_new_buf = 0;
            break;
            case 3:
                    //cap0+cap1 ->vout0 (side by side)
                stream[0].copy_new_buf = 1;
            break;
            default:
            {
                printf("not sup %d\r\n",display);
                return 0;
            }
        }
		printf("###display %d,copy_new_buf %d\r\n",display,stream[0].copy_new_buf);
	}

	// init hdal
	ret = hd_common_init(0);
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
		goto exit;
	}

	//set project config for AI
	hd_common_sysconfig(0, (1<<16), 0, VENDOR_AI_CFG); //enable AI engine

	// init memory
	ret = mem_init();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
		goto exit;
	}

	// init all modules
	ret = init_module();
	if (ret != HD_OK) {
		printf("init fail=%d\n", ret);
		goto exit;
	}

	// open video liview modules (sensor 1st)
	stream[0].proc_max_dim.w = VDO_SIZE_W; //assign by user
	stream[0].proc_max_dim.h = VDO_SIZE_H; //assign by user
    stream[0].wait_ms = -1; //blocking mode
	ret = open_module(&stream[0], &stream[0].proc_max_dim, VDOOUT0_TYPE);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}

	// open video liview modules (sensor 2nd)
	stream[1].proc_max_dim.w = VDO_SIZE_W; //assign by user
	stream[1].proc_max_dim.h = VDO_SIZE_H; //assign by user
    stream[1].wait_ms = -1;///0; //none-blocking mode
	ret = open_module_2(&stream[1], &stream[1].proc_max_dim, VDOOUT0_TYPE);
	if (ret != HD_OK) {
		printf("open2 fail=%d\n", ret);
		goto exit;
	}

	// open video proc (nn)
	#if NET_NUM_SENSOR1 > 0
	stream0_extpath[0].proc_max_dim.w = VDO_SIZE_W;
	stream0_extpath[0].proc_max_dim.h = VDO_SIZE_H;
	ret = open_module_extend(&stream0_extpath[0], &stream0_extpath[0].proc_max_dim, HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_5, HD_VIDEO_DIR_NONE);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}
	#endif
	#if NET_NUM_SENSOR1 > 1
	stream0_extpath[1].proc_max_dim.w = VDO_SIZE_W;
	stream0_extpath[1].proc_max_dim.h = VDO_SIZE_H;
	ret = open_module_extend(&stream0_extpath[1], &stream0_extpath[1].proc_max_dim, HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_6, HD_VIDEO_DIR_NONE);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}
	#endif

	#if NET_NUM_SENSOR2 > 0
	stream1_extpath[0].proc_max_dim.w = VDO_SIZE_W;
	stream1_extpath[0].proc_max_dim.h = VDO_SIZE_H;
	ret = open_module_extend(&stream1_extpath[0], &stream1_extpath[0].proc_max_dim, HD_VIDEOPROC_1_IN_0, HD_VIDEOPROC_1_OUT_5, HD_VIDEO_DIR_NONE);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}
	#endif
	#if NET_NUM_SENSOR2 > 1
	stream1_extpath[1].proc_max_dim.w = VDO_SIZE_W;
	stream1_extpath[1].proc_max_dim.h = VDO_SIZE_H;
	ret = open_module_extend(&stream1_extpath[1], &stream1_extpath[1].proc_max_dim, HD_VIDEOPROC_1_IN_0, HD_VIDEOPROC_1_OUT_6, HD_VIDEO_DIR_NONE);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}
	#endif

	// get videocap capability (sensor 1st)
	ret = get_cap_caps(stream[0].cap_ctrl, &stream[0].cap_syscaps);
	if (ret != HD_OK) {
		printf("get cap-caps fail=%d\n", ret);
		goto exit;
	}

	// get videocap capability (sensor 2nd)
	ret = get_cap_caps(stream[1].cap_ctrl, &stream[1].cap_syscaps);
	if (ret != HD_OK) {
		printf("get cap2-caps fail=%d\n", ret);
		goto exit;
	}

	// get videoout capability (sensor 1st)
	ret = get_out_caps(stream[0].out_ctrl, &stream[0].out_syscaps);
	if (ret != HD_OK) {
		printf("get out-caps fail=%d\n", ret);
		goto exit;
	}
	stream[0].out_max_dim = stream[0].out_syscaps.output_dim;

	// set videocap parameter (sensor 1st)
	stream[0].cap_dim.w = VDO_SIZE_W; //assign by user
	stream[0].cap_dim.h = VDO_SIZE_H; //assign by user
	ret = set_cap_param(stream[0].cap_path, &stream[0].cap_dim);
	if (ret != HD_OK) {
		printf("set cap fail=%d\n", ret);
		goto exit;
	}

	// set videocap parameter (sensor 2nd)
	stream[1].cap_dim.w = VDO_SIZE_W; //assign by user
	stream[1].cap_dim.h = VDO_SIZE_H; //assign by user
	ret = set_cap_param(stream[1].cap_path, &stream[1].cap_dim);
	if (ret != HD_OK) {
		printf("set cap2 fail=%d\n", ret);
		goto exit;
	}

    if(!stream[0].copy_new_buf){
        stream[0].pip_rect.x = 0;
        stream[0].pip_rect.y = 0;
        stream[0].pip_rect.w = stream[0].out_max_dim.w;
        stream[0].pip_rect.h = stream[0].out_max_dim.h;
    } else {
        stream[0].pip_rect.x = 0;
        stream[0].pip_rect.y = 0;
        stream[0].pip_rect.w = stream[0].out_max_dim.w/2;
        stream[0].pip_rect.h = stream[0].out_max_dim.h;
    }
    main_dim.w = stream[0].pip_rect.w;
    main_dim.h = stream[0].pip_rect.h;
	
	// set videoproc parameter (sensor 1st)
	ret = set_proc_param(stream[0].proc_path, &main_dim);
	if (ret != HD_OK) {
		printf("set proc fail=%d\n", ret);
		goto exit;
	}
    if(!stream[0].copy_new_buf) {
        stream[1].pip_rect.x = 50;
        stream[1].pip_rect.y = 50;
        stream[1].pip_rect.w = stream[0].out_syscaps.output_dim.w/2;
        stream[1].pip_rect.h = stream[0].out_syscaps.output_dim.h/2;
    } else {
        stream[1].pip_rect.x = stream[0].out_max_dim.w/2;
        stream[1].pip_rect.y = 0;
        stream[1].pip_rect.w = stream[0].out_max_dim.w/2;
        stream[1].pip_rect.h = stream[0].out_max_dim.h;
    }
    sub_dim.w = stream[1].pip_rect.w;
    sub_dim.h = stream[1].pip_rect.h;

	// set videoproc parameter (sensor 1st, alg)
	stream[0].proc_max_dim_alg.w = VDO_SIZE_W;
	stream[0].proc_max_dim_alg.h = VDO_SIZE_H;
	ret = set_proc_param(stream[0].proc_path_alg, &stream[0].proc_max_dim_alg);
	if (ret != HD_OK) {
		printf("set proc alg fail=%d\n", ret);
		goto exit;
	}
	
	// set videoproc parameter (sensor 2nd)
	ret = set_proc_param(stream[1].proc_path, &sub_dim);
	if (ret != HD_OK) {
		printf("set proc2 fail=%d\n", ret);
		goto exit;
	}
	// set videoproc parameter (sensor 2nd, alg)
	stream[1].proc_max_dim_alg.w = VDO_SIZE_W;
	stream[1].proc_max_dim_alg.h = VDO_SIZE_H;
	ret = set_proc_param(stream[1].proc_path_alg, &stream[1].proc_max_dim_alg);
	if (ret != HD_OK) {
		printf("set proc alg fail=%d\n", ret);
		goto exit;
	}

	// set videoproc parameter (sensor 1st, nn)
	#if NET_NUM_SENSOR1 > 0
	for(i=0; i < NET_NUM_SENSOR1; i++) {
		stream0_extpath[i].proc_dim_alg.w = VDO_SIZE_W;
		stream0_extpath[i].proc_dim_alg.h = VDO_SIZE_H;
		stream0_extpath[i].proc_exit = 0;
		ret = set_proc_param_extend(stream0_extpath[i].proc_path_alg, HD_VIDEOPROC_0_OUT_1, &stream0_extpath[i].proc_dim_alg, HD_VIDEO_DIR_NONE);
		if(ret != HD_OK) {
			printf("set_proc_param_extend 0 fail\n\r");
			goto exit;
		}
	}
	#endif

	// set videoproc parameter (sensor 2nd, nn)
	#if NET_NUM_SENSOR2 > 0
	for(i=0; i < NET_NUM_SENSOR2; i++) {
		stream1_extpath[i].proc_dim_alg.w = VDO_SIZE_W;
		stream1_extpath[i].proc_dim_alg.h = VDO_SIZE_H;
		stream1_extpath[i].proc_exit = 0;
		ret = set_proc_param_extend(stream1_extpath[i].proc_path_alg, HD_VIDEOPROC_1_OUT_1, &stream1_extpath[i].proc_dim_alg, HD_VIDEO_DIR_NONE);
		if(ret != HD_OK) {
			printf("set_proc_param_extend 1 fail\n\r");
			goto exit;
		}
	}
	#endif

	// set videoout parameter (sensor 1st)
	stream[0].out_dim.w = stream[0].out_max_dim.w; //using device max dim.w
	stream[0].out_dim.h = stream[0].out_max_dim.h; //using device max dim.h
	ret = set_out_param(stream[0].out_path, &stream[0].out_dim);
	if (ret != HD_OK) {
		printf("set out fail=%d\n", ret);
		goto exit;
	}
	
	// bind video_liveview modules (sensor 1st)
	hd_videocap_bind(HD_VIDEOCAP_0_OUT_0, HD_VIDEOPROC_0_IN_0);

	// bind video_liveview modules (sensor 2nd)
	hd_videocap_bind(HD_VIDEOCAP_1_OUT_0, HD_VIDEOPROC_1_IN_0);

    if(display ==0) {
        hd_videoproc_bind(HD_VIDEOPROC_0_OUT_0, HD_VIDEOOUT_0_IN_0);
    } else if(display ==1) {
        hd_videoproc_bind(HD_VIDEOPROC_1_OUT_0, HD_VIDEOOUT_0_IN_0);
    }

    ret = hd_gfx_init();
    if(ret != HD_OK) {
        printf("init fail=%d\n", ret);
        goto exit;
    }

	// start video_liveview modules (sensor 1st)
	hd_videocap_start(stream[0].cap_path);
	hd_videoproc_start(stream[0].proc_path);
	hd_videoout_start(stream[0].out_path);

	#if NET_NUM_SENSOR1 > 0
	hd_videoproc_start(stream[0].proc_path_alg);
	for(i=0; i < NET_NUM_SENSOR1; i++) {
		hd_videoproc_start(stream0_extpath[i].proc_path_alg);
	}
	#endif

	// start video_liveview modules (sensor 2nd)
	hd_videocap_start(stream[1].cap_path);
	hd_videoproc_start(stream[1].proc_path);

	#if NET_NUM_SENSOR2 > 0
	hd_videoproc_start(stream[1].proc_path_alg);
	for(i=0; i < NET_NUM_SENSOR2; i++) {
		hd_videoproc_start(stream1_extpath[i].proc_path_alg);
	}
	#endif
	
	// just wait ae/awb stable for auto-test, if don't care, user can remove it
	sleep(1);

    if((display ==2)||(display ==3)) {
    	// let encode_thread start to work
    	stream[0].flow_start = 1;
    	// create aquire_thread (pull_out bitstream)
    	ret = pthread_create(&stream[0].aquire_thread_id, NULL, aquire_yuv_thread, (void *)&stream[0]);
    	if (ret < 0) {
    		printf("create aquire thread failed");
    		goto exit;
    	}
    }	

	// nn S ------------------------------------------------
	#if NN_USE_DSP
	ret = dsp_init();
	if (ret != HD_OK) {
		DBG_ERR("dsp_init fail=%d\n", ret);
		goto exit;
	}
	#endif

#if (DYNAMIC_LABEL == 0)
	ret = ai_read_label((UINT32)g_class_labels, VENDOR_AIS_LBL_LEN, g_label_name);
	if (ret != HD_OK) {
		DBG_ERR("ai_read_label fail=%d\n", ret);
		goto exit;
	}
#endif
	// allocate memory
	pa_mark = g_mem.pa;
	va_mark = g_mem.va;
	id_cnt = 0;

	#if NET_NUM_SENSOR1 > 0
	for(i=0; i < NET_NUM_SENSOR1; i++) {
		mem_parm_sen1[i].pa = pa_mark;
		mem_parm_sen1[i].va = va_mark;
		mem_parm_sen1[i].size = mem_size_sen1[i];

		net_parm_sen1[i].mem = mem_parm_sen1[i];
		net_parm_sen1[i].run_id = id_cnt;
		net_parm_sen1[i].p_model_path = model_name_sen1[i];
		net_parm_sen1[i].stream = &stream0_extpath[i];
		net_parm_sen1[i].sensor_id = 1;
		net_parm_sen1[i].net_id = i + 1;
		
		pa_mark += mem_size_sen1[i];
		va_mark += mem_size_sen1[i];
		id_cnt++;
	}
	#endif
	#if NET_NUM_SENSOR2 > 0
	for(i=0; i < NET_NUM_SENSOR2; i++) {
		mem_parm_sen2[i].pa = pa_mark;
		mem_parm_sen2[i].va = va_mark;
		mem_parm_sen2[i].size = mem_size_sen2[i];

		net_parm_sen2[i].mem = mem_parm_sen2[i];
		net_parm_sen2[i].run_id = id_cnt;
		net_parm_sen2[i].p_model_path = model_name_sen2[i];
		net_parm_sen2[i].stream = &stream1_extpath[i];
		net_parm_sen2[i].sensor_id = 2;
		net_parm_sen2[i].net_id = i + 1;
		
		pa_mark += mem_size_sen2[i];
		va_mark += mem_size_sen2[i];
		id_cnt++;
	}
	#endif


	// load model
	#if NET_NUM_SENSOR1 > 0
	for(i=0; i < NET_NUM_SENSOR1; i++) { 
		file_size = ai_load_file(net_parm_sen1[i].p_model_path, net_parm_sen1[i].mem.va);
		if (file_size == 0) {
			DBG_DUMP("net load file fail: %s\r\n", net_parm_sen1[i].p_model_path);
			return 0;
		}
	}
	#endif

	#if NET_NUM_SENSOR2 > 0
	for(i=0; i < NET_NUM_SENSOR2; i++) { 
		file_size = ai_load_file(net_parm_sen2[i].p_model_path, net_parm_sen2[i].mem.va);
		if (file_size == 0) {
			DBG_DUMP("net load file fail: %s\r\n", net_parm_sen2[i].p_model_path);
			return 0;
		}
	}
	#endif

	// create threads
	#if 1
	#if NET_NUM_SENSOR1 > 0
	for(i=0; i < NET_NUM_SENSOR1; i++) {
		ret = pthread_create(&stream0_extpath[i].aquire_thread_id, NULL, nn_thread, (VOID*)(&net_parm_sen1[i]));
		if (ret < 0) {
			DBG_ERR("create thread0,%d failed\n\r", i);
			goto exit;
		}
	}
	#endif
	
	#if NET_NUM_SENSOR2 > 0
	for(i=0; i < NET_NUM_SENSOR2; i++) {
		ret = pthread_create(&stream1_extpath[i].aquire_thread_id, NULL, nn_thread, (VOID*)(&net_parm_sen2[i]));
		if (ret < 0) {
			DBG_ERR("create thread0,%d failed\n\r", i);
			goto exit;
		}
	}
	#endif

	#endif
	// nn E ------------------------------------------------


	// query user key
    printf("\r\nUsage:\n\r");
	printf("Enter q to exit\n\r");
	printf("Enter 1 (2) to swith result showing for sensor 1, net 1 (2)\n\r");
	printf("Enter 3 (4) to swith result showing for sensor 2, net 1 (2)\n\r");
	while (1) {
		key = GETCHAR();
		if (key == 'q' || key == 0x3) {
			// let feed_thread, aquire_thread stop loop and exit
			stream[0].proc_exit = 1;
			
			#if NET_NUM_SENSOR1 > 0
			for(i=0; i < NET_NUM_SENSOR1; i++) stream0_extpath[i].proc_exit=1;
			#endif
			#if NET_NUM_SENSOR2 > 0
			for(i=0; i < NET_NUM_SENSOR2; i++) stream1_extpath[i].proc_exit=1;
			#endif
				
			break;
		}

		if(key == '1') {
			show_net_results[0] = (show_net_results[0]+1) % 2;
			continue;
		}
		if(key == '2') {
			show_net_results[1] = (show_net_results[1]+1) % 2;
			continue;
		}
		if(key == '3') {
			show_net_results[2] = (show_net_results[2]+1) % 2;
			continue;
		}
		if(key == '4') {
			show_net_results[3] = (show_net_results[3]+1) % 2;
			continue;
		}

		#if (DEBUG_MENU == 1)
		if (key == 'd') {
			// enter debug menu
			hd_debug_run_menu();
            printf("\r\nEnter q to exit, Enter d to debug\r\n");
		}
		#endif
		if (key == '0') {
			get_cap_sysinfo(stream[0].cap_ctrl);
		}
	}

	/*
	for(i=0; i<NET_NUM_SENSOR1; i++) {
		printf("net-%d, start %lu, %lu;  end %lu, %lu;  total %lu\n\r", i, 
			tstart[i].tv_sec, tstart[i].tv_usec, tend[i].tv_sec, tend[i].tv_usec, 
			(tend[i].tv_sec - tstart[i].tv_sec) * 1000000 + (tend[i].tv_usec - tstart[i].tv_usec));
	}
	*/
	

	// stop video_liveview modules (sensor 1st)
	hd_videocap_stop(stream[0].cap_path);
	hd_videoproc_stop(stream[0].proc_path);
	hd_videoproc_stop(stream[0].proc_path_alg);
	#if NET_NUM_SENSOR1 > 0
	//hd_videoproc_stop(stream0_extpath[0].proc_path);		// ??
	hd_videoproc_stop(stream0_extpath[0].proc_path_alg);	// ??
	#endif
	#if NET_NUM_SENSOR1 > 1
	hd_videoproc_stop(stream0_extpath[1].proc_path_alg);	// ??
	#endif
	hd_videoout_stop(stream[0].out_path);
	
	// stop video_liveview modules (sensor 2nd)
	hd_videocap_stop(stream[1].cap_path);
	hd_videoproc_stop(stream[1].proc_path);
	hd_videoproc_stop(stream[1].proc_path_alg);
	#if NET_NUM_SENSOR2 > 0
	//hd_videoproc_stop(stream1_extpath[0].proc_path);
	hd_videoproc_stop(stream1_extpath[0].proc_path_alg);	//	??
	#endif
	#if NET_NUM_SENSOR2 > 1
	hd_videoproc_stop(stream1_extpath[1].proc_path_alg);	//	??
	#endif
	
    if((display ==2)||(display ==3)) {
    	// destroy thread (pull_out bitstream)
    	pthread_join(stream[0].aquire_thread_id, (void* )NULL);
    }
	
	#if NET_NUM_SENSOR1 > 0
	for(i=0; i < NET_NUM_SENSOR1; i++) {
		pthread_join(stream0_extpath[i].aquire_thread_id, (void* )NULL);
	}
	#endif
	#if NET_NUM_SENSOR2 > 0
	for(i=0; i < NET_NUM_SENSOR2; i++) {
		pthread_join(stream1_extpath[i].aquire_thread_id, (void* )NULL);
	}
	#endif

	// unbind video_liveview modules (sensor 1st)
	hd_videocap_unbind(HD_VIDEOCAP_0_OUT_0);

	// unbind video_liveview modules (sensor 2nd)
	hd_videocap_unbind(HD_VIDEOCAP_1_OUT_0);
    if(display ==0){
        hd_videoproc_unbind(HD_VIDEOPROC_0_OUT_0);
    } else if(display ==1) {
        hd_videoproc_unbind(HD_VIDEOPROC_1_IN_0);
    }

exit:
	#if NN_USE_DSP
	ret = dsp_uninit();
	if (ret != HD_OK) {
		DBG_ERR("dsp_uninit fail=%d\n", ret);
	}
	#endif
	
	// close video_liveview modules (sensor 1st)
	ret = close_module(&stream[0]);
	if (ret != HD_OK) {
		printf("close fail=%d\n", ret);
	}

	// close video_liveview modules (sub)
	ret = close_module(&stream[1]);
	if (ret != HD_OK) {
		printf("close fail=%d\n", ret);
	}

	#if NET_NUM_SENSOR1 > 0
	for(i=0; i < NET_NUM_SENSOR1; i++) {
		ret = close_module_ext(&stream0_extpath[i]);
	}
	#endif
	#if NET_NUM_SENSOR2 > 0
	for(i=0; i < NET_NUM_SENSOR2; i++) {
		ret = close_module_ext(&stream1_extpath[i]);
	}
	#endif

	// uninit all modules
	ret = exit_module();
	if (ret != HD_OK) {
		printf("exit fail=%d\n", ret);
	}

	// uninit memory
	ret = mem_exit();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
	}

	// uninit hdal
	ret = hd_common_uninit();
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
	}

	return 0;
}
