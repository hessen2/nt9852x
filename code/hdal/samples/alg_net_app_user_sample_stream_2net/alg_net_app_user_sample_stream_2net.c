/**
	@brief Sample code of video liveview.\n

	@file video_liveview.c

	@author Janice Huang

	@ingroup mhdal

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include "hdal.h"
#include "hd_debug.h"

#include "vendor_ai/vendor_ai.h"
#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include "net_flow_user_sample/net_layer_sample.h"
#include "vendor_common.h"

// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#else
#include <FreeRTOS_POSIX.h>	
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(hd_video_liveview, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

#define DEBUG_MENU 		1


///////////////////////////////////////////////////////////////////////////////

//header
#define DBGINFO_BUFSIZE()	(0x200)

//RAW
#define VDO_RAW_BUFSIZE(w, h, pxlfmt)   (ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NRX: RAW compress: Only support 12bit mode
#define RAW_COMPRESS_RATIO ((7/12)*100)
#define VDO_NRX_BUFSIZE(w, h)           (ALIGN_CEIL_4(ALIGN_CEIL_64(w) * 12 / 8 * RAW_COMPRESS_RATIO / 100 * (h)))
//CA for AWB
#define VDO_CA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 3) << 1)
//LA for AE
#define VDO_LA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 1) << 1)

//YUV
#define VDO_YUV_BUFSIZE(w, h, pxlfmt)	(ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NVX: YUV compress
#define YUV_COMPRESS_RATIO 75
#define VDO_NVX_BUFSIZE(w, h, pxlfmt)	(VDO_YUV_BUFSIZE(w, h, pxlfmt) * YUV_COMPRESS_RATIO / 100)
 
///////////////////////////////////////////////////////////////////////////////

#define SEN_OUT_FMT			HD_VIDEO_PXLFMT_RAW12
#define CAP_OUT_FMT			HD_VIDEO_PXLFMT_RAW12
#define CA_WIN_NUM_W		32
#define CA_WIN_NUM_H		32
#define LA_WIN_NUM_W		32
#define LA_WIN_NUM_H		32
#define VA_WIN_NUM_W		16
#define VA_WIN_NUM_H		16
#define YOUT_WIN_NUM_W		128
#define YOUT_WIN_NUM_H		128
#define ETH_8BIT_SEL		0 //0: 2bit out, 1:8 bit out
#define ETH_OUT_SEL			1 //0: full, 1: subsample 1/2

#define VDO_SIZE_W			1920
#define VDO_SIZE_H			1080

// nn
#define	NET_NUM				2	// 1 or 2

#define	VDO_FRAME_FORMAT	HD_VIDEO_PXLFMT_YUV420

#define MBYTE				(1024 * 1024)
#define NN_TOTAL_MEM_SIZE	(200 * MBYTE)
#define VENDOR_AI_CFG  		0x000f0000  //ai project config

#define MAX_FRAME_WIDTH		1280
#define MAX_FRAME_HEIGHT	720
#define MAX_OBJ_NUM			1024
#define YUV_OUT_BUF_SIZE	(3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT)


static CHAR model_name[NET_NUM][256] = { "para/nvt_model.bin" , "para/nvt_model1.bin" };
static UINT32 mem_size[NET_NUM]	  	 = { 30*MBYTE , 30*MBYTE };
static CHAR g_label_name[] = "accuracy/labels.txt";


typedef struct _VIDEO_LIVEVIEW {
	// (1)
	HD_VIDEOCAP_SYSCAPS cap_syscaps;
	HD_PATH_ID cap_ctrl;
	HD_PATH_ID cap_path;

	HD_DIM  cap_dim;
	HD_DIM  proc_max_dim;

	// (2)
	HD_VIDEOPROC_SYSCAPS proc_syscaps;
	HD_PATH_ID proc_ctrl;
	HD_PATH_ID proc_path;

	HD_DIM  out_max_dim;
	HD_DIM  out_dim;

	// nn
	HD_PATH_ID proc_ctrl_alg;
	HD_PATH_ID proc_path_alg;
	HD_DIM	proc_dim_alg;
	HD_DIM	proc_max_dim_alg;

	// (3)
	HD_VIDEOOUT_SYSCAPS out_syscaps;
	HD_PATH_ID out_ctrl;
	HD_PATH_ID out_path;

    HD_VIDEOOUT_HDMI_ID hdmi_id;

	//
	UINT32 proc_exit;
	pthread_t thread_id;
} VIDEO_LIVEVIEW;

typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	VENDOR_AIS_IMG_PARM	src_img;
	VENDOR_AIS_FLOW_MEM_PARM mem;
	UINT32 run_id;
	VIDEO_LIVEVIEW *stream;
} VENDOR_AIS_NETWORK_PARM;


static VENDOR_AIS_FLOW_MEM_PARM g_mem = {0};
static HD_COMMON_MEM_VB_BLK g_blk_info[2];
static HD_VIDEO_FRAME yuv_out_buffer;
static VOID *yuv_out_buffer_va = NULL;
#if DYNAMIC_LABEL
static CHAR* g_class_labels[NET_NUM] = {NULL};
#else
static CHAR g_class_labels[MAX_CLASS_NUM*VENDOR_AIS_LBL_LEN];
#endif
static BOOL show_net_results[2] = { 0, 0 };


///////////////////////////////////////////////////////////////////////////////


static HD_RESULT mem_init(void)
{
	HD_RESULT              		ret;
	HD_COMMON_MEM_INIT_CONFIG 	mem_cfg = { 0 };
	HD_COMMON_MEM_DDR_ID		ddr_id = DDR_ID0;

	HD_COMMON_MEM_DDR_ID	  	ddr_id_nn = DDR_ID1;
	HD_COMMON_MEM_VB_BLK      	blk;
	UINT32                    	pa, va;

	// config common pool (cap)
	mem_cfg.pool_info[0].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[0].blk_size = DBGINFO_BUFSIZE() + VDO_RAW_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, CAP_OUT_FMT)
        											  + VDO_CA_BUF_SIZE(CA_WIN_NUM_W, CA_WIN_NUM_H)
        											  + VDO_LA_BUF_SIZE(LA_WIN_NUM_W, LA_WIN_NUM_H);
	mem_cfg.pool_info[0].blk_cnt = 2;
	mem_cfg.pool_info[0].ddr_id = ddr_id;
	
	// config common pool (main)
	mem_cfg.pool_info[1].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[1].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
	mem_cfg.pool_info[1].blk_cnt = 3;
	mem_cfg.pool_info[1].ddr_id = ddr_id;

	// config common pool (alg)
	mem_cfg.pool_info[2].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[2].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, VDO_FRAME_FORMAT);
	mem_cfg.pool_info[2].blk_cnt = 3;
	mem_cfg.pool_info[2].ddr_id = ddr_id;

	// nn mem pool ---
	mem_cfg.pool_info[3].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[3].blk_size = NN_TOTAL_MEM_SIZE;
	mem_cfg.pool_info[3].blk_cnt = 1;
	mem_cfg.pool_info[3].ddr_id = ddr_id_nn;
	mem_cfg.pool_info[4].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[4].blk_size = YUV_OUT_BUF_SIZE;
	mem_cfg.pool_info[4].blk_cnt = 1;
	mem_cfg.pool_info[4].ddr_id = ddr_id_nn;
	// - end nn mem pool	

	ret = hd_common_mem_init(&mem_cfg);

	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, NN_TOTAL_MEM_SIZE, ddr_id_nn);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail, 1\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		printf("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, NN_TOTAL_MEM_SIZE);
	g_blk_info[0] = blk;

	// Release buffer 
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = NN_TOTAL_MEM_SIZE;

	// Allocate scale out buffer 
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, YUV_OUT_BUF_SIZE, ddr_id_nn);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail, 2\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	yuv_out_buffer.phy_addr[0] = hd_common_mem_blk2pa(blk);
	if (yuv_out_buffer.phy_addr[0] == 0) {
		printf("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			printf("yuv_out_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	yuv_out_buffer_va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
						  yuv_out_buffer.phy_addr[0],
						  YUV_OUT_BUF_SIZE);
	g_blk_info[1] = blk;
	printf("Allocate yuv_out_buffer pa(%#lx) va(%p)\n", yuv_out_buffer.phy_addr[0], yuv_out_buffer_va);

exit:
	
	return ret;
}

static HD_RESULT mem_exit(void)
{
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		printf("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release scale out buffer */
	if (yuv_out_buffer_va) {
		ret = hd_common_mem_munmap(yuv_out_buffer_va, YUV_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (yuv_out_buffer_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	ret = hd_common_mem_release_block(g_blk_info[1]);
	if (ret != HD_OK) {
		printf("mem_uninit : (yuv_out_buffer.phy_addr[0])hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	return hd_common_mem_uninit();
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT get_cap_caps(HD_PATH_ID video_cap_ctrl, HD_VIDEOCAP_SYSCAPS *p_video_cap_syscaps)
{
	HD_RESULT ret = HD_OK;
	hd_videocap_get(video_cap_ctrl, HD_VIDEOCAP_PARAM_SYSCAPS, p_video_cap_syscaps);
	return ret;
}

static HD_RESULT get_cap_sysinfo(HD_PATH_ID video_cap_ctrl)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOCAP_SYSINFO sys_info = {0};

	hd_videocap_get(video_cap_ctrl, HD_VIDEOCAP_PARAM_SYSINFO, &sys_info);
	printf("sys_info.devid =0x%X, cur_fps[0]=%d/%d, vd_count=%llu, output_started=%d, cur_dim(%dx%d)\r\n",
		sys_info.dev_id, GET_HI_UINT16(sys_info.cur_fps[0]), GET_LO_UINT16(sys_info.cur_fps[0]), sys_info.vd_count, sys_info.output_started, sys_info.cur_dim.w, sys_info.cur_dim.h);
	return ret;
}

static HD_RESULT set_cap_cfg(HD_PATH_ID *p_video_cap_ctrl)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOCAP_DRV_CONFIG cap_cfg = {0};
	HD_PATH_ID video_cap_ctrl = 0;
	HD_VIDEOCAP_CTRL iq_ctl = {0};
#if 1
	snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, "nvt_sen_imx290");
	printf("sen_imx290 MIPI Interface\n");
	cap_cfg.sen_cfg.sen_dev.if_type = HD_COMMON_VIDEO_IN_MIPI_CSI;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.serial_if_pinmux = 0xF01;//PIN_MIPI_LVDS_CFG_CLK2 | PIN_MIPI_LVDS_CFG_DAT0|PIN_MIPI_LVDS_CFG_DAT1 | PIN_MIPI_LVDS_CFG_DAT2 | PIN_MIPI_LVDS_CFG_DAT3
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = 0x10;//PIN_I2C_CFG_CH2
	cap_cfg.sen_cfg.sen_dev.pin_cfg.clk_lane_sel = HD_VIDEOCAP_SEN_CLANE_SEL_CSI0_USE_C0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[0] = 0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[1] = 1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = 2;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = 3;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[4] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[5] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[6] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[7] = HD_VIDEOCAP_SEN_IGNORE;
#else
    snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, "nvt_sen_ar0237ir");
    printf("sen_ar0237ir Parallel Interface\n");
	cap_cfg.sen_cfg.sen_dev.if_type = HD_COMMON_VIDEO_IN_P_RAW;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  0x204; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = 0x10;//PIN_I2C_CFG_CH2
#endif
	ret = hd_videocap_open(0, HD_VIDEOCAP_0_CTRL, &video_cap_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_DRV_CONFIG, &cap_cfg);
	iq_ctl.func = HD_VIDEOCAP_FUNC_AE | HD_VIDEOCAP_FUNC_AWB;
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_CTRL, &iq_ctl);

	*p_video_cap_ctrl = video_cap_ctrl;
	return ret;
}

static HD_RESULT set_cap_param(HD_PATH_ID video_cap_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	{//select sensor mode, manually or automatically
		HD_VIDEOCAP_IN video_in_param = {0};

		video_in_param.sen_mode = HD_VIDEOCAP_SEN_MODE_AUTO; //auto select sensor mode by the parameter of HD_VIDEOCAP_PARAM_OUT
		video_in_param.frc = HD_VIDEO_FRC_RATIO(30,1);
		video_in_param.dim.w = p_dim->w;
		video_in_param.dim.h = p_dim->h;
		video_in_param.pxlfmt = SEN_OUT_FMT;
		video_in_param.out_frame_num = HD_VIDEOCAP_SEN_FRAME_NUM_1;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN, &video_in_param);
		//printf("set_cap_param MODE=%d\r\n", ret);
		if (ret != HD_OK) {
			return ret;
		}
	}
	#if 1 //no crop, full frame
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_OFF;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN_CROP, &video_crop_param);
		//printf("set_cap_param CROP NONE=%d\r\n", ret);
	}
	#else //HD_CROP_ON
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_ON;
		video_crop_param.win.rect.x = 0;
		video_crop_param.win.rect.y = 0;
		video_crop_param.win.rect.w = 1920/2;
		video_crop_param.win.rect.h= 1080/2;
		video_crop_param.align.w = 4;
		video_crop_param.align.h = 4;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN_CROP, &video_crop_param);
		//printf("set_cap_param CROP ON=%d\r\n", ret);
	}
	#endif
	{
		HD_VIDEOCAP_OUT video_out_param = {0};

		//without setting dim for no scaling, using original sensor out size
		video_out_param.pxlfmt = CAP_OUT_FMT;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_OUT, &video_out_param);
		//printf("set_cap_param OUT=%d\r\n", ret);
	}

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_proc_cfg(HD_PATH_ID *p_video_proc_ctrl, HD_DIM* p_max_dim)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOPROC_DEV_CONFIG video_cfg_param = {0};
	HD_VIDEOPROC_CTRL video_ctrl_param = {0};
	HD_PATH_ID video_proc_ctrl = 0;

	ret = hd_videoproc_open(0, HD_VIDEOPROC_0_CTRL, &video_proc_ctrl); //open this for device control
	if (ret != HD_OK)
		return ret;

	if (p_max_dim != NULL ) {
		video_cfg_param.pipe = HD_VIDEOPROC_PIPE_RAWALL;
		video_cfg_param.isp_id = 0;
		video_cfg_param.ctrl_max.func = 0;
		video_cfg_param.in_max.func = 0;
		video_cfg_param.in_max.dim.w = p_max_dim->w;
		video_cfg_param.in_max.dim.h = p_max_dim->h;
		video_cfg_param.in_max.pxlfmt = CAP_OUT_FMT;
		video_cfg_param.in_max.frc = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_DEV_CONFIG, &video_cfg_param);
		if (ret != HD_OK) {
			return HD_ERR_NG;
		}
	}

	video_ctrl_param.func = 0;
	ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_CTRL, &video_ctrl_param);

	*p_video_proc_ctrl = video_proc_ctrl;

	return ret;
}

static HD_RESULT set_proc_param(HD_PATH_ID video_proc_path, HD_DIM* p_dim)
{
	HD_RESULT ret = HD_OK;

	if (p_dim != NULL) { //if videoproc is already binding to dest module, not require to setting this!
		HD_VIDEOPROC_OUT video_out_param = {0};
		video_out_param.func = 0;
		video_out_param.dim.w = p_dim->w;
		video_out_param.dim.h = p_dim->h;
		video_out_param.pxlfmt = HD_VIDEO_PXLFMT_YUV420;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		video_out_param.frc = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoproc_set(video_proc_path, HD_VIDEOPROC_PARAM_OUT, &video_out_param);
	}

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_out_cfg(HD_PATH_ID *p_video_out_ctrl, UINT32 out_type, HD_VIDEOOUT_HDMI_ID hdmi_id)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOOUT_MODE videoout_mode = {0};
	HD_PATH_ID video_out_ctrl = 0;

	ret = hd_videoout_open(0, HD_VIDEOOUT_0_CTRL, &video_out_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}

	printf("out_type=%d\r\n", out_type);

	#if 1
	videoout_mode.output_type = HD_COMMON_VIDEO_OUT_LCD;
	videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
	videoout_mode.output_mode.lcd = HD_VIDEOOUT_LCD_0;
	if (out_type != 1) {
		printf("520 only support LCD\r\n");
	}
	#else
	switch(out_type){
	case 0:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_CVBS;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.cvbs= HD_VIDEOOUT_CVBS_NTSC;
	break;
	case 1:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_LCD;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.lcd = HD_VIDEOOUT_LCD_0;
	break;
	case 2:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_HDMI;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.hdmi= hdmi_id;
	break;
	default:
		printf("not support out_type\r\n");
	break;
	}
	#endif
	ret = hd_videoout_set(video_out_ctrl, HD_VIDEOOUT_PARAM_MODE, &videoout_mode);

	*p_video_out_ctrl=video_out_ctrl ;
	return ret;
}

static HD_RESULT get_out_caps(HD_PATH_ID video_out_ctrl,HD_VIDEOOUT_SYSCAPS *p_video_out_syscaps)
{
	HD_RESULT ret = HD_OK;
    HD_DEVCOUNT video_out_dev = {0};

	ret = hd_videoout_get(video_out_ctrl, HD_VIDEOOUT_PARAM_DEVCOUNT, &video_out_dev);
	if (ret != HD_OK) {
		return ret;
	}
	printf("##devcount %d\r\n", video_out_dev.max_dev_count);

	ret = hd_videoout_get(video_out_ctrl, HD_VIDEOOUT_PARAM_SYSCAPS, p_video_out_syscaps);
	if (ret != HD_OK) {
		return ret;
	}
	return ret;
}

static HD_RESULT set_out_param(HD_PATH_ID video_out_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOOUT_IN video_out_param={0};

	video_out_param.dim.w = p_dim->w;
	video_out_param.dim.h = p_dim->h;
	video_out_param.pxlfmt = HD_VIDEO_PXLFMT_YUV420;
	video_out_param.dir = HD_VIDEO_DIR_NONE;
	ret = hd_videoout_set(video_out_path, HD_VIDEOOUT_PARAM_IN, &video_out_param);
	if (ret != HD_OK) {
		return ret;
	}
	memset((void *)&video_out_param,0,sizeof(HD_VIDEOOUT_IN));
	ret = hd_videoout_get(video_out_path, HD_VIDEOOUT_PARAM_IN, &video_out_param);
	if (ret != HD_OK) {
		return ret;
	}
	printf("##video_out_param w:%d,h:%d %x %x\r\n", video_out_param.dim.w, video_out_param.dim.h, video_out_param.pxlfmt, video_out_param.dir);

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT init_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_init()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_init()) != HD_OK)
		return ret;
	if ((ret = hd_videoout_init()) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT open_module(VIDEO_LIVEVIEW *p_stream, HD_DIM* p_proc_max_dim, UINT32 out_type)
{
	HD_RESULT ret;
	// set videocap config
	ret = set_cap_cfg(&p_stream->cap_ctrl);
	if (ret != HD_OK) {
		printf("set cap-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set videoproc config
	ret = set_proc_cfg(&p_stream->proc_ctrl, p_proc_max_dim);
	if (ret != HD_OK) {
		printf("set proc-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	ret = set_proc_cfg(&p_stream->proc_ctrl_alg, p_proc_max_dim);
	if (ret != HD_OK) {
		printf("set proc-cfg alg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set videoout config
	ret = set_out_cfg(&p_stream->out_ctrl, out_type, p_stream->hdmi_id);
	if (ret != HD_OK) {
		printf("set out-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	if ((ret = hd_videocap_open(HD_VIDEOCAP_0_IN_0, HD_VIDEOCAP_0_OUT_0, &p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_0, &p_stream->proc_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_1, &p_stream->proc_path_alg)) != HD_OK)
		return ret;
	if ((ret = hd_videoout_open(HD_VIDEOOUT_0_IN_0, HD_VIDEOOUT_0_OUT_0, &p_stream->out_path)) != HD_OK)
		return ret;

	return HD_OK;
}

static HD_RESULT open_module_extend(VIDEO_LIVEVIEW *p_stream, HD_DIM* p_proc_max_dim, HD_IN_ID in_id, HD_OUT_ID out_id, UINT32 out_type)
{
	HD_RESULT ret;
	if ((ret = hd_videoproc_open(in_id, out_id, &p_stream->proc_path_alg)) != HD_OK) {
		printf("open_module_extend fail!\n\r");
		return ret;
	}
	return HD_OK;
}

static HD_RESULT set_proc_param_extend(HD_PATH_ID video_proc_path, HD_PATH_ID src_path, HD_DIM* p_dim, UINT32 dir)
{
	HD_RESULT ret = HD_OK;

	if (p_dim != NULL) { //if videoproc is already binding to dest module, not require to setting this!
		HD_VIDEOPROC_OUT_EX video_out_param = {0};
		video_out_param.src_path = src_path;
		video_out_param.dim.w = p_dim->w;
		video_out_param.dim.h = p_dim->h;
		video_out_param.pxlfmt = VDO_FRAME_FORMAT;
		video_out_param.dir = dir;
		video_out_param.depth = 1; //set 1 to allow pull

		ret = hd_videoproc_set(video_proc_path, HD_VIDEOPROC_PARAM_OUT_EX, &video_out_param);
	}

	return ret;
}

static HD_RESULT close_module(VIDEO_LIVEVIEW *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_close(p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_close(p_stream->proc_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoout_close(p_stream->out_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT exit_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoout_uninit()) != HD_OK)
		return ret;
	return HD_OK;
}

#if DYNAMIC_LABEL
HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, UINT32 label_num, const CHAR *filename)
#else
HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, const CHAR *filename)
#endif
{
	FILE *fd;
	CHAR *p_line = (CHAR *)addr;
#if DYNAMIC_LABEL
	UINT32 label_idx = 0;
#endif

	fd = fopen(filename, "r");
	if (!fd) {
		DBG_ERR("cannot read %s\r\n", filename);
		return HD_ERR_NG;
	}

	DBG_DUMP("open %s ok\r\n", filename);

	while (fgets(p_line, line_len, fd) != NULL) {
		p_line[strlen(p_line) - 1] = '\0'; // remove newline character
		p_line += line_len;
#if DYNAMIC_LABEL
		label_idx++;
		if (label_idx == label_num) {
			break;
		}
#endif
	}

	if (fd) {
		fclose(fd);
	}

	return HD_OK;
}

#if 0
static BOOL net_sample_write_file(CHAR *filepath, UINT32 addr, UINT32 size)
{
	FILE *fsave = NULL;

	fsave = fopen(filepath, "wb");
	if (fsave == NULL) {
		printf("fopen fail\n");
		return FALSE;
	}
	fwrite((UINT8 *)addr, size, 1, fsave);

	return TRUE;
}

static void *nn_thread(void *arg)
{
	UINT32 i, va;
	HD_RESULT ret;

	VENDOR_AIS_NETWORK_PARM *p_net_parm = (VENDOR_AIS_NETWORK_PARM*)arg;
	VIDEO_LIVEVIEW *stream = p_net_parm->stream;
	HD_VIDEO_FRAME video_frame = {0};

	while(stream->proc_exit==0) {
		ret = hd_videoproc_pull_out_buf(stream->proc_path_alg, &video_frame, -1);  // -1 = blocking mode, 0 = non-blocking mode, >0 = blocking-timeout mode
		if(ret != HD_OK) {
			printf("hd_videoproc_pull_out_buf fail (%d)\n\r", ret);
			return 0;
		}

		printf("\n\rVideo frame info, run_id %d\n\r", p_net_parm->run_id);
		printf("  ddr_id %d\n\r", video_frame.ddr_id);
		printf("  pxlfmt %d\n\r", video_frame.pxlfmt);
		printf("  plane-0 width %d, height %d\n\r", video_frame.dim.w, video_frame.dim.h);	
		for(i=0; i<HD_VIDEO_MAX_PLANE; i++) {
			printf("  plane %d width %d, height %d\n\r", i, video_frame.pw[i], video_frame.ph[i]);
		}

		va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, video_frame.phy_addr[0], video_frame.pw[0]*video_frame.ph[0]*3/2);

		if(p_net_parm->run_id==0) {
			net_sample_write_file("net_sample_write_yuv_net0.raw", va, video_frame.pw[0]*video_frame.ph[0]*3/2);
		} else {
			net_sample_write_file("net_sample_write_yuv_net1.raw", va, video_frame.pw[0]*video_frame.ph[0]*3/2);
		}

		ret = hd_common_mem_munmap((void *)va, video_frame.pw[0]*video_frame.ph[0]*3/2);
		if(ret != HD_OK) {
			printf("memory unmap fail!\n\r");
			return 0;
		}
		ret = hd_videoproc_release_out_buf(stream->proc_path_alg, &video_frame);
		if(ret != HD_OK) {
			printf("hd_videoproc_release_out_buf fail (%d)\n\r", ret);
			return 0;
		}
	}

	return 0;
}
#else
static void *nn_thread(void *arg)
{
	HD_RESULT ret;
	UINT32 i, model_size, req_size=0;

	VENDOR_AIS_NETWORK_PARM *p_net_parm = (VENDOR_AIS_NETWORK_PARM*)arg;
	VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem = &p_net_parm->mem;
	VENDOR_AIS_IMG_PARM src_img;
	VENDOR_AIS_RESULT_INFO *p_net_rslt;
	VENDOR_AIS_FLOW_MAP_MEM_PARM mem_manager;
	VENDOR_AIS_FLOW_MEM_PARM model_mem, rslt_mem;
	VIDEO_LIVEVIEW *stream = p_net_parm->stream;
	HD_VIDEO_FRAME video_frame = {0};
#if DYNAMIC_LABEL
	UINT32 label_num = 0;
#endif

	// allocate memory (parm, model, io buffer)
	model_size      = vendor_ais_auto_alloc_mem(p_tot_mem, &mem_manager);
	model_mem.pa    = p_tot_mem->pa;
	model_mem.va    = p_tot_mem->va;    // = load_addr
	model_mem.size  = model_size;
	rslt_mem.pa     = model_mem.pa + model_mem.size;
	rslt_mem.va     = model_mem.va + model_mem.size;
	rslt_mem.size   = sizeof(VENDOR_AIS_RESULT_INFO) + MAX_OBJ_NUM * sizeof(VENDOR_AIS_RESULT);
	req_size        = model_mem.size + rslt_mem.size;
	if (req_size > p_tot_mem->size) {
		printf("memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)req_size);
		return 0;
	}
	if (model_size > p_tot_mem->size) {
		printf("memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)model_size);
		return 0;
	}
	memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer

	// map user/kernel memory
	ret = vendor_ais_net_gen_init(mem_manager, p_net_parm->run_id);		// run -> quit (ok) -> run -> quit (exception!?)
	if (ret != HD_OK) {
		printf("net gen init fail=%d\r\n", ret);
		goto gen_init_fail;
	} else {
		printf("[ai sample] init done: %s!\n", p_net_parm->p_model_path);
	}
#if DYNAMIC_LABEL
	vendor_ais_get_label_num(&label_num, model_mem, p_net_parm->run_id);
	if (label_num > 0) {
		g_class_labels[p_net_parm->run_id] = (CHAR*)malloc(label_num*VENDOR_AIS_LBL_LEN);//alloc label buffer
		ret = ai_read_label((UINT32)g_class_labels[p_net_parm->run_id], VENDOR_AIS_LBL_LEN, label_num, g_label_name);
		if (ret != HD_OK) {
			DBG_ERR("ai_read_label fail=%d\n", ret);
			goto gen_init_fail;
		}
	}
#endif

	// nn processing
	while(stream->proc_exit==0) {
		ret = hd_videoproc_pull_out_buf(stream->proc_path_alg, &video_frame, -1);  // -1 = blocking mode, 0 = non-blocking mode, >0 = blocking-timeout mode
		if(ret != HD_OK) {
			printf("hd_videoproc_pull_out_buf fail (%d)\n\r", ret);
			return 0;
		}

		src_img.width   	= video_frame.dim.w;
		src_img.height  	= video_frame.dim.h;
		src_img.channel 	= 2;
		src_img.line_ofs	= video_frame.loff[0];
		src_img.fmt 		= VDO_FRAME_FORMAT;
		src_img.pa			= video_frame.phy_addr[0];
		src_img.va			= (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, video_frame.phy_addr[0], video_frame.pw[0]*video_frame.ph[0]*3/2);
		src_img.fmt_type    = VENDOR_COMMON_PXLFMT_YUV420_TYPE;
		
		if (vendor_ais_net_input_init(&src_img, p_net_parm->run_id) != HD_OK) {
			printf("vendor_ais_net_input_init fail (run_id %d)\n\r", p_net_parm->run_id);
			goto gen_init_fail;
		}
		
		memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
		hd_common_mem_flush_cache((VOID *)mem_manager.user_buff.va, mem_manager.user_buff.size);


#if USE_NEON
		vendor_ais_proc_net(model_mem, rslt_mem, &src_img, p_net_parm->run_id);
#else
		vendor_ais_proc_net(model_mem, rslt_mem, p_net_parm->run_id);
#endif

		p_net_rslt = vendor_ais_get_results(p_net_parm->run_id);

		// show results
		if(p_net_rslt != NULL) {
			for(i=0; i<2; i++) {
				if(show_net_results[i] && p_net_parm->run_id==i) {
					UINT32 m, n;
					printf("\r\nClassification results, net %d\r\n", p_net_parm->run_id);
					for(m=0; m < p_net_rslt->result_num; m++){
						VENDOR_AIS_RESULT *p_rslt = &p_net_rslt->p_result[m];
						for(n=0; n < TOP_N; n++) {
						#if DYNAMIC_LABEL
							printf("%ld. no=%ld, label=%s, score=%f\r\n", n + 1, p_rslt->no[n]
								, &g_class_labels[p_net_parm->run_id][p_rslt->no[n] * VENDOR_AIS_LBL_LEN], p_rslt->score[n]);
						#else
							printf("%ld. no=%ld, label=%s, score=%f\r\n", n + 1, p_rslt->no[n],
								&g_class_labels[p_rslt->no[n] * VENDOR_AIS_LBL_LEN], p_rslt->score[n]);
						#endif
						}
					}
				}
			}
		}

		// release & un-init
		ret = hd_common_mem_munmap((void *)src_img.va, video_frame.pw[0]*video_frame.ph[0]*3/2);
		if (ret != HD_OK) {
			printf("nn_thread_api : (src_img.va)hd_common_mem_munmap fail\r\n");
			goto gen_init_fail;
		}

		vendor_ais_net_input_uninit(p_net_parm->run_id);

		ret = hd_videoproc_release_out_buf(stream->proc_path_alg, &video_frame);
		if(ret != HD_OK) {
			printf("hd_videoproc_release_out_buf fail (%d)\n\r", ret);
			return 0;
		}
	}	// end while

gen_init_fail:
	ret = vendor_ais_net_gen_uninit(p_net_parm->run_id);
	if(ret != HD_OK) {
		printf("vendor_ais_net_gen_uninit fail (%d)\n\r", ret);
	}
#if DYNAMIC_LABEL
	if (g_class_labels[p_net_parm->run_id] != NULL) {
		free(g_class_labels[p_net_parm->run_id]);
		g_class_labels[p_net_parm->run_id] = NULL;
	}
#endif

	return 0;
}
#endif

static UINT32 ai_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;

	fd = fopen(p_filename, "rb");
	if (!fd) {
		printf("cannot read %s\r\n", p_filename);
		return 0;
	}

	fseek(fd, 0, SEEK_END);
	file_size = ALIGN_CEIL_4(ftell(fd));
	fseek(fd, 0, SEEK_SET);

	read_size = fread((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
		printf("size mismatch, real = %d, idea = %d, addr 0x%x\r\n", (int)read_size, (int)file_size, model_addr);
	}
	fclose(fd);
	return read_size;
}


MAIN(argc, argv)
{
	INT key;
	UINT32 out_type = 1;
	HD_RESULT ret;
	VIDEO_LIVEVIEW stream[1] = {0}; //0: main stream

	UINT32 i, pa_mark, va_mark, file_size;
	VIDEO_LIVEVIEW stream_ext[NET_NUM] = { 0 };
	VENDOR_AIS_FLOW_MEM_PARM mem_parm[NET_NUM] = {0};
	VENDOR_AIS_NETWORK_PARM net_parm[NET_NUM] = {0};

	// query program options
	if (argc >= 2) {
		out_type = atoi(argv[1]);
		printf("out_type %d\r\n", out_type);
		if(out_type > 2) {
			printf("error: not support out_type!\r\n");
			return 0;
		}
	}
    stream[0].hdmi_id=HD_VIDEOOUT_HDMI_1920X1080I60;//default
	// query program options
	if (argc >= 3 && (atoi(argv[2]) !=0)) {
		stream[0].hdmi_id = atoi(argv[2]);
		printf("hdmi_mode %d\r\n", stream[0].hdmi_id);
	}

	// init hdal
	ret = hd_common_init(0);
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
		goto exit;
	}
	//set project config for AI
	hd_common_sysconfig(0, (1<<16), 0, VENDOR_AI_CFG); //enable AI engine

	// init memory
	ret = mem_init();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
		goto exit;
	}

	// init all modules
	ret = init_module();
	if (ret != HD_OK) {
		printf("init fail=%d\n", ret);
		goto exit;
	}

	// open video_liveview modules (main)
	stream[0].proc_max_dim.w = VDO_SIZE_W; //assign by user
	stream[0].proc_max_dim.h = VDO_SIZE_H; //assign by user
	ret = open_module(&stream[0], &stream[0].proc_max_dim, out_type);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}

	// open video_liveview modules (extend)
	stream_ext[0].proc_max_dim.w = VDO_SIZE_W;
	stream_ext[0].proc_max_dim.h = VDO_SIZE_H;
	ret = open_module_extend(&stream_ext[0], &stream_ext[0].proc_max_dim, HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_5, HD_VIDEO_DIR_NONE);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}
	#if NET_NUM > 1
	stream_ext[1].proc_max_dim.w = VDO_SIZE_W;
	stream_ext[1].proc_max_dim.h = VDO_SIZE_H;
	ret = open_module_extend(&stream_ext[1], &stream_ext[1].proc_max_dim, HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_6, HD_VIDEO_DIR_NONE);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}
	#endif

	// get videocap capability
	ret = get_cap_caps(stream[0].cap_ctrl, &stream[0].cap_syscaps);
	if (ret != HD_OK) {
		printf("get cap-caps fail=%d\n", ret);
		goto exit;
	}

	// get videoout capability
	ret = get_out_caps(stream[0].out_ctrl, &stream[0].out_syscaps);
	if (ret != HD_OK) {
		printf("get out-caps fail=%d\n", ret);
		goto exit;
	}
	stream[0].out_max_dim = stream[0].out_syscaps.output_dim;

	// set videocap parameter
	stream[0].cap_dim.w = VDO_SIZE_W; //assign by user
	stream[0].cap_dim.h = VDO_SIZE_H; //assign by user
	ret = set_cap_param(stream[0].cap_path, &stream[0].cap_dim);
	if (ret != HD_OK) {
		printf("set cap fail=%d\n", ret);
		goto exit;
	}

	// set videoproc parameter (main)
	ret = set_proc_param(stream[0].proc_path, NULL);
	if (ret != HD_OK) {
		printf("set proc fail=%d\n", ret);
		goto exit;
	}

	// set videoproc parameter (alg)
	stream[0].proc_max_dim_alg.w = VDO_SIZE_W;
	stream[0].proc_max_dim_alg.h = VDO_SIZE_H;
	ret = set_proc_param(stream[0].proc_path_alg, &stream[0].proc_max_dim_alg);
	if (ret != HD_OK) {
		printf("set proc alg fail=%d\n", ret);
		goto exit;
	}

	// set videoproc parameter (nn)
	#if NET_NUM > 0
	for(i=0; i < NET_NUM; i++) {
		stream_ext[i].proc_dim_alg.w = VDO_SIZE_W;
		stream_ext[i].proc_dim_alg.h = VDO_SIZE_H;
		stream_ext[i].proc_exit = 0;
		ret = set_proc_param_extend(stream_ext[i].proc_path_alg, HD_VIDEOPROC_0_OUT_1, &stream_ext[i].proc_dim_alg, HD_VIDEO_DIR_NONE);
		if(ret != HD_OK) {
			printf("set_proc_param_extend 0 fail\n\r");
			goto exit;
		}
	}
	#endif

	// set videoout parameter (main)
	stream[0].out_dim.w = stream[0].out_max_dim.w; //using device max dim.w
	stream[0].out_dim.h = stream[0].out_max_dim.h; //using device max dim.h
	ret = set_out_param(stream[0].out_path, &stream[0].out_dim);
	if (ret != HD_OK) {
		printf("set out fail=%d\n", ret);
		goto exit;
	}

	// bind video_liveview modules (main)
	hd_videocap_bind(HD_VIDEOCAP_0_OUT_0, HD_VIDEOPROC_0_IN_0);
	hd_videoproc_bind(HD_VIDEOPROC_0_OUT_0, HD_VIDEOOUT_0_IN_0);

	// start video_liveview modules (main)
	hd_videocap_start(stream[0].cap_path);
	hd_videoproc_start(stream[0].proc_path);
	hd_videoout_start(stream[0].out_path);
	hd_videoproc_start(stream[0].proc_path_alg);
	for(i=0; i < NET_NUM; i++) {
		hd_videoproc_start(stream_ext[i].proc_path_alg);
	}

	// just wait ae/awb stable for auto-test, if don't care, user can remove it
	sleep(1);


	// nn S ------------------------------------------------
#if (DYNAMIC_LABEL == 0)
	ret = ai_read_label((UINT32)g_class_labels, VENDOR_AIS_LBL_LEN, g_label_name);
	if (ret != HD_OK) {
		DBG_ERR("ai_read_label fail=%d\n", ret);
		goto exit;
	}
#endif
	// allocate memory
	pa_mark = g_mem.pa;
	va_mark = g_mem.va;

	for(i=0; i < NET_NUM; i++) {
		mem_parm[i].pa = pa_mark;
		mem_parm[i].va = va_mark;
		mem_parm[i].size = mem_size[i];

		net_parm[i].mem = mem_parm[i];
		net_parm[i].run_id = i;
		net_parm[i].p_model_path = model_name[i];
		net_parm[i].stream = &stream_ext[i];
		
		pa_mark += mem_size[i];
		va_mark += mem_size[i];
	}

	// load model
	for(i=0; i < NET_NUM; i++) { 
		file_size = ai_load_file(net_parm[i].p_model_path, net_parm[i].mem.va);
		if (file_size == 0) {
			printf("net load file fail: %s\r\n", net_parm[i].p_model_path);
			return 0;
		}
	}

	// create threads
	for(i=0; i < NET_NUM; i++) {
		ret = pthread_create(&stream_ext[i].thread_id, NULL, nn_thread, (VOID*)(&net_parm[i]));
		if (ret < 0) {
			printf("create thread for net %d failed\n\r", i);
			goto exit;
		}
	}
	// nn E ------------------------------------------------


	// query user key
	printf("Enter q to exit\n");
	while (1) {
		key = GETCHAR();
		if (key == 'q' || key == 0x3) {
			// quit program
			#if NET_NUM > 0
			for(i=0; i < NET_NUM; i++) stream_ext[i].proc_exit = 1;
			#endif
			break;
		}

		#if (DEBUG_MENU == 1)
		if (key == 'd') {
			// enter debug menu
			hd_debug_run_menu();
			printf("\r\nEnter q to exit, Enter d to debug\r\n");
		}
		#endif

		if(key == '1') {
			show_net_results[0] = (show_net_results[0]+1) % 2;
			continue;
		}
		if(key == '2') {
			show_net_results[1] = (show_net_results[1]+1) % 2;
			continue;
		}
		
		if (key == '0') {
			get_cap_sysinfo(stream[0].cap_ctrl);
		}
	}

	for(i=0; i < NET_NUM; i++) {
		pthread_join(stream_ext[i].thread_id, NULL);
	}

	// stop video_liveview modules (main)
	hd_videocap_stop(stream[0].cap_path);
	hd_videoproc_stop(stream[0].proc_path);
	hd_videoout_stop(stream[0].out_path);
	hd_videoproc_stop(stream[0].proc_path_alg);
	for(i=0; i < NET_NUM; i++) {
		hd_videoproc_stop(stream_ext[i].proc_path_alg);
	}

	// unbind video_liveview modules (main)
	hd_videocap_unbind(HD_VIDEOCAP_0_OUT_0);
	hd_videoproc_unbind(HD_VIDEOPROC_0_OUT_0);

exit:
	// close video_liveview modules (main)
	ret = close_module(&stream[0]);
	if (ret != HD_OK) {
		printf("close fail=%d\n", ret);
	}

	// uninit all modules
	ret = exit_module();
	if (ret != HD_OK) {
		printf("exit fail=%d\n", ret);
	}

	// uninit memory
	ret = mem_exit();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
	}

	// uninit hdal
	ret = hd_common_uninit();
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
	}

	return 0;
}
