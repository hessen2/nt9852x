/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
////#include <pthread.h>
#include "hdal.h"
//#include "hd_type.h"
#include "hd_common.h"
//#include "nvtipc.h"
//#include "vendor_dsp_util.h"
//#include "vendor_ai/vendor_ai.h"

#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
//#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
//#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include "vendor_preproc/vendor_preproc.h"
//#include "net_flow_user_sample/net_layer_sample.h"
//#include "custnn/custnn_lib.h"
#include <sys/time.h>
// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME
#else
#include <FreeRTOS_POSIX.h>	
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(alg_net_app_user_sample, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME

#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080

#define YUV_OUT_BUF_SIZE        (3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT)

#define MBYTE					(1024 * 1024)
#define NN_TOTAL_MEM_SIZE       (200 * MBYTE)
#define NN_RUN_NET_NUM			2

#define MAX_OBJ_NUM             1024
#define AUTO_UPDATE_DIM     	0
#define DYNAMIC_MEM             0

#define PREPROC_TEST            1

#define VENDOR_AI_CFG  0x000f0000  //ai project config

#define GET_OUTPUT_LAYER_FEATURE_MAP DISABLE

#define PROF                    DISABLE
#if PROF
	static struct timeval tstart, tend;
	#define PROF_START()    gettimeofday(&tstart, NULL);
	#define PROF_END(msg)   gettimeofday(&tend, NULL);  \
			printf("%s time (us): %lu\r\n", msg,         \
					(tend.tv_sec - tstart.tv_sec) * 1000000 + (tend.tv_usec - tstart.tv_usec));
#else
	#define PROF_START()
	#define PROF_END(msg)
#endif
/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/**
	Parameters of network
*/
typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	VENDOR_AIS_IMG_PARM	src_img;
	VENDOR_AIS_FLOW_MEM_PARM mem;
	UINT32 run_id;
} VENDOR_AIS_NETWORK_PARM;

/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
static VENDOR_AIS_FLOW_MEM_PARM g_mem		= {0};
static HD_COMMON_MEM_VB_BLK g_blk_info[2];


#if PREPROC_TEST
static BOOL is_preproc_proc[NN_RUN_NET_NUM]	= {FALSE, FALSE};
static BOOL is_preproc_run[NN_RUN_NET_NUM]	= {FALSE, FALSE};
UINT32 preproc_mem_va = 0;
UINT32 preproc_mem_pa = 0;
UINT32 g_preproc_user_buff_va[NN_RUN_NET_NUM] = {0};
UINT32 g_preproc_user_buff_pa[NN_RUN_NET_NUM] = {0};
UINT32 g_preproc_user_parm_va[NN_RUN_NET_NUM] = {0};
UINT32 g_preproc_user_parm_pa[NN_RUN_NET_NUM] = {0};
AI_PREPROC_SIZE g_dim = {0};
#endif

//NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/
static int mem_init(void)
{
	HD_RESULT                 ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};
	UINT32                    pa, va;
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID0;
	HD_COMMON_MEM_VB_BLK      blk;
	UINT32                    total_mem_size = 0;
	UINT32                    idx = 0;

	/* Allocate parameter buffer */
	if (g_mem.va != 0) {
		printf("err: mem has already been inited\r\n");
		return -1;
	}
	
	
	total_mem_size = NN_RUN_NET_NUM * (vendor_preproc_get_mem_size() + YUV_OUT_BUF_SIZE*9);

	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = total_mem_size;
	mem_cfg.pool_info[0].blk_cnt = 1;
	mem_cfg.pool_info[0].ddr_id = DDR_ID0;

	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		printf("hd_common_mem_init err: %d\r\n", ret);
		return ret;
	}

	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, total_mem_size, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		printf("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, total_mem_size);
	g_blk_info[0] = blk;

	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_mem_size);
		if (ret != HD_OK) {
			printf("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = total_mem_size;
	
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		// parm buff for lib usage 
		g_preproc_user_parm_va[idx] = va;
		g_preproc_user_parm_pa[idx] = pa;
		// user buff for IO buffer usage
		g_preproc_user_buff_va[idx] = va + vendor_preproc_get_mem_size();
		g_preproc_user_buff_pa[idx] = pa + vendor_preproc_get_mem_size();
		va += (total_mem_size / NN_RUN_NET_NUM);
		pa += (total_mem_size / NN_RUN_NET_NUM);
	}
	

exit:
	return ret;
}

static HD_RESULT mem_uninit(void)
{
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, g_mem.size);
		if (ret != HD_OK) {
			printf("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		printf("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}




	if(g_mem.va) {
		g_mem.va = 0;
	}
	
	return hd_common_mem_uninit();
}

#if PREPROC_TEST
INT32 preproc_init_parm(AI_PREPROC_PARM* p_param, UINT32 input_addr, UINT32 output_addr, AI_PREPROC_SIZE dim, UINT32 sub_type)
{
	AI_PREPROC_IO_INFO io_info = {0};
	AI_PREPROC_TRANS_FMT_PARM fmt_trans_info = {0};
	AI_PREPROC_SUB_PARM sub_info = {0};
	
	if (p_param == NULL) {
		printf("input parameter is NULL\n");
		return -1;
	}
	printf("input addr  = 0x%08X\n", (unsigned int)input_addr);
	printf("output addr = 0x%08X\n", (unsigned int)output_addr);
	
	// init parameter
	vendor_preproc_init_parm(p_param);
	
	// set io parameter
	memset(&io_info, 0, sizeof(AI_PREPROC_IO_INFO));
	io_info.pa = input_addr;
	io_info.type = AI_PREPROC_TYPE_UINT8;
	io_info.size.width = dim.width;
	io_info.size.height = dim.height;
	io_info.size.channel = dim.channel;
	io_info.ofs.line_ofs = dim.width;
	vendor_preproc_set_io_parm(p_param, &io_info, AI_PREPROC_FMT_YUV420, 0, AI_PREPROC_CH_Y);
	
	io_info.pa = input_addr + io_info.size.width*io_info.size.height;
	vendor_preproc_set_io_parm(p_param, &io_info, AI_PREPROC_FMT_YUV420, 0, AI_PREPROC_CH_UVPACK);
	
	memset(&io_info, 0, sizeof(AI_PREPROC_IO_INFO));
	io_info.pa = output_addr;
	io_info.type = AI_PREPROC_TYPE_UINT8;
	io_info.size.width = dim.width;
	io_info.size.height = dim.height;
	io_info.size.channel = 3;
	io_info.ofs.line_ofs = dim.width;
	vendor_preproc_set_io_parm(p_param, &io_info, AI_PREPROC_FMT_RGB, 1, AI_PREPROC_CH_R);
	io_info.pa = output_addr + io_info.size.width*io_info.size.height;
	vendor_preproc_set_io_parm(p_param, &io_info, AI_PREPROC_FMT_RGB, 1, AI_PREPROC_CH_G);
	io_info.pa = output_addr + 2*io_info.size.width*io_info.size.height;
	vendor_preproc_set_io_parm(p_param, &io_info, AI_PREPROC_FMT_RGB, 1, AI_PREPROC_CH_B);
	
	// set func parameter
	fmt_trans_info.mode = AI_PREPROC_YUV2RGB;
	vendor_preproc_set_func_parm(p_param, &fmt_trans_info, AI_PREPROC_TRANS_FMT_EN);
	
	if (sub_type == 0) {
		sub_info.mode = AI_PREPROC_SUB_SCALAR;
		sub_info.sub_val[0] = 128;
		sub_info.sub_val[1] = 127;
		sub_info.sub_val[2] = 126;
	} else {
		sub_info.mode = AI_PREPROC_SUB_PLANAR;
		sub_info.sub_planar.pa = output_addr + 3*io_info.size.width*io_info.size.height;
		sub_info.sub_planar.ofs.line_ofs = dim.width*3;
		sub_info.sub_planar.size.width = dim.width;
		sub_info.sub_planar.size.height = dim.height;
	}
	vendor_preproc_set_func_parm(p_param, &sub_info, AI_PREPROC_SUB_EN);

	return 0;
}

static VOID *preproc_thread_api(VOID *arg)
{
	HD_RESULT ret;
	AI_PREPROC_PARM preproc_param = {0};
	VENDOR_AIS_NETWORK_PARM* p_input = (VENDOR_AIS_NETWORK_PARM*)arg;
	UINT32 run_id = p_input->run_id;
	UINT32 preproc_mem_va = g_preproc_user_buff_va[run_id];
	UINT32 preproc_mem_pa = g_preproc_user_buff_pa[run_id];
	UINT32 input_ofs = g_dim.width*g_dim.height*g_dim.channel;
	UINT32 input_addr = preproc_mem_va;
	UINT32 output_addr = preproc_mem_va + input_ofs;
	UINT32 input_pa = preproc_mem_pa;
	UINT32 output_pa = preproc_mem_pa + input_ofs;
	UINT32 parm_va = g_preproc_user_parm_va[run_id];
	UINT32 parm_pa = g_preproc_user_parm_pa[run_id];
	KDRV_AI_PREPROC_INFO preproc_mem = {0};
	CHAR dump_path[256] = {0};

	// init preproc param
	if (preproc_init_parm(&preproc_param, input_pa, output_pa, g_dim, run_id) < 0){
		printf("preproc parameter init fail\n");
		return 0;
	}
	
	// start thread loop
	vendor_preproc_auto_alloc_mem(parm_pa, parm_va, &preproc_mem);

	do {
		if (is_preproc_run[run_id]) {
			
			// flush input
			//PROF_START();
			hd_common_mem_flush_cache((VOID *)input_addr, input_ofs);
			hd_common_mem_flush_cache((VOID *)output_addr, 512*376*3);
			
			memset((VOID *)(output_addr + 512*376*3), 0x80808080, 512*376*3);    // clear result buffer
			hd_common_mem_flush_cache((VOID *)(output_addr + 512*376*3), 512*376*3);
			//PROF_END("nue_flush");
			// run fc
			PROF_START();
			ret = vendor_preproc_run_v2(&preproc_mem, &preproc_param, NULL);
			//ret = vendor_preproc_run(&preproc_param, NULL);
			PROF_END("preproc_inference");
			if (ret != 0) {
				printf("run preproc fail\n");
				return 0;
			}
			is_preproc_run[run_id] = FALSE;

			printf("[preproc sample] preproc %d done!\n", (int)run_id);

			hd_common_mem_flush_cache((VOID *)output_addr, 512*376*3);
			snprintf(dump_path, STR_MAX_LENGTH, "out_rgb%d/r.bin", (int)run_id);
			vendor_ais_writebin(output_addr, 512*376, dump_path);

			
			snprintf(dump_path, STR_MAX_LENGTH, "out_rgb%d/g.bin", (int)run_id);
			vendor_ais_writebin(output_addr + 512*376, 512*376, dump_path);

			
			snprintf(dump_path, STR_MAX_LENGTH, "out_rgb%d/b.bin", (int)run_id);
			vendor_ais_writebin(output_addr + 512*376*2, 512*376, dump_path);	
		}
	} while(is_preproc_proc[run_id]);
	
	return 0;
}
#endif
/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
MAIN(argc, argv)
{
	HD_RESULT           ret;
	VENDOR_AIS_IMG_PARM	src_img = {0};
	int idx = 0;
#if defined(__LINUX)
	INT32 key;
#else
	CHAR key;
#endif
#if PREPROC_TEST
	pthread_t preproc_thread_id[NN_RUN_NET_NUM];
#endif
	VENDOR_AIS_NETWORK_PARM preproc_thread_info[NN_RUN_NET_NUM] = {0};
	
	if(argc < 2){
		DBG_WRN("usage : net_app_user_sample wrong input\n");
		return -1;
	}
	
	// get images info
	idx = 2;
	if (argc > idx) {
		sscanf(argv[idx++], "%hu", &g_dim.width);
	}
	if (argc > idx) {
		sscanf(argv[idx++], "%hu", &g_dim.height);
	}
	if (argc > idx) {
		sscanf(argv[idx++], "%hu", &g_dim.channel);
	}
	if (argc > idx) {
		sscanf(argv[idx++], "%lu", &src_img.line_ofs);
	}
	if (argc > idx) {
		sscanf(argv[idx++], "%x", &src_img.fmt);
	}








	ret = hd_common_init(0);
	if (ret != HD_OK) {
		DBG_ERR("hd_common_init fail=%d\n", ret);
		goto exit;
	}
	
	ret = vendor_preproc_init();
	if (ret != 0) {
		printf("preproc init fail\n");
		return 0;
	}

	// init memory
	ret = mem_init();
	if (ret != HD_OK) {
		DBG_ERR("mem_init fail=%d\n", ret);
		goto exit;
	}

	ret = hd_videoproc_init();
	if (ret != HD_OK) {
		DBG_ERR("hd_videoproc_init fail=%d\n", ret);
		goto exit;
	}
	
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		INT32 file_len = 0;
		file_len = vendor_ais_loadbin((UINT32)g_preproc_user_buff_va[idx], argv[1]);
		if (file_len < 0) {
			goto exit;
		}
	}
	
#if PREPROC_TEST
	// create preproc thread
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		preproc_thread_info[idx].run_id = idx;
		ret = pthread_create(&preproc_thread_id[idx], NULL, preproc_thread_api, &preproc_thread_info[idx]);
		if (ret < 0) {
			DBG_ERR("create preproc thread failed");
			goto exit;
		}
		is_preproc_proc[idx] = TRUE;
		is_preproc_run[idx]  = FALSE;
	}
	
#endif

	do {
		printf("usage:\n");
		printf("  enter q: exit\n");
		printf("  enter r: run engine\n");

		key = GETCHAR();

		if (key == 'q' || key == 0x3) {
			for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
				is_preproc_run[idx]  = FALSE;
				is_preproc_proc[idx] = FALSE;
			}
			break;
		} else if (key == 'r') {
			//  run preproc
			for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
				is_preproc_run[idx] = TRUE;
			}
			printf("[ai sample] write preproc done!\n");
			continue;
		}
	} while(1);

	// wait encode thread destroyed
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		pthread_join(preproc_thread_id[idx], NULL);
	}
	
exit:

	ret = hd_videoproc_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_videoproc_uninit fail=%d\n", ret);
	}

	ret = mem_uninit();
	if (ret != HD_OK) {
		DBG_ERR("mem_uninit fail=%d\n", ret);
	}

	ret = hd_common_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_common_uninit fail=%d\n", ret);
	}
	
	ret = vendor_preproc_uninit();
	if (ret != 0) {
		printf("preproc uninit fail\n");
		return 0;
	}
	printf("preproc library test finish...\r\n");

	return ret;
}
