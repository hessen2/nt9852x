/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
////#include <pthread.h>
#include "hdal.h"
//#include "hd_type.h"
#include "hd_common.h"
//#include "nvtipc.h"
//#include "vendor_dsp_util.h"
//#include "vendor_ai/vendor_ai.h"

#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
//#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
//#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
//#include "net_flow_user_sample/net_layer_sample.h"
//#include "custnn/custnn_lib.h"
// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME
#else
#include <FreeRTOS_POSIX.h>	
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(alg_net_app_user_sample, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME

#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080

#define AI_SAMPLE_TEST_BATCH    ENABLE // v2 not support
#if AI_SAMPLE_TEST_BATCH
#define AI_SAMPLE_MAX_BATCH_NUM 8
#define AI_SAMPLE_MAX_INPUT_NUM 2
#else
#define AI_SAMPLE_MAX_BATCH_NUM 1
#define AI_SAMPLE_MAX_INPUT_NUM 1
#endif
#define YUV_SINGLE_BUF_SIZE     (3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT * AI_SAMPLE_MAX_BATCH_NUM)
#define YUV_OUT_BUF_SIZE        (YUV_SINGLE_BUF_SIZE * AI_SAMPLE_MAX_INPUT_NUM)

#define MBYTE					(1024 * 1024)
#define NN_TOTAL_MEM_SIZE       (200 * MBYTE)
#define NN_RUN_NET_NUM			2

#define MAX_OBJ_NUM             1024
#define AUTO_UPDATE_DIM     	0
#define DYNAMIC_MEM             1
#define MODIFY_ENG_IN_SAMPLE 0

#define VENDOR_AI_CFG  0x000f0000  //ai project config

#define GET_OUTPUT_LAYER_FEATURE_MAP DISABLE
#define SUPPORT_MULTI_BLOB_IN_SAMPLE      1
#define AI_TEST_DYNAMIC_BATCH   0
/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/**
	Parameters of network
*/
typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	UINT32 src_img_num;
	VENDOR_AIS_IMG_PARM	src_img[AI_SAMPLE_MAX_INPUT_NUM];
	VENDOR_AIS_FLOW_MEM_PARM mem;
	UINT32 run_id;
} VENDOR_AIS_NETWORK_PARM;

/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
#if defined(__FREERTOS)
static CHAR model_name[NN_RUN_NET_NUM][256]	= { "A:\\para\\nvt_model.bin", "A:\\para\\nvt_model1.bin"};
#else
static CHAR model_name[NN_RUN_NET_NUM][256]	= { "para/nvt_model.bin", "para/nvt_model1.bin"};
#endif
static UINT32 mem_size[NN_RUN_NET_NUM]		= { 30 * MBYTE, 30 * MBYTE };
static VENDOR_AIS_FLOW_MEM_PARM g_mem		= {0};
static BOOL is_net_proc						= TRUE;
static BOOL is_net_run[NN_SUPPORT_NET_MAX]	= {FALSE};
#if DYNAMIC_LABEL
static CHAR* g_class_labels[NN_RUN_NET_NUM] = {NULL};
#else
static CHAR g_class_labels[MAX_CLASS_NUM * VENDOR_AIS_LBL_LEN];   //[MAX_CLASS_NUM * VENDOR_AIS_LBL_LEN];
#endif
static HD_VIDEO_FRAME yuv_out_buffer;
static VOID *yuv_out_buffer_va = NULL;
static HD_COMMON_MEM_VB_BLK g_blk_info[2];
#if AUTO_UPDATE_DIM
UINT32 diff_model_size = 0;
#if defined(__FREERTOS)
static CHAR diff_model_name[NN_RUN_NET_NUM][256]	= { "A:\\para\\nvt_stripe_model.bin", "A:\\para\\nvt_stripe_model1.bin" };
#else
static CHAR diff_model_name[NN_RUN_NET_NUM][256]	= { "para/nvt_stripe_model.bin", "para/nvt_stripe_model1.bin" };
#endif
#endif

//NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/
#if DYNAMIC_LABEL
HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, UINT32 label_num, const CHAR *filename)
#else
HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, const CHAR *filename)
#endif
{
	FILE *fd;
	CHAR *p_line = (CHAR *)addr;
#if DYNAMIC_LABEL
	UINT32 label_idx = 0;
#endif

	fd = fopen(filename, "r");
	if (!fd) {
		DBG_ERR("cannot read %s\r\n", filename);
		return HD_ERR_NG;
	}

	printf("open %s ok\r\n", filename);

	while (fgets(p_line, line_len, fd) != NULL) {
		p_line[strlen(p_line) - 1] = '\0'; // remove newline character
		p_line += line_len;
#if DYNAMIC_LABEL
		label_idx++;
		if (label_idx == label_num) {
			break;
		}
#endif
	}

	if (fd) {
		fclose(fd);
	}

	return HD_OK;
}

static int mem_init(void)
{
	HD_RESULT                 ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};
	UINT32                    pa, va;
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID0;
	HD_COMMON_MEM_VB_BLK      blk;
	UINT32                    total_mem_size = 0;
#if DYNAMIC_MEM
	UINT32                    i = 0;
#endif	

	/* Allocate parameter buffer */
	if (g_mem.va != 0) {
		printf("err: mem has already been inited\r\n");
		return -1;
	}
	
#if DYNAMIC_MEM
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];	
	}
#else	
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif

	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = total_mem_size;
	mem_cfg.pool_info[0].blk_cnt = 1;
	mem_cfg.pool_info[0].ddr_id = DDR_ID0;
	mem_cfg.pool_info[1].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[1].blk_size = YUV_OUT_BUF_SIZE;
	mem_cfg.pool_info[1].blk_cnt = 1;
	mem_cfg.pool_info[1].ddr_id = DDR_ID0;

	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		printf("hd_common_mem_init err: %d\r\n", ret);
		return ret;
	}

	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, total_mem_size, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		printf("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, total_mem_size);
	g_blk_info[0] = blk;

	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_mem_size);
		if (ret != HD_OK) {
			printf("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = total_mem_size;

	/* Allocate scale out buffer */
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, YUV_OUT_BUF_SIZE, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	yuv_out_buffer.phy_addr[0] = hd_common_mem_blk2pa(blk);
	if (yuv_out_buffer.phy_addr[0] == 0) {
		printf("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			printf("yuv_out_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	yuv_out_buffer_va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
						  yuv_out_buffer.phy_addr[0],
						  YUV_OUT_BUF_SIZE);
	g_blk_info[1] = blk;
	printf("Allocate yuv_out_buffer pa(%#lx) va(%p)\n",
			yuv_out_buffer.phy_addr[0], yuv_out_buffer_va);



exit:
	return ret;
}

static HD_RESULT mem_uninit(void)
{
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, g_mem.size);
		if (ret != HD_OK) {
			printf("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		printf("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release scale out buffer */
	if (yuv_out_buffer_va) {
		ret = hd_common_mem_munmap(yuv_out_buffer_va, YUV_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (yuv_out_buffer_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)yuv_out_buffer.phy_addr[0]);
	ret = hd_common_mem_release_block(g_blk_info[1]);
	if (ret != HD_OK) {
		printf("mem_uninit : (yuv_out_buffer.phy_addr[0])hd_common_mem_release_block fail.\r\n");
		return ret;
	}
	if(g_mem.va) {
		g_mem.va = 0;
	}

	
	return hd_common_mem_uninit();
}

static HD_RESULT ai_load_img(CHAR *filename, VENDOR_AIS_IMG_PARM *p_img, UINT32 img_ofs_idx)
{
	INT32 file_len;
	UINT32 key = 0;

	file_len = vendor_ais_loadbin((UINT32)((UINT32)yuv_out_buffer_va + img_ofs_idx*YUV_SINGLE_BUF_SIZE), filename);
	if (file_len < 0) {
		return HD_ERR_IO;
	}

#if AI_SUPPORT_MULTI_FMT
	p_img->fmt_type = 0;
#endif

	if (p_img->width == 0) {
		while (TRUE) {
			printf("Enter width = ?\r\n");
			if (scanf("%hu", &p_img->width) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->height == 0) {
		while (TRUE) {
			printf("Enter height = ?\r\n");
			if (scanf("%hu", &p_img->height) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->channel == 0) {
		while (TRUE) {
			printf("Enter channel = ?\r\n");
			if (scanf("%hu", &p_img->channel) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->line_ofs == 0) {
		while (TRUE) {
			printf("Enter line offset = ?\r\n");
			if (scanf("%lu", &p_img->line_ofs) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->fmt == 0) {
		while (TRUE) {
			printf("Enter image format = ?\r\n");
			printf("[%d] RGB888 (0x%08x)\r\n", NET_IMG_RGB_PLANE, HD_VIDEO_PXLFMT_RGB888_PLANAR);
			printf("[%d] YUV420 (0x%08x)\r\n", NET_IMG_YUV420, HD_VIDEO_PXLFMT_YUV420);
			printf("[%d] YUV422 (0x%08x)\r\n", NET_IMG_YUV422, HD_VIDEO_PXLFMT_YUV422);
			printf("[%d] Y only (0x%08x)\r\n", NET_IMG_YONLY, HD_VIDEO_PXLFMT_Y8);
#if AI_SUPPORT_MULTI_FMT
			printf("[%d] YUV420_NV21 (0x%08x)\r\n", NET_IMG_YUV420_NV21, HD_VIDEO_PXLFMT_YUV420+1);
#endif

			if (scanf("%lu", &key) != 1) {
				printf("Wrong input\n");
				continue;
			} else if (key == NET_IMG_YUV420) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
			} else if (key == NET_IMG_YUV422) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV422;
			} else if (key == NET_IMG_RGB_PLANE) {
				p_img->fmt = HD_VIDEO_PXLFMT_RGB888_PLANAR;
			} else if (key == NET_IMG_YONLY) {
				p_img->fmt = HD_VIDEO_PXLFMT_Y8;
#if AI_SUPPORT_MULTI_FMT
			} else if (key == NET_IMG_YUV420_NV21) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
				p_img->fmt_type = 1;
#endif
			} else {
				printf("Wrong enter format\n");
				continue;
			}
			break;
		}
#if AI_SUPPORT_MULTI_FMT
	} else if (p_img->fmt == HD_VIDEO_PXLFMT_YUV420 + 1) {
		p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
		p_img->fmt_type = 1;
#endif
	}
	p_img->pa = yuv_out_buffer.phy_addr[0] + img_ofs_idx*YUV_SINGLE_BUF_SIZE;
	p_img->va = (UINT32)((UINT32)yuv_out_buffer_va + img_ofs_idx*YUV_SINGLE_BUF_SIZE);
	return HD_OK;
}

static VOID *nn_thread_api(VOID *arg)
{
	HD_RESULT ret;
	VENDOR_AIS_NETWORK_PARM *p_net_parm = (VENDOR_AIS_NETWORK_PARM*)arg;
	VENDOR_AIS_IMG_PARM	*p_src_img = p_net_parm->src_img;
	VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem = &p_net_parm->mem;
	UINT32 input_img_num = p_net_parm->src_img_num;
	UINT32 run_id = p_net_parm->run_id;
	UINT32 file_size = 0, model_size = 0;
	UINT32 load_addr = p_tot_mem->va;
	VENDOR_AIS_FLOW_MAP_MEM_PARM mem_manager;
	//NN_IOMEM first_iomem;
	UINT32 i = 0;
	VENDOR_AIS_FLOW_MEM_PARM rslt_mem;
	VENDOR_AIS_FLOW_MEM_PARM model_mem;
	UINT32 req_size = 0;
	VENDOR_AIS_RESULT_INFO *p_net_rslt;
	VENDOR_AIS_RESULT *p_rslt;
	UINT32 net_id = run_id;
#if MODIFY_ENG_IN_SAMPLE
	UINT8 eng_update_info[100] = {0};
#endif
#if AUTO_UPDATE_DIM
	VENDOR_AIS_DIFF_MODEL_INFO diff_info = {0};
	VENDOR_AIS_FLOW_MEM_PARM diff_model_mem = {0};
#endif
#if DYNAMIC_LABEL
	UINT32 label_num = 0;
#endif
#if GET_OUTPUT_LAYER_FEATURE_MAP
	UINT32 output_layer1_indx = -1;
	NN_IOMEM output_layer_iomem = {0};
	FLOAT *output_layer_tran_float = NULL;
	NN_LAYER_OUTPUT_INFO *output_layer = NULL;
#endif
	NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
	UINT32 num_output_layer = 0;
#if !CNN_25_MATLAB
	NN_IN_OUT_FMT *input_info = NULL;
#endif
#if AI_TEST_DYNAMIC_BATCH
	VENDOR_AIS_DIFF_BATCH_MODEL_INFO diff_batch_info = {0};
#endif

	if (p_net_parm->p_model_path == NULL) {
		printf("input network model is null\r\n");
		return 0;
	}

	//load file
	//file_size = ai_load_file(p_net_parm->p_model_path, load_addr);
	file_size = vendor_ais_load_file(p_net_parm->p_model_path, load_addr, &output_layer_info, &num_output_layer);
	if (file_size == 0) {
		printf("net load file fail: %s\r\n", p_net_parm->p_model_path);
		return 0;
	}

#if GET_OUTPUT_LAYER_FEATURE_MAP
	vendor_ais_find_layer_info_by_name("conv23", output_layer_info, &output_layer, num_output_layer);
	if (output_layer) {
		output_layer1_indx = output_layer->fusion_layer_index;
		output_layer_tran_float = (FLOAT*)malloc(sizeof(FLOAT)*output_layer->out_width*output_layer->out_height*output_layer->out_channel);	
	} else {
		printf("can't find layer '%s'\r\n", "conv23");
	}
#endif

	//allocate memory (parm, model, io buffer)
	model_size      = vendor_ais_auto_alloc_mem(p_tot_mem, &mem_manager);
	model_mem.pa    = p_tot_mem->pa;
	model_mem.va    = p_tot_mem->va;    // = load_addr
	model_mem.size  = model_size;
	rslt_mem.pa     = model_mem.pa + model_mem.size;
	rslt_mem.va     = model_mem.va + model_mem.size;
	rslt_mem.size   = sizeof(VENDOR_AIS_RESULT_INFO) + MAX_OBJ_NUM * sizeof(VENDOR_AIS_RESULT);
	req_size        = model_mem.size + rslt_mem.size;
	if (model_size > p_tot_mem->size) {
		printf("model memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)model_size);
		if (output_layer_info != NULL) {
			free(output_layer_info);
		}
		return 0;
	}
	if (req_size > p_tot_mem->size) {
		printf("require memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)req_size);
		if (output_layer_info != NULL) {
			free(output_layer_info);
		}
		return 0;
	}
	
	memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
	memset((VOID *)rslt_mem.va, 0, rslt_mem.size);    // clear result buffer
	
	// map user/kernel memory
	ret = vendor_ais_net_gen_init(mem_manager, net_id);
	if (ret != HD_OK) {
		printf("net gen init fail=%d\r\n", ret);
		goto gen_init_fail;
	} else {
		printf("[ai sample] init done: %s!\n", p_net_parm->p_model_path);
	}
	
#if !CNN_25_MATLAB
	ret = vendor_ais_get_net_input_info(&input_info, model_mem);
	if (ret == 0) {
		printf("model_fmt = %s\n", input_info->model_fmt);
		printf("model_width = %d\n", input_info->model_width);
		printf("model_height = %d\n", input_info->model_height);
		printf("model_channel = %d\n", input_info->model_channel);
		printf("model_batch = %d\n", input_info->model_batch);
		printf("in_fmt = %s\n", input_info->in_fmt);
		printf("in_channel = %d\n", input_info->in_channel);
	}
#endif

#if DYNAMIC_LABEL
	vendor_ais_get_label_num(&label_num, model_mem, net_id);
	if (label_num > 0) {
		g_class_labels[net_id] = (CHAR*)malloc(label_num*VENDOR_AIS_LBL_LEN);//alloc label buffer
#if defined(__FREERTOS)
		ret = ai_read_label((UINT32)g_class_labels[net_id], VENDOR_AIS_LBL_LEN, label_num, "A:\\accuracy\\labels.txt");
#else
		ret = ai_read_label((UINT32)g_class_labels[net_id], VENDOR_AIS_LBL_LEN, label_num, "accuracy/labels.txt");
#endif
		if (ret != HD_OK) {
			DBG_ERR("ai_read_label fail=%d\n", ret);
			goto gen_init_fail;
		}
	}
#endif
	
#if MODIFY_ENG_IN_SAMPLE
	// parse engine id for all cnn layer
	vendor_ais_pars_engid(1, net_id);
	// parse engine id per layer
	for(i=0; i<100; i++) {
		eng_update_info[i] = i % 2;
	}
	vendor_ais_pars_engid_perlayer((UINT32)eng_update_info, net_id);
#endif
#if AUTO_UPDATE_DIM
	diff_info.input_width = p_src_img[0].width;
	diff_info.input_height = p_src_img[0].height;
	diff_info.id = 0;
	diff_model_mem.pa = rslt_mem.pa + rslt_mem.size;
	diff_model_mem.va = rslt_mem.va + rslt_mem.size;
	diff_model_mem.size = diff_model_size;
	file_size = vendor_ais_load_file(diff_model_name[run_id], diff_model_mem.va, NULL, &num_output_layer);
	if (file_size == 0) {
		printf("net load file fail: %s\r\n", diff_model_name[run_id]);
#if CNN_25_MATLAB
		if (output_layer_info != NULL) {
			free(output_layer_info);
		}
#endif
		return 0;
	}
	ret = vendor_ais_pars_diff_mem(&mem_manager, &diff_model_mem, &diff_info, net_id);
	if (ret != HD_OK) {
		printf("vendor_ais_pars_diff_mem (%d)\n\r", ret);
#if CNN_25_MATLAB
		if (output_layer_info != NULL) {
			free(output_layer_info);
		}
#endif
		return 0;
	}
#endif
#if AI_TEST_DYNAMIC_BATCH
	{
		UINT32* p_batch_id = NULL;
		UINT32 max_batch_id_num = 0;
		UINT32 max_batch_num = 0;
		// display usable batch id
		vendor_ais_get_diff_batch_id_num(&mem_manager, &max_batch_id_num);
		printf("usable switch batch id num = %d\n", (int)max_batch_id_num);
		p_batch_id = (UINT32*)malloc(max_batch_id_num*sizeof(UINT32));
		
		vendor_ais_get_diff_batch_id_info(&mem_manager, &max_batch_num, p_batch_id);
		printf("max batch num = %d\n", (int)max_batch_num);
		for (i=0; i < max_batch_id_num; i++) {
			printf("switchable batch id = %d\n", (int)p_batch_id[i]);
		}

		free(p_batch_id);
	}
	diff_batch_info.batch_num = p_src_img[0].batch_num; //assign batch num
	ret = vendor_ais_pars_diff_batch_mem(&mem_manager, &diff_batch_info, net_id);
	if (ret != HD_OK) {
		printf("vendor_ais_pars_diff_mem (%d)\n\r", ret);
		return 0;
	}
#endif
	//do {
#if AI_SAMPLE_TEST_BATCH
	#if !SUPPORT_MULTI_BLOB_IN_SAMPLE
	for (i = 0; i < input_img_num; i++) {
		if (vendor_ais_net_input_layer_init(&p_src_img[i], i, net_id) != HD_OK) {
			goto gen_init_fail;
		}
	}
	#else
	
	{
		UINT32 j = 0;
		UINT32 io_num = 0;
		UINT32* p_input_blob_info = NULL;
		
		for (i = 0; i < input_img_num; i++) {
			if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK){
				goto gen_init_fail;
			}
#if AI_TEST_DYNAMIC_BATCH
			for (j = 0; j < p_src_img[0].batch_num; j++) {
#else
			for (j = 0; j < io_num; j++) {
#endif
				UINT32 proc_idx = p_input_blob_info[j] >> 8;
				UINT32 tmp_va = p_src_img[i].va;
				UINT32 tmp_pa = p_src_img[i].pa;
				p_src_img[i].va += (j*p_src_img[i].batch_ofs); 
				p_src_img[i].pa += (j*p_src_img[i].batch_ofs); 
				if (vendor_ais_net_input_layer_init(&p_src_img[i], proc_idx, net_id) != HD_OK) {
					goto gen_init_fail;
				}
				p_src_img[i].va = tmp_va;
				p_src_img[i].pa = tmp_pa;
			}
		}
	}
	#endif
#else
	if (vendor_ais_net_input_init(p_src_img, net_id) != HD_OK) {
		goto gen_init_fail;
	}
#endif
	//printf("vendor_ais_pars_diff_mem (%d)\n\r", ret);

	do {
		if (is_net_run[run_id]) {
			memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
			hd_common_mem_flush_cache((VOID *)mem_manager.user_buff.va, mem_manager.user_buff.size);
#if USE_NEON
			vendor_ais_proc_net(model_mem, rslt_mem, &p_src_img[0], net_id);
#else
			vendor_ais_proc_net(model_mem, rslt_mem, net_id);
#endif
			
#if GET_OUTPUT_LAYER_FEATURE_MAP
			ret = vendor_ais_get_layer_mem(model_mem, &output_layer_iomem, output_layer1_indx);
			if (ret == HD_OK) {
#if CNN_25_MATLAB
				VOID *output_layer_mem  = (VOID*)(output_layer_iomem.SAO[0].va);
#else
				NN_DATA* p_sao = (NN_DATA *)output_layer_iomem.omem_addr;
				VOID *output_layer_mem  = (VOID*)(p_sao[0].va);
				if (p_sao[0].va > 0 && p_sao[0].size > 0) {
					hd_common_mem_flush_cache((VOID *)p_sao[0].va, p_sao[0].size);
				}
#endif
				ret = vendor_ais_transform_float_output(output_layer, output_layer_mem, output_layer_tran_float);
				if (ret != HD_OK) {
					printf("vendor_ais_transform_float_output fail\n");
					return 0;
				}
			} else {
				printf("vendor_ais_get_layer_mem fail\n");
			}
			
#endif

			p_net_rslt = vendor_ais_get_results(net_id);
			printf("\r\nClassification Results:\r\n");
			if (p_net_rslt != NULL) {
				if (p_net_rslt->result_num == 1 && p_net_rslt->p_result[0].w == 0.0 && p_net_rslt->p_result[0].h == 0.0) {
					//printf("\r\nClassification Results (Top 5):\r\n");
					p_rslt = &p_net_rslt->p_result[0];
					for (i = 0; i < TOP_N; i++) {
					#if DYNAMIC_LABEL
						printf("%ld. no=%ld, label=%s, score=%f\r\n", i + 1, p_rslt->no[i]
								 , &g_class_labels[net_id][p_rslt->no[i] * VENDOR_AIS_LBL_LEN], p_rslt->score[i]);
					#else
						printf("%ld. no=%ld, label=%s, score=%f\r\n", i + 1, p_rslt->no[i]
								 , &g_class_labels[p_rslt->no[i] * VENDOR_AIS_LBL_LEN], p_rslt->score[i]);
					#endif
					}
				} else {
					//printf("\r\nClassification Results (Top 1):\r\n");
					for (i = 0; i < p_net_rslt->result_num; i++) {
						p_rslt = &p_net_rslt->p_result[i];
#if USE_NEON
					#if DYNAMIC_LABEL
						//printf("label=%s, x=%d y=%d w=%d h=%d\r\n", &g_class_labels[net_id][p_rslt->no[0] * VENDOR_AIS_LBL_LEN], 
						//		p_rslt->x, p_rslt->y, p_rslt->w, p_rslt->h);
						printf("%ld: no=%ld score=%f x=%f y=%f w=%f h=%f\r\n", i + 1, p_rslt->no[0],
								p_rslt->score[0], p_rslt->x, p_rslt->y, p_rslt->w, p_rslt->h);
					#else
						printf("%ld: no=%ld score=%f x=%f y=%f w=%f h=%f\r\n", i + 1, p_rslt->no[0],
								p_rslt->score[0], p_rslt->x, p_rslt->y, p_rslt->w, p_rslt->h);
					#endif	
#else
					
						
					#if DYNAMIC_LABEL
						printf("%ld. no=%ld, label=%s, score=%f\r\n", i + 1, p_rslt->no[0]
								 , &g_class_labels[net_id][p_rslt->no[0] * VENDOR_AIS_LBL_LEN], p_rslt->score[0]);
					#else
						printf("%ld. no=%ld, label=%s, score=%f\r\n", i + 1, p_rslt->no[0],
								 &g_class_labels[p_rslt->no[0] * VENDOR_AIS_LBL_LEN], p_rslt->score[0]);
					#endif
#endif
					}
				}
			}
			is_net_run[run_id] = FALSE;
			printf("[ai sample] write result done!\n");
		}
	} while(is_net_proc);
	
#if AI_SAMPLE_TEST_BATCH
#if !SUPPORT_MULTI_BLOB_IN_SAMPLE
	for (i = 0; i < input_img_num; i++) {
		ret = vendor_ais_net_input_layer_uninit(i, net_id);
		if(ret != HD_OK) {
			printf("vendor_ais_net_input_uninit (%d)\n\r", ret);
		}
	}
#else
	{
		UINT32 j = 0;
		UINT32 io_num = 0;
		UINT32* p_input_blob_info = NULL;
		
		for (i = 0; i < input_img_num; i++) {
			if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK){
				goto gen_init_fail;
			}
#if AI_TEST_DYNAMIC_BATCH
			for (j = 0; j < p_src_img[0].batch_num; j++) {
#else
			for (j = 0; j < io_num; j++) {
#endif
				UINT32 proc_idx = p_input_blob_info[j] >> 8;
	
				ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
				if(ret != HD_OK) {
					printf("vendor_ais_net_input_uninit (%d)\n\r", ret);
				}
			}
		}
	}
#endif
#else
	ret = vendor_ais_net_input_uninit(net_id);
	if(ret != HD_OK) {
		printf("vendor_ais_net_input_uninit (%d)\n\r", ret);
	}
#endif
	//} while(is_net_proc);
#if AI_TEST_DYNAMIC_BATCH
	ret = vendor_ais_unpars_diff_batch_mem(&mem_manager, &diff_batch_info, net_id);
	if (ret != HD_OK) {
		printf("vendor_ais_unpars_diff_mem (%d)\n\r", ret);
		return 0;
	}
#endif
#if AUTO_UPDATE_DIM	
	vendor_ais_unpars_diff_mem(&mem_manager, &diff_model_mem, &diff_info, net_id);
#endif
gen_init_fail:
	ret = vendor_ais_net_gen_uninit(net_id);
	if(ret != HD_OK) {
		printf("vendor_ais_net_gen_uninit (%d)\n\r", ret);
	}
#if DYNAMIC_LABEL
	if (g_class_labels[net_id] != NULL) {
		free(g_class_labels[net_id]);
		g_class_labels[net_id] = NULL;
	}
#endif
#if GET_OUTPUT_LAYER_FEATURE_MAP
	if (output_layer_tran_float != NULL) {
		free(output_layer_tran_float);	
	}
#endif
#if AI_V4
	if (output_layer_info != NULL) {
		free(output_layer_info);
		output_layer_info = NULL;
	}
	if (input_info != NULL) {
		free(input_info);
		input_info = NULL;
	}
#endif
	
	return 0;
}

/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
MAIN(argc, argv)
{
	CHAR* ai_sdk_version = {NULL};
	VENDOR_AIS_FLOW_MEM_PARM mem_parm[NN_RUN_NET_NUM] = {0};
	VENDOR_AIS_NETWORK_PARM net_parm[NN_RUN_NET_NUM] = {0};
	pthread_t nn_thread_id[NN_RUN_NET_NUM];
	HD_RESULT           ret;
	VENDOR_AIS_IMG_PARM	src_img[AI_SAMPLE_MAX_INPUT_NUM] = {0};
	int idx = 0;
#if defined(__LINUX)
	INT32 key;
#else
	CHAR key;
#endif
	int dbg_setting = 0, debug_parm = 0;
	int img_blob_num = 1;
	FILE *fp = NULL;
	int i = 0;
	CHAR img_path[AI_SAMPLE_MAX_INPUT_NUM][256];

	if(argc != 2){
		DBG_WRN("usage : net_app_user_sample wrong input\n");
		return -1;
	}
	
	
	// read input config mode
	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		printf("open input config fail.\r\n");
		return -1;
	} else {
		fscanf(fp, "total_img_blob_num = %d\n\n", &img_blob_num);
		printf("total_img_blob_num= %d\r\n", img_blob_num);
		
		if (img_blob_num > AI_SAMPLE_MAX_INPUT_NUM) {
			printf("input img num exceed the max: %d\n", AI_SAMPLE_MAX_INPUT_NUM);
			if (fp) {
				fclose(fp);
				fp = NULL;
			}
			return -1;
		}
		
		for (i = 0; i < img_blob_num; i++) {
			//read img info
			fscanf(fp, "%s %hd %hd %hd %hd %ld %ld %ld 0x%08x\n", img_path[i], &src_img[i].width, &src_img[i].height, &src_img[i].channel, 
			&src_img[i].batch_num, &src_img[i].line_ofs, &src_img[i].channel_ofs, &src_img[i].batch_ofs, &src_img[i].fmt);
		}
		
		// scan dbg setting
		fscanf(fp, "dbg_mode = %d\n\n", &dbg_setting);
		if (dbg_setting > 0) {
			fscanf(fp, "dump_output = %d\n\n", &debug_parm);
			vendor_ais_set_dbg_mode(AI_DUMP_OUT_BIN, debug_parm);
		}
		if (dbg_setting > 1) {
			fscanf(fp, "dump_input = %d\n\n", &debug_parm);
			vendor_ais_set_dbg_mode(AI_DUMP_IN_BIN, debug_parm);
		}
		if (dbg_setting > 2) {
			fscanf(fp, "stop process index = %d\n\n", &debug_parm);
			vendor_ais_set_dbg_mode(AI_DBG_STOP_IDX, debug_parm);
		}
	}
	
	if (fp) {
		fclose(fp);
		fp = NULL;
	}


	ai_sdk_version = vendor_ais_chk_sdk_version();
	printf("ai_sdk_version: %s\n", ai_sdk_version);


	ret = hd_common_init(0);
	if (ret != HD_OK) {
		DBG_ERR("hd_common_init fail=%d\n", ret);
		goto exit;
	}
	//set project config for AI
	hd_common_sysconfig(0, (1<<16), 0, VENDOR_AI_CFG); //enable AI engine
	// global init for ai sdk
	vendor_ai_global_init();

	// init memory
#if DYNAMIC_MEM
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		ret = vendor_ais_get_model_mem_sz(&mem_size[idx], model_name[idx]);
		if (ret != HD_OK) {
			DBG_WRN("something wrong in get model %s size info\n", model_name[idx]);
		}
		if (mem_size[idx] > 0) {
			mem_size[idx] += (sizeof(VENDOR_AIS_RESULT_INFO) + MAX_OBJ_NUM * sizeof(VENDOR_AIS_RESULT));
			mem_size[idx] = ALIGN_CEIL_16(mem_size[idx]);
		}
	}
#endif
#if AUTO_UPDATE_DIM
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		ret = vendor_ais_get_diff_model_mem_sz(&diff_model_size, diff_model_name[idx]);
		if (ret != HD_OK) {
			DBG_WRN("something wrong in get diff model %s size info\n", diff_model_name[idx]);
		}
		if (diff_model_size > 0) {
			mem_size[idx] = ALIGN_CEIL_16(mem_size[idx] + diff_model_size);
		}
	}
#endif

	ret = mem_init();
	if (ret != HD_OK) {
		DBG_ERR("mem_init fail=%d\n", ret);
		goto exit;
	}

	ret = hd_videoproc_init();
	if (ret != HD_OK) {
		DBG_ERR("hd_videoproc_init fail=%d\n", ret);
		goto exit;
	}
	
	for (i = 0; i < img_blob_num; i++) {
		ret = ai_load_img(img_path[i], &src_img[i], i);
		if (ret != HD_OK) {
			DBG_ERR("ai_load_img %d fail=%d\n", i, ret);
			goto exit;
		}
	}
#if (DYNAMIC_LABEL == 0)
	ret = ai_read_label((UINT32)g_class_labels, VENDOR_AIS_LBL_LEN, "accuracy/labels.txt");
	if (ret != HD_OK) {
		DBG_ERR("ai_read_label fail=%d\n", ret);
		goto exit;
	}
#endif

	// allocate memory
	mem_parm[0].pa 		= g_mem.pa;
	mem_parm[0].va 		= g_mem.va;
	mem_parm[0].size 	= mem_size[0];
	for (idx = 1; idx < NN_RUN_NET_NUM; idx++) {
		mem_parm[idx].pa 		= mem_parm[idx - 1].pa + mem_parm[idx - 1].size;
		mem_parm[idx].va 		= mem_parm[idx - 1].va + mem_parm[idx - 1].size;
		mem_parm[idx].size 		= mem_size[idx];
	}

	// create encode_thread (pull_out bitstream)
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		net_parm[idx].src_img_num = img_blob_num;
		for (i = 0; i < img_blob_num; i++) {
			net_parm[idx].src_img[i] = src_img[i];
		}
		net_parm[idx].mem = mem_parm[idx];
		net_parm[idx].run_id = idx;
		net_parm[idx].p_model_path = model_name[idx];
		ret = pthread_create(&nn_thread_id[idx], NULL, nn_thread_api, (VOID*)(&net_parm[idx]));
		if (ret < 0) {
			DBG_ERR("create encode thread failed");
			goto exit;
		}
	}

	do {
		printf("usage:\n");
		printf("  enter q: exit\n");
		printf("  enter r: run engine\n");

		key = GETCHAR();

		if (key == 'q' || key == 0x3) {
			is_net_proc = FALSE;
			for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
				is_net_run[idx] = FALSE;
			}
			break;
		} else if (key == 'r') {
			//  run network
			is_net_proc = TRUE;
			for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
				is_net_run[idx] = TRUE;
			}
			printf("[ai sample] write result done!\n");
			continue;
		}
	} while(1);

	// wait encode thread destroyed
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		pthread_join(nn_thread_id[idx], NULL);
	}

exit:

	ret = hd_videoproc_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_videoproc_uninit fail=%d\n", ret);
	}

	ret = mem_uninit();
	if (ret != HD_OK) {
		DBG_ERR("mem_uninit fail=%d\n", ret);
	}
	// global uninit for ai sdk
	vendor_ai_global_uninit();

	ret = hd_common_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_common_uninit fail=%d\n", ret);
	}

	printf("network test finish...\r\n");
	
	

	return ret;
}
