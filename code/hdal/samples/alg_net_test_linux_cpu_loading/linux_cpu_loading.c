/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file Linux_cpu_loading.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/


#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <linux/limits.h>
#include <sys/times.h>
#include <sys/types.h>
#include "linux_cpu_loading.h"



/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/

typedef long long int num;

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/

num pid;
char tcomm[PATH_MAX];
char state;

num ppid;
num pgid;
num sid;
num tty_nr;
num tty_pgrp;

num flags;
num min_flt;
num cmin_flt;
num maj_flt;
num cmaj_flt;
num utime;
num stimev;

num cutime;
num cstime;
num priority;
num nicev;
num num_threads;
num it_real_value;

unsigned long long start_time;

num vsize;
num rss;
num rsslim;
num start_code;
num end_code;
num start_stack;
num esp;
num eip;

num pending;
num blocked;
num sigign;
num sigcatch;
num wchan;
num zero1;
num zero2;
num exit_signal;
num cpu;
num rt_priority;
num policy;

long tickspersec;

FILE *input, *cpu_ptr;

/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/

void readone(num *x) { fscanf(input, "%lld ", x); }
void readunsigned(unsigned long long *x) { fscanf(input, "%llu ", x); }
void readstr(char *x) {  fscanf(input, "%s ", x);}
void readchar(char *x) {  fscanf(input, "%c ", x);}

void printone(char *name, num x) {  printf("%20s: %lld\n", name, x);}
void printonex(char *name, num x) {  printf("%20s: %016llx\n", name, x);}
void printunsigned(char *name, unsigned long long x) {  printf("%20s: %llu\n", name, x);}
void printchar(char *name, char x) {  printf("%20s: %c\n", name, x);}
void printstr(char *name, char *x) {  printf("%20s: %s\n", name, x);}
void printtime(char *name, num x) {  printf("%20s: %f\n", name, (((double)x) / tickspersec));}

int gettimesinceboot(void) {
	FILE *procuptime;
	int sec, ssec;

	procuptime = fopen("/proc/uptime", "r");
	fscanf(procuptime, "%d.%ds", &sec, &ssec);
	fclose(procuptime);
	return (sec*tickspersec)+ssec;
}

void printtimediff(char *name, num x) {
	int sinceboot = gettimesinceboot();
	int running = sinceboot - x;
	time_t rt = time(NULL) - (running / tickspersec);
	char buf[1024];

	strftime(buf, sizeof(buf), "%m.%d %H:%M", localtime(&rt));
	//printf("%20s: %s (%u.%us)\n", name, buf, running / tickspersec, running % tickspersec);
}

/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/

void cal_linux_cpu_loading(int loop_num, double *cpu_user, double *cpu_sys) {
	//printf("1. cal_linux_cpu_loading\r\n");
	pid_t process_id = getpid();
	//printf("2. cal_linux_cpu_loading\r\n");
	tickspersec = sysconf(_SC_CLK_TCK);
	//printf("3. cal_linux_cpu_loading\r\n");
	char filePath[200];
	sprintf(filePath, "/proc/%d/stat", (int)process_id);

	input = NULL;
	cpu_ptr = NULL;

	double pre_cpu_time = 0;
	double pre_utime = 0;
	double pre_stimev = 0;

	double cpu_avg_user = 0.0;
	double cpu_avg_sys = 0.0;
	for(int i = 0; i <= loop_num; ++i){//do num + 1 times
		//printf("read proc/stat\r\n");
		cpu_ptr = fopen("/proc/stat", "r");  

		char cpu_str[20];
		fscanf(cpu_ptr, "%s ", cpu_str);
		double cpu_time = 0;
		long cpu_temp = 0;
		//printf("read proc/stat cpu_tmp\r\n");
		for(int i = 0; i < 9; ++i)
		{
		  fscanf(cpu_ptr, "%lu ", &cpu_temp);
		  cpu_time += cpu_temp;
		}
		fclose(cpu_ptr);

		//printf("read %s\r\n", filePath);
		input = fopen(filePath, "r");
		readone(&pid);
		readstr(tcomm);
		readchar(&state);
		readone(&ppid);
		readone(&pgid);
		readone(&sid);
		readone(&tty_nr);
		readone(&tty_pgrp);
		readone(&flags);
		readone(&min_flt);
		readone(&cmin_flt);
		readone(&maj_flt);
		readone(&cmaj_flt);
		readone(&utime);
		readone(&stimev);
		readone(&cutime);
		readone(&cstime);
		readone(&priority);
		readone(&nicev);
		readone(&num_threads);
		readone(&it_real_value);
		readunsigned(&start_time);
		readone(&vsize);
		readone(&rss);
		readone(&rsslim);
		readone(&start_code);
		readone(&end_code);
		readone(&start_stack);
		readone(&esp);
		readone(&eip);
		readone(&pending);
		readone(&blocked);
		readone(&sigign);
		readone(&sigcatch);
		readone(&wchan);
		readone(&zero1);
		readone(&zero2);
		readone(&exit_signal);
		readone(&cpu);
		readone(&rt_priority);
		readone(&policy);
		fclose(input);

		//printf("cpu time = %f, utime = %f, preutime = %f, sub = %f\r\n", cpu_time - pre_cpu_time, utime, pre_utime, utime - pre_utime);
		double user_util = 100 * (utime - pre_utime) / (cpu_time - pre_cpu_time);
		double sys_util = 100 * (stimev - pre_stimev) / (cpu_time - pre_cpu_time);
		//printf("cpu(user) = %f, cpu(system) = %f\r\n", user_util, sys_util);

		pre_utime = utime;
		pre_stimev = stimev;
		pre_cpu_time = cpu_time;
		sleep(3);
		if(i != 0){
			cpu_avg_user += user_util;
			cpu_avg_sys += sys_util;
		}
	}
	*cpu_user = cpu_avg_user / loop_num;
	*cpu_sys = cpu_avg_sys / loop_num;
}
