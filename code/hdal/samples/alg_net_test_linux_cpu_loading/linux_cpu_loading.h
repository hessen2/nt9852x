/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file Linux_cpu_loading.h

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/


#ifndef LINUX_CPU_LOADING_H
#define LINUX_CPU_LOADING_H

/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/

void cal_linux_cpu_loading(int loop_num, double *cpu_user, double *cpu_sys); 

#endif
