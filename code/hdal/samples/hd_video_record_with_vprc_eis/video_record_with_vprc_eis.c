/**
	@brief Sample code of video record with eis.\n

	@file video_record_with_vprc_eis.c

	@author Janice Huang

	@ingroup mhdal


	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "hdal.h"
#include "hd_debug.h"
#include "vendor_videoprocess.h"
#include "vendor_videocapture.h"
#include "vendor_eis.h"
#include "vendor_vpe.h"
#include "vendor_isp.h"
#include <math.h>

// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(hd_video_record_with_vprc_eis, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

#define DEBUG_MENU 		1

#define CHKPNT			printf("\033[37mCHK: %s, %s: %d\033[0m\r\n",__FILE__,__func__,__LINE__)
#define DBGH(x)			printf("\033[0;35m%s=0x%08X\033[0m\r\n", #x, x)
#define DBGD(x)			printf("\033[0;35m%s=%d\033[0m\r\n", #x, x)

///////////////////////////////////////////////////////////////////////////////

//header
#define DBGINFO_BUFSIZE()	(0x200)

//RAW
#define VDO_RAW_BUFSIZE(w, h, pxlfmt)   (ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NRX: RAW compress: Only support 12bit mode
#define RAW_COMPRESS_RATIO 59
#define VDO_NRX_BUFSIZE(w, h)           (ALIGN_CEIL_4(ALIGN_CEIL_64(w) * 12 / 8 * RAW_COMPRESS_RATIO / 100 * (h)))
//CA for AWB
#define VDO_CA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 3) << 1)
//LA for AE
#define VDO_LA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 1) << 1)

//YUV
#define VDO_YUV_BUFSIZE(w, h, pxlfmt)	(ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NVX: YUV compress
#define YUV_COMPRESS_RATIO 75
#define VDO_NVX_BUFSIZE(w, h, pxlfmt)	(VDO_YUV_BUFSIZE(w, h, pxlfmt) * YUV_COMPRESS_RATIO / 100)

///////////////////////////////////////////////////////////////////////////////

#define SEN_OUT_FMT		HD_VIDEO_PXLFMT_RAW12
#define CAP_OUT_FMT		HD_VIDEO_PXLFMT_RAW12
#define CA_WIN_NUM_W		32
#define CA_WIN_NUM_H		32
#define LA_WIN_NUM_W		32
#define LA_WIN_NUM_H		32
#define VA_WIN_NUM_W		16
#define VA_WIN_NUM_H		16
#define YOUT_WIN_NUM_W	128
#define YOUT_WIN_NUM_H	128
#define ETH_8BIT_SEL		0 //0: 2bit out, 1:8 bit out
#define ETH_OUT_SEL		1 //0: full, 1: subsample 1/2

#define SEN_SEL_IMX290   0   //2M_SHDR
#define SEN_SEL_IMX415   1   //8M_SHDR


#define VDO_SIZE_W_8M      3840
#define VDO_SIZE_H_8M      2160
#define VDO_SIZE_W_2M	   1920
#define VDO_SIZE_H_2M	   1080


static UINT32 VDO_SIZE_W=0;
static UINT32 VDO_SIZE_H=0;

#define SUB_VDO_SIZE_W	1280
#define SUB_VDO_SIZE_H	720

#define GYRO_DATA_NUM   16
#define GYRO_DATA_SIZE (4+4*GYRO_DATA_NUM*6)

#define EIS_PATH0_2DLUT_SIZE VENDOR_EIS_LUT_9x9

#define ISP_EFFECT_ID 0

static UINT32 g_enc_poll = 1;
static UINT32 g_write_file = 1;
static UINT32 sensor_sel = SEN_SEL_IMX290;
static UINT32 vdo_br     = (2 * 1024 * 1024);

static UINT32 g_capbind = 0;  //0:D2D, 1:direct, 2: one-buf, 0xff: no-bind

#define VPRC_PI 3.1415926

static double normalized_default_2dlut[2][9][9] =
{
	{//X
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9},
		{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9}
	},
	{//Y
		{0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1},
		{0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2},
		{0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3},
		{0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4},
		{0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5},
		{0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6},
		{0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7},
		{0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8},
		{0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9}
	}
};

static void gen_default_2dlut(UINT32 *p_lut2d_buf, INT32 width, INT32 height, INT32 lut_size)
{
	INT32 i, j;
	UINT32 line_data_num = ((lut_size+3)>>2)<<2;

	for (j = 0; j < lut_size; j++) {
		for (i = 0; i < lut_size; i++) {
			p_lut2d_buf[j*line_data_num + i] =
				((UINT32)round(normalized_default_2dlut[1][j][i]*(double)height*4) << 16) +
				(UINT32)round(normalized_default_2dlut[0][j][i]*(double)width*4);
		}
	}
}
static VPET_2DLUT_PARAM lut2d_path0 = {
	0,
	{
		EIS_PATH0_2DLUT_SIZE,
		{0}
	}
};
///////////////////////////////////////////////////////////////////////////////


static HD_RESULT mem_init(void)
{
	HD_RESULT              ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};

	// config common pool (cap)
	mem_cfg.pool_info[0].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[0].blk_size = DBGINFO_BUFSIZE()+VDO_RAW_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, CAP_OUT_FMT)
														+VDO_CA_BUF_SIZE(CA_WIN_NUM_W, CA_WIN_NUM_H)
														+VDO_LA_BUF_SIZE(LA_WIN_NUM_W, LA_WIN_NUM_H);
	mem_cfg.pool_info[0].blk_size += GYRO_DATA_SIZE;
	mem_cfg.pool_info[0].blk_cnt = 3;
	mem_cfg.pool_info[0].ddr_id = DDR_ID0;
	// config common pool (pre-process)
	mem_cfg.pool_info[1].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[1].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
	mem_cfg.pool_info[1].blk_size += (vendor_eis_buf_query(VENDOR_EIS_LUT_65x65) + (GYRO_DATA_SIZE + sizeof(VENDOR_EIS_DBG_CTX)));
	mem_cfg.pool_info[1].blk_cnt = 3;
	mem_cfg.pool_info[1].ddr_id = DDR_ID0;
	// config common pool (main)
	mem_cfg.pool_info[2].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[2].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
	mem_cfg.pool_info[2].blk_cnt = 3;
	if (g_capbind == 1) {
		//direct
		mem_cfg.pool_info[1].blk_cnt += 2; //direct will pre-new 1
	}
	mem_cfg.pool_info[2].ddr_id = DDR_ID0;
	// config common pool (sub)
	mem_cfg.pool_info[3].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[3].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(SUB_VDO_SIZE_W, SUB_VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
	mem_cfg.pool_info[3].blk_cnt = 3;
	mem_cfg.pool_info[3].ddr_id = DDR_ID0;
	// config common pool (display)
	mem_cfg.pool_info[2].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[2].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, HD_VIDEO_PXLFMT_YUV420);
	mem_cfg.pool_info[2].blk_cnt = 3;
	mem_cfg.pool_info[2].ddr_id = DDR_ID0;
	ret = hd_common_mem_init(&mem_cfg);
	return ret;
}

static HD_RESULT mem_exit(void)
{
	HD_RESULT ret = HD_OK;
	hd_common_mem_uninit();
	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT get_cap_caps(HD_PATH_ID video_cap_ctrl, HD_VIDEOCAP_SYSCAPS *p_video_cap_syscaps)
{
	HD_RESULT ret = HD_OK;
	hd_videocap_get(video_cap_ctrl, HD_VIDEOCAP_PARAM_SYSCAPS, p_video_cap_syscaps);
	return ret;
}

static HD_RESULT set_cap_cfg(HD_PATH_ID *p_video_cap_ctrl)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOCAP_DRV_CONFIG cap_cfg = {0};
	HD_PATH_ID video_cap_ctrl = 0;
	HD_VIDEOCAP_CTRL iq_ctl = {0};

	if (sensor_sel == SEN_SEL_IMX290) {
		snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, "nvt_sen_imx290");
		printf("Using nvt_sen_imx290\n");
	} else if (sensor_sel == SEN_SEL_IMX415) {
		snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, "nvt_sen_imx415");
		printf("Using nvt_sen_imx415\n");
	} else {
		// MIPI interface
		cap_cfg.sen_cfg.sen_dev.if_type = HD_COMMON_VIDEO_IN_MIPI_CSI;
		cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux = 0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
	}
	cap_cfg.sen_cfg.sen_dev.if_type = HD_COMMON_VIDEO_IN_MIPI_CSI;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.serial_if_pinmux = 0xf04;//PIN_MIPI_LVDS_CFG_CLK2 | PIN_MIPI_LVDS_CFG_DAT0|PIN_MIPI_LVDS_CFG_DAT1 | PIN_MIPI_LVDS_CFG_DAT2 | PIN_MIPI_LVDS_CFG_DAT3
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = 0x10;//PIN_I2C_CFG_CH2
	cap_cfg.sen_cfg.sen_dev.pin_cfg.clk_lane_sel = HD_VIDEOCAP_SEN_CLANE_SEL_CSI0_USE_C2;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[0] = 0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[1] = 1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[4] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[5] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[6] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[7] = HD_VIDEOCAP_SEN_IGNORE;
	ret = hd_videocap_open(0, HD_VIDEOCAP_0_CTRL, &video_cap_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_DRV_CONFIG, &cap_cfg);
	iq_ctl.func = HD_VIDEOCAP_FUNC_AE | HD_VIDEOCAP_FUNC_AWB;
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_CTRL, &iq_ctl);

	*p_video_cap_ctrl = video_cap_ctrl;
	return ret;
}

static HD_RESULT set_cap_param(HD_PATH_ID video_cap_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	{//select sensor mode, manually or automatically
		HD_VIDEOCAP_IN video_in_param = {0};

		video_in_param.sen_mode = HD_VIDEOCAP_SEN_MODE_AUTO; //auto select sensor mode by the parameter of HD_VIDEOCAP_PARAM_OUT
		video_in_param.frc = HD_VIDEO_FRC_RATIO(30,1);
		//video_in_param.frc = HD_VIDEO_FRC_RATIO(5,1);
		//video_in_param.frc = HD_VIDEO_FRC_RATIO(1,1);
		video_in_param.dim.w = p_dim->w;
		video_in_param.dim.h = p_dim->h;
		video_in_param.pxlfmt = SEN_OUT_FMT;
		video_in_param.out_frame_num = HD_VIDEOCAP_SEN_FRAME_NUM_1;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN, &video_in_param);
		//printf("set_cap_param MODE=%d\r\n", ret);
		if (ret != HD_OK) {
			return ret;
		}
	}
	#if 1 //no crop, full frame
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_OFF;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_OUT_CROP, &video_crop_param);
		//printf("set_cap_param CROP NONE=%d\r\n", ret);
	}
	#else //HD_CROP_ON
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_ON;
		video_crop_param.win.rect.x = 0;
		video_crop_param.win.rect.y = 0;
		video_crop_param.win.rect.w = 1920/2;
		video_crop_param.win.rect.h= 1080/2;
		video_crop_param.align.w = 4;
		video_crop_param.align.h = 4;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_OUT_CROP, &video_crop_param);
		//printf("set_cap_param CROP ON=%d\r\n", ret);
	}
	#endif
	{
		HD_VIDEOCAP_OUT video_out_param = {0};

		//without setting dim for no scaling, using original sensor out size
		video_out_param.pxlfmt = CAP_OUT_FMT;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_OUT, &video_out_param);
		//printf("set_cap_param OUT=%d\r\n", ret);
	}
	{
		HD_VIDEOCAP_FUNC_CONFIG video_path_param = {0};

		video_path_param.out_func = 0;
		if (g_capbind == 1) //direct mode
			video_path_param.out_func = HD_VIDEOCAP_OUTFUNC_DIRECT;
		if (g_capbind == 2) //one-buf mode
			video_path_param.out_func = HD_VIDEOCAP_OUTFUNC_ONEBUF;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_FUNC_CONFIG, &video_path_param);
		//printf("set_cap_param PATH_CONFIG=0x%X\r\n", ret);
	}
	{
		VENDOR_VIDEOCAP_GYRO_INFO vcap_gyro_info = {0};

		vcap_gyro_info.en = TRUE;
		vcap_gyro_info.data_num = GYRO_DATA_NUM;
		ret = vendor_videocap_set(video_cap_path, VENDOR_VIDEOCAP_PARAM_GYRO_INFO, &vcap_gyro_info);
		//printf("set_cap_param OUT=%d\r\n", ret);
	}
	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_proc_cfg(HD_OUT_ID ctrl, HD_PATH_ID *p_video_proc_ctrl, HD_VIDEOPROC_PIPE pipe, HD_DIM* p_max_dim)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOPROC_DEV_CONFIG video_cfg_param = {0};
	HD_VIDEOPROC_CTRL video_ctrl_param = {0};
	HD_PATH_ID video_proc_ctrl = 0;

	ret = hd_videoproc_open(0, ctrl, &video_proc_ctrl); //open this for device control
	if (ret != HD_OK)
		return ret;

	if (p_max_dim != NULL ) {
		video_cfg_param.pipe = pipe;
		if (pipe == HD_VIDEOPROC_PIPE_RAWALL) {
			video_cfg_param.isp_id = 0;
		} else { //HD_VIDEOPROC_PIPE_VPE
			video_cfg_param.isp_id = ISP_EFFECT_ID;
		}
		video_cfg_param.ctrl_max.func = 0;
		video_cfg_param.in_max.func = 0;
		video_cfg_param.in_max.dim.w = p_max_dim->w;
		video_cfg_param.in_max.dim.h = p_max_dim->h;
		video_cfg_param.in_max.pxlfmt = (pipe == HD_VIDEOPROC_PIPE_VPE) ? HD_VIDEO_PXLFMT_YUV420 : CAP_OUT_FMT;
		video_cfg_param.in_max.frc = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_DEV_CONFIG, &video_cfg_param);
		if (ret != HD_OK) {
			return HD_ERR_NG;
		}
	}
	if (pipe == HD_VIDEOPROC_PIPE_RAWALL) {
		HD_VIDEOPROC_FUNC_CONFIG video_path_param = {0};
		BOOL eis_func = TRUE;

		video_path_param.in_func = 0;
		if (g_capbind == 1)
			video_path_param.in_func |= HD_VIDEOPROC_INFUNC_DIRECT; //direct NOTE: enable direct
		if (g_capbind == 2)
			video_path_param.in_func |= HD_VIDEOPROC_INFUNC_ONEBUF;
		ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_FUNC_CONFIG, &video_path_param);
		//printf("set_proc_param PATH_CONFIG=0x%X\r\n", ret);

		vendor_videoproc_set(video_proc_ctrl, VENDOR_VIDEOPROC_PARAM_EIS_FUNC, &eis_func);
	}

	video_ctrl_param.func = 0;
	ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_CTRL, &video_ctrl_param);

	*p_video_proc_ctrl = video_proc_ctrl;

	return ret;
}

static HD_RESULT set_proc_param(HD_PATH_ID video_proc_path, HD_DIM* p_dim)
{
	HD_RESULT ret = HD_OK;

	if (p_dim != NULL) { //if videoproc is already binding to dest module, not require to setting this!
		HD_VIDEOPROC_OUT video_out_param = {0};
		video_out_param.func = 0;
		video_out_param.dim.w = p_dim->w;
		video_out_param.dim.h = p_dim->h;
		video_out_param.pxlfmt = HD_VIDEO_PXLFMT_YUV420;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		video_out_param.frc = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoproc_set(video_proc_path, HD_VIDEOPROC_PARAM_OUT, &video_out_param);
	}

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_enc_cfg(HD_PATH_ID video_enc_path, HD_DIM *p_max_dim, UINT32 max_bitrate)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOENC_PATH_CONFIG video_path_config = {0};

	if (p_max_dim != NULL) {

		//--- HD_VIDEOENC_PARAM_PATH_CONFIG ---
		video_path_config.max_mem.codec_type = HD_CODEC_TYPE_H264;
		video_path_config.max_mem.max_dim.w  = p_max_dim->w;
		video_path_config.max_mem.max_dim.h  = p_max_dim->h;
		video_path_config.max_mem.bitrate    = max_bitrate;
		video_path_config.max_mem.enc_buf_ms = 3000;
		video_path_config.max_mem.svc_layer  = HD_SVC_4X;
		video_path_config.max_mem.ltr        = TRUE;
		video_path_config.max_mem.rotate     = FALSE;
		video_path_config.max_mem.source_output   = FALSE;
		video_path_config.isp_id             = 0;
		ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_PATH_CONFIG, &video_path_config);
		if (ret != HD_OK) {
			printf("set_enc_path_config = %d\r\n", ret);
			return HD_ERR_NG;
		}
	}

	return ret;
}

static HD_RESULT set_enc_param(HD_PATH_ID video_enc_path, HD_DIM *p_dim, UINT32 enc_type, UINT32 bitrate)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOENC_IN  video_in_param = {0};
	HD_VIDEOENC_OUT video_out_param = {0};
	HD_H26XENC_RATE_CONTROL rc_param = {0};

	if (p_dim != NULL) {

		//--- HD_VIDEOENC_PARAM_IN ---
		video_in_param.dir           = HD_VIDEO_DIR_NONE;
		video_in_param.pxl_fmt = HD_VIDEO_PXLFMT_YUV420;
		video_in_param.dim.w   = p_dim->w;
		video_in_param.dim.h   = p_dim->h;
		video_in_param.frc     = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_IN, &video_in_param);
		if (ret != HD_OK) {
			printf("set_enc_param_in = %d\r\n", ret);
			return ret;
		}

		printf("enc_type=%d\r\n", enc_type);

		if (enc_type == 0) {

			//--- HD_VIDEOENC_PARAM_OUT_ENC_PARAM ---
			video_out_param.codec_type         = HD_CODEC_TYPE_H265;
			video_out_param.h26x.profile       = HD_H265E_MAIN_PROFILE;
			video_out_param.h26x.level_idc     = HD_H265E_LEVEL_5;
			video_out_param.h26x.gop_num       = 15;
			video_out_param.h26x.ltr_interval  = 0;
			video_out_param.h26x.ltr_pre_ref   = 0;
			video_out_param.h26x.gray_en       = 0;
			video_out_param.h26x.source_output = 0;
			video_out_param.h26x.svc_layer     = HD_SVC_DISABLE;
			video_out_param.h26x.entropy_mode  = HD_H265E_CABAC_CODING;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_ENC_PARAM, &video_out_param);
			if (ret != HD_OK) {
				printf("set_enc_param_out = %d\r\n", ret);
				return ret;
			}

			//--- HD_VIDEOENC_PARAM_OUT_RATE_CONTROL ---
			rc_param.rc_mode             = HD_RC_MODE_CBR;
			rc_param.cbr.bitrate         = bitrate;
			rc_param.cbr.frame_rate_base = 30;
			rc_param.cbr.frame_rate_incr = 1;
			rc_param.cbr.init_i_qp       = 26;
			rc_param.cbr.min_i_qp        = 10;
			rc_param.cbr.max_i_qp        = 45;
			rc_param.cbr.init_p_qp       = 26;
			rc_param.cbr.min_p_qp        = 10;
			rc_param.cbr.max_p_qp        = 45;
			rc_param.cbr.static_time     = 4;
			rc_param.cbr.ip_weight       = 0;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_RATE_CONTROL, &rc_param);
			if (ret != HD_OK) {
				printf("set_enc_rate_control = %d\r\n", ret);
				return ret;
			}
		} else if (enc_type == 1) {

			//--- HD_VIDEOENC_PARAM_OUT_ENC_PARAM ---
			video_out_param.codec_type         = HD_CODEC_TYPE_H264;
			video_out_param.h26x.profile       = HD_H264E_HIGH_PROFILE;
			video_out_param.h26x.level_idc     = HD_H264E_LEVEL_5_1;
			video_out_param.h26x.gop_num       = 15;
			video_out_param.h26x.ltr_interval  = 0;
			video_out_param.h26x.ltr_pre_ref   = 0;
			video_out_param.h26x.gray_en       = 0;
			video_out_param.h26x.source_output = 0;
			video_out_param.h26x.svc_layer     = HD_SVC_DISABLE;
			video_out_param.h26x.entropy_mode  = HD_H264E_CABAC_CODING;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_ENC_PARAM, &video_out_param);
			if (ret != HD_OK) {
				printf("set_enc_param_out = %d\r\n", ret);
				return ret;
			}

			//--- HD_VIDEOENC_PARAM_OUT_RATE_CONTROL ---
			rc_param.rc_mode             = HD_RC_MODE_CBR;
			rc_param.cbr.bitrate         = bitrate;
			rc_param.cbr.frame_rate_base = 30;
			rc_param.cbr.frame_rate_incr = 1;
			rc_param.cbr.init_i_qp       = 26;
			rc_param.cbr.min_i_qp        = 10;
			rc_param.cbr.max_i_qp        = 45;
			rc_param.cbr.init_p_qp       = 26;
			rc_param.cbr.min_p_qp        = 10;
			rc_param.cbr.max_p_qp        = 45;
			rc_param.cbr.static_time     = 4;
			rc_param.cbr.ip_weight       = 0;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_RATE_CONTROL, &rc_param);
			if (ret != HD_OK) {
				printf("set_enc_rate_control = %d\r\n", ret);
				return ret;
			}

		} else if (enc_type == 2) {

			//--- HD_VIDEOENC_PARAM_OUT_ENC_PARAM ---
			video_out_param.codec_type         = HD_CODEC_TYPE_JPEG;
			video_out_param.jpeg.retstart_interval = 0;
			video_out_param.jpeg.image_quality = 50;
			ret = hd_videoenc_set(video_enc_path, HD_VIDEOENC_PARAM_OUT_ENC_PARAM, &video_out_param);
			if (ret != HD_OK) {
				printf("set_enc_param_out = %d\r\n", ret);
				return ret;
			}

		} else {

			printf("not support enc_type\r\n");
			return HD_ERR_NG;
		}
	}

	return ret;
}
///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_out_cfg(HD_PATH_ID *p_video_out_ctrl, UINT32 out_type, HD_VIDEOOUT_HDMI_ID hdmi_id)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOOUT_MODE videoout_mode = {0};
	HD_PATH_ID video_out_ctrl = 0;

	ret = hd_videoout_open(0, HD_VIDEOOUT_0_CTRL, &video_out_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}

	printf("out_type=%d\r\n", out_type);

	#if 1
	videoout_mode.output_type = HD_COMMON_VIDEO_OUT_LCD;
	videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
	videoout_mode.output_mode.lcd = HD_VIDEOOUT_LCD_0;
	if (out_type != 1) {
		printf("520 only support LCD\r\n");
	}
	#else
	switch(out_type){
	case 0:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_CVBS;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.cvbs= HD_VIDEOOUT_CVBS_NTSC;
	break;
	case 1:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_LCD;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.lcd = HD_VIDEOOUT_LCD_0;
	break;
	case 2:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_HDMI;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.hdmi= hdmi_id;
	break;
	default:
		printf("not support out_type\r\n");
	break;
	}
	#endif
	ret = hd_videoout_set(video_out_ctrl, HD_VIDEOOUT_PARAM_MODE, &videoout_mode);

	*p_video_out_ctrl=video_out_ctrl ;
	return ret;
}

static HD_RESULT get_out_caps(HD_PATH_ID video_out_ctrl,HD_VIDEOOUT_SYSCAPS *p_video_out_syscaps)
{
	HD_RESULT ret = HD_OK;
    HD_DEVCOUNT video_out_dev = {0};

	ret = hd_videoout_get(video_out_ctrl, HD_VIDEOOUT_PARAM_DEVCOUNT, &video_out_dev);
	if (ret != HD_OK) {
		return ret;
	}
	printf("##devcount %d\r\n", video_out_dev.max_dev_count);

	ret = hd_videoout_get(video_out_ctrl, HD_VIDEOOUT_PARAM_SYSCAPS, p_video_out_syscaps);
	if (ret != HD_OK) {
		return ret;
	}
	return ret;
}

static HD_RESULT set_out_param(HD_PATH_ID video_out_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOOUT_IN video_out_param={0};

	video_out_param.dim.w = p_dim->w;
	video_out_param.dim.h = p_dim->h;
	video_out_param.pxlfmt = HD_VIDEO_PXLFMT_YUV420;
	video_out_param.dir = HD_VIDEO_DIR_NONE;
	ret = hd_videoout_set(video_out_path, HD_VIDEOOUT_PARAM_IN, &video_out_param);
	if (ret != HD_OK) {
		return ret;
	}
	memset((void *)&video_out_param,0,sizeof(HD_VIDEOOUT_IN));
	ret = hd_videoout_get(video_out_path, HD_VIDEOOUT_PARAM_IN, &video_out_param);
	if (ret != HD_OK) {
		return ret;
	}
	printf("##video_out_param w:%d,h:%d %x %x\r\n", video_out_param.dim.w, video_out_param.dim.h, video_out_param.pxlfmt, video_out_param.dir);

	return ret;
}


///////////////////////////////////////////////////////////////////////////////

typedef struct _VIDEO_RECORD {

	// (1)
	HD_VIDEOCAP_SYSCAPS cap_syscaps;
	HD_PATH_ID cap_ctrl;
	HD_PATH_ID cap_path;

	HD_DIM  cap_dim;
	HD_DIM  proc0_max_dim;

	// (2)
	HD_VIDEOPROC_SYSCAPS proc0_syscaps;
	HD_PATH_ID proc0_ctrl;
	HD_PATH_ID proc0_path;

	HD_DIM  proc1_max_dim;
	HD_DIM  proc1_dim;

	// (3)
	HD_VIDEOPROC_SYSCAPS proc1_syscaps;
	HD_PATH_ID proc1_ctrl;
	HD_PATH_ID proc1_path;

	HD_DIM  enc_max_dim;
	HD_DIM  enc_dim;

	// (4)
	HD_VIDEOENC_SYSCAPS enc_syscaps;
	HD_PATH_ID enc_path;

	// (5) user pull
	pthread_t  enc_thread_id;
	UINT32     enc_exit;
	UINT32     flow_start;

	// (6) vout
	HD_VIDEOOUT_SYSCAPS out_syscaps;
	HD_PATH_ID out_ctrl;
	HD_PATH_ID out_path;
	HD_DIM  out_max_dim;
	HD_DIM  out_dim;

    HD_VIDEOOUT_HDMI_ID hdmi_id;
} VIDEO_RECORD;

static HD_RESULT init_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_init()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_init()) != HD_OK)
		return ret;
    if ((ret = hd_videoenc_init()) != HD_OK)
		return ret;
   if ((ret = hd_videoout_init()) != HD_OK)
		return ret;
	return HD_OK;
}

#if 0 //for IMX291
INT32 src_size[2] = {1920, 1080};
#if 0
//IMX291 with lens TRC-2191B6(IMX317)
double distor_pt_lut_4R[257] = {
0, 18.0243122612896, 36.0510039231625, 54.0824440742380, 72.1209811792078, 90.1689327668716, 108.228575118174, 126.302132954239, 144.391769124409, 162.499574294278, 180.627556633728, 198.777631504967, 216.951611150564, 235.151194381486, 253.377956265131, 271.633337813367, 289.918635670569, 308.234991801651, 326.583383180107, 344.964611476044, 363.379292744217, 381.827847112070, 400.310488467766, 418.827214148228, 437.377794627174, 455.961763203149, 474.578405687568, 493.226750092746, 511.905556319938, 530.613305847373, 549.348191418292, 568.108106728981, 586.890636116810, 605.693044248269, 624.512265807003, 643.344895181846, 662.187176154862, 681.034991589378, 699.883853118019, 718.728890830749, 737.564842962899, 756.386045583214, 775.186422281878, 793.959473858556, 812.698268010433, 831.395429020241, 850.043127444303, 868.633069800568, 887.156488256642, 905.604130317831, 923.966248515174, 942.232590093474, 960.392386699345, 978.434344069240, 996.346631717488, 1014.11687262433, 1031.73213292397, 1049.17891159257, 1066.44313013633, 1083.51012227953, 1100.36462365249, 1116.99076147972, 1133.37204426785, 1149.49135149374, 1165.33092329247, 1180.87235014540, 1196.09656256821, 1210.98382079891, 1225.51370448592, 1239.66510237604, 1253.41620200255, 1266.74447937323, 1279.62668865836, 1292.03885187880, 1303.95624859400, 1315.35340559006, 1326.20408656774, 1336.48128183051, 1346.15719797258, 1355.20324756694, 1363.59003885341, 1371.28736542665, 1378.26419592421, 1384.48866371455, 1389.92805658512, 1394.54880643034, 1398.31647893967, 1401.19576328565, 1403.15046181192, 1404.14347972124, 1404.13681476357, 1403.09154692408, 1400.96782811119, 1397.72487184459, 1393.32094294330, 1387.71334721371, 1380.85842113759, 1372.71152156014, 1363.22701537802, 1352.35826922741, 1340.05763917202, 1326.27646039112, 1310.96503686760, 1294.07263107601, 1275.54745367057, 1255.33665317321, 1233.38630566162, 1209.64140445729, 1184.04584981353, 1156.54243860351, 1127.07285400830, 1095.57765520491, 1061.99626705432, 1026.26696978950, 988.326888703502, 948.111983837415, 905.557039668468, 860.595654798034, 813.160231639676, 763.181966107179, 710.590837302589, 655.315597204248, 597.283760354830, 536.421593549378, 472.654105523336, 405.905036640590, 336.096848581505, 263.150714030957, 186.986506366370, 107.522789345752, 24.6768067957333, -61.6355277003971, -151.499641114664, -245.002311286354, -342.231677233991, -443.277249467305, -548.229920299177, -657.181974157624, -770.227097897749, -887.460391113704, -1008.97837645067, -1134.87900991681, -1265.26169119522, -1400.22727395591, -1539.87807616777, -1684.31789041053, -1833.65199418671, -1987.98716023361, -2147.43166683525, -2312.09530813433, -2482.08940444423, -2657.52681256095, -2838.52193607506, -3025.19073568368, -3217.65073950246, -3416.02105337752, -3620.42237119739, -3830.97698520506, -4047.80879630984, -4271.04332439939, -4500.80771865168, -4737.23076784691, -4980.44291067954, -5230.57624607019, -5487.76454347764, -5752.14325321078, -6023.84951674060, -6303.02217701210, -6589.80178875632, -6884.33062880224, -7186.75270638882, -7497.21377347687, -7815.86133506109, -8142.84465948200, -8478.31478873792, -8822.42454879690, -9175.32855990874, -9537.18324691690, -9908.14684957050, -10288.3794328362, -10678.0428972105, -11077.3009890310, -11486.3193107891, -11905.2653314417, -12334.3083967229, -12773.6197394564, -13223.3724898672, -13683.7416858936, -14154.9042834992, -14637.0391669847, -15130.3271593004, -15634.9510323573, -16151.0955173399, -16678.9473150177, -17218.6951060571, -17770.5295613338, -18334.6433522443, -18911.2311610182, -19500.4896910300, -20102.6176771109, -20717.8158958612, -21346.2871759619, -21988.2364084868, -22643.8705572143, -23313.3986689399, -23997.0318837874, -24694.9834455212, -25407.4687118587, -26134.7051647815, -26876.9124208478, -27634.3122415044, -28407.1285433985, -29195.5874086897, -29999.9170953619, -30820.3480475355, -31657.1129057792, -32510.4465174219, -33380.5859468646, -34267.7704858929, -35172.2416639883, -36094.2432586402, -37034.0213056587, -37991.8241094854, -




38967.9022535061, -39962.5086103628, -40975.8983522651, -42008.3289613028, -43060.0602397574, -44131.3543204143, -45222.4756768747, -46333.6911338676, -47465.2698775617, -48617.4834658774, -49790.6058387987, -50984.9133286854, -52200.6846705847, -53438.2010125435, -54697.7459259201, -55979.6054156963, -57284.0679307894, -58611.4243743641, -59961.9681141444, -61335.9949927258, -62733.8033378868, -64155.6939729015, -65601.9702268510, -67072.9379449358, -68568.9054987873, -70090.1837967801, -71637.0862943440, -73209.9290042758, -74809.0305070513, -76434.7119611372, -78087.2971133032, -79767.1123089339, -81474.4865023408, -83209.7512670741
};

double undistor_pt_lut[65] = {
0, 18.0235184490379, 36.0446579687234, 54.0610524949744, 72.0703621509529, 90.0702864733339, 108.058577409238, 126.033052006235, 143.991604722740, 161.932219292138, 179.852980081122, 197.752082890669, 215.627845156945, 233.478715518831, 251.303282728719, 269.100283893509, 286.868612043274, 304.607323035602, 322.315641814227, 339.992968050928, 357.638881209903, 375.253145083726, 392.835711859566, 410.386725783639, 427.906526500802, 445.395652154856, 462.854842343639, 480.285041031363, 497.687399529069, 515.063279662786, 532.414257257965, 549.742126078586, 567.048902369978, 584.336830166406, 601.608387538148, 618.866293968621, 636.113519070625, 653.353292872621, 670.589117931878, 687.824783562240, 705.064382501235, 722.312330385560, 739.573388457299, 756.852910553027, 774.157363297735, 791.492079919997, 808.863517677882, 826.278647022756, 843.745003935869, 861.270750596296, 878.864745769246, 896.536626578538, 914.296903665936, 932.157072159837, 950.129741398399, 968.228787006013, 986.469529744493, 1004.86894660114, 1023.44592090088, 1042.22153992785, 1061.21945073354, 1080.46628765810, 1100.00016097508, 1119.86959509757, 1140.10292498988
};

double distor_center[2] = {988.9514, 593.8084};
INT32 focal_length = 1170;
#else
//IMX291 with wide lens
double distor_pt_lut_4R[257] = {
0, 17.7187500302821, 35.4269870126385, 53.1143252082676, 70.7705467495221, 88.3856317538897, 105.949786356709, 123.453468541339, 140.887411677701, 158.242645710866, 175.510515970319, 192.682699597180, 209.751219610910, 226.708456658612, 243.547158508918, 260.260447368654, 276.841825114045, 293.285176539276, 309.584770733897, 325.735260707122, 341.731681381543, 357.569446081560, 373.244341642971, 388.752522269939, 404.090502264119, 419.255147748285, 434.243667503479, 449.053603034674, 463.682817975330, 478.129486936184, 492.392083898152, 506.469370243562, 520.360382514082, 534.064419977721, 547.581032081291, 560.910005858719, 574.051353359657, 587.005299157030, 599.772267986452, 612.352872564941, 624.747901631019, 636.958308243233, 648.985198369179, 660.829819792608, 672.493551361744, 683.977892597915, 695.284453679784, 706.414945814899, 717.371172007072, 728.155018225068, 738.768444975384, 749.213479279435, 759.492207053254, 769.606765885839, 779.559338210551, 789.352144862445, 798.987439013120, 808.467500473510, 817.794630354182, 826.971146071844, 835.999376690233, 844.881658583019, 853.620331406069, 862.217734366119, 870.676202772852, 878.998064861293, 887.185638871493, 895.241230372621, 903.167129818728, 910.965610323714, 918.638925643296, 926.189308352111, 933.618968204407, 940.930090667202, 948.124835615130, 955.205336176648, 962.173697721681, 969.031996981186, 975.782281289585, 982.426567941404, 988.966843653905, 995.405064127901, 1001.74315369934, 1007.98300507469, 1014.12647914350, 1020.17540486186, 1026.13157920102, 1031.99676715545, 1037.77270180543, 1043.46108442900, 1049.06358465903, 1054.58184068094, 1060.01745946724, 1065.37201704511, 1070.64705879361, 1075.84409976733, 1080.96462504348, 1086.01009008965, 1090.98192114978, 1095.88151564578, 1100.71024259284, 1105.46944302629, 1110.16043043809, 1114.78449122146, 1119.34288512181, 1123.83684569279, 1128.26758075591, 1132.63627286271, 1136.94407975828, 1141.19213484516, 1145.38154764670, 1149.51340426912, 1153.58876786138, 1157.60867907238, 1161.57415650477, 1165.48619716481, 1169.34577690790, 1173.15385087927, 1176.91135394950, 1180.61920114452, 1184.27828806978, 1187.88949132840, 1191.45366893304, 1194.97166071129, 1198.44428870450, 1201.87235755986, 1205.25665491569, 1208.59795177975, 1211.89700290072, 1215.15454713258, 1218.37130779205, 1221.54799300899, 1224.68529606984, 1227.78389575404, 1230.84445666354, 1233.86762954544, 1236.85405160774, 1239.80434682840, 1242.71912625763, 1245.59898831360, 1248.44451907159, 1251.25629254672, 1254.03487097029, 1256.78080505988, 1259.49463428327, 1262.17688711636, 1264.82808129504, 1267.44872406129, 1270.03931240347, 1272.60033329108, 1275.13226390387, 1277.63557185561, 1280.11071541254, 1282.55814370662, 1284.97829694365, 1287.37160660649, 1289.73849565333, 1292.07937871122, 1294.39466226498, 1296.68474484143, 1298.95001718933, 1301.19086245485, 1303.40765635281, 1305.60076733377, 1307.77055674712, 1309.91737900013, 1312.04158171314, 1314.14350587107, 1316.22348597113, 1318.28185016700, 1320.31892040950, 1322.33501258384, 1324.33043664352, 1326.30549674100, 1328.26049135516, 1330.19571341570, 1332.11145042453, 1334.00798457420, 1335.88559286343, 1337.74454720994, 1339.58511456044, 1341.40755699806, 1343.21213184711, 1344.99909177542, 1346.76868489409, 1348.52115485496, 1350.25674094566, 1351.97567818244, 1353.67819740077, 1355.36452534373, 1357.03488474835, 1358.68949442990, 1360.32856936410, 1361.95232076749, 1363.56095617582, 1365.15467952060, 1366.73369120388, 1368.29818817122, 1369.84836398303, 1371.38440888411, 1372.90650987168, 1374.41485076178, 1375.90961225411, 1377.39097199536, 1378.85910464114, 1380.31418191636, 1381.75637267430, 1383.18584295435, 1384.60275603828, 1386.00727250539, 1387.39955028628, 1388.77974471545, 1390.14800858268, 1391.50449218323, 1392.84934336695, 1394.18270758624, 1395.50472794292, 1396.81554523409, 1398.11529799692, 1399.40412255245, 1400.68215304843, 1401.94952150115, 1403.20635783643, 1404.45278992963, 1405.68894364478, 1406.91494287289, 1408.13090956937, 1409.33696379067, 141




0.53322373009, 1411.71980575281, 1412.89682443017, 1414.06439257320, 1415.22262126542, 1416.37161989496, 1417.51149618592, 1418.64235622914, 1419.76430451224, 1420.87744394906, 1421.98187590848, 1423.07770024258, 1424.16501531424, 1425.24391802416, 1426.31450383727, 1427.37686680865, 1428.43109960882, 1429.47729354861, 1430.51553860340, 1431.54592343691, 1432.56853542453, 1433.58346067610, 1434.59078405825, 1435.59058921627, 1436.58295859559, 1437.56797346270, 1438.54571392577, 1439.51625895476, 1440.47968640118
};

double undistor_pt_lut[65] = {
0, 17.7222692322158, 35.4550753881381, 53.2089007873239, 70.9941926737935, 88.8213894054504, 106.700945758248, 124.643357289111, 142.659183712290, 160.759071253450, 178.953773954327, 197.254173908232, 215.671300413193, 234.216348035032, 252.900693577448, 271.735911960177, 290.733791009792, 309.906345170671, 329.265828146283, 348.824744483332, 368.595860113438, 388.592211869120, 408.841046857249, 429.360147345403, 450.150183639302, 471.225661881268, 492.601393802905, 514.297633065954, 536.376270259598, 558.808214979990, 581.609835219918, 604.830196057766, 628.505798440141, 652.610327930112, 677.217717485156, 702.343939790223, 727.984833522334, 754.245675811870, 781.069186337092, 808.584986316929, 836.754422304552, 865.652499743348, 895.344480835882, 925.828341285478, 957.166358190048, 989.471133617199, 1022.75250906755, 1057.07631024434, 1092.51528534582, 1129.14478124536, 1167.04258080870, 1206.30989116345, 1247.07178651107, 1289.37224767974, 1333.36673821840, 1379.16953215405, 1426.94838128543, 1476.81922613926, 1528.97292696759, 1583.60605214428, 1640.92028474694, 1701.12188831089, 1764.56932261484, 1831.46179820356, 1902.16736236265
};

double distor_center[2] = {948.6517, 491.9434};
INT32 focal_length = 975;
#endif

#else //for IMX415
INT32 src_size[2] = {3840, 2160};
//IMX415 with lens TRC-2191B6
double distor_pt_lut_4R[257] = {
0, 35.1759341865169, 70.3569391212253, 105.548068004418, 140.754338940590, 175.980717390542, 211.232098623479, 246.513290169112, 281.828994269763, 317.183790332461, 352.582117381047, 388.028256508274, 423.526313327910, 459.080200426838, 494.693619817155, 530.370045388279, 566.112705359046, 601.924564729812, 637.808307734557, 673.766320292982, 709.800672462614, 745.913100890907, 782.104991267341, 818.377360775526, 854.730840545301, 891.165658104839, 927.681619832743, 964.278093410153, 1000.95399027284, 1037.70774806333, 1074.53731308296, 1111.44012274402, 1148.41308802184, 1185.45257590691, 1222.55439185694, 1259.71376224899, 1296.92531683157, 1334.18307117675, 1371.48040913223, 1408.81006527347, 1446.16410735579, 1483.53391876645, 1520.91018097676, 1558.28285599421, 1595.64116881454, 1632.97358987383, 1670.26781750065, 1707.51076036810, 1744.68851994598, 1781.78637295282, 1818.78875380803, 1855.67923708400, 1892.44051995817, 1929.05440466516, 1965.50178094886, 2001.76260851453, 2037.81589948091, 2073.63970083231, 2109.21107687072, 2144.50609166791, 2179.49979151753, 2214.16618738720, 2248.47823737064, 2282.40782913974, 2315.92576239669, 2349.00173132605, 2381.60430704688, 2413.70092006483, 2445.25784272422, 2476.24017166020, 2506.61181025079, 2536.33545106899, 2565.37255833493, 2593.68335036792, 2621.22678203856, 2647.96052722087, 2673.84096124437, 2698.82314334617, 2722.86079912309, 2745.90630298376, 2767.91066060071, 2788.82349136249, 2808.59301082575, 2827.16601316736, 2844.48785363650, 2860.50243100675, 2875.15217002823, 2888.37800387967, 2900.11935662050, 2910.31412564299, 2918.89866412432, 2925.80776347871, 2930.97463580949, 2934.33089636121, 2935.80654597175, 2935.32995352443, 2932.82783840008, 2928.22525292919, 2921.44556484395, 2912.41043973041, 2901.03982348052, 2887.25192474432, 2870.96319738193, 2852.08832291576, 2830.54019298251, 2806.22989178537, 2779.06667854604, 2748.95796995688, 2715.80932263300, 2679.52441556433, 2640.00503256779, 2597.15104473932, 2550.86039290601, 2501.02907007823, 2447.55110390167, 2390.31853910951, 2329.22141997445, 2264.14777276089, 2194.98358817696, 2121.61280382665, 2043.91728666193,
1961.77681543482, 1875.06906314952, 1783.66957951448, 1687.45177339453, 1586.28689526296, 1480.04401965364, 1368.59002761310, 1251.78958915266, 1129.50514570051, 1001.59689255380, 867.922761330780, 728.338402422866, 582.697167446757, 430.850091696533, 272.645876595763, 107.930872149595, -63.4509406031353, -241.657967137808, -426.851016991905, -619.193321312938, -818.850550406305, -1025.99083128321, -1240.78476520860, -1463.40544524898, -1694.02847382038, -1932.83198023622, -2179.99663825526, -2435.70568362940, -2700.14493165168, -2973.50279470411, -3255.97029980562, -3547.74110615992, -3849.01152270340, -4159.98052565306, -4480.84977605438, -4811.82363732924, -5153.10919282378, -5504.91626335636, -5867.45742476539, -6240.94802545729, -6625.60620395434, -7021.65290644262, -7429.31190431987, -7848.80981174340, -8280.37610317804, -8724.24313094393, -9180.64614276454, -9649.82329931447, -10132.0156917674, -10627.4673593441, -11136.4253068599, -11659.1395222732, -12195.8629942329, -12746.8517296266, -13312.3647711283, -13892.6642147462, -14488.0152273710, -15098.6860643234, -15724.9480869022, -16367.0757799323, -17025.3467693122, -17700.0418395622, -18391.4449513725, -19099.8432591506, -19825.5271285695, -20568.7901541156, -21329.9291766364, -22109.2443008888, -22907.0389130866, -23723.6196984484, -24559.2966587459, -25414.3831298513, -26289.1957992857, -27184.0547237665, -28099.2833467557, -29035.2085160074, -29992.1605011163, -30970.4730110648, -31970.4832117717, -32992.5317436396, -34036.9627391027, -35104.1238401753, -36194.3662159991, -37308.0445803914, -38445.5172093928, -39607.1459588155, -40793.2962817904, -42004.3372463161, -43240.6415528060, -44502.5855516362, -45790.5492606938, -47104.9163829246, -48446.0743238812, -49814.4142092702, -51210.3309025012, -52634.2230222336, -54086.4929599254, -55567.5468973804, -57077.7948242966, -58617.6505558138, -60187.5317500617, -61787.8599257075, -63419.0604795042, -65081.5627038383, -66775.7998042774, -68502.2089171187, -70261.2311269366, -72053.3114841303, -73878.8990224723, -75738.4467766558, -77632.4117998428, -79561.2551812121, -81525.4420635070, -83525.4416605832, -85561.7272749569, -87634.7763153525, -89745.0703142507, -91893.0949454361, -94079.3400415455, -96304.2996116153, -98568.4718586299, -100872.359197069, -103216.468270457, -105601.309968908, -108027.399446676, -110495.256139704, -113005.403783168, -115558.370429028, -118154.688463574, -120794.894624978, -123479.530020834, -126209.140145714, -128984.274898712, -131805.488600991, -134673.340013335, -137588.392353692
};

double undistor_pt_lut[65] = {
0, 35.1742427875927, 70.3434159235352, 105.502473250513, 140.646416689467, 175.770320659845, 210.869356056976, 245.938813635645, 280.974126657529, 315.970892670749, 350.924894302229, 385.832118957555, 420.688777338443, 455.491320704393, 490.236456822464, 524.921164566965, 559.542707149103, 594.098643974846, 628.586841147355, 663.005480648020, 697.353068247240, 731.628440212521, 765.830768896982, 799.959567306086, 834.014692754085, 867.996349734513, 901.905092140940, 935.741824985310, 969.507805771656, 1003.20464569288, 1036.83431082794, 1070.39912352641, 1103.90176417711, 1137.34527356803, 1170.73305605604, 1204.06888377761, 1237.35690214648, 1270.60163690116, 1303.80800298529, 1336.98131556790, 1370.12619224921, 1403.25054216031, 1436.36138711482, 1469.46593802596, 1502.57190525464, 1535.68752940922, 1568.82161740684, 1601.98358452807, 1635.18350330947, 1668.43216025464, 1701.74112150826, 1735.12280883604, 1768.59058749342, 1802.15886785817, 1835.84322305779, 1869.66052525834, 1903.62910381654, 1937.76892915726, 1972.10182705728, 2006.65172903560, 2041.44496582779, 2076.51061253005, 2111.88089603577, 2147.59167798535, 2183.68302978353
};

double distor_center[2] = {1971.80633643159, 1072.76489350867};
INT32 focal_length = 2416;
#endif

#define GYRO_FXPT_PREC (1 << 16)
static HD_RESULT init_eis(void)
{
	VENDOR_EIS_OPEN_CFG test_open_obj = {0};
	UINT32 i;
	HD_RESULT ret;

	test_open_obj.cam_intrins.distor_center[0] = distor_center[0]; //pixel
	test_open_obj.cam_intrins.distor_center[1] = distor_center[1]; //pixel
	for (i = 0; i < DISTOR_CURVE_TABLE_NUM; i++) {
		test_open_obj.cam_intrins.distor_curve[i] = distor_pt_lut_4R[i];
	}
	for (i = 0; i < UNDISTOR_CURVE_TABLE_NUM; i++) {
		test_open_obj.cam_intrins.undistor_curve[i] = undistor_pt_lut[i];
	}
	test_open_obj.cam_intrins.calib_img_size.w = src_size[0]; //pixel
	test_open_obj.cam_intrins.calib_img_size.h = src_size[1]; //pixel

	test_open_obj.cam_intrins.focal_length = focal_length; //pixel

    test_open_obj.imu_sync_shift_exposure_time_threshold = 15000; //us
    test_open_obj.imu_sync_shift_exposure_time_precent = 30;

	test_open_obj.imu_type = VENDOR_EIS_GYROSCOPE; //
	test_open_obj.gyro.axes_mapping[0].axis = VENDOR_EIS_CAM_X; //Ex. axis = CAM_Z, sign = -1;
	test_open_obj.gyro.axes_mapping[0].sign = -1;
	test_open_obj.gyro.axes_mapping[1].axis = VENDOR_EIS_CAM_Y;
	test_open_obj.gyro.axes_mapping[1].sign = 1;
	test_open_obj.gyro.axes_mapping[2].axis = VENDOR_EIS_CAM_Z;
	test_open_obj.gyro.axes_mapping[2].sign = -1;
	test_open_obj.gyro.sampling_rate = 30*GYRO_DATA_NUM; //Ex. 1000 Hz
	test_open_obj.gyro.unit_conv = 1000.0/32768.0/180.0*VPRC_PI;  //Ex. gyro_value*unit_conv = rad/s

	if (vendor_isp_init() == HD_ERR_NG) {
		return -1;
	}

	ret = vendor_eis_open(&test_open_obj);

	{
		VENDOR_EIS_PATH_INFO test_path_obj = {0};
		BOOL lib_set_status = FALSE;
		INT32 frame_rate;
		INT32 gyro_sample_per_frame;
		INT32 crop_percent;
		BOOL switch_val;
		VENDOR_EIS_IMG_ROTATE_SEL rot_sel;

		test_path_obj.path_id = 0;
		test_path_obj.frame_latency = 4;
		test_path_obj.frame_size.w = src_size[0];
		test_path_obj.frame_size.h = src_size[1];
		test_path_obj.lut2d_size_sel = lut2d_path0.lut2d.lut_sz;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_PATH_INFO, &test_path_obj);
		printf("Set EIS_RSC_PARAM_PATH_INFO: %s\r\n", lib_set_status ? "Success" : "Failed");

#if 0
		test_path_obj.path_id = 1;
		test_path_obj.frame_latency = 1;
		test_path_obj.frame_size.w = src_size[0];
		test_path_obj.frame_size.h = src_size[1];
		test_path_obj.lut2d_size_sel = VENDOR_EIS_LUT_65x65;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_PATH_INFO, &test_path_obj);
		printf("Set EIS_RSC_PARAM_PATH_INFO: %s\r\n", lib_set_status ? "Success" : "Failed");
#endif
		switch_val = FALSE;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_FRAME_CNT_RESET, &switch_val);
		printf("Set EIS_RSC_PARAM_FRAME_CNT_RESET: %s\r\n", lib_set_status ? "Success" : "Failed");

		frame_rate = 30;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_FRAME_RATE, &frame_rate);
		printf("Set EIS_RSC_PARAM_FRAME_RATE: %s\r\n", lib_set_status ? "Success" : "Failed");

		gyro_sample_per_frame = GYRO_DATA_NUM;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_IMU_SAMPLE_NUM_PER_FRAME, &gyro_sample_per_frame);
		printf("Set EIS_RSC_PARAM_IMU_SAMPLE_NUM_PER_FRAME: %s\r\n", lib_set_status ? "Success" : "Failed");

		rot_sel = VENDOR_EIS_ROT_0;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_ROTATE_TYPE, &rot_sel);
		printf("Set EIS_RSC_PARAM_ROTATE_TYPE: %s\r\n", lib_set_status ? "Success" : "Failed");

		crop_percent = 20;
		lib_set_status = vendor_eis_set(VENDOR_EIS_PARAM_COMPEN_CROP_PERCENT, &crop_percent);
		printf("Set EIS_RSC_PARAM_COMPEN_CROP_PERCENT: %s\r\n", lib_set_status ? "Success" : "Failed");
	}

	return ret;
}
static HD_RESULT init_vpe(void)
{
	HD_RESULT ret;
	VPET_DCE_CTL_PARAM dce_ctl = {0};

	if ((ret = vendor_vpe_init()) != HD_OK) {
		printf("vendor_vpe_init failed!(%d)\r\n", ret);
		return ret;
	}

	dce_ctl.id = ISP_EFFECT_ID;
	dce_ctl.dce_ctl.enable = 1;
	dce_ctl.dce_ctl.dce_mode = 1; // 0:GDC, 1:2DLUT
	dce_ctl.dce_ctl.lens_radius = 0;
	vendor_vpe_set_cmd(VPET_ITEM_DCE_CTL_PARAM, &dce_ctl);

	lut2d_path0.id = ISP_EFFECT_ID;
	gen_default_2dlut(lut2d_path0.lut2d.lut, VDO_SIZE_W, VDO_SIZE_H, 9);
	vendor_vpe_set_cmd(VPET_ITEM_2DLUT_PARAM, &lut2d_path0);
	return ret;
}
static HD_RESULT open_module(VIDEO_RECORD *p_stream, HD_DIM* p_proc0_max_dim, HD_DIM* p_proc1_max_dim)
{
	HD_RESULT ret;
	// set videocap config
	ret = set_cap_cfg(&p_stream->cap_ctrl);
	if (ret != HD_OK) {
		printf("set cap-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set 1st videoproc config
	ret = set_proc_cfg(HD_VIDEOPROC_0_CTRL, &p_stream->proc0_ctrl, HD_VIDEOPROC_PIPE_RAWALL, p_proc0_max_dim);
	if (ret != HD_OK) {
		printf("set proc-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set 2nd videoproc config
	ret = set_proc_cfg(HD_VIDEOPROC_1_CTRL, &p_stream->proc1_ctrl, HD_VIDEOPROC_PIPE_VPE, p_proc1_max_dim);
	if (ret != HD_OK) {
		printf("set proc-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}

	if ((ret = hd_videocap_open(HD_VIDEOCAP_0_IN_0, HD_VIDEOCAP_0_OUT_0, &p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_0, &p_stream->proc0_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_1_IN_0, HD_VIDEOPROC_1_OUT_0, &p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoenc_open(HD_VIDEOENC_0_IN_0, HD_VIDEOENC_0_OUT_0, &p_stream->enc_path)) != HD_OK)
		return ret;

	return HD_OK;
}

static HD_RESULT open_module_2(VIDEO_RECORD *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_1_IN_0, HD_VIDEOPROC_1_OUT_1,  &p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoenc_open(HD_VIDEOENC_0_IN_1, HD_VIDEOENC_0_OUT_1,  &p_stream->enc_path)) != HD_OK)
		return ret;

	return HD_OK;
}
static HD_RESULT open_module_3(VIDEO_RECORD *p_stream,UINT32 out_type)
{
	HD_RESULT ret;
	// set videoout config
	ret = set_out_cfg(&p_stream->out_ctrl, out_type, p_stream->hdmi_id);
	if (ret != HD_OK) {
		printf("set out-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}

	if ((ret = hd_videoproc_open(HD_VIDEOPROC_1_IN_0, HD_VIDEOPROC_1_OUT_2,  &p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoout_open(HD_VIDEOOUT_0_IN_0, HD_VIDEOOUT_0_OUT_0,  &p_stream->out_path)) != HD_OK)
		return ret;

	return HD_OK;
}

static HD_RESULT close_module(VIDEO_RECORD *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_close(p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_close(p_stream->proc0_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_close(p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoenc_close(p_stream->enc_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT close_module_2(VIDEO_RECORD *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videoproc_close(p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoenc_close(p_stream->enc_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT close_module_3(VIDEO_RECORD *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videoproc_close(p_stream->proc1_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoout_close(p_stream->out_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT exit_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoenc_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoout_uninit()) != HD_OK)
		return ret;
	return HD_OK;
}

static void *encode_thread(void *arg)
{
	VIDEO_RECORD* p_stream0 = (VIDEO_RECORD *)arg;
	VIDEO_RECORD* p_stream1 = p_stream0 + 1;
	HD_RESULT ret = HD_OK;
	HD_VIDEOENC_BS  data_pull;
	UINT32 j;

	UINT32 vir_addr_main;
	HD_VIDEOENC_BUFINFO phy_buf_main;
	char file_path_main[32] = "/mnt/sd/dump_bs_main.dat";
	FILE *f_out_main;
	#define PHY2VIRT_MAIN(pa) (vir_addr_main + (pa - phy_buf_main.buf_info.phy_addr))
	UINT32 vir_addr_sub;
	HD_VIDEOENC_BUFINFO phy_buf_sub;
	char file_path_sub[32]  = "/mnt/sd/dump_bs_sub.dat";
	FILE *f_out_sub;
	#define PHY2VIRT_SUB(pa) (vir_addr_sub + (pa - phy_buf_sub.buf_info.phy_addr))
	HD_VIDEOENC_POLL_LIST poll_list[2];
	UINT32 poll_num = 2;

	//------ wait flow_start ------
	while (p_stream0->flow_start == 0) sleep(1);

	// query physical address of bs buffer ( this can ONLY query after hd_videoenc_start() is called !! )
	hd_videoenc_get(p_stream0->enc_path, HD_VIDEOENC_PARAM_BUFINFO, &phy_buf_main);
	hd_videoenc_get(p_stream1->enc_path, HD_VIDEOENC_PARAM_BUFINFO, &phy_buf_sub);

	// mmap for bs buffer (just mmap one time only, calculate offset to virtual address later)
	vir_addr_main = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, phy_buf_main.buf_info.phy_addr, phy_buf_main.buf_info.buf_size);
	vir_addr_sub  = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, phy_buf_sub.buf_info.phy_addr, phy_buf_sub.buf_info.buf_size);

	//----- open output files -----
	if ((f_out_main = fopen(file_path_main, "wb")) == NULL) {
		HD_VIDEOENC_ERR("open file (%s) fail....\r\n", file_path_main);
	} else {
		printf("\r\ndump main bitstream to file (%s) ....\r\n", file_path_main);
	}
	if ((f_out_sub = fopen(file_path_sub, "wb")) == NULL) {
		HD_VIDEOENC_ERR("open file (%s) fail....\r\n", file_path_sub);
	} else {
		printf("\r\ndump sub  bitstream to file (%s) ....\r\n", file_path_sub);
	}

	printf("\r\nif you want to stop, enter \"q\" to exit !!\r\n\r\n");

	//--------- pull data test ---------
	poll_list[0].path_id = p_stream0->enc_path;
	poll_list[1].path_id = p_stream1->enc_path;


	//--------- pull data test ---------
	while (p_stream0->enc_exit == 0) {
		if (g_enc_poll && HD_OK == hd_videoenc_poll_list(poll_list, poll_num, -1)) {    // multi path poll_list , -1 = blocking mode
			if (TRUE == poll_list[0].revent.event) {
				//pull data
				ret = hd_videoenc_pull_out_buf(p_stream0->enc_path, &data_pull, -1); // -1 = blocking mode

				if (ret == HD_OK) {
					for (j=0; j< data_pull.pack_num; j++) {
						UINT8 *ptr = (UINT8 *)PHY2VIRT_MAIN(data_pull.video_pack[j].phy_addr);
						UINT32 len = data_pull.video_pack[j].size;
						if (g_write_file) {
							if (f_out_main) fwrite(ptr, 1, len, f_out_main);
							if (f_out_main) fflush(f_out_main);
						}
					}

					// release data
					ret = hd_videoenc_release_out_buf(p_stream0->enc_path, &data_pull);
				}
			}

			if (TRUE == poll_list[1].revent.event) {
				//pull data
				ret = hd_videoenc_pull_out_buf(p_stream1->enc_path, &data_pull, -1); // -1 = blocking mode
				if (ret == HD_OK) {
					for (j=0; j< data_pull.pack_num; j++) {
						UINT8 *ptr = (UINT8 *)PHY2VIRT_SUB(data_pull.video_pack[j].phy_addr);
						UINT32 len = data_pull.video_pack[j].size;
						if (g_write_file) {
							if (f_out_sub) fwrite(ptr, 1, len, f_out_sub);
							if (f_out_sub) fflush(f_out_sub);
						}
					}

					// release data
					ret = hd_videoenc_release_out_buf(p_stream1->enc_path, &data_pull);
				}
			}
		} else {
			usleep(100000);
		}
	}

	// mummap for bs buffer
	hd_common_mem_munmap((void *)vir_addr_main, phy_buf_main.buf_info.buf_size);
	hd_common_mem_munmap((void *)vir_addr_sub, phy_buf_sub.buf_info.buf_size);

	// close output file
	if (f_out_main) fclose(f_out_main);
	if (f_out_sub) fclose(f_out_sub);

	return 0;
}

MAIN(argc, argv)
{
	HD_RESULT ret;
	INT key;
	VIDEO_RECORD stream[3] = {0}; //0: main stream, 1: sub stream,2: liveview
	UINT32 enc_type = 0;
	HD_DIM main_dim;
	HD_DIM sub_dim;
	UINT32 out_type = 1;
	AET_CFG_INFO cfg_info = {0};
	AET_EXPT_BOUND expt_bound = {0};

	// query program options

	if (argc == 1 || argc > 4) {
		printf("Usage: <enc-type> <cap_out_bind> <cap_out_fmt> <prc_out_bind> <prc_out_fmt>.\r\n");
		printf("Help:\r\n");
		printf("  <enc_type> : 0(H265), 1(H264), 2(MJPG)\r\n");
		printf("  <cap_out_bind>: 0(D2D Mode), 1(Direct Mode), 2(OneBuf Mode)\r\n");
		printf("  <sensor> :  0(IMX290) , 1(IMX415)\r\n");
		return 0;
	}


	if (argc > 1) {
		enc_type = atoi(argv[1]);
		printf("enc_type %d\r\n", enc_type);
		if(enc_type > 2) {
			printf("error: not support enc_type!\r\n");
			return 0;
		}
	}

	if (argc > 2) {
		if (argv[2][0] == 'x') {
			g_capbind = 0xff;
			printf("CAP-BIND x\r\n");
		} else {
			g_capbind = atoi(argv[2]);
			printf("CAP-BIND %d\r\n", g_capbind);
			if(g_capbind > 3) {
				printf("error: not support CAP-BIND! (%d) \r\n", g_capbind);
				return 0;
			}
		}
	}
	if (argc > 3) {
		sensor_sel = atoi(argv[3]);
		printf("sensor_sel %d\r\n", sensor_sel);
		if(sensor_sel > 2) {
			printf("error: not support sensor_sel!\r\n");
			return 0;
		}
	}

    if (sensor_sel == SEN_SEL_IMX415) {
		VDO_SIZE_W = VDO_SIZE_W_8M;
		VDO_SIZE_H = VDO_SIZE_H_8M;
		vdo_br = 8 * 1024 *1024;
	} else {
		VDO_SIZE_W = VDO_SIZE_W_2M;
		VDO_SIZE_H = VDO_SIZE_H_2M;
		vdo_br = 2 * 1024 *1024;
	}

     printf("main %d*%d vdo_br=%d \r\n",VDO_SIZE_W,VDO_SIZE_H,vdo_br);

	if (vendor_isp_init() != HD_ERR_NG) {
		cfg_info.id = 0;
		if (sensor_sel == SEN_SEL_IMX415) {
			strncpy(cfg_info.path, "/mnt/app/isp/isp_imx415_0.cfg", CFG_NAME_LENGTH);
		} else {
			strncpy(cfg_info.path, "/mnt/app/isp/isp_imx290_0.cfg", CFG_NAME_LENGTH);
		}
		printf("sensor 1 load %s \n", cfg_info.path);
		vendor_isp_set_ae(AET_ITEM_RLD_CONFIG, &cfg_info);
		expt_bound.id = 0;
		expt_bound.bound.l = (UINT32)500;
		expt_bound.bound.h = (UINT32)8000;
		vendor_isp_set_ae(AET_ITEM_EXPT_BOUND, &expt_bound);
		vendor_isp_set_awb(AWBT_ITEM_RLD_CONFIG, &cfg_info);
		vendor_isp_set_iq(IQT_ITEM_RLD_CONFIG, &cfg_info);
		vendor_isp_uninit();
	}

	// init hdal
	ret = hd_common_init(0);
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
		goto exit;
	}

	// init memory
	ret = mem_init();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
		goto exit;
	}

	// init all modules
	ret = init_module();
	if (ret != HD_OK) {
		printf("init fail=%d\n", ret);
		goto exit;
	}

	// init EIS lib
	ret = init_eis();
	if (ret != HD_OK) {
		printf("eis init fail=%d\n", ret);
		goto exit;
	}
	ret = init_vpe();
	if (ret != HD_OK) {
		printf("vpe init fail=%d\n", ret);
		goto exit;
	}

	// open video_record modules (pre-process)
	stream[0].proc0_max_dim.w = VDO_SIZE_W; //assign by user
	stream[0].proc0_max_dim.h = VDO_SIZE_H; //assign by user
	// open video_record modules (main)
	stream[0].proc1_max_dim.w = VDO_SIZE_W; //assign by user
	stream[0].proc1_max_dim.h = VDO_SIZE_H; //assign by user
	ret = open_module(&stream[0], &stream[0].proc0_max_dim, &stream[0].proc1_max_dim);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}

	// open video_record modules (sub)
	ret = open_module_2(&stream[1]);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}


	// open vout modules (display)
	ret = open_module_3(&stream[2], out_type);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}

	// get videocap capability
	ret = get_cap_caps(stream[0].cap_ctrl, &stream[0].cap_syscaps);
	if (ret != HD_OK) {
		printf("get cap-caps fail=%d\n", ret);
		goto exit;
	}

	// get videoout capability
	ret = get_out_caps(stream[2].out_ctrl, &stream[2].out_syscaps);
	if (ret != HD_OK) {
		printf("get out-caps fail=%d\n", ret);
		goto exit;
	}

    if (sensor_sel == SEN_SEL_IMX415) {
    	stream[2].out_dim.w = VDO_SIZE_W/6; //using vpe/8 and ide scale
    	stream[2].out_dim.h = VDO_SIZE_H/6; //using vpe/8 and ide scale
    } else {
	    stream[2].out_max_dim = stream[2].out_syscaps.output_dim;
    }
	// set videocap parameter
	stream[0].cap_dim.w = VDO_SIZE_W; //assign by user
	stream[0].cap_dim.h = VDO_SIZE_H; //assign by user
	ret = set_cap_param(stream[0].cap_path, &stream[0].cap_dim);
	if (ret != HD_OK) {
		printf("set cap fail=%d\n", ret);
		goto exit;
	}

	// assign parameter by program options
	main_dim.w = VDO_SIZE_W;
	main_dim.h = VDO_SIZE_H;
	sub_dim.w = SUB_VDO_SIZE_W;
	sub_dim.h = SUB_VDO_SIZE_H;

	// set videoproc parameter (pre-process)
	stream[0].proc1_dim.w = main_dim.w;
	stream[0].proc1_dim.h = main_dim.h;
	ret = set_proc_param(stream[0].proc0_path, &stream[0].proc1_dim);
	if (ret != HD_OK) {
		printf("set proc fail=%d\n", ret);
		goto exit;
	}

	// set videoproc parameter (main)
	stream[0].proc1_dim.w = main_dim.w;
	stream[0].proc1_dim.h = main_dim.h;
	ret = set_proc_param(stream[0].proc1_path, NULL);
	if (ret != HD_OK) {
		printf("set proc fail=%d\n", ret);
		goto exit;
	}

	stream[1].proc1_dim.w = sub_dim.w;
	stream[1].proc1_dim.h = sub_dim.h;
	// set videoproc parameter (sub)
	ret = set_proc_param(stream[1].proc1_path, NULL);
	if (ret != HD_OK) {
		printf("set proc fail=%d\n", ret);
		goto exit;
	}

    //vpe scale ratio is 1/8,ide scale ratio is 1/2
    if (sensor_sel == SEN_SEL_IMX415) {
	// set videoout parameter (main)
	stream[2].out_dim.w = VDO_SIZE_W/6; //using vpe/8 and ide scale
	stream[2].out_dim.h = VDO_SIZE_H/6; //using vpe/8 and ide scale

    } else {
	// set videoout parameter (main)
	stream[2].out_dim.w = stream[2].out_max_dim.w; //using device max dim.w
	stream[2].out_dim.h = stream[2].out_max_dim.h; //using device max dim.h
    }
	ret = set_out_param(stream[2].out_path, &stream[2].out_dim);
	if (ret != HD_OK) {
		printf("set out fail=%d\n", ret);
		goto exit;
	}

	// set videoenc config (main)
	stream[0].enc_max_dim.w = main_dim.w;
	stream[0].enc_max_dim.h = main_dim.h;
	ret = set_enc_cfg(stream[0].enc_path, &stream[0].enc_max_dim, vdo_br);
	if (ret != HD_OK) {
		printf("set enc-cfg fail=%d\n", ret);
		goto exit;
	}

	// set videoenc parameter (main)
	stream[0].enc_dim.w = main_dim.w;
	stream[0].enc_dim.h = main_dim.h;
	ret = set_enc_param(stream[0].enc_path, &stream[0].enc_dim, enc_type, vdo_br);
	if (ret != HD_OK) {
		printf("set enc fail=%d\n", ret);
		goto exit;
	}

	// set videoenc config (sub)
	stream[1].enc_max_dim.w = sub_dim.w;
	stream[1].enc_max_dim.h = sub_dim.h;
	ret = set_enc_cfg(stream[1].enc_path, &stream[1].enc_max_dim, 1 * 1024 * 1024);
	if (ret != HD_OK) {
		printf("set enc-cfg fail=%d\n", ret);
		goto exit;
	}

	// set videoenc parameter (sub)
	stream[1].enc_dim.w = sub_dim.w;
	stream[1].enc_dim.h = sub_dim.h;
	ret = set_enc_param(stream[1].enc_path, &stream[1].enc_dim, enc_type, 1 * 1024 * 1024);
	if (ret != HD_OK) {
		printf("set enc fail=%d\n", ret);
		goto exit;
	}

	// bind video_record modules (main)
	hd_videocap_bind(HD_VIDEOCAP_0_OUT_0, HD_VIDEOPROC_0_IN_0);
	hd_videoproc_bind(HD_VIDEOPROC_0_OUT_0, HD_VIDEOPROC_1_IN_0);
	hd_videoproc_bind(HD_VIDEOPROC_1_OUT_0, HD_VIDEOENC_0_IN_0);

	// bind video_record modules (sub)
	hd_videoproc_bind(HD_VIDEOPROC_1_OUT_1, HD_VIDEOENC_0_IN_1);

	// bind vout modules (display)
	hd_videoproc_bind(HD_VIDEOPROC_1_OUT_2, HD_VIDEOOUT_0_IN_0);

	// create encode_thread (pull_out bitstream)
	ret = pthread_create(&stream[0].enc_thread_id, NULL, encode_thread, (void *)stream);
	if (ret < 0) {
		printf("create encode thread failed");
		goto exit;
	}

	if (g_capbind == 1) {
		//direct NOTE: ensure videocap start after 1st videoproc phy path start
		hd_videoproc_start(stream[0].proc0_path);
		hd_videocap_start(stream[0].cap_path);
	} else {
		hd_videocap_start(stream[0].cap_path);
		hd_videoproc_start(stream[0].proc0_path);
	}
	// start video_record modules (main)
	hd_videoproc_start(stream[0].proc1_path);
	// start video_record modules (sub)
	hd_videoproc_start(stream[1].proc1_path);
	// start vout modules (display)
	hd_videoproc_start(stream[2].proc1_path);

	// just wait ae/awb stable for auto-test, if don't care, user can remove it
	sleep(1);
	hd_videoenc_start(stream[0].enc_path);
	hd_videoenc_start(stream[1].enc_path);

	hd_videoout_start(stream[2].out_path);

	// let encode_thread start to work
	stream[0].flow_start= 1;

	// query user key
	printf("Enter q to exit\n");
	while (1) {
		key = GETCHAR();
		if (key == 'q' || key == 0x3) {
			// let encode_thread stop loop and exit
			stream[0].enc_exit = 1;
			// quit program
			break;
		}
        if (key == 'c') {
			printf("start rtsp\r\n");
            g_enc_poll = 0;
			system("nvtrtspd_ipc &");
		}

		#if (DEBUG_MENU == 1)
		if (key == 'd') {
			// enter debug menu
			hd_debug_run_menu();
			printf("\r\nEnter q to exit, Enter d to debug\r\n");
		}
		#endif
	}

	// destroy encode thread
	pthread_join(stream[0].enc_thread_id, NULL);

	if (g_capbind == 1) {
		//direct NOTE: ensure videocap stop after all videoproc path stop
		hd_videoproc_stop(stream[0].proc0_path);
		hd_videocap_stop(stream[0].cap_path);
	} else {
		hd_videocap_stop(stream[0].cap_path);
		hd_videoproc_stop(stream[0].proc0_path);
	}
	 // stop video_record modules (main)
	hd_videoproc_stop(stream[0].proc1_path);
    // stop video_record modules (sub)
	hd_videoproc_stop(stream[1].proc1_path);
    // stop vout modules (dispaly)
	hd_videoproc_stop(stream[2].proc1_path);

	hd_videoenc_stop(stream[0].enc_path);
	hd_videoenc_stop(stream[1].enc_path);
	hd_videoout_stop(stream[2].out_path);

	// unbind video_record modules (main)
	hd_videocap_unbind(HD_VIDEOCAP_0_OUT_0);
	hd_videoproc_unbind(HD_VIDEOPROC_0_OUT_0);
	hd_videoproc_unbind(HD_VIDEOPROC_1_OUT_0);

	// unbind video_record modules (sub)
	hd_videoproc_unbind(HD_VIDEOPROC_1_OUT_1);
	// unbind vout modules (display)
	hd_videoproc_unbind(HD_VIDEOPROC_1_OUT_2);

exit:
	// close video_record modules (main)
	ret = close_module(&stream[0]);
	if (ret != HD_OK) {
		printf("close fail=%d\n", ret);
	}

	// close video_record modules (sub)
	ret = close_module_2(&stream[1]);
	if (ret != HD_OK) {
		printf("close fail=%d\n", ret);
	}
	// close vout modules (display)
	ret = close_module_3(&stream[2]);
	if (ret != HD_OK) {
		printf("close fail=%d\n", ret);
	}

	vendor_vpe_uninit();
	ret = vendor_eis_close();
	if (ret != HD_OK) {
		printf("eis close fail=%d\n", ret);
	}

	// uninit all modules
	ret = exit_module();
	if (ret != HD_OK) {
		printf("exit fail=%d\n", ret);
	}

	// uninit memory
	ret = mem_exit();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
	}

	// uninit hdal
	ret = hd_common_uninit();
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
	}

	return 0;
}
