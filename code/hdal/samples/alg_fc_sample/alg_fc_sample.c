/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#if defined(__LINUX)
#include <pthread.h>
#else
#include <FreeRTOS_POSIX.h>	
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#endif
#include "hdal.h"
#include "hd_type.h"
#include "hd_common.h"
//#include "nvtipc.h"
//#include "vendor_dsp_util.h"
#include "vendor_ai/vendor_ai.h"

#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include "net_flow_user_sample/net_layer_sample.h"
//#include "custnn/custnn_lib.h"
#include <sys/time.h>

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME

#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080

#define YUV_OUT_BUF_SIZE        (3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT)

#define MBYTE					(1024 * 1024)
#define NN_TOTAL_MEM_SIZE       (200 * MBYTE)
#define NN_RUN_NET_NUM			2

#define MAX_OBJ_NUM             1024


#define VENDOR_AI_CFG  0x000f0000  //ai project config

#define PROF                    ENABLE
#if PROF
	static struct timeval tstart, tend;
	#define PROF_START()    gettimeofday(&tstart, NULL);
	#define PROF_END(msg)   gettimeofday(&tend, NULL);  \
			printf("%s time (us): %lu\r\n", msg,         \
					(tend.tv_sec - tstart.tv_sec) * 1000000 + (tend.tv_usec - tstart.tv_usec));
#else
	#define PROF_START()
	#define PROF_END(msg)
#endif

#define FC_USE_LL 1
/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
static HD_COMMON_MEM_VB_BLK g_blk_info[2];
static BOOL is_fc_proc	= TRUE;
static BOOL is_fc_run	= FALSE;
UINT32 fc_weight_mem_va = 0;
UINT32 fc_weight_mem_pa = 0;
UINT32 fc_mem_va = 0;
UINT32 fc_mem_pa = 0;


/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/
static int mem_init(void)
{
	HD_RESULT                 ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};
	UINT32                    pa, va;
	HD_COMMON_MEM_VB_BLK      blk;

	/* Allocate parameter buffer */
	if (fc_mem_va != 0) {
		printf("err: mem has already been inited\r\n");
		return -1;
	}

	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = NN_TOTAL_MEM_SIZE;
	mem_cfg.pool_info[0].blk_cnt = 1;
	mem_cfg.pool_info[0].ddr_id = DDR_ID0;
	mem_cfg.pool_info[1].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[1].blk_size = NN_TOTAL_MEM_SIZE;
	mem_cfg.pool_info[1].blk_cnt = 1;
	mem_cfg.pool_info[1].ddr_id = DDR_ID1;

	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		printf("hd_common_mem_init err: %d\r\n", ret);
		return ret;
	}
	
	// allocate IO buffer from dram1
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, NN_TOTAL_MEM_SIZE, mem_cfg.pool_info[0].ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		printf("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, NN_TOTAL_MEM_SIZE);
	g_blk_info[0] = blk;

	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	fc_mem_pa = pa;
	fc_mem_va = va;

	// allocate weight buffer from dram2
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, NN_TOTAL_MEM_SIZE, mem_cfg.pool_info[1].ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	fc_weight_mem_pa = hd_common_mem_blk2pa(blk);
	if (fc_weight_mem_pa == 0) {
		printf("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			printf("fc_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	fc_weight_mem_va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, fc_weight_mem_pa, NN_TOTAL_MEM_SIZE);
	g_blk_info[1] = blk;

exit:
	return ret;
}

static HD_RESULT mem_uninit(void)
{
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (fc_mem_va) {
		ret = hd_common_mem_munmap((void *)fc_mem_va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (fc_mem_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		printf("mem_uninit : (fc_mem_pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release scale out buffer */
	if (fc_weight_mem_va) {
		ret = hd_common_mem_munmap((void *)fc_weight_mem_va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (fc_weight_mem_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	ret = hd_common_mem_release_block(g_blk_info[1]);
	if (ret != HD_OK) {
		printf("mem_uninit : (fc_weight_mem_pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	return hd_common_mem_uninit();
}




/*
	The code below is the flow of using FC 
	suppose the input feature size is 256 bytes (defined as SV_FEA_LENGTH)
	and the desired output length is 10240 (defined as SV_LENGTH)
	the following sample will transpose the input 256 bytes feature (1 byte per element) into 10240*4 bytes feature (4 bytes per element)
	
	fc_init_param is for setting parameter of FC
	user should set input/output/weight address
*/
#define SV_LENGTH 10240
#define SV_FEA_LENGTH 9216
INT32 fc_init_parm(KDRV_AI_FC_PARM* p_param, UINT32 input_addr, UINT32 output_addr, UINT32 weight_addr)
{
	if (p_param == NULL) {
		printf("input parameter is NULL\n");
		return -1;
	}
	//printf("input addr  = 0x%08X\n", (unsigned int)input_addr);
	//printf("output addr = 0x%08X\n", (unsigned int)output_addr);
	//printf("weight addr = 0x%08X\n", (unsigned int)weight_addr);
	p_param->in_type = AI_IO_INT8;
	p_param->in_addr = input_addr;                 //< input address     (size = SV_FEA_LENGTH bytes)
	p_param->out_interm_addr   = output_addr;      //< output address    (size = SV_LENGTH*4 bytes)
	p_param->fc_ker.weight_addr = weight_addr;     //< sv weight address (size = SV_LENGTH*SV_FEA_LENGTH bytes)
	p_param->fc_ker.weight_w = SV_FEA_LENGTH;      //< sv weight width
	p_param->fc_ker.weight_h = SV_LENGTH;          //< sv weight height
	p_param->fc_ker.weight_shf = 8;
	p_param->fc_ker.acc_shf = 0;
	p_param->fc_ker.norm_scl = 1;
	p_param->fc_ker.norm_shf = 3;
	

	return 0;
}

/*
	fc_thread_api is the main flow of running fc
*/

static VOID *fc_thread_api(VOID *arg)
{
	HD_RESULT ret;
#if	FC_USE_LL
	UINT32 parm_offset = SV_FEA_LENGTH + SV_LENGTH*SV_FEA_LENGTH + SV_LENGTH*4;
	KDRV_AI_FC_PARM* p_fc_param = NULL;
	KDRV_AI_FC_INFO fc_info = {0};
	UINT32 parm_range = 0;
#else
	KDRV_AI_FC_PARM fc_param = {0};
#endif
	UINT32 input_offset = 0;
	UINT32 output_offset = SV_FEA_LENGTH; 


	// init input address for test
	memset((VOID *)fc_mem_va, 1, SV_FEA_LENGTH); // IO buffer allocate in dram1
	memset((VOID *)fc_weight_mem_va, 1, SV_LENGTH*SV_FEA_LENGTH); // Weight buffer allocate in dram2
	memset((VOID *)(fc_mem_va + output_offset), 0, SV_LENGTH*4);  // IO buffer allocate in dram1
	

	
	
	// start thread loop
	// 1. init nue drv
	ret = vendor_ai_drv_nue_init();
	if (ret != 0) {
		printf("nue init fail\n");
		return 0;
	}

#if	FC_USE_LL
	parm_range = vendor_ai_drv_nue_get_mem_size(SV_LENGTH);
	parm_range += (fc_mem_pa + parm_offset);
	printf("parm_range = 0x%08X\n", (unsigned int)parm_range);

	vendor_ai_drv_nue_auto_alloc_mem(fc_mem_pa + parm_offset, fc_mem_va + parm_offset, &fc_info);
	p_fc_param = (KDRV_AI_FC_PARM*)fc_info.input_parm_va;
	
	// init fc param
	if (fc_init_parm(p_fc_param, fc_mem_pa + input_offset, 
		fc_mem_pa + output_offset, fc_weight_mem_pa) < 0){
		printf("fc parameter init fail\n");
		return 0;
	}
	PROF_START();
	if (vendor_ai_drv_nue_set_fc_info(&fc_info)) {
		printf("vendor_ai_drv_nue_set_fc_info fail\n");
		return 0;
	}
	PROF_END("nue_construct_cmd");
#else	
	// init fc param
	if (fc_init_parm(&fc_param, fc_mem_pa + input_offset, 
		fc_mem_pa + output_offset, fc_weight_mem_pa) < 0){
		printf("fc parameter init fail\n");
		return 0;
	}
#endif	
	do {
		if (is_fc_run) {
			// 2. flush input
			PROF_START();
			hd_common_mem_flush_cache((VOID *)fc_mem_va, SV_FEA_LENGTH);
			hd_common_mem_flush_cache((VOID *)fc_weight_mem_va, SV_LENGTH*SV_FEA_LENGTH);
			memset((VOID *)(fc_mem_va + output_offset), 0, SV_LENGTH*4);
			hd_common_mem_flush_cache((VOID *)(fc_mem_va + output_offset), SV_LENGTH*4);
			PROF_END("nue_flush");
			
			// 3. run fc
			PROF_START();
#if	FC_USE_LL
			//ret = vendor_ai_drv_nue_run_fc(p_fc_param);
			PROF_END("nue_inference");
			PROF_START();
			ret = vendor_ai_drv_nue_run_fc_ll(&fc_info);
			PROF_END("nue_ll_inference");
#else
			ret = vendor_ai_drv_nue_run_fc(&fc_param);
			PROF_END("nue_inference");
#endif
			if (ret != 0) {
				printf("run fc fail\n");
				return 0;
			}
			is_fc_run = FALSE;
			printf("[fc sample] fc done!\n");
			// 4. after running vendor_ai_drv_nue_run_fc, user should flush the output.
			//    user can get results on the output address (fc_mem_va + output_offset)
			hd_common_mem_flush_cache((VOID *)(fc_mem_va + output_offset), SV_LENGTH*4);
			
			{
				UINT32* p_rst = (UINT32 *)(fc_mem_va + output_offset);
				UINT32 i = 0;
				//check result
				for (i=0; i<SV_LENGTH; i++) {
					if (p_rst[i] != 0x2400) {
						printf("cmp fail in index %d: val = 0x%08X\n", (int)i, (unsigned int)p_rst[i]);
						break;
					}
				}
			}
		}
	} while(is_fc_proc);
	
	// 5. uninit nue drv
	ret = vendor_ai_drv_nue_uninit();
	if (ret != 0) {
		printf("nue uninit fail\n");
		return 0;
	}

	return 0;
}


/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
	HD_RESULT           ret;
	INT32 key;
	pthread_t fc_thread_id;


	ret = hd_common_init(0);
	if (ret != HD_OK) {
		DBG_ERR("hd_common_init fail=%d\n", ret);
		goto exit;
	}


	// init memory
	ret = mem_init();
	if (ret != HD_OK) {
		DBG_ERR("mem_init fail=%d\n", ret);
		goto exit;
	}

	ret = hd_videoproc_init();
	if (ret != HD_OK) {
		DBG_ERR("hd_videoproc_init fail=%d\n", ret);
		goto exit;
	}

	// create fc thread
	ret = pthread_create(&fc_thread_id, NULL, fc_thread_api, NULL);
	if (ret < 0) {
		DBG_ERR("create fc thread failed");
		goto exit;
	}
	is_fc_proc = TRUE;
	is_fc_run  = FALSE;

	do {
		//usleep(50);
	
		printf("usage:\n");
		printf("  enter q: exit\n");
		printf("  enter r: run engine\n");
		key = getchar();
		if (key == 'q' || key == 0x3) {
			is_fc_proc = FALSE;
			is_fc_run = FALSE;
			break;
		} else if (key == 'r') {
			//  run network
			is_fc_run = TRUE;
			//printf("[ai sample] write result done!\n");
			continue;
		}
	} while(1);

	
	pthread_join(fc_thread_id, NULL);

exit:

	ret = hd_videoproc_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_videoproc_uninit fail=%d\n", ret);
	}

	ret = mem_uninit();
	if (ret != HD_OK) {
		DBG_ERR("mem_uninit fail=%d\n", ret);
	}

	ret = hd_common_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_common_uninit fail=%d\n", ret);
	}

	printf("fc test finish...\r\n");

	return ret;
}
