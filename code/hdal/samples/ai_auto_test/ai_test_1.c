/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
////#include <pthread.h>
#include "hdal.h"
//#include "hd_type.h"
#include "hd_common.h"
//#include "nvtipc.h"
//#include "vendor_dsp_util.h"
//#include "vendor_ai/vendor_ai.h"

#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
//#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
//#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
//#include "net_flow_user_sample/net_layer_sample.h"
//#include "custnn/custnn_lib.h"
// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(alg_net_app_user_sample, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME

#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080
#define MAX_FRAME_BYTE          4
#define AI_SAMPLE_TEST_BATCH    ENABLE//v2 not support
#if AI_SAMPLE_TEST_BATCH
#define AI_SAMPLE_MAX_BATCH_NUM 8
#define AI_SAMPLE_MAX_INPUT_NUM 2
#else
#define AI_SAMPLE_MAX_BATCH_NUM 1
#define AI_SAMPLE_MAX_INPUT_NUM 1
#endif
#define KBYTE                   1024
#define MBYTE			(1024 * 1024)
#define NN_RUN_NET_NUM			1
#define MAX_FRAME_CHANNEL       3
//#define SRC_IN_SINGLE_BUF_SIZE       (MAX_FRAME_WIDTH*MAX_FRAME_HEIGHT*MAX_FRAME_CHANNEL*AI_SAMPLE_MAX_BATCH_NUM)
//#define SRC_IN_BUF_SIZE        (MAX_FRAME_WIDTH*MAX_FRAME_HEIGHT*MAX_FRAME_CHANNEL*AI_SAMPLE_MAX_BATCH_NUM)
//#define EXTRA_BUF_SIZE          100 * MBYTE
#define MAX_OBJ_NUM             1024
#define AUTO_UPDATE_DIM     	0
#define DYNAMIC_MEM             1
#define MODIFY_ENG_IN_SAMPLE 0
#define MAX_LEN          (256)
#define VENDOR_AI_CFG  0x000f0000  //ai project config

#define GET_OUTPUT_LAYER_FEATURE_MAP DISABLE
#define NN_USE_DRAM2             ENABLE
#define RESULT_COMPARE           ENABLE
#define NET_RESULT_MAX_SIZE      (32 * MBYTE)
#define SUPPORT_MULTI_BLOB_IN_SAMPLE      1
#define AI_SUPPORT_MULTI_FMT   1
#define _MAX(a,b) (((a)>(b))?(a):(b))
UINT32 NN_TOTAL_MEM_SIZE       	= 0;
/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/**
	Parameters of network
*/
typedef struct TEST_LIST_PATH {
	CHAR model_list[256];
	CHAR image_list[256];
	CHAR model_path[256];
	CHAR image_path[256];
	UINT32 test_times;
	UINT32 debug_mode;
} TEST_LIST_PATH;
typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	CHAR *p_image_path;
	CHAR *p_model_list;
	CHAR *p_image_list;
	//UINT32 src_img_num;
	VENDOR_AIS_IMG_PARM	src_img[AI_SAMPLE_MAX_INPUT_NUM];
	VENDOR_AIS_FLOW_MEM_PARM mem;
	UINT32 run_id;
	UINT32 test_times;
	UINT32 model_nums;
	//UINT32 image_nums;
	UINT32 result_tag;
	UINT32 error_tag;
	UINT32 max_model_sizes;
	UINT32 dbg_setting;
} VENDOR_AIS_NETWORK_PARM;
typedef struct CONFIG_OPT {
	CHAR key[256];
	CHAR value[256];
}CONFIG_OPT;

typedef struct _TEST_IMAGE_INFO {
	CHAR img_name[256];
	UINT32 img_w;
	UINT32 img_h;
	UINT32 ch;
	UINT32 bits;
	CHAR fmt[50];
	HD_VIDEO_PXLFMT img_fmt;
}TEST_IMAGE_INFO;


/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/

static VENDOR_AIS_FLOW_MEM_PARM g_mem = { 0 };


//static BOOL is_net_proc				= TRUE;
//static BOOL is_net_run[NN_SUPPORT_NET_MAX]	= {FALSE};

static HD_COMMON_MEM_VB_BLK g_blk_info[2];
#if AUTO_UPDATE_DIM
UINT32 diff_model_size = 0;
#if defined(__FREERTOS)
static CHAR diff_model_name[NN_RUN_NET_NUM][256] = { "A:\\para\\nvt_stripe_model.bin", "A:\\para\\nvt_stripe_model1.bin" };
#else
static CHAR diff_model_name[NN_RUN_NET_NUM][256] = { "para/nvt_stripe_model.bin", "para/nvt_stripe_model1.bin" };
#endif
#endif
static CHAR root_path[128] = "/mnt/sd/AI_auto_test_files/";

//NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/


static INT32 remove_blank(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	CHAR *p_str1 = p_str;

	while (*p_str0 != '\0'){
	
		if ((*p_str0 != ' ') && (*p_str0 != '\t') && (*p_str0 != '\n')){
			*p_str1++ = *p_str0++;
			
		} else {
			p_str0++;
		}
	}

	*p_str1 = '\0';
	return TRUE;
}


static BOOL find_semicolon(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	while (*p_str0 != '\0')	{

		if (*p_str0 == ';') {
			return TRUE;
		} else {
			p_str0++;
		}
	}
	*p_str0 = '\0';
	return FALSE;
}
int spilt_fun(char *line, char c, char out[][256])
{
	/* char *p_line = line; */
	char *a, *b;
	a = b = line;
	int ii = 0;
	while (1) {

		if (*b == '\0') {

			break;
		}
		if (*b == c){
		
			memcpy(out[ii], a, b - a);
			out[ii][b - a] = '\0';
			ii++;
			a = b = b + 1;
			continue;
		}
		b++;
	}
	memcpy(out[ii], a, b - a);
	out[ii][b - a] = '\0';

	return 0;
}


static INT32 split_kvalue(CHAR *p_str, CONFIG_OPT* p_config, CHAR c)
{
	CHAR *p_key = p_str;
	CHAR *p_value = p_str;
	CHAR *p_local = p_str;
	INT32 cnt = 0;

	// check only one '='
	while (*p_local != '\0') {

		p_local++;
		if (*p_local == c)
			cnt++;
	}
	if (cnt != 1) {
		return FALSE;
	}

	p_local = p_str;
	while (*p_local != c) {

		p_local++;
	}
	p_value = p_local + 1;
	*p_local = '\0';
	strcpy(p_config->key, p_key);
	strcpy(p_config->value, p_value);
	return TRUE;
}

static INT32 config_parser(CHAR *file_path, TEST_LIST_PATH *test_config)
{

	FILE *fp_tmp = NULL;


	if ((fp_tmp = fopen(file_path, "r")) == NULL) {
		DBG_ERR("config.txt is not exist!\n");
		return 0;
	}
	CHAR line[512];
	while (1) {

		if (feof(fp_tmp)) {
			break;
		}
		if (fgets(line, 512 - 1, fp_tmp) == NULL) {

			if (feof(fp_tmp)) {
				break;
			}
			DBG_ERR("fgets line: %s error!\n", line);
			continue;
		}

		remove_blank(line);

		if (line[0] == '#' || line[0] == '\0') {
			continue;
		}
		CONFIG_OPT tmp;
		if (split_kvalue(line, &tmp, '=') != 1) {

			printf("pauser line : %s error!\r\n", line);
			continue;
		}

		if (strcmp(tmp.key, "[path/model_list]") == 0) {
			strcpy(test_config->model_list, tmp.value);
		}
		if (strcmp(tmp.key, "[path/image_list]") == 0) {
			strcpy(test_config->image_list, tmp.value);
		}
		if (strcmp(tmp.key, "[path/model_path]") == 0) {
			strcpy(test_config->model_path, tmp.value);
		}
		if (strcmp(tmp.key, "[path/image_path]") == 0) {
			strcpy(test_config->image_path, tmp.value);
		}
		if (strcmp(tmp.key, "[test/num]") == 0) {
			test_config->test_times = atoi(tmp.value);
		}
		if (strcmp(tmp.key, "[debug/mode]") == 0) {
			test_config->debug_mode = atoi(tmp.value);
		}


	}
	fclose(fp_tmp);
	return 1;
}


static int mem_init(UINT32 max_mem)
{
	HD_RESULT                 ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = { 0 };
	UINT32                    total_mem_size = 0;

#if DYNAMIC_MEM

	total_mem_size += max_mem;


#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif

	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = total_mem_size;
	mem_cfg.pool_info[0].blk_cnt = 1;
#if NN_USE_DRAM2
	mem_cfg.pool_info[0].ddr_id = DDR_ID1;
#else
	mem_cfg.pool_info[0].ddr_id = DDR_ID0;
#endif
	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		DBG_ERR("hd_common_mem_init err: %d\r\n", ret);
	}
	return ret;
}

static INT32 get_mem_block(UINT32 max_mem)
{
	HD_RESULT                 ret = HD_OK;
	UINT32                    pa, va;
	HD_COMMON_MEM_VB_BLK      blk;
	UINT32 total_mem_size = 0;


#if NN_USE_DRAM2
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID1;
#else
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID0;
#endif

#if DYNAMIC_MEM

	total_mem_size += max_mem;

#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif

	/* Allocate scale out buffer */
	if (g_mem.va != 0) {
		DBG_DUMP(" mem has already been inited\r\n");
		return -1;
	}
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, total_mem_size, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		DBG_ERR("hd_common_mem_get_block fail\r\n");
		ret = HD_ERR_NG;
		goto mem_exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		DBG_DUMP("hd_common_mem_blk2pa fail,pa=%08x\r\n", pa);
		ret = -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
		pa, total_mem_size);
	g_blk_info[0] = blk;
	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_mem_size);
		if (ret != HD_OK) {
			DBG_DUMP("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}
	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = total_mem_size;

mem_exit:
	return ret;
}
static HD_RESULT release_mem_block(UINT32 max_mem)
{
	HD_RESULT ret = HD_OK;
	UINT32 total_mem_size = 0;

#if DYNAMIC_MEM
	total_mem_size += max_mem;

#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif


	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, total_mem_size);
		if (ret != HD_OK) {
			DBG_DUMP("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		DBG_DUMP("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	return ret;
}


static HD_RESULT mem_uninit(void)
{
	return hd_common_mem_uninit();
}


VENDOR_AIS_FLOW_MEM_PARM get_mem_custom(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size, char *file, UINT32 *flag)
{
	VENDOR_AIS_FLOW_MEM_PARM mem = { 0 };
	FILE *fp = NULL;
	required_size = ALIGN_CEIL_64(required_size);

	if (required_size <= valid_mem->size) {
		/*printf("Required size %d , valid size %d\r\n", required_size, valid_mem->size);*/

		mem.va = valid_mem->va;
		mem.pa = valid_mem->pa;
		mem.size = required_size;

		valid_mem->va += required_size;
		valid_mem->pa += required_size;
		valid_mem->size -= required_size;
	} else {
		DBG_ERR("Required size %ld > valid size %ld\r\n", required_size, valid_mem->size);
		fp = fopen(file, "w+");
		if (fp == NULL) {

			*flag = 1;
		} else {
			DBG_ERR("Required size %ld > valid size %ld\r\n", required_size, valid_mem->size);
			fclose(fp);
			*flag = 1;
		}
	}
	return mem;
}

HD_RESULT clear_mem_custom(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size)
{

	required_size = ALIGN_CEIL_64(required_size);
	valid_mem->va -= required_size;
	valid_mem->pa -= required_size;
	valid_mem->size += required_size;

	return HD_OK;
}



#if !RESULT_COMPARE
static UINT32 ai_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;

	fd = fopen(p_filename, "rb");
	if (!fd) {
		DBG_DUMP("cannot read %s\r\n", p_filename);
		return 0;
	}
	fseek(fd, 0, SEEK_END);
	file_size = ALIGN_CEIL_64(ftell(fd));
	fseek(fd, 0, SEEK_SET);

	read_size = fread((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
		DBG_DUMP("model size mismatch, real = %ld, idea = %ld\r\n", (int)read_size, (int)file_size);
	}
	fclose(fd);
	return read_size;
}
#endif
static INT32 custom_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	INT32		file_size = 0, read_size = 0;
	const UINT32 model_addr = va;

	fd = fopen(p_filename, "rb");
	if (!fd) {
		/*DBG_ERR("cannot read %s\r\n", p_filename);*/
		return 0;
	}

	fseek(fd, 0, SEEK_END);
	/*file_size = ALIGN_CEIL_64(ftell(fd));*/
	file_size = ftell(fd);
	fseek(fd, 0, SEEK_SET);
	if (file_size < 0) {

		fclose(fd);
		return -1;
	}
	read_size = fread((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
		DBG_ERR("size mismatch, read_size = %ld, file_size = %ld\r\n", (int)read_size, (int)file_size);
		fclose(fd);
		return -1;
	}
	fclose(fd);

	return read_size;
}



#if RESULT_COMPARE

static UINT32 result_compare(UINT32 va, UINT32 gtsize, UINT8* data, UINT32 size)
{

	UINT32 idx;
	UINT8 *gt = (UINT8 *)va;
	if (gtsize < size) {

		return -1;
	}
	for (idx = 0; idx < size; idx++) {

		if (((*(gt++)) - (*(data++))) != 0) {
			return -1;
		}
	}

	return 0;
}
#endif





static VOID *nn_thread_api(VOID *arg)
{
	HD_RESULT ret;
	VENDOR_AIS_NETWORK_PARM *p_net_parm = (VENDOR_AIS_NETWORK_PARM*)arg;
	VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem = &p_net_parm->mem;
	VENDOR_AIS_IMG_PARM *p_src_img = p_net_parm->src_img;
	VENDOR_AIS_IMG_PARM *pb_src_img = NULL;
	VENDOR_AIS_FLOW_MEM_PARM tem_mem;
	VENDOR_AIS_FLOW_MEM_PARM src_mem[2] = { 0 };
	VENDOR_AIS_FLOW_MEM_PARM rslt_mem;
	VENDOR_AIS_FLOW_MEM_PARM model_mem;

	UINT32  test_times = p_net_parm->test_times;
	UINT32 run_id = p_net_parm->run_id;
	UINT32 net_id = run_id;
	UINT32 model_number = p_net_parm->model_nums;
	UINT32 model_file_size = 0;
	UINT32 dbg_set = p_net_parm->dbg_setting;

	VENDOR_AIS_FLOW_MAP_MEM_PARM mem_manager;
	//UINT32 req_size = 0;
	//UINT32 th_tag = 0;
	UINT32 out_temp_va = 0;
	UINT32 tmp_src_va;
	UINT32 tmp_src_pa;
	UINT32 src_img_size;
#if RESULT_COMPARE
	CHAR img_save_path[MAX_LEN];
	NN_DATA output_layer_iomem = { 0 };
	NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
	UINT32 num_output_layer = 0;
	VENDOR_AIS_FLOW_MEM_PARM gt_mem, output_result_mem;
	UINT32 model_count = 0;
	UINT32 image_count = 0;
	CHAR error_file[MAX_LEN];
	FILE *m_list_fp = NULL;
	FILE *check_fp = NULL;
	FILE *i_list_fp = NULL;
	UINT32 input_proc_num = 0;
	UINT32 batch_i;


#endif

#if !CNN_25_MATLAB
	NN_IN_OUT_FMT *input_info = NULL;
#endif


	tem_mem.va = p_tot_mem->va;
	tem_mem.pa = p_tot_mem->pa;
	tem_mem.size = p_tot_mem->size;



	/*allocate every memory*/
	sprintf(error_file, "%s/output/ai_test_1/error_0.txt", root_path);
	m_list_fp = fopen(p_net_parm->p_model_list, "r");
	if (m_list_fp == NULL) {

		DBG_ERR("net_id %ld model_list_path %s is null!\r\n", net_id, p_net_parm->p_model_list);
		goto texit;
	}

	CHAR model_name[50] = { 0 };
	CHAR image_name[AI_SAMPLE_MAX_INPUT_NUM][256];
	CHAR model_path[256] = { 0 };
	CHAR image_path[256] = { 0 };
	CHAR roi_path[256] = { 0 };
	UINT32 input_proc_idx[AI_SAMPLE_MAX_BATCH_NUM];
	CHAR line[512] = { 0 };
	CHAR fmt[256] = { 0 };
	CHAR split_line[3][256];
	CHAR blob1[10][256];
	CHAR blob2[10][256];
	CHAR splitline[6][256];
	UINT32 model_mem_size = 0;
    UINT32 model_size = 0;
    UINT32 image_size = 0;  
	UINT32 file_size = 0;
    UINT32 image_size_1 = 0;   	
    UINT32 image_size_2 = 0;  	

	for (UINT32 j = 0; j < model_number; j++)
	{
		fscanf(m_list_fp, "%s \n", model_name);
		sprintf(model_path, "%s/model_bins/%s/sdk/nvt_model.bin", root_path, model_name);
	    ret = vendor_ais_get_model_mem_sz(&model_mem_size, model_path);
        model_mem.va = tem_mem.va;
        model_mem.pa = tem_mem.pa;
        model_mem.size = model_mem_size;		
#if RESULT_COMPARE
		model_file_size = vendor_ais_load_file(model_path, model_mem.va, &output_layer_info, &num_output_layer);
#else
		model_file_size = ai_load_file(model_path, model_mem.va);
#endif
		if (model_file_size == 0) {
#if RESULT_COMPARE
			if (output_layer_info != NULL) {
				free(output_layer_info);
				output_layer_info = NULL;
			}
#endif
			if (check_fp == NULL) {

				check_fp = fopen(error_file, "w+");
				if (check_fp == NULL) {

					p_net_parm->result_tag = 1;
					DBG_ERR("error file %s is null\r\n", error_file);
				} else {
					p_net_parm->result_tag = 1;
					fprintf(check_fp, "%s null null 1\r\n", model_name);
				}
			} else {
				p_net_parm->result_tag = 1;
				fprintf(check_fp, "%s null null 1\r\n", model_name);
			}

			continue;
		}
		model_size = vendor_ais_auto_alloc_mem(&model_mem, &mem_manager);

		if (model_size > p_net_parm->max_model_sizes) {

			if (check_fp == NULL) {

				check_fp = fopen(error_file, "w+");
				if (check_fp == NULL) {

					p_net_parm->result_tag = 1;
					DBG_ERR("error file is null\r\n");
				} else {
					p_net_parm->result_tag = 1;
				}
			} else {
				p_net_parm->result_tag = 1;
			}
			continue;
		}
		memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);   
		memset((VOID *)rslt_mem.va, 0, rslt_mem.size);    
		ret = vendor_ais_net_gen_init(mem_manager, net_id);
		if (ret != HD_OK) {
			if (check_fp == NULL) {

				check_fp = fopen(error_file, "w+");
				if (check_fp == NULL) {
					p_net_parm->result_tag = 1;
					DBG_ERR("error file is null.\r\n");
				} else {

					p_net_parm->result_tag = 1;
					fprintf(check_fp, "%s null null 1\r\n", model_name);
				}
			} else {

				p_net_parm->result_tag = 1;
				fprintf(check_fp, "%s null null 1\r\n", model_name);
			}
			goto gen_init_fail;
		}

		model_count = model_count + 1;
#if !CNN_25_MATLAB

		ret = vendor_ais_get_net_input_info(&input_info, model_mem);
		if (ret != HD_OK) {
			/*DBG_DUMP("model_fmt = %s\n", input_info->model_fmt);*/
	        if (output_layer_info != NULL) {
    		free(output_layer_info);
    		output_layer_info = NULL;
    	    }
    	    if (input_info != NULL) {
    		free(input_info);
    		input_info = NULL;
    	    }
			if (check_fp == NULL) {

				check_fp = fopen(error_file, "w+");
				if (check_fp == NULL) {
					p_net_parm->result_tag = 1;
					DBG_ERR("error file is null.\r\n");
				} else {

					p_net_parm->result_tag = 1;
					fprintf(check_fp, "%s null null 1\r\n", model_name);
				}
			} else {

				p_net_parm->result_tag = 1;
				fprintf(check_fp, "%s null null 1\r\n", model_name);
			}
			goto gen_init_fail;

		}
#endif
        UINT32 io_num_m=0;
	    UINT32 *p_input_blob_info_m=NULL;
		NN_DATA in_layer_mem_m={0};
		UINT32 bit_num;
		UINT32 blob_num = 0;
		INT16 retm=0;
		for(UINT16 i_m=0;i_m<AI_SAMPLE_MAX_INPUT_NUM;i_m++) {
			retm=vendor_ais_net_get_input_blob_info(model_mem, i_m, &io_num_m, &p_input_blob_info_m);
			if( retm!= HD_OK) {
				break;
			} else {
				blob_num = blob_num + 1;
			}
		}
		if (blob_num != 1 && blob_num != 2){
			continue;
		}
		vendor_ais_get_input_layer_mem(model_mem, &in_layer_mem_m, 0);
		bit_num= (INT16)in_layer_mem_m.fmt.int_bits+(INT16)in_layer_mem_m.fmt.frac_bits+(INT16)in_layer_mem_m.fmt.sign_bits;
		vendor_ais_pars_engid(0, net_id);
		/*starting image cycle*/
		i_list_fp = fopen(p_net_parm->p_image_list, "r");
		if (i_list_fp == NULL) {

			DBG_ERR("net_id %ld image_list_path %s is null!\r\n", net_id, p_net_parm->p_model_list);
			goto texit;
		}
		src_mem[0].va = tem_mem.va + ALIGN_CEIL_64(model_mem_size);/////////////
		src_mem[0].pa = tem_mem.pa + ALIGN_CEIL_64(model_mem_size);//////////////
		while (fgets(line, 512 - 1, i_list_fp) != NULL) {
			if (line[strlen(line) - 1] == '\n' || line[strlen(line) - 1] == '\r') {
				line[strlen(line) - 1] = '\0';
			}
			if (blob_num == 1) {
				if (find_semicolon(line))
				{
					continue;
				} else {

					spilt_fun(line, ' ', splitline);
					strcpy(image_name[0], splitline[0]);
					p_src_img[0].width = atoi(splitline[1]);
					p_src_img[0].height = atoi(splitline[2]);
					p_src_img[0].channel = atoi(splitline[3]);
					p_src_img[0].fmt_type = atoi(splitline[4]);
					strcpy(fmt, splitline[5]);
					if (p_src_img[0].width <= 0 || p_src_img[0].width > MAX_FRAME_WIDTH) {

						continue;

					}
					if (p_src_img[0].height <= 0 || p_src_img[0].height > MAX_FRAME_HEIGHT) {

						continue;

					}
					if (p_src_img[0].channel <= 0 || p_src_img[0].channel > MAX_FRAME_CHANNEL) {

						continue;

					}
					if (p_src_img[0].fmt_type <= 0 || p_src_img[0].fmt_type > 32 || p_src_img[0].fmt_type % 8 != 0) {

						continue;

					}


					if (strcmp(fmt, "HD_VIDEO_PXLFMT_YUV420") == 0) {
						p_src_img[0].fmt = 0x520c0420;
					} else if (strcmp(fmt, "HD_VIDEO_PXLFMT_Y8") == 0) {
						p_src_img[0].fmt = 0x51080400;
					} else if (strcmp(fmt, "HD_VIDEO_PXLFMT_RGB888_PLANAR") == 0) {
						p_src_img[0].fmt = 0x23180888;
					} else {
						printf("fmt not enough.\r\n");
						continue;
					}



					/*  单blob的操作*/
					if ((p_src_img[0].channel != input_info->in_channel) || (p_src_img[0].height < input_info->model_height) || (p_src_img[0].width < input_info->model_width) || p_src_img[0].fmt_type != bit_num){
					
						continue;
					}

					sprintf(image_path, "%s/test_images/%s", root_path, image_name[0]);

					file_size = custom_load_file(image_path, src_mem[0].va);

					/*check image*/
					if (file_size <= 0) {

						if (check_fp == NULL) {

							check_fp = fopen(error_file, "w+");
							if (check_fp == NULL) {
								p_net_parm->result_tag = 1;
								DBG_ERR("error file is null.\r\n");
							} else {

								p_net_parm->result_tag = 1;
								fprintf(check_fp, "%s %s null 2\r\n", model_name, image_name[0]);
							}
						} else {

							p_net_parm->result_tag = 1;
							fprintf(check_fp, "%s %s null  2\r\n", model_name, image_name[0]);
						}
						continue;
					}

					p_src_img[0].va = src_mem[0].va;
					p_src_img[0].pa = src_mem[0].pa;
					p_src_img[0].line_ofs = p_src_img[0].width * p_src_img[0].fmt_type / 8;
					/*单blob batch处理方法 start*/
#if AI_SAMPLE_TEST_BATCH
			/* test for get input layers:*/
					pb_src_img = &p_src_img[0];
					tmp_src_va = pb_src_img->va;
					tmp_src_pa = pb_src_img->pa;
					src_img_size = pb_src_img->line_ofs * pb_src_img->height * pb_src_img->channel;

					input_proc_num = vendor_ais_net_get_input_layer_index(model_mem, input_proc_idx);
				    image_size = p_src_img[0].width * p_src_img[0].height * _MAX(3, p_src_img[0].channel) * (p_src_img[0].fmt_type / 8) * input_proc_num;

#if AI_SUPPORT_MULTI_FMT
						p_src_img[0].fmt_type=0;
#endif	

					for (batch_i = 0; batch_i < input_proc_num; batch_i++) {

						if (batch_i == 0) {
							hd_common_mem_flush_cache((VOID *)(pb_src_img->va), src_img_size);

						} else {
							memcpy((VOID *)pb_src_img->va + src_img_size, (VOID *)pb_src_img->va, src_img_size);
							hd_common_mem_flush_cache((VOID *)(pb_src_img->va + src_img_size), src_img_size);

							pb_src_img->pa += src_img_size;
							pb_src_img->va += src_img_size;
						}
						if (vendor_ais_net_input_layer_init(pb_src_img, batch_i, net_id) != HD_OK)
						{

							if (check_fp == NULL) {

								check_fp = fopen(error_file, "w+");
								if (check_fp == NULL) {
									p_net_parm->result_tag = 1;
									DBG_ERR("error file is null.\r\n");
								} else {

									p_net_parm->result_tag = 1;
									fprintf(check_fp, "%s %s null 2\r\n", model_name, image_name[0]);
								}
							} else {
								p_net_parm->result_tag = 1;
								fprintf(check_fp, "%s  %s null 2\r\n", model_name, image_name[0]);
							}
							goto input_init_fail;

						}

					}
					pb_src_img->va = tmp_src_va;
					pb_src_img->pa = tmp_src_pa;


#else

					if (vendor_ais_net_input_init(&p_src_img[0], net_id) != HD_OK) {

						if (check_fp == NULL) {

							check_fp = fopen(error_file, "w+");
							if (check_fp == NULL) {
								p_net_parm->result_tag = 1;
								DBG_ERR("error file is null.\r\n");
							} else {

								p_net_parm->result_tag = 1;
								fprintf(check_fp, "%s %s null 2\r\n", model_name, image_name[0]);
							}
						} else
						{
							p_net_parm->result_tag = 1;
							fprintf(check_fp, "%s %s null 2\r\n", model_name, image_name[0]);

						}
						goto input_init_fail;
					}
#endif
					/*单blob batch处理方法 end*/

				}
			}


			if (blob_num == 2)
			{
				/* 双blob读入 */
				if (!find_semicolon(line)) {
					continue;
				} else {
					spilt_fun(line, ';', split_line);

					if (strcmp(model_name, split_line[0]) == 0) {
						spilt_fun(split_line[1], ' ', blob1);
						spilt_fun(split_line[2], ' ', blob2);
						strcpy(image_name[0], blob1[0]);
						p_src_img[0].width = atoi(blob1[1]);
						p_src_img[0].height = atoi(blob1[2]);
						p_src_img[0].channel = atoi(blob1[3]);
						p_src_img[0].batch_num = atoi(blob1[4]);
						p_src_img[0].line_ofs = atoi(blob1[5]);
						p_src_img[0].channel_ofs = atoi(blob1[6]);
						p_src_img[0].batch_ofs = atoi(blob1[7]);
						if (strcmp(blob1[8], "0x520c0420") == 0) {
							p_src_img[0].fmt = 0x520c0420;
						} else if (strcmp(blob1[8], "0x51080400") == 0) {
							p_src_img[0].fmt = 0x51080400;
						} else if (strcmp(blob1[8], "0x23180888") == 0) {
							p_src_img[0].fmt = 0x23180888;
						} else {
							printf("image fmt not enough.\r\n");
							continue;
						}
						sprintf(image_path, "%s/test_images/%s", root_path, blob1[0]);
						image_size_1 = custom_load_file(image_path, src_mem[0].va);

						if (image_size_1 <= 0) {

							if (check_fp == NULL) {

								check_fp = fopen(error_file, "w+");
								if (check_fp == NULL) {
									p_net_parm->result_tag = 1;
									DBG_ERR("error file is null.\r\n");
								} else {

									p_net_parm->result_tag = 1;
									fprintf(check_fp, "%s  %s null 2\r\n", model_name, image_name[0]);
								}
							} else {

								p_net_parm->result_tag = 1;
								fprintf(check_fp, "%s  %s null  2\r\n", model_name, image_name[0]);
							}
							continue;
						}

						p_src_img[0].va = src_mem[0].va;
						p_src_img[0].pa = src_mem[0].pa;
#if AI_SUPPORT_MULTI_FMT
						p_src_img[0].fmt_type = 0;
#endif
						strcpy(image_name[1], blob2[0]);
						p_src_img[1].width = atoi(blob2[1]);
						p_src_img[1].height = atoi(blob2[2]);
						p_src_img[1].channel = atoi(blob2[3]);
						p_src_img[1].batch_num = atoi(blob2[4]);
						p_src_img[1].line_ofs = atoi(blob2[5]);
						p_src_img[1].channel_ofs = atoi(blob2[6]);
						p_src_img[1].batch_ofs = atoi(blob2[7]);
						if (strcmp(blob2[8], "0x520c0420") == 0) {
							p_src_img[1].fmt = 0x520c0420;
						} else if (strcmp(blob2[8], "0x51080400") == 0) {
							p_src_img[1].fmt = 0x51080400;
						} else if (strcmp(blob2[8], "0x23180888") == 0) {
							p_src_img[1].fmt = 0x23180888;
						} else {
							printf("roi fmt not enough.\r\n");
							continue;
						}
						sprintf(roi_path, "%s/test_images/%s", root_path, image_name[1]);
					    src_mem[1].va = src_mem[0].va + image_size_1;
						src_mem[1].pa = src_mem[0].pa + image_size_1;
						image_size_2 = custom_load_file(roi_path, src_mem[1].va);
						if (image_size_2 <= 0) {

							if (check_fp == NULL) {

								check_fp = fopen(error_file, "w+");
								if (check_fp == NULL) {
									p_net_parm->result_tag = 1;
									DBG_ERR("error file is null.\r\n");
								} else {

									p_net_parm->result_tag = 1;
									fprintf(check_fp, "%s  %s null 2\r\n", model_name, image_name[1]);
								}
							} else {

								p_net_parm->result_tag = 1;
								fprintf(check_fp, "%s  %s null  2\r\n", model_name, image_name[1]);
							}
							continue;
						}


						p_src_img[1].va = src_mem[1].va;
						p_src_img[1].pa = src_mem[1].pa;

						/*加载双blob图片*/

						UINT32 io_num = 0;
						UINT32* p_input_blob_info = NULL;
						for (UINT i = 0; i < blob_num; i++) {
							if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
								goto input_init_fail;
							}
							for (batch_i = 0; batch_i < io_num; batch_i++) {
								UINT32 proc_idx = p_input_blob_info[batch_i] >> 8;
								UINT32 tmp_src_va = p_src_img[i].va;
								UINT32 tmp_src_pa = p_src_img[i].pa;
								p_src_img[i].va += (batch_i*p_src_img[i].batch_ofs);
								p_src_img[i].pa += (batch_i*p_src_img[i].batch_ofs);
								hd_common_mem_flush_cache((VOID *)(p_src_img[i].va), p_src_img[i].batch_ofs);
								ret = vendor_ais_net_input_layer_init(&p_src_img[i], proc_idx, net_id);
								if (ret != HD_OK) {

									goto input_init_fail;
								}
								p_src_img[i].va = tmp_src_va;
								p_src_img[i].pa = tmp_src_pa;
							}
							image_size += (p_src_img[i].width * p_src_img[i].height * _MAX(3, p_src_img[i].channel) * (p_src_img[i].fmt_type / 8))*io_num;

						}
						

					} 
					
					else {
						continue;
					}					
				}						
			}			

			image_count = image_count + 1;
			INT32 gt_size = 0;
			CHAR *imgsplit_name;
			imgsplit_name = strtok(image_name[0], ".");
			sprintf(img_save_path, "%s/gt_results/%s/%s/outfeature.bin", root_path, model_name, imgsplit_name);

			FILE *gt_fp = NULL;
			gt_fp = fopen(img_save_path, "rb");
			if (gt_fp == NULL) {
			
				p_net_parm->result_tag = 1;
				DBG_ERR(" model:%s name:%s gt_bin is null.\r\n", model_name, imgsplit_name);
				goto input_init_fail;
			}
			fclose(gt_fp);
			gt_fp = NULL;
            gt_mem.va =  src_mem[0].va+ ALIGN_CEIL_64(image_size);
			gt_mem.pa =  src_mem[0].pa+ ALIGN_CEIL_64(image_size);
			gt_size = custom_load_file(img_save_path, gt_mem.va);
			if (gt_size <= 0) {
			
				p_net_parm->result_tag = 1;
				DBG_ERR(" gt_size is wrong.\r\n");
				goto input_init_fail;
			}

			for (UINT32 kk = 0; kk < test_times; kk++) {

				memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
				hd_common_mem_flush_cache((VOID *)mem_manager.user_buff.va, mem_manager.user_buff.size);
#if USE_NEON
				vendor_ais_proc_net(model_mem, rslt_mem, &p_src_img[0], net_id);
#else
				vendor_ais_proc_net(model_mem, rslt_mem, net_id);
#endif

# if RESULT_COMPARE
                output_result_mem.va = gt_mem.va + ALIGN_CEIL_64(gt_size);
				output_result_mem.pa = gt_mem.pa + ALIGN_CEIL_64(gt_size);
				
				UINT32 output_size = 0;
				out_temp_va = output_result_mem.va;
				INT8 Vva;
				for (UINT32 l = 0; l < num_output_layer; l++) {
				
					ret = vendor_ais_get_layer_mem_v2(model_mem, &output_layer_iomem, &output_layer_info[l]);

					if (ret == HD_OK) {
						if (output_layer_iomem.va > 0 && output_layer_iomem.size > 0) {
							hd_common_mem_flush_cache((VOID *)output_layer_iomem.va, output_layer_iomem.size);
						} else {
							if (check_fp == NULL) {
								check_fp = fopen(error_file, "w+");
								if (check_fp == NULL) {

									p_net_parm->result_tag = 1;
									DBG_ERR("error file is null.\r\n");

								
								} else {
								
									p_net_parm->result_tag = 1;
									fprintf(check_fp, "%s  %s  %d  3\r\n", model_name, image_name[0], (INT)kk);

								}
							} else {
							
								p_net_parm->result_tag = 1;
								fprintf(check_fp, "%s  %s  %d  3\r\n", model_name, image_name[0], (INT)kk);

							}
							goto input_init_fail;
						}
						output_size += output_layer_iomem.size;

						//if (output_size > output_result_mem.size) {

							//DBG_ERR("model result size(%ld)  mem is not enough(%ld)", output_size, output_result_mem.size);
						//}
						memcpy((VOID *)out_temp_va, (VOID *)output_layer_iomem.va, output_layer_iomem.size);
						out_temp_va = out_temp_va +  output_layer_iomem.size;
					} else {
						if (check_fp == NULL) {
							check_fp = fopen(error_file, "w+");
							if (check_fp == NULL) {
							
								p_net_parm->result_tag = 1;
								DBG_ERR("error out file: %s  null\r\n", error_file);

							} else {
							
								p_net_parm->result_tag = 1;
								fprintf(check_fp, "%s  %s  %d  3\r\n", model_name, image_name[0], (INT)kk);


							}
						} else {
						
							p_net_parm->result_tag = 1;
							fprintf(check_fp, "%s  %s  %d  3\r\n", model_name, image_name[0], (INT)kk);


						}
						goto input_init_fail;
					}
				}

				Vva = result_compare(gt_mem.va, gt_size, (UINT8 *)output_result_mem.va, output_size);
				if (Vva < 0) {
					if (check_fp == NULL) {
						check_fp = fopen(error_file, "w+");
						if (check_fp == NULL) {
							p_net_parm->result_tag = 1;
							DBG_ERR("error file is null\r\n");
						} else {
						
							p_net_parm->result_tag = 1;
							p_net_parm->error_tag = 1;
							fprintf(check_fp, "%s  %s %d  4\r\n", model_name , image_name[0], (INT)kk);

						}
					} else {
					
						p_net_parm->result_tag = 1;
						p_net_parm->error_tag = 1;
						fprintf(check_fp, "%s %s  %d  4\r\n", model_name , image_name[0], (INT)kk);
					}

					if (dbg_set > 0) {
						DBG_DUMP("different found in thread: %d, model: %s, img: %s, iteration: %d!!!\r\n", (int)run_id, model_name, imgsplit_name, (int)kk);
					}
					break;
				}

#endif
			}


		input_init_fail:
#if AI_SAMPLE_TEST_BATCH
			if (blob_num == 1) {
				for (batch_i = 0; batch_i < input_proc_num; batch_i++)
				{
					ret = vendor_ais_net_input_layer_uninit(batch_i, net_id);
					if (ret != HD_OK) {
						p_net_parm->result_tag = 1;
						if (dbg_set > 0) {
							DBG_ERR("pethread %u: %s %u %s vendor_ais_net_input_uninit fail\r\n", net_id, model_name, image_name[0]);
						}
						goto gen_init_fail;
					}

				}
			} else if (blob_num == 2) {
				UINT32 j = 0, i = 0;
				UINT32 io_num = 0;
				UINT32* p_input_blob_info = NULL;

				for (i = 0; i < blob_num; i++) {
					if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
						goto gen_init_fail;
					}
					for (j = 0; j < io_num; j++) {
						UINT32 proc_idx = p_input_blob_info[j] >> 8;


						ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
						if (ret != HD_OK) {
							printf("vendor_ais_net_input_uninit (%d)\r\n", ret);
						}
					}
				}
			}

#else
			ret = vendor_ais_net_input_uninit(net_id);
			if (ret != HD_OK) {

				if (check_fp == NULL) {

					check_fp = fopen(error_file, "w+");
					if (check_fp == NULL) {
						p_net_parm->result_tag = 1;
						DBG_ERR("error file is null\r\n");
					} else {
						p_net_parm->result_tag = 1;
						DBG_ERR("fail\r\n");
					}
				} else {
					p_net_parm->result_tag = 1;
					DBG_ERR("fail\r\n");
				}
				goto gen_init_fail;
			}
#endif



		}

		if (i_list_fp != NULL) {

			fclose(i_list_fp);
			i_list_fp = NULL;
		}




	gen_init_fail:
		ret = vendor_ais_net_gen_uninit(net_id);
		if (ret != HD_OK) {
			if (check_fp == NULL) {
				check_fp = fopen(error_file, "w+");
				if (check_fp == NULL) {
					p_net_parm->result_tag = 1;
					DBG_ERR("error file is null\r\n");
				} else {
					p_net_parm->result_tag = 1;
					DBG_ERR("fail\r\n");
				}
			} else {
				p_net_parm->result_tag = 1;
				DBG_ERR("fail\r\n");
			}
			goto texit;
		}

#if AI_V4
#if RESULT_COMPARE

		if (output_layer_info != NULL) {
			free(output_layer_info);
			output_layer_info = NULL;
		}

#endif
		if (input_info != NULL) {
			free(input_info);
			input_info = NULL;
		}
#endif


	}
	if (m_list_fp != NULL) {

		fclose(m_list_fp);
		m_list_fp = NULL;
	}

texit:

#if AI_V4
#if RESULT_COMPARE

	if (output_layer_info != NULL) {
		free(output_layer_info);
		output_layer_info = NULL;
	}

#endif
	if (input_info != NULL) {
		free(input_info);
		input_info = NULL;
	}
#endif
	if (check_fp != NULL) {

		fclose(check_fp);
		check_fp = NULL;
	}
	if (m_list_fp != NULL) {

		fclose(m_list_fp);
		m_list_fp = NULL;
	}
	if (i_list_fp != NULL) {

		fclose(i_list_fp);
		i_list_fp = NULL;
	}
	return 0;
}

INT32 check_file(CHAR *file)
{
	FILE *fp = NULL;
	UINT32 cnt = 0;
	CHAR line[MAX_LEN * 2];
	fp = fopen(file, "r");
	if (fp == NULL)
		return -1;
	while (1) {
		if (feof(fp)) {
			break;
		}
		if (fgets(line, MAX_LEN * 2 - 1, fp) == NULL) {

			if (feof(fp)) {
				break;
			}
			continue;
		}
		cnt += 1;
	}
	fclose(fp);
	if (cnt == 0) {
		return -1;
	}
	return 0;
}

/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/

MAIN(argc, argv)
{

	VENDOR_AIS_NETWORK_PARM net_parm[NN_RUN_NET_NUM] = { 0 };
	UINT32 test_model_num = 0;
	pthread_t *nn_thread_id = NULL;
	HD_RESULT      ret = 0;
	int idx = 0;
	UINT32 dbg_setting;
	//UINT32 thread_total_mem;
	char error_file[MAX_LEN] = "/mnt/sd/AI_auto_test_files/output/ai_test_1/main_error.txt";

	CHAR command_test[50];
	CHAR command_test_1[50];
	sprintf(command_test, "rm -rf %s/output/ai_test_1", root_path);
	system(command_test);
	sprintf(command_test_1, "mkdir %s/output/ai_test_1", root_path);
	system(command_test_1);


	CHAR config_path[128];
	TEST_LIST_PATH test_config;
	UINT32 main_error_tag = 0;
	FILE *fp = NULL;
	sprintf(config_path, "%s/paras/config_1.txt", root_path);
	if (!(config_parser(config_path, &test_config))) {
	
		DBG_DUMP("parse config file is null.\r\n");
		main_error_tag = 1;
		goto main_exit;
	}
	dbg_setting = test_config.debug_mode;



	CHAR image_info_file[MAX_LEN];
	UINT32 tmp_model_size = 0;
	UINT32 max_model_size = 0;
	CHAR model_name[50] = { 0 };
	CHAR model_bin_file[MAX_LEN];
	FILE *fp_m = NULL;
	INT32 check_flag;
	CHAR stemp[MAX_LEN];
	fp_m = fopen(test_config.model_list, "r");
	if (fp_m == NULL) {
		main_error_tag = 1;
		DBG_ERR(" model_list is null\r\n");

		goto main_exit;
	}
	while (1) {
		if (feof(fp_m)) {
			break;
		}
		if (fscanf(fp_m, "%s \n", model_name) == EOF) {

			if (feof(fp_m)) {
				break;
			}
			if (dbg_setting > 0) {

				DBG_DUMP("fscanf name : %s error!\r\n", model_name);
			}
			continue;
		}
        sprintf(model_bin_file, "%s/model_bins/%s/sdk/nvt_model.bin", root_path, model_name);

		ret = vendor_ais_get_model_mem_sz(&tmp_model_size, model_bin_file);
		if (ret != HD_OK) {
			if (dbg_setting > 0) {
				DBG_DUMP("get model %s size info fail.\n", model_name);
			}
		}
		if (tmp_model_size > max_model_size) {
		
			max_model_size = tmp_model_size;
		}
		test_model_num = test_model_num + 1;
	}
	fclose(fp_m);
	memset(stemp, '\0', MAX_LEN);
	strcpy(image_info_file, test_config.image_list);

	if (strcmp(image_info_file, stemp) != 0) {

		check_flag = check_file(image_info_file);
		if (check_flag < 0) {

			/*DBG_ERR("%s null!\r\n",config_para.image_list_1);*/
			main_error_tag = 1;
			goto main_exit;
		}
	}
	FILE *mem_fp = NULL;
	CHAR mem_txt_path[MAX_LEN];
    UINT32 memory_max[3];	
	sprintf(mem_txt_path,"%s/gt_results/memory_max.txt",root_path);
	mem_fp = fopen(mem_txt_path, "r");
	if (mem_fp == NULL) {
		DBG_ERR("open mem txt fail.\r\n");
	    main_error_tag = 1;
        
	} else {
        for (UINT32 idx = 0; idx < 3; idx++) {
		   fscanf(mem_fp, "%ld\n", &memory_max[idx]);

	    }
	}
	if (mem_fp) {
		fclose(mem_fp);
	}
	INT32 max_mem = memory_max[2];
	DBG_DUMP("\r\n");
	DBG_DUMP("ai_test_1 start...\r\n");
	/* init memory */
	ret = hd_common_init(0);
	if (ret != HD_OK) {
		main_error_tag = 1;
		goto exit;
	}
	/*set project config for AI*/
	hd_common_sysconfig(0, (1 << 16), 0, VENDOR_AI_CFG); //enable AI engine
	vendor_ai_global_init();
	ret = mem_init(max_mem);
	if (ret != HD_OK) {
	    main_error_tag = 1;
		DBG_ERR("mem_init fail=%d\n", ret);
		
		goto exit;
	}
	ret = get_mem_block(max_mem);
	if (ret != HD_OK) {
	    main_error_tag = 1;
		DBG_ERR("mem_init fail=%d\n", ret);
		goto exit;
	}
	ret = hd_videoproc_init();
	if (ret != HD_OK) {

		DBG_ERR("hd_videoproc_init fail=%d\n", ret);
		goto exit;
	}
	ret = hd_gfx_init();
	if (ret != HD_OK) {
	    main_error_tag = 1;
		DBG_ERR("hd_gfx_init fail=%d\n", ret);
		goto exit;
	}


	/* create encode_thread (pull_out bitstream)*/
	nn_thread_id = (pthread_t *)calloc(NN_RUN_NET_NUM, sizeof(pthread_t));
	VENDOR_AIS_FLOW_MEM_PARM local_mem = g_mem;

	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		net_parm[idx].mem = get_mem_custom(&local_mem, max_mem, error_file, &main_error_tag);

		if (main_error_tag > 0) {

			DBG_ERR("local mem memory get failed.\r\n ");
			goto exit;
		}

		net_parm[idx].run_id = idx;
		net_parm[idx].p_image_list = test_config.image_list;
		net_parm[idx].test_times = test_config.test_times;
		net_parm[idx].max_model_sizes = max_model_size;
		net_parm[idx].result_tag = 0;
		net_parm[idx].error_tag = 0;
		net_parm[idx].dbg_setting = dbg_setting;
		net_parm[idx].p_model_list = test_config.model_list;
		net_parm[idx].model_nums = test_model_num;

		ret = pthread_create(&nn_thread_id[idx], NULL, nn_thread_api, (VOID*)(&net_parm[idx]));
		if (ret < 0) {
			DBG_ERR("create encode thread failed");
			goto thr_exit;
		}
	}

	/*wait encode thread destroyed*/
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		pthread_join(nn_thread_id[idx], NULL);
	}

thr_exit:

	if (nn_thread_id != NULL) {

		free(nn_thread_id);
	}




exit:

	ret = hd_gfx_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_gfx_uninit fail=%d\n", ret);
		goto main_exit;
	}


	ret = hd_videoproc_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_videoproc_uninit fail=%d\n", ret);
		goto main_exit;
	}

	ret = release_mem_block(max_mem);
	if (ret != HD_OK) {
		DBG_ERR("mem_uninit fail=%d\n", ret);
		goto main_exit;
	}

	ret = mem_uninit();
	if (ret != HD_OK) {
		DBG_ERR("mem_uninit fail=%d\n", ret);
		goto main_exit;
	}

	/*global uninit for ai sdk*/
	vendor_ai_global_uninit();

	ret = hd_common_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_common_uninit fail=%d\n", ret);
		goto main_exit;
	}



main_exit:
	if (fp != NULL)
	{
		fclose(fp);
		fp = NULL;
	}
	if (main_error_tag > 0) {

		DBG_ERR("failed.\r\n");
	} else {
		for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {

			net_parm[0].error_tag += net_parm[idx].error_tag;
		}
		if (net_parm[0].error_tag == 0) {

			DBG_DUMP("success.\r\n");
		} else {
			DBG_ERR("failed.\r\n");
		}
		DBG_DUMP("\r\n");
		DBG_DUMP("ai_test_1 finished…\r\n");
	}
	return ret;
}

