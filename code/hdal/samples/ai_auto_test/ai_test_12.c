/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "hdal.h"
#include "hd_common.h"
#include <sys/sysinfo.h>


#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
#include "net_pre_sample/net_pre_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include <sys/time.h>

// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(alg_net_app_user_sample, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME



#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080
#define MAX_FRAME_CHANNEL       3
#define MAX_FRAME_BYTE          4
#define KBYTE                   1024

#define MAX_RESULT_SIZE             (200 * KBYTE)
#define AI_SAMPLE_TEST_BATCH    DISABLE // v2 not support
#if AI_SAMPLE_TEST_BATCH
#define AI_SAMPLE_MAX_BATCH_NUM 8
#define AI_SAMPLE_MAX_INPUT_NUM 2
#else
#define AI_SAMPLE_MAX_BATCH_NUM 1
#define AI_SAMPLE_MAX_INPUT_NUM 1
#endif
#define YUV_SINGLE_BUF_SIZE     (3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT * AI_SAMPLE_MAX_BATCH_NUM)
#define YUV_OUT_BUF_SIZE        (YUV_SINGLE_BUF_SIZE * AI_SAMPLE_MAX_INPUT_NUM)

#define MBYTE					(1024 * 1024)
#define NN_TOTAL_MEM_SIZE       (400 * MBYTE)
#define NN_RUN_NET_NUM			2
#define NN_RUN_THREAD_NUM	    2


#define MAX_OBJ_NUM             1024
#define AUTO_UPDATE_DIM     	0
#define DYNAMIC_MEM             0
#define MODIFY_ENG_IN_SAMPLE    0

#define VENDOR_AI_CFG  0x000f0000  //ai project config

#define GET_OUTPUT_LAYER_FEATURE_MAP DISABLE
#define SUPPORT_MULTI_BLOB_IN_SAMPLE      1
#define EXTRA_NET_SIZE           (200 * KBYTE)
#define M_NET_MAX_BUF_SIZE       (25000 * KBYTE)
#define NN_USE_DRAM2             ENABLE
#define CONFIG_MAX_SIZE          (256)
#define CONFIG_MAX_ITEMS         (15)
#define IMG_MAX_ITEMS            (15)
#define RESULT_COMPARE           ENABLE
#define NET_RESULT_MAX_SIZE      (230 * KBYTE)
#define CORE_MAX_NUM             2

#define AI_STRCPY(dst, src, dst_size) do { \
	strncpy(dst, src, (dst_size)-1); \
	dst[(dst_size)-1] = '\0'; \
} while(0)


/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/**
	Parameters of network
*/
typedef struct _CONFIG_PARA {
	FLOAT time_threshold_upper;
	FLOAT time_threshold_lower;
	FLOAT bw_threshold_upper;
	FLOAT bw_threshold_lower;
    FLOAT size_threshold_lower;
	FLOAT size_threshold_upper;
	FLOAT memory_threshold_lower;
    FLOAT memory_threshold_upper;
	FLOAT load_threshold_lower;
    FLOAT load_threshold_upper;	
}CONFIG_PARA;

/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
#if DYNAMIC_MEM
static UINT32 mem_size[NN_RUN_NET_NUM] = { 200 * MBYTE, 200 * MBYTE };
#endif


//static HD_COMMON_MEM_VB_BLK g_blk_info[2];
#if AUTO_UPDATE_DIM
UINT32 diff_model_size = 0;
#if defined(__FREERTOS)
static CHAR diff_model_name[NN_RUN_NET_NUM][256] = { "A:\\para\\nvt_stripe_model.bin", "A:\\para\\nvt_stripe_model1.bin" };
#else
static CHAR diff_model_name[NN_RUN_NET_NUM][256] = { "para/nvt_stripe_model.bin", "para/nvt_stripe_model1.bin" };
#endif
#endif

static INT32 split_kvalue(CHAR *p_str, char **key, char **value, CHAR c, UINT32 max_len)
{
 	UINT32 cnt = 0;
	UINT32 len = 0;	
	*key = p_str;	
	while (*p_str != '\0') {
		if (*p_str == c) {
			*p_str = '\0';			
			p_str++;
			len++;
			cnt++;				
			if (cnt > 1) {
				DBG_ERR("can only have one = in config line!");
				return -1;
			}			
			while (*p_str == ' ') {
				p_str++;
				len++;				
				if (len > max_len) {
					DBG_ERR("the config is too long!\r\n");
					return -1;
				}
			}			
			*value = p_str;					
		} else {
			p_str++;	
			len++;	
			if (len > max_len) {
				DBG_ERR("the config is too long!\r\n");
				return -1;
			}
		}
					
	}	
	if (cnt == 0) {
		DBG_ERR("can not find = !\r\n");
		return -1;		
	}		
	return 1;
}

static INT32 remove_blank(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	CHAR *p_str1 = p_str;

	while (*p_str0 != '\0')
	{
		if ((*p_str0 != ' ') && (*p_str0 != '\t') && (*p_str0 != '\n') && (*p_str0 != '\r')){
			*p_str1++ = *p_str0++;
  			} else {
				p_str0++;
				}
	}
	*p_str1 = '\0';
	return 0;
}

static INT32 load_config_file(CHAR *file_path, CHAR key[][CONFIG_MAX_SIZE], CHAR value[][CONFIG_MAX_SIZE], UINT max_items)
{
	FILE *fp;
	//printf("======================%s \r\n",file_path);
	if ((fp = fopen(file_path, "r")) == NULL) {
		DBG_ERR("fopen file path %s error!\r\n", file_path);
		return -1;
	}

	CHAR line[CONFIG_MAX_SIZE * 2];
	UINT32 cnt = 0;
	CHAR * temp_key = NULL;
	CHAR * temp_value = NULL;
	while (1) {
		if (feof(fp))
			break;
		if (fgets(line, CONFIG_MAX_SIZE * 2 - 1, fp) == NULL){
			if (feof(fp)){
					break;
				}
			DBG_ERR("fgets line : %s error!\r\n", line);
			continue;
		}
		remove_blank(line);
		if (line[0] == '#' || line[0] == '\0'){
			continue;
		}

		
		if (split_kvalue(line, &temp_key, &temp_value, '=', CONFIG_MAX_SIZE)){	
			AI_STRCPY(key[cnt], temp_key, CONFIG_MAX_SIZE);
			AI_STRCPY(value[cnt], temp_value, CONFIG_MAX_SIZE);				
		    temp_key = NULL;
			temp_value = NULL;
		} else {
			continue;
		}		
		cnt = cnt + 1;
		if (cnt > max_items){
			DBG_ERR("pauser max_items is too larger %d\r\n", cnt);
		}
	}
	
	if(fp){
		fclose(fp);
		fp = NULL ;
	}
	return cnt;
}

static INT32 flow_config_parse(CHAR key[][CONFIG_MAX_SIZE], CHAR value[][CONFIG_MAX_SIZE], UINT32 cnt, CONFIG_PARA *cpara)
{
	UINT32 idx;	
	for (idx = 0; idx < cnt; idx++){		
		if (strcmp(key[idx], "[time_threshold_upper]") == 0)
		{
			cpara->time_threshold_upper = atof(value[idx]);	
		} else if (strcmp(key[idx], "[time_threshold_lower]") == 0)
		{
			cpara->time_threshold_lower = atof(value[idx]);
		} else if (strcmp(key[idx], "[bw_threshold_upper]") == 0)
		{
			cpara->bw_threshold_upper = atof(value[idx]);
		} else if (strcmp(key[idx], "[bw_threshold_lower]") == 0)
		{
			cpara->bw_threshold_lower = atof(value[idx]);
		} else if (strcmp(key[idx], "[size_threshold_lower]") == 0)
		{
			cpara->size_threshold_lower = atof(value[idx]);
		} else if (strcmp(key[idx], "[size_threshold_upper]") == 0)
		{
			cpara->size_threshold_upper = atof(value[idx]);
		} else if (strcmp(key[idx], "[memory_threshold_lower]") == 0)
		{
			cpara->memory_threshold_lower = atof(value[idx]);
		} else if (strcmp(key[idx], "[memory_threshold_upper]") == 0)
		{
			cpara->memory_threshold_upper = atof(value[idx]);
		}else if (strcmp(key[idx], "[loading_threshold_upper]") == 0)
		{
			cpara->load_threshold_upper = atof(value[idx]);
		}else if (strcmp(key[idx], "[loading_threshold_lower]") == 0)
		{
			cpara->load_threshold_lower = atof(value[idx]);
		}
		
	}
	return 0;
}

UINT32 count_line(CHAR *txt_path)
{
	FILE *fp;
	int flag = 0, count = 0;
	if ((fp = fopen(txt_path, "r")) == NULL){
		return -1;
	}
	while (!feof(fp)){
		flag = fgetc(fp);
		if (flag == '\n')
			count++;
	}
	if(fp){
		fclose(fp);
		fp = NULL ;
	}	
	return count;
}
/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
MAIN(argc, argv)
{
	DBG_DUMP("\r\n");
	DBG_DUMP("ai_test_12 start...\r\n");

	HD_RESULT           ret = 0;
	UINT32 idx = 0;



	UINT32 para_cnt = 0;
	CHAR key[CONFIG_MAX_ITEMS][CONFIG_MAX_SIZE];
	CHAR value[CONFIG_MAX_ITEMS][CONFIG_MAX_SIZE];
	CONFIG_PARA config_para = { 0 };
	FILE *cfp = NULL;
	CHAR error_file[CONFIG_MAX_SIZE];
	UINT32 error_flag = 0;
	CHAR config_file[CONFIG_MAX_SIZE] = "/mnt/sd/AI_auto_test_files/paras/config_12.txt";
	CHAR path_files[256] = "/mnt/sd/AI_auto_test_files";	

	CHAR mname_new[256] = { 0 };
	CHAR mname_old[256] = { 0 };
	CHAR img_name_new[256] = { 0 };
	CHAR img_name_old[256] = { 0 };
	FLOAT time_new, time_old;
	//FLOAT bw_new, bw_old,size_new,memory_new,size_old,memory_old,loading_new,loading_old;
	FLOAT bw_new, bw_old,size_new,memory_new,size_old,memory_old;

	CHAR old_result_txt[256];
	CHAR result_txt[256];
	CHAR diff_result_txt[256];
	CHAR diff_result_txt_name[256];
	CHAR diff_result_err_txt[256];
	CHAR msg[256];
	CHAR chip_id[256];
	BOOL flag = TRUE;
	FLOAT diff_time = 0;
	FLOAT diff_bw = 0;
	FLOAT diff_size = 0;
	FLOAT diff_memory = 0;
	
	

	CHAR new_list[1][256] = { "ai_test_7/result_0.txt"};
	                        
    CHAR result_list[1][256] = { "ai_test_12/ai_test_7_0.txt"};
	CHAR result_err_list[1][256] = { "ai_test_12/err_0.txt" };
	//get CHIP ID
	memset(chip_id, '\0', 256);


	UINT32 cpu_num = 0;
	cpu_num = get_nprocs();
	//printf("cpu num: %d\n", cpu_num);
	
#if defined(_BSP_NA51055_)    //528
	if (cpu_num == 2){
		AI_STRCPY(chip_id, "528",sizeof(chip_id));
		}else if(cpu_num == 1){
			AI_STRCPY(chip_id, "525",sizeof(chip_id));
		}

#elif defined(_BSP_NA51068_)  //323
	AI_STRCPY(chip_id, "323",sizeof(chip_id)); 

#endif
if (strlen(chip_id) == 0) {
		error_flag = 1;
        DBG_ERR("can not get chip ID,suport 525 528 323.\r\n");
        goto eexit;
	}

//printf("chip id : %s\r\n",chip_id);


	//vendor_ais_set_dbg_mode(AI_DUMP_OUT_BIN, dbg_setting);
	CHAR command_test[256];
	sprintf(command_test, "rm -rf %s/output/ai_test_12", path_files);
	system(command_test);
	sprintf(command_test, "mkdir %s/output/ai_test_12", path_files);
	system(command_test);
	sprintf(error_file, "%s/output/ai_test_12/error.txt", path_files);

    if((access(config_file, F_OK )) == -1){
            if (cfp == NULL) {
                cfp = fopen(error_file, "w+");
                if (cfp == NULL) {
                    error_flag = 1;
                    DBG_ERR("main error out file: %s  null\r\n", error_file);
                    goto eexit;
                 } else {
                    error_flag = 1;
                    fprintf(cfp, "result file %s not exits \r\n", config_file);
                    goto eexit;
                }
            } else {
                error_flag = 1;
                fprintf(cfp, "result file %s not exits \r\n", config_file);
                goto eexit;
            }
        }

    for (UINT32 test_num = 0; test_num < 1; test_num++){
        sprintf(old_result_txt, "%s/output_last/%s_%s", path_files, chip_id,new_list[test_num]);
        if((access(old_result_txt, F_OK )) == -1){
            if (cfp == NULL) {
                cfp = fopen(error_file, "w+");
                if (cfp == NULL) {
                    error_flag = 1;
                    DBG_ERR("main error out file: %s  null\r\n", error_file);
                    goto eexit;
                } else {
                    error_flag = 1;
                    fprintf(cfp, "result file %s not exits \r\n", old_result_txt);
                    goto eexit;
                }
            } else {
                error_flag = 1;
                fprintf(cfp, "result file %s not exits \r\n", old_result_txt);
                goto eexit;
            }
        }

        }

    for (UINT32 test_num = 0; test_num < 1; test_num++){
        sprintf(result_txt, "%s/output/%s", path_files, new_list[test_num]);
        if((access(result_txt, F_OK )) == -1){
            if (cfp == NULL)
            {
                cfp = fopen(error_file, "w+");
                if (cfp == NULL) {
                    error_flag = 1;
                    DBG_ERR("main error out file: %s  null\r\n", error_file);
                    goto eexit;
                } else {
                    error_flag = 1;
                    fprintf(cfp, "result file %s not exits \r\n", result_txt);
                    goto eexit;
                }
            } else {
                error_flag = 1;
                fprintf(cfp, "result file %s not exits \r\n", result_txt);
                goto eexit;
            }
        }
        }

	//printf("================load_config_file==============================\n");
	para_cnt = load_config_file(config_file, key, value, CONFIG_MAX_ITEMS);
	//printf("====================load_config_file=============================\n");
	if (para_cnt <= 0){
		//printf("config para file %s is null\r\n",argv[1]);
		if (cfp == NULL) {
			cfp = fopen(error_file, "w+");
			if (cfp == NULL) {
				error_flag = 1;
				DBG_ERR("main error out file: %s  null\r\n", error_file);
			} else {
				error_flag = 1;
				fprintf(cfp, "config para file %s null\r\n", argv[1]);
			}
 		} else {
			error_flag = 1;
			fprintf(cfp, "config para file %s null\r\n", argv[1]);
		}
		goto eexit;
	}


	if (flow_config_parse(key, value, para_cnt, &config_para) < 0){
		//DBG_ERR("config  core  error!\r\n");
		if (cfp == NULL){
			cfp = fopen(error_file, "w+");
			if (cfp == NULL){
				error_flag = 1;
				DBG_ERR("main error out file: %s  null\r\n", error_file);
			}else{
				error_flag = 1;
				fprintf(cfp, "config  core  error\r\n");
			}
		}else{
			error_flag = 1;
			fprintf(cfp, "config  core  error\r\n");

		}
		goto eexit;
	}


	for (UINT32 test_num = 0; test_num < 1; test_num++){

		//printf(new_list[test_num]);  
		sprintf(old_result_txt, "%s/output_last/%s_%s", path_files, chip_id,new_list[test_num]);
		sprintf(result_txt, "%s/output/%s", path_files, new_list[test_num]);
		sprintf(diff_result_txt, "%s/output/%s_%s", path_files, chip_id,result_list[test_num]);
		sprintf(diff_result_err_txt, "%s/output/%s", path_files, result_err_list[test_num]);

		AI_STRCPY(diff_result_txt_name, result_list[test_num],sizeof(diff_result_txt_name));

		UINT32 old_lines = count_line(old_result_txt);
		UINT32 new_lines = count_line(result_txt);
		FILE *fp_new = NULL, *fp_old = NULL;
		fp_new = fopen(result_txt, "r");
		fp_old = fopen(old_result_txt, "r");

		if (fp_new == NULL || old_result_txt == NULL){
			if (cfp == NULL){
				cfp = fopen(error_file, "w+");
				if (cfp == NULL){
					error_flag = 1;
					DBG_ERR("error out file: %s  null\r\n", error_file);
				} else {
					error_flag = 1;
					fprintf(cfp, "cpu time reslt txt is NULL!\r\n");
				}
			} else {
				error_flag = 1;
				fprintf(cfp, "cpu time reslt txt is NULL!\r\n");
			}
			//goto texit;
		} else {
			for (idx = 0; idx < new_lines; idx++) {				
				if (strcmp(new_list[test_num], "ai_test_7/result_0.txt") == 0 ) {
					fscanf(fp_new, "%s %s %f %f %f %f", mname_new, img_name_new, &time_new, &bw_new,&size_new,&memory_new);					
					flag = TRUE;
					for (UINT32 j = 0; j < old_lines; j++) {
						fscanf(fp_old, "%s %s %f %f %f %f", mname_old, img_name_old, &time_old, &bw_old,&size_old,&memory_old);
						if (strcmp(mname_new, mname_old) == 0 && strcmp(img_name_new, img_name_old) == 0){
							flag = FALSE;
							diff_time = time_new - time_old;
							diff_bw = bw_new - bw_old;
							diff_size = size_new - size_old;
							diff_memory = memory_new - memory_old;							
							if ((diff_time / time_old) < config_para.time_threshold_lower || (diff_time / time_old) > config_para.time_threshold_upper ){
								error_flag = 1;
                                sprintf(msg, "echo -n \"%s %s %f %f %f %f\r\n\" >> /mnt/sd/AI_auto_test_files/output/%s",
								mname_new,
								img_name_new,
								diff_time / time_old,
								diff_bw / bw_old,
								diff_size / size_old,
								diff_memory / memory_old,								
								diff_result_txt_name);
							    system(msg);
                                continue;
							}
							if ((diff_bw / bw_old) < config_para.bw_threshold_lower || (diff_bw / bw_old) > config_para.bw_threshold_upper){
								error_flag = 1;
                                sprintf(msg, "echo -n \"%s %s %f %f %f %f\r\n\" >> /mnt/sd/AI_auto_test_files/output/%s",
								mname_new,
								img_name_new,
								diff_time / time_old,
								diff_bw / bw_old,
								diff_size / size_old,
								diff_memory / memory_old,
								diff_result_txt_name);
							    system(msg);
								continue;
							}
							if ((diff_size / size_old) < config_para.size_threshold_lower || (diff_size / size_old) > config_para.size_threshold_upper){
								error_flag = 1;
                                sprintf(msg, "echo -n \"%s %s %f %f %f %f\r\n\" >> /mnt/sd/AI_auto_test_files/output/%s",
								mname_new,
								img_name_new,
								diff_time / time_old,
								diff_bw / bw_old,
								diff_size / size_old,
								diff_memory / memory_old,
								diff_result_txt_name);
							    system(msg);
								continue;
							}

							if ((diff_memory / memory_old) < config_para.memory_threshold_lower || (diff_memory / memory_old) > config_para.memory_threshold_upper){
								error_flag = 1;
                                sprintf(msg, "echo -n \"%s %s %f %f %f %f\r\n\" >> /mnt/sd/AI_auto_test_files/output/%s",
								mname_new,
								img_name_new,
								diff_time / time_old,
								diff_bw / bw_old,
								diff_size / size_old,
								diff_memory / memory_old,
								diff_result_txt_name);
							    system(msg);
							}						  
						}

					}
					fseek(fp_old, 0, SEEK_SET);
					if (flag) {
						//flag =TRUE;
						sprintf(msg, "echo -n \"%s not exits \r\n\" >> /mnt/sd/AI_auto_test_files/output/%s",
							mname_new,
							diff_result_txt_name);
						system(msg);
					}
				}
			}
		}
		//fclose(fp_old);
	if(fp_old){
		fclose(fp_old);
		fp_old = NULL ;
	}
		//fclose(fp_new);
	if(fp_new){
		fclose(fp_new);
		fp_new = NULL ;
	}
	}

//////////////////////////////////////////////////
	CHAR none_cpu_result_txt[256];
	CHAR cpu_result_txt[256];
	CHAR cup_diff_result_txt[256];                    

	sprintf(none_cpu_result_txt, "%s/output/ai_test_7/result_0.txt", path_files);	                                              
	sprintf(cpu_result_txt, "%s/output/ai_test_7/result_cpu_0.txt",path_files);
	sprintf(cup_diff_result_txt, "%s/output/ai_test_12/ai_test_7_cpu.txt", path_files);
	UINT32 non_cpu_lines = count_line(none_cpu_result_txt);
	UINT32 cpu_lines = count_line(cpu_result_txt);	
	FILE *fp_none = NULL, *fp_cpu = NULL;

	if(((access(none_cpu_result_txt, F_OK )) == 0) && ((access(cpu_result_txt, F_OK )) == 0)){
		fp_none = fopen(none_cpu_result_txt, "r");
		fp_cpu = fopen(cpu_result_txt, "r");
		for (idx = 0; idx < cpu_lines; idx++) {
			fscanf(fp_cpu, "%s %s %f ", mname_new, img_name_new, &time_new );					
			flag = TRUE;
			for (UINT32 j = 0; j < non_cpu_lines; j++) {
				//fscanf(fp_none, "%s %s %f", mname_old, img_name_old, &time_old );
				fscanf(fp_none, "%s %s %f %f %f %f", mname_old, img_name_old, &time_old, &bw_new,&size_new,&memory_new);					

				if (strcmp(mname_new, mname_old) == 0 && strcmp(img_name_new, img_name_old) == 0){
					flag = FALSE;
					diff_time = time_new - time_old;
					//ff_bw = bw_new - bw_old;			

					//error_flag = 1;
					sprintf(msg, "echo -n \"%s %s %f \r\n\" >> %s",
					mname_new,
					img_name_new,
					diff_time / time_old,
					cup_diff_result_txt);			
					system(msg);			

				}

			}
				fseek(fp_none, 0, SEEK_SET);

			
		}
	}




	
// diff loading	
		CHAR cpuLoading_txt[256];   
		CHAR last_cpuLoading_txt[256];   
		CHAR cupLoading_diff_txt[256];  
		FLOAT cpuLoading_new, cpuLoading_last;
		FLOAT diff_cpuLoading = 0;

		sprintf(cpuLoading_txt, "%s/output/ai_test_7/result_cpuLoading_0.txt", path_files);
		sprintf(last_cpuLoading_txt, "%s/output_last/%s_ai_test_7/result_cpuLoading_0.txt",path_files,chip_id);
		sprintf(cupLoading_diff_txt, "%s/output/ai_test_12/ai_test_7_cpuLoading.txt", path_files);

		UINT32 cpuLoading_lines = count_line(cpuLoading_txt);  //non_cpu_lines
		UINT32 last_cpuLoading_lines = count_line(last_cpuLoading_txt); //cpu_lines 
		FILE *fp_cpuLoading = NULL, *fp_last_cpuLoading = NULL;

		if(((access(cpuLoading_txt, F_OK )) == 0) && ((access(last_cpuLoading_txt, F_OK )) == 0)){
			//printf("cpu loading test\r\n");
			fp_cpuLoading = fopen(cpuLoading_txt, "r");  //fp_cpu
			fp_last_cpuLoading = fopen(last_cpuLoading_txt, "r");  //fp_none
			for (idx = 0; idx < last_cpuLoading_lines; idx++) {		 	
				fscanf(fp_cpuLoading, "%s %s %f ", mname_new, img_name_new, &cpuLoading_new);	
				for (UINT32 j = 0; j < cpuLoading_lines; j++) {	
					fscanf(fp_last_cpuLoading, "%s %s %f", mname_old, img_name_old, &cpuLoading_last);
					if (strcmp(mname_new, mname_old) == 0 && strcmp(img_name_new, img_name_old) == 0) {
						diff_cpuLoading = cpuLoading_new - cpuLoading_last; 
						if ((diff_cpuLoading / cpuLoading_last) < config_para.load_threshold_lower || (diff_cpuLoading / cpuLoading_last) > config_para.load_threshold_upper){
							error_flag = 1;	
							sprintf(msg, "echo -n \"%s %s %f \r\n\" >> %s",
						    mname_new,
							img_name_new,
							diff_cpuLoading / cpuLoading_last,
							cupLoading_diff_txt);
							system(msg);							
						}
					}
				}
				fseek(fp_last_cpuLoading, 0, SEEK_SET);
			}
		}	
eexit:


	if (error_flag == 0){
		DBG_DUMP("success\r\n");
	} else {
		DBG_ERR("failed\r\n");
	}
	DBG_DUMP("ai_test_12 finished...\r\n");
	return ret;

}







