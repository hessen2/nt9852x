/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
////#include <pthread.h>
#include "hdal.h"
//#include "hd_type.h"
#include "hd_common.h"
//#include "nvtipc.h"
//#include "vendor_dsp_util.h"
//#include "vendor_ai/vendor_ai.h"

#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
//#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
//#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include "vendor_preproc/vendor_preproc.h"
//#include "net_flow_user_sample/net_layer_sample.h"
//#include "custnn/custnn_lib.h"
#include <sys/time.h>
// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(alg_net_app_user_sample, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME
#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080
#define MAX_FRAME_BYTE          4
#define AI_SAMPLE_TEST_BATCH    DISABLE//v2 not support
#if AI_SAMPLE_TEST_BATCH
#define AI_SAMPLE_MAX_BATCH_NUM 8
#else
#define AI_SAMPLE_MAX_BATCH_NUM 1
#define AI_SAMPLE_MAX_INPUT_NUM 1
#endif
#define KBYTE                   1024
#define MBYTE		        	(1024 * 1024)
#define NN_RUN_NET_NUM			2
#define MAX_FRAME_CHANNEL       3
//#define SRC_IN_BUF_SIZE		(MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT * MAX_FRAME_CHANNEL * AI_SAMPLE_MAX_BATCH_NUM * AI_SAMPLE_MAX_INPUT_NUM * MAX_FRAME_BYTE)
//#define EXTRA_BUF_SIZE		20 * MBYTE
//#define YUV_SINGLE_BUF_SIZE	(3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT * AI_SAMPLE_MAX_BATCH_NUM)
//#define YUV_OUT_BUF_SIZE	(YUV_SINGLE_BUF_SIZE * AI_SAMPLE_MAX_INPUT_NUM)
//#define MAX_OBJ_NUM		1024
#define AUTO_UPDATE_DIM     	0
#define DYNAMIC_MEM             1
#define MODIFY_ENG_IN_SAMPLE 0
#define MAX_LEN          (256)
#define VENDOR_AI_CFG  0x000f0000  //ai project config

#define GET_OUTPUT_LAYER_FEATURE_MAP DISABLE

#define NN_USE_DRAM2             ENABLE
#define RESULT_COMPARE           ENABLE
#define NET_RESULT_MAX_SIZE      (10 * MBYTE)
#define PREPROC_TEST             1
#define PREPROC_SCALE            1
#define _MAX(a,b) (((a)>(b))?(a):(b))
UINT32 NN_TOTAL_MEM_SIZE       	= 0;
/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/**
	Parameters of network
*/
typedef struct TEST_LIST_PATH {
	CHAR	model_list_1[256];
	CHAR	model_list_2[256];
	CHAR	image_list[256];
	CHAR	model_path[256];
	CHAR	image_path[256];
	UINT32 test_times;
	UINT32 debug_mode;
	UINT32 scale_tag;
} TEST_LIST_PATH;

typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	CHAR *p_image_path;
	CHAR *p_model_list;
	CHAR *p_image_list;
	VENDOR_AIS_IMG_PARM	src_img;
	VENDOR_AIS_FLOW_MEM_PARM mem;
	UINT32 run_id;
    UINT32 test_times;
	UINT32 model_nums;
	UINT32 image_nums;
	UINT32 result_tag;
	UINT32 error_tag;
	UINT32 max_model_sizes;
	UINT32 scale_flag;

} VENDOR_AIS_NETWORK_PARM;
typedef struct CONFIG_OPT {
	CHAR key[256];
	CHAR value[256];
}CONFIG_OPT;

typedef struct _TEST_IMAGE_INFO {
    CHAR img_name[128];
    UINT32 img_w;
	UINT32 img_h;
	UINT32 ch;
	UINT32 bits;
	HD_VIDEO_PXLFMT img_fmt;
   // UINT32  img_size;
}TEST_IMAGE_INFO;


typedef struct _PREPROC_PARM{
	//AI_PREPROC_SIZE g_dim;
	UINT32 sub_type;
	VENDOR_AIS_FLOW_MEM_PARM mem;
	UINT32 run_id;
	CHAR  *p_image_list;
    UINT32 test_times;
	UINT32 image_nums;
	UINT32 result_tag;
	UINT32 scale_flag;
	UINT32 error_tag;
	UINT32 dbg_setting;
}PREPROC_PARM;
/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
/*
#if defined(__FREERTOS)
static CHAR model_name[NN_RUN_NET_NUM][256]	= { "A:\\para\\nvt_model.bin", "A:\\para\\nvt_model1.bin"};
#else
static CHAR model_name[NN_RUN_NET_NUM][256]	= { "para/nvt_model.bin", "para/nvt_model1.bin"};
#endif
*/
//static UINT32 mem_size[NN_RUN_NET_NUM]		= { 200 * MBYTE, 200 * MBYTE };
static VENDOR_AIS_FLOW_MEM_PARM g_mem		= {0};



//static BOOL is_net_proc				= TRUE;
//static BOOL is_net_run[NN_SUPPORT_NET_MAX]	= {FALSE};

static HD_COMMON_MEM_VB_BLK g_blk_info[2];
#if AUTO_UPDATE_DIM
UINT32 diff_model_size = 0;
#if defined(__FREERTOS)
static CHAR diff_model_name[NN_RUN_NET_NUM][256]	= { "A:\\para\\nvt_stripe_model.bin", "A:\\para\\nvt_stripe_model1.bin" };
#else
static CHAR diff_model_name[NN_RUN_NET_NUM][256]	= { "para/nvt_stripe_model.bin", "para/nvt_stripe_model1.bin" };
#endif
#endif
static CHAR root_path[128] = "/mnt/sd/AI_auto_test_files/";
UINT32	mem_size[NN_RUN_NET_NUM] = { 0, 0 };

//NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/


static INT32 remove_blank(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	CHAR *p_str1 = p_str;

	while (*p_str0 != '\0')
	{
		if ((*p_str0 != ' ') && (*p_str0 != '\t') && (*p_str0 != '\n')){
			*p_str1++ = *p_str0++;
		} else {
			p_str0++;
		}
	}

	*p_str1 = '\0';
	return TRUE;
}
static INT32 split_kvalue(CHAR *p_str, CONFIG_OPT* p_config, CHAR c)
{
	CHAR *p_key = p_str;
	CHAR *p_value = p_str;
	CHAR *p_local = p_str;
	INT32 cnt = 0;

	// check only one '='
	while (*p_local != '\0') {

		p_local++;
		if (*p_local == c) {
			cnt++;
		}
	}
	if (cnt != 1) {
		return FALSE;
	}

	p_local = p_str;
	while (*p_local != c) {

		p_local++;
	}
	p_value = p_local + 1;
	*p_local = '\0';
	strcpy(p_config->key, p_key);
	strcpy(p_config->value, p_value);
	return TRUE;
}

static INT32 config_parser(CHAR *file_path,TEST_LIST_PATH *test_config)
{

    FILE *fp_tmp = NULL;

    if ((fp_tmp = fopen(file_path, "r")) == NULL)
    {
        DBG_ERR("config.txt is not exist!\n");
        return 0;
    }
    CHAR line[512];
    while (1) {

		if (feof(fp_tmp)) {
			break;
		}
		if (fgets(line, 512 - 1, fp_tmp) == NULL) {

			if (feof(fp_tmp)) {
				break;
			}
			DBG_ERR("fgets line: %s error!\n", line);
			continue;
		}

        remove_blank(line);

        if (line[0] == '#' || line[0] == '\0')
            continue;
        CONFIG_OPT tmp;
        if (split_kvalue(line, &tmp, '=') != 1){

            printf("pauser line : %s error!\r\n", line);
            continue;
        }

        if (strcmp(tmp.key, "[path/model_list_1]") == 0) {
            strcpy(test_config->model_list_1, tmp.value);
        }
		if (strcmp(tmp.key, "[path/model_list_2]") == 0) {
			strcpy(test_config->model_list_2, tmp.value);
        }
        if (strcmp(tmp.key, "[path/image_list]") == 0) {
            strcpy(test_config->image_list, tmp.value);
        }
        if (strcmp(tmp.key, "[path/model_path]") == 0) {
            strcpy(test_config->model_path, tmp.value);
        }
        if (strcmp(tmp.key, "[path/image_path]") == 0) {
            strcpy(test_config->image_path, tmp.value);
        }
        if (strcmp(tmp.key, "[test/num]") == 0) {
            test_config->test_times = atoi(tmp.value);
        }
		if (strcmp(tmp.key, "[debug/mode]") == 0) {
            test_config->debug_mode = atoi(tmp.value);
        }
		if (strcmp(tmp.key, "[scale/tag]") == 0) {
		test_config->scale_tag = atoi(tmp.value);
        }


    }


    fclose(fp_tmp);
    return 1;
}


static int mem_init(VOID)
{
	HD_RESULT                 ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};
	UINT32                    total_mem_size = 0;
	UINT32                    i ;

	/* Allocate parameter buffer */
	if (g_mem.va != 0) {
		printf("err: mem has already been inited\r\n");
		return -1;
	}

#if DYNAMIC_MEM
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
        total_mem_size += vendor_preproc_get_mem_size();
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif

	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = total_mem_size;
	mem_cfg.pool_info[0].blk_cnt = 1;
	mem_cfg.pool_info[0].ddr_id = DDR_ID1;

	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		DBG_ERR("hd_common_mem_init err: %d\r\n", ret);
		}
		return ret;
}

static INT32 get_mem_block(VOID)
{
	HD_RESULT                 ret = HD_OK;
	UINT32                    pa, va;
	HD_COMMON_MEM_VB_BLK      blk;
	UINT32 total_mem_size = 0;
	UINT32 i;

#if NN_USE_DRAM2
    HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID1;
#else
    HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID0;
#endif

#if DYNAMIC_MEM
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
	//total_mem_size += vendor_preproc_get_mem_size();
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif

	/* Allocate scale out buffer */
	if (g_mem.va != 0) {
	DBG_DUMP("mem has already been inited\r\n");
	return -1;
	}
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, total_mem_size, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		DBG_ERR("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto mem_exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		DBG_DUMP("hd_common_mem_blk2pa fail,pa=%08x\r\n", pa);
		ret = -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
						  pa,total_mem_size);
	g_blk_info[0] = blk;
	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_mem_size);
		if (ret != HD_OK) {
			DBG_DUMP("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}
	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = total_mem_size;

mem_exit:
	return ret;
}
static HD_RESULT release_mem_block(VOID)
{
	HD_RESULT ret = HD_OK;
    UINT32 total_mem_size = 0,i;

#if DYNAMIC_MEM
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
        //total_mem_size += vendor_preproc_get_mem_size();
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif

	/* Release in buffer */


	if (g_mem.va) {

		ret = hd_common_mem_munmap((void *)g_mem.va, total_mem_size);
		if (ret != HD_OK) {
			DBG_DUMP("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		DBG_DUMP("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	return ret;
}
static HD_RESULT mem_uninit(void)
{
	return hd_common_mem_uninit();
}


VENDOR_AIS_FLOW_MEM_PARM get_mem_custom(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size,CHAR *file,UINT32 *flag)
{
        VENDOR_AIS_FLOW_MEM_PARM mem = {0};
        FILE *fp=NULL;
        required_size = ALIGN_CEIL_32(required_size);
	if(required_size <= valid_mem->size) {
		mem.va = valid_mem->va;
        mem.pa = valid_mem->pa;
		mem.size = required_size;

		valid_mem->va += required_size;
        valid_mem->pa += required_size;
		valid_mem->size -= required_size;
	} else {
		DBG_ERR("Required size %ld > valid size %ld\r\n", required_size, valid_mem->size);
	    fp=fopen(file,"w+");
		if (fp == NULL) {

			*flag = 1;
		} else {

            DBG_ERR("Required size %ld > valid size %ld\r\n", required_size, valid_mem->size);
            fclose(fp);
            *flag=1;
        }


	}
	return mem;
}
HD_RESULT clear_mem_custom(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size)
{

    required_size = ALIGN_CEIL_32(required_size);
    valid_mem->va -= required_size;
    valid_mem->pa -= required_size;
    valid_mem->size += required_size;

	return HD_OK;
}



#if !RESULT_COMPARE
static UINT32 ai_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;


	fd = fopen(p_filename, "rb");
	if (!fd) {
		//DBG_DUMP("cannot read %s\r\n", p_filename);
		return 0;
	}
	fseek ( fd, 0, SEEK_END );
	file_size = ALIGN_CEIL_4( ftell(fd) );
	fseek ( fd, 0, SEEK_SET );

	read_size = fread ((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
		//DBG_DUMP("model size mismatch, real = %d, idea = %d\r\n", (int)read_size, (int)file_size);
	}
	fclose(fd);
	return read_size;
}
#endif

#if RESULT_COMPARE
static UINT32 custom_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;

	fd = fopen(p_filename, "rb");
	if (!fd) {
		//DBG_ERR("cannot read %s\r\n", p_filename);
		return 0;
	}

	fseek ( fd, 0, SEEK_END );

	file_size = ALIGN_CEIL_4( ftell(fd) );
        file_size = ftell(fd);
	fseek ( fd, 0, SEEK_SET );

	read_size = fread ((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
		/*
		 *  DBG_ERR("size mismatch, read_size = %d, file_size = %d\r\n", (int)read_size, (int)file_size);
		 * return 0;
		 */
	}
	fclose(fd);

	return read_size;
}
#endif
static INT32 image_list_parser(CHAR *file_path, TEST_IMAGE_INFO *test_image_total,UINT32 image_number)
	{
		   int img_w = 0, img_h = 0, ch = 0, bits = 0;
		   CHAR fmt[50] = { 0 };
		   HD_VIDEO_PXLFMT img_fmt = 0;
		   CHAR img_path[128] = {0};
		   FILE* fp_img_tmp= NULL;
		   fp_img_tmp = fopen(file_path, "r");
		   if (fp_img_tmp == NULL) {
			   printf("open test_images_list_info fail.\r\n");
		   }

		   for (UINT32 i=0; i<image_number; i++){

			   fscanf(fp_img_tmp, "%s %d %d %d %d %s\n", img_path, &img_w, &img_h, &ch, &bits, fmt);
			   if (img_w <= 0) {
				   printf("img_w read fail\r\n");
				   //goto exit;
			   }
			   if (img_h <= 0) {
				   printf("img_h read fail\r\n");
				   //goto exit;
			   }
			   if (ch <= 0) {
				   printf("ch read fail\r\n");
				   //goto exit;
			   }
			   if (bits <=0 || bits > 32 || bits % 8 != 0){
				   printf("bits read fail\r\n");
				   //goto exit;
			   }
			   if (strcmp(fmt, "HD_VIDEO_PXLFMT_YUV420")== 0) {
				   img_fmt = 0x520c0420;
			   } else if (strcmp(fmt, "HD_VIDEO_PXLFMT_Y8") == 0) {
				   img_fmt = 0x51080400;
			   } else if (strcmp(fmt, "HD_VIDEO_PXLFMT_RGB888_PLANAR") == 0) {
				   img_fmt = 0x23180888;
			   } else {
				   printf("fmt not enough.\r\n");

		          //more see hd_type.h
			   }

			   test_image_total[i].img_w = img_w;
			   test_image_total[i].img_h = img_h;
			   test_image_total[i].ch = ch;
			   test_image_total[i].bits= bits;
			   strcpy(test_image_total[i].img_name,img_path);
			   test_image_total[i].img_fmt= img_fmt;
               //test_image_total[i].img_size = test_image_total[i].img_w * test_image_total[i].img_h * _MAX(3, test_image_total[i].ch) * (test_image_total[i].bits/ 8);
		   }
		   fclose(fp_img_tmp);
		   return 1;
	}


#if RESULT_COMPARE

static INT8 result_compare(UINT32 va,UINT32 data_va,UINT32 size)
{

    UINT32 idx;
    UINT8 *gt=(UINT8 *)va;
    UINT8 *data=(UINT8 *)data_va;

    for(idx=0;idx<size;idx++){

        if(((*(gt++))-(*(data++))) !=0)
            return -1;
    }
    return 0;

}
#endif
 #if PREPROC_TEST
INT32 preproc_init_parm(AI_PREPROC_PARM* p_param, UINT32 input_addr, UINT32 output_addr, AI_PREPROC_SIZE dim, UINT32 sub_type,UINT32 scale_flag)
{
	AI_PREPROC_IO_INFO io_info = {0};
	AI_PREPROC_TRANS_FMT_PARM fmt_trans_info = {0};
	AI_PREPROC_SUB_PARM sub_info = {0};
#if PREPROC_SCALE
	AI_PREPROC_SCALE_PARM scale_info={0};
#endif
	if (p_param == NULL) {
		printf("input parameter is NULL\n");
		return -1;
	}

	// init  nue parameter
	vendor_preproc_init_parm(p_param);

	// set io parameter
	memset(&io_info, 0, sizeof(AI_PREPROC_IO_INFO));
	io_info.pa = input_addr;
	io_info.type = AI_PREPROC_TYPE_UINT8;
	io_info.size.width = dim.width;
	io_info.size.height = dim.height;
	io_info.size.channel = dim.channel;
	io_info.ofs.line_ofs = dim.width;
	vendor_preproc_set_io_parm(p_param, &io_info, AI_PREPROC_FMT_YUV420, 0, AI_PREPROC_CH_Y);

	io_info.pa = input_addr + io_info.size.width*io_info.size.height;
	vendor_preproc_set_io_parm(p_param, &io_info, AI_PREPROC_FMT_YUV420, 0, AI_PREPROC_CH_UVPACK);

	memset(&io_info, 0, sizeof(AI_PREPROC_IO_INFO));
	io_info.pa = output_addr;
	io_info.type = AI_PREPROC_TYPE_UINT8;
	io_info.size.width = dim.width;
	io_info.size.height = dim.height;
	io_info.size.channel = 3;
	io_info.ofs.line_ofs = dim.width;
	vendor_preproc_set_io_parm(p_param, &io_info, AI_PREPROC_FMT_RGB, 1, AI_PREPROC_CH_R);
	io_info.pa = output_addr + io_info.size.width*io_info.size.height;
	vendor_preproc_set_io_parm(p_param, &io_info, AI_PREPROC_FMT_RGB, 1, AI_PREPROC_CH_G);
	io_info.pa = output_addr + 2*io_info.size.width*io_info.size.height;
	vendor_preproc_set_io_parm(p_param, &io_info, AI_PREPROC_FMT_RGB, 1, AI_PREPROC_CH_B);

	// set func parameter
	fmt_trans_info.mode = AI_PREPROC_YUV2RGB;
	vendor_preproc_set_func_parm(p_param, &fmt_trans_info, AI_PREPROC_TRANS_FMT_EN);
#if PREPROC_SCALE
	//scale
    if(scale_flag == 1){

	scale_info.scl_out_size.width = dim.width/2;
	scale_info.scl_out_size.height = dim.height/2;
	scale_info.scl_out_size.channel = dim.channel;

	vendor_preproc_set_func_parm(p_param,&scale_info,AI_PREPROC_SCALE_EN);
	dim.width=dim.width/2;
	dim.height=dim.height/2;
	if (sub_type == 0) {
		sub_info.mode = AI_PREPROC_SUB_SCALAR;
		sub_info.sub_val[0] = 128;
		sub_info.sub_val[1] = 127;
		sub_info.sub_val[2] = 126;
	} else {
		sub_info.mode = AI_PREPROC_SUB_PLANAR;
		sub_info.sub_planar.pa = output_addr + 3*io_info.size.width*io_info.size.height/4;
		sub_info.sub_planar.ofs.line_ofs = dim.width*3;
		sub_info.sub_planar.size.width = dim.width;
		sub_info.sub_planar.size.height = dim.height;
	}
    }

#else

	if (sub_type == 0) {
		sub_info.mode = AI_PREPROC_SUB_SCALAR;
		sub_info.sub_val[0] = 128;
		sub_info.sub_val[1] = 127;
		sub_info.sub_val[2] = 126;
	} else {
		sub_info.mode = AI_PREPROC_SUB_PLANAR;
		sub_info.sub_planar.pa = output_addr + 3*io_info.size.width*io_info.size.height;
		sub_info.sub_planar.ofs.line_ofs = dim.width*3;
		sub_info.sub_planar.size.width = dim.width;
		sub_info.sub_planar.size.height = dim.height;
	}
#endif
	vendor_preproc_set_func_parm(p_param, &sub_info, AI_PREPROC_SUB_EN);

	return 0;
 }


static VOID *nue2_thread_api(VOID *arg)
{
    HD_RESULT ret;
    PREPROC_PARM *preproc_parm=(PREPROC_PARM *)arg;
    VENDOR_AIS_FLOW_MEM_PARM *mem=&preproc_parm->mem;
	//VENDOR_AIS_FLOW_MEM_PARM local_mem = g_mem;
	VENDOR_AIS_FLOW_MEM_PARM tem_mem;
    AI_PREPROC_PARM preproc_param = {0};
    UINT32 image_number = 0;
    int img_w = 0, img_h = 0, ch = 0, bits = 0;
    CHAR fmt[50] = { 0 };
    UINT32  test_times = preproc_parm->test_times;
    UINT32 run_id = preproc_parm->run_id;
    KDRV_AI_PREPROC_INFO preproc_mem = {0};
    CHAR image_name[128];
    AI_PREPROC_SIZE g_dim;
    UINT32 output_pa,parm_pa,input_pa;
    UINT32 input_addr,output_addr,parm_va;
    CHAR error_file[MAX_LEN];
    UINT32 para_mem_size;
	UINT32 dbg_set = preproc_parm->dbg_setting;
    VENDOR_AIS_FLOW_MEM_PARM input_mem,para_mem,output_first_mem;
    UINT32 idx;
    FILE *fp=NULL,*fp_i=NULL;
    //UINT32 mem_flag=0;
    UINT32 file_size = 0;
    CHAR image_path[256] = {0};
    CHAR image_path_file[256];
   // UINT32 input_ofs;
	UINT32 outsize;
	UINT32 scale_flag = preproc_parm->scale_flag;
    para_mem_size = vendor_preproc_get_mem_size();
    sprintf(error_file,"%s/output/ai_test_10/error_%d.txt",root_path,(int) run_id);
	
	tem_mem.va=mem->va;
    tem_mem.pa=mem->pa;
    tem_mem.size=mem->size;
	
	
	
	para_mem.va = tem_mem.va;
	para_mem.pa = tem_mem.pa;
	para_mem.size = tem_mem.size;

	fp_i = fopen(preproc_parm->p_image_list, "r");
    //calculate image num
	while (1) {
		if (feof(fp_i)) {
			break;
		}
		if (fscanf(fp_i, "%s %d %d %d %d %s\n", image_name, &img_w, &img_h, &ch, &bits, fmt) == EOF) {

        if(dbg_set){
        DBG_DUMP("fscanf image name : %s error!\r\n", image_name);
        }
        continue;
    }
    sprintf(image_path_file, "%s/test_images/%s",root_path,image_name);
    image_number = image_number+1;
    }
	fclose(fp_i);


	//parser imagelist
	TEST_IMAGE_INFO *test_image_total = (TEST_IMAGE_INFO *)calloc(image_number, sizeof(TEST_IMAGE_INFO));

	if (!(image_list_parser(preproc_parm->p_image_list, test_image_total, image_number))) {

	DBG_ERR("image list parser failed!!!\n");
	goto pre_exit;
	}


    input_mem.va = tem_mem.va + para_mem_size ;
	input_mem.pa = tem_mem.pa+ para_mem_size;

      for (UINT32 bb = 0; bb< image_number;bb++){
		input_addr = input_mem.va;
		input_pa = input_mem.pa;
        parm_va=para_mem.va;
        parm_pa=para_mem.pa;


	    g_dim.width = test_image_total[bb].img_w;
        g_dim.height = test_image_total[bb].img_h;
	    g_dim.channel= test_image_total[bb].ch;
		//UINT32 image_size = test_image_total[bb].img_size;


		if(dbg_set>0){
		DBG_DUMP("g_dim.width=%d, g_dim.height = %d, g_dim.channel=%d\r\n",g_dim.width ,g_dim.height , g_dim.channel);}
	   // input_ofs = g_dim.width * g_dim.height * g_dim.channel;
        strcpy(image_name,test_image_total[bb].img_name);

	    //scale_flag
        if(dbg_set>0){
		DBG_DUMP("scale_flag=%d\r\n",scale_flag);
		}
		outsize=g_dim.width*g_dim.height*g_dim.channel;

        sprintf(image_path,"%s/test_images/%s",root_path,image_name);

        file_size=custom_load_file(image_path,input_addr);
		//image_size = _MAX(file_size,image_size);

		output_first_mem.va = input_addr + file_size ;
		output_first_mem.pa = input_pa + file_size;
		output_addr = output_first_mem.va;
		output_pa = output_first_mem.pa;
		////check image
		if (file_size == 0) {
			if (fp == NULL) {
				fp = fopen(error_file, "w+");
				if (fp == NULL) {
					preproc_parm->result_tag = 1;
					DBG_ERR("error file is null.\r\n");
				} else {
					preproc_parm->result_tag = 1;
					fprintf(fp, " null  %s  null  2\r\n", image_name);
				}
			} else {
				preproc_parm->result_tag = 1;
				fprintf(fp, " null  %s  null  2\r\n", image_name);
			}

			continue;
		}

	// init preproc param
	   if (preproc_init_parm(&preproc_param, input_pa, output_pa, g_dim, 0,preproc_parm->scale_flag) < 0){

			preproc_parm->result_tag=1;
		    DBG_ERR("pthread%u: preproc_init_parm fail\r\n",run_id);
            goto pre_exit;
	    }

		// start thread loop
		vendor_preproc_auto_alloc_mem(parm_pa, parm_va, &preproc_mem);
		hd_common_mem_flush_cache((VOID *)input_addr, outsize);
		hd_common_mem_flush_cache((VOID *)output_addr, outsize);
		memset((VOID *)(output_addr + outsize), 0, outsize);    // clear result buffer
		hd_common_mem_flush_cache((VOID *)(output_addr + outsize), outsize);

		ret = vendor_preproc_run_v2(&preproc_mem, &preproc_param, NULL);

		if (ret != 0){

			preproc_parm->result_tag=1;
			DBG_ERR(" thread%u:vendor_preproc_run_v2 fail\r\n",run_id);
			goto pre_exit;
		}
		hd_common_mem_flush_cache((VOID *)output_addr,outsize);
		memcpy((void *)output_first_mem.va,(UINT8 *)output_addr,outsize);
		//VENDOR_AIS_FLOW_MEM_PARM output_mem;
		//output_mem.va = output_addr + outsize;
		//output_mem.pa = output_pa + outsize;

	 	for(idx=0;idx< test_times;idx++){

			hd_common_mem_flush_cache((VOID *)input_addr, outsize);
			hd_common_mem_flush_cache((VOID *)output_addr, outsize);
			memset((VOID *)(output_addr + outsize), 0, outsize); /* clear result buffer */
			hd_common_mem_flush_cache((VOID *)(output_addr + outsize), outsize);

			ret = vendor_preproc_run_v2(&preproc_mem, &preproc_param, NULL);

			if (ret != 0) {

					preproc_parm->result_tag=1;
			        DBG_ERR("NUE pthread%u: iteration %u vendor_preproc_run_v2 fail\r\n",run_id,idx);
					goto pre_exit;
			}
			if(result_compare(output_first_mem.va,output_addr,outsize)<0){

				if(fp == NULL){

					fp=fopen(error_file,"w+");
					if(fp==NULL){

						preproc_parm->result_tag=1;
						DBG_ERR("error file is null\r\n");
					} else {
						fprintf(fp, " null  %s  %d  4\r\n", image_name, idx);
						preproc_parm->result_tag = 1;
						preproc_parm->error_tag = 1;
					}
				} else {
					fprintf(fp, " null %s  %d  4\r\n", image_name, idx);
					preproc_parm->result_tag = 1;
					preproc_parm->error_tag = 1;
				}
				break;
			}
		}
	}


pre_exit:
    if(fp != NULL){

        fclose(fp);

    }
    return 0;

}
#endif


/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/

MAIN(argc, argv)
{

   #if PREPROC_TEST
	PREPROC_PARM *preproc_parm;
	pthread_t preproc_thread_id[NN_RUN_NET_NUM];

	preproc_parm=(PREPROC_PARM *)calloc(NN_RUN_NET_NUM,sizeof(PREPROC_PARM));

#endif

	//VENDOR_AIS_NETWORK_PARM net_parm[NN_RUN_NET_NUM] = {0};

	HD_RESULT   ret =0;
	UINT32 idx = 0;
	UINT32 dbg_setting;
	UINT32 thread_total_mem;
	char error_file[MAX_LEN]="/mnt/sd/AI_auto_test_files/output/ai_test_10/main_error.txt";
	UINT32 main_error_tag=0;
    CHAR command_test[50];
	CHAR command_test_1[50];
	sprintf(command_test, "rm -rf %s/output/ai_test_10", root_path);
	system(command_test);
	sprintf(command_test_1, "mkdir -p %s/output/ai_test_10", root_path);
    system(command_test_1);


    CHAR config_path[128] ;
    TEST_LIST_PATH test_config;
    CHAR image_list_file[256];
    sprintf(config_path, "%s/paras/config_10.txt", root_path);
    if (!(config_parser(config_path,&test_config))){

 		DBG_DUMP("parse config file is null.\r\n");
		main_error_tag=1;
		goto main_exit;
    }
	dbg_setting =  test_config.debug_mode;
    strcpy(image_list_file,test_config.image_list);

   FILE *fp = NULL;
  for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {

        strcpy(image_list_file,test_config.image_list);
        fp = fopen(image_list_file, "r");
        if (fp == NULL) {
			DBG_ERR("open image info fail.\r\n");
			main_error_tag=1;
            goto main_exit;
	    }
	fclose(fp);
}
	FILE *mem_fp = NULL;
	CHAR mem_txt_path[MAX_LEN];
    UINT32 memory_max[3]= {0};
	sprintf(mem_txt_path,"%s/gt_results/memory_max.txt",root_path);
	mem_fp = fopen(mem_txt_path, "r");
	if (mem_fp == NULL) {
		DBG_ERR("open mem txt fail.\r\n");
	} else {
        for (UINT32 idx = 0; idx < 3; idx++) {
		   fscanf(mem_fp, "%ld", &memory_max[idx]);
	    }
	}
	if (mem_fp) {
		fclose(mem_fp);
	}
    UINT32 max_mem = memory_max[0];

	DBG_DUMP("\r\n");
	DBG_DUMP("ai_test_10 start...\r\n");
	// init memory
	
	for(idx=0;idx<NN_RUN_NET_NUM;idx++)
	{
		mem_size[idx]=max_mem;
	}
		
	ret = hd_common_init(0);
	if (ret != HD_OK) {
		main_error_tag = 1;
		DBG_ERR("hd_common_init fail=%d\n", ret);
		goto exit;
	}
    hd_common_sysconfig(0, (1<<16), 0, VENDOR_AI_CFG); //enable AI engine
	vendor_ai_global_init();
	ret = vendor_preproc_init();
	if (ret != HD_OK) {
		main_error_tag = 1;
		DBG_ERR("preproc init fail\n");
		goto exit;
	}

	ret = mem_init();
	if (ret != HD_OK) {
	    main_error_tag = 1;
		DBG_ERR("mem_init fail=%d\n", ret);
		goto exit;
	}
	ret = get_mem_block();
	if (ret != HD_OK) {
		DBG_ERR("mem_init fail=%d\n", ret);
		goto exit;
	}
	ret = hd_videoproc_init();
	if (ret != HD_OK) {
		main_error_tag = 1;
		DBG_ERR("hd_videoproc_init fail=%d\n", ret);
		goto exit;
	}

	ret = hd_gfx_init();
	if (ret != HD_OK) {
	    main_error_tag = 1;
		DBG_ERR("hd_gfx_init fail=%d\n", ret);
		goto exit;
	}

	VENDOR_AIS_FLOW_MEM_PARM local_mem = g_mem;
#if PREPROC_TEST

	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
	    thread_total_mem = max_mem;	
		preproc_parm[idx].mem = get_mem_custom(&local_mem,thread_total_mem,error_file,&main_error_tag) ;
		if(main_error_tag >0){		   
		    DBG_ERR("local mem memory get failed.\r\n ");
			goto exit;
		}

		preproc_parm[idx].run_id = idx;
		preproc_parm[idx].p_image_list = test_config.image_list;
		preproc_parm[idx].test_times = test_config.test_times;
		preproc_parm[idx].result_tag = 0;
		preproc_parm[idx].error_tag = 0;

		preproc_parm[idx].dbg_setting = dbg_setting;
		preproc_parm[idx].scale_flag = test_config.scale_tag;

		ret = pthread_create(&preproc_thread_id[idx], NULL, nue2_thread_api, (VOID*)(&preproc_parm[idx]));
		if (ret < 0) {
			DBG_ERR("create encode thread failed");
			goto exit;
		}
	}



	// wait encode thread destroyed
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
		pthread_join(preproc_thread_id[idx], NULL);
	}
#endif

exit:


    ret = hd_gfx_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_gfx_uninit fail=%d\n", ret);
        goto main_exit;
	}


	ret = hd_videoproc_uninit();
	if (ret != HD_OK) {
		main_error_tag = 1;
		DBG_ERR("hd_videoproc_uninit fail=%d\n", ret);
		goto main_exit;
	}
	ret = release_mem_block();
	if (ret != HD_OK) {
	    main_error_tag = 1;
		DBG_ERR("mem_uninit fail=%d\n", ret);
		goto main_exit;
	}
	ret = mem_uninit();
	if (ret != HD_OK) {
		main_error_tag = 1;
		DBG_ERR("mem_uninit fail=%d\n", ret);
		goto main_exit;
	}

	// global uninit for ai sdk

	ret = hd_common_uninit();
	if (ret != HD_OK) {
		main_error_tag = 1;
		DBG_ERR("hd_common_uninit fail=%d\n", ret);
		goto main_exit;
	}
	ret = vendor_preproc_uninit();
	if (ret != 0) {
		main_error_tag = 1;
		DBG_ERR("vendor_preproc_uninit fail.");
		goto main_exit;
		}

   vendor_ai_global_uninit();

main_exit:
	/*
	 * if(fp!=NULL)
	 *  {
	 *    fclose(fp);
	 *  fp=NULL;
	 * }
	 */
    if(main_error_tag >0){

    DBG_ERR("failed.\r\n");

	} else {
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++){

	preproc_parm[0].error_tag += preproc_parm[idx].error_tag;

	}
	if(preproc_parm[0].error_tag == 0){

		printf("success.\r\n");

	    }else{

		DBG_ERR("failed.\r\n");
	    }
	DBG_DUMP("\r\n");
	DBG_DUMP("ai_test_10 finished��\r\n");
    }
	if (preproc_parm != NULL) {
		free(preproc_parm);
	}
	return ret;
}




