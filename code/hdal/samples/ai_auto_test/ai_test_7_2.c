/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "hdal.h"
#include "hd_common.h"


#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
#include "net_pre_sample/net_pre_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include <sys/time.h>
#include "vendor_mau.h"

//cpu loading
//#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <linux/limits.h>
#include <sys/times.h>
#include <sys/types.h>
//#include "linux_cpu_loading.h"
typedef long long int num;

// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(alg_net_app_user_sample, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME



#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080
#define MAX_FRAME_CHANNEL       3
#define MAX_FRAME_BYTE          4
#define KBYTE                   1024

#define MAX_RESULT_SIZE             (200 * KBYTE)
#define AI_SAMPLE_TEST_BATCH    1 // v2 not support
#if AI_SAMPLE_TEST_BATCH
#define AI_SAMPLE_MAX_BATCH_NUM 8
#define AI_SAMPLE_MAX_INPUT_NUM 2
#else
#define AI_SAMPLE_MAX_BATCH_NUM 1
#define AI_SAMPLE_MAX_INPUT_NUM 1
#endif
#define YUV_SINGLE_BUF_SIZE     (3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT * AI_SAMPLE_MAX_BATCH_NUM)
#define YUV_OUT_BUF_SIZE        (YUV_SINGLE_BUF_SIZE * AI_SAMPLE_MAX_INPUT_NUM)

#define MBYTE					(1024 * 1024)
#define NN_TOTAL_MEM_SIZE       (400 * MBYTE)
#define NN_RUN_NET_NUM			2
#define NN_RUN_THREAD_NUM	    2


#define MAX_OBJ_NUM             1024
#define AUTO_UPDATE_DIM     	0
#define DYNAMIC_MEM             0
#define MODIFY_ENG_IN_SAMPLE    0

#define VENDOR_AI_CFG  0x000f0000  //ai project config

#define GET_OUTPUT_LAYER_FEATURE_MAP DISABLE
#define SUPPORT_MULTI_BLOB_IN_SAMPLE      1
#define EXTRA_NET_SIZE           (200 * KBYTE)
#define M_NET_MAX_BUF_SIZE       (25000 * KBYTE)
#define NN_USE_DRAM2             ENABLE
#define CONFIG_MAX_SIZE          (256)
#define CONFIG_MAX_ITEMS         (10)
#define IMG_MAX_ITEMS            (10)
#define RESULT_COMPARE           DISABLE
#define NET_RESULT_MAX_SIZE      (18 * MBYTE)

extern INT32 vendor_mau_ch_mon_start(int ch, int rw, int dram);
extern UINT64 vendor_mau_ch_mon_stop(int ch, int dram);

#define AI_STRCPY(dst, src, dst_size) do { \
	strncpy(dst, src, (dst_size)-1); \
	dst[(dst_size)-1] = '\0'; \
} while(0)


/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/**
	Parameters of network
*/
typedef struct _SCMT_NETWORK_PARM {
	UINT32 net_id;
	VENDOR_AIS_FLOW_MAP_MEM_PARM net_manager;
} SCMT_NETWORK_PARM;

typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	VENDOR_AIS_IMG_PARM	src_img[AI_SAMPLE_MAX_INPUT_NUM];
	VENDOR_AIS_FLOW_MEM_PARM mem;
	UINT32 run_id;
	UINT32 max_model_size;
} VENDOR_AIS_NETWORK_PARM;

typedef struct _SCMT_PARM {
	VENDOR_AIS_NETWORK_PARM model_parm;
	char *model_list_path;
	char img_list_path[IMG_MAX_ITEMS][CONFIG_MAX_SIZE];
	char *path_files;
	UINT32 models_number;
	UINT32 imagelist_num;
	UINT32 model_iterations;
	//UINT32 core;
	UINT32 result_flag;
	UINT32 debug_mode;

}SCMT_PARM;
typedef struct _SCMT_THREAD_INIT_PARM {
	UINT32 net_id;
} SCMT_THREAD_INIT_PARM;

typedef struct _SCMT_FLOW_PARM {
	BOOL    net_lock;
	INT32   init_state;
} SCMT_FLOW_PARM;


typedef enum
{

	SCMT_IMG_MEM_STRUCT = 0,
	SCMT_RESULT_MEM_STRUCT,
	SCMT_NETWORK_PARM_STRUCT,
	ENUM_DUMMY4WORD(SCMT_STRUCT_TYPE)
} SCMT_STRUCT_TYPE;

typedef struct _SCMT_IMG_MEM {
	VENDOR_AIS_FLOW_MEM_PARM src;
} SCMT_IMG_MEM;
typedef struct _SCMT_RESULT_MEM {
	VENDOR_AIS_FLOW_MEM_PARM result;
} SCMT_RESULT_MEM;
typedef struct _CONFIG_PARA {
	char model_list_1[CONFIG_MAX_SIZE];
	char model_list_2[CONFIG_MAX_SIZE];
	char image_list_1[CONFIG_MAX_SIZE];
	char image_list_2[CONFIG_MAX_SIZE];
	//UINT32 core;
	UINT32 model_iterations;
	UINT32 thread_numbers;
	UINT32 debug_mode;
}CONFIG_PARA;

/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
//cpu loading

num pid;
char tcomm[PATH_MAX];
char state;

num ppid;
num pgid;
num sid;
num tty_nr;
num tty_pgrp;

num flags;
num min_flt;
num cmin_flt;
num maj_flt;
num cmaj_flt;
num utime;
num stimev;

num cutime;
num cstime;
num priority;
num nicev;
num num_threads;
num it_real_value;

unsigned long long start_time;

num vsize;
num rss;
num rsslim;
num start_code;
num end_code;
num start_stack;
num esp;
num eip;

num pending;
num blocked;
num sigign;
num sigcatch;
num wchan;
num zero1;
num zero2;
num exit_signal;
num cpu;
num rt_priority;
num policy;

long tickspersec;

FILE *input, *cpu_ptr;

static BOOL is_net_proc						= TRUE;


/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/

void readone(num *x) { fscanf(input, "%lld ", x); }
void readunsigned(unsigned long long *x) { fscanf(input, "%llu ", x); }
void readstr(char *x) {  fscanf(input, "%s ", x);}
void readchar(char *x) {  fscanf(input, "%c ", x);}

void printone(char *name, num x) {  printf("%20s: %lld\n", name, x);}
void printonex(char *name, num x) {  printf("%20s: %016llx\n", name, x);}
void printunsigned(char *name, unsigned long long x) {  printf("%20s: %llu\n", name, x);}
void printchar(char *name, char x) {  printf("%20s: %c\n", name, x);}
void printstr(char *name, char *x) {  printf("%20s: %s\n", name, x);}
void printtime(char *name, num x) {  printf("%20s: %f\n", name, (((double)x) / tickspersec));}

int gettimesinceboot(void) {
	FILE *procuptime;
	int sec, ssec;

	procuptime = fopen("/proc/uptime", "r");
	fscanf(procuptime, "%d.%ds", &sec, &ssec);
	fclose(procuptime);
	return (sec*tickspersec)+ssec;
}

void printtimediff(char *name, num x) {
	int sinceboot = gettimesinceboot();
	int running = sinceboot - x;
	time_t rt = time(NULL) - (running / tickspersec);
	char buf[1024];

	strftime(buf, sizeof(buf), "%m.%d %H:%M", localtime(&rt));
	//printf("%20s: %s (%u.%us)\n", name, buf, running / tickspersec, running % tickspersec);
}






#if DYNAMIC_MEM

static UINT32 mem_size[NN_RUN_NET_NUM] = { 150 * MBYTE, 150 * MBYTE };
#endif

static VENDOR_AIS_FLOW_MEM_PARM g_mem = { 0 };
static HD_COMMON_MEM_VB_BLK g_blk_info[2];
//static UINT32 pthread_flag_gen_init=1;
static pthread_mutex_t     lock;

#if AUTO_UPDATE_DIM
UINT32 diff_model_size = 0;
#if defined(__FREERTOS)
static CHAR diff_model_name[NN_RUN_NET_NUM][256] = { "A:\\para\\nvt_stripe_model.bin", "A:\\para\\nvt_stripe_model1.bin" };
#else
static CHAR diff_model_name[NN_RUN_NET_NUM][256] = { "para/nvt_stripe_model.bin", "para/nvt_stripe_model1.bin" };
#endif
#endif

//NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/

void cal_linux_cpu_loading(int loop_num, double *cpu_user, double *cpu_sys) {
	//printf("1. cal_linux_cpu_loading\r\n");
	pid_t process_id = getpid();
	//printf("2. cal_linux_cpu_loading\r\n");
	tickspersec = sysconf(_SC_CLK_TCK);
	//printf("3. cal_linux_cpu_loading\r\n");
	char filePath[200];
	sprintf(filePath, "/proc/%d/stat", (int)process_id);

	input = NULL;
	cpu_ptr = NULL;

	double pre_cpu_time = 0;
	double pre_utime = 0;
	double pre_stimev = 0;

	double cpu_avg_user = 0.0;
	double cpu_avg_sys = 0.0;
	for(int i = 0; i <= loop_num; ++i){//do num + 1 times
		//printf("read proc/stat\r\n");
		cpu_ptr = fopen("/proc/stat", "r");  

		char cpu_str[20];
		fscanf(cpu_ptr, "%s ", cpu_str);
		double cpu_time = 0;
		long cpu_temp = 0;
		//printf("read proc/stat cpu_tmp\r\n");
		for(int i = 0; i < 9; ++i)
		{
		  fscanf(cpu_ptr, "%lu ", &cpu_temp);
		  cpu_time += cpu_temp;
		}
		fclose(cpu_ptr);

		//printf("read %s\r\n", filePath);
		input = fopen(filePath, "r");
		readone(&pid);
		readstr(tcomm);
		readchar(&state);
		readone(&ppid);
		readone(&pgid);
		readone(&sid);
		readone(&tty_nr);
		readone(&tty_pgrp);
		readone(&flags);
		readone(&min_flt);
		readone(&cmin_flt);
		readone(&maj_flt);
		readone(&cmaj_flt);
		readone(&utime);
		readone(&stimev);
		readone(&cutime);
		readone(&cstime);
		readone(&priority);
		readone(&nicev);
		readone(&num_threads);
		readone(&it_real_value);
		readunsigned(&start_time);
		readone(&vsize);
		readone(&rss);
		readone(&rsslim);
		readone(&start_code);
		readone(&end_code);
		readone(&start_stack);
		readone(&esp);
		readone(&eip);
		readone(&pending);
		readone(&blocked);
		readone(&sigign);
		readone(&sigcatch);
		readone(&wchan);
		readone(&zero1);
		readone(&zero2);
		readone(&exit_signal);
		readone(&cpu);
		readone(&rt_priority);
		readone(&policy);
		fclose(input);

		//printf("cpu time = %f, utime = %f, preutime = %f, sub = %f\r\n", cpu_time - pre_cpu_time, utime, pre_utime, utime - pre_utime);
		double user_util = 100 * (utime - pre_utime) / (cpu_time - pre_cpu_time);
		double sys_util = 100 * (stimev - pre_stimev) / (cpu_time - pre_cpu_time);
		//printf("cpu(user) = %f, cpu(system) = %f\r\n", user_util, sys_util);

		pre_utime = utime;
		pre_stimev = stimev;
		pre_cpu_time = cpu_time;
		sleep(3);
		if(i != 0){
			cpu_avg_user += user_util;
			cpu_avg_sys += sys_util;
		}
	}
	*cpu_user = cpu_avg_user / loop_num;
	*cpu_sys = cpu_avg_sys / loop_num;
}


static int mem_init(void)
{
	HD_RESULT              ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = { 0 };
	UINT32 total_mem_size = 0;


#if DYNAMIC_MEM
	UINT32 i;
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif

	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = total_mem_size;
	mem_cfg.pool_info[0].blk_cnt = 1;

#if NN_USE_DRAM2
	mem_cfg.pool_info[0].ddr_id = DDR_ID1;
#else
	mem_cfg.pool_info[0].ddr_id = DDR_ID0;
#endif



	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		printf("hd_common_mem_init err: %d\r\n", ret);
	}
	return ret;
}

static INT32 get_mem_block(VOID)
{
	HD_RESULT                 ret = HD_OK;
	UINT32                    pa, va;
	HD_COMMON_MEM_VB_BLK      blk;
	UINT32 total_mem_size = 0;


#if NN_USE_DRAM2
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID1;
#else
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID0;
#endif

#if DYNAMIC_MEM
	UINT32 i;
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif



	/* Allocate parameter buffer */
	if (g_mem.va != 0) {		
		return -1;
	}
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, total_mem_size, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		//DBG_DUMP("hd_common_mem_get_block fail\r\n");
		ret = HD_ERR_NG;
		goto mexit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		//DBG_DUMP("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, total_mem_size);
	g_blk_info[0] = blk;

	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_mem_size);
		if (ret != HD_OK) {
			//DBG_DUMP("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}
	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = total_mem_size;

mexit:
	return ret;
}

static HD_RESULT release_mem_block(VOID)
{
	HD_RESULT ret = HD_OK;
	UINT32 total_mem_size = 0;

#if DYNAMIC_MEM
	UINT32 i;
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif


	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, total_mem_size);
		if (ret != HD_OK) {
			//DBG_DUMP("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		//DBG_DUMP("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	return ret;
}

static HD_RESULT mem_uninit(void)
{
	return hd_common_mem_uninit();
}


VENDOR_AIS_FLOW_MEM_PARM scmt_getmem_v1(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size, CHAR *file, UINT32 *flag, UINT32 debugm)
{
	VENDOR_AIS_FLOW_MEM_PARM mem = { 0 };
	//FILE *fp=NULL;
	required_size = ALIGN_CEIL_32(required_size);
	if (required_size <= valid_mem->size) {
		mem.va = valid_mem->va;
		mem.pa = valid_mem->pa;
		mem.size = required_size;

		valid_mem->va += required_size;
		valid_mem->pa += required_size;
		valid_mem->size -= required_size;
	} else {
		if (debugm == 1) {
			DBG_ERR("Required size %d > total memory size %d\r\n", required_size, valid_mem->size);
		}
		*flag = 1;
	}
	return mem;
}


VENDOR_AIS_FLOW_MEM_PARM scmt_getmem_v2(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size, UINT32 run_id, CHAR *file, UINT32 *flag, UINT32 debugm)
{
	VENDOR_AIS_FLOW_MEM_PARM mem = { 0 };
	//FILE *fp=NULL;
	required_size = ALIGN_CEIL_32(required_size);
	if (required_size <= valid_mem->size) {
		mem.va = valid_mem->va;
		mem.pa = valid_mem->pa;
		mem.size = required_size;

		valid_mem->va += required_size;
		valid_mem->pa += required_size;
		valid_mem->size -= required_size;
	} else {
		if (debugm == 1) {
			DBG_ERR("pthread %u Required size %d > total memory size %d\r\n", run_id, required_size, valid_mem->size);
		}		
		*flag = 1;
	}
	return mem;
}


HD_RESULT scmt_clearmem(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size)
{

	required_size = ALIGN_CEIL_32(required_size);
	valid_mem->va -= required_size;
	valid_mem->pa -= required_size;
	valid_mem->size += required_size;

	return HD_OK;
}

VENDOR_AIS_FLOW_MEM_PARM scmt_va_align(VENDOR_AIS_FLOW_MEM_PARM buf)
{
	VENDOR_AIS_FLOW_MEM_PARM align_buf;
	UINT32 align_size;

	//align_buf.va = ALIGN_CEIL_16(buf.va);
	align_buf.va = ALIGN_CEIL_32(buf.va);

	align_size = align_buf.va - buf.va;
	align_buf.pa = buf.pa + align_size;
	align_buf.size = buf.size - align_size;

	return align_buf;
}
static INT32 split_kvalue(CHAR *p_str, char **key, char **value, CHAR c, UINT32 max_len)
{
 	UINT32 cnt = 0;
	UINT32 len = 0;
	
	*key = p_str;
	
	while (*p_str != '\0') {
		if (*p_str == c) {
			*p_str = '\0';			
			p_str++;
			len++;
			cnt++;		
			
			if (cnt > 1) {
				DBG_ERR("can only have one = in config line!");
				return -1;
			}
			
			while (*p_str == ' ') {
				p_str++;
				len++;				
				if (len > max_len) {
					DBG_ERR("the config is too long!\r\n");
					return -1;
				}
			}			
			*value = p_str;					
		} else {
			p_str++;	
			len++;	
			if (len > max_len) {
				DBG_ERR("the config is too long!\r\n");
				return -1;
			}
		}
					
	}
	
	if (cnt == 0) {
		DBG_ERR("can not find = !\r\n");
		return -1;		
	}		
	return 1;
}

static INT32 remove_blank(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	CHAR *p_str1 = p_str;

	while (*p_str0 != '\0') {
		if ((*p_str0 != ' ') && (*p_str0 != '\t') && (*p_str0 != '\n') && (*p_str0 != '\r')) {
			*p_str1++ = *p_str0++;
		} else {
			p_str0++;
		}
	}

	*p_str1 = '\0';
	return 0;
}

static INT32 load_config_file(CHAR *file_path, CHAR key[][CONFIG_MAX_SIZE], CHAR value[][CONFIG_MAX_SIZE], UINT max_items)
{
	FILE *fp;
	if ((fp = fopen(file_path, "r")) == NULL) {
		return -1;
	}

	CHAR line[CONFIG_MAX_SIZE * 2];
	UINT32 cnt = 0;
	CHAR * temp_key = NULL;
	CHAR * temp_value = NULL;
	while (1)
	{
		if (feof(fp)) {
			break;
		}
		if (fgets(line, CONFIG_MAX_SIZE * 2 - 1, fp) == NULL){
			if (feof(fp)) {
				break;
			}
			continue;
		}
		remove_blank(line);
		if (line[0] == '#' || line[0] == '\0') {
			continue;
		}

		
		if (split_kvalue(line, &temp_key, &temp_value, '=', CONFIG_MAX_SIZE)){	
			AI_STRCPY(key[cnt], temp_key, CONFIG_MAX_SIZE);
			AI_STRCPY(value[cnt], temp_value, CONFIG_MAX_SIZE);				
		    temp_key = NULL;
			temp_value = NULL;
		} else {
			continue;
		}


		
		cnt = cnt + 1;
		if (cnt > max_items) {
			DBG_ERR("pauser max_items is too larger %d\r\n",cnt);
		}
	}
	if(fp){
		fclose(fp);
		fp = NULL ;
	}

	return cnt;
}
static INT32 flow_config_parse(CHAR key[][CONFIG_MAX_SIZE], CHAR value[][CONFIG_MAX_SIZE], UINT32 cnt, CONFIG_PARA *cpara)
{
	UINT32 idx;
	for (idx = 0; idx < cnt; idx++) {		
		if (strcmp(key[idx], "[debug_mode]") == 0) {
			cpara->debug_mode = atoi(value[idx]);
		}
		if (strcmp(key[idx], "[path/model_list_1]") == 0) {
			AI_STRCPY(cpara->model_list_1, value[idx],sizeof(cpara->model_list_1));
		} else if (strcmp(key[idx], "[path/model_list_2]") == 0) {
			AI_STRCPY(cpara->model_list_2, value[idx],sizeof(cpara->model_list_2));
		} else if (strcmp(key[idx], "[path/image_list_1]") == 0) {
			AI_STRCPY(cpara->image_list_1, value[idx],sizeof(cpara->image_list_1));
		} else if (strcmp(key[idx], "[path/image_list_2]") == 0) {
			AI_STRCPY(cpara->image_list_2, value[idx],sizeof(cpara->image_list_2));
		} else if (strcmp(key[idx], "[model_iterations]") == 0) {
			cpara->model_iterations = atoi(value[idx]);
		} else if (strcmp(key[idx], "[thread_numbers]") == 0) {
			cpara->thread_numbers = atoi(value[idx]);
		}
	}
	return 0;

}

#if !RESULT_COMPARE
static UINT32 ai_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;
	fd = fopen(p_filename, "rb");
	if (!fd) {
		return 0;
	}

	fseek(fd, 0, SEEK_END);
	file_size = ALIGN_CEIL_4(ftell(fd));
	fseek(fd, 0, SEEK_SET);

	read_size = fread((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
		DBG_DUMP("model size mismatch, real = %ld, idea = %ld\r\n", (int)read_size, (int)file_size);
	}
	if(fd){
		fclose(fd);
		fd = NULL ;
	}
	return read_size;
}
#endif


VENDOR_AIS_FLOW_MEM_PARM scmt_getmem2(VENDOR_AIS_FLOW_MEM_PARM *p_total_mem, UINT32 required_size, UINT32 align_size)
{
	VENDOR_AIS_FLOW_MEM_PARM mem = { 0 };
	if (p_total_mem == NULL) {
		DBG_ERR("mem ptr is null\r\n");
		return mem;
	}

	if ((align_size & (align_size - 1)) != 0) {
		DBG_ERR("do not support align size = %d, use 4 instead\r\n", align_size);
		align_size = 4;
	}

	UINT32 va = ALIGN_CEIL(p_total_mem->va, align_size);
	UINT32 pa = ALIGN_CEIL(p_total_mem->pa, align_size);
	UINT32 sz = p_total_mem->size - (pa - p_total_mem->pa);

	if (required_size <= sz) {
		mem.va = va;
		mem.pa = pa;
		mem.size = required_size;

		p_total_mem->va = va + required_size;
		p_total_mem->pa = pa + required_size;
		p_total_mem->size = sz - required_size;
	} else {
		DBG_ERR("required size %d > total memory size %d\r\n", required_size, sz);
	}
	return mem;
}









static UINT32 scmt_load_file(CHAR *p_filename, UINT32 va, UINT32 size)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;

	fd = fopen(p_filename, "rb");
	if (!fd) {
		DBG_ERR("cannot read %s\r\n", p_filename);
		return 0;
	}

	fseek(fd, 0, SEEK_END);
	file_size = ALIGN_CEIL_4(ftell(fd));
	file_size = ftell(fd);
	fseek(fd, 0, SEEK_SET);

	read_size = fread((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size || read_size > size) {
		DBG_ERR("size mismatch, real = %d, idea = %d\r\n", (int)read_size, (int)file_size);
		return -1;
	}

	if(fd){
		fclose(fd);
		fd = NULL ;
	}
	return read_size;
}

#if RESULT_COMPARE

static BOOL vendor_ais_write_file(CHAR *filepath, UINT32 addr, UINT32 size)
{
#if NET_FLOW_USR_SAMPLE_LAYER_OUT
	FILE *fsave = NULL;

	fsave = fopen(filepath, "wb");
	if (fsave == NULL) {
		DBG_ERR("fopen fail\n");
		return FALSE;
	}

	fwrite((UINT8 *)addr, size, 1, fsave);
	
	if(fsave){
		fclose(fsave);
		fsave = NULL ;
	}
#endif
	return TRUE;
}


static INT8 result_compare_v1(UINT32 va, UINT32 allsize, UINT8* data, UINT32 size)
{

	UINT32 idx;
	UINT8 *gt = (UINT8 *)va;
	if (allsize < size)
	{
		return -1;
	}
	for (idx = 0; idx < size; idx++)
	{

		if (((*(gt++)) - (*(data++))) != 0)
			return -1;
	}
	return 0;

}
#endif


static BOOL find_semicolon(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	while (*p_str0 != '\0') {
		if (*p_str0 == ';') {
			return TRUE;
		}
		else {
			p_str0++;
		}
	}
	*p_str0 = '\0';
	return FALSE;
}


int spilt_fun(char *line, char c, char out[][256])
{
	//char *p_line = line;
	char *a, *b;
	a = b = line;
	int ii = 0;
	while (1)
	{

		if (*b == '\0'){
			break;
		}

		if (*b == c){
			memcpy(out[ii], a, b - a);
			out[ii][b - a] = '\0';
			ii++;
			a = b = b + 1;
			continue;
		}
		b++;
	}
	memcpy(out[ii], a, b - a);
	out[ii][b - a] = '\0';

	return 0;
}

void bubble_sort(UINT32 arr[], int len) {
	int i, j, temp;
	for (i = 0; i < len - 1; i++) {
		for (j = 0; j < len - 1 - i; j++) {
			if (arr[j] > arr[j + 1]) {
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}


typedef struct _VENDOR_AIS_NETWORK_PARM_CPU_LOADING {	
	VENDOR_AIS_FLOW_MEM_PARM max_model_mem;
	VENDOR_AIS_FLOW_MEM_PARM rslt_new_mem;
	VENDOR_AIS_IMG_PARM *p_src_img ;
	UINT32 net_id ;
} VENDOR_AIS_NETWORK_PARM_CPU_LOADING;



void* nn_thread_api_cpu_loading(VOID *arg) {
    //////set cpu stress
    VENDOR_AIS_NETWORK_PARM_CPU_LOADING *p_net_parm = (VENDOR_AIS_NETWORK_PARM_CPU_LOADING*)arg;
    VENDOR_AIS_FLOW_MEM_PARM max_model_mem = p_net_parm->max_model_mem;
	VENDOR_AIS_FLOW_MEM_PARM rslt_new_mem = p_net_parm->rslt_new_mem;
	VENDOR_AIS_IMG_PARM *p_src_img = p_net_parm->p_src_img;
	UINT32 net_id = p_net_parm->net_id;
	do {
		vendor_ais_proc_net(max_model_mem, rslt_new_mem, p_src_img, net_id);
	} while(is_net_proc);

	return 0;
}


static VOID *scmt_thread_api(VOID *arg)
{
	UINT32 count = 0;
	UINT32 counti = 0;
	HD_RESULT ret;
	SCMT_PARM *p_scmt_parm = (SCMT_PARM *)arg;
	VENDOR_AIS_NETWORK_PARM *p_net_parm = &p_scmt_parm->model_parm;
	
	VENDOR_AIS_FLOW_MEM_PARM src_img[AI_SAMPLE_MAX_INPUT_NUM] = { 0 };

	//VENDOR_AIS_FLOW_MEM_PARM model_mem;
	CHAR path_files[50];
	CHAR line[512];

	CHAR modellist_path[CONFIG_MAX_SIZE];
	CHAR imglist_path[2][CONFIG_MAX_SIZE];

	CHAR name[50] = { 0 };
	CHAR mname[50] = { 0 };
	CHAR model_path[CONFIG_MAX_SIZE] = { 0 };
	UINT32 debugm = p_scmt_parm->debug_mode, run_id = p_net_parm->run_id;	

	CHAR imgpath[CONFIG_MAX_SIZE] = { 0 };
	CHAR fmt[50] = { 0 };
	pthread_t nn_thread_id_cpu_loading;


	VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem = &p_net_parm->mem;
	VENDOR_AIS_FLOW_MEM_PARM tem_mem;


	VENDOR_AIS_NETWORK_PARM_CPU_LOADING net_parm_cpu_loading;
	VENDOR_AIS_FLOW_MEM_PARM max_model_mem;
	VENDOR_AIS_FLOW_MEM_PARM rslt_new_mem;
	VENDOR_AIS_IMG_PARM *p_src_img = p_net_parm->src_img;
	UINT32 net_id = run_id;

	VENDOR_AIS_FLOW_MAP_MEM_PARM mem_manager;
	
	UINT32  model_size = 0;
	UINT32 file_size = 0;

	CHAR msg[200] = { 0 };
	UINT32 idx = 0, jdx = 0, kdx = 0;
	UINT32 max_src_size = MAX_FRAME_WIDTH*MAX_FRAME_HEIGHT*MAX_FRAME_CHANNEL*AI_SAMPLE_MAX_BATCH_NUM;
	UINT32 bit_num;
	UINT32 blob_num;
	UINT32 tflag = 0;
	CHAR pattern_name[200] = { 0 };

	CHAR blob_model_name[50];
	// blob and bitdepth	
	UINT32 io_num_m=0;	
	UINT32 *p_input_blob_info_m=NULL;
	NN_DATA in_layer_mem_m={0};
	
#if !CNN_25_MATLAB
	NN_IN_OUT_FMT *input_info = NULL;
#endif

	UINT32 req_size = 0;
	UINT32 iterations, model_num;
	FILE *fp = NULL, *fpp = NULL;
	FILE *cfp = NULL;
	CHAR error_file[CONFIG_MAX_SIZE];
#if AI_SAMPLE_TEST_BATCH
	VENDOR_AIS_IMG_PARM *pb_src_img = NULL;
	UINT32 input_proc_idx[AI_SAMPLE_MAX_BATCH_NUM];
	UINT32 tmp_src_va;
	UINT32 tmp_src_pa;
	UINT32 src_img_size;
	UINT32 batch_i;
	UINT32 input_proc_num = 0;
#endif
	//compare result
#if RESULT_COMPARE
	CHAR img_save_path[CONFIG_MAX_SIZE];
	//CHAR command_test[128];
	//CHAR out_img_path[CONFIG_MAX_SIZE];
	NN_DATA output_layer_iomem = { 0 };
	CHAR cname[50] = { 0 };
	NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
	UINT32 num_output_layer = 0, net_out_size = 0, out_temp_va = 0, out_temp_gt_va = 0;

	CHAR *imgsplit_name;
	FILE *gfp = NULL;
	CHAR command_test[CONFIG_MAX_SIZE];
	CHAR out_img_path[CONFIG_MAX_SIZE];
	VENDOR_AIS_FLOW_MEM_PARM net_result;
	VENDOR_AIS_FLOW_MEM_PARM net_out_result;
	VENDOR_AIS_FLOW_MEM_PARM ft_out_result = { 0 };

#endif



	p_scmt_parm->result_flag = 0;

	model_num = p_scmt_parm->models_number;
	iterations = p_scmt_parm->model_iterations;
	if (debugm == 1) {
		DBG_WRN("pthread %d:  model_iterations:%u\r\n", (INT)net_id, iterations);
	}

	tem_mem.va = p_tot_mem->va;
	tem_mem.pa = p_tot_mem->pa;
	tem_mem.size = p_tot_mem->size;

	AI_STRCPY(path_files, p_scmt_parm->path_files,sizeof(path_files));

	sprintf(error_file, "%s/output/ai_test_7/error_%d.txt", path_files, (int)net_id);
	AI_STRCPY(modellist_path, p_scmt_parm->model_list_path,sizeof(modellist_path));
	for (idx = 0; idx < p_scmt_parm->imagelist_num; idx++) {
		AI_STRCPY(imglist_path[idx], p_scmt_parm->img_list_path[idx],sizeof(imglist_path[idx]));
	}

	src_img[0] = scmt_getmem_v2(&tem_mem, max_src_size, run_id, error_file, &tflag, debugm);
	src_img[1] = scmt_getmem_v2(&tem_mem, max_src_size, run_id, error_file, &tflag, debugm);

	if (tflag > 0) {
		p_scmt_parm->result_flag = 1;
		goto texit;
	}
	max_model_mem = scmt_getmem_v2(&tem_mem, p_net_parm->max_model_size, run_id, error_file, &tflag, debugm);
	if (tflag > 0) {
		p_scmt_parm->result_flag = 1;
		goto texit;
	}

	rslt_new_mem = scmt_getmem_v2(&tem_mem, sizeof(VENDOR_AIS_RESULT_INFO) + MAX_OBJ_NUM * sizeof(VENDOR_AIS_RESULT), run_id, error_file, &tflag, debugm);
	if (tflag > 0) {
		p_scmt_parm->result_flag = 1;
		goto texit;
	}

#if RESULT_COMPARE
	net_result = scmt_getmem_v2(&tem_mem, NET_RESULT_MAX_SIZE, run_id, error_file, &tflag, debugm);
	if (tflag > 0) {
		p_scmt_parm->result_flag = 1;
		goto texit;
	}
	net_out_result = scmt_getmem_v2(&tem_mem, NET_RESULT_MAX_SIZE, run_id, error_file, &tflag, debugm);
	if (tflag > 0) {
		p_scmt_parm->result_flag = 1;
		goto texit;
	}
	if (debugm == 1) {
		ft_out_result = scmt_getmem_v2(&tem_mem, NET_RESULT_MAX_SIZE, run_id, error_file, &tflag, debugm);
		if (tflag > 0)
		{
			p_scmt_parm->result_flag = 1;
			goto texit;

		}
		req_size = src_img[0].size + src_img[1].size + rslt_new_mem.size + max_model_mem.size + net_result.size + net_out_result.size + ft_out_result.size;
	} else {

		req_size = src_img[0].size + src_img[1].size + rslt_new_mem.size + max_model_mem.size + net_result.size + net_out_result.size;

	}

	if (req_size > p_tot_mem->size) {
		p_scmt_parm->result_flag = 1;
		if (debugm == 1) {
			DBG_ERR("pthread %d: require memory is not enough(%d), need(%d)\r\n", (INT)net_id, (INT)p_tot_mem->size, (INT)req_size);
		}
		goto texit;
	}


#else

	req_size = src_img[0].size + src_img[1].size + rslt_new_mem.size + max_model_mem.size;

	if (req_size > p_tot_mem->size) {
		p_scmt_parm->result_flag = 1;
		if (debugm == 1) {
			DBG_ERR("pthread %d:  require memory is not enough(%d), need(%d)\r\n", (INT)net_id, (INT)p_tot_mem->size, (INT)req_size);
		}
		goto texit;
	}
#endif



	fp = fopen(modellist_path, "r");
	if (fp == NULL) {
		p_scmt_parm->result_flag = 1;
		if (debugm == 1) {
			DBG_ERR("pthread %u : %s NULL!\r\n", net_id, modellist_path);
		}
		goto texit;
	}
	for (idx = 0; idx < model_num; idx++) {
		//fscanf(fp, "%s %ld %ld %ld", mname, &core, &bit_num, &blob_num);
		fscanf(fp, "%s", mname);
		sprintf(model_path, "%s/model_bins/%s/sdk/nvt_model.bin", path_files, mname);

#if RESULT_COMPARE
		file_size = vendor_ais_load_file(model_path, max_model_mem.va, &output_layer_info, &num_output_layer);
#else
		file_size = ai_load_file(model_path, max_model_mem.va);
#endif
		if (file_size == 0) {
			if (cfp == NULL)
			{
				cfp = fopen(error_file, "w+");
				if (cfp == NULL) {
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("error out file: %s  null\r\n", error_file);
					}
				} else {
					p_scmt_parm->result_flag = 1;
					fprintf(cfp, "%s  NULL NULL 1\r\n", mname);
				}
			} else {
				p_scmt_parm->result_flag = 1;
				fprintf(cfp, "%s  NULL NULL 1\r\n", mname);
			}
			continue;
		}

		model_size = vendor_ais_auto_alloc_mem(&max_model_mem, &mem_manager);
		if (model_size > max_model_mem.size) {
			if (cfp == NULL) {
				cfp = fopen(error_file, "w+");
				if (cfp == NULL) {
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("error out file: %s  null\r\n", error_file);
					}
				} else {
					p_scmt_parm->result_flag = 1;
					fprintf(cfp, "%s NULL NULL 1\r\n", mname);
				}
			} else {
				p_scmt_parm->result_flag = 1;
				fprintf(cfp, "%s NULL NULL 1\r\n", mname);
			}
			continue;
		}

		memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
		memset((VOID *)rslt_new_mem.va, 0, rslt_new_mem.size);    // clear result buffer
		pthread_mutex_lock(&lock);
		ret = vendor_ais_net_gen_init(mem_manager, net_id);
		pthread_mutex_unlock(&lock);

		if (ret != HD_OK) {
			if (cfp == NULL) {
				cfp = fopen(error_file, "w+");
				if (cfp == NULL) {
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("error out file: %s  null\r\n", error_file);
					}
				} else {
					p_scmt_parm->result_flag = 1;
					fprintf(cfp, "%s NULL NULL 1\r\n", mname);
				}
			} else {
				p_scmt_parm->result_flag = 1;
				fprintf(cfp, "%s NULL NULL 1\r\n", mname);
			}

			goto gen_init_fail;
		}

		count = count + 1;

#if !CNN_25_MATLAB
		ret = vendor_ais_get_net_input_info(&input_info, max_model_mem);
		if (ret != 0)  {
			//DBG_ERR("vendor_ais_get_net_input_info fail!\r\n");
			if (cfp == NULL) {
				cfp = fopen(error_file, "w+");
				if (cfp == NULL) {
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("error out file: %s  null\r\n", error_file);
					}
				} else {
					p_scmt_parm->result_flag = 1;
					fprintf(cfp, "%s NULL NULL 1\r\n", mname);
				}
			} else {
				p_scmt_parm->result_flag = 1;
				fprintf(cfp, "%s NULL NULL 1\r\n", mname);
			}
			goto gen_init_fail;
		}

#endif
		blob_num=0;			
		INT16 retm=0;
		for(UINT16 i_m=0;i_m<AI_SAMPLE_MAX_INPUT_NUM;i_m++) {
			retm=vendor_ais_net_get_input_blob_info(max_model_mem, i_m, &io_num_m, &p_input_blob_info_m);
			if( retm!= HD_OK) {
				break;
			} else {
				blob_num = blob_num + 1;				
			}
		}
		if (blob_num != 1 && blob_num != 2){
			continue;
		}
		vendor_ais_get_input_layer_mem(max_model_mem, &in_layer_mem_m, 0);			
		bit_num= (INT16)in_layer_mem_m.fmt.int_bits+(INT16)in_layer_mem_m.fmt.frac_bits+(INT16)in_layer_mem_m.fmt.sign_bits;
		vendor_ais_pars_engid(0, net_id);

		for (kdx = 0; kdx < p_scmt_parm->imagelist_num; kdx++) {
			if ((fpp = fopen(imglist_path[kdx], "r")) == NULL) {
				if (cfp == NULL) {
					cfp = fopen(error_file, "w+");
					if (cfp == NULL) {
						p_scmt_parm->result_flag = 1;
						if (debugm == 1) {
							DBG_ERR("error out file: %s  null\r\n", error_file);
						}
					} else {
						p_scmt_parm->result_flag = 1;
						fprintf(cfp, "%s %s NULL 2!\r\n", mname,  imglist_path[kdx]);
					}
				} else {
					p_scmt_parm->result_flag = 1;
					fprintf(cfp, "%s %s NULL 2!\r\n", mname,  imglist_path[kdx]);
				}
				continue;
			}

			while (1)
			{
				if (feof(fpp)){
					break;
				}

				if (fgets(line, 512 - 1, fpp) != NULL) {
					if (line[strlen(line) - 1] == '\n') {
						line[strlen(line) - 1] = '\0';
					}

					if (blob_num == 1) {
						if (find_semicolon(line)) {
							continue;
						}

						char img_info[10][256];
						char cc = ' ';
						spilt_fun(line, cc, img_info);
						AI_STRCPY(name, img_info[0],sizeof(name));
						p_src_img[0].width = atoi(img_info[1]);
						p_src_img[0].height = atoi(img_info[2]);
						p_src_img[0].channel = atoi(img_info[3]);
						p_src_img[0].fmt_type = atoi(img_info[4]);
						AI_STRCPY(fmt, img_info[5],sizeof(fmt));
						AI_STRCPY(pattern_name, name,sizeof(pattern_name));
						if (p_src_img[0].width <= 0 || p_src_img[0].width > MAX_FRAME_WIDTH) {
							continue;
						}
						if (p_src_img[0].height <= 0 || p_src_img[0].height > MAX_FRAME_HEIGHT) {
							continue;
						}
						if (p_src_img[0].channel <= 0 || p_src_img[0].channel > MAX_FRAME_CHANNEL) {
							continue;
						}
						if (p_src_img[0].fmt_type <= 0 || p_src_img[0].fmt_type > 32 || p_src_img[0].fmt_type % 8 != 0) {
							continue;
						}

						if (strcmp(fmt, "HD_VIDEO_PXLFMT_YUV420") == 0) {
							p_src_img[0].fmt = 0x520c0420;
						} else if (strcmp(fmt, "HD_VIDEO_PXLFMT_Y8") == 0) {
							p_src_img[0].fmt = 0x51080400;
						} else if (strcmp(fmt, "HD_VIDEO_PXLFMT_RGB888_PLANAR") == 0) {
							p_src_img[0].fmt = 0x23180888;
						} else {
							continue;
						}

						if ((p_src_img[0].channel != input_info->in_channel) || (p_src_img[0].height < input_info->model_height) || (p_src_img[0].width < input_info->model_width) || p_src_img[0].fmt_type != bit_num) {
							continue;
						}
						sprintf(imgpath, "%s/test_images/%s", path_files, name);
						file_size = scmt_load_file(imgpath, src_img[0].va, src_img[0].size);
						if (file_size <= 0) {
							printf("file_size is %d\r\n", file_size);
							if (cfp == NULL) {
								cfp = fopen(error_file, "w+");
								if (cfp == NULL) {
									p_scmt_parm->result_flag = 1;
									if (debugm == 1) {
										DBG_ERR("error out file: %s  null\r\n", error_file);
									}
								} else {
									p_scmt_parm->result_flag = 1;
									fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);
									if (debugm == 1) {
										fprintf(cfp, "file size:%d\r\n", file_size);
									}
								}
							} else {
								p_scmt_parm->result_flag = 1;
								fprintf(cfp, "%s %s NULL 2!\r\n", mname,  name);
								if (debugm == 1) {
									fprintf(cfp, "file size:%d\r\n", file_size);
								}
							}
							continue;
						}

						p_src_img[0].va = src_img[0].va;
						p_src_img[0].pa = src_img[0].pa;
						p_src_img[0].line_ofs = p_src_img[0].width*p_src_img[0].fmt_type / 8;
#if AI_SAMPLE_TEST_BATCH
						pb_src_img = &p_src_img[0];

						tmp_src_va = pb_src_img->va;
						tmp_src_pa = pb_src_img->pa;
						src_img_size = pb_src_img->line_ofs * pb_src_img->height * pb_src_img->channel;
						input_proc_num = vendor_ais_net_get_input_layer_index(max_model_mem, input_proc_idx);
						if (debugm == 1) {
							DBG_WRN("pb_src_img=====\r\n");
							DBG_WRN("pb_src_img.va:%ld\r\n", pb_src_img->va);
							DBG_WRN("input_proc_num:%u", input_proc_num);
						}
						for (batch_i = 0; batch_i < input_proc_num; batch_i++)
						{
							if (batch_i == 0) {
								hd_common_mem_flush_cache((VOID *)(pb_src_img->va), src_img_size);
							}
							if (batch_i > 0) {
								memcpy((VOID *)pb_src_img->va + src_img_size, (VOID *)pb_src_img->va, src_img_size);
								hd_common_mem_flush_cache((VOID *)(pb_src_img->va + src_img_size), src_img_size);
								pb_src_img->pa += src_img_size;
								pb_src_img->va += src_img_size;
							}
							if (vendor_ais_net_input_layer_init(pb_src_img, batch_i, net_id) != HD_OK) {
								if (cfp == NULL) {
									cfp = fopen(error_file, "w+");
									if (cfp == NULL) {
										p_scmt_parm->result_flag = 1;
										if (debugm == 1) {
											DBG_ERR("error out file: %s  null\r\n", error_file);
										}
									} else {
										p_scmt_parm->result_flag = 1;
										fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);
									}
								} else {
									p_scmt_parm->result_flag = 1;
									fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);
								}
								goto input_init_fail;
							}
						}
						pb_src_img->va = tmp_src_va;
						pb_src_img->pa = tmp_src_pa;
#else
						if (vendor_ais_net_input_init(&p_src_img[0], net_id) != HD_OK) {

							if (cfp == NULL) {
								cfp = fopen(error_file, "w+");
								if (cfp == NULL) {
									p_scmt_parm->result_flag = 1;
									if (debugm == 1) {
										DBG_ERR("error out file: %s  null\r\n", error_file);
									}
								} else {
									p_scmt_parm->result_flag = 1;
									fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);
								}
							} else {
								p_scmt_parm->result_flag = 1;
								fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);
							}
							goto input_init_fail;
						}
#endif

					}


					if (blob_num == 2) {
						if (!find_semicolon(line)) {
							continue;
						}
						char double_modelname[3][256];
						char c = ';';
						spilt_fun(line, c, double_modelname);

						AI_STRCPY(blob_model_name, double_modelname[0],sizeof(blob_model_name));
						AI_STRCPY(pattern_name, double_modelname[0],sizeof(pattern_name));

						if (strcmp(blob_model_name, mname) == 0) {
							char img_lsiit1[10][256];
							char img_lsiit2[10][256];
							c = ' ';
							spilt_fun(double_modelname[1], c, img_lsiit1);
							spilt_fun(double_modelname[2], c, img_lsiit2);


							p_src_img[0].width = atoi(img_lsiit1[1]);
							p_src_img[0].height = atoi(img_lsiit1[2]);
							p_src_img[0].channel = atoi(img_lsiit1[3]);
							p_src_img[0].batch_num = atoi(img_lsiit1[4]);
							p_src_img[0].line_ofs = atoi(img_lsiit1[5]);
							p_src_img[0].channel_ofs = atoi(img_lsiit1[6]);
							p_src_img[0].batch_ofs = atoi(img_lsiit1[7]);
							if (strcmp(img_lsiit1[8], "0x520c0420") == 0) {
								p_src_img[0].fmt = 0x520c0420;
							} else if (strcmp(img_lsiit1[8], "0x51080400") == 0) {
								p_src_img[0].fmt = 0x51080400;
							} else if (strcmp(img_lsiit1[8], "0x23180888") == 0) {
								p_src_img[0].fmt = 0x23180888;
							} else {
								continue;
							}

							p_src_img[1].width = atoi(img_lsiit1[1]);
							p_src_img[1].height = atoi(img_lsiit2[2]);
							p_src_img[1].channel = atoi(img_lsiit2[3]);
							p_src_img[1].batch_num = atoi(img_lsiit2[4]);
							p_src_img[1].line_ofs = atoi(img_lsiit2[5]);
							p_src_img[1].channel_ofs = atoi(img_lsiit2[6]);
							p_src_img[1].batch_ofs = atoi(img_lsiit2[7]);
							if (strcmp(img_lsiit2[8], "0x520c0420") == 0) {
								p_src_img[1].fmt = 0x520c0420;
							} else if (strcmp(img_lsiit2[8], "0x51080400") == 0) {
								p_src_img[1].fmt = 0x51080400;
							} else if (strcmp(img_lsiit2[8], "0x23180888") == 0) {
								p_src_img[1].fmt = 0x23180888;
							} else {
								continue;
							}

							sprintf(imgpath, "%s/test_images/%s", path_files, img_lsiit1[0]);
							file_size = scmt_load_file(imgpath, src_img[0].va, src_img[0].size);

							sprintf(imgpath, "%s/test_images/%s", path_files, img_lsiit2[0]);
							file_size = scmt_load_file(imgpath, src_img[1].va, src_img[1].size);
							p_src_img[0].va = src_img[0].va;
							p_src_img[0].pa = src_img[0].pa;
							p_src_img[1].va = src_img[1].va;
							p_src_img[1].pa = src_img[1].pa;

							UINT32 j = 0;
							UINT32 io_num = 0;
							UINT32* p_input_blob_info = NULL;

							for (UINT i = 0; i < 2; i++) {
								if (vendor_ais_net_get_input_blob_info(max_model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
									goto gen_init_fail;
								}
								for (j = 0; j < io_num; j++) {
									UINT32 proc_idx = p_input_blob_info[j] >> 8;
									UINT32 tmp_va = p_src_img[i].va;
									UINT32 tmp_pa = p_src_img[i].pa;
									p_src_img[i].va += (j*p_src_img[i].batch_ofs);
									p_src_img[i].pa += (j*p_src_img[i].batch_ofs);
                                    hd_common_mem_flush_cache((VOID *)(p_src_img[i].va), p_src_img[i].batch_ofs);
									if (vendor_ais_net_input_layer_init(&p_src_img[i], proc_idx, net_id) != HD_OK) {
										goto gen_init_fail;
									}
									p_src_img[i].va = tmp_va;
									p_src_img[i].pa = tmp_pa;
								}
							}
						} else {
							continue;
						}
					}
				} else {
					continue;
				}


				////////
				///////

				counti = counti + 1;
				if (debugm == 1) {
					DBG_WRN("pthread %d: iter:%s  \r\n", (INT)net_id, mname);
				}
#if RESULT_COMPARE
				AI_STRCPY(cname, name,sizeof(cname));
				imgsplit_name = strtok(name, ".");
				sprintf(img_save_path, "%s/gt_results/%s/%s/outfeature.bin", path_files, mname, imgsplit_name);
				gfp = fopen(img_save_path, "rb");
				if (gfp == NULL) {
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("pthread:%u %s not found!\r\n", net_id, img_save_path);
					}
					goto input_init_fail;
				}
				//fclose(gfp);
				//gfp = NULL;
				if(gfp){
					fclose(gfp);
					gfp = NULL ;
				}

				file_size = scmt_load_file(img_save_path, net_result.va, net_result.size);
				if (file_size <= 0) {
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("pthread:%u %s NULL!\r\n", net_id, img_save_path);
						DBG_ERR("file size:%d \r\n", file_size);
					}
					goto input_init_fail;
				}

#endif

				for (jdx = 0; jdx < iterations; jdx++) {
					memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
					hd_common_mem_flush_cache((VOID *)mem_manager.user_buff.va, mem_manager.user_buff.size);

					if (debugm == 1) {
						DBG_WRN("pthread %d: iter:%u  \r\n", (INT)net_id, jdx);
					}


#if USE_NEON

#endif


					net_parm_cpu_loading.max_model_mem = max_model_mem;
					net_parm_cpu_loading.rslt_new_mem = rslt_new_mem;
					net_parm_cpu_loading.p_src_img = &p_src_img[0];
					net_parm_cpu_loading.net_id = net_id;
					
					ret = pthread_create(&nn_thread_id_cpu_loading, NULL, nn_thread_api_cpu_loading, (VOID*)(&net_parm_cpu_loading));
					//is_net_proc = TRUE;
					sleep(10);	

					double cpu_usr = 0.0;		
					double cpu_sys = 0.0;		
					cal_linux_cpu_loading(5, &cpu_usr, &cpu_sys);
					//printf("1.cpu_user=%f\r\n", cpu_usr);					
					//printf("2.cpu_sys=%f\r\n", cpu_sys);
					is_net_proc = FALSE;
					pthread_join(nn_thread_id_cpu_loading, NULL);	
					is_net_proc = TRUE;
#if USE_NEON
	
					
#else
					vendor_ais_proc_net(max_model_mem, rslt_new_mem, net_id);
					vendor_ais_proc_net(max_model_mem, rslt_new_mem, net_id);
					vendor_ais_proc_net(max_model_mem, rslt_new_mem, net_id);
					vendor_ais_proc_net(max_model_mem, rslt_new_mem, net_id);
					vendor_ais_proc_net(max_model_mem, rslt_new_mem, net_id);

#endif			
				

					sprintf(msg, "echo -n \"%s \" >> /mnt/sd/AI_auto_test_files/output/ai_test_7/result_cpuLoading_%ld.txt", mname, run_id);
					system(msg);
					sprintf(msg, "echo \"%s %f\" >> /mnt/sd/AI_auto_test_files/output/ai_test_7/result_cpuLoading_%ld.txt",
						pattern_name,cpu_sys + cpu_usr,run_id);					
					system(msg);
					//sum_time = 0;
                    

				}
			input_init_fail:
#if AI_SAMPLE_TEST_BATCH
				if (blob_num == 1) {
					for (batch_i = 0; batch_i < input_proc_num; batch_i++){
						ret = vendor_ais_net_input_layer_uninit(batch_i, net_id);
						if (ret != HD_OK) {
							p_scmt_parm->result_flag = 1;
							if (debugm == 1) {
								DBG_ERR("pethread %u: %s %s vendor_ais_net_input_uninit fail: %d\r\n", net_id, mname, name, ret);
							}
							goto gen_init_fail;
						}

					}
				} else if (blob_num == 2) {
					UINT32 j = 0;
					UINT32 io_num = 0;
					UINT32* p_input_blob_info = NULL;

					for (UINT32 i = 0; i < 2; i++) {
						if (vendor_ais_net_get_input_blob_info(max_model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
							goto gen_init_fail;
						}
						for (j = 0; j < io_num; j++) {
							UINT32 proc_idx = p_input_blob_info[j] >> 8;

							ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
							if (ret != HD_OK) {
								printf("vendor_ais_net_input_uninit (%d)\n\r", ret);
							}
						}
					}
				}
#else
				ret = vendor_ais_net_input_uninit(net_id);
				if (ret != HD_OK) {
					//printf("vendor_ais_net_input_uninit (%d)\n\r", ret);
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("pethread %u: %s %s vendor_ais_net_input_uninit fail: %d\r\n", net_id, mname, name, ret);
					}
					goto gen_init_fail;
				}
#endif
			}
			if (fpp != NULL)
			{
				fclose(fpp);
				fpp = NULL;
			}
		}

	gen_init_fail:
		ret = vendor_ais_net_gen_uninit(net_id);
		if (ret != HD_OK) {

			p_scmt_parm->result_flag = 1;
			if (debugm == 1) {
				DBG_ERR("pethread:%u:%s vendor_ais_net_gen_uninit fail: %d\r\n", net_id, mname, ret);
			}
			goto texit;
		}
#if AI_V4
#if RESULT_COMPARE

		if (output_layer_info != NULL) {
			free(output_layer_info);
			output_layer_info = NULL;
		}

#endif
		if (input_info != NULL) {
			free(input_info);
			input_info = NULL;
		}
#endif

	}

	if (fp != NULL) {
		fclose(fp);
		fp = NULL;
	}

texit:

#if AI_V4
#if RESULT_COMPARE

	if (output_layer_info != NULL) {
		free(output_layer_info);
		output_layer_info = NULL;
	}

#endif
	if (input_info != NULL) {
		free(input_info);
		input_info = NULL;
	}
#endif

	if (fp != NULL) {
		fclose(fp);
		fp = NULL;
	}
	if (fpp != NULL) {
		fclose(fpp);
		fpp = NULL;
	}

	if (counti == 0) {
		p_scmt_parm->result_flag = 1;
		if (debugm == 1) {
			DBG_ERR("all img can not be used\r\n");
		}
	}
	if (cfp != NULL) {
		fclose(cfp);
		cfp = NULL;
	}

	return 0;

}



// return the max model size
INT32 scmt_find_max_modelsize(CHAR *file, CHAR *path, UINT32 *models_number)
{
	FILE *fp;
	UINT32 model_num = 0;

	char name[50];
	char full_path[256];
	UINT32 maxmodelsize = 0, modelsize = 0;
	HD_RESULT           ret;

	remove_blank(file);
	fp = fopen(file, "r");
	if (fp == NULL) {
		return -1;
	}
	while (1)
	{
		if (feof(fp)) {
			break;
		}
		if (fscanf(fp, "%s\n", name) == EOF) {
			if (feof(fp)) {
				break;
			}
			continue;
		}
#if defined(__LINUX)

		sprintf(full_path, "%smodel_bins/%s/sdk/nvt_model.bin", path, name);
#else
		sprintf(full_path, "%ssmodel_bins\\\\%s\\\\CNN\\\\nvt_model.bin", path, name);
#endif

		ret = vendor_ais_get_model_mem_sz(&modelsize, full_path);
		if (ret != HD_OK) {
			return -1;
		}
		if (modelsize > maxmodelsize) {
			maxmodelsize = modelsize;
		}
		model_num = model_num + 1;
	}
	if(fp){
		fclose(fp);
		fp = NULL ;
	}
	*models_number = model_num;
	return maxmodelsize;
}

INT32 check_file(CHAR *file)
{
	FILE *fp = NULL;
	UINT32 cnt = 0;
	CHAR line[CONFIG_MAX_SIZE * 2];
	fp = fopen(file, "r");
	if (fp == NULL) {
		return -1;
	}
		
	while (1) {
		if (feof(fp)) {
			break;
		}			
		if (fgets(line, CONFIG_MAX_SIZE * 2 - 1, fp) == NULL) {
			if (feof(fp)) {
				break;
			}				
			continue;
		}
		cnt += 1;
	}
	if(fp){
		fclose(fp);
		fp = NULL ;
	}
	if (cnt == 0) {
		return -1;
	}		
	return 0;
}


/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
MAIN(argc, argv)
{
	DBG_DUMP("\r\n");
	DBG_DUMP("ai_test_7_2 start...\r\n");


	UINT32 thread_num;
	char list_path[256] = { 0 }; //model list
	char imglist_path[2][256] = { {0},{0} };
	UINT32 imglist_num = 0;
	VENDOR_AIS_NETWORK_PARM *net_parm = NULL;
	pthread_t *nn_thread_id = NULL;
	SCMT_PARM *scmt_parm = NULL;
	HD_RESULT           ret = 0;
	UINT32 idx = 0, models_number = 0, jdx;
	INT32 maxmodelsize = 0;
	UINT32 para_cnt = 0;
	CHAR key[CONFIG_MAX_ITEMS][CONFIG_MAX_SIZE];
	CHAR value[CONFIG_MAX_ITEMS][CONFIG_MAX_SIZE];
	CONFIG_PARA config_para;
	CHAR path_files[256] = "/mnt/sd/AI_auto_test_files/";
	CHAR config_path_files[256] = "/mnt/sd/AI_auto_test_files/paras/config_7.txt";
	CHAR stemp[CONFIG_MAX_SIZE];
	CHAR path_files_dir7[256] = "/mnt/sd/AI_auto_test_files/output/ai_test_7";

	UINT32 max_src_size = MAX_FRAME_WIDTH*MAX_FRAME_HEIGHT*MAX_FRAME_CHANNEL*AI_SAMPLE_MAX_BATCH_NUM;
	UINT32 thread_mem;
	CHAR error_file[CONFIG_MAX_SIZE];
	UINT32 error_flag_main = 0, debugm = 0;
	INT32 check_flag;

	sprintf(error_file, "%s/output/ai_test_7/error.txt", path_files);
	CHAR command_test[50];

	sprintf(command_test, "mkdir %s/output/ai_test_7", path_files);
	if (access(path_files_dir7,0)){
		system(command_test);
	}


	if (argc != 1) {
		error_flag_main = 1;
		goto eexit;
	}

	para_cnt = load_config_file(config_path_files, key, value, CONFIG_MAX_ITEMS);

	if (para_cnt <= 0)
	{
		error_flag_main = 1;
		goto eexit;

	}
	memset(config_para.model_list_1, '\0', CONFIG_MAX_SIZE);
	memset(config_para.model_list_2, '\0', CONFIG_MAX_SIZE);
	memset(config_para.image_list_1, '\0', CONFIG_MAX_SIZE);
	memset(config_para.image_list_2, '\0', CONFIG_MAX_SIZE);
	memset(stemp, '\0', CONFIG_MAX_SIZE);
	config_para.model_iterations = 0;
	config_para.thread_numbers = 0;
	flow_config_parse(key, value, para_cnt, &config_para);
	debugm = config_para.debug_mode;


	if ((strcmp(config_para.model_list_1, stemp) == 0)) {
		if (debugm == 1) {
			DBG_ERR("model_list_1 is NULL!\r\n");
		}
		error_flag_main = 1;
		goto eexit;
	}

	if (strcmp(config_para.image_list_1, stemp) != 0) {

		AI_STRCPY(imglist_path[imglist_num], config_para.image_list_1,sizeof(imglist_path[imglist_num]));
		check_flag = check_file(config_para.image_list_1);
		if (check_flag < 0) {			
			error_flag_main = 1;
			goto eexit;
		}

		imglist_num++;

	}

	if (strcmp(config_para.image_list_2, stemp) != 0) {

		AI_STRCPY(imglist_path[imglist_num], config_para.image_list_2,sizeof(imglist_path[imglist_num]));
		check_flag = check_file(config_para.image_list_2);

		if (check_flag < 0) {
			if (debugm == 1) {
				DBG_ERR("%s null!\r\n", config_para.image_list_2);
			}
			error_flag_main = 1;
			goto eexit;
		}
		imglist_num++;
	}

	if (imglist_num == 0) {
		if (debugm == 1) {
			DBG_ERR("imglist not find!\r\n");
		}
		error_flag_main = 1;
		goto eexit;

	}
	thread_num = config_para.thread_numbers;

	if (thread_num == 0) {
		if (debugm == 1) {
			DBG_ERR("wrong thread number %u \r\n", thread_num);
		}
		error_flag_main = 1;
		goto eexit;

	}

	//ai_sdk_version = vendor_ais_chk_sdk_version();
	//printf("ai_sdk_version: %s\n", ai_sdk_version);


	ret = hd_common_init(0);
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_common_init fail=%d\n", ret);
		}
		error_flag_main = 1;
		goto exit;

	}

	//set project config for AI
	hd_common_sysconfig(0, (1 << 16), 0, VENDOR_AI_CFG); //enable AI engine
	//vendor_ai_global_init();
	ret = mem_init();

	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("mem_init fail=%d\n", ret);
		}
		error_flag_main = 1;
		goto exit;
	}

	ret = get_mem_block();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("get_mem_block fail=%d\n", ret);
		}
		error_flag_main = 1;
		goto exit;
	}

	ret = hd_videoproc_init();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_videoproc_init fail=%d\n", ret);
		}
		error_flag_main = 1;
		goto exit;

	}

	ret = hd_gfx_init();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_gfx_init fail=%d\n", ret);
		}
		error_flag_main = 1;
		goto exit;

	}
	pthread_mutex_init(&lock, NULL);

	net_parm = (VENDOR_AIS_NETWORK_PARM *)calloc(thread_num, sizeof(VENDOR_AIS_NETWORK_PARM));
	nn_thread_id = (pthread_t *)calloc(thread_num, sizeof(pthread_t));
	scmt_parm = (SCMT_PARM *)calloc(thread_num, sizeof(SCMT_PARM));
	VENDOR_AIS_FLOW_MEM_PARM local_mem = g_mem;


	check_flag = check_file(config_para.model_list_1);
	if (check_flag < 0) {
		if (debugm == 1) {
			DBG_ERR("%s null!\r\n", config_para.model_list_1);
		}
		error_flag_main = 1;
		goto exit;
	}


	maxmodelsize = scmt_find_max_modelsize(config_para.model_list_1, path_files, &models_number);

	if (maxmodelsize <= 0) {
		if (debugm == 1) {
			DBG_ERR("find_max_modelsize error %s\r\n", config_para.model_list_1);
		}
		error_flag_main = 1;
		goto exit;
	}
	AI_STRCPY(list_path, config_para.model_list_1,sizeof(list_path));


	if (debugm == 1) {
		DBG_WRN("maxmodelsize:%ld\r\n", maxmodelsize);
	}

	for (idx = 0; idx < thread_num; idx++) {

		thread_mem = maxmodelsize + max_src_size + EXTRA_NET_SIZE + NET_RESULT_MAX_SIZE * 3;
		if (debugm == 1) {
			DBG_WRN("thread_mem:%ld\r\n", thread_mem);
		}

		net_parm[idx].mem = scmt_getmem_v1(&local_mem, thread_mem, error_file, &error_flag_main, debugm);
		if (error_flag_main > 0) {
			goto exit;
		}			

		if (debugm == 1) {
			DBG_WRN("local_mem:%ld\r\n", local_mem.size);
		}
		net_parm[idx].max_model_size = maxmodelsize;
		net_parm[idx].run_id = idx;

		scmt_parm[idx].model_parm = net_parm[idx];
		scmt_parm[idx].model_list_path = list_path;
		scmt_parm[idx].models_number = models_number;		
		scmt_parm[idx].debug_mode = config_para.debug_mode;
		for (jdx = 0; jdx < imglist_num; jdx++) {
			AI_STRCPY(scmt_parm[idx].img_list_path[jdx], imglist_path[jdx],sizeof(scmt_parm[idx].img_list_path[jdx]));
		}

		scmt_parm[idx].imagelist_num = imglist_num;
		scmt_parm[idx].model_iterations = config_para.model_iterations;
		scmt_parm[idx].path_files = path_files;
		scmt_parm[idx].result_flag = 0;
		ret = pthread_create(&nn_thread_id[idx], NULL, scmt_thread_api, (VOID*)(&scmt_parm[idx]));

		if (ret < 0) {
			if (debugm == 1) {
				DBG_ERR("create  thread failed =%d \r\n", ret);
			}
			error_flag_main = 1;
			goto exit;
		}

	}
	for (idx = 0; idx < thread_num; idx++) {
		pthread_join(nn_thread_id[idx], NULL);
	}


exit:
	for (idx = 0; idx < thread_num; idx++) {
		error_flag_main += scmt_parm[idx].result_flag;
	}
	pthread_mutex_destroy(&lock);
	ret = hd_gfx_uninit();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_gfx_uninit fail=%d \r\n", ret);
		}
		error_flag_main = 1;
		goto eexit;

	}

	ret = hd_videoproc_uninit();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_videoproc_uninit fail=%d \r\n", ret);
		}
		error_flag_main = 1;
		goto eexit;

	}
	ret = release_mem_block();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("release_mem_block fail=%d \r\n", ret);
		}
		error_flag_main = 1;
		goto eexit;

	}

	ret = mem_uninit();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("mem_uninit fail=%d \r\n", ret);
		}
		error_flag_main = 1;
		goto eexit;

	}
	// global uninit for ai sdk
	//vendor_ai_global_uninit();

	ret = hd_common_uninit();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_common_uninit fail=%d \r\n", ret);
		}
		error_flag_main = 1;
		goto eexit;

	}


eexit:
	if(nn_thread_id != NULL){
		free(nn_thread_id);
		nn_thread_id = NULL;
	}
	if(net_parm != NULL){
		free(net_parm);
		net_parm = NULL;
	}
	if(scmt_parm != NULL){
		free(scmt_parm);
		scmt_parm = NULL;
	}

	if (error_flag_main == 0) {
		DBG_DUMP("success\r\n");
	} else {
		DBG_ERR("failed\r\n");
	}
	DBG_DUMP("ai_test_7_2 finished...\r\n");
	return ret;
}

