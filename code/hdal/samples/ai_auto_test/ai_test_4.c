/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
////#include <pthread.h>
#include "hdal.h"
//#include "hd_type.h"
#include "hd_common.h"
//#include "nvtipc.h"
//#include "vendor_dsp_util.h"
//#include "vendor_ai/vendor_ai.h"

#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
//#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
//#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
//#include "net_flow_user_sample/net_layer_sample.h"
//#include "custnn/custnn_lib.h"
// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(alg_net_app_user_sample, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME



#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080
#define MAX_FRAME_CHANNEL       3
#define MAX_FRAME_BYTE          4
#define KBYTE                   1024

#define MAX_RESULT_SIZE             (200 * KBYTE)
#define AI_SAMPLE_TEST_BATCH    1 // v2 not support
#if AI_SAMPLE_TEST_BATCH
#define AI_SAMPLE_MAX_BATCH_NUM 8
#define AI_SAMPLE_MAX_INPUT_NUM 2
#else
#define AI_SAMPLE_MAX_BATCH_NUM 1
#define AI_SAMPLE_MAX_INPUT_NUM 1
#endif
#define YUV_SINGLE_BUF_SIZE     (3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT * AI_SAMPLE_MAX_BATCH_NUM)
#define YUV_OUT_BUF_SIZE        (YUV_SINGLE_BUF_SIZE * AI_SAMPLE_MAX_INPUT_NUM)
#define IMG_SIZE                (YUV_SINGLE_BUF_SIZE * AI_SAMPLE_MAX_INPUT_NUM*2)

#define MBYTE					(1024 * 1024)
#define NN_TOTAL_MEM_SIZE       (500 * MBYTE)
#define NN_RUN_NET_NUM			2
#define NN_RUN_THREAD_NUM	    2




#define MAX_OBJ_NUM             1024
#define AUTO_UPDATE_DIM     	1
#define DYNAMIC_MEM             1
#define MODIFY_ENG_IN_SAMPLE    1

#define VENDOR_AI_CFG  0x000f0000  //ai project config

#define GET_OUTPUT_LAYER_FEATURE_MAP DISABLE
#define SUPPORT_MULTI_BLOB_IN_SAMPLE      1
#define EXTRA_NET_SIZE           (200 * KBYTE)
#define M_NET_MAX_BUF_SIZE       (25000 * KBYTE)
#define NN_USE_DRAM2             ENABLE
#define CONFIG_MAX_SIZE          (256)
#define CONFIG_MAX_ITEMS         (10)
#define IMG_MAX_ITEMS            (10)
#define RESULT_COMPARE           ENABLE
#define NET_RESULT_MAX_SIZE      (32 * MBYTE)
#define CORE_MAX_NUM             2
#define AI_STRCPY(dst, src, dst_size) do { \
strncpy(dst, src, (dst_size)-1); \
dst[(dst_size)-1] = '\0'; \
} while(0)

/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/**
	Parameters of network
*/
typedef struct _SCMT_NETWORK_PARM {
    UINT32 net_id;
    VENDOR_AIS_FLOW_MAP_MEM_PARM net_manager;
} SCMT_NETWORK_PARM;

typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	VENDOR_AIS_IMG_PARM	src_img[AI_SAMPLE_MAX_INPUT_NUM];
	VENDOR_AIS_FLOW_MEM_PARM mem;
	VENDOR_AIS_FLOW_MEM_PARM mem_img;
	UINT32 run_id;
    UINT32 max_model_size;
	UINT32 max_diffmodel_size;
} VENDOR_AIS_NETWORK_PARM;

typedef struct _SCMT_PARM{
    VENDOR_AIS_NETWORK_PARM model_parm;
    char model_list_path[CONFIG_MAX_SIZE];
	char img_list_path[CONFIG_MAX_SIZE];
    char path_files[50];
	UINT32 models_number;
	UINT32 imagelist_num;
	UINT32 model_iterations;
    UINT32 core[NN_RUN_NET_NUM];
    UINT32 core_num;
    INT32  auto_updata_dim_flag;
    UINT32 result_flag;
    UINT32 debug_mode;
}SCMT_PARM;

typedef struct _SCMT_THREAD_INIT_PARM{
    UINT32 net_id;
} SCMT_THREAD_INIT_PARM;

typedef struct _SCMT_FLOW_PARM {
    BOOL    net_lock;
    INT32   init_state;
} SCMT_FLOW_PARM;


typedef enum
{
	SCMT_IMG_MEM_STRUCT=0,
	SCMT_RESULT_MEM_STRUCT,
	SCMT_NETWORK_PARM_STRUCT,
	ENUM_DUMMY4WORD(SCMT_STRUCT_TYPE)
} SCMT_STRUCT_TYPE;

typedef struct _SCMT_IMG_MEM{
    VENDOR_AIS_FLOW_MEM_PARM src;
} SCMT_IMG_MEM;
typedef struct _SCMT_RESULT_MEM{
    VENDOR_AIS_FLOW_MEM_PARM result;
} SCMT_RESULT_MEM;
typedef struct _CONFIG_PARA{
    char model_list_1[CONFIG_MAX_SIZE];
    char image_list_1[CONFIG_MAX_SIZE];
    UINT32 core[NN_RUN_NET_NUM];
    UINT32 model_iterations;
    UINT32 thread_numbers;
    UINT32 core_num;
    INT32  auto_updata_dim_flag;
    UINT32 debug_mode;
}CONFIG_PARA;

/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
#if DYNAMIC_MEM

static UINT32 mem_size[NN_RUN_NET_NUM]		= {0,0};
//static UINT32 max_src_size=0;
#endif
static VENDOR_AIS_FLOW_MEM_PARM g_mem		= {0};
//static VENDOR_AIS_FLOW_MEM_PARM g_mem_img		= {0};


static HD_COMMON_MEM_VB_BLK g_blk_info[2];
static pthread_mutex_t     lock;



//NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/


static int mem_init(void)
{
	HD_RESULT              ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};
    UINT32 total_mem_size = 0;


#if DYNAMIC_MEM
    UINT32 i;
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif

    mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = total_mem_size;
	mem_cfg.pool_info[0].blk_cnt = 1;

#if NN_USE_DRAM2
    mem_cfg.pool_info[0].ddr_id = DDR_ID1;
#else
    mem_cfg.pool_info[0].ddr_id = DDR_ID0;
#endif


	// mem_cfg.pool_info[1].type = HD_COMMON_MEM_CNN_POOL;
	// mem_cfg.pool_info[1].blk_size = max_src_size*NN_RUN_NET_NUM;
	// mem_cfg.pool_info[1].blk_cnt = 1;
    // mem_cfg.pool_info[1].ddr_id = DDR_ID0;
	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		//printf("hd_common_mem_init err: %d\r\n", ret);
	}
	return ret;
}


static INT32 get_mem_block(VOID)
{
	HD_RESULT                 ret = HD_OK;
	UINT32                    pa, va;
	HD_COMMON_MEM_VB_BLK      blk;
    UINT32 total_mem_size = 0;


#if NN_USE_DRAM2
    HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID1;
#else
    HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID0;
#endif

#if DYNAMIC_MEM
    UINT32 i;
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif



    /* Allocate parameter buffer */
	if (g_mem.va != 0) {
		//DBG_DUMP("err: mem has already been inited\r\n");
		return -1;
	}
    blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, total_mem_size, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		//DBG_DUMP("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto mexit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		//DBG_DUMP("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, total_mem_size);
	g_blk_info[0] = blk;

	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_mem_size);
		if (ret != HD_OK) {
			//DBG_DUMP("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}
	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = total_mem_size;



    // if(g_mem_img.va!=0)
    // {
        // return -1;
    // }
    // blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, max_src_size*NN_RUN_NET_NUM, DDR_ID0);
	// if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		// //DBG_DUMP("hd_common_mem_get_block fail\r\n");
		// ret =  HD_ERR_NG;
		// goto mexit;
	// }
    // pa = hd_common_mem_blk2pa(blk);
	// if (pa == 0) {
		// //DBG_DUMP("not get buffer, pa=%08x\r\n", (int)pa);
		// return -1;
	// }
    // va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa,  max_src_size*NN_RUN_NET_NUM);
	// g_blk_info[1] = blk;
    // if (va == 0) {
		// ret = hd_common_mem_munmap((void *)va, max_src_size*NN_RUN_NET_NUM);
		// if (ret != HD_OK) {
			// //DBG_DUMP("mem unmap fail\r\n");
			// return ret;
		// }
		// return -1;
	// }
    // g_mem_img.pa = pa;
	// g_mem_img.va = va;
	// g_mem_img.size = max_src_size*NN_RUN_NET_NUM;

mexit:
	return ret;
}


static HD_RESULT release_mem_block(VOID)
{
	HD_RESULT ret = HD_OK;
    UINT32 total_mem_size = 0;

#if DYNAMIC_MEM
    UINT32 i;
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif


	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, total_mem_size);
		if (ret != HD_OK) {
			//DBG_DUMP("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		//DBG_DUMP("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}


    // if (g_mem_img.va) {
		// ret = hd_common_mem_munmap((void *)g_mem_img.va, IMG_SIZE);
		// if (ret != HD_OK) {
			// //DBG_DUMP("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			// return ret;
		// }
	// }
	// //ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	// ret = hd_common_mem_release_block(g_blk_info[1]);
	// if (ret != HD_OK) {
		// //DBG_DUMP("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		// return ret;
	// }


	return ret;
}

static HD_RESULT mem_uninit(void)
{
	return hd_common_mem_uninit();
}



VENDOR_AIS_FLOW_MEM_PARM scmt_getmem_v1(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size,CHAR *file,UINT32 *flag,UINT32 debugm)
{
	VENDOR_AIS_FLOW_MEM_PARM mem = {0};
    //FILE *fp=NULL;
    required_size = ALIGN_CEIL_32(required_size);
	if(required_size <= valid_mem->size) {
		mem.va = valid_mem->va;
        mem.pa = valid_mem->pa;
		mem.size = required_size;

		valid_mem->va += required_size;
        valid_mem->pa += required_size;
		valid_mem->size -= required_size;
	} else {
	    if(debugm==1){
			DBG_ERR("Required size %ld > total memory size %ld\r\n", required_size, valid_mem->size);
	    }
        *flag=1;
	}
	return mem;
}


VENDOR_AIS_FLOW_MEM_PARM scmt_getmem_v2(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size,UINT32 run_id,CHAR *file,UINT32 *flag,UINT32 debugm)
{
	VENDOR_AIS_FLOW_MEM_PARM mem = {0};
    //FILE *fp=NULL;
    required_size = ALIGN_CEIL_32(required_size);
	if(required_size <= valid_mem->size) {
		mem.va = valid_mem->va;
        mem.pa = valid_mem->pa;
		mem.size = required_size;

		valid_mem->va += required_size;
        valid_mem->pa += required_size;
		valid_mem->size -= required_size;
	} else {
	    if(debugm==1){
			DBG_ERR(" Required size %ld > total memory size %ld\r\n", required_size, valid_mem->size);
	    }
       *flag=1;
	}
	return mem;
}





static INT32 split_kvalue(CHAR *p_str, char *key,char *value, CHAR c)
{
    //CHAR *p_key = p_str;
	CHAR *p_value = NULL;
	CHAR *p_local = p_str;
	UINT32 cnt = 0;

	// check only one '='
	while (*p_local != '\0') {
		p_local++;
		if (*p_local == c)
			cnt++;
	}
	if (cnt != 1) {
		return -1;
	}

	p_local = p_str;
	while (*p_local != c) {
		p_local++;
	}
	p_value = p_local + 1;
	*p_local = '\0';
	strcpy(key, p_str);
	strcpy(value, p_value);
	return 0;
}
static INT32 remove_blank(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	CHAR *p_str1 = p_str;

	while (*p_str0 != '\0') {
		if ((*p_str0 != ' ') && (*p_str0 != '\t') && (*p_str0 != '\n') && (*p_str0 != '\r')) {
			*p_str1++ = *p_str0++;
		} else {
			p_str0++;
		}
	}

	*p_str1 = '\0';
	return 0;
}

static INT32 load_config_file(CHAR *file_path,CHAR key[][CONFIG_MAX_SIZE],CHAR value[][CONFIG_MAX_SIZE],UINT max_items)
{
    FILE *fp;
    if ((fp=fopen(file_path,"r")) == NULL) {
        DBG_ERR("fopen file path %s error!\r\n", file_path);
        return 0;
    }

    CHAR line[CONFIG_MAX_SIZE*2];
    UINT32 cnt=0;
    while(1) {
        if (feof(fp))
            break;
        if (fgets(line,CONFIG_MAX_SIZE*2-1,fp) == NULL) {
            if(feof(fp))
                break;
            DBG_ERR("fgets line : %s error!\r\n",line);
            continue;
        }
        remove_blank(line);
        if(line[0] == '#' || line[0] == '\0')
            continue;
        if (split_kvalue(line,key[cnt],value[cnt],'=')) {
            DBG_ERR("pauser line : %s error!\r\n", line);
            continue;
        }
        cnt=cnt+1;
        if(cnt > max_items) {
            DBG_ERR("pauser max_items is too larger %d\r\n",cnt);
        }
    }
    fclose(fp);

   return cnt;
}
static INT32 flow_core_parse(CHAR *value,UINT32 *core,CHAR c,UINT32 *core_num)
{
    UINT32 a=0,i=0;
    INT32 posc[CORE_MAX_NUM];
    CHAR *p_value=value;
    CHAR *p_valuet=value;
    CHAR num_str[2];

    while(*p_value !='\0') {
        if(*p_value == c) {
            a++;
            *p_value='\0';
            posc[a]=i;
        }
        i++;
        p_value++;
    }
    if(i==0)
        return -1;
    if(a==0) {
       *core_num=1;
        core[0]=atoi(p_valuet);
    }

    strcpy(num_str,p_valuet);
    core[0]=atoi(num_str);
    for(i=1;i<a+1;i++) {
          strcpy(num_str,p_valuet+posc[i]+1);
          core[i]=atoi(num_str);
          //printf("core[%d]=%d\r\n",(int)i,(int)(core[i]));
    }
    *core_num=a+1;

    return 0;
}


static INT32 flow_config_parse_v1(CHAR key[][CONFIG_MAX_SIZE],CHAR value[][CONFIG_MAX_SIZE],UINT32 cnt,CONFIG_PARA *cpara)
{
    UINT32 idx;

    for (idx=0;idx<cnt;idx++) {
        if(strcmp(key[idx],"[debug_mode]")==0) {
            cpara->debug_mode=atoi(value[idx]);
        }
        if(strcmp(key[idx],"[path/model_list_1]")==0) {
            strcpy(cpara->model_list_1,value[idx]);
        } else if(strcmp(key[idx],"[path/image_list_1]")==0) {
            strcpy(cpara->image_list_1,value[idx]);
        } else if(strcmp(key[idx],"[thread_numbers]")==0) {
          cpara->thread_numbers=atoi(value[idx]);
          if(cpara->thread_numbers <0)
            return -1;
        } else if(strcmp(key[idx],"[core]")==0) {
            if (flow_core_parse(value[idx],cpara->core,',',&(cpara->core_num)) < 0)
                return -1;
            if(cpara->core_num > CORE_MAX_NUM || cpara->core_num <=0)
                return -1;
        } else if(strcmp(key[idx],"[model_iterations]")==0) {
            cpara->model_iterations=atoi(value[idx]);
        } else if(strcmp(key[idx],"[auto_updata_dim]")==0) {
            cpara->auto_updata_dim_flag=atoi(value[idx]);
        }
    }
    return 0;
}



static INT32 get_imgparas(CHAR *ori_info,VENDOR_AIS_IMG_PARM *src, CHAR modelname[][50],CHAR imgname[][256],UINT32 blob,CHAR c)
{
    CHAR *ori=ori_info;
    CHAR mp[1024],p[512],fmt[256];
    CHAR *fstr;
    INT16 j=0;
    

    if (blob==1) {
        j=strlen(imgname[0]);
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
        fstr=strtok(mp," ");

		AI_STRCPY(p, fstr, sizeof(p));		
        src[0].width=atoi(p);
		
        j = strlen(p)+j+1;
    	AI_STRCPY(mp, ori+j+1, sizeof(mp));
    	fstr = strtok(mp, " ");
    	AI_STRCPY(p, fstr, sizeof(p));
    	src[0].height= atoi(p);
       
        j = strlen(p)+j+1;
    	AI_STRCPY(mp, ori+j+1, sizeof(mp));
    	fstr = strtok(mp, " ");
    	AI_STRCPY(p, fstr, sizeof(p));
    	src[0].channel= atoi(p);
        
        j = strlen(p) + j+1;
    	AI_STRCPY(mp, ori+j+1, sizeof(mp));
    	fstr = strtok(mp, " ");
    	AI_STRCPY(p, fstr, sizeof(p));
    	src[0].fmt_type= atoi(p);
       
        j = strlen(p) + j+1;
    	AI_STRCPY(mp, ori+j+1, sizeof(mp));
    	fstr = strtok(mp, " ");
        AI_STRCPY(fmt, fstr, sizeof(fmt));
        remove_blank(fmt);

        if (strcmp(fmt, "HD_VIDEO_PXLFMT_YUV420")== 0) {
			src[0].fmt = 0x520c0420;
		} else if (strcmp(fmt, "HD_VIDEO_PXLFMT_Y8") == 0) {
			src[0].fmt = 0x51080400;
		} else if (strcmp(fmt, "HD_VIDEO_PXLFMT_RGB888_PLANAR") == 0) {
			src[0].fmt = 0x23180888;
		} else {
			return -1;
		}

    } else {
		
        j=strlen(modelname[0])+strlen(imgname[0])+1;
        AI_STRCPY(mp, ori+j+1, sizeof(mp));
        fstr = strtok(mp, " ");
		AI_STRCPY(p, fstr, sizeof(p));
		src[0].width= atoi(p);
        
        j = strlen(p) + j + 1;;
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
		AI_STRCPY(p, fstr, sizeof(p));
		src[0].height= atoi(p);
       
        j = strlen(p) + j + 1;
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
		AI_STRCPY(p, fstr, sizeof(p));
		src[0].channel= atoi(p);
       
        j = strlen(p) + j + 1;
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");       
		AI_STRCPY(p, fstr, sizeof(p));
		src[0].batch_num= atoi(p);
        
        j = strlen(p) + j + 1;
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
		AI_STRCPY(p, fstr, sizeof(p));
		src[0].line_ofs= atoi(p);
       
        j = strlen(p) + j + 1;
       
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
        
		AI_STRCPY(p, fstr, sizeof(p));
		src[0].channel_ofs= atoi(p);
        //printf("src[0].channel_ofs:%d\r\n",src[0].channel_ofs);
        j = strlen(p) + j + 1;
        
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
        
		AI_STRCPY(p, fstr, sizeof(p));
		src[0].batch_ofs= atoi(p);
        //printf("src[0].batch_ofs:%d\r\n",src[0].batch_ofs);
        j = strlen(p) + j + 1;
        
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
        AI_STRCPY(p, fstr, sizeof(p));

        if (split_kvalue(p,modelname[1],imgname[1],c) == -1)
            return -1;
		AI_STRCPY(fmt, modelname[1], sizeof(fmt));
    
        if (strcmp(fmt, "0x520c0420")== 0) {
			src[0].fmt = 0x520c0420;
		} else if (strcmp(fmt, "0x51080400") == 0) {
			src[0].fmt = 0x51080400;
		} else if (strcmp(fmt, "0x23180888") == 0) {
			src[0].fmt = 0x23180888;
		} else {
			return -1;
		}
        remove_blank(modelname[1]);
        remove_blank(imgname[1]);
        j = j+strlen(modelname[1]) + strlen(imgname[1]) + 1;
       
        AI_STRCPY(mp, ori+j+1, sizeof(mp));
        fstr=strtok(mp,  " ");
        
        AI_STRCPY(p, fstr, sizeof(p));
        src[1].width=atoi(p);
        //printf("src[1].width:%d\r\n",src[1].width);
        j = strlen(p) + j + 1;
        
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
        
		AI_STRCPY(p, fstr, sizeof(p));
        src[1].height=atoi(p);
        //printf("src[1].height:%d\r\n",src[1].height);
        j = strlen(p) + j + 1;
        
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
        
		AI_STRCPY(p, fstr, sizeof(p));
        src[1].channel=atoi(p);
        //printf("src[1].channel:%d\r\n",src[1].channel);
        j = strlen(p) + j + 1;
        
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
        
		AI_STRCPY(p, fstr, sizeof(p));
		src[1].batch_num= atoi(p);
        //printf("src[1].batch_num:%d\r\n",src[1].batch_num);
        j = strlen(p) + j + 1;
        
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
        
		AI_STRCPY(p, fstr, sizeof(p));
		src[1].line_ofs= atoi(p);
        //printf("src[1].line_ofs:%d\r\n",src[1].line_ofs);
        j = strlen(p) + j + 1;
        
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
        
		AI_STRCPY(p, fstr, sizeof(p));
		src[1].channel_ofs= atoi(p);
        //printf("src[1].channel_ofs:%d\r\n",src[1].channel_ofs);
        j = strlen(p) + j + 1;
        
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
        
		AI_STRCPY(p, fstr, sizeof(p));
		src[1].batch_ofs= atoi(p);
        //printf("src[1].batch_ofs:%d\r\n",src[1].batch_ofs);
        j = strlen(p) + j + 1;
        
		AI_STRCPY(mp, ori+j+1, sizeof(mp));
		fstr = strtok(mp, " ");
        AI_STRCPY(fmt, fstr, sizeof(fmt));
        remove_blank(fmt);
		
        if (strcmp(fmt, "0x520c0420")== 0) {
			src[1].fmt = 0x520c0420;
		} else if (strcmp(fmt, "0x51080400") == 0) {
			src[1].fmt = 0x51080400;
		} else if (strcmp(fmt, "0x23180888") == 0) {
			src[1].fmt = 0x23180888;
		} else {

			return -1;
		}
    }
    return 0;
}

#if !RESULT_COMPARE
static UINT32 ai_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;


	fd = fopen(p_filename, "rb");
	if (!fd) {

		return 0;
	}

	fseek ( fd, 0, SEEK_END );
	file_size = ALIGN_CEIL_4( ftell(fd) );
	fseek ( fd, 0, SEEK_SET );

	read_size = fread ((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
		return 0;
	}
	fclose(fd);
	return read_size;
}
#endif


static UINT32 scmt_load_file(CHAR *p_filename, UINT32 va,UINT32 size)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;

	fd = fopen(p_filename, "rb");
	if (fd==NULL) {
		DBG_ERR("cannot read %s\r\n", p_filename);
		return 0;
	}

	fseek ( fd, 0, SEEK_END );
	file_size = ALIGN_CEIL_4( ftell(fd) );
    file_size = ftell(fd);
	fseek ( fd, 0, SEEK_SET );

	read_size = fread ((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size || read_size >  size) {
		//DBG_ERR("size mismatch, real = %d, idea = %d\r\n", (int)read_size, (int)file_size);
		return 0;
	}
	fclose(fd);

	return read_size;
}

#if RESULT_COMPARE
/*
static BOOL vendor_ais_write_file(CHAR *filepath, UINT32 addr, UINT32 size)
{
#if NET_FLOW_USR_SAMPLE_LAYER_OUT
	FILE *fsave = NULL;

	fsave = fopen(filepath, "wb");
	if (fsave == NULL) {
		DBG_ERR("fopen fail\n");
		return FALSE;
	}

	fwrite((UINT8 *)addr, size, 1, fsave);

	fclose(fsave);
#endif
	return TRUE;
}
*/

static INT8 result_compare_v1(UINT32 va,UINT32 allsize,UINT8* data,UINT32 size)
{
    UINT32 idx;
    UINT8 *gt=(UINT8 *)va;
    if(allsize<size) {
		return -1;
	}
    for(idx=0;idx<size;idx++) {
        if(((*(gt++))-(*(data++))) !=0)
            return -1;
    }
    return 0;
}

#endif
UINT32 get_thread_model_size(CHAR *model_name, CHAR *path,UINT32 *tflag) 
{
	UINT32 model_size=0;
	HD_RESULT ret;
	CHAR full_path[100];
	sprintf(full_path,"%s/model_bins/%s/sdk/nvt_model.bin",path,model_name);
	ret=vendor_ais_get_model_mem_sz(&model_size, full_path);
	if (ret != HD_OK || model_size==0) {
		*tflag=1;
		DBG_ERR("vendor_ais_get_model_mem_szfail: %d\r\n", ret);
	}
	return model_size;
}
UINT32 get_thread_diffmodel_size(CHAR *model_name, CHAR *path,UINT32 *tflag)
{
	UINT32 model_size=0;
	CHAR full_path[100];
	HD_RESULT ret;
	sprintf(full_path,"%s/model_bins/%s/sdk/nvt_stripe_model.bin",path,model_name);
	ret=vendor_ais_get_diff_model_mem_sz(&model_size,full_path);
	if (ret != HD_OK || model_size==0) {
		*tflag=1;
		DBG_ERR("vendor_ais_get_diff_model_mem_sz failed %d\r\n", ret);
	}
	return model_size;
}
UINT32 get_thread_resultsize(CHAR *model_path_file,VENDOR_AIS_FLOW_MEM_PARM max_model_mem,UINT32 *flag,UINT32 net_id)
{
	UINT32 result_mem_size = 0;
	HD_RESULT ret;
	NN_DATA output_layer_iomem = {0};
	NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
	UINT32 num_output_layer = 0;
	UINT32 model_file_size,model_size;
	VENDOR_AIS_FLOW_MAP_MEM_PARM nn_flow_mem_manager;
	VENDOR_AIS_FLOW_MEM_PARM model_mem;
	model_mem.va=max_model_mem.va;
	model_mem.pa=max_model_mem.pa;
	model_mem.size=max_model_mem.size;
	model_file_size = vendor_ais_load_file(model_path_file, model_mem.va, &output_layer_info, &num_output_layer);
	if (model_file_size == 0) {
        if (output_layer_info != NULL) {
            free(output_layer_info);
            output_layer_info = NULL;
        }
        DBG_ERR("net load model file fail: %s\r\n", model_path_file);
         *flag=1;
        return 0;

    }
	model_size = vendor_ais_auto_alloc_mem(&model_mem, &nn_flow_mem_manager);
	// printf("model_size:%ld\r\n",model_size);
	// printf("model_mem.size:%ld\r\n",model_mem.size);
	// printf("net_id:%ld\r\n",net_id);
	if (model_size > model_mem.size || model_size == 0) {
		DBG_ERR("vendor_ais_auto_alloc_mem fail\r\n");
		DBG_ERR("model_size %ld\r\n",model_size);
		DBG_ERR("model_mem.size %ld\r\n",model_mem.size);
		*flag=1;
         return 0;
	}
	//memset((VOID *)nn_flow_mem_manager.user_buff.va, 0, nn_flow_mem_manager.user_buff.size);    // clear io buffer
	pthread_mutex_lock(&lock);
	ret = vendor_ais_net_gen_init(nn_flow_mem_manager,net_id);
	pthread_mutex_unlock(&lock);
	if (ret != HD_OK) {
        if (output_layer_info != NULL) {
            free(output_layer_info);
            output_layer_info = NULL;
         }
         DBG_ERR("net gen init fail=%d\r\n", ret);
         vendor_ais_net_gen_uninit(net_id);
         *flag=1;
         return 0;
    }

	for (UINT32 i= 0; i<num_output_layer; i++) {
		ret = vendor_ais_get_layer_mem_v2(model_mem,&output_layer_iomem,&output_layer_info[i]);
		if (ret == HD_OK) {

            if (output_layer_iomem.va > 0 && output_layer_iomem.size > 0) {
                hd_common_mem_flush_cache((VOID *)output_layer_iomem.va, output_layer_iomem.size);
            }
            result_mem_size = result_mem_size + output_layer_iomem.size;
        } else {
            DBG_ERR("vendor_ais_get_layer_mem fail\n");
            vendor_ais_net_gen_uninit(net_id);
            *flag=1;
            return 0;
        }
	}
    if (output_layer_info != NULL) {
		free(output_layer_info);
		output_layer_info = NULL;
	}
	ret=vendor_ais_net_gen_uninit(net_id);
	if (ret != HD_OK) {      
         DBG_ERR("vendor_ais_net_gen_uninit fail=%d\r\n", ret);
         *flag=1;
         return 0;
    }
	
	return result_mem_size;
}
static VOID *scmt_thread_api(VOID *arg)
{
    UINT32 count=0;
    UINT32 counti=0;
	HD_RESULT ret;
    SCMT_PARM *p_scmt_parm=(SCMT_PARM *)arg;
    VENDOR_AIS_NETWORK_PARM *p_net_parm=&p_scmt_parm->model_parm;
    VENDOR_AIS_IMG_PARM *p_src_img=p_net_parm->src_img;
    VENDOR_AIS_FLOW_MEM_PARM src_img;
    CHAR path_files[50];
    CHAR imglist_path[CONFIG_MAX_SIZE];

    //CHAR name[50]={0};
    CHAR mname[50]={0};
    CHAR model_path[CONFIG_MAX_SIZE]={0};

    CHAR imgpath[CONFIG_MAX_SIZE]={0};
    //CHAR fmt[50]={0};
//   blob fmt

    CHAR name[AI_SAMPLE_MAX_INPUT_NUM][256];
    CHAR miname[AI_SAMPLE_MAX_INPUT_NUM][50];
    UINT32 blob=0,img_fmttype=0;
    CHAR imginfo[1024],ori_imginfo[1024],temp_info[512];
	CHAR *pstr;
	CHAR key_c=';';
	UINT32 va_src_temp,pa_src_temp,temp_size;
	UINT32 blob_j;
	UINT32 io_num = 0;
	UINT32 *p_input_blob_info=NULL;
	UINT32 proc_idx=0;
	UINT32 tmp_va,tmp_pa;



// blob fmt

    VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem = &p_net_parm->mem;
	//VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem_img = &p_net_parm->mem_img;

    VENDOR_AIS_FLOW_MEM_PARM max_model_mem;
    VENDOR_AIS_FLOW_MEM_PARM tem_mem ;
    //VENDOR_AIS_FLOW_MEM_PARM tem_mem_img;


    UINT32 debugm=p_scmt_parm->debug_mode,run_id =p_net_parm->run_id;
    UINT32 net_id = run_id;

    UINT32 file_size = 0, model_size = 0;
	UINT32 model_size_temp=0;
	UINT32 result_size_temp=0;
	UINT32 img_size_temp=0;


    UINT32 idx = 0,jdx=0;
	UINT32 mdx=0;
    //UINT32 max_src_size=MAX_FRAME_WIDTH*MAX_FRAME_HEIGHT*MAX_FRAME_CHANNEL*AI_SAMPLE_MAX_BATCH_NUM*AI_SAMPLE_MAX_INPUT_NUM;
    UINT32 core;
    UINT32 tflag=0;


	VENDOR_AIS_FLOW_MEM_PARM rslt_new_mem;

    VENDOR_AIS_FLOW_MAP_MEM_PARM mem_manager;



#if !CNN_25_MATLAB
    NN_IN_OUT_FMT *input_info = NULL;
#endif

    //UINT32 req_size = 0;
    UINT32 iterations,model_num;
    FILE *fpp=NULL;
    FILE *cfp=NULL;
	FILE *fp=NULL;
    CHAR error_file[CONFIG_MAX_SIZE];
    //compare result
#if RESULT_COMPARE
    CHAR img_save_path[CONFIG_MAX_SIZE];
   // CHAR command_test[128];
    //CHAR out_img_path[CONFIG_MAX_SIZE];
    NN_DATA output_layer_iomem = {0};
    NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
	UINT32 num_output_layer = 0,net_out_size=0,out_temp_va=0;
    FILE *gfp=NULL;
    CHAR cname[50]={0};
    char *imgsplit_name;
    VENDOR_AIS_FLOW_MEM_PARM net_result;
    VENDOR_AIS_FLOW_MEM_PARM net_out_result;
	

    //CHAR command_test[CONFIG_MAX_SIZE];
    //CHAR out_img_path[CONFIG_MAX_SIZE];

#endif
#if AI_SAMPLE_TEST_BATCH
	VENDOR_AIS_IMG_PARM *pb_src_img=NULL;
	UINT32 input_proc_idx[AI_SAMPLE_MAX_BATCH_NUM];
	UINT32 tmp_src_va;
	UINT32 tmp_src_pa;
	UINT32 src_img_size ;
	UINT32 batch_i;
	UINT32 input_proc_num = 0;
#endif

#if AUTO_UPDATE_DIM
	VENDOR_AIS_DIFF_MODEL_INFO diff_info = {0};
	VENDOR_AIS_FLOW_MEM_PARM diff_model_mem = {0};
     VENDOR_AIS_FLOW_MEM_PARM ft_out_result={0};
     UINT32 out_temp_gt_va=0,ft_out_size=0;
	//NN_LAYER_OUTPUT_INFO *diff_output_layer_info = NULL;
	CHAR diff_model_path[CONFIG_MAX_SIZE];
	//UINT32 diff_model_size = 0;
	UINT32 model_dif_size_temp=0;
#endif

// blob and bitdepth
	UINT32 io_num_m=0;
	UINT32 *p_input_blob_info_m=NULL;
	
	NN_DATA in_layer_mem_m={0};
	INT16 input_bit_m=0;


    p_scmt_parm->result_flag=0;

    model_num=p_scmt_parm->models_number;
    iterations=p_scmt_parm->model_iterations;



    tem_mem.va=p_tot_mem->va;
    tem_mem.pa=p_tot_mem->pa;
    tem_mem.size=p_tot_mem->size;

	// tem_mem_img.va=p_tot_mem_img->va;
    // tem_mem_img.pa=p_tot_mem_img->pa;
    // tem_mem_img.size=p_tot_mem_img->size;


    strcpy(path_files,p_scmt_parm->path_files);
    snprintf(error_file,CONFIG_MAX_SIZE,"%s/output/ai_test_4/error_%d.txt",path_files,(int) net_id);

	AI_STRCPY(imglist_path,p_scmt_parm->img_list_path,CONFIG_MAX_SIZE);
    // src_img=scmt_getmem_v2(&tem_mem_img,max_src_size,run_id,error_file,&tflag,debugm);
    // if(tflag>0) {
        // p_scmt_parm->result_flag=1;
        // goto texit;
    // }

    // max_model_mem=scmt_getmem_v2(&tem_mem,p_net_parm->max_model_size,run_id,error_file,&tflag,debugm);
    // if(tflag>0) {
        // p_scmt_parm->result_flag=1;
        // goto texit;
    // }

    // rslt_new_mem=scmt_getmem_v2(&tem_mem,sizeof(VENDOR_AIS_RESULT_INFO)+MAX_OBJ_NUM * sizeof(VENDOR_AIS_RESULT),run_id,error_file,&tflag,debugm);
    // if(tflag>0) {
        // p_scmt_parm->result_flag=1;
        // goto texit;
    // }
// #if AUTO_UPDATE_DIM
	// diff_model_mem=scmt_getmem_v2(&tem_mem,p_net_parm->max_diffmodel_size,run_id,error_file,&tflag,debugm);
	// if(tflag>0) {
        // p_scmt_parm->result_flag=1;
        // goto texit;
    // }
    // ft_out_result=scmt_getmem_v2(&tem_mem,NET_RESULT_MAX_SIZE,run_id,error_file,&tflag,debugm);
    // if(tflag>0) {
        // p_scmt_parm->result_flag=1;
        // goto texit;
    // }
// #endif

// #if RESULT_COMPARE
    // net_result=scmt_getmem_v2(&tem_mem,NET_RESULT_MAX_SIZE,run_id,error_file,&tflag,debugm);
    // if(tflag>0) {
        // p_scmt_parm->result_flag=1;
        // goto texit;
    // }	
    // net_out_result=scmt_getmem_v2(&tem_mem,NET_RESULT_MAX_SIZE,run_id,error_file,&tflag,debugm);
    // if(tflag>0) {
        // p_scmt_parm->result_flag=1;
        // goto texit;
    // }
	
    // req_size=rslt_new_mem.size+max_model_mem.size+net_result.size+net_out_result.size;

    // if (req_size > p_tot_mem->size) {
        // //p_scmt_parm->result_flag=1;
        // if(debugm==1){
			// DBG_ERR("pthread %d:  require memory is not enough(%d), need(%d)\r\n", (INT)net_id,(INT)p_tot_mem->size, (INT)req_size);
        // }
		// //goto texit;
	// }
// #else
    // req_size=rslt_new_mem.size+max_model_mem.size;

    // if (req_size > p_tot_mem->size) {

        // //p_scmt_parm->result_flag=1;
        // if(debugm==1){
        // DBG_ERR("pthread %d:  require memory is not enough(%d), need(%d)\r\n", (INT)net_id,(INT)p_tot_mem->size, (INT)req_size);
        // }
		// //goto texit;
	// }
// #endif

	fp=fopen(p_scmt_parm->model_list_path,"r");
	if(fp==NULL) {
		p_scmt_parm->result_flag=1;
		if(debugm==1){
			DBG_ERR("pthread:%u %s is NULL!\r\n",net_id,p_scmt_parm->model_list_path);
		}
		goto texit;
	}
		
    if(debugm==1){
        DBG_ERR("model_num: %u\r\n",(unsigned int)model_num);
	    DBG_ERR("core_num: %u\r\n",(unsigned int)p_scmt_parm->core_num);
        DBG_ERR("imagelist_num: %u\r\n",(unsigned int)p_scmt_parm->imagelist_num);
        DBG_ERR("auto_flag: %u\r\n",(unsigned int)p_scmt_parm->auto_updata_dim_flag);
    }
    for (idx=0;idx<model_num;idx++) {
		if(feof(fp))
			break;
		fscanf(fp,"%s\n",mname);
		tem_mem.va=p_tot_mem->va;
		tem_mem.pa=p_tot_mem->pa;
		tem_mem.size=p_tot_mem->size;

		sprintf(model_path,"%s/model_bins/%s/sdk/nvt_model.bin",path_files,mname);
		model_size_temp = get_thread_model_size(mname, path_files,&tflag);
		if(tflag>0 || model_size_temp == 0) {
			p_scmt_parm->result_flag=1;
			goto texit;
		}
		if(p_scmt_parm->auto_updata_dim_flag == 1) {
#if AUTO_UPDATE_DIM		
			model_dif_size_temp=get_thread_diffmodel_size(mname, path_files,&tflag);
			if(tflag>0 || model_dif_size_temp == 0) {
				p_scmt_parm->result_flag=1;
				goto texit;
			}
#endif		
		}		
		result_size_temp=get_thread_resultsize(model_path,tem_mem,&tflag,net_id);
		
		if(p_scmt_parm->auto_updata_dim_flag == 1) {
#if AUTO_UPDATE_DIM	
			diff_model_mem=scmt_getmem_v2(&tem_mem,model_dif_size_temp,run_id,error_file,&tflag,debugm);
			if(tflag>0) {
				p_scmt_parm->result_flag=1;
				goto texit;
			}
#endif
		}
		max_model_mem=scmt_getmem_v2(&tem_mem,model_size_temp,run_id,error_file,&tflag,debugm);
		if(debugm==1){
			DBG_WRN("pthread %d:  model_iterations:%u max_model_mem.size:%ld\r\n", (INT)net_id,iterations,max_model_mem.size);
		}
		if(tflag>0) {
			p_scmt_parm->result_flag=1;
			if(debugm==1){
				DBG_WRN("pthread %d:  model_iterations:%u max_model_mem scmt_getmem_v2\r\n", (INT)net_id,iterations);
			}
			goto texit;

		}		
		net_result=scmt_getmem_v2(&tem_mem,result_size_temp,run_id,error_file,&tflag,debugm);
		if(tflag>0) {
			p_scmt_parm->result_flag=1;
			if(debugm==1){
				DBG_WRN("pthread %d:  model_iterations:%u net_result scmt_getmem_v2\r\n", (INT)net_id,iterations);
			}
			goto texit;

		}
		net_out_result=scmt_getmem_v2(&tem_mem,result_size_temp,run_id,error_file,&tflag,debugm);
		if(tflag>0) {
			p_scmt_parm->result_flag=1;
			if(debugm==1){
				DBG_WRN("pthread %d:  model_iterations:%u net_out_result scmt_getmem_v2\r\n", (INT)net_id,iterations);
			}
			goto texit;

		}
		rslt_new_mem=scmt_getmem_v2(&tem_mem,sizeof(VENDOR_AIS_RESULT_INFO)+MAX_OBJ_NUM * sizeof(VENDOR_AIS_RESULT),run_id,error_file,&tflag,debugm);
		if(tflag>0) {
			p_scmt_parm->result_flag=1;
			goto texit;
		}
		
		img_size_temp=tem_mem.size;
		src_img=scmt_getmem_v2(&tem_mem,img_size_temp,run_id,error_file,&tflag,debugm);
		if(tflag>0) {
			p_scmt_parm->result_flag=1;
			if(debugm==1){
				DBG_WRN("pthread %d:  model_iterations:%u src_img scmt_getmem_v2\r\n", (INT)net_id,iterations);
			}
			goto texit;

		}

		for(mdx=0;mdx<p_scmt_parm->core_num;mdx++) {         		
			core=mdx+1;
			//if (core != p_scmt_parm->core[mdx])
				//continue;
			if(p_scmt_parm->auto_updata_dim_flag==1) {
#if AUTO_UPDATE_DIM
				sprintf(diff_model_path,"%s/model_bins/%s/sdk/nvt_stripe_model.bin",path_files,mname);
#endif
			}

#if RESULT_COMPARE

			file_size=vendor_ais_load_file(model_path,max_model_mem.va,&output_layer_info, &num_output_layer);
#else
			file_size=ai_load_file(model_path,max_model_mem.va);
#endif
			if (file_size == 0) {

				if(cfp==NULL ) {
					cfp=fopen(error_file,"w+");
					if(cfp==NULL) {
						p_scmt_parm->result_flag=1;
                        if(debugm==1){
							DBG_ERR("error out file: %s  null\r\n",error_file);
                        }
					} else {
						p_scmt_parm->result_flag=1;
						fprintf(cfp,"%s %u NULL NULL 1\r\n",mname,core);
					}
				} else {
					p_scmt_parm->result_flag=1;
					fprintf(cfp,"%s %u NULL NULL 1\r\n",mname,core);
				}
				continue;
				}


			model_size=vendor_ais_auto_alloc_mem(&max_model_mem, &mem_manager);
			if (model_size > p_net_parm->max_model_size) {
				if(cfp==NULL ) {
					cfp=fopen(error_file,"w+");
					if(cfp==NULL) {
						p_scmt_parm->result_flag=1;
                        if(debugm==1){
						DBG_ERR("error out file: %s  null\r\n",error_file);
                        }

					} else {
						p_scmt_parm->result_flag=1;
						fprintf(cfp,"%s %u NULL NULL 1\r\n",mname,core);

					}
				} else {
					 p_scmt_parm->result_flag=1;
					 fprintf(cfp,"%s %u NULL NULL 1\r\n",mname,core);

				}
				continue;
			}

            if(debugm==1){
        		DBG_ERR("pthread:%u start gen init\r\n",(unsigned int)net_id);
            }

			memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
			memset((VOID *)rslt_new_mem.va, 0, rslt_new_mem.size);    // clear result buffer
			pthread_mutex_lock(&lock);
			ret = vendor_ais_net_gen_init(mem_manager,net_id);
			pthread_mutex_unlock(&lock);
			if (ret != HD_OK) {
				if(cfp==NULL ) {
					cfp=fopen(error_file,"w+");
					if(cfp==NULL) {
						p_scmt_parm->result_flag=1;
                        if(debugm==1){
							DBG_ERR("error out file: %s  null\r\n",error_file);
                        }

					} else {
						p_scmt_parm->result_flag=1;
						fprintf(cfp,"%s %u NULL NULL 1\r\n",mname,core);

					}
				} else {
					 p_scmt_parm->result_flag=1;
					 fprintf(cfp,"%s %u NULL NULL 1\r\n",mname,core);

				}
				goto gen_init_fail;
			}

			count=count+1;

#if !CNN_25_MATLAB
			ret = vendor_ais_get_net_input_info(&input_info, max_model_mem);
			if (ret == 0) {
                /*
				printf("model_fmt = %s\n", input_info->model_fmt);
				printf("model_width = %d\n", input_info->model_width);
				printf("model_height = %d\n", input_info->model_height);
				printf("model_channel = %d\n", input_info->model_channel);
				printf("model_batch = %d\n", input_info->model_batch);
				printf("in_fmt = %s\n", input_info->in_fmt);
				printf("in_channel = %d\n", input_info->in_channel);
				*/

			} else{
				if(cfp==NULL ) {
					cfp=fopen(error_file,"w+");
					if(cfp==NULL){
						p_scmt_parm->result_flag=1;
                        if(debugm==1){
						DBG_ERR("error out file: %s  null\r\n",error_file);
                        }
					} else {
						p_scmt_parm->result_flag=1;
						fprintf(cfp,"%s %u NULL NULL 1\r\n",mname,core);

					}
				} else {
					 p_scmt_parm->result_flag=1;
					 fprintf(cfp,"%s %u NULL NULL 1\r\n",mname,core);

				}
				goto gen_init_fail;
			}
#endif
			// get blob and bitdepth
			//blob
			blob=0;			
			for(UINT16 i_m=0;i_m<AI_SAMPLE_MAX_INPUT_NUM;i_m++) {
				ret=vendor_ais_net_get_input_blob_info(max_model_mem, i_m, &io_num_m, &p_input_blob_info_m);
				if( ret!= HD_OK) {
					break;
				} else {
					blob=blob+1;
				}
			}
			//blob
			if (blob != 1 && blob != 2)
				continue;
            if((p_scmt_parm->auto_updata_dim_flag == 1) && (blob==2))
                continue;
			//bitdepth
			vendor_ais_get_input_layer_mem(max_model_mem, &in_layer_mem_m, 0);
			input_bit_m= (INT16)in_layer_mem_m.fmt.int_bits+(INT16)in_layer_mem_m.fmt.frac_bits+(INT16)in_layer_mem_m.fmt.sign_bits+(INT16)in_layer_mem_m.fmt.reserved;
			img_fmttype= (UINT32) input_bit_m;
#if MODIFY_ENG_IN_SAMPLE
			// parse engine id for all cnn layer
			//printf("%s core: %ld \r\n",mname,core);
			vendor_ais_pars_engid(mdx, net_id);			
#endif
			if((fpp=fopen(imglist_path,"r")) == NULL) {
				p_scmt_parm->result_flag=1;
				if(debugm==1){
					DBG_ERR("pthread %u : %s NULL!\r\n",net_id,imglist_path);
				}
				goto texit;	
			}
              
			while(1) {
				if(blob == 1) {
					if(feof(fpp)) {
						fclose(fpp);
						fpp=NULL;
						break;
					}
				
					if(fgets(imginfo,1024,fpp) == NULL) {
						fclose(fpp);
						fpp=NULL;
						break;
					}
					memset(ori_imginfo,0,sizeof(ori_imginfo)); 
					strcpy(ori_imginfo,imginfo);
					pstr=strtok(imginfo," ");
					strcpy(temp_info,pstr);

					if(debugm==1){
						DBG_ERR("before split_kvalue\r\n");
						DBG_ERR("temp_info: %s\r\n",temp_info);
					}
					if(split_kvalue(temp_info,miname[0],name[0],key_c) != -1) {
						if(debugm==1){
						 DBG_WRN("temp_info :%s\r\n",temp_info);
						}
						continue;
					}
					strcpy(name[0],temp_info);
				
					
					if(get_imgparas(ori_imginfo,p_src_img,miname,name,blob,key_c) == -1) {
						 if(debugm==1){
							DBG_WRN("p_src_img[0].fmt_type :%d\r\n",p_src_img[0].fmt_type);
						 }
						 continue;
					}
					if(debugm==1){
						DBG_ERR("after get_imgparas\r\n");
						DBG_WRN("img_fmttype :%u\r\n",(unsigned int)img_fmttype);
					}

					if (p_src_img[0].fmt_type <=0 || p_src_img[0].fmt_type > 32 || p_src_img[0].fmt_type % 8 != 0 || p_src_img[0].fmt_type != img_fmttype){

						continue;
					}

					if(p_scmt_parm->auto_updata_dim_flag==1) {
#if AUTO_UPDATE_DIM
						if( (p_src_img[0].channel!=input_info->in_channel))
							continue;
#endif
					} else {
						if( (p_src_img[0].channel!=input_info->in_channel) || (p_src_img[0].height < input_info->model_height) || (p_src_img[0].width < input_info->model_width))
							continue;
					}
					sprintf(imgpath,"%s/test_images/%s",path_files,name[0]);

					file_size=scmt_load_file(imgpath,src_img.va,src_img.size);

					if (file_size<=0) {
						if(cfp==NULL) {
							cfp=fopen(error_file,"w+");
							if(cfp==NULL) {
								p_scmt_parm->result_flag=1;
								if(debugm==1){
								DBG_ERR("error out file: %s  null\r\n",error_file);
								}

							} else {
								p_scmt_parm->result_flag=1;
								fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name);

							}
						} else {
							 p_scmt_parm->result_flag=1;
							 fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name);

						}

						continue;
					}




					p_src_img[0].va=src_img.va;
					p_src_img[0].pa=src_img.pa;
					p_src_img[0].line_ofs=p_src_img[0].width*p_src_img[0].fmt_type/8;
					#if AI_SUPPORT_MULTI_FMT
					p_src_img[0].fmt_type=0;
					#endif
					if(p_scmt_parm->auto_updata_dim_flag==1) {
#if AUTO_UPDATE_DIM
						diff_info.input_width = p_src_img[0].width;
						diff_info.input_height =p_src_img[0].height;
						diff_info.id = 0;
						file_size=vendor_ais_load_file(diff_model_path,diff_model_mem.va,NULL,&num_output_layer);
						if (file_size == 0) {
							if(cfp==NULL ) {
								cfp=fopen(error_file,"w+");
								if(cfp==NULL) {
									p_scmt_parm->result_flag=1;
									if(debugm==1){
									DBG_ERR("error out file: %s  null\r\n",error_file);
									}

								} else {
									p_scmt_parm->result_flag=1;
									fprintf(cfp,"%s %u NULL NULL 1\r\n",diff_model_path,core);

								}
							} else {
								p_scmt_parm->result_flag=1;
								fprintf(cfp,"%s %u NULL NULL 1\r\n",diff_model_path,core);

							}
							goto gen_init_fail;
						}

						ret=vendor_ais_pars_diff_mem(&mem_manager, &diff_model_mem, &diff_info, net_id);
						if (ret != HD_OK) {

							continue;
						}
#endif
					}

#if AI_SAMPLE_TEST_BATCH

					pb_src_img = &p_src_img[0];
					tmp_src_va=pb_src_img->va;
					tmp_src_pa=pb_src_img->pa;
					src_img_size=pb_src_img->line_ofs * pb_src_img->height * pb_src_img->channel;
					input_proc_num=vendor_ais_net_get_input_layer_index(max_model_mem, input_proc_idx);
					if(debugm==1){
						DBG_WRN("pb_src_img=====\r\n");
						DBG_WRN("pb_src_img.va:%ld\r\n",pb_src_img->va);
						DBG_WRN("input_proc_num:%u",input_proc_num);
					}
					for (batch_i = 0; batch_i < input_proc_num; batch_i++) {
						if(batch_i == 0){
							hd_common_mem_flush_cache((VOID *)(pb_src_img->va), src_img_size);
						}
						if(batch_i>0) {
							memcpy((VOID *)pb_src_img->va+src_img_size,(VOID *)pb_src_img->va,src_img_size);
							hd_common_mem_flush_cache((VOID *)(pb_src_img->va + src_img_size), src_img_size);
							pb_src_img->pa += src_img_size;
							pb_src_img->va += src_img_size;
						}
						if (vendor_ais_net_input_layer_init(pb_src_img, batch_i, net_id) != HD_OK) {
							if(cfp==NULL) {
								cfp=fopen(error_file,"w+");
								if(cfp==NULL) {
									p_scmt_parm->result_flag=1;
									if(debugm==1){
										DBG_ERR("error out file: %s  null\r\n",error_file);
									}

								} else {
									p_scmt_parm->result_flag=1;
									fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name);

								}
							} else {
								 p_scmt_parm->result_flag=1;
								 fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name);

							}
							goto input_init_fail;

						}
					}
					pb_src_img->va = tmp_src_va;
					pb_src_img->pa = tmp_src_pa;
					if(debugm==1){
						DBG_ERR("after img init\r\n");
					}
#else

					if (vendor_ais_net_input_init(&p_src_img[0], net_id) != HD_OK) {
						if(cfp==NULL ) {
							cfp=fopen(error_file,"w+");
							if(cfp==NULL) {
								p_scmt_parm->result_flag=1;
								if(debugm==1){
								DBG_ERR("error out file: %s  null\r\n",error_file);
								}

							} else {							
								p_scmt_parm->result_flag=1;
								fprintf(cfp," %s %u %s NULL 2\r\n",mname,core,name);

							}
						} else {						
							 p_scmt_parm->result_flag=1;
							 fprintf(cfp," %s %u %s NULL 2\r\n",mname,core,name);

						}
						goto input_init_fail;
					}
#endif
				} else {
					if(feof(fpp)) {
						fclose(fpp);
						fpp=NULL;
						break;
					}
					if(fgets(imginfo,1024,fpp) == NULL) {
						fclose(fpp);
						fpp=NULL;
						break;
					}
					memset(ori_imginfo,0,sizeof(ori_imginfo)); 
					strcpy(ori_imginfo,imginfo);
					pstr=strtok(imginfo," ");
					strcpy(temp_info,pstr);
					if(split_kvalue(temp_info,miname[0],name[0],key_c) == -1) {
						continue;
					} else {
						if(debugm==1){
							 DBG_WRN("miname[0]:%s\r\n",miname[0]);
							 DBG_WRN("name[0]:%s\r\n",name[0]);
						}
						remove_blank(miname[0]);
						remove_blank(name[0]);
						if(strcmp(mname,miname[0]) != 0)
							continue;
						if (get_imgparas(ori_imginfo,p_src_img,miname,name,blob,key_c) == -1)
								 continue;

						sprintf(imgpath,"%s/test_images/%s",path_files,name[0]);
						if(debugm==1){
							 DBG_WRN("imgpath0:%s\r\n",imgpath);
						}
						file_size=scmt_load_file(imgpath,src_img.va,src_img.size);
						if(debugm==1){
							 DBG_WRN("imgpath0 file_size:%ld\r\n",file_size);
						}
						if (file_size <= 0) {
								if(cfp==NULL) {
									cfp=fopen(error_file,"w+");
									if(cfp==NULL) {
										p_scmt_parm->result_flag=1;
										if(debugm==1){
											DBG_ERR("error out file: %s  null\r\n",error_file);
										}

									} else {									
										p_scmt_parm->result_flag=1;
										fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name[0]);
										if(debugm==1){
											fprintf(cfp,"file size:%d\r\n",file_size);
										}

									}
								} else {							
									 p_scmt_parm->result_flag=1;
									 fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name[0]);
									 if(debugm==1){
											fprintf(cfp,"file size:%d\r\n",file_size);
									}


								}

								continue;
						}
						p_src_img[0].va=src_img.va;
						p_src_img[0].pa=src_img.pa;
						#if AI_SUPPORT_MULTI_FMT
						p_src_img[0].fmt_type=0;
						#endif
						temp_size=src_img.size-file_size;
						va_src_temp=src_img.va+YUV_SINGLE_BUF_SIZE;
						pa_src_temp=src_img.pa+YUV_SINGLE_BUF_SIZE;


						sprintf(imgpath,"%s/test_images/%s",path_files,name[1]);
						if(debugm==1){
							 DBG_WRN("imgpath0:%s\r\n",imgpath);

						}
						file_size=scmt_load_file(imgpath,va_src_temp,temp_size);
						if(debugm==1){

							 DBG_WRN("imgpath1 file_size:%ld\r\n",file_size);
						}
						if (file_size <= 0) {						
								if(cfp==NULL) {								
									cfp=fopen(error_file,"w+");
									if(cfp==NULL) {									
										p_scmt_parm->result_flag=1;
										if(debugm==1){
											DBG_ERR("error out file: %s  null\r\n",error_file);
										}

									} else {
										p_scmt_parm->result_flag=1;
										fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name[1]);
										if(debugm==1){
											fprintf(cfp,"file size:%d\r\n",file_size);
										}

									}
								} else {

									 p_scmt_parm->result_flag=1;
									 fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name[1]);
									 if(debugm==1){
											fprintf(cfp,"file size:%d\r\n",file_size);
									}


								}

								continue;
						}
						p_src_img[1].va=va_src_temp;
						p_src_img[1].pa=pa_src_temp;
						#if AI_SUPPORT_MULTI_FMT
						p_src_img[1].fmt_type=0;
						#endif

#if AI_SAMPLE_TEST_BATCH
#if !SUPPORT_MULTI_BLOB_IN_SAMPLE

						for(blob_j=0;blob_j<blob;blob_j++) {
							if (vendor_ais_net_input_layer_init(&p_src_img[blob_j], blob_j, net_id) != HD_OK) {
								if(cfp==NULL ) {
									cfp=fopen(error_file,"w+");
									if(cfp==NULL) {
										p_scmt_parm->result_flag=1;
										if(debugm==1){
											DBG_ERR("error out file: %s  null\r\n",error_file);
										}

									} else {
										p_scmt_parm->result_flag=1;
										fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name[blob_j]);

									}
								} else {
									 p_scmt_parm->result_flag=1;
									 fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name[blob_j]);

								}
								goto input_init_fail;
							}
						}
#else
						if(debugm==1){
							DBG_ERR("START BLOB INIT\r\n");
						}

						for(blob_j=0;blob_j<blob;blob_j++) {

							if (vendor_ais_net_get_input_blob_info(max_model_mem, blob_j, &io_num, &p_input_blob_info) != HD_OK) {
								goto input_init_fail;
							}
							if(debugm==1){
								DBG_ERR("INPUT\r\n");
								DBG_ERR("blob_j:%u io_num: %u  \r\n",(unsigned int)blob_j,(unsigned int)io_num);
								DBG_ERR("p_input_blob_info[0]: %u  p_input_blob_info[1]: %u\r\n",(unsigned int)p_input_blob_info[0],(unsigned int)p_input_blob_info[1]);
							}
							for(batch_i=0;batch_i<io_num;batch_i++) {
								proc_idx=p_input_blob_info[batch_i] >> 8;
								tmp_va=p_src_img[blob_j].va;
								tmp_pa=p_src_img[blob_j].pa;
								p_src_img[blob_j].va += (batch_i*p_src_img[blob_j].batch_ofs);
								p_src_img[blob_j].pa += (batch_i*p_src_img[blob_j].batch_ofs);
								hd_common_mem_flush_cache((VOID *)(p_src_img[blob_j].va), p_src_img[blob_j].batch_ofs);
								if (vendor_ais_net_input_layer_init(&p_src_img[blob_j], proc_idx, net_id) != HD_OK){
									if(cfp==NULL ) {
										cfp=fopen(error_file,"w+");
										if(cfp==NULL) {
											p_scmt_parm->result_flag=1;
											if(debugm==1){
												DBG_ERR("error out file: %s  null\r\n",error_file);
											}

										} else {
											p_scmt_parm->result_flag=1;
											fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name[blob_j]);

										}
									} else {
										 p_scmt_parm->result_flag=1;
										 fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name[blob_j]);

									}
									goto input_init_fail;
								}
								p_src_img[blob_j].va = tmp_va;
								p_src_img[blob_j].pa = tmp_pa;
							}
						}
#endif
#else
						if(vendor_ais_net_input_init(p_src_img, net_id) != HD_OK) {

							if(cfp==NULL ) {
								cfp=fopen(error_file,"w+");
								if(cfp==NULL) {
									p_scmt_parm->result_flag=1;
									if(debugm==1){
										DBG_ERR("error out file: %s  null\r\n",error_file);
									}

								} else {
									p_scmt_parm->result_flag=1;
									fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name[0]);

								}
							} else {
								 p_scmt_parm->result_flag=1;
								 fprintf(cfp,"%s %u %s NULL 2!\r\n",mname,core,name[0]);

							}
							goto input_init_fail;
						}
#endif


					}
				}
				counti=counti+1;
#if RESULT_COMPARE
				if(debugm==1){
					DBG_ERR("start result compare\r\n");
				}
				file_size=0;
				strcpy(cname,name[0]);
				imgsplit_name=strtok(cname,".");
				snprintf(img_save_path, CONFIG_MAX_SIZE,"%s/gt_results/%s/%s/outfeature.bin", path_files,mname,imgsplit_name);


				gfp=fopen(img_save_path,"r");
				if(gfp==NULL) {
#if AUTO_UPDATE_DIM
					if(p_scmt_parm->auto_updata_dim_flag == 1) {
						if(debugm==1){
							DBG_ERR("pthread:%u %s  auto_updata no gt!\r\n",net_id,img_save_path);
						}
					} else {
						p_scmt_parm->result_flag=1;
						if(debugm==1){
							DBG_ERR("pthread:%u %s not found!\r\n",net_id,img_save_path);
						}
						goto input_init_fail;
					}
#else
					p_scmt_parm->result_flag=1;
					if(debugm==1){
						DBG_ERR("pthread:%u %s not found!\r\n",net_id,img_save_path);
					}
					goto input_init_fail;
#endif


				} else {
					fclose(gfp);
					gfp=NULL;
					file_size=scmt_load_file(img_save_path,net_result.va,net_result.size);
					if(file_size <= 0)
					{
						p_scmt_parm->result_flag=1;
						if(debugm==1){
							DBG_ERR("pthread:%u %s NULL!\r\n",(unsigned int)net_id,img_save_path);
						}
						goto input_init_fail;
					}
				}



#endif
				for (jdx=0;jdx<iterations;jdx++) {
					 if(debugm==1){
						DBG_ERR("procnet iter:%u\r\n",(unsigned int)jdx);
					}
					memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
					hd_common_mem_flush_cache((VOID *)mem_manager.user_buff.va, mem_manager.user_buff.size);

#if USE_NEON

					vendor_ais_proc_net(max_model_mem, rslt_new_mem, &p_src_img[0], net_id);
#else
					vendor_ais_proc_net(max_model_mem, rslt_new_mem, net_id);
#endif

//compare result
#if RESULT_COMPARE
					net_out_size=0;
					out_temp_va=net_out_result.va;
#if AUTO_UPDATE_DIM
					if((p_scmt_parm->auto_updata_dim_flag == 1) && (jdx==0) && file_size <=0) {
						ft_out_result.va=net_result.va;
						ft_out_result.pa=net_result.pa;
						ft_out_result.size=net_result.size;
						out_temp_gt_va=ft_out_result.va;
						ft_out_size=0;
					}																		
#endif

					for (UINT32 i= 0; i<num_output_layer; i++) {
						ret = vendor_ais_get_layer_mem_v2(max_model_mem,&output_layer_iomem,&output_layer_info[i]);

						if (ret == HD_OK) {

							if (output_layer_iomem.va > 0 && output_layer_iomem.size > 0 && output_layer_iomem.size<=net_result.size) {
								hd_common_mem_flush_cache((VOID *)output_layer_iomem.va, output_layer_iomem.size);
							} else {
								if(cfp==NULL) {
									cfp=fopen(error_file,"w+");
									if(cfp==NULL) {
										p_scmt_parm->result_flag=1;
										if(debugm==1){
										DBG_ERR("error out file: %s  null\r\n",error_file);
										}

									} else {
										p_scmt_parm->result_flag=1;
										fprintf(cfp,"%s %u %s %u : 3\r\n",mname,core,name[0],(unsigned int) jdx);

									}
								} else {
									 p_scmt_parm->result_flag=1;
									 fprintf(cfp,"%s %u %s %u : 3\r\n",mname,core,name[0],(unsigned int) jdx);

								}
								goto input_init_fail;
							}
							net_out_size += output_layer_iomem.size;
							if (net_out_size > net_out_result.size) {

								p_scmt_parm->result_flag=1;
								if(debugm==1){
									DBG_ERR("pthread %u:%s %u %s out store mem not enough:%ld need >%ld \r\n",(unsigned int)net_id,mname,(unsigned int)core,name[0],net_out_result.size,net_out_size);
								}
								goto input_init_fail;
							}

							memcpy((VOID *)out_temp_va,(VOID *)output_layer_iomem.va,output_layer_iomem.size);
							out_temp_va=out_temp_va+output_layer_iomem.size;
#if AUTO_UPDATE_DIM
							if((p_scmt_parm->auto_updata_dim_flag == 1) && (jdx==0)&& file_size <=0) {

								memcpy((VOID *)out_temp_gt_va,(VOID *)output_layer_iomem.va,output_layer_iomem.size);
								out_temp_gt_va+=output_layer_iomem.size;
								ft_out_size+=output_layer_iomem.size;
							}

#endif
						} else {


							if(cfp==NULL ) {
								cfp=fopen(error_file,"w+");
								if(cfp==NULL) {
									p_scmt_parm->result_flag=1;
									if(debugm==1){
									DBG_ERR("error out file: %s  null\r\n",error_file);
									}

								} else {
									p_scmt_parm->result_flag=1;
									fprintf(cfp,"%s %u %s %u 3\r\n",mname,core,name[0],(unsigned int) jdx);

								}
							} else {
								 p_scmt_parm->result_flag=1;
								 fprintf(cfp,"%s %u %s %u 3\r\n",mname,core,name[0],(unsigned int) jdx);

							}
							goto input_init_fail;
						}


					}
					if(p_scmt_parm->auto_updata_dim_flag == 1) {
#if AUTO_UPDATE_DIM
						if(file_size > 0) {
							///gt
							if(result_compare_v1(net_result.va,file_size,(UINT8 *)net_out_result.va,net_out_size)<0 ) {

								if(cfp==NULL ) {
									cfp=fopen(error_file,"w+");
									if(cfp==NULL) {									
										p_scmt_parm->result_flag=1;
										if(debugm==1){
										DBG_ERR("error out file: %s  null\r\n",error_file);
										}

									} else {
										p_scmt_parm->result_flag=1;
										fprintf(cfp,"%s %u %s %u 4\r\n",mname,core,name[0],(unsigned int) jdx);

									}
								} else {
									 p_scmt_parm->result_flag=1;
									 fprintf(cfp,"%s %u %s %u 4\r\n",mname,core,name[0],(unsigned int) jdx);

								}
								/*
								if(debugm==1){
										 sprintf(command_test, "mkdir -p %s/output/ai_test_4/output_bin_%d/%s/%s_%d",path_files,(int)run_id,mname,imgsplit_name,(int)jdx);
										 system(command_test);
										 sprintf(out_img_path,"%s/output/ai_test_4/output_bin_%d/%s/%s_%d/OUT.bin",path_files,(int)run_id,mname,imgsplit_name,(int)jdx);

										 vendor_ais_write_file(out_img_path, net_out_result.va, net_out_size);

								}
								*/
								break;
							}
							///gt
						} else {

							if(jdx==0){
								/*
								if(debugm==1){

										 sprintf(command_test, "mkdir -p %s/output/ai_test_4/output_bin_%d/%s/%s_%d",path_files,(int)run_id,mname,imgsplit_name,(int)jdx);
										 system(command_test);
										 sprintf(out_img_path,"%s/output/ai_test_4/output_bin_%d/%s/%s_%d/outgt.bin",path_files,(int)run_id,mname,imgsplit_name,(int)jdx);
										 vendor_ais_write_file(out_img_path, ft_out_result.va, ft_out_size);
								}
								*/
								continue;
							} else {

								if(result_compare_v1(ft_out_result.va,ft_out_size,(UINT8 *)net_out_result.va,net_out_size)<0 ) {

									if(cfp==NULL ) {
										cfp=fopen(error_file,"w+");
										if(cfp==NULL) {
											p_scmt_parm->result_flag=1;
											if(debugm==1){
											DBG_ERR("error out file: %s  null\r\n",error_file);
											}

										} else {
											p_scmt_parm->result_flag=1;
											fprintf(cfp,"%s %u %s %u 4\r\n",mname,core,name[0],(unsigned int) jdx);

										}
									} else {
										 p_scmt_parm->result_flag=1;
										 fprintf(cfp,"%s %u %s %u 4\r\n",mname,core,name[0],(unsigned int) jdx);

									}
									/*
									if(debugm==1){
											 DBG_WRN("net_out_size: %u ft_out_size: %u",(unsigned int)net_out_size,(unsigned int)ft_out_size);
											 sprintf(command_test, "mkdir -p %s/output/ai_test_4/output_bin_%d/%s/%s_%d",path_files,(int)run_id,mname,imgsplit_name,(int)jdx);
											 system(command_test);
											 sprintf(out_img_path,"%s/output/ai_test_4/output_bin_%d/%s/%s_%d/OUT.bin",path_files,(int)run_id,mname,imgsplit_name,(int)jdx);

											 vendor_ais_write_file(out_img_path, net_out_result.va, net_out_size);

									}
									*/
									break;
								}

							}

						}

#endif
					} else {
						//////compare
						if(result_compare_v1(net_result.va,file_size,(UINT8 *)net_out_result.va,net_out_size)<0) {
						

							if(cfp==NULL )
							{
								cfp=fopen(error_file,"w+");
								if(cfp==NULL)
								{
									p_scmt_parm->result_flag=1;
									if(debugm==1){
									DBG_ERR("error out file: %s  null\r\n",error_file);
									}

								} else {
									p_scmt_parm->result_flag=1;
									fprintf(cfp,"%s %u %s %u 4\r\n",mname,core,name[0],(unsigned int) jdx);

								}
							} else {
								 p_scmt_parm->result_flag=1;
								 fprintf(cfp,"%s %u %s %u 4\r\n",mname,core,name[0],(unsigned int) jdx);

							}
							/*
							if(debugm==1){
									 sprintf(command_test, "mkdir -p %s/output/ai_test_4/output_bin_%d/%s/%s_%d",path_files,(int)run_id,mname,imgsplit_name,(int)jdx);
									 system(command_test);
									 sprintf(out_img_path,"%s/output/ai_test_4/output_bin_%d/%s/%s_%d/OUT.bin",path_files,(int)run_id,mname,imgsplit_name,(int)jdx);

									 vendor_ais_write_file(out_img_path, net_out_result.va, net_out_size);

							}
							*/
							break;
						}
						//////compare
					}



#endif
				}//jdx iteration
input_init_fail:

				if(blob ==1) {
				
					if(debugm==1){
						DBG_ERR("uninit img\r\n");
					}
#if AI_SAMPLE_TEST_BATCH
					for(batch_i=0;batch_i<input_proc_num;batch_i++) {
					
						ret=vendor_ais_net_input_layer_uninit(batch_i, net_id);
						if(ret!=HD_OK){
							p_scmt_parm->result_flag=1;
							if(debugm==1){
								DBG_ERR("pethread %u: %s %u %s vendor_ais_net_input_uninit fail: %d\r\n",(unsigned int)net_id,mname,(unsigned int)core,name[0],ret);
							}
							goto gen_init_fail;
						}

					}
#else

					ret=vendor_ais_net_input_uninit(net_id);
					if(ret != HD_OK) {

						p_scmt_parm->result_flag=1;
						if(debugm==1){
						DBG_ERR("pethread %u:%s %u %s vendor_ais_net_input_uninit fail: %d\r\n",(unsigned int)net_id,mname,(unsigned int)core,name[0],ret);
						}
						goto gen_init_fail;
					}
#endif
					if(p_scmt_parm->auto_updata_dim_flag==1) {
					
#if AUTO_UPDATE_DIM
						vendor_ais_unpars_diff_mem(&mem_manager, &diff_model_mem, &diff_info, net_id);
#endif
					}
				} else {
#if AI_SAMPLE_TEST_BATCH
#if !SUPPORT_MULTI_BLOB_IN_SAMPLE
					for (blob_j = 0; blob_j < blob; blob_j++) {
						ret = vendor_ais_net_input_layer_uninit(blob_j, net_id);
						if(ret != HD_OK) {
							if(debugm==1){
								DBG_ERR("pethread %u: %s %u %s vendor_ais_net_input_uninit fail: %d\r\n",(unsigned int)net_id,mname,(unsigned int)core,name[blob_j],ret);
						   }
						   goto gen_init_fail;

						}
					}
#else

					for (blob_j = 0; blob_j < blob; blob_j++) {
					
						if(vendor_ais_net_get_input_blob_info(max_model_mem, blob_j, &io_num, &p_input_blob_info) != HD_OK){
							if(debugm==1){
								DBG_ERR("pethread %u: %s %u %s vendor_ais_net_get_input_blob_info fail: %d\r\n",(unsigned int)net_id,mname,(unsigned int)core,name[blob_j],ret);
						   }
						   goto gen_init_fail;
						}
						if(debugm==1){
							DBG_ERR("blob_j:%u io_num: %u  \r\n",(unsigned int)blob_j,(unsigned int)io_num);
							DBG_ERR("p_input_blob_info[0]: %u  p_input_blob_info[1]: %u\r\n",(unsigned int)p_input_blob_info[0],(unsigned int)p_input_blob_info[1]);
						}
						for(batch_i=0;batch_i<io_num;batch_i++) {
						
							proc_idx = p_input_blob_info[batch_i] >> 8;
							ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
							if(ret != HD_OK) {
								if(debugm==1){
									DBG_ERR("pethread %u: %s %u %s vendor_ais_net_input_layer_uninit fail: %d\r\n",(unsigned int)net_id,mname,(unsigned int)core,name[blob_j],ret);
							   }
							   goto gen_init_fail;
							}
						}
					}

#endif
#else
					ret=vendor_ais_net_input_uninit(net_id);
					if(ret != HD_OK) {
						if(debugm==1){
							DBG_ERR("pethread %u: %s %u %s vendor_ais_net_input_uninit fail: %d\r\n",(unsigned int)net_id,mname,(unsigned int)core,name[0],ret);
					   }
					   goto gen_init_fail;

					}
#endif
				}
			}
			if(fpp != NULL) {
			
				fclose(fpp);
				fpp=NULL;
			}


			

gen_init_fail:
			ret=vendor_ais_net_gen_uninit(net_id);
			if(ret != HD_OK) {
					p_scmt_parm->result_flag=1;
                    if(debugm==1){
					    DBG_ERR("pthread%u :%s %u vendor_ais_net_gen_uninit fail: %d\r\n",(unsigned int)net_id,mname,(unsigned int)core,ret);
                    }
					goto texit;
			}
#if AI_V4
#if RESULT_COMPARE

			if (output_layer_info != NULL) {
				free(output_layer_info);
				output_layer_info = NULL;
			}
#endif
			if (input_info != NULL) {
				free(input_info);
				input_info = NULL;
			}
#endif
		}//mdx


    }//idx

    if(fp != NULL) {
	
		fclose(fp);
		fp=NULL;
	}
texit:
#if AI_V4
#if RESULT_COMPARE

    if (output_layer_info != NULL) {
        free(output_layer_info);
        output_layer_info = NULL;
    }
#endif
    if (input_info != NULL) {
        free(input_info);
        input_info = NULL;
    }
#endif

	if(fp != NULL) {
	
		fclose(fp);
		//fp=NULL;
	}
    if(fpp != NULL) {
    
        fclose(fpp);
        //fpp=NULL;
    }
    if(counti ==0 ) {
    

        p_scmt_parm->result_flag=1;
        if(debugm==1){
			DBG_ERR("pthread %u: all img can not be used\r\n",(unsigned int)net_id);
        }
    }
    if(cfp != NULL) {
    
        fclose(cfp);
        //cfp=NULL;
    }
    return 0;

}



// return the max model size
INT32 scmt_find_max_modelsize(CHAR *file,CHAR *path,UINT32 *models_number,UINT32 auto_updata_dim)
{
    FILE *fp;
    UINT32 model_num=0;
//    UINT32 fmt_type,blob,cnn_id;
    char name[50];
    char full_path[256];
    UINT32 maxmodelsize=0,modelsize=0;
    HD_RESULT           ret;

    remove_blank(file);


    fp=fopen(file,"r");
    if (fp==NULL) {

	        return -1;
    }

    while(1) {
        if(feof(fp))
            break;
        if (fscanf(fp,"%s\n",name)==EOF) {
            if(feof(fp))
                break;
            continue;
        }

#if defined(__LINUX)
		if(auto_updata_dim==0) {
		
			sprintf(full_path,"%s/model_bins/%s/sdk/nvt_model.bin",path,name);
		} else {
			sprintf(full_path,"%s/model_bins/%s/sdk/nvt_stripe_model.bin",path,name);
		}
#else
		if(auto_updata_dim==0)  {
		
			sprintf(full_path,"%s\\\\model_bins\\\\%s\\\\sdk\\\\nvt_model.bin",path,name);
		} else {
			sprintf(full_path,"%s\\\\model_bins\\\\%s\\\\sdk\\\\nvt_stripe_model.bin",path,name);
		}
#endif



		if(auto_updata_dim==0)
		{
			ret = vendor_ais_get_model_mem_sz(&modelsize, full_path);
			if (ret != HD_OK) {

				return -1;
			}
		} else {			
			ret=vendor_ais_get_diff_model_mem_sz(&modelsize,full_path);
			if (ret != HD_OK) {

				//DBG_WRN("diff find model error\r\n");
				return -1;
			}
		}

        if (modelsize>maxmodelsize){
            maxmodelsize=modelsize;
        }
        model_num=model_num+1;

    }

    fclose(fp);
    *models_number=model_num;

    return maxmodelsize;
}

INT32 check_file(CHAR *file)
{
    FILE *fp=NULL;
    UINT32 cnt=0;
    CHAR line[CONFIG_MAX_SIZE*2];
    fp=fopen(file,"r");
    if(fp==NULL)
        return -1;
    while(1) {
        if(feof(fp))
            break;
        if(fgets(line,CONFIG_MAX_SIZE*2-1,fp) == NULL) {
            if(feof(fp))
                break;

            continue;
        }
        cnt+=1;
    }
    fclose(fp);

    if(cnt==0)
        return -1;
    return 0;
}


/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
MAIN(argc, argv)
{
	//CHAR* ai_sdk_version = {NULL};
    UINT32 thread_num;
    VENDOR_AIS_NETWORK_PARM *net_parm;

	pthread_t *nn_thread_id;
    SCMT_PARM *scmt_parm=NULL;
	HD_RESULT           ret=0;

	UINT32 idx = 0,models_number=0,jdx;
    UINT32 maxmodelsize=0;
	UINT32 thread_mem=0,temp_mem_size=0;
    UINT32 para_cnt=0;
    CHAR key[CONFIG_MAX_ITEMS][CONFIG_MAX_SIZE];
    CHAR value[CONFIG_MAX_ITEMS][CONFIG_MAX_SIZE];
    CONFIG_PARA config_para;

    CHAR stemp[CONFIG_MAX_SIZE];

    //UINT32 max_src_size=MAX_FRAME_WIDTH*MAX_FRAME_HEIGHT*MAX_FRAME_CHANNEL*AI_SAMPLE_MAX_BATCH_NUM*AI_SAMPLE_MAX_INPUT_NUM;

	CHAR error_file[CONFIG_MAX_SIZE];
    //FILE *fp=NULL;
    UINT32 error_flag_main=0,debugm=0;
    INT32 check_flag;
#if AUTO_UPDATE_DIM
	INT32 max_diffmodel_size=0;
#endif

    CHAR path_files[50]="/mnt/sd/AI_auto_test_files/";
    CHAR config_path_files[CONFIG_MAX_SIZE]="/mnt/sd/AI_auto_test_files/paras/config_4.txt";
	CHAR mem_file[100] = "/mnt/sd/AI_auto_test_files/gt_results/memory_max.txt";
	FILE *fp=NULL;

    CHAR command_test[100];
    sprintf(command_test, "rm -rf %s/output/ai_test_4",path_files);
    system(command_test);
    sprintf(command_test, "mkdir %s/output/ai_test_4",path_files);
    system(command_test);


	sprintf(error_file,"%s/output/ai_test_4/error.txt",path_files);


    DBG_DUMP("\r\n");
    DBG_DUMP("ai_test_4 start��\r\n");


    if(argc != 1){
		error_flag_main=1;
		goto eexit;
    }

    para_cnt=load_config_file(config_path_files,key,value,CONFIG_MAX_ITEMS);

    if(para_cnt<=0) {
        error_flag_main=1;
        goto eexit;
    }
    memset(config_para.model_list_1,'\0',CONFIG_MAX_SIZE);
    memset(config_para.image_list_1,'\0',CONFIG_MAX_SIZE);
    memset(stemp,'\0',CONFIG_MAX_SIZE);


    for(idx=0;idx<CORE_MAX_NUM;idx++) {
    
        config_para.core[idx]=0;
    }

    config_para.model_iterations=0;
    config_para.thread_numbers=0;
    config_para.core_num=0;
    if(flow_config_parse_v1(key,value,para_cnt,&config_para) < 0) {
    

        error_flag_main=1;
        goto eexit;
    }
    if(config_para.core_num<=1) {
    
         error_flag_main=1;
         goto eexit;
    }

	if(strcmp(config_para.model_list_1,stemp)!=0) {
		check_flag=check_file(config_para.image_list_1);

        if(check_flag<0)
        {
            if(debugm==1){
				DBG_ERR("%s null!\r\n",config_para.image_list_1);
            }
            error_flag_main=1;
            goto eexit;
        }
	} else {
		error_flag_main=1;
		goto eexit;
	}
		
	if(strcmp(config_para.image_list_1,stemp)!=0) {
    
        check_flag=check_file(config_para.image_list_1);

        if(check_flag<0)
        {
            if(debugm==1){
				DBG_ERR("%s null!\r\n",config_para.image_list_1);
            }
            error_flag_main=1;
            goto eexit;
        }
    } else {
		error_flag_main=1;
        goto eexit;
	}
    for(idx=0;idx<config_para.core_num;idx++) {
    
        if(config_para.core[idx]!=1 && config_para.core[idx]!=2) {
        
           error_flag_main=1;
			goto eexit;
        }     
    }
    debugm=config_para.debug_mode;
	
    thread_num=config_para.thread_numbers;

    if (thread_num==0 || thread_num>NN_RUN_NET_NUM) {
    
        error_flag_main=1;
        if(debugm==1){
			DBG_ERR("wrong thread number %u \r\n",thread_num);
        }
        goto eexit;

    }
	fp=fopen(mem_file,"r");
	if(fp == NULL) {
		if(debugm==1){
			DBG_ERR("open mem_file failed \r\n");
		}
		error_flag_main=1;
		goto eexit;
	}
	if (fscanf(fp,"%ld\n",&maxmodelsize) == EOF) {
        if(debugm==1){
			DBG_ERR("mem error \r\n");
		}
		fclose(fp);
		fp=NULL;
		error_flag_main=1;
		goto eexit;
    }
	if (fscanf(fp,"%ld\n",&temp_mem_size) == EOF) {
        if(debugm==1){
			DBG_ERR("mem error \r\n");
		}
		fclose(fp);
		fp=NULL;
		error_flag_main=1;
		goto eexit;
    }
	fscanf(fp,"%ld\n",&thread_mem);
	fclose(fp);
	fp=NULL;
	//max_src_size = thread_mem-temp_mem_size;
	//max_src_size=max_src_size*5;
	if(maxmodelsize ==0 || thread_mem ==0 || temp_mem_size == 0 ||(thread_mem <= temp_mem_size) || (temp_mem_size <= maxmodelsize))
	{
		if(debugm==1){
			DBG_ERR("mem error \r\n");
		}
		error_flag_main=1;
		goto eexit;
	}
	thread_mem=thread_mem+EXTRA_NET_SIZE;
	for(idx=0;idx<thread_num;idx++)
	{
		mem_size[idx]=thread_mem;
	}

    //ai_sdk_version = vendor_ais_chk_sdk_version();
    //printf("ai_sdk_version: %s\n", ai_sdk_version);

    ret = hd_common_init(0);
	if (ret != HD_OK) {
        error_flag_main=1;
        if(debugm==1){
			DBG_ERR("hd_common_init fail=%d\n", ret);
        }
        goto exit;

	}

	//set project config for AI
	hd_common_sysconfig(0, (1<<16), 0, VENDOR_AI_CFG); //enable AI engine
	//vendor_ai_global_init();
	ret = mem_init();
	if (ret != HD_OK) {
        error_flag_main=1;
        if(debugm==1){
        DBG_ERR("mem_init fail=%d\n", ret);
        }
        goto exit;

	}

	ret = get_mem_block();
	if (ret != HD_OK) {

		error_flag_main=1;
        if(debugm==1){
        DBG_ERR("get_mem_block fail=%d\n", ret);
        }
        goto exit;
	}

	ret = hd_videoproc_init();
	if (ret != HD_OK) {
        error_flag_main=1;
        if(debugm==1){
        DBG_ERR("hd_videoproc_init fail=%d\n", ret);
        }
        goto exit;

	}

    ret = hd_gfx_init();
	if (ret != HD_OK) {
        error_flag_main=1;
        if(debugm==1){
        DBG_ERR("hd_gfx_init fail=%d\n", ret);
        }
        goto exit;

	}


	pthread_mutex_init(&lock, NULL);
    net_parm=( VENDOR_AIS_NETWORK_PARM *)calloc(thread_num,sizeof( VENDOR_AIS_NETWORK_PARM));
    nn_thread_id=(pthread_t *)calloc(thread_num,sizeof(pthread_t));
    scmt_parm=(SCMT_PARM *)calloc(thread_num,sizeof( SCMT_PARM ));




    VENDOR_AIS_FLOW_MEM_PARM local_mem = g_mem;
	//VENDOR_AIS_FLOW_MEM_PARM local_mem_img = g_mem_img;
	models_number=0;
	INT32 temp_size=0;
	temp_size=scmt_find_max_modelsize(config_para.model_list_1,path_files,&models_number,0);
	if(temp_size <= 0 || models_number <=0) {
	
		error_flag_main=1;
		if(debugm==1){
			DBG_ERR("find_max_modelsize error %s\r\n",config_para.model_list_1);
		}
		goto fexit;
	}
	if(config_para.auto_updata_dim_flag == 1) {
	
#if AUTO_UPDATE_DIM
		UINT32 models_num_temp=0;
		temp_size=scmt_find_max_modelsize(config_para.model_list_1,path_files,&models_num_temp,1);
		if(temp_size <= 0 || models_num_temp <=0) {
		
			error_flag_main=1;
			if(debugm==1){
				DBG_ERR("find_max_modelsize error diff %s\r\n",config_para.model_list_1);
			}
			goto fexit;
		}
        if (models_num_temp<models_number) {		
		
			error_flag_main=1;
			if(debugm==1){
				DBG_ERR("find_max_modelsize error diff %s\r\n",config_para.model_list_1);
			}
			goto fexit;
		}
#endif
	}

    for(idx=0;idx<thread_num;idx++) {
    
        
        scmt_parm[idx].core_num=config_para.core_num;

        for(jdx=0;jdx<config_para.core_num;jdx++) {
        
            scmt_parm[idx].core[jdx]=config_para.core[jdx];
        }
		AI_STRCPY(scmt_parm[idx].model_list_path,config_para.model_list_1,CONFIG_MAX_SIZE);
		
        //thread_mem=maxmodelsize+EXTRA_NET_SIZE+max_diffmodel_size+NET_RESULT_MAX_SIZE*3;
        net_parm[idx].mem=scmt_getmem_v1(&local_mem,thread_mem,error_file,&error_flag_main,debugm);
        if(error_flag_main>0)
            goto fexit;

      
        //net_parm[idx].mem_img=scmt_getmem_v1(&local_mem_img,thread_mem,error_file,&error_flag_main,debugm) ;
        // if(error_flag_main>0)
            // goto fexit;
        net_parm[idx].max_model_size=maxmodelsize;
		net_parm[idx].max_diffmodel_size=max_diffmodel_size;
        net_parm[idx].run_id=idx;
        scmt_parm[idx].model_parm=net_parm[idx];
        scmt_parm[idx].models_number=models_number;
		scmt_parm[idx].auto_updata_dim_flag=config_para.auto_updata_dim_flag;
		AI_STRCPY(scmt_parm[idx].img_list_path,config_para.image_list_1,CONFIG_MAX_SIZE);
        scmt_parm[idx].model_iterations=config_para.model_iterations;
		AI_STRCPY(scmt_parm[idx].path_files,path_files,50);
        scmt_parm[idx].result_flag=0;
        scmt_parm[idx].debug_mode=debugm;
        ret=pthread_create(&nn_thread_id[idx],NULL,scmt_thread_api,(VOID*)(&scmt_parm[idx]));

        if (ret < 0) {
            error_flag_main=1;
            if(debugm==1){
            DBG_ERR("create  thread failed =%d \r\n",ret);
            }
            goto fexit;

		}
    }
    for (idx = 0; idx < thread_num; idx++) {
		pthread_join(nn_thread_id[idx], NULL);
	}


fexit:
	for(idx=0;idx<thread_num;idx++) {
    
        error_flag_main += scmt_parm[idx].result_flag;
    }
	free(nn_thread_id);

    free(net_parm);

    free(scmt_parm);
	pthread_mutex_destroy(&lock);
exit:
    ret = hd_gfx_uninit();
	if (ret != HD_OK) {
        if(debugm==1){
        DBG_ERR("hd_gfx_uninit fail=%d \r\n",ret);
        }
        error_flag_main=1;
        goto eexit;

	}

	ret = hd_videoproc_uninit();
	if (ret != HD_OK) {
        if(debugm==1){
        DBG_ERR("hd_videoproc_uninit fail=%d \r\n",ret);
        }
        error_flag_main=1;
        goto eexit;

	}
    ret = release_mem_block();
	if (ret != HD_OK) {
        if(debugm==1){
        DBG_ERR("release_mem_block fail=%d \r\n",ret);
        }
        error_flag_main=1;
        goto eexit;

	}

	ret = mem_uninit();
	if (ret != HD_OK) {
        if(debugm==1){
        DBG_ERR("mem_uninit fail=%d \r\n",ret);
        }
        error_flag_main=1;
        goto eexit;

	}
	// global uninit for ai sdk
	//vendor_ai_global_uninit();

	ret = hd_common_uninit();
	if (ret != HD_OK) {
        if(debugm==1){
        DBG_ERR("hd_common_uninit fail=%d \r\n",ret);
        }
        error_flag_main=1;
        goto eexit;

	}

eexit:
	if (fp!=NULL)
	{
		fclose(fp);
	}
    if(error_flag_main == 0) {
    
       DBG_DUMP("success\r\n");
    } else {
       DBG_ERR("failed\r\n");
    }
    DBG_DUMP("ai_test_4 finished��\r\n");
	return ret;


}
















