/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#define _GNU_SOURCE

#include <stdio.h>
#include<stdlib.h>

#include <string.h>
#include<unistd.h>
#include "hdal.h"
#include "hd_common.h"
#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
#include "net_pre_sample/net_pre_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"

// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(alg_net_app_user_sample, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME



#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080
#define MAX_FRAME_CHANNEL       3
#define MAX_FRAME_BYTE          4
#define KBYTE                   1024

#define MAX_RESULT_SIZE             (200 * KBYTE)
#define AI_SAMPLE_TEST_BATCH    1 // v2 not support
#if AI_SAMPLE_TEST_BATCH
#define AI_SAMPLE_MAX_BATCH_NUM 8
#define AI_SAMPLE_MAX_INPUT_NUM 2
#else
#define AI_SAMPLE_MAX_BATCH_NUM 1
#define AI_SAMPLE_MAX_INPUT_NUM 1
#endif
#define EXTRA_NET_SIZE           (200 * KBYTE)

#define YUV_SINGLE_BUF_SIZE     (3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT * AI_SAMPLE_MAX_BATCH_NUM)
#define YUV_OUT_BUF_SIZE        (YUV_SINGLE_BUF_SIZE * AI_SAMPLE_MAX_INPUT_NUM)
#define IMG_SIZE                (YUV_SINGLE_BUF_SIZE * AI_SAMPLE_MAX_INPUT_NUM*2 + EXTRA_NET_SIZE)



#define MBYTE					(1024 * 1024)
//#define NN_TOTAL_MEM_SIZE       (300 * MBYTE)
UINT32 NN_TOTAL_MEM_SIZE = 0;
#define NN_RUN_NET_NUM			2
#define NN_RUN_THREAD_NUM	    2


#define MAX_OBJ_NUM             1024
#define AUTO_UPDATE_DIM     	0
#define DYNAMIC_MEM             0
#define MODIFY_ENG_IN_SAMPLE    0

#define VENDOR_AI_CFG  0x000f0000  //ai project config

#define GET_OUTPUT_LAYER_FEATURE_MAP DISABLE
#define SUPPORT_MULTI_BLOB_IN_SAMPLE      1
#define EXTRA_NET_SIZE           (200 * KBYTE)
#define M_NET_MAX_BUF_SIZE       (25000 * KBYTE)
#define NN_USE_DRAM2             ENABLE
#define CONFIG_MAX_SIZE          (256)
#define CONFIG_MAX_ITEMS         (10)
#define IMG_MAX_ITEMS            (10)
#define RESULT_COMPARE           ENABLE
#define NET_RESULT_MAX_SIZE      (32 * MBYTE)
#define AI_STRCPY(dst, src, dst_size) do { \
	strncpy(dst, src, (dst_size)-1); \
	dst[(dst_size)-1] = '\0'; \
} while(0)
#define _MAX(a,b) (((a)>(b))?(a):(b))

/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/**
	Parameters of network
*/
typedef struct _SCMT_NETWORK_PARM {
	UINT32 net_id;
	VENDOR_AIS_FLOW_MAP_MEM_PARM net_manager;
} SCMT_NETWORK_PARM;

typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	VENDOR_AIS_IMG_PARM	src_img[AI_SAMPLE_MAX_INPUT_NUM];
	VENDOR_AIS_FLOW_MEM_PARM mem;
    VENDOR_AIS_FLOW_MEM_PARM mem_img;
	UINT32 run_id;
	UINT32 max_model_size;
} VENDOR_AIS_NETWORK_PARM;

typedef struct _SCMT_PARM {
	VENDOR_AIS_NETWORK_PARM model_parm;
	char *model_list_path;
	char img_list_path[IMG_MAX_ITEMS][CONFIG_MAX_SIZE];
	char *path_files;
	UINT32 models_number;
	UINT32 imagelist_num;
	UINT32 model_iterations;
	//UINT32 core;
	UINT32 result_flag;
	UINT32 debug_mode;

}SCMT_PARM;
typedef struct _SCMT_THREAD_INIT_PARM {
	UINT32 net_id;
} SCMT_THREAD_INIT_PARM;

typedef struct _SCMT_FLOW_PARM {
	BOOL    net_lock;
	INT32   init_state;
} SCMT_FLOW_PARM;


typedef enum
{

	SCMT_IMG_MEM_STRUCT = 0,
	SCMT_RESULT_MEM_STRUCT,
	SCMT_NETWORK_PARM_STRUCT,
	ENUM_DUMMY4WORD(SCMT_STRUCT_TYPE)
} SCMT_STRUCT_TYPE;

typedef struct _SCMT_IMG_MEM {
	VENDOR_AIS_FLOW_MEM_PARM src;
} SCMT_IMG_MEM;
typedef struct _SCMT_RESULT_MEM {
	VENDOR_AIS_FLOW_MEM_PARM result;
} SCMT_RESULT_MEM;
typedef struct _CONFIG_PARA {
	char model_list_1[CONFIG_MAX_SIZE];
	char model_list_2[CONFIG_MAX_SIZE];
	char image_list_1[CONFIG_MAX_SIZE];
	char image_list_2[CONFIG_MAX_SIZE];
	//UINT32 core;
	UINT32 model_iterations;
	UINT32 thread_numbers;
	UINT32 debug_mode;
}CONFIG_PARA;

/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
#if DYNAMIC_MEM

static UINT32 mem_size[NN_RUN_NET_NUM] = { 250 * MBYTE, 250 * MBYTE };
#endif

static VENDOR_AIS_FLOW_MEM_PARM g_mem = { 0 };
static VENDOR_AIS_FLOW_MEM_PARM g_mem_img		= {0};

static HD_COMMON_MEM_VB_BLK g_blk_info[2];
static pthread_mutex_t     lock;

#if AUTO_UPDATE_DIM
UINT32 diff_model_size = 0;
#if defined(__FREERTOS)
static CHAR diff_model_name[NN_RUN_NET_NUM][256] = { "A:\\para\\nvt_stripe_model.bin", "A:\\para\\nvt_stripe_model1.bin" };
#else
static CHAR diff_model_name[NN_RUN_NET_NUM][256] = { "para/nvt_stripe_model.bin", "para/nvt_stripe_model1.bin" };
#endif
#endif

//NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/


static int mem_init(void)
{
	HD_RESULT              ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = { 0 };
	UINT32 total_mem_size = 0;


#if DYNAMIC_MEM
	UINT32 i;
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = ALIGN_CEIL_64(NN_TOTAL_MEM_SIZE);
#endif

	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = total_mem_size;
	mem_cfg.pool_info[0].blk_cnt = 1;

#if NN_USE_DRAM2
	mem_cfg.pool_info[0].ddr_id = DDR_ID1;
#else
	mem_cfg.pool_info[0].ddr_id = DDR_ID0;
#endif

    mem_cfg.pool_info[1].type = HD_COMMON_MEM_CNN_POOL;    
	mem_cfg.pool_info[1].blk_size = IMG_SIZE;
	mem_cfg.pool_info[1].blk_cnt = 1;
    mem_cfg.pool_info[1].ddr_id = DDR_ID0;
	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		printf("hd_common_mem_init err: %d\r\n", ret);
	}
	return ret;
}

static INT32 get_mem_block(VOID)
{
	HD_RESULT                 ret = HD_OK;
	UINT32                    pa, va;
	HD_COMMON_MEM_VB_BLK      blk;
	UINT32 total_mem_size = 0;


#if NN_USE_DRAM2
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID1;
#else
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID0;
#endif

#if DYNAMIC_MEM
	UINT32 i;
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif



	/* Allocate parameter buffer */
	if (g_mem.va != 0) {		
		return -1;
	}
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, total_mem_size, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {		
		ret = HD_ERR_NG;
		goto mexit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {		
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, total_mem_size);
	g_blk_info[0] = blk;

	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_mem_size);
		if (ret != HD_OK) {
			//DBG_DUMP("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}
	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = total_mem_size;

    if(g_mem_img.va!=0){
        return -1;
    }
    blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, IMG_SIZE, DDR_ID0);	
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {		
		ret =  HD_ERR_NG;
		goto mexit;
	}
    pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		//DBG_DUMP("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}	
    va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa,  IMG_SIZE);	
	g_blk_info[1] = blk;
    if (va == 0) {		
		ret = hd_common_mem_munmap((void *)va, IMG_SIZE);		
		if (ret != HD_OK) {
			//DBG_DUMP("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}
    g_mem_img.pa = pa;
	g_mem_img.va = va;	
	g_mem_img.size = IMG_SIZE;

mexit:
	return ret;
}

static HD_RESULT release_mem_block(VOID)
{
	HD_RESULT ret = HD_OK;
	UINT32 total_mem_size = 0;

#if DYNAMIC_MEM
	UINT32 i;
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif


	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, total_mem_size);
		if (ret != HD_OK) {
			//DBG_DUMP("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		//DBG_DUMP("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}


    if (g_mem_img.va) {		
		ret = hd_common_mem_munmap((void *)g_mem_img.va, IMG_SIZE);
		if (ret != HD_OK) {
			//DBG_DUMP("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[1]);
	if (ret != HD_OK) {
		//DBG_DUMP("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}
	return ret;
}

static HD_RESULT mem_uninit(void)
{
	return hd_common_mem_uninit();
}

VENDOR_AIS_FLOW_MEM_PARM scmt_getmem(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size)
{
	VENDOR_AIS_FLOW_MEM_PARM mem = { 0 };
	required_size = ALIGN_CEIL_32(required_size);
	if (required_size <= valid_mem->size) {
		mem.va = valid_mem->va;
		mem.pa = valid_mem->pa;
		mem.size = required_size;

		valid_mem->va += required_size;
		valid_mem->pa += required_size;
		valid_mem->size -= required_size;
	}else {
		DBG_ERR("Required size %d > total memory size %d\r\n", required_size, valid_mem->size);
	}
	return mem;
}
VENDOR_AIS_FLOW_MEM_PARM scmt_getmem_v1(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size, CHAR *file, UINT32 *flag, UINT32 debugm)
{
	VENDOR_AIS_FLOW_MEM_PARM mem = { 0 };
	//FILE *fp=NULL;
	required_size = ALIGN_CEIL_32(required_size);
	if (required_size <= valid_mem->size) {
		mem.va = valid_mem->va;
		mem.pa = valid_mem->pa;
		mem.size = required_size;		

		valid_mem->va += required_size;
		valid_mem->pa += required_size;
		valid_mem->size -= required_size;		
	} else {
		if (debugm == 1) {
			DBG_ERR("Required size %d > total memory size %d\r\n", required_size, valid_mem->size);
		}
		*flag = 1;
	}
	return mem;
}


VENDOR_AIS_FLOW_MEM_PARM scmt_getmem_v2(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size, UINT32 run_id, CHAR *file, UINT32 *flag, UINT32 debugm)
{
	VENDOR_AIS_FLOW_MEM_PARM mem = { 0 };
	required_size = ALIGN_CEIL_32(required_size);
	if (required_size <= valid_mem->size) {
		mem.va = valid_mem->va;
		mem.pa = valid_mem->pa;
		mem.size = required_size;
		valid_mem->va += required_size;
		valid_mem->pa += required_size;
		valid_mem->size -= required_size;	
	} else {
		if (debugm == 1){
			DBG_ERR("pthread %u Required size %d > total memory size %d\r\n", run_id, required_size, valid_mem->size);
		}
		*flag = 1;
	}
	return mem;
}


HD_RESULT scmt_clearmem(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size)
{

	required_size = ALIGN_CEIL_32(required_size);
	valid_mem->va -= required_size;
	valid_mem->pa -= required_size;
	valid_mem->size += required_size;

	return HD_OK;
}

VENDOR_AIS_FLOW_MEM_PARM scmt_va_align(VENDOR_AIS_FLOW_MEM_PARM buf)
{
	VENDOR_AIS_FLOW_MEM_PARM align_buf;
	UINT32 align_size;

	//align_buf.va = ALIGN_CEIL_16(buf.va);
	align_buf.va = ALIGN_CEIL_32(buf.va);

	align_size = align_buf.va - buf.va;
	align_buf.pa = buf.pa + align_size;
	align_buf.size = buf.size - align_size;

	return align_buf;
}

static INT32 split_kvalue(CHAR *p_str, char **key, char **value, CHAR c, UINT32 max_len)
{
 	UINT32 cnt = 0;
	UINT32 len = 0;
	
	*key = p_str;
	
	while (*p_str != '\0') {
		if (*p_str == c) {
			*p_str = '\0';			
			p_str++;
			len++;
			cnt++;		
			
			if (cnt > 1) {
				DBG_ERR("can only have one = in config line!");
				return -1;
			}
			
			while (*p_str == ' ') {
				p_str++;
				len++;				
				if (len > max_len) {
					DBG_ERR("the config is too long!\r\n");
					return -1;
				}
			}			
			*value = p_str;					
		} else {
			p_str++;	
			len++;	
			if (len > max_len) {
				DBG_ERR("the config is too long!\r\n");
				return -1;
			}
		}
					
	}
	
	if (cnt == 0) {
		DBG_ERR("can not find = !\r\n");
		return -1;		
	}		
	return 1;
}

static INT32 remove_blank(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	CHAR *p_str1 = p_str;

	while (*p_str0 != '\0'){
		if ((*p_str0 != ' ') && (*p_str0 != '\t') && (*p_str0 != '\n') && (*p_str0 != '\r')) {
			*p_str1++ = *p_str0++;
		}
		else {
			p_str0++;
		}			
	}
	*p_str1 = '\0';
	return 0;
}

static INT32 load_config_file(CHAR *file_path, CHAR key[][CONFIG_MAX_SIZE], CHAR value[][CONFIG_MAX_SIZE], UINT max_items)
{
	FILE *fp;
	if ((fp = fopen(file_path, "r")) == NULL){
		//DBG_ERR("fopen file path %s error!\r\n", file_path);
		return -1;
	}

	CHAR line[CONFIG_MAX_SIZE * 2];
	UINT32 cnt = 0;
	CHAR * temp_key = NULL;
	CHAR * temp_value = NULL;
	while (1){
		if (feof(fp)) {
			break;
		}
		if (fgets(line, CONFIG_MAX_SIZE * 2 - 1, fp) == NULL){
			if (feof(fp)) {
				break;
			}
			continue;
		}
		remove_blank(line);
		if (line[0] == '#' || line[0] == '\0')
			continue;
		
		if (split_kvalue(line, &temp_key, &temp_value, '=', CONFIG_MAX_SIZE)){	
			AI_STRCPY(key[cnt], temp_key, CONFIG_MAX_SIZE);
			AI_STRCPY(value[cnt], temp_value, CONFIG_MAX_SIZE);				
		    temp_key = NULL;
			temp_value = NULL;
		} else {
			continue;
		}
		cnt = cnt + 1;
		if (cnt > max_items)
		{
			DBG_ERR("pauser max_items is too larger %d\r\n",cnt);
		}
	}

	if(fp){
		fclose(fp);
		fp= NULL ;
	}

	return cnt;
}
static INT32 flow_config_parse(CHAR key[][CONFIG_MAX_SIZE], CHAR value[][CONFIG_MAX_SIZE], UINT32 cnt, CONFIG_PARA *cpara)
{
	UINT32 idx;	
	for (idx = 0; idx < cnt; idx++)	{
		
		if (strcmp(key[idx], "[debug_mode]") == 0)
		{

			cpara->debug_mode = atoi(value[idx]);
		}
		if (strcmp(key[idx], "[path/model_list_1]") == 0)
		{

			AI_STRCPY(cpara->model_list_1, value[idx],sizeof(cpara->model_list_1));
		}
		else if (strcmp(key[idx], "[path/model_list_2]") == 0)
		{
			AI_STRCPY(cpara->model_list_2, value[idx],sizeof(cpara->model_list_2));
		}
		else if (strcmp(key[idx], "[path/image_list_1]") == 0)
		{
			AI_STRCPY(cpara->image_list_1, value[idx],sizeof(cpara->image_list_1));
		}
		else if (strcmp(key[idx], "[path/image_list_2]") == 0)
		{
			AI_STRCPY(cpara->image_list_2, value[idx],sizeof(cpara->image_list_2));
			
		}
		/*else if (strcmp(key[idx], "[core]") == 0)
		{
			cpara->core = atoi(value[idx]);
		}*/
		else if (strcmp(key[idx], "[model_iterations]") == 0)
		{
			cpara->model_iterations = atoi(value[idx]);
		}
		else if (strcmp(key[idx], "[thread_numbers]") == 0)
		{
			cpara->thread_numbers = atoi(value[idx]);
		}


	}
	return 0;

}

#if !RESULT_COMPARE
static UINT32 ai_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;
	//DBG_DUMP("model addr = %08x\r\n", (int)model_addr);

	fd = fopen(p_filename, "rb");
	if (!fd) {
		//printf("cannot read %s\r\n", p_filename);
		return 0;
	}

	fseek(fd, 0, SEEK_END);
	file_size = ALIGN_CEIL_4(ftell(fd));
	fseek(fd, 0, SEEK_SET);

	read_size = fread((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
        return 0;
	}

	if(fd){
		fclose(fd);
		fd = NULL ;
	}
	return read_size;
}
#endif


VENDOR_AIS_FLOW_MEM_PARM scmt_getmem2(VENDOR_AIS_FLOW_MEM_PARM *p_total_mem, UINT32 required_size, UINT32 align_size)
{
	VENDOR_AIS_FLOW_MEM_PARM mem = { 0 };
	if (p_total_mem == NULL) {
		DBG_ERR("mem ptr is null\r\n");
		return mem;
	}

	if ((align_size & (align_size - 1)) != 0) {
		DBG_ERR("do not support align size = %d, use 4 instead\r\n", align_size);
		align_size = 4;
	}

	UINT32 va = ALIGN_CEIL(p_total_mem->va, align_size);
	UINT32 pa = ALIGN_CEIL(p_total_mem->pa, align_size);
	UINT32 sz = p_total_mem->size - (pa - p_total_mem->pa);

	if (required_size <= sz) {
		mem.va = va;
		mem.pa = pa;
		mem.size = required_size;

		p_total_mem->va = va + required_size;
		p_total_mem->pa = pa + required_size;
		p_total_mem->size = sz - required_size;
	}else {
		DBG_ERR("required size %d > total memory size %d\r\n", required_size, sz);
	}
	return mem;
}


static UINT32 scmt_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;
	fd = fopen(p_filename, "rb");
	if (!fd) {
	   fclose(fd);	
	   fd = NULL;
	   return 0;
	}

	fseek(fd, 0, SEEK_END);
	file_size = ALIGN_CEIL_4(ftell(fd));
	file_size = ftell(fd);
	fseek(fd, 0, SEEK_SET);

	read_size = fread((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size ) {		
		if(fd){
			fclose(fd);
			fd = NULL;
		}		
		return -1;
	}
	if(fd){
		fclose(fd);
		fd = NULL;
	}
	return read_size;
}


#if RESULT_COMPARE

static UINT32 result_compare(UINT32 va, UINT32 gtsize, UINT8* data, UINT32 size)
{

	UINT32 idx;
	UINT8 *gt = (UINT8 *)va;
	if (gtsize < size) {

		return -1;
	}
	for (idx = 0; idx < size; idx++) {

		if (((*(gt++)) - (*(data++))) != 0) {
			return -1;
		}
	}

	return 0;
}

#endif


static BOOL find_semicolon(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	while (*p_str0 != '\0'){
		if (*p_str0 == ';') {
			return TRUE;
		}
		else {
			p_str0++;
		}			
	}
	*p_str0 = '\0';
	return FALSE;
}


int spilt_fun(char *line, char c, char out[][256])
{
	//char *p_line = line;
	char *a, *b;
	a = b = line;
	int ii = 0;
	while (1){
		if (*b == '\0'){
			break;
		}
		if (*b == c){
			memcpy(out[ii], a, b - a);
			out[ii][b - a] = '\0';
			ii++;
			a = b = b + 1;
			continue;
		}
		b++;
	}
	memcpy(out[ii], a, b - a);
	out[ii][b - a] = '\0';
	return 0;
}



static VOID *scmt_thread_api(VOID *arg)
{
	UINT32 count = 0;
	UINT32 counti = 0;
	HD_RESULT ret;
	SCMT_PARM *p_scmt_parm = (SCMT_PARM *)arg;
	VENDOR_AIS_NETWORK_PARM *p_net_parm = &p_scmt_parm->model_parm;
	VENDOR_AIS_IMG_PARM *p_src_img = p_net_parm->src_img;
	CHAR path_files[50];
	CHAR line[512];
	VENDOR_AIS_FLOW_MEM_PARM output_result_mem;

	CHAR modellist_path[CONFIG_MAX_SIZE];
	CHAR imglist_path[2][CONFIG_MAX_SIZE];

	CHAR name[50] = { 0 };
	CHAR mname[50] = { 0 };
	CHAR model_path[CONFIG_MAX_SIZE] = { 0 };

	CHAR imgpath[CONFIG_MAX_SIZE] = { 0 };
	CHAR fmt[50] = { 0 };
	UINT32 gt_size = 0;

	UINT32 model_max = p_net_parm->max_model_size;


	VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem = &p_net_parm->mem;

	VENDOR_AIS_FLOW_MEM_PARM rslt_new_mem;
   // VENDOR_AIS_FLOW_MEM_PARM tem_mem_img ;
	VENDOR_AIS_FLOW_MAP_MEM_PARM mem_manager;

	UINT32 debugm = p_scmt_parm->debug_mode, run_id = p_net_parm->run_id;
	UINT32 net_id = run_id;
	UINT32  model_size = 0;
	INT32 file_size = 0;
	UINT32 io_num_m=0;	
	UINT32 *p_input_blob_info_m=NULL;
	NN_DATA in_layer_mem_m={0};
	UINT32 idx = 0, jdx = 0, kdx = 0;	
	UINT32 bit_num;
	UINT32 blob_num;
	CHAR blob_model_name[50];

#if !CNN_25_MATLAB
	NN_IN_OUT_FMT *input_info = NULL;
#endif

	//UINT32 req_size = 0;
	UINT32 iterations, model_num;
	FILE *fp = NULL, *fpp = NULL;
	FILE *cfp = NULL;
	CHAR error_file[CONFIG_MAX_SIZE];
#if AI_SAMPLE_TEST_BATCH
	VENDOR_AIS_IMG_PARM *pb_src_img = NULL;
	UINT32 input_proc_idx[AI_SAMPLE_MAX_BATCH_NUM];
	UINT32 tmp_src_va;
	UINT32 tmp_src_pa;
	UINT32 src_img_size;
	UINT32 batch_i;
	UINT32 input_proc_num = 0;
	UINT32 mem_size =0;
#endif
	//compare result
#if RESULT_COMPARE
	CHAR img_save_path[CONFIG_MAX_SIZE];
	NN_DATA output_layer_iomem = { 0 };
	CHAR cname[50] = { 0 };
	NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
	UINT32 num_output_layer = 0, out_temp_va = 0;
	CHAR *imgsplit_name;
	FILE *gfp = NULL;
	VENDOR_AIS_FLOW_MEM_PARM gt_mem;

#endif
	p_scmt_parm->result_flag = 0;
	model_num = p_scmt_parm->models_number;
	iterations = p_scmt_parm->model_iterations;
	if (debugm == 1) {
		DBG_WRN("pthread %d:  model_iterations:%u\r\n", (INT)net_id, iterations);
	}
	AI_STRCPY(path_files, p_scmt_parm->path_files,sizeof(path_files));
	sprintf(error_file, "%s/output/ai_test_6/error_%d.txt", path_files, (int)net_id);

	AI_STRCPY(modellist_path, p_scmt_parm->model_list_path,sizeof(modellist_path));
	for (idx = 0; idx < p_scmt_parm->imagelist_num; idx++){
		AI_STRCPY(imglist_path[idx], p_scmt_parm->img_list_path[idx],sizeof(imglist_path[idx]));
	}
	fp = fopen(modellist_path, "r");
	if (fp == NULL){
		p_scmt_parm->result_flag = 1;
		if (debugm == 1) {
			DBG_ERR("pthread %u : %s NULL!\r\n", net_id, modellist_path);
		}
		goto texit;
	}
	
	VENDOR_AIS_FLOW_MEM_PARM model_mem_start;
	model_mem_start.va= p_tot_mem->va;
	model_mem_start.pa= p_tot_mem->pa;
	
	
	for (idx = 0; idx < model_num; idx++) {		
				if(fscanf(fp,"%s\n",mname) == EOF) {
             break;
        }
		sprintf(model_path, "%s/model_bins/%s/sdk/nvt_model.bin", path_files, mname);
		mem_size = 0;
		ret = vendor_ais_get_model_mem_sz(&mem_size, model_path);
		if (ret != HD_OK) {
				DBG_ERR("vendor_ais_get_model_mem_sz fail: %s\r\n", model_path);
				
		}
		if (debugm) {
			DBG_DUMP("vendor_ais_get_model_mem_sz: %d\r\n", mem_size);
		}
		model_mem_start.size= mem_size ;	
		
#if RESULT_COMPARE
		file_size = vendor_ais_load_file(model_path, model_mem_start.va, &output_layer_info, &num_output_layer);
#else
		file_size = ai_load_file(model_path, model_mem_start.va);		
		//model_file_size = file_size;
#endif
		if (file_size == 0) {
			if (cfp == NULL){
				cfp = fopen(error_file, "w+");
				if (cfp == NULL){
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("error out file: %s  null\r\n", error_file);
					}
				}else{
					p_scmt_parm->result_flag = 1;
					fprintf(cfp, "%s NULL NULL 1\r\n", mname);
				}
			}else{
				p_scmt_parm->result_flag = 1;
				fprintf(cfp, "%s NULL NULL 1\r\n", mname);
			}
			continue;
		}

		model_size = vendor_ais_auto_alloc_mem(&model_mem_start, &mem_manager);
		if (model_size > model_max) {
			if (cfp == NULL) {
				cfp = fopen(error_file, "w+");
				if (cfp == NULL){
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("error out file: %s  null\r\n", error_file);
					}
				}else{
					p_scmt_parm->result_flag = 1;
					fprintf(cfp, "%s NULL NULL 1\r\n", mname);
				}
			}else{
				p_scmt_parm->result_flag = 1;
				fprintf(cfp, "%s NULL NULL 1\r\n", mname);
			}
			continue;
		}

		memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
		memset((VOID *)rslt_new_mem.va, 0, rslt_new_mem.size);    // clear result buffer	
		pthread_mutex_lock(&lock);
		ret = vendor_ais_net_gen_init(mem_manager, net_id);
		pthread_mutex_unlock(&lock);

		if (ret != HD_OK) {
			if (cfp == NULL){
				cfp = fopen(error_file, "w+");
				if (cfp == NULL){
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("error out file: %s  null\r\n", error_file);
					}
				}else{
					p_scmt_parm->result_flag = 1;
					fprintf(cfp, "%sNULL NULL 1\r\n", mname);
				}
			}else{
				p_scmt_parm->result_flag = 1;
				fprintf(cfp, "%s NULL NULL 1\r\n", mname);
			}
			goto gen_init_fail;
		}

		count = count + 1;

#if !CNN_25_MATLAB
		ret = vendor_ais_get_net_input_info(&input_info, model_mem_start);
		if (ret != 0) {
			if (cfp == NULL){
				cfp = fopen(error_file, "w+");
				if (cfp == NULL){
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("error out file: %s  null\r\n", error_file);
					}
				}else{
					p_scmt_parm->result_flag = 1;
					fprintf(cfp, "%s NULL NULL 1\r\n", mname);
				}
			}else{
				p_scmt_parm->result_flag = 1;
				fprintf(cfp, "%s NULL NULL 1\r\n", mname);
			}
			goto gen_init_fail;
		}

#endif
		blob_num=0;			
		INT16 retm=0;
		for(UINT16 i_m=0;i_m<AI_SAMPLE_MAX_INPUT_NUM;i_m++) {
			retm=vendor_ais_net_get_input_blob_info(model_mem_start, i_m, &io_num_m, &p_input_blob_info_m);
			if( retm!= HD_OK) {
				break;
			} else {
				blob_num = blob_num + 1;
				//printf("retm :%d\r\n",retm);
			}
		}
		if (blob_num != 1 && blob_num != 2){
			continue;
		}

		vendor_ais_get_input_layer_mem(model_mem_start, &in_layer_mem_m, 0);			
		bit_num= (INT16)in_layer_mem_m.fmt.int_bits+(INT16)in_layer_mem_m.fmt.frac_bits+(INT16)in_layer_mem_m.fmt.sign_bits;
		vendor_ais_pars_engid(0, net_id);

		VENDOR_AIS_FLOW_MEM_PARM image_mem ;
		image_mem.va = p_tot_mem->va + mem_size;
		image_mem.pa = p_tot_mem->pa + mem_size;
		
		
		UINT32 image_size = 0;
		for (kdx = 0; kdx < p_scmt_parm->imagelist_num; kdx++){
			if ((fpp = fopen(imglist_path[kdx], "r")) == NULL){
				if (cfp == NULL){
					cfp = fopen(error_file, "w+");
					if (cfp == NULL){
						p_scmt_parm->result_flag = 1;
						if (debugm == 1) {
							DBG_ERR("error out file: %s  null\r\n", error_file);
						}
					}else{
						p_scmt_parm->result_flag = 1;
						fprintf(cfp, "%s %s NULL 2!\r\n", mname,  imglist_path[kdx]);
					}
				}else{
					p_scmt_parm->result_flag = 1;
					fprintf(cfp, "%s %s NULL 2!\r\n", mname,  imglist_path[kdx]);
				}
				continue;
			}
			while (1){
				if (feof(fpp)) {
					break;
				}	
				if (fgets(line, 512 - 1, fpp) != NULL) {
					if (line[strlen(line) - 1] == '\n') {
						line[strlen(line) - 1] = '\0';
					}
					if (blob_num == 1) {
						if (find_semicolon(line)) {
							continue;
						}
						char img_info[10][256];
						char cc = ' ';
						spilt_fun(line, cc, img_info);
						//strcpy(name, img_info[0]);
						AI_STRCPY(name, img_info[0],sizeof(name));
						p_src_img[0].width = atoi(img_info[1]);
						p_src_img[0].height = atoi(img_info[2]);
						p_src_img[0].channel = atoi(img_info[3]);
						p_src_img[0].fmt_type = atoi(img_info[4]);
						//strcpy(fmt, img_info[5]);
						AI_STRCPY(fmt, img_info[5],sizeof(fmt));
						if (p_src_img[0].width <= 0 || p_src_img[0].width > MAX_FRAME_WIDTH) {
							continue;
						}
						if (p_src_img[0].height <= 0 || p_src_img[0].height > MAX_FRAME_HEIGHT) {
							continue;
						}
						if (p_src_img[0].channel <= 0 || p_src_img[0].channel > MAX_FRAME_CHANNEL) {
							continue;
						}
						if (p_src_img[0].fmt_type <= 0 || p_src_img[0].fmt_type > 32 || p_src_img[0].fmt_type % 8 != 0) {
							continue;
						}

						if (strcmp(fmt, "HD_VIDEO_PXLFMT_YUV420") == 0) {
							p_src_img[0].fmt = 0x520c0420;
						}else if (strcmp(fmt, "HD_VIDEO_PXLFMT_Y8") == 0) {
							p_src_img[0].fmt = 0x51080400;
						}else if (strcmp(fmt, "HD_VIDEO_PXLFMT_RGB888_PLANAR") == 0) {
							p_src_img[0].fmt = 0x23180888;
						}else {
							continue;
						}
						if ((p_src_img[0].channel != input_info->in_channel) || (p_src_img[0].height < input_info->model_height) || (p_src_img[0].width < input_info->model_width) || p_src_img[0].fmt_type != bit_num) {
							continue;
						}
						sprintf(imgpath, "%s/test_images/%s", path_files, name);
						file_size = scmt_load_file(imgpath, image_mem.va);
						if (file_size <= 0){
							if (cfp == NULL){
								cfp = fopen(error_file, "w+");
								if (cfp == NULL){
									p_scmt_parm->result_flag = 1;
									if (debugm == 1) {
										DBG_ERR("error out file: %s  null\r\n", error_file);
									}
								}else{
									p_scmt_parm->result_flag = 1;
									fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);
									if (debugm == 1) {
										fprintf(cfp, "file size:%d\r\n", file_size);
									}
								}
							}else{
								p_scmt_parm->result_flag = 1;
								fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);
								if (debugm == 1) {
									fprintf(cfp, "file size:%d\r\n", file_size);
								}
							}
							continue;
						}

						p_src_img[0].va = image_mem.va;
						p_src_img[0].pa = image_mem.pa;
						p_src_img[0].line_ofs = p_src_img[0].width*p_src_img[0].fmt_type / 8;
						
						
						
#if AI_SAMPLE_TEST_BATCH

						pb_src_img = &p_src_img[0];

						tmp_src_va = pb_src_img->va;
						tmp_src_pa = pb_src_img->pa;
						src_img_size = pb_src_img->line_ofs * pb_src_img->height * pb_src_img->channel;
						input_proc_num = vendor_ais_net_get_input_layer_index(model_mem_start, input_proc_idx);
						
						image_size = p_src_img[0].width * p_src_img[0].height * _MAX(3, p_src_img[0].channel) * (p_src_img[0].fmt_type/ 8) * input_proc_num;
								
					
						image_size = ALIGN_CEIL_64(image_size);			
						
						if (debugm == 1) {
							DBG_WRN("pb_src_img=====\r\n");
							DBG_WRN("pb_src_img.va:%ld\r\n", pb_src_img->va);
							DBG_WRN("input_proc_num:%u", input_proc_num);
						}
						for (batch_i = 0; batch_i < input_proc_num; batch_i++){
							if (batch_i == 0) {
								hd_common_mem_flush_cache((VOID *)(pb_src_img->va), src_img_size);
							}
							if (batch_i > 0){
								memcpy((VOID *)pb_src_img->va + src_img_size, (VOID *)pb_src_img->va, src_img_size);
								hd_common_mem_flush_cache((VOID *)(pb_src_img->va + src_img_size), src_img_size);
								pb_src_img->pa += src_img_size;
								pb_src_img->va += src_img_size;
							}
							if (vendor_ais_net_input_layer_init(pb_src_img, batch_i, net_id) != HD_OK){
								//printf("error=========vendor_ais_net_input_layer_init ==========\n\r");
								if (cfp == NULL){
									cfp = fopen(error_file, "w+");
									if (cfp == NULL){
										p_scmt_parm->result_flag = 1;
										if (debugm == 1) {
											DBG_ERR("error out file: %s  null\r\n", error_file);
										}
									} else {
										p_scmt_parm->result_flag = 1;
										fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);
									}
								} else {
									p_scmt_parm->result_flag = 1;
									fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);
								}
								goto input_init_fail;
							}
						}
						pb_src_img->va = tmp_src_va;
						pb_src_img->pa = tmp_src_pa;
#else
						if (vendor_ais_net_input_init(&p_src_img[0], net_id) != HD_OK) {

							if (cfp == NULL)
							{
								cfp = fopen(error_file, "w+");
								if (cfp == NULL)
								{
									p_scmt_parm->result_flag = 1;
									if (debugm == 1) {
										DBG_ERR("error out file: %s  null\r\n", error_file);
									}

								}
								else
								{
									p_scmt_parm->result_flag = 1;
									fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);

								}
							}
							else
							{
								p_scmt_parm->result_flag = 1;
								fprintf(cfp, "%s %s NULL 2!\r\n", mname, name);

							}
							goto input_init_fail;
						}
#endif
					}

					if (blob_num == 2) {
						if (!find_semicolon(line)) {
							continue;
						}
						char double_modelname[3][256];
						char c = ';';
						spilt_fun(line, c, double_modelname);
						//strcpy(blob_model_name, double_modelname[0]);
						AI_STRCPY(blob_model_name, double_modelname[0],sizeof(blob_model_name));


						if (strcmp(blob_model_name, mname) == 0) {
							char img_lsiit1[10][256];
							char img_lsiit2[10][256];
							c = ' ';
							spilt_fun(double_modelname[1], c, img_lsiit1);
							spilt_fun(double_modelname[2], c, img_lsiit2);

							//strcpy(name, img_lsiit1[0]);
							AI_STRCPY(name, img_lsiit1[0],sizeof(name));
							
							p_src_img[0].width = atoi(img_lsiit1[1]);
							p_src_img[0].height = atoi(img_lsiit1[2]);
							p_src_img[0].channel = atoi(img_lsiit1[3]);
							p_src_img[0].batch_num = atoi(img_lsiit1[4]);
							p_src_img[0].line_ofs = atoi(img_lsiit1[5]);
							p_src_img[0].channel_ofs = atoi(img_lsiit1[6]);
							p_src_img[0].batch_ofs = atoi(img_lsiit1[7]);
							if (strcmp(img_lsiit1[8], "0x520c0420") == 0) {
								p_src_img[0].fmt = 0x520c0420;
							} else if (strcmp(img_lsiit1[8], "0x51080400") == 0) {
								p_src_img[0].fmt = 0x51080400;
							} else if (strcmp(img_lsiit1[8], "0x23180888") == 0) {
								p_src_img[0].fmt = 0x23180888;
							} else {
								continue;
							}

							p_src_img[1].width = atoi(img_lsiit1[1]);
							p_src_img[1].height = atoi(img_lsiit2[2]);
							p_src_img[1].channel = atoi(img_lsiit2[3]);
							p_src_img[1].batch_num = atoi(img_lsiit2[4]);
							p_src_img[1].line_ofs = atoi(img_lsiit2[5]);
							p_src_img[1].channel_ofs = atoi(img_lsiit2[6]);
							p_src_img[1].batch_ofs = atoi(img_lsiit2[7]);
							if (strcmp(img_lsiit2[8], "0x520c0420") == 0) {
								p_src_img[1].fmt = 0x520c0420;
							} else if (strcmp(img_lsiit2[8], "0x51080400") == 0) {
								p_src_img[1].fmt = 0x51080400;
							} else if (strcmp(img_lsiit2[8], "0x23180888") == 0) {
								p_src_img[1].fmt = 0x23180888;
							} else {
								continue;
							}

							sprintf(imgpath, "%s/test_images/%s", path_files, img_lsiit1[0]);
							file_size = scmt_load_file(imgpath, image_mem.va);

							p_src_img[0].va = image_mem.va;
							p_src_img[0].pa = image_mem.pa;		
							retm=vendor_ais_net_get_input_blob_info(model_mem_start, 0, &io_num_m, &p_input_blob_info_m);
			
							image_size =  p_src_img[0].width * p_src_img[0].height * _MAX(3,  p_src_img[0].channel) * (p_src_img[0].batch_num / 8) * io_num_m;
                            image_size = ALIGN_CEIL_64(image_size);							
							image_mem.va = p_tot_mem->va + mem_size + image_size;
							image_mem.pa = p_tot_mem->pa + mem_size + image_size;
							//image_mem.size = image_mem.size + image_size;
							sprintf(imgpath, "%s/test_images/%s", path_files, img_lsiit2[0]);
							file_size = scmt_load_file(imgpath, image_mem.va);	
						
						    p_src_img[1].va = image_mem.va;
							p_src_img[1].pa = image_mem.pa;
							retm=vendor_ais_net_get_input_blob_info(model_mem_start, 1, &io_num_m, &p_input_blob_info_m);
							
							
							// io_num_m
							image_size =  p_src_img[1].width * p_src_img[1].height * _MAX(3,  p_src_img[1].channel) * (p_src_img[1].batch_num / 8) * io_num_m;	
							
							image_size = ALIGN_CEIL_64(image_size);

							UINT32 j = 0;
							UINT32 io_num = 0;
							UINT32* p_input_blob_info = NULL;

							for (UINT i = 0; i < 2; i++) {
								if (vendor_ais_net_get_input_blob_info(model_mem_start, i, &io_num, &p_input_blob_info) != HD_OK) {
									goto gen_init_fail;
								}
								for (j = 0; j < io_num; j++) {
									UINT32 proc_idx = p_input_blob_info[j] >> 8;
									UINT32 tmp_va = p_src_img[i].va;
									UINT32 tmp_pa = p_src_img[i].pa;
									p_src_img[i].va += (j*p_src_img[i].batch_ofs);
									p_src_img[i].pa += (j*p_src_img[i].batch_ofs);
                                    hd_common_mem_flush_cache((VOID *)(p_src_img[i].va), p_src_img[i].batch_ofs);
									if (vendor_ais_net_input_layer_init(&p_src_img[i], proc_idx, net_id) != HD_OK) {
										goto gen_init_fail;
									}
									p_src_img[i].va = tmp_va;
									p_src_img[i].pa = tmp_pa;
								}
							}

						} else {
							continue;
						}
					}
				} else {
					continue;
				}
				counti = counti + 1;
				if (debugm == 1) {
					DBG_WRN("pthread %d: iter:%s  \r\n", (INT)net_id, mname);
				}
#if RESULT_COMPARE
				//strcpy(cname, name);
				AI_STRCPY(cname, name,sizeof(cname));
				imgsplit_name = strtok(name, ".");

				sprintf(img_save_path, "%s/gt_results/%s/%s/outfeature.bin", path_files, mname, imgsplit_name);


				gfp = fopen(img_save_path, "rb");
				if (gfp == NULL){
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("pthread:%u %s not found!\r\n", net_id, img_save_path);
					}
					goto input_init_fail;
				}


				if(gfp){
					fclose(gfp);
					gfp = NULL ;
				}
				
				gt_mem.va = image_mem.va + image_size;
				gt_mem.pa = image_mem.pa + image_size;
				

				gt_size = scmt_load_file(img_save_path, gt_mem.va);
				gt_size = ALIGN_CEIL_64(gt_size);	
				
		
				if (gt_size <= 0){
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("pthread:%u %s NULL!\r\n", net_id, img_save_path);
						DBG_ERR("file size:%d \r\n", gt_size);
					}
					goto input_init_fail;
				}

#endif
				for (jdx = 0; jdx < iterations; jdx++) {
					memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
					hd_common_mem_flush_cache((VOID *)mem_manager.user_buff.va, mem_manager.user_buff.size);

					if (debugm == 1) {
						DBG_WRN("pthread %d: iter:%u  \r\n", (INT)net_id, jdx);
					}

#if USE_NEON

					vendor_ais_proc_net(model_mem_start, rslt_new_mem, &p_src_img[0], net_id);
#else
					vendor_ais_proc_net(model_mem_start, rslt_new_mem, net_id);
#endif



#if RESULT_COMPARE
                output_result_mem.va = gt_mem.va + gt_size;			
				
				UINT32 output_size = 0;
				out_temp_va = output_result_mem.va;
				

				INT8 Vva;
				for (UINT32 l = 0; l < num_output_layer; l++) {

					ret = vendor_ais_get_layer_mem_v2(model_mem_start, &output_layer_iomem, &output_layer_info[l]);

					if (ret == HD_OK) {
						if (output_layer_iomem.va > 0 && output_layer_iomem.size > 0) {
							hd_common_mem_flush_cache((VOID *)output_layer_iomem.va, output_layer_iomem.size);
						} else {
							if (cfp  == NULL) {
								cfp  = fopen(error_file, "w+");
								if (cfp  == NULL) {

									p_scmt_parm->result_flag = 1;
									DBG_ERR("error file is null.\r\n");

								} else {
									p_scmt_parm->result_flag = 1;
									fprintf(cfp , "%s  %s  %d  3\r\n", mname, cname, (INT)jdx);
								}
							} else {
								p_scmt_parm->result_flag = 1;
								fprintf(cfp , "%s  %s  %d  3\r\n", mname, cname, (INT)jdx);

							}
							goto input_init_fail;
						}
						output_size += output_layer_iomem.size;
						memcpy((VOID *)out_temp_va, (VOID *)output_layer_iomem.va, output_layer_iomem.size);
						out_temp_va = out_temp_va + output_layer_iomem.size;
					} else {
						if (cfp  == NULL) {
							cfp  = fopen(error_file, "w+");
							if (cfp  == NULL) {

								p_scmt_parm->result_flag = 1;
								DBG_ERR("error out file: %s  null\r\n", error_file);

							} else {
								p_scmt_parm->result_flag = 1;
								fprintf(cfp , "%s  %s  %d  3\r\n", mname, cname, (INT)jdx);


							}
						} else {
							p_scmt_parm->result_flag = 1;
							fprintf(cfp , "%s  %s  %d  3\r\n", mname, cname, (INT)jdx);
						}
						goto input_init_fail;
					}
				}
			
				Vva = result_compare(gt_mem.va, gt_size, (UINT8 *)output_result_mem.va, output_size);
				if (Vva < 0) {
					if (cfp  == NULL) {
						cfp  = fopen(error_file, "w+");
						if (cfp  == NULL) {
							p_scmt_parm->result_flag = 1;
							DBG_ERR("error file is null\r\n");
						} else {
							p_scmt_parm->result_flag = 1;
							fprintf(cfp , "%s  %s %d  4\r\n", mname , cname, (INT)jdx);

						}
					} else {
						p_scmt_parm->result_flag = 1;
						fprintf(cfp , "%s %s  %d  4\r\n", mname , cname, (INT)jdx);
					}
					if (debugm == 1) {
						DBG_DUMP("different found in thread: %d, model: %s, img: %s, iteration: %d!!!\r\n", (int)run_id, mname, imgsplit_name, (int)jdx);
					}
					break;
				}

#endif


				}
			input_init_fail:
#if AI_SAMPLE_TEST_BATCH
				if (blob_num == 1) {
					for (batch_i = 0; batch_i < input_proc_num; batch_i++) {
						ret = vendor_ais_net_input_layer_uninit(batch_i, net_id);
						if (ret != HD_OK) {
							p_scmt_parm->result_flag = 1;
							if (debugm == 1) {
								DBG_ERR("pethread %u: %s %s vendor_ais_net_input_uninit fail: %d\r\n", net_id, mname, name, ret);
							}
							goto gen_init_fail;
						}

					}
				}


				else if (blob_num == 2) {
					UINT32 j = 0;
					UINT32 io_num = 0;
					UINT32* p_input_blob_info = NULL;

					for (UINT32 i = 0; i < 2; i++) {
						if (vendor_ais_net_get_input_blob_info(model_mem_start, i, &io_num, &p_input_blob_info) != HD_OK) {
							goto gen_init_fail;
						}
						for (j = 0; j < io_num; j++) {
							UINT32 proc_idx = p_input_blob_info[j] >> 8;

							ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
							if (ret != HD_OK) {
								printf("vendor_ais_net_input_uninit (%d)\n\r", ret);
							}
						}
					}
				}


#else
				ret = vendor_ais_net_input_uninit(net_id);
				if (ret != HD_OK) {
					//printf("vendor_ais_net_input_uninit (%d)\n\r", ret);
					p_scmt_parm->result_flag = 1;
					if (debugm == 1) {
						DBG_ERR("pethread %u: %s %s vendor_ais_net_input_uninit fail: %d\r\n", net_id, mname, name, ret);
					}
					goto gen_init_fail;
				}
#endif
			}
			if (fpp != NULL) {
				fclose(fpp);
				fpp = NULL;
			}
		}

	gen_init_fail:
		ret = vendor_ais_net_gen_uninit(net_id);
		if (ret != HD_OK) {
			p_scmt_parm->result_flag = 1;
			if (debugm == 1) {
				DBG_ERR("pethread:%u:%s  vendor_ais_net_gen_uninit fail: %d\r\n", net_id, mname,  ret);
			}
			goto texit;
		}
#if AI_V4
#if RESULT_COMPARE

		if (output_layer_info != NULL) {
			free(output_layer_info);
			output_layer_info = NULL;
		}

#endif
		if (input_info != NULL) {
			free(input_info);
			input_info = NULL;
		}
#endif

	}

	if (fp != NULL){
		fclose(fp);
		fp = NULL;
	}

texit:

#if AI_V4
#if RESULT_COMPARE

	if (output_layer_info != NULL) {
		free(output_layer_info);
		output_layer_info = NULL;
	}

#endif
	if (input_info != NULL) {
		free(input_info);
		input_info = NULL;
	}
#endif

	if (fp != NULL){
		fclose(fp);
		fp = NULL;
	}
	if (fpp != NULL){
		fclose(fpp);
		fpp = NULL;
	}

	if (counti == 0){
		p_scmt_parm->result_flag = 1;
		if (debugm == 1) {
			DBG_ERR("all img can not be used\r\n");
		}
	}
	if (cfp != NULL){
		fclose(cfp);
		cfp = NULL;
	}

	return 0;

}



// return the max model size
INT32 scmt_find_max_modelsize(CHAR *file, CHAR *path, UINT32 *models_number)
{
	FILE *fp;
	UINT32 model_num = 0;
	char name[50];
	char full_path[256];
	UINT32 maxmodelsize = 0, modelsize = 0;
	HD_RESULT           ret;
	remove_blank(file);	

	fp = fopen(file, "r");
	if (fp == NULL) {		
		return -1;
	}
	while (1) {
		if (feof(fp)) {
			break;
		}
		if (fscanf(fp, "%s", name) == EOF) {
			if (feof(fp)) {
				break;
			}			
			continue;
		}
#if defined(__LINUX)

		sprintf(full_path, "%smodel_bins/%s/sdk/nvt_model.bin", path, name);
#else
		sprintf(full_path, "%ssmodel_bins\\\\%s\\\\CNN\\\\nvt_model.bin", path, name);
#endif
		ret = vendor_ais_get_model_mem_sz(&modelsize, full_path);
		if (ret != HD_OK) {
			return -1;			
		}		
		if (modelsize > maxmodelsize) {
			maxmodelsize = modelsize;
		}
		model_num = model_num + 1;
	}

	if(fp){
		fclose(fp);
		fp = NULL ;
	}
	*models_number = model_num;	
	return maxmodelsize;
}

INT32 check_file(CHAR *file)
{
	FILE *fp = NULL;
	UINT32 cnt = 0;
	CHAR line[CONFIG_MAX_SIZE * 2];
	fp = fopen(file, "r");
	if (fp == NULL) {
		return -1;
	}
	while (1) {
		if (feof(fp)) {
			break;
		}
		if (fgets(line, CONFIG_MAX_SIZE * 2 - 1, fp) == NULL) {
			if (feof(fp)) {
				break;
			}			
			continue;
		}
		cnt += 1;
	}
	
	if(fp){
		fclose(fp);
		fp = NULL ;
	}
	//printf("file check:%d\r\n",cnt);
	if (cnt == 0) {
		return -1;
	}
	return 0;
}


////////////////////////////CPU STRESS THREAD
void* thread_cpuStress(void* args) {
    //////set cpu stress
	UINT32 i = 0;
	UINT32 m = 30000;
	UINT32 n = 40000000;

	while(1) {	
		for (i = 0;i<n;i++);
		usleep(m);
	}
	return 0;
}

void* thread_cpuStress_1(void* args) {
    //////set cpu stress
	UINT32 i = 0;
	UINT32 m = 30000;
	UINT32 n = 40000000;

	while(1) {
		for (i = 0;i<n;i++);
		usleep(m);
	}
	return 0;
}
static void checkResults(char *string, int rc) {
 if (rc) {
  //printf("Error on : %s, rc=%d",
      //string, rc);
  exit(EXIT_FAILURE);
 }
 return;
}

void read_max_memory(FILE *fp, UINT32 memory_max[3]) {
	for (UINT32 idx = 0; idx < 3; idx++) {
		fscanf(fp, "%ld", &memory_max[idx]);
	}
}
//////////////////////////
/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
MAIN(argc, argv)
{

	DBG_DUMP("\r\n");
	DBG_DUMP("ai_test_6 start��\r\n");

	UINT32 thread_num;
	char list_path[256] = { 0 };
	char imglist_path[2][256] = { {0},{0} };
	UINT32 imglist_num = 0;


	VENDOR_AIS_NETWORK_PARM *net_parm = NULL;

	pthread_t *nn_thread_id =NULL;
	SCMT_PARM *scmt_parm = NULL;
	HD_RESULT           ret = 0;
	
	UINT32 idx = 0, models_number = 0, jdx;
	INT32 maxmodelsize = 0;
	UINT32 para_cnt = 0;
	CHAR key[CONFIG_MAX_ITEMS][CONFIG_MAX_SIZE];
	CHAR value[CONFIG_MAX_ITEMS][CONFIG_MAX_SIZE];
	CONFIG_PARA config_para;
	CHAR path_files[256] = "/mnt/sd/AI_auto_test_files/";
	CHAR config_path_files[256] = "/mnt/sd/AI_auto_test_files/paras/config_6.txt";
	CHAR stemp[CONFIG_MAX_SIZE];
	//UINT32 max_src_size = MAX_FRAME_WIDTH*MAX_FRAME_HEIGHT*MAX_FRAME_CHANNEL*AI_SAMPLE_MAX_BATCH_NUM;	
	UINT32 thread_mem;
	CHAR error_file[CONFIG_MAX_SIZE];	
	UINT32 error_flag_main = 0, debugm = 0;
	INT32 check_flag;


	sprintf(error_file, "%s/output/ai_test_6/error.txt", path_files);
	CHAR command_test[50];

	sprintf(command_test, "rm -rf %s/output/ai_test_6", path_files);
	system(command_test);
	sprintf(command_test, "mkdir %s/output/ai_test_6", path_files);
	system(command_test);
	
	UINT32 memory_max[3];
		CHAR memory_max_files[256] = "/mnt/sd/AI_auto_test_files/gt_results/memory_max.txt";
		FILE *fp_memory;
		if ((fp_memory = fopen(memory_max_files, "r")) == NULL) {
			DBG_ERR("memory_max.txt open file erorr\n");
			return -1;
		} else {
			read_max_memory(fp_memory, memory_max); 	
		}	
		if (fp_memory != NULL){
			fclose(fp_memory);
		}	

	if (argc != 1) {
		error_flag_main = 1;
		goto eexit;
	}

	para_cnt = load_config_file(config_path_files, key, value, CONFIG_MAX_ITEMS);

	if (para_cnt <= 0) {		
		error_flag_main = 1;
		goto eexit;
	}
	memset(config_para.model_list_1, '\0', CONFIG_MAX_SIZE);
	memset(config_para.model_list_2, '\0', CONFIG_MAX_SIZE);
	memset(config_para.image_list_1, '\0', CONFIG_MAX_SIZE);
	memset(config_para.image_list_2, '\0', CONFIG_MAX_SIZE);
	memset(stemp, '\0', CONFIG_MAX_SIZE);

	config_para.model_iterations = 0;
	config_para.thread_numbers = 0;

	flow_config_parse(key, value, para_cnt, &config_para);
	debugm = config_para.debug_mode;

	if (debugm == 1) {
		DBG_DUMP("model_size + work_buf_size : %d\r\n", memory_max[0]);
		DBG_DUMP("mode_size + work_buf_size + outfeature_size*2 : %d\r\n", memory_max[1]);
		DBG_DUMP("mode_size + work_buf_size + outfeature_size*2 + image_size : %d\r\n", memory_max[2]);
	}

	if ((strcmp(config_para.model_list_1, stemp) == 0)) {
		if (debugm == 1) {
			DBG_ERR("CNN is 1 but model_list_1 is NULL!\r\n");
		}
		error_flag_main = 1;
		goto eexit;

	}


	if (strcmp(config_para.image_list_1, stemp) != 0) {
		//strcpy(imglist_path[imglist_num], config_para.image_list_1);
		AI_STRCPY(imglist_path[imglist_num], config_para.image_list_1,sizeof(imglist_path[imglist_num]));
		check_flag = check_file(config_para.image_list_1);

		if (check_flag < 0) {			
			error_flag_main = 1;
			goto eexit;
		}
		imglist_num++;
	}

	if (strcmp(config_para.image_list_2, stemp) != 0) {
		//strcpy(imglist_path[imglist_num], config_para.image_list_2);
		AI_STRCPY(imglist_path[imglist_num], config_para.image_list_2,sizeof(imglist_path[imglist_num]));
		check_flag = check_file(config_para.image_list_2);

		if (check_flag < 0) {
			if (debugm == 1) {
				DBG_ERR("%s null!\r\n", config_para.image_list_2);
			}
			error_flag_main = 1;
			goto eexit;
		}
		imglist_num++;
	}


	if (imglist_num == 0) {
		if (debugm == 1) {
			DBG_ERR("imglist not find!\r\n");
		}
		error_flag_main = 1;
		goto eexit;

	}
	thread_num = config_para.thread_numbers;




	if (thread_num == 0) {
		if (debugm == 1) {
			DBG_ERR("wrong thread number %u \r\n", thread_num);
		}
		error_flag_main = 1;
		goto eexit;

	}

	//ai_sdk_version = vendor_ais_chk_sdk_version();
	//printf("ai_sdk_version: %s\n", ai_sdk_version);


	ret = hd_common_init(0);
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_common_init fail=%d\n", ret);
		}
		error_flag_main = 1;
		goto exit;

	}


	//set project config for AI
	hd_common_sysconfig(0, (1 << 16), 0, VENDOR_AI_CFG); //enable AI engine

	NN_TOTAL_MEM_SIZE = memory_max[2];

	ret = mem_init(); 


	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("mem_init fail=%d\n", ret);
		}
		error_flag_main = 1;
		goto exit;
	}

	ret = get_mem_block();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("get_mem_block fail=%d\n", ret);
		}
		error_flag_main = 1;
		goto exit;
	}

	ret = hd_videoproc_init();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_videoproc_init fail=%d\n", ret);
		}
		error_flag_main = 1;
		goto exit;

	}

	ret = hd_gfx_init();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_gfx_init fail=%d\n", ret);
		}
		error_flag_main = 1;
		goto exit;

	}
	pthread_mutex_init(&lock, NULL);

	net_parm = (VENDOR_AIS_NETWORK_PARM *)calloc(thread_num, sizeof(VENDOR_AIS_NETWORK_PARM));
	nn_thread_id = (pthread_t *)calloc(thread_num, sizeof(pthread_t));
	scmt_parm = (SCMT_PARM *)calloc(thread_num, sizeof(SCMT_PARM));

	VENDOR_AIS_FLOW_MEM_PARM local_mem = g_mem;
    VENDOR_AIS_FLOW_MEM_PARM local_mem_img = g_mem_img;


	check_flag = check_file(config_para.model_list_1);
	if (check_flag < 0) {
		if (debugm == 1) {
			DBG_ERR("%s null!\r\n", config_para.model_list_1);
		}
		error_flag_main = 1;
		goto exit;
	}
	maxmodelsize = scmt_find_max_modelsize(config_para.model_list_1, path_files, &models_number);

	if (maxmodelsize <= 0) {
		if (debugm == 1) {
			DBG_ERR("find_max_modelsize error %s\r\n", config_para.model_list_1);
		}
		error_flag_main = 1;
		goto exit;
	}
	//strcpy(list_path, config_para.model_list_1);
	AI_STRCPY(list_path, config_para.model_list_1,sizeof(list_path));


	if (debugm == 1) {
		DBG_WRN("maxmodelsize:%ld\r\n", maxmodelsize);
	}
	/////set CPU stress
	int          rc =0;
	int          rc_1=0;
	pthread_t th;
    pthread_t th_1;
    pthread_create(&th,NULL,thread_cpuStress,NULL);
    pthread_create(&th_1,NULL,thread_cpuStress_1,NULL);

	for (idx = 0; idx < thread_num; idx++) {		
		thread_mem = memory_max[2];		
		net_parm[idx].mem = scmt_getmem_v1(&local_mem, thread_mem, error_file, &error_flag_main, debugm);			
        net_parm[idx].mem_img=scmt_getmem_v1(&local_mem_img,thread_mem,error_file,&error_flag_main,debugm) ;		
		if (error_flag_main > 0) {
			goto exit;
		}

		//net_parm[idx].max_model_size = maxmodelsize;
		net_parm[idx].max_model_size = memory_max[0];
		net_parm[idx].run_id = idx;
		scmt_parm[idx].model_parm = net_parm[idx];
		scmt_parm[idx].model_list_path = list_path;
		scmt_parm[idx].models_number = models_number;
		//scmt_parm[idx].core = config_para.core;
		scmt_parm[idx].debug_mode = config_para.debug_mode;
		for (jdx = 0; jdx < imglist_num; jdx++) {
			AI_STRCPY(scmt_parm[idx].img_list_path[jdx], imglist_path[jdx],sizeof(scmt_parm[idx].img_list_path[jdx]));
		}

		scmt_parm[idx].imagelist_num = imglist_num;
		scmt_parm[idx].model_iterations = config_para.model_iterations;
		scmt_parm[idx].path_files = path_files;
		scmt_parm[idx].result_flag = 0;
		ret = pthread_create(&nn_thread_id[idx], NULL, scmt_thread_api, (VOID*)(&scmt_parm[idx]));

		if (ret < 0) {
			if (debugm == 1) {
				DBG_ERR("create  thread failed =%d \r\n", ret);
			}
			error_flag_main = 1;
			goto exit;
		}
	}
	for (idx = 0; idx < thread_num; idx++) {
		pthread_join(nn_thread_id[idx], NULL);
	}


	//////////////////////cancel cpu stress thread
	rc = pthread_cancel(th);
	checkResults("pthread_cancel()\n", rc);
	rc_1 = pthread_cancel(th_1);
	checkResults("pthread_cancel()\n", rc_1);
	//////////////////////
exit:
	for (idx = 0; idx < thread_num; idx++) {
		error_flag_main += scmt_parm[idx].result_flag;
	}
	pthread_mutex_destroy(&lock);
	ret = hd_gfx_uninit();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_gfx_uninit fail=%d \r\n", ret);
		}
		error_flag_main = 1;
		goto eexit;

	}

	ret = hd_videoproc_uninit();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_videoproc_uninit fail=%d \r\n", ret);
		}
		error_flag_main = 1;
		goto eexit;

	}
	ret = release_mem_block();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("release_mem_block fail=%d \r\n", ret);
		}
		error_flag_main = 1;
		goto eexit;

	}

	ret = mem_uninit();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("mem_uninit fail=%d \r\n", ret);
		}
		error_flag_main = 1;
		goto eexit;

	}
	// global uninit for ai sdk
	//vendor_ai_global_uninit();

	ret = hd_common_uninit();
	if (ret != HD_OK) {
		if (debugm == 1) {
			DBG_ERR("hd_common_uninit fail=%d ...\r\n", ret);
		}
		error_flag_main = 1;
		goto eexit;

	}

eexit:

	if(nn_thread_id != NULL){
		free(nn_thread_id);
		nn_thread_id = NULL;
	}
	if(net_parm != NULL){
		free(net_parm);
		net_parm = NULL;
	}
	if(scmt_parm != NULL){
		free(scmt_parm);
		scmt_parm = NULL;
	}

	if (error_flag_main == 0) {
		DBG_DUMP("success\r\n");
	} else {
		DBG_ERR("failed\r\n");
	}
	
	
	DBG_DUMP("ai_test_6 finished...\r\n");
	return ret;
}

