/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "hdal.h"
#include "hd_type.h"
#include "hd_common.h"
//#include "nvtipc.h"
//#include "vendor_dsp_util.h"
#include "vendor_ai/vendor_ai.h"

#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include "net_flow_user_sample/net_layer_sample.h"
#include <sys/time.h>

/*-----------------------------------------------------------------------------*/
/* Probe Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <sys/stat.h>
/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define AI_DEVICE          		"/proc/kdrv_ai"
#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080
#define DEVICE_32X			    (0x01)
#define DEVICE_52X				(0x02)
#define DEVICE_SETTING			DEVICE_52X
#define AI_SAMPLE_TEST_BATCH    1 // v2 not support
#define AI_SAMPLE_MAX_INPUT_NUM 2
#define AI_SAMPLE_MAX_BATCH_NUM 8
#define YUV_SINGLE_BUF_SIZE     (3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT * AI_SAMPLE_MAX_BATCH_NUM)
#define YUV_OUT_BUF_SIZE_NULTI_BLOB     (YUV_SINGLE_BUF_SIZE * AI_SAMPLE_MAX_INPUT_NUM)
#define MBYTE			(1024 * 1024)
#define RESULT_MEM_GT_SIZE    40 * MBYTE
#define RESULT_MEM_SIZE       40 * MBYTE


#define MODIFY_ENG_IN_SAMPLE 1
#define MAX_BLOB_NUM 2

#if (DEVICE_SETTING == DEVICE_32X)
#define DDR_TYPE (DDR_ID0)
#define MEM_TYPE  (HD_COMMON_MEM_USER_BLK)
#define MEM_GET_BLOCK (0)
#elif (DEVICE_SETTING == DEVICE_52X)
#define DDR_TYPE (DDR_ID1)
#define MEM_TYPE  (HD_COMMON_MEM_CNN_POOL)
#define MEM_GET_BLOCK (1)
#endif
#define MBYTE					(1024 * 1024)
UINT32 YUV_OUT_BUF_SIZE 		= 0;
UINT32 NN_TOTAL_MEM_SIZE       	= 0;
#define VENDOR_AI_CFG  0x000f0000  //ai project config
static UINT32 compara_with_last_version = 0;
static UINT32 flag_all_test = 1;
static UINT32 debug_mode = 0;
static UINT32 cnn_id = 0;

#define LAYER_SAVE_DUMP 0
static UINT32 test_model_type = 1;
static UINT32 auto_updata_dim = 0;
static int width_range[6] = {102,480,480,960,960,1280};
static int height_range[6] = {126,320,540,540,640,720};
static UINT32 max_mem = 0;
static UINT32 max_model_mem = 0;
static UINT32 max_model_output_mem = 0;

#define AI_STRCPY(dst, src, dst_size) do { \
strncpy(dst, src, (dst_size)-1); \
dst[(dst_size)-1] = '\0'; \
} while(0)



/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
#define _MAX(a,b) (((a)>(b))?(a):(b))
static VENDOR_AIS_FLOW_MEM_PARM g_mem	= {0};


#if (MEM_GET_BLOCK)
static HD_COMMON_MEM_VB_BLK g_blk_info[2];
#endif


typedef struct _TEST_LIST_PATH
{
	CHAR model_list[256];
	CHAR image_list[256];
    CHAR model_list_multires[256];
    CHAR model_path[256];
    CHAR image_path[256];
    CHAR gt_save_path[256];
    UINT32 test_img_num;
} TEST_LIST_PATH;

typedef struct _CONFIG_OPT
{
	CHAR key[256];
	CHAR value[256];
} CONFIG_OPT;

typedef struct _TEST_IMAGE_INFO
{
    CHAR img_name[256];
	int img_w;
    int img_h;
    int ch;
    int bits;
	HD_VIDEO_PXLFMT img_fmt;
} TEST_IMAGE_INFO;

typedef struct _TEST_IMAGE_MULTI_BLO_INFOB
{
    CHAR img_name[2][256];
	int img_w[2];
    int img_h[2];
    int ch[2];
    int batch_num[2];
    int line_ofs[2];
    int channel_ofs[2];
    int batch_ofs[2];
	HD_VIDEO_PXLFMT img_fmt[2];
} TEST_IMAGE_MULTI_BLO_INFOB;


static INT32 remove_blank(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	CHAR *p_str1 = p_str;

	while (*p_str0 != '\0') {
		if ((*p_str0 != ' ') && (*p_str0 != '\t') && (*p_str0 != '\n')) {
			*p_str1++ = *p_str0++;
        } else {
			p_str0++;
        }
	}

	*p_str1 = '\0';
	return TRUE;
}
static INT32 split_kvalue(CHAR *p_str, CONFIG_OPT* p_config, CHAR c)
{
	CHAR *p_key = p_str;
	CHAR *p_value = NULL;
	CHAR *p_local = p_str;
	INT32 cnt = 0;

	// check only one '='
	while (*p_local != '\0') {
		p_local++;
		if (*p_local == c) {
			cnt++;
        }
	}
	if (cnt != 1) {
		return FALSE;
    }

	p_local = p_str;
	while (*p_local != c) {
		p_local++;
	}
	p_value = p_local + 1;
	*p_local = '\0';
	strncpy(p_config->key, p_key, strlen(p_key)+1);
	strncpy(p_config->value, p_value, strlen(p_value)+1);
	return TRUE;
}


/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/
static INT check_driver(void)
{
	struct stat st = { 0 };
	CHAR device[20] = AI_DEVICE;

	if (stat(device, &st) == 0 && S_ISDIR(st.st_mode)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

#if MEM_GET_BLOCK
static int mem_init(CHAR *model_path, CHAR *model_diff_path_file, UINT32 *diff_model_size, UINT32 mem_size)
{
	HD_RESULT                 ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};
	UINT32                    pa, va;
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_TYPE;
	HD_COMMON_MEM_VB_BLK      blk;

	/* Allocate parameter buffer */
	if (g_mem.va != 0) {
		DBG_ERR("err: mem has already been inited\r\n");
		return -1;
	}


    if (auto_updata_dim) {
        ret = vendor_ais_get_diff_model_mem_sz(diff_model_size, model_diff_path_file);
        if (ret != HD_OK) {
            DBG_WRN("something wrong in get diff model %s size info\n", model_diff_path_file);
            return ret;
        }
        NN_TOTAL_MEM_SIZE = ALIGN_CEIL_64(mem_size) + ALIGN_CEIL_64(*diff_model_size);

    } else {
        NN_TOTAL_MEM_SIZE = ALIGN_CEIL_64(mem_size);

    }


    UINT32 total_size = NN_TOTAL_MEM_SIZE;
	//CNET need more image buffer
	if (debug_mode) {
        DBG_DUMP("NN size = %d\r\n", (int)NN_TOTAL_MEM_SIZE);
    }

	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = total_size;
	mem_cfg.pool_info[0].blk_cnt = 1;
	mem_cfg.pool_info[0].ddr_id = DDR_TYPE;

#if (DEVICE_SETTING == DEVICE_52X)
	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		DBG_ERR("hd_common_mem_init err: %d\r\n", ret);
		return ret;
	}
#endif

	blk = hd_common_mem_get_block(MEM_TYPE, total_size, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		DBG_ERR("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		DBG_ERR("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, total_size);
	g_blk_info[0] = blk;

	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_size);
		if (ret != HD_OK) {
			DBG_ERR("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = NN_TOTAL_MEM_SIZE;

exit:
	return ret;
}

static HD_RESULT mem_uninit(void)
{
	HD_RESULT ret = HD_OK;

    UINT32 total_size = NN_TOTAL_MEM_SIZE;


	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, total_size);
		if (ret != HD_OK) {
			DBG_ERR("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
		g_mem.va = 0;
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		DBG_ERR("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

#if (DEVICE_SETTING == DEVICE_52X)
	ret = hd_common_mem_uninit();
#endif
	return ret;
}

#else
static int mem_init(CHAR *model_path, CHAR *model_diff_path_file, UINT32 *diff_model_size, UINT32 mem_size)
{
	HD_RESULT                 ret;

	UINT32                    pa, va;
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_TYPE;
    HD_COMMON_MEM_POOL_TYPE   pool_type = HD_COMMON_MEM_CNN_POOL;
	HD_COMMON_MEM_POOL_INFO   mem_info = { 0 };
	/* Allocate parameter buffer */
	if (g_mem.va != 0) {
		DBG_ERR("err: mem has already been inited\r\n");
		return -1;
	}

    if (auto_updata_dim) {
        ret = vendor_ais_get_diff_model_mem_sz(diff_model_size, model_diff_path_file);
        if (ret != HD_OK) {
            DBG_WRN("something wrong in get diff model %s size info\n", model_diff_path_file);
            return ret;
        }
        NN_TOTAL_MEM_SIZE = ALIGN_CEIL_64(mem_size) + ALIGN_CEIL_64(*diff_model_size);
    } else {
        NN_TOTAL_MEM_SIZE = ALIGN_CEIL_64(mem_size);
    }

    UINT32 total_size = NN_TOTAL_MEM_SIZE;

	ret = HD_OK;
	mem_info.type = pool_type;
	mem_info.ddr_id = ddr_id;
	if (hd_common_mem_get(HD_COMMON_MEM_PARAM_POOL_CONFIG, (VOID *)&mem_info) != HD_OK) {
		DBG_DUMP("hd_common_mem_get (CNN) fail\r\n");
		return HD_ERR_NOBUF;
	}
    pa = mem_info.start_addr;
    if (pa == 0) {
		DBG_DUMP("not get buffer, pa=0x%08x\r\n", (int)pa);
		return HD_ERR_NOBUF;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, total_size);
	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_size);
		if (ret != HD_OK) {
			DBG_ERR("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}
	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = total_size;

	return ret;
}

static HD_RESULT mem_uninit(void)
{

	HD_RESULT ret = HD_OK;

    UINT32 total_size = NN_TOTAL_MEM_SIZE;

	/* Release in buffer */
	if (g_mem.va!=0) {
		ret = hd_common_mem_munmap((void *)g_mem.va, total_size);
		if (ret != HD_OK) {
			DBG_ERR("mem_uninit : hd_common_mem_munmap fail.\r\n");
			return ret;
		}
		g_mem.va = 0;
	}
#if (DEVICE_SETTING == DEVICE_52X)
	ret = hd_common_mem_uninit();
#endif
	return ret;
}


#endif

static INT32 file_loadbin(UINT32 addr, const CHAR *filename)
{
	FILE *fd;
	INT32 size = 0;

	fd = fopen(filename, "rb");

	if (NULL == fd) {
		DBG_ERR("cannot read %s\r\n", filename);
		return -1;
	}
    if (debug_mode) {
        DBG_IND("open %s ok\r\n", filename);
    }


	fseek(fd, 0, SEEK_END);
	size = ftell(fd);
	fseek(fd, 0, SEEK_SET);

	if (size < 0) {
		DBG_ERR("getting %s size failed\r\n", filename);
	} else if ((INT32)fread((VOID *)addr, 1, size, fd) != size) {
		DBG_ERR("read size < %ld\r\n", size);
		size = -1;
	}

	hd_common_mem_flush_cache((VOID *)addr, size);

	if (fd) {
		fclose(fd);
	}

	return size;
}

static HD_RESULT ai_load_img(CHAR *filename, VENDOR_AIS_IMG_PARM *p_img, VENDOR_AIS_FLOW_MEM_PARM image_mem, UINT32 img_ofs_idx, int blob_num)
{
	INT32 file_len;
    UINT32 va = image_mem.va;
	UINT32 key = 0;
    if (blob_num==1) {
        file_len = file_loadbin((UINT32)va, filename);

        if (file_len < 0) {
            return HD_ERR_IO;
        }
    } else if (blob_num==2) {

        file_len = file_loadbin((UINT32)va, filename);

        if (file_len < 0) {
            return HD_ERR_IO;
        }
    }


#if AI_SUPPORT_MULTI_FMT
		p_img->fmt_type = 0;
#endif

	if (p_img->width == 0) {
		while (TRUE) {
			DBG_DUMP("Enter width = ?\r\n");
			if (scanf("%hu", &p_img->width) != 1) {
				DBG_ERR("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->height == 0) {
		while (TRUE) {
			DBG_DUMP("Enter height = ?\r\n");
			if (scanf("%hu", &p_img->height) != 1) {
				DBG_ERR("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->channel == 0) {
		while (TRUE) {
			DBG_DUMP("Enter channel = ?\r\n");
			if (scanf("%hu", &p_img->channel) != 1) {
				DBG_ERR("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->line_ofs == 0) {
		while (TRUE) {
			DBG_DUMP("Enter line offset = ?\r\n");
			if (scanf("%lu", &p_img->line_ofs) != 1) {
				DBG_ERR("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->fmt == 0) {
		while (TRUE) {
			DBG_DUMP("Enter image format = ?\r\n");
			DBG_DUMP("[%d] RGB888 (0x%08x)\r\n", NET_IMG_RGB_PLANE, HD_VIDEO_PXLFMT_RGB888_PLANAR);
			DBG_DUMP("[%d] YUV420 (0x%08x)\r\n", NET_IMG_YUV420, HD_VIDEO_PXLFMT_YUV420);
			DBG_DUMP("[%d] YUV422 (0x%08x)\r\n", NET_IMG_YUV422, HD_VIDEO_PXLFMT_YUV422);
			DBG_DUMP("[%d] Y only (0x%08x)\r\n", NET_IMG_YONLY, HD_VIDEO_PXLFMT_Y8);
#if AI_SUPPORT_MULTI_FMT
			DBG_DUMP("[%d] YUV420_NV21 (0x%08x)\r\n", NET_IMG_YUV420_NV21, HD_VIDEO_PXLFMT_YUV420+1);
#endif

			if (scanf("%lu", &key) != 1) {
				DBG_ERR("Wrong input\n");
				continue;
			} else if (key == NET_IMG_YUV420) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
			} else if (key == NET_IMG_YUV422) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV422;
			} else if (key == NET_IMG_RGB_PLANE) {
				p_img->fmt = HD_VIDEO_PXLFMT_RGB888_PLANAR;
			} else if (key == NET_IMG_YONLY) {
				p_img->fmt = HD_VIDEO_PXLFMT_Y8;
#if AI_SUPPORT_MULTI_FMT
			} else if (key == NET_IMG_YUV420_NV21) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
				p_img->fmt_type = 1;
#endif

			} else {
				DBG_ERR("Wrong enter format\n");
				continue;
			}
			break;
		}
#if AI_SUPPORT_MULTI_FMT
	} else if (p_img->fmt == HD_VIDEO_PXLFMT_YUV420 + 1) {
		p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
		p_img->fmt_type = 1;
#endif
	}
    if (blob_num==1) {
        p_img->pa = image_mem.pa;
        p_img->va = (UINT32)va;

    } else if (blob_num==2){

    	p_img->pa = image_mem.pa;
    	p_img->va = (UINT32)va;

    }

	return HD_OK;
}


static BOOL vendor_ais_write_file(CHAR *filepath, UINT32 addr, UINT32 size)
{
	FILE *fsave = NULL;
	fsave = fopen(filepath, "wb");
	if (fsave == NULL) {
		DBG_ERR("fopen fail\n");
		return FALSE;
	}

	fwrite((UINT8 *)addr, size, 1, fsave);
	fclose(fsave);
	return TRUE;
}

static VOID write_err_txt(CHAR *save_error_path, CHAR *model_name, CHAR*pattern_name_tmp, int error_type)
{

    FILE *efp = NULL;

    efp = fopen(save_error_path, "a+");
    if (efp == NULL) {
        DBG_ERR("open error txt fail.\r\n");
    } else {

        fprintf(efp,"%s 1 %s 0 %d\n",model_name,pattern_name_tmp,error_type);
    }
	if (efp) {
		fclose(efp);
		//efp = NULL;
	}

}

static INT32 result_compare(CHAR *file, UINT8 *p2, UINT32 size,UINT32 result_gt_mem_va)
{

    FILE *fp;
    UINT32 file_size,read_size;
    UINT32 flag;
    UINT8 *p1=(UINT8 *)(result_gt_mem_va);
    fp = fopen(file,"rb");
    if (fp == NULL) {
        DBG_ERR("result_compare gt bin :%s NULL!\r\n",file);
        return -1;
    } else {

        fseek(fp,0,SEEK_END);
        file_size=ftell(fp);
        fseek(fp,0,SEEK_SET);
        if (file_size < 0) {
            fclose(fp);
            return 0;
        }
        read_size=fread((UINT8 *)p1,sizeof(UINT8),file_size,fp);
        if (read_size != file_size) {
            DBG_ERR("result_compare gt bin :%s error!\r\n",file);
        }
        if (read_size != size) {
            if (fp) {
                fclose(fp);
                //fp = NULL;
            }
            return 0;
        }
        if (debug_mode) {
            DBG_DUMP("gt size:%d vs %d!\r\n", read_size,size);
        }
        for (UINT32 t = 0; t<size; t++) {
            if (p1[t] == p2[t]) {
                flag = 1;
            } else {
                flag = 0;
                break;
            }
        }

    	if (fp) {
    		fclose(fp);
    		//fp = NULL;
    	}
        return flag;
    }


}


static UINT32 find_multi_blob_img_info(char *file_path, char *pattern_name, TEST_IMAGE_MULTI_BLO_INFOB *img_info_multi_blob,int blob_num)
{

    UINT32 flag = 0;
	CHAR *fmt;
	HD_VIDEO_PXLFMT img_fmt = 0;
	CHAR *model_name;
	FILE* fp = NULL;
	fp = fopen(file_path, "r");
	if (fp == NULL) {
		DBG_ERR("open test_images_list_info fail.\r\n");
	} else {
        CHAR img[2][256];
        CHAR *line_tmp;
        while (1) {
            char line[512];
            if (feof(fp)) {
                break;
            }
            if (fgets(line, 512 - 1, fp) == NULL) {
                if (feof(fp))
                    break;
                //printf("fgets line: %s erroe!\n", line);
                continue;
            }

            if (strstr(line, ";") != NULL) {
                //printf("%s\r\n", line);
                line_tmp = strtok(line, "\n");
                AI_STRCPY(line,line_tmp,sizeof(line));
                model_name = strtok(line, ";");
                line_tmp = strtok(NULL, ";");
                AI_STRCPY(img[0], line_tmp,sizeof(img[0]));
                line_tmp = strtok(NULL, ";");
                AI_STRCPY(img[1], line_tmp,sizeof(img[1]));
                if (strcmp(model_name, pattern_name) == 0) {
                    for (int i = 0; i < blob_num; i++) {
                        line_tmp = strtok(img[i], " ");
                        AI_STRCPY(img_info_multi_blob->img_name[i], line_tmp,sizeof(img_info_multi_blob->img_name[i]));
                        img_info_multi_blob->img_w[i] = atoi(strtok(NULL, " "));
                        img_info_multi_blob->img_h[i] = atoi(strtok(NULL, " "));
                        img_info_multi_blob->ch[i] = atoi(strtok(NULL, " "));
                        img_info_multi_blob->batch_num[i] = atoi(strtok(NULL, " "));
                        img_info_multi_blob->line_ofs[i] = atoi(strtok(NULL, " "));
                        img_info_multi_blob->channel_ofs[i] = atoi(strtok(NULL, " "));
                        img_info_multi_blob->batch_ofs[i] = atoi(strtok(NULL, " "));
                        fmt = strtok(NULL, " ");
                        if (strcmp(fmt, "0x520c0420") == 0) {
                            img_fmt = 0x520c0420;
                        } else if (strcmp(fmt, "0x51080400") == 0) {
                            img_fmt = 0x51080400;
                        } else if (strcmp(fmt, "0x23180888") == 0) {
                            img_fmt = 0x23180888;
                        } else {
                            DBG_ERR("fmt not enough.\r\n");
                        }
                        img_info_multi_blob->img_fmt[i] = img_fmt;
                        flag = 1;
                    }
                    break;
                } else {

                    continue;
                }
            }

        }
    }

	if (fp) {
		fclose(fp);
		//fp = NULL;
	}
    return flag;
}


static int compute_max_mem(CHAR *model_path,CHAR *model_name,TEST_LIST_PATH *test_config,TEST_IMAGE_INFO *test_img_info,int single_blob_mg_num)
{
	HD_RESULT ret;
	int net_id = 0;

	VENDOR_AIS_FLOW_MAP_MEM_PARM nn_flow_mem_manager;

	int model_file_size, model_size;
	NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
	UINT32 num_output_layer = 0;
	CHAR model_path_file[256];

    NN_IN_OUT_FMT *input_info = NULL;
#if AI_SAMPLE_TEST_BATCH
    	UINT32 input_proc_num = 0;
#endif

	NN_DATA output_layer_iomem = {0};


    int width = 0,height = 0,channel = 0,bits = 0;


    CHAR model_diff_path_file[256] = {0};
    UINT32 diff_model_size = 0;
#if LAYER_SAVE_DUMP
	    vendor_ais_set_dbg_mode(AI_DUMP_OUT_BIN, 1);
#endif
    sprintf(model_path_file, "%s/nvt_model.bin", model_path);

    UINT32 mem_size = 0;
	ret = vendor_ais_get_model_mem_sz(&mem_size, model_path_file);
	if (ret != HD_OK) {
	        DBG_ERR("vendor_ais_get_model_mem_sz fail: %s\r\n", model_path_file);
	        return -1;
	}
    if (debug_mode) {
	    DBG_DUMP("vendor_ais_get_model_mem_sz: %d\r\n", mem_size);
    }

    ret = mem_init(model_path_file,model_diff_path_file,&diff_model_size, mem_size);//avoid YUV420 but only need Y
    if (ret != HD_OK) {
        DBG_ERR("mem_init fail = %s\r\n", model_path_file);
        mem_uninit();
        return -1;
    }

    model_file_size = vendor_ais_load_file(model_path_file, g_mem.va, &output_layer_info, &num_output_layer);
    if (model_file_size == 0) {
        if (output_layer_info != NULL) {
            free(output_layer_info);
            output_layer_info = NULL;
        }
        DBG_ERR("net load model file fail: %s\r\n", model_path_file);
        mem_uninit();
        return -1;

    }

    model_size = vendor_ais_auto_alloc_mem(&g_mem, &nn_flow_mem_manager);
    ret = vendor_ais_net_gen_init(nn_flow_mem_manager, net_id);
    if (ret != HD_OK) {
        if (output_layer_info != NULL) {
            free(output_layer_info);
            output_layer_info = NULL;
         }
         DBG_ERR("net gen init fail=%d\r\n", ret);
         vendor_ais_net_gen_uninit(net_id);
         mem_uninit();
         return -1;

    }
    #if MODIFY_ENG_IN_SAMPLE
             //parse engine id for all cnn layer
        vendor_ais_pars_engid(cnn_id, net_id);
    #endif

    UINT32 local_model_mem = ALIGN_CEIL_64(NN_TOTAL_MEM_SIZE);
    if (local_model_mem > max_model_mem) {
        max_model_mem = local_model_mem;
    }


    UINT32 result_mem_size = 0;
    for (UINT32 i= 0; i<num_output_layer; i++) {
        ret = vendor_ais_get_layer_mem_v2(g_mem,&output_layer_iomem,&output_layer_info[i]);
        if (ret == HD_OK) {

            if (output_layer_iomem.va > 0 && output_layer_iomem.size > 0) {
                hd_common_mem_flush_cache((VOID *)output_layer_iomem.va, output_layer_iomem.size);
            }
            result_mem_size = result_mem_size + output_layer_iomem.size;
        } else {
            DBG_ERR("vendor_ais_get_layer_mem fail\n");
            vendor_ais_net_gen_uninit(net_id);
            mem_uninit();
            return -1;
        }

    }
    //printf("see_result_mem:%d\r\n",result_mem_size);
    UINT32 local_model_output_mem = ALIGN_CEIL_64(NN_TOTAL_MEM_SIZE) + ALIGN_CEIL_64(result_mem_size)*2;
    if (local_model_output_mem > max_model_output_mem) {
        max_model_output_mem = local_model_output_mem;
    }

	ret = vendor_ais_get_net_input_info(&input_info, g_mem);
    if (ret != HD_OK) {
    	if (output_layer_info != NULL) {
    		free(output_layer_info);
    		output_layer_info = NULL;
    	}
    	if (input_info != NULL) {
    		free(input_info);
    		input_info = NULL;
    	}
        DBG_ERR("Get input info fail\n");
        vendor_ais_net_gen_uninit(net_id);
		mem_uninit();
        return -1;
    }

    UINT32 io_num = 0;
    UINT32* p_input_blob_info = NULL;
    int blob_num = 0;
    for (UINT32 i= 0;i < MAX_BLOB_NUM ; i++){
        if(vendor_ais_net_get_input_blob_info(g_mem, i, &io_num, &p_input_blob_info) == 0){
            blob_num = blob_num + 1;
        } else {
            break;
        }
    }
    if (debug_mode) {
        DBG_DUMP("blob_num = %d\r\n", blob_num);
    }
    if(blob_num == 1) {

        VENDOR_AIS_FLOW_MEM_PARM model_mem;
        model_mem.va = g_mem.va;
        model_mem.pa = g_mem.pa;
        model_mem.size  = model_size;
        NN_DATA in_layer_mem = {0};
        vendor_ais_get_input_layer_mem(model_mem, &in_layer_mem, 0);    // get input layer frac bit info
        int input_bit = (int)in_layer_mem.fmt.frac_bits + (int)in_layer_mem.fmt.int_bits + (int)in_layer_mem.fmt.sign_bits;
        UINT32 max_image_mem = 0;
        UINT32 image_mem = 0;
        UINT32 input_proc_idx[8];
        input_proc_num = vendor_ais_net_get_input_layer_index(g_mem, input_proc_idx);
        //printf("input_num_batch:%d\r\n",input_proc_num);
        for (int kk =0; kk < single_blob_mg_num; kk++) {
            width = test_img_info[kk].img_w;
            height = test_img_info[kk].img_h;
            channel = test_img_info[kk].ch;
            bits = test_img_info[kk].bits;

            if ((input_info->model_width > width) || (input_info->model_height > height) || input_info->in_channel != channel) {
                continue;
            }
            if (input_bit != bits) {
                continue;
            }

            image_mem = width * height * _MAX(3, channel) * (bits / 8) * input_proc_num;
            if (image_mem > max_image_mem) {
                max_image_mem = image_mem;
            }
        }
        //printf("max_image_mem:%d\r\n",max_image_mem);
        UINT32 local_all_mem = ALIGN_CEIL_64(NN_TOTAL_MEM_SIZE) + ALIGN_CEIL_64(result_mem_size)*2 + ALIGN_CEIL_64(max_image_mem);
        if (local_all_mem > max_mem) {
            max_mem = local_all_mem;
        }

    }
    if (blob_num == 2) {

        TEST_IMAGE_MULTI_BLO_INFOB img_info_multi_blob = {0};
        UINT32 img_info_num;
        img_info_num = find_multi_blob_img_info(test_config->image_list,model_name,&img_info_multi_blob,blob_num);
        if (img_info_num == 0) {
            if (debug_mode) {
                DBG_DUMP("multi_blob img not found!\r\n");
            }
            if (output_layer_info != NULL) {
                free(output_layer_info);
                output_layer_info = NULL;
            }
            if (input_info != NULL) {
                free(input_info);
                input_info = NULL;
            }
            vendor_ais_net_gen_uninit(net_id);
            mem_uninit();
            return 0;
        }
        UINT32 io_num = 0;
        UINT32* p_input_blob_info = NULL;
        UINT32 image_mem = 0;
        for(int i=0; i<blob_num; i++){
            if (vendor_ais_net_get_input_blob_info(g_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
                flag_all_test = flag_all_test*0;
                if (output_layer_info != NULL) {
                    free(output_layer_info);
                    output_layer_info = NULL;
                }
                if (input_info != NULL) {
                    free(input_info);
                    input_info = NULL;
                }
                vendor_ais_net_gen_uninit(net_id);
                mem_uninit();
                return -1;
            }
            //printf("io_num:%d\r\n",io_num);
            width = img_info_multi_blob.img_w[i];
            height = img_info_multi_blob.img_h[i];
            channel = img_info_multi_blob.ch[i];
            bits = img_info_multi_blob.batch_num[i];
            image_mem = image_mem + width * height * _MAX(3, channel) * (bits / 8) * io_num;
            //printf("image_mem:%d\r\n",image_mem);
        }

        UINT32 local_all_mem = ALIGN_CEIL_64(NN_TOTAL_MEM_SIZE) + ALIGN_CEIL_64(result_mem_size)*2 + ALIGN_CEIL_64(image_mem);
        if (local_all_mem > max_mem) {
            max_mem = local_all_mem;
        }
    }

	ret = vendor_ais_net_gen_uninit(net_id);
    if (ret != HD_OK) {
		DBG_WRN("vendor_ais_net_gen_uninit (%d)\n\r", ret);
        mem_uninit();
        return -1;
    }
    ret = mem_uninit();
    if (ret != HD_OK) {
        DBG_ERR("mem_uninit (%d)\n\r", ret);
    }
    //printf("max_mem: %d\r\n", max_mem);
    return 0;

}


VENDOR_AIS_FLOW_MEM_PARM get_mem_custom(VENDOR_AIS_FLOW_MEM_PARM *valid_mem, UINT32 required_size, CHAR *save_error_path, CHAR *model_name)
{
	VENDOR_AIS_FLOW_MEM_PARM	mem = { 0 };
	required_size = ALIGN_CEIL_64(required_size);

	if (required_size <= valid_mem->size) {

		/*  printf("Required size %d , valid size %d\r\n", required_size, valid_mem->size); */

		mem.va = valid_mem->va;
		mem.pa = valid_mem->pa;
		mem.size = required_size;

		valid_mem->va += required_size;
		valid_mem->pa += required_size;
		valid_mem->size -= required_size;
	} else {
		DBG_ERR("Required size %ld > valid size %ld\r\n", required_size, valid_mem->size);
        flag_all_test = flag_all_test*0;
        write_err_txt(save_error_path,model_name,NULL,1);
	}
	return mem;
}


int app_nn_test_proc(CHAR *model_path,CHAR *model_name,TEST_LIST_PATH *test_config,TEST_IMAGE_INFO *test_img_info,int single_blob_mg_num,
                    CHAR *save_error_path)

{

	HD_RESULT ret;
	int net_id = 0;
	VENDOR_AIS_FLOW_MEM_PARM rslt_mem = {0};
	VENDOR_AIS_FLOW_MAP_MEM_PARM nn_flow_mem_manager;
	VENDOR_AIS_IMG_PARM	src_img[AI_SAMPLE_MAX_INPUT_NUM] = {0};
	int model_file_size, model_size;
	NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
	UINT32 num_output_layer = 0;
	CHAR model_path_file[256];



    //#if MODIFY_ENG_IN_SAMPLE
        // parse engine id for all cnn layer
        //vendor_ais_pars_engid(cnn_id, net_id);
    //#endif


    NN_IN_OUT_FMT *input_info = NULL;

    #if AI_SAMPLE_TEST_BATCH
    	//INT32 BATCH_SIZE = 8;
    	UINT32 input_proc_num = 0;
    #endif

	NN_DATA output_layer_iomem = {0};

    CHAR *image_path;
    CHAR *gt_save_path;
    image_path = test_config->image_path;
    gt_save_path = test_config->gt_save_path;

    int width = 0,height = 0,channel = 0,bits = 0;
    HD_VIDEO_PXLFMT fmt = 0;;
    CHAR pattern_name_tmp[256];
    CHAR pattern_name[AI_SAMPLE_MAX_INPUT_NUM][256];
    CHAR *image_name_rest;
    CHAR command_test[256];
    CHAR model_diff_path_file[256] = {0};
    UINT32 diff_model_size = 0;
    #if LAYER_SAVE_DUMP
	    vendor_ais_set_dbg_mode(AI_DUMP_OUT_BIN, 1);
    #endif
    sprintf(model_path_file, "%s/nvt_model.bin", model_path);

	//load cnn1 app model file
    if (auto_updata_dim) {
        sprintf(model_diff_path_file, "%s/nvt_stripe_model.bin", model_path);
    }

    ret = mem_init(model_path_file,model_diff_path_file,&diff_model_size,max_mem);//avoid YUV420 but only need Y
    if (ret != HD_OK) {
        flag_all_test = flag_all_test*0;
        DBG_ERR("mem_init fail = %s\r\n", model_path_file);
        write_err_txt(save_error_path,model_name,NULL,1);
        mem_uninit();
        return -1;
    }
    VENDOR_AIS_FLOW_MEM_PARM local_mem = g_mem;
    //printf("local_mem addr :%d\r\n",local_mem.va);


    UINT32 mem_size = 0;
	ret = vendor_ais_get_model_mem_sz(&mem_size, model_path_file);
	if (ret != HD_OK) {
	        DBG_ERR("vendor_ais_get_model_mem_sz fail: %s\r\n", model_path_file);
	        return ret;
	}
    if (debug_mode) {
	    DBG_DUMP("vendor_ais_get_model_mem_sz: %d\r\n", mem_size);
    }
    if (auto_updata_dim) {
        mem_size = mem_size + diff_model_size;
    }

    VENDOR_AIS_FLOW_MEM_PARM model_mem;

    model_mem = get_mem_custom(&local_mem, mem_size, save_error_path, model_name);
    //printf("model_mem addr, size:%d,%d\r\n",model_mem.va,mem_size);
    //printf("local addr, size:%d\r\n",local_mem.va);

    model_file_size = vendor_ais_load_file(model_path_file, model_mem.va, &output_layer_info, &num_output_layer);
    if (model_file_size == 0) {
        flag_all_test = flag_all_test*0;
        if (output_layer_info != NULL) {
            free(output_layer_info);
            output_layer_info = NULL;
        }
        DBG_ERR("net load model file fail: %s\r\n", model_path_file);
        write_err_txt(save_error_path,model_name,NULL,1);
        mem_uninit();
        return -1;
    }
    model_size = vendor_ais_auto_alloc_mem(&model_mem, &nn_flow_mem_manager);
    if (model_size == 0) {
        flag_all_test = flag_all_test*0;
        if (output_layer_info != NULL) {
            free(output_layer_info);
            output_layer_info = NULL;
        }
        DBG_ERR("net load model file fail: %s\r\n", model_path_file);
        write_err_txt(save_error_path,model_name,NULL,1);
        mem_uninit();
        return -1;
    }

    if (debug_mode) {
	    DBG_DUMP("model_size: %d\r\n", model_size);
    }

    ret = vendor_ais_net_gen_init(nn_flow_mem_manager, net_id);
    if (ret != HD_OK) {
        flag_all_test = flag_all_test*0;
        if (output_layer_info != NULL) {
            free(output_layer_info);
            output_layer_info = NULL;
         }
         DBG_ERR("net gen init fail=%d\r\n", ret);
         write_err_txt(save_error_path,model_name,NULL,1);
         vendor_ais_net_gen_uninit(net_id);
         mem_uninit();
         return -1;
    }
    #if MODIFY_ENG_IN_SAMPLE
             //parse engine id for all cnn layer
        vendor_ais_pars_engid(cnn_id, net_id);
    #endif

	ret = vendor_ais_get_net_input_info(&input_info, model_mem);
    if (ret != HD_OK) {
        flag_all_test = flag_all_test*0;
    	if (output_layer_info != NULL) {
    		free(output_layer_info);
    		output_layer_info = NULL;
    	}
    	if (input_info != NULL) {
    		free(input_info);
    		input_info = NULL;
    	}
        DBG_ERR("Get input info fail\n");
        write_err_txt(save_error_path,model_name,NULL,1);
        vendor_ais_net_gen_uninit(net_id);
		mem_uninit();
        return -1;
    }

    UINT32 io_num = 0;
    UINT32* p_input_blob_info = NULL;
    int blob_num = 0;
    for (UINT32 i= 0;i < MAX_BLOB_NUM ; i++){
        if(vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) == 0){
            blob_num = blob_num + 1;
        } else {
            break;
        }
    }
    if (debug_mode) {
        DBG_DUMP("blob_num = %d\r\n", blob_num);
    }
    if(blob_num == 1) {



        VENDOR_AIS_DIFF_MODEL_INFO diff_info = {0};
        VENDOR_AIS_FLOW_MEM_PARM diff_model_mem = {0};


        if (auto_updata_dim) {
            diff_model_mem.pa = model_mem.pa + model_mem.size;
            diff_model_mem.va = model_mem.va + model_mem.size;
            diff_model_mem.size = diff_model_size;
            model_file_size = vendor_ais_load_file(model_diff_path_file, diff_model_mem.va, NULL, &num_output_layer);
            if (model_file_size == 0) {
                DBG_ERR("net load diff model file fail: %s\r\n", model_diff_path_file);
                flag_all_test = flag_all_test*0;
                if (output_layer_info != NULL) {
                    free(output_layer_info);
                    output_layer_info = NULL;
                }
                if (input_info != NULL) {
                    free(input_info);
                    input_info = NULL;
                }
                write_err_txt(save_error_path,model_name,pattern_name_tmp,1);
                vendor_ais_net_gen_uninit(net_id);
                mem_uninit();
                return -1;
            }
        }

        NN_DATA in_layer_mem = {0};
        vendor_ais_get_input_layer_mem(model_mem, &in_layer_mem, 0);    // get input layer frac bit info
        int input_bit = (int)in_layer_mem.fmt.frac_bits + (int)in_layer_mem.fmt.int_bits + (int)in_layer_mem.fmt.sign_bits;
        if (debug_mode) {
            DBG_DUMP("in layer bit = %d\r\n", input_bit);
        }

        VENDOR_AIS_FLOW_MEM_PARM image_mem = local_mem;
        //printf("image_mem addr:%d\r\n",image_mem.va);

        for (int kk =0; kk < single_blob_mg_num; kk++) {

            width = test_img_info[kk].img_w;
            height = test_img_info[kk].img_h;
            channel = test_img_info[kk].ch;
            bits = test_img_info[kk].bits;
            fmt = test_img_info[kk].img_fmt;

            AI_STRCPY(pattern_name_tmp,test_img_info[kk].img_name,sizeof(pattern_name_tmp));
            sprintf(pattern_name[0],"%s/%s",image_path,pattern_name_tmp);
            image_name_rest = strtok(pattern_name_tmp,".");

            src_img[0].width = width;
            src_img[0].height = height;
            src_img[0].channel = channel;
            src_img[0].line_ofs = width * bits /8;
            src_img[0].fmt = fmt;

            if  (debug_mode) {
                DBG_DUMP("****** cnn1 app: %s, %s, %dx%dx%d\r\n", model_path_file, pattern_name[0], width, height, channel);
            }

            if (auto_updata_dim) {
                int width_multires;
                int height_multires;
                UINT32 flag_multires = 0;
                for (int i = 0; i<6; i++) {
                    width_multires = width_range[i];
                    height_multires = height_range[i];
                    if ((width == width_multires) && (height == height_multires) && (input_info->in_channel == channel)) {
                        flag_multires = 1;
                        break;
                    }
                }

                if (flag_multires == 0)  {
                   continue;
                }

            } else {
                if  (debug_mode) {
                    DBG_DUMP("prototxt width,height,channel:%d,%d,%d\r\n", input_info->model_width, input_info->model_height,input_info->in_channel);
                }
                if ((input_info->model_width > width) || (input_info->model_height > height) || input_info->in_channel != channel) {
                    continue;
                }
            }

            if (input_bit != bits) {
                continue;
            }
            #if AI_SAMPLE_TEST_BATCH
                UINT32 input_proc_idx[8];
                input_proc_num = vendor_ais_net_get_input_layer_index(model_mem, input_proc_idx);
            #endif



            UINT32 image_size = width * height * _MAX(3, channel) * (bits / 8) * input_proc_num;

            ret = ai_load_img(pattern_name[0],&src_img[0], image_mem,0,blob_num);
            if (ret != HD_OK) {
                flag_all_test = flag_all_test*0;
                DBG_ERR("ai_load_img fail=%d\n", ret);
                write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                continue;
            }

            VENDOR_AIS_IMG_PARM *p_src_img = &src_img[0];
            if (auto_updata_dim) {

                diff_info.input_width = p_src_img->width;
                diff_info.input_height = p_src_img->height;
                diff_info.id = 0;
                ret = vendor_ais_pars_diff_mem(&nn_flow_mem_manager, &diff_model_mem, &diff_info, net_id);
                if (ret != HD_OK) {
                    DBG_ERR("vendor_ais_pars_diff_mem (%d)\n\r", ret);
                    flag_all_test = flag_all_test*0;
                    write_err_txt(save_error_path,model_name,pattern_name_tmp,1);
                    vendor_ais_unpars_diff_mem(&nn_flow_mem_manager, &diff_model_mem, &diff_info, net_id);

                    continue;
                }
            }
            #if AI_SAMPLE_TEST_BATCH


                UINT32 tmp_src_va = p_src_img->va;
                UINT32 tmp_src_pa = p_src_img->pa;
                UINT32 src_img_size = p_src_img->line_ofs * p_src_img->height * p_src_img->channel;
                UINT32 i = 0;


                for (i = 0; i < input_proc_num; i++) {
                    if (i==0) {
                        hd_common_mem_flush_cache((VOID *)(p_src_img->va), src_img_size);
                    }
                    if (i > 0) {
                        memcpy((VOID*)(p_src_img->va + src_img_size), (VOID*)p_src_img->va, src_img_size);
                        hd_common_mem_flush_cache((VOID *)(p_src_img->va + src_img_size), src_img_size);
                        p_src_img->pa += src_img_size;
                        p_src_img->va += src_img_size;
                    }
                    if (vendor_ais_net_input_layer_init(p_src_img, i, net_id) != HD_OK) {
                        flag_all_test = flag_all_test*0;

                        write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                        if (auto_updata_dim) {
                            vendor_ais_unpars_diff_mem(&nn_flow_mem_manager, &diff_model_mem, &diff_info, net_id);
                        }
                        vendor_ais_net_input_layer_uninit(i, net_id);
                        continue;
                    }
                }
                p_src_img->va = tmp_src_va;
                p_src_img->pa = tmp_src_pa;
            #else
                if (vendor_ais_net_input_init(&src_img, net_id) != HD_OK) {
                    flag_all_test = flag_all_test*0;

                    write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                    if (auto_updata_dim) {
                        vendor_ais_unpars_diff_mem(&nn_flow_mem_manager, &diff_model_mem, &diff_info, net_id);
                    }
                    vendor_ais_net_input_uninit(net_id);
                    continue;
                }
            #endif


            memset((VOID *)nn_flow_mem_manager.user_buff.va, 0, nn_flow_mem_manager.user_buff.size);
            if (hd_common_mem_flush_cache((VOID *)nn_flow_mem_manager.user_buff.va, nn_flow_mem_manager.user_buff.size) != HD_OK) {
                flag_all_test = flag_all_test*0;

                write_err_txt(save_error_path,model_name,pattern_name_tmp,3);
                if (auto_updata_dim) {
                    vendor_ais_unpars_diff_mem(&nn_flow_mem_manager, &diff_model_mem, &diff_info, net_id);
                }
                #if AI_SAMPLE_TEST_BATCH
                    for (i = 0; i < input_proc_num; i++) {
                        ret = vendor_ais_net_input_layer_uninit(i, net_id);
                    }
                #else
                    vendor_ais_net_input_uninit(net_id);
                #endif
                continue;
            }

            #if USE_NEON
                    ret = vendor_ais_proc_net(model_mem, rslt_mem, &src_img[0], net_id);
            #else
                    ret = vendor_ais_proc_net(model_mem, rslt_mem, net_id);

            #endif

            if (ret != HD_OK) {
                flag_all_test = flag_all_test*0;

                write_err_txt(save_error_path,model_name,pattern_name_tmp,3);
                if (auto_updata_dim) {
                    vendor_ais_unpars_diff_mem(&nn_flow_mem_manager, &diff_model_mem, &diff_info, net_id);
                }
                #if AI_SAMPLE_TEST_BATCH
                    for (i = 0; i < input_proc_num; i++) {
                        ret = vendor_ais_net_input_layer_uninit(i, net_id);
                    }
                #else
                    vendor_ais_net_input_uninit(net_id);
                #endif
                continue;
            }
            if (debug_mode) {
                DBG_DUMP("process finish!!\n");
            }

            #if LAYER_SAVE_DUMP
                CHAR output_bin_path[256];
                sprintf(output_bin_path, "output_bin/%s_LL_CNN1_FR8B", model_name);
                if (access(output_bin_path,0) == -1) {
                    sprintf(output_bin_path, "mkdir output_bin/%s_LL_CNN1_FR8B", model_name);
                    system(output_bin_path);
                }
                sprintf(output_bin_path, "mv output0 output_bin/%s_LL_CNN1_FR8B/%s", model_name,image_name_rest);
                system(output_bin_path);
            #endif



            UINT32 result_mem_start = local_mem.va + ALIGN_CEIL_64(image_size);
            UINT32 save_result_start = local_mem.va + ALIGN_CEIL_64(image_size);
            UINT32 result_mem_size = 0;
           //printf("image_size:%d\r\n",image_size);
           //printf("result_mem_start addr:%d\r\n",result_mem_start);

            for (UINT32 i= 0; i<num_output_layer; i++) {
                ret = vendor_ais_get_layer_mem_v2(model_mem,&output_layer_iomem,&output_layer_info[i]);
                if (ret == HD_OK) {

                    if (output_layer_iomem.va > 0 && output_layer_iomem.size > 0) {
                        hd_common_mem_flush_cache((VOID *)output_layer_iomem.va, output_layer_iomem.size);
                    }

                    memcpy((VOID*)save_result_start,(VOID*)output_layer_iomem.va,output_layer_iomem.size);
                    save_result_start = save_result_start + output_layer_iomem.size;
                    result_mem_size = result_mem_size + output_layer_iomem.size;
                } else {
                    flag_all_test = flag_all_test*0;

                    write_err_txt(save_error_path,model_name,pattern_name_tmp,3);
                    if (auto_updata_dim) {
                        vendor_ais_unpars_diff_mem(&nn_flow_mem_manager, &diff_model_mem, &diff_info, net_id);
                    }
                    DBG_ERR("vendor_ais_get_layer_mem fail\n");
                    #if AI_SAMPLE_TEST_BATCH
                        for (i = 0; i < input_proc_num; i++) {
                            vendor_ais_net_input_layer_uninit(i, net_id);
                        }
                    #else
                        vendor_ais_net_input_uninit(net_id);
                    #endif
                    continue;
                }

            }

            CHAR img_save_path[256];
            sprintf(img_save_path, "%s/%s/%s",gt_save_path,model_name,image_name_rest);
            sprintf(command_test,"mkdir %s",img_save_path);
            system(command_test);
            snprintf(img_save_path, STR_MAX_LENGTH,"%s/%s/%s/outfeature.bin",gt_save_path,model_name,image_name_rest);
            if (debug_mode) {
                DBG_DUMP("model ouput: %s\n",img_save_path);
            }

            if (!(vendor_ais_write_file(img_save_path, result_mem_start, result_mem_size))) {
                flag_all_test = flag_all_test*0;
                write_err_txt(save_error_path,model_name,pattern_name_tmp,3);
                if (auto_updata_dim) {
                    vendor_ais_unpars_diff_mem(&nn_flow_mem_manager, &diff_model_mem, &diff_info, net_id);
                }

                #if AI_SAMPLE_TEST_BATCH
                    for (i = 0; i < input_proc_num; i++) {
                        vendor_ais_net_input_layer_uninit(i, net_id);
                    }
                #else
                    vendor_ais_net_input_uninit(net_id);
                #endif
                continue;
            }

            UINT32 result_gt_mem_va = result_mem_start + ALIGN_CEIL_64(result_mem_size);
            //printf("result_mem_size:%d\r\n",result_mem_size);
            //printf("result_gt_mem_va addr:%d\r\n",result_gt_mem_va);


            if (compara_with_last_version) {

                CHAR gt_path[256];
                sprintf(gt_path,"/mnt/sd/AI_auto_test_files/output_last_gt/%s/%s/outfeature.bin",model_name,image_name_rest);
                if (access(gt_path,0) == 0) {
                    UINT flag = 0;
                    UINT8 *p1 = (UINT8*)(result_mem_start);
                    flag = result_compare(gt_path,p1,result_mem_size,result_gt_mem_va);
                    flag_all_test = flag_all_test*flag;

                    if (flag == 1) {
                        if (debug_mode) {
                            DBG_DUMP("model: %s, image: %s, %s\n",model_name,image_name_rest, "Output is same with last version!!!");
                        }
                    } else {
                        if (debug_mode) {
                            DBG_ERR("Output is different with last version!!!\n");
                        }
                        write_err_txt(save_error_path,model_name,pattern_name_tmp,4);

                    }
                } else {
                    if (auto_updata_dim) {
                        vendor_ais_unpars_diff_mem(&nn_flow_mem_manager, &diff_model_mem, &diff_info, net_id);
                    }

                    #if AI_SAMPLE_TEST_BATCH
                        for (i = 0; i < input_proc_num; i++) {
                            vendor_ais_net_input_layer_uninit(i, net_id);
                        }
                    #else
                        vendor_ais_net_input_uninit(net_id);
                    #endif
                    continue;
                }


            }

            #if AI_SAMPLE_TEST_BATCH
            	for (i = 0; i < input_proc_num; i++) {
            		vendor_ais_net_input_layer_uninit(i, net_id);

            	}
            #else
            	vendor_ais_net_input_uninit(net_id);

            #endif

            if (auto_updata_dim) {
                vendor_ais_unpars_diff_mem(&nn_flow_mem_manager, &diff_model_mem, &diff_info, net_id);
             }

        }
    }

    if (blob_num == 2) {
        TEST_IMAGE_MULTI_BLO_INFOB img_info_multi_blob = {0};
        UINT32 img_info_num;
        img_info_num = find_multi_blob_img_info(test_config->image_list,model_name,&img_info_multi_blob,blob_num);
        if (img_info_num == 0) {
            if (debug_mode) {
                DBG_DUMP("multi_blob img not found!\r\n");
            }
            if (output_layer_info != NULL) {
                free(output_layer_info);
                output_layer_info = NULL;
            }
            if (input_info != NULL) {
                free(input_info);
                input_info = NULL;
            }
            vendor_ais_net_gen_uninit(net_id);
            mem_uninit();
            return 0;
        }




        for(int i=0;i<AI_SAMPLE_MAX_INPUT_NUM;i++){
            src_img[i].width = img_info_multi_blob.img_w[i];
            src_img[i].height = img_info_multi_blob.img_h[i];
            src_img[i].channel = img_info_multi_blob.ch[i];
            src_img[i].line_ofs = img_info_multi_blob.line_ofs[i];
            src_img[i].channel_ofs = img_info_multi_blob.channel_ofs[i];
            src_img[i].batch_ofs = img_info_multi_blob.batch_ofs[i];
            src_img[i].batch_num = img_info_multi_blob.batch_num[i];
            src_img[i].fmt = img_info_multi_blob.img_fmt[i];

        }
        sprintf(pattern_name[0],"%s/%s",image_path,img_info_multi_blob.img_name[0]);
        sprintf(pattern_name[1],"%s/%s",image_path,img_info_multi_blob.img_name[1]);
        strcpy(pattern_name_tmp,img_info_multi_blob.img_name[0]);
        image_name_rest = strtok(pattern_name_tmp,".");

        UINT32 io_num = 0;
        UINT32* p_input_blob_info = NULL;
        VENDOR_AIS_IMG_PARM *p_src_img = src_img;
        UINT32 j;
        UINT32 src_img_size;

        VENDOR_AIS_FLOW_MEM_PARM image_mem = local_mem;
        UINT32 image_size = 0;
        //printf("image_mem addr:%d\r\n",image_mem.va);

        for (int i = 0; i < blob_num; i++) {


            if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
                flag_all_test = flag_all_test*0;
                if (output_layer_info != NULL) {
                    free(output_layer_info);
                    output_layer_info = NULL;
                }
                if (input_info != NULL) {
                    free(input_info);
                    input_info = NULL;
                }
                write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                vendor_ais_net_gen_uninit(net_id);
                mem_uninit();
                return -1;
            }
            width = img_info_multi_blob.img_w[i];
            height = img_info_multi_blob.img_h[i];
            channel = img_info_multi_blob.ch[i];
            bits = img_info_multi_blob.batch_num[i];



            image_mem.va = image_mem.va + image_size;
            image_mem.pa = image_mem.pa + image_size;

            ret = ai_load_img(pattern_name[i], &src_img[i], image_mem, i, blob_num);

            if (ret != HD_OK) {
                flag_all_test = flag_all_test*0;
                if (output_layer_info != NULL) {
                    free(output_layer_info);
                    output_layer_info = NULL;
                }
                if (input_info != NULL) {
                    free(input_info);
                    input_info = NULL;
                }
                DBG_ERR("ai_load_img fail=%d\n", ret);
                write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                vendor_ais_net_gen_uninit(net_id);
                mem_uninit();
                return -1;
            }
            image_size = image_size + width * height * _MAX(3, channel) * (bits / 8) * io_num;

            src_img_size = p_src_img[i].batch_ofs;
            for (j = 0; j < io_num; j++) {
                UINT32 proc_idx = p_input_blob_info[j] >> 8;
                UINT32 tmp_va = p_src_img[i].va;
                UINT32 tmp_pa = p_src_img[i].pa;
                p_src_img[i].va += (j*p_src_img[i].batch_ofs);
                p_src_img[i].pa += (j*p_src_img[i].batch_ofs);
                hd_common_mem_flush_cache((VOID *)(p_src_img[i].va), src_img_size);
                if (vendor_ais_net_input_layer_init(&p_src_img[i], proc_idx, net_id) != HD_OK) {
                    flag_all_test = flag_all_test*0;
                    if (output_layer_info != NULL) {
                        free(output_layer_info);
                        output_layer_info = NULL;
                    }
                    if (input_info != NULL) {
                        free(input_info);
                        input_info = NULL;
                    }
                    write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                    vendor_ais_net_gen_uninit(net_id);
                    mem_uninit();
                    return -1;

                }
                p_src_img[i].va = tmp_va;
                p_src_img[i].pa = tmp_pa;
            }
        }

        memset((VOID *)nn_flow_mem_manager.user_buff.va, 0, nn_flow_mem_manager.user_buff.size);
        if (hd_common_mem_flush_cache((VOID *)nn_flow_mem_manager.user_buff.va, nn_flow_mem_manager.user_buff.size) != HD_OK) {
            flag_all_test = flag_all_test*0;
            if (output_layer_info != NULL) {
                free(output_layer_info);
                output_layer_info = NULL;
            }
            if (input_info != NULL) {
                free(input_info);
                input_info = NULL;
            }
            write_err_txt(save_error_path,model_name,pattern_name_tmp,3);
            #if AI_SAMPLE_TEST_BATCH
                UINT32 j = 0;
                UINT32 io_num = 0;
                UINT32* p_input_blob_info = NULL;

                for (int i = 0; i < blob_num; i++) {
                    if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
                        flag_all_test = flag_all_test*0;
                        write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                        for (j = 0; j < io_num; j++) {
                            UINT32 proc_idx = p_input_blob_info[j] >> 8;
                            ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                            if (ret != HD_OK) {
                                DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                            }
                        }
                        vendor_ais_net_gen_uninit(net_id);
                        mem_uninit();
                        return -1;

                    }
                    for (j = 0; j < io_num; j++) {
                        UINT32 proc_idx = p_input_blob_info[j] >> 8;
                        ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                        if (ret != HD_OK) {
                            DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                        }
                    }
                }
            #else
                vendor_ais_net_input_uninit(net_id);
            #endif
            vendor_ais_net_gen_uninit(net_id);
            mem_uninit();
            return -1;

        }


        #if USE_NEON
             ret = vendor_ais_proc_net(model_mem, rslt_mem, &p_src_img[0], net_id);
        #else
             ret = vendor_ais_proc_net(model_mem, rslt_mem, net_id);
        #endif

        if (ret != HD_OK) {
            flag_all_test = flag_all_test*0;
            if (output_layer_info != NULL) {
                free(output_layer_info);
                output_layer_info = NULL;
            }
            if (input_info != NULL) {
                free(input_info);
                input_info = NULL;
            }
            write_err_txt(save_error_path,model_name,pattern_name_tmp,3);
            #if AI_SAMPLE_TEST_BATCH
                UINT32 j = 0;
                UINT32 io_num = 0;
                UINT32* p_input_blob_info = NULL;

                for (int i = 0; i < blob_num; i++) {
                    if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
                        flag_all_test = flag_all_test*0;
                        write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                        for (j = 0; j < io_num; j++) {
                            UINT32 proc_idx = p_input_blob_info[j] >> 8;
                            ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                            if (ret != HD_OK) {
                                DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                            }
                        }
                        vendor_ais_net_gen_uninit(net_id);
                        mem_uninit();
                        return -1;

                    }
                    for (j = 0; j < io_num; j++) {
                        UINT32 proc_idx = p_input_blob_info[j] >> 8;

                        ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                        if(ret != HD_OK) {
                            DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                        }
                    }
                }
            #else
                vendor_ais_net_input_uninit(net_id);
            #endif
            vendor_ais_net_gen_uninit(net_id);
            mem_uninit();

            return -1;
        }
        if (debug_mode) {
            DBG_DUMP("process finish!!\n");
        }

        #if LAYER_SAVE_DUMP
            CHAR output_bin_path[256];
            sprintf(output_bin_path, "output_bin/%s_LL_CNN1_FR8B", model_name);
            if (access(output_bin_path,0) == -1) {
                sprintf(output_bin_path, "mkdir output_bin/%s_LL_CNN1_FR8B", model_name);
                system(output_bin_path);
            }
            sprintf(output_bin_path, "mv output0 output_bin/%s_LL_CNN1_FR8B/%s", model_name,image_name_rest);
            system(output_bin_path);
        #endif



        UINT32 result_mem_start = local_mem.va + ALIGN_CEIL_64(image_size);
        UINT32 save_result_start = local_mem.va + ALIGN_CEIL_64(image_size);
        UINT32 result_mem_size = 0;

        //printf("image_size:%d\r\n",image_size);
        //printf("result_mem_start addr:%d\r\n",result_mem_start);

        for (UINT32 i= 0; i<num_output_layer; i++)  {
            ret = vendor_ais_get_layer_mem_v2(model_mem,&output_layer_iomem,&output_layer_info[i]);

            if (ret == HD_OK) {

                if (output_layer_iomem.va > 0 && output_layer_iomem.size > 0) {
                    hd_common_mem_flush_cache((VOID *)output_layer_iomem.va, output_layer_iomem.size);
                }

                memcpy((VOID*)save_result_start,(VOID*)output_layer_iomem.va,output_layer_iomem.size);
                save_result_start = save_result_start + output_layer_iomem.size;
                result_mem_size = result_mem_size + output_layer_iomem.size;
            } else {
                flag_all_test = flag_all_test*0;
                if (output_layer_info != NULL) {
                    free(output_layer_info);
                    output_layer_info = NULL;
                }
                if (input_info != NULL) {
                    free(input_info);
                    input_info = NULL;
                }
                write_err_txt(save_error_path,model_name,pattern_name_tmp,3);
                DBG_ERR("vendor_ais_get_layer_mem fail\n");
                #if AI_SAMPLE_TEST_BATCH
                    UINT32 j = 0;
                    UINT32 io_num = 0;
                    UINT32* p_input_blob_info = NULL;

                    for (int i = 0; i < blob_num; i++) {
                        if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
                            flag_all_test = flag_all_test*0;
                            write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                            for (j = 0; j < io_num; j++) {
                                UINT32 proc_idx = p_input_blob_info[j] >> 8;
                                ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                                if (ret != HD_OK) {
                                    DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                                }
                            }
                            vendor_ais_net_gen_uninit(net_id);
                            mem_uninit();
                            return -1;

                        }
                        for (j = 0; j < io_num; j++) {
                            UINT32 proc_idx = p_input_blob_info[j] >> 8;

                            ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                            if (ret != HD_OK) {
                                DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                            }
                        }
                    }
                #else
                    vendor_ais_net_input_uninit(net_id);
                #endif
                vendor_ais_net_gen_uninit(net_id);
                mem_uninit();
                return -1;
            }

        }

        CHAR img_save_path[256];
        sprintf(img_save_path, "%s/%s/%s",gt_save_path,model_name,image_name_rest);
        sprintf(command_test,"mkdir %s",img_save_path);
        system(command_test);
        snprintf(img_save_path, STR_MAX_LENGTH,"%s/%s/%s/outfeature.bin",gt_save_path,model_name,image_name_rest);
        if (debug_mode) {
            DBG_DUMP("model ouput: %s\n",img_save_path);
        }

        if (!(vendor_ais_write_file(img_save_path, result_mem_start, result_mem_size))) {
           flag_all_test = flag_all_test*0;
           if (output_layer_info != NULL) {
                free(output_layer_info);
                output_layer_info = NULL;
           }
           if (input_info != NULL) {
                free(input_info);
                input_info = NULL;
           }
           write_err_txt(save_error_path,model_name,pattern_name_tmp,3);

            #if AI_SAMPLE_TEST_BATCH
            	UINT32 j = 0;
            	UINT32 io_num = 0;
            	UINT32* p_input_blob_info = NULL;

            	for (int i = 0; i < blob_num; i++) {
            		if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
                        flag_all_test = flag_all_test*0;
                        write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                        for (j = 0; j < io_num; j++) {
                            UINT32 proc_idx = p_input_blob_info[j] >> 8;
                            ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                            if (ret != HD_OK) {
                                DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                            }
                        }
                        vendor_ais_net_gen_uninit(net_id);
                        mem_uninit();
                        return -1;

            		}
            		for (j = 0; j < io_num; j++) {
            			UINT32 proc_idx = p_input_blob_info[j] >> 8;

            			ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
            			if (ret != HD_OK) {
            				DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
            			}
            		}
            	}
            #else
                vendor_ais_net_input_uninit(net_id);
            #endif
            vendor_ais_net_gen_uninit(net_id);
            mem_uninit();
            return -1;

        }


        UINT32 result_gt_mem_va = result_mem_start + ALIGN_CEIL_64(result_mem_size);
        //printf("result_mem_size:%d\r\n",result_mem_size);
        //printf("result_gt_mem_va addr:%d\r\n",result_gt_mem_va);

        if (compara_with_last_version) {
            CHAR gt_path[256];
            sprintf(gt_path,"/mnt/sd/AI_auto_test_files/output_last_gt/%s/%s/outfeature.bin",model_name,image_name_rest);
            if (access(gt_path,0) == 0) {
                UINT flag = 0;
                UINT8 *p1 = (UINT8*)(result_mem_start);
                flag = result_compare(gt_path,p1,result_mem_size,result_gt_mem_va);
                flag_all_test = flag_all_test*flag;
                if (flag == 1) {
                    if (debug_mode) {
                        DBG_DUMP("model: %s, image: %s, %s\n",model_name,image_name_rest, "Output is same with last version!!!");
                    }
                } else {
                    if  (debug_mode) {
                        DBG_ERR("Output is different with last version!!!\n");
                    }
                    write_err_txt(save_error_path,model_name,pattern_name_tmp,4);

                }
            } else {
                if (output_layer_info != NULL) {
                     free(output_layer_info);
                     output_layer_info = NULL;
                }
                if (input_info != NULL) {
                     free(input_info);
                     input_info = NULL;
                }

                #if AI_SAMPLE_TEST_BATCH
                     UINT32 j = 0;
                     UINT32 io_num = 0;
                     UINT32* p_input_blob_info = NULL;

                     for (int i = 0; i < blob_num; i++) {
                         if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
                             flag_all_test = flag_all_test*0;
                             write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                             for (j = 0; j < io_num; j++) {
                                 UINT32 proc_idx = p_input_blob_info[j] >> 8;
                                 ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                                 if (ret != HD_OK) {
                                     DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                                 }
                             }
                             vendor_ais_net_gen_uninit(net_id);
                             mem_uninit();
                             return -1;

                         }
                         for (j = 0; j < io_num; j++) {
                             UINT32 proc_idx = p_input_blob_info[j] >> 8;

                             ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                             if (ret != HD_OK) {
                                 DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                             }
                         }
                     }
                #else
                     vendor_ais_net_input_uninit(net_id);
                #endif
                vendor_ais_net_gen_uninit(net_id);
                mem_uninit();
                return 0;
            }

        }

        #if AI_SAMPLE_TEST_BATCH
            j = 0;
            io_num = 0;
            p_input_blob_info = NULL;

            for (int i = 0; i < blob_num; i++) {
                if (vendor_ais_net_get_input_blob_info(model_mem, i, &io_num, &p_input_blob_info) != HD_OK) {
                    flag_all_test = flag_all_test*0;
                    write_err_txt(save_error_path,model_name,pattern_name_tmp,2);
                    for (j = 0; j < io_num; j++) {
                        UINT32 proc_idx = p_input_blob_info[j] >> 8;
                        ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                        if (ret != HD_OK) {
                            DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                        }
                    }
                    vendor_ais_net_gen_uninit(net_id);
                    mem_uninit();
                    return -1;

                }
                for (j = 0; j < io_num; j++) {
                    UINT32 proc_idx = p_input_blob_info[j] >> 8;

                    ret = vendor_ais_net_input_layer_uninit(proc_idx, net_id);
                    if (ret != HD_OK) {
                        DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                    }
                }
            }
        #else
            vendor_ais_net_input_uninit(net_id);
        #endif

    }


    if (output_layer_info != NULL) {
        free(output_layer_info);
        output_layer_info = NULL;
    }
    if (input_info != NULL) {
        free(input_info);
        input_info = NULL;
    }


	ret = vendor_ais_net_gen_uninit(net_id);
    if (ret != HD_OK) {
		DBG_WRN("vendor_ais_net_gen_uninit (%d)\n\r", ret);
        mem_uninit();
        return -1;
    }
    ret = mem_uninit();
    if (ret != HD_OK) {
		DBG_WRN("mem_uninit (%d)\n\r", ret);
        return -1;
    }
    if (debug_mode) {
        DBG_DUMP("The process is finishing normally!\n\r");
    }

    return 0;
}

static INT32 config_parser(CHAR *file_path, TEST_LIST_PATH *test_config)
{

    FILE* fp_tmp= NULL;
    if ((fp_tmp = fopen(file_path, "r")) == NULL) {
        DBG_ERR("config_0.txt is not exist!\n");
        return 0;
    }
    CHAR line[512];
    while (1) {

        if (feof(fp_tmp))
            break;
        if (fgets(line, 512 - 1, fp_tmp) == NULL) {
            if (feof(fp_tmp))
                break;

            continue;
        }
        //printf("see:%s\n", line);
        remove_blank(line);
        //printf("see after:%s\n", line);
        if (line[0] == '#' || line[0] == '\0')
            continue;
        CONFIG_OPT tmp;
        if (split_kvalue(line, &tmp, '=') != 1) {
            //printf("pauser line : %s error!\r\n", line);
            continue;
        }

        if (strcmp(tmp.key, "[path/model_list]") == 0) {
            AI_STRCPY(test_config->model_list, tmp.value,sizeof(test_config->model_list));
        }
        if (strcmp(tmp.key, "[path/image_list]") == 0) {
            AI_STRCPY(test_config->image_list, tmp.value,sizeof(test_config->image_list));
        }
        if (strcmp(tmp.key, "[path/model_list_multires]") == 0) {
            AI_STRCPY(test_config->model_list_multires, tmp.value,sizeof(test_config->model_list_multires));
        }
        if (strcmp(tmp.key, "[path/model_path]") == 0) {
            AI_STRCPY(test_config->model_path, tmp.value,sizeof(test_config->model_path));
        }
        if (strcmp(tmp.key, "[path/image_path]") == 0) {
            AI_STRCPY(test_config->image_path, tmp.value,sizeof(test_config->image_path));
        }
        if (strcmp(tmp.key, "[path/gt_save_path]") == 0) {
            AI_STRCPY(test_config->gt_save_path, tmp.value,sizeof(test_config->gt_save_path));
        }
        if (strcmp(tmp.key, "[whether_to_compare]") == 0) {
            compara_with_last_version = atoi(tmp.value);
        }
        if (strcmp(tmp.key, "[test_img_num]") == 0) {
            test_config->test_img_num = atoi(tmp.value);
        }
        if (strcmp(tmp.key, "[debug_mode]") == 0) {
            debug_mode = atoi(tmp.value);
        }
        if (strcmp(tmp.key, "[cnn_id]") == 0) {
            cnn_id = atoi(tmp.value);
        }


    }
    if (strcmp(test_config->model_list_multires,"None") == 0) {
        test_model_type = 1;
    } else {
        test_model_type = 2;
    }

    if (debug_mode) {
        DBG_DUMP("model_list_path:%s\n", test_config->model_list);
        DBG_DUMP("image_list_path:%s\n", test_config->image_list);
        DBG_DUMP("image_list_path:%s\n", test_config->model_list_multires);
        DBG_DUMP("model_path:%s\n", test_config->model_path);
        DBG_DUMP("image_path:%s\n", test_config->image_path);
        DBG_DUMP("gt_save_path:%s\n", test_config->gt_save_path);
        DBG_DUMP("whether to compare with last version: %d\n", compara_with_last_version);
        DBG_DUMP("The numbers of images:%d\n",test_config->test_img_num);
        DBG_DUMP("CNN ID:%d\n",cnn_id);

    }

    fclose(fp_tmp);
    return 1;
}
static int image_list_parser(CHAR *file_path, TEST_IMAGE_INFO *test_image_total)
{
    int img_w = 0, img_h = 0, ch = 0, bits = 0;
    CHAR *fmt;
    HD_VIDEO_PXLFMT img_fmt = 0;
    CHAR img_path[256] = { 0 };
    FILE* fp = NULL;
    fp = fopen(file_path, "r");
    if (fp == NULL) {
        DBG_ERR("open test_images_list_info fail.\r\n");
        return -1;
    } else {

        CHAR *line_tmp;
        int i = 0;
        while (1) {
                CHAR line[512];
                if (feof(fp)) {
                    break;
                }
                if (fgets(line, 512 - 1, fp) == NULL) {
                    if (feof(fp))
                        break;
                    //printf("fgets line: %s erroe!\n", line);
                    continue;
                }

                if (strstr(line, ";") == NULL) {
                    if (debug_mode) {
                        DBG_WRN("%s\r\n", line);
                    }
                    line_tmp = strtok(line, "\n");
                    AI_STRCPY(line,line_tmp,sizeof(line));

                    line_tmp = strtok(line, " ");
                    AI_STRCPY(img_path,line_tmp,sizeof(img_path));

                    img_w = atoi(strtok(NULL, " "));

                    img_h= atoi(strtok(NULL, " "));

                    ch = atoi(strtok(NULL, " "));

                    bits = atoi(strtok(NULL, " "));

                    fmt = strtok(NULL, " ");


                    if (debug_mode) {
                        DBG_WRN("www:%s\r\n",fmt);
                    }

                    if (strcmp(fmt, "HD_VIDEO_PXLFMT_YUV420")== 0) {
                        img_fmt = 0x520c0420;
                    } else if (strcmp(fmt, "HD_VIDEO_PXLFMT_Y8") == 0) {
                        img_fmt = 0x51080400;
                    } else if (strcmp(fmt, "HD_VIDEO_PXLFMT_RGB888_PLANAR") == 0) {
                        img_fmt = 0x23180888;
                    } else {
                        img_fmt = 0;
                        DBG_ERR("fmt not enough.\r\n");

                    }

                    test_image_total[i].img_w = img_w;
                    test_image_total[i].img_h = img_h;
                    test_image_total[i].ch = ch;
                    test_image_total[i].bits= bits;
                    AI_STRCPY(test_image_total[i].img_name,img_path,sizeof(test_image_total[i].img_name));
                    test_image_total[i].img_fmt= img_fmt;
                    i++;
                }
        }
        if (fp) {
            fclose(fp);
        }

        return i;
    }
  //read model name
}


/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
    DBG_DUMP("ai_test_0 start...\r\n");
	HD_RESULT ret;


	CHAR model_name[50] = { 0 };

	ret	= hd_common_init(0);
	if (ret != HD_OK) {
		DBG_ERR("hd_common_init fail=%d\n", ret);
		goto exit;
	}
#if (DEVICE_SETTING == DEVICE_52X)
	hd_common_sysconfig(0, (1<<16), 0, VENDOR_AI_CFG); //enable AI engine
#endif

	if (check_driver() == FALSE) {
		system("modprobe kdrv_ai.ko");
		system("modprobe netflowsample.ko");
	}

    #if LAYER_SAVE_DUMP
    	system("rm -rf output0");
    	system("rm -rf output_bin");
    	system("mkdir output_bin");
    #endif

    TEST_LIST_PATH test_config = {0};
    CHAR config_path[256];

    sprintf(config_path,"%s","/mnt/sd/AI_auto_test_files/paras/config_0.txt");
    if (debug_mode) {
        DBG_DUMP("config path: %s\r\n", config_path);
    }
    if (!(config_parser(config_path,&test_config))) {
        DBG_ERR("config parser failed!!!\n");
        return 0;
    }

    if (debug_mode) {
        DBG_DUMP("total_image_num= %ld\r\n", test_config.test_img_num);
    }

    TEST_IMAGE_INFO *test_image_total = (TEST_IMAGE_INFO *)calloc(test_config.test_img_num,sizeof(TEST_IMAGE_INFO));
    int single_blob_mg_num = 0;
    if (test_image_total) {
        single_blob_mg_num = image_list_parser(test_config.image_list,test_image_total);
    } else {
        goto exit;
    }

    if (debug_mode) {
        DBG_DUMP("the test num of single blob models:%d\r\n", single_blob_mg_num);
    }

	//read model num from file
    //#if MODIFY_ENG_IN_SAMPLE
        // parse engine id for all cnn layer
    //    vendor_ais_pars_engid(1, cnn_id);
    //#endif


    CHAR model_path_tmp[256];
    CHAR command_test[256];
    if (access(test_config.gt_save_path,0) == 0) {
        sprintf(command_test,"rm -r %s",test_config.gt_save_path);
        system(command_test);

    }
    if (access(test_config.gt_save_path,0) == -1) {
        sprintf(command_test,"mkdir %s",test_config.gt_save_path);
        system(command_test);
    }
    CHAR save_error_path[256];
    CHAR result_txt_msg[256];
    sprintf(save_error_path,"%s","/mnt/sd/AI_auto_test_files/output/ai_test_0");
    if (access(save_error_path,0) == 0) {
        sprintf(result_txt_msg,"rm -rf %s",save_error_path);
        system(result_txt_msg);
    }
    if (access(save_error_path,0) == -1) {
        sprintf(result_txt_msg,"mkdir %s",save_error_path);
        system(result_txt_msg);
    }
    sprintf(save_error_path,"/mnt/sd/AI_auto_test_files/output/ai_test_0/error_0.txt");

    //FILE *efp = NULL;
    CHAR test_mpdel_type_path[256];
    for (UINT32 k = 0; k < test_model_type; k++) {
        if (k==0) {
            sprintf(test_mpdel_type_path,"%s",test_config.model_list);
            auto_updata_dim = 0;
        } else if(k==1) {
            sprintf(test_mpdel_type_path,"%s",test_config.model_list_multires);
            auto_updata_dim = 1;
        }
        FILE *fp = NULL;
    	fp = fopen(test_mpdel_type_path, "r");
        if (fp == NULL) {
        	DBG_ERR("open model_info fail.\r\n");
            if (test_image_total != NULL) {
                free(test_image_total);
            }
        	goto exit;
        }

        while (1) {
    		//read model name
            if (feof(fp)) {
                break;
            }
            if (fscanf(fp, "%s\n", model_name) == EOF) {
                if (feof(fp))
                    break;
                continue;
            }


            sprintf(model_path_tmp,"%s/%s/sdk",test_config.model_path,model_name);
            sprintf(command_test,"mkdir %s/%s",test_config.gt_save_path, model_name);
            system(command_test);
            if (debug_mode) {
                DBG_DUMP("\r\n========== %s ==========\r\n", model_name);
            }
            if (compute_max_mem(model_path_tmp, model_name,&test_config,test_image_total,single_blob_mg_num) < 0) {
                DBG_WRN("\r\n%s get memory failed!\r\n", model_name);
            }
        }
        //printf("final max mem:%d\r\n", max_mem);
        FILE *mem_fp = NULL;
        CHAR mem_txt_path[256];
        sprintf(mem_txt_path,"%s/memory_max.txt",test_config.gt_save_path);
        mem_fp = fopen(mem_txt_path, "a+");
        if (mem_fp == NULL) {
            DBG_ERR("open mem txt fail.\r\n");
        } else {
            fprintf(mem_fp,"%d\n",max_model_mem);
            fprintf(mem_fp,"%d\n",max_model_output_mem);
            fprintf(mem_fp,"%d\n",max_mem);
        }
        if (mem_fp) {
            fclose(mem_fp);
        }

        if (fp) {
		    fclose(fp);
		//fp = NULL;
	    }
        //while (1==1) {continue;}
    	fp = fopen(test_mpdel_type_path, "r");
        if (fp == NULL) {
        	DBG_ERR("open model_info fail.\r\n");
            if (test_image_total != NULL) {
                free(test_image_total);
            }
        	goto exit;
        }

    	while (1) {
    		//read model name
            if (feof(fp)) {
                break;
            }
            if (fscanf(fp, "%s\n", model_name) == EOF) {
                if (feof(fp))
                    break;
                continue;
            }

            if (debug_mode) {
                DBG_DUMP("\r\n========== %s ==========\r\n", model_name);
            }
            sprintf(model_path_tmp,"%s/%s/sdk",test_config.model_path,model_name);
            //if (app_nn_test_proc(model_path_tmp, model_name,test_config,&test_image_total[j],&efp,save_error_path,blob_num)){
            if (app_nn_test_proc(model_path_tmp, model_name,&test_config,test_image_total,single_blob_mg_num,save_error_path)) {
                DBG_ERR("model process fail.\r\n");
                continue;
            }

    	}
        if (fp) {
		    fclose(fp);
		//fp = NULL;
	    }

    }


    if (flag_all_test) {
        DBG_DUMP("success.\n");
    } else {
        DBG_ERR("failed.\n");
    }

    DBG_DUMP("ai_test_0 finished...\r\n");

	system("sync");


    if (test_image_total != NULL) {
        free(test_image_total);
    }

exit:

    //if (efp) {
	//	fclose(efp);
	//	efp = NULL;
	//}


	ret = hd_common_uninit();
	if (ret != HD_OK) {
		DBG_WRN("hd_common_uninit fail=%d\n", ret);
	}


	return ret;
}

