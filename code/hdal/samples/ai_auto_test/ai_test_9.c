/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/

#include <stdio.h>

#include <string.h>

#include <time.h>

////#include <pthread.h>

#include "hdal.h"
//#include "hd_type.h"
#include "hd_common.h"
//#include "nvtipc.h"
//#include "vendor_dsp_util.h"
//#include "vendor_ai/vendor_ai.h"

#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
//#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
//#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
//#include "net_flow_user_sample/net_layer_sample.h"
//#include "custnn/custnn_lib.h"
// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(alg_net_app_user_sample, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME

#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080

#define AI_SAMPLE_TEST_BATCH    ENABLE//v2 not support
#if AI_SAMPLE_TEST_BATCH
#define AI_SAMPLE_MAX_BATCH_NUM 8
#else
#define AI_SAMPLE_MAX_BATCH_NUM 1
#endif

#define MBYTE					(1024 * 1024)
#define NN_TOTAL_MEM_SIZE       (200 * MBYTE)
#define NN_RUN_NET_NUM			2
#define YUV_OUT_BUF_SIZE        (3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT * AI_SAMPLE_MAX_BATCH_NUM * NN_RUN_NET_NUM)

#define MAX_OBJ_NUM             1024
#define AUTO_UPDATE_DIM     	1
#define DYNAMIC_MEM             1
#define MODIFY_ENG_IN_SAMPLE 0
#define MEM_GET_BLOCK 1
#define VENDOR_AI_CFG  0x000f0000  //ai project config

#define GET_OUTPUT_LAYER_FEATURE_MAP DISABLE
#define CONFORMANCE_TEST_NUMBERS   10
static UINT32 conforence_test_numbers = 1;
static UINT32 debug_mode = 0;
static UINT32 cnn_id = 0;
static UINT32 flag_all_test[NN_RUN_NET_NUM] = {1};
static UINT32 flag_all_net[NN_RUN_NET_NUM] = {1};
#define RESULT_SAVE_DUMP 0


#define AI_STRCPY(dst, src, dst_size) do { \
strncpy(dst, src, (dst_size)-1); \
dst[(dst_size)-1] = '\0'; \
} while(0)


/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/**
	Parameters of network
*/
typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_list;
    CHAR *p_image_list;
    CHAR *p_model_path;
    CHAR *P_image_path;
    UINT32 test_img_num;
	VENDOR_AIS_IMG_PARM	src_img;
	VENDOR_AIS_FLOW_MEM_PARM mem;
    VENDOR_AIS_FLOW_MEM_PARM result_mem;
	UINT32 run_id;
} VENDOR_AIS_NETWORK_PARM;


typedef struct _TEST_LIST_PATH
{
	CHAR model_list[256];
	CHAR image_list[256];
    CHAR model_path[256];
    CHAR image_path[256];
    INT32 thread_num;
    UINT32 test_times;
    UINT32 test_img_num;
    UINT32 debug_mode;
} TEST_LIST_PATH;

typedef struct _CONFIG_OPT
{
	CHAR key[256];
	CHAR value[256];
} CONFIG_OPT;

typedef struct _TEST_IMAGE_INFO
{
    CHAR img_name[256];
	int img_w;
    int img_h;
    int ch;
    int bits;
	HD_VIDEO_PXLFMT img_fmt;
} TEST_IMAGE_INFO;


/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
/*
#if defined(__FREERTOS)
static CHAR model_name[NN_RUN_NET_NUM][256]	= { "A:\\para\\nvt_model.bin", "A:\\para\\nvt_model1.bin"};
#else
static CHAR model_name[NN_RUN_NET_NUM][256]	= { "para/nvt_model.bin", "para/nvt_model1.bin"};
#endif
*/
static UINT32 mem_size[NN_RUN_NET_NUM]		= { 30 * MBYTE};
static VENDOR_AIS_FLOW_MEM_PARM g_mem		= {0};
//static BOOL is_net_proc						= TRUE;
//static BOOL is_net_run[NN_SUPPORT_NET_MAX]	= {FALSE};
static HD_VIDEO_FRAME yuv_out_buffer;
static VOID *yuv_out_buffer_va = NULL;
static HD_VIDEO_FRAME yuv_out_buffer_tmp[NN_RUN_NET_NUM] = {0};
static VOID *yuv_out_buffer_va_tmp[NN_RUN_NET_NUM] = {NULL};
static VENDOR_AIS_FLOW_MEM_PARM yuv_scale_img[NN_RUN_NET_NUM] = {0};
static VENDOR_AIS_FLOW_MEM_PARM result_compare_mem[NN_RUN_NET_NUM] = {0};

static HD_COMMON_MEM_VB_BLK g_blk_info[2];
#if AUTO_UPDATE_DIM
static UINT32 diff_model_size = 0;
#if defined(__FREERTOS)
//static CHAR diff_model_name[NN_RUN_NET_NUM][256]	= { "A:\\para\\nvt_stripe_model.bin", "A:\\para\\nvt_stripe_model1.bin" };
#else
//static CHAR diff_model_name[NN_RUN_NET_NUM][256]	= { "para/nvt_stripe_model.bin", "para/nvt_stripe_model1.bin" };
#endif
#endif

//static pthread_mutex_t     lock;

static UINT32 width_range[13] = {102,480,480,960,960,1280,0,102,480,480,960,960,1280};
static UINT32 height_range[13] = {126,320,540,540,640,720,0,126,320,540,540,640,720};
//NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/
#if DYNAMIC_LABEL
HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, UINT32 label_num, const CHAR *filename)
#else
HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, const CHAR *filename)
#endif
{
	FILE *fd;
	CHAR *p_line = (CHAR *)addr;
#if DYNAMIC_LABEL
	UINT32 label_idx = 0;
#endif

	fd = fopen(filename, "r");
	if (!fd) {
		DBG_ERR("cannot read %s\r\n", filename);
		return HD_ERR_NG;
	}

	DBG_DUMP("open %s ok\r\n", filename);

	while (fgets(p_line, line_len, fd) != NULL) {
		p_line[strlen(p_line) - 1] = '\0'; // remove newline character
		p_line += line_len;
#if DYNAMIC_LABEL
		label_idx++;
		if (label_idx == label_num) {
			break;
		}
#endif
	}

	if (fd) {
		fclose(fd);
	}

	return HD_OK;
}


#if MEM_GET_BLOCK
static int mem_init(void)
{
	HD_RESULT                 ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};
	UINT32                    pa, va;
	//HD_COMMON_MEM_DDR_ID      ddr_id0 = DDR_ID0;
    HD_COMMON_MEM_DDR_ID      ddr_id1 = DDR_ID1;
	HD_COMMON_MEM_VB_BLK      blk;
	UINT32                    total_mem_size = 0;
#if DYNAMIC_MEM
	UINT32                    i = 0;
#endif

	/* Allocate parameter buffer */
	if (g_mem.va != 0) {
		DBG_WRN("err: mem has already been inited\r\n");
		return -1;
	}

#if DYNAMIC_MEM
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif
    total_mem_size = total_mem_size + YUV_OUT_BUF_SIZE + 30 * MBYTE;


	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = total_mem_size;
	mem_cfg.pool_info[0].blk_cnt = 1;
	mem_cfg.pool_info[0].ddr_id = DDR_ID1;
	mem_cfg.pool_info[1].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[1].blk_size = YUV_OUT_BUF_SIZE;
	mem_cfg.pool_info[1].blk_cnt = 1;
	mem_cfg.pool_info[1].ddr_id = DDR_ID1;

	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		DBG_ERR("hd_common_mem_init err: %d\r\n", ret);
		return ret;
	}

	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, total_mem_size, ddr_id1);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		DBG_ERR("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		DBG_ERR("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, total_mem_size);
	g_blk_info[0] = blk;

	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_mem_size);
		if (ret != HD_OK) {
			DBG_ERR("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = total_mem_size;

	/* Allocate scale out buffer */
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, YUV_OUT_BUF_SIZE, ddr_id1);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		DBG_ERR("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	yuv_out_buffer.phy_addr[0] = hd_common_mem_blk2pa(blk);
	if (yuv_out_buffer.phy_addr[0] == 0) {
		DBG_ERR("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			DBG_WRN("yuv_out_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	yuv_out_buffer_va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
						  yuv_out_buffer.phy_addr[0],
						  YUV_OUT_BUF_SIZE);
	g_blk_info[1] = blk;
    if (debug_mode) {
        DBG_DUMP("Allocate yuv_out_buffer pa(%#lx) va(%p)\n",
                yuv_out_buffer.phy_addr[0], yuv_out_buffer_va);
    }


exit:
	return ret;
}

static HD_RESULT mem_uninit(void)
{
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, g_mem.size);
		if (ret != HD_OK) {
			DBG_ERR("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		DBG_ERR("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release scale out buffer */
	if (yuv_out_buffer_va) {
		ret = hd_common_mem_munmap(yuv_out_buffer_va, YUV_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			DBG_ERR("mem_uninit : (yuv_out_buffer_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)yuv_out_buffer.phy_addr[0]);
	ret = hd_common_mem_release_block(g_blk_info[1]);
	if (ret != HD_OK) {
		DBG_ERR("mem_uninit : (yuv_out_buffer.phy_addr[0])hd_common_mem_release_block fail.\r\n");
		return ret;
	}
	if (g_mem.va) {
		g_mem.va = 0;
	}


	return hd_common_mem_uninit();
}
#else
static int mem_init(void)
{
	HD_RESULT                 ret;
	UINT32                    pa, va;
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_ID0;
    HD_COMMON_MEM_POOL_TYPE   pool_type = HD_COMMON_MEM_CNN_POOL;
	HD_COMMON_MEM_POOL_INFO   mem_info = { 0 };
    UINT32                    total_mem_size = 0;
#if DYNAMIC_MEM
	UINT32                    i = 0;
#endif
	/* Allocate parameter buffer */
	if (g_mem.va != 0) {
		DBG_WRN("err: mem has already been inited\r\n");
		return -1;
	}

#if DYNAMIC_MEM
	for (i = 0; i < NN_RUN_NET_NUM; i++) {
		total_mem_size += mem_size[i];
	}
#else
	total_mem_size = NN_TOTAL_MEM_SIZE;
#endif
    total_mem_size = total_mem_size + 30 * MBYTE;

	ret = HD_OK;
	mem_info.type = pool_type;
	mem_info.ddr_id = ddr_id;
	if (hd_common_mem_get(HD_COMMON_MEM_PARAM_POOL_CONFIG, (VOID *)&mem_info) != HD_OK) {
		DBG_DUMP("hd_common_mem_get (CNN) fail\r\n");
		return HD_ERR_NOBUF;
	}
    pa = mem_info.start_addr;
    if (pa == 0) {
		DBG_DUMP("not get buffer, pa=0x%08x\r\n", (int)pa);
		return HD_ERR_NOBUF;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, total_mem_size+YUV_OUT_BUF_SIZE);
	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, total_mem_size+YUV_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			DBG_ERR("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = total_mem_size;
    yuv_out_buffer.phy_addr[0] = g_mem.pa + total_mem_size;
    yuv_out_buffer_va = (void *)(g_mem.va + total_mem_size);

	/* Allocate scale out buffer */
    if (debug_mode) {
	    DBG_DUMP("Allocate yuv_out_buffer pa(%#lx) va(%p)\n",
			    yuv_out_buffer.phy_addr[0], yuv_out_buffer_va);
    }

exit:
	return ret;
}

static HD_RESULT mem_uninit(void)
{
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (g_mem.va!=0 && yuv_out_buffer_va!=0 ) {
		ret = hd_common_mem_munmap((void *)g_mem.va, g_mem.size+YUV_OUT_BUF_SIZE + 30 * MBYTE);
		if (ret != HD_OK) {
			DBG_ERR("mem_uninit : hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}

	//return hd_common_mem_uninit();
	return HD_OK;
}
#endif


static INT32 file_loadbin(UINT32 addr, const CHAR *filename)
{
	FILE *fd;
	INT32 size = 0;

	fd = fopen(filename, "rb");

	if (NULL == fd) {
		DBG_ERR("cannot read %s\r\n", filename);
		return -1;
	}
    if (debug_mode) {
        DBG_IND("open %s ok\r\n", filename);
    }


	fseek(fd, 0, SEEK_END);
	size = ftell(fd);
	fseek(fd, 0, SEEK_SET);

	if (size < 0) {
		DBG_ERR("getting %s size failed\r\n", filename);
	} else if ((INT32)fread((VOID *)addr, 1, size, fd) != size) {
		DBG_ERR("read size < %ld\r\n", size);
		size = -1;
	}

	if (fd) {
		fclose(fd);
	}

	return size;
}

static HD_RESULT ai_load_img(CHAR *filename, VENDOR_AIS_IMG_PARM *p_img,UINT32 net_id)
{
	INT32 file_len;
	UINT32 key = 0;

	file_len = file_loadbin((UINT32)(yuv_out_buffer_va_tmp[net_id]), filename);
	if (file_len < 0) {
		return HD_ERR_IO;
	}

#if AI_SUPPORT_MULTI_FMT
	p_img->fmt_type = 0;
#endif

	if (p_img->width == 0) {
		while (TRUE) {
			DBG_DUMP("Enter width = ?\r\n");
			if (scanf("%hu", &p_img->width) != 1) {
				DBG_ERR("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->height == 0) {
		while (TRUE) {
			DBG_DUMP("Enter height = ?\r\n");
			if (scanf("%hu", &p_img->height) != 1) {
				DBG_ERR("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->channel == 0) {
		while (TRUE) {
			DBG_DUMP("Enter channel = ?\r\n");
			if (scanf("%hu", &p_img->channel) != 1) {
				DBG_ERR("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->line_ofs == 0) {
		while (TRUE) {
			DBG_DUMP("Enter line offset = ?\r\n");
			if (scanf("%lu", &p_img->line_ofs) != 1) {
				DBG_ERR("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->fmt == 0) {
		while (TRUE) {
			DBG_DUMP("Enter image format = ?\r\n");
			DBG_DUMP("[%d] RGB888 (0x%08x)\r\n", NET_IMG_RGB_PLANE, HD_VIDEO_PXLFMT_RGB888_PLANAR);
			DBG_DUMP("[%d] YUV420 (0x%08x)\r\n", NET_IMG_YUV420, HD_VIDEO_PXLFMT_YUV420);
			DBG_DUMP("[%d] YUV422 (0x%08x)\r\n", NET_IMG_YUV422, HD_VIDEO_PXLFMT_YUV422);
			DBG_DUMP("[%d] Y only (0x%08x)\r\n", NET_IMG_YONLY, HD_VIDEO_PXLFMT_Y8);
#if AI_SUPPORT_MULTI_FMT
			DBG_DUMP("[%d] YUV420_NV21 (0x%08x)\r\n", NET_IMG_YUV420_NV21, HD_VIDEO_PXLFMT_YUV420+1);
#endif

			if (scanf("%lu", &key) != 1) {
				DBG_ERR("Wrong input\n");
				continue;
			} else if (key == NET_IMG_YUV420) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
			} else if (key == NET_IMG_YUV422) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV422;
			} else if (key == NET_IMG_RGB_PLANE) {
				p_img->fmt = HD_VIDEO_PXLFMT_RGB888_PLANAR;
			} else if (key == NET_IMG_YONLY) {
				p_img->fmt = HD_VIDEO_PXLFMT_Y8;
#if AI_SUPPORT_MULTI_FMT
			} else if (key == NET_IMG_YUV420_NV21) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
				p_img->fmt_type = 1;
#endif
			} else {
				DBG_ERR("Wrong enter format\n");
				continue;
			}
			break;
		}
#if AI_SUPPORT_MULTI_FMT
	} else if (p_img->fmt == HD_VIDEO_PXLFMT_YUV420 + 1) {
		p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
		p_img->fmt_type = 1;
#endif
	}
	p_img->pa = yuv_out_buffer_tmp[net_id].phy_addr[0];
	p_img->va = (UINT32)(yuv_out_buffer_va_tmp[net_id]);
	return HD_OK;
}

HD_RESULT yuv_input_scale(HD_GFX_IMG_BUF *imgin, HD_GFX_IMG_BUF *imgout, HD_GFX_SCALE_QUALITY method)
{
	HD_RESULT ret;
	HD_GFX_SCALE param = {0};
    UINT32 i;

	param.src_img.dim.w            = imgin->dim.w;
    param.src_img.dim.h            = imgin->dim.h;
	param.src_img.format           = imgin->format;
	param.src_img.p_phy_addr[0]    = imgin->p_phy_addr[0];
	param.src_img.p_phy_addr[1]    = imgin->p_phy_addr[1];
	param.src_img.p_phy_addr[2]    = imgin->p_phy_addr[2];
	param.src_img.lineoffset[0]    = imgin->lineoffset[0];
	param.src_img.lineoffset[1]    = imgin->lineoffset[1];
	param.src_img.lineoffset[2]    = imgin->lineoffset[2];

	param.dst_img.dim.w            = imgout->dim.w;
    param.dst_img.dim.h            = imgout->dim.h;
	param.dst_img.format           = imgout->format;
	param.dst_img.p_phy_addr[0]    = imgout->p_phy_addr[0];
	param.dst_img.p_phy_addr[1]    = imgout->p_phy_addr[1];
	param.dst_img.p_phy_addr[2]    = imgout->p_phy_addr[2];
	param.dst_img.lineoffset[0]    = imgout->lineoffset[0];
	param.dst_img.lineoffset[1]    = imgout->lineoffset[1];
	param.dst_img.lineoffset[2]    = imgout->lineoffset[2];

	param.src_region.x             = 0;
	param.src_region.y             = 0;
	param.src_region.w             = imgin->dim.w;
	param.src_region.h             = imgin->dim.h;

	param.dst_region.x             = 0;
	param.dst_region.y             = 0;
	param.dst_region.w             = imgout->dim.w;
	param.dst_region.h             = imgout->dim.h;

	param.quality				   = method;

    if (imgin->format == HD_VIDEO_PXLFMT_RGB888_PLANAR) {
        for (i = 0; i < 3; i++) {
            param.src_img.format        = HD_VIDEO_PXLFMT_Y8;
            param.src_img.p_phy_addr[0] = imgin->p_phy_addr[i];
            param.dst_img.format        = HD_VIDEO_PXLFMT_Y8;
            param.dst_img.p_phy_addr[0] = imgout->p_phy_addr[i];

            ret = hd_gfx_scale(&param);
            if (HD_OK != ret) {
                DBG_ERR("hd_gfx_scale fail\r\n");
            }
        }
    } else {
        ret = hd_gfx_scale(&param);
        if (HD_OK != ret) {
            DBG_ERR("hd_gfx_scale fail\r\n");
        }
    }
    return ret;
}

static VOID image_list_parser(CHAR *file_path, TEST_IMAGE_INFO *test_image_total,UINT32 image_num)
{
       int img_w = 0, img_h = 0, ch = 0, bits = 0;
       CHAR fmt[50] = { 0 };
       HD_VIDEO_PXLFMT img_fmt = 0;
       CHAR img_path[256] = { 0 };
       FILE* fp_img_tmp= NULL;
       //UINT32 image_num;
       fp_img_tmp = fopen(file_path, "r");
       if (fp_img_tmp == NULL) {
           DBG_ERR("open test_images_list_info fail.\r\n");
       } else {

           for (UINT32 i=0; i<image_num; i++) {
               if (fscanf(fp_img_tmp, "%s %d %d %d %d %s\n", img_path, &img_w, &img_h, &ch, &bits, fmt) == EOF) {
                   continue;
               }
               if (img_w <= 0) {
                   DBG_ERR("img_w read fail\r\n");
                   //goto exit;
               }
               if (img_h <= 0) {
                   DBG_ERR("img_h read fail\r\n");
                   //goto exit;
               }
               if (ch <= 0) {
                   DBG_ERR("ch read fail\r\n");
                   //goto exit;
               }
               if (bits <=0 || bits > 32 || bits % 8 != 0) {
                   DBG_ERR("bits read fail\r\n");
                   //goto exit;
               }
               if (strcmp(fmt, "HD_VIDEO_PXLFMT_YUV420")== 0) {
                   img_fmt = 0x520c0420;
               } else if (strcmp(fmt, "HD_VIDEO_PXLFMT_Y8") == 0) {
                   img_fmt = 0x51080400;
               } else if (strcmp(fmt, "HD_VIDEO_PXLFMT_RGB888_PLANAR") == 0) {
                   img_fmt = 0x23180888;
               } else {
                   DBG_ERR("fmt not enough.\r\n");

                   //more see hd_type.h
               }

               test_image_total[i].img_w = img_w;
               test_image_total[i].img_h = img_h;
               test_image_total[i].ch = ch;
               test_image_total[i].bits= bits;
               AI_STRCPY(test_image_total[i].img_name,img_path,sizeof(test_image_total[i].img_name));
               test_image_total[i].img_fmt= img_fmt;
           }

       }
       if (fp_img_tmp) {
           fclose(fp_img_tmp);
           //fp_img_tmp = NULL;
       }

}

#if RESULT_SAVE_DUMP
static BOOL vendor_ais_write_file(CHAR *filepath, UINT32 addr, UINT32 size)
{
#if NET_FLOW_USR_SAMPLE_LAYER_OUT
	FILE *fsave = NULL;

	fsave = fopen(filepath, "wb");
	if (fsave == NULL) {
		DBG_ERR("fopen fail\n");
		return FALSE;
	}

	fwrite((UINT8 *)addr, size, 1, fsave);

	fclose(fsave);
#endif
	return TRUE;
}
#endif
typedef struct _TEST_SCALE_PARM
{
	UINT32 random_tmp_w;
    UINT32 random_tmp_h;
    VENDOR_AIS_IMG_PARM p_src_img;
}TEST_SCALE_PARM;

static VOID random_scale(VENDOR_AIS_IMG_PARM *p_img, UINT32 net_id, UINT32 ii, int bits,TEST_SCALE_PARM *test_scale_rgb)
{

      HD_GFX_IMG_BUF input_image;
      HD_GFX_IMG_BUF input_scale_image;
      UINT32 random_tmp_w = 0;
      UINT32 random_tmp_h = 0;
      int min_value_w = 0;
      int min_value_h = 0;
      srand(time(0));

      input_image.dim.w = p_img->width;
      input_image.dim.h = p_img->height;
      input_image.format = HD_VIDEO_PXLFMT_RGB888_PLANAR;
      input_image.p_phy_addr[0] = p_img->pa;
      input_image.p_phy_addr[1] = input_image.p_phy_addr[0] + p_img->width*p_img->height;
      input_image.p_phy_addr[2] = input_image.p_phy_addr[1] + p_img->width*p_img->height;
      input_image.lineoffset[0] = ALIGN_CEIL_4(input_image.dim.w);
      input_image.lineoffset[1] = ALIGN_CEIL_4(input_image.dim.w);
      input_image.lineoffset[2] = ALIGN_CEIL_4(input_image.dim.w);





      if (ii < 6) {
          random_tmp_w = width_range[ii];
          random_tmp_h = height_range[ii];
      } else {
          min_value_w = (width_range[ii + 1] + width_range[ii]) / 2;
          min_value_h = (height_range[ii + 1] + height_range[ii]) / 2;
          if (width_range[ii + 1] == width_range[ii]) {
              random_tmp_w = width_range[ii + 1];
          } else {
              random_tmp_w = rand() % (width_range[ii + 1] - min_value_w) + min_value_w;
          }
          if (height_range[ii + 1] == height_range[ii]) {
              random_tmp_h = height_range[ii + 1];
          } else {
              random_tmp_h = rand() % (height_range[ii + 1] - min_value_h) + min_value_h;
          }
      }


      if (random_tmp_w <= (input_image.dim.w)/16) {
          random_tmp_w = (input_image.dim.w)/16+1;
      }
      if (random_tmp_h <= (input_image.dim.h)/16) {
          random_tmp_h = (input_image.dim.h)/16+1;
      }


      random_tmp_w = random_tmp_w % 2 ? (random_tmp_w + 1) : random_tmp_w;
      random_tmp_h = random_tmp_h % 2 ? (random_tmp_h + 1) : random_tmp_h;


        //random_tmp_w = 960;
        //random_tmp_h = 540;
      if (debug_mode) {
          DBG_DUMP("width random:%d\r\n", random_tmp_w);
          DBG_DUMP("height random:%d\r\n", random_tmp_h);
      }

      input_scale_image.dim.w = random_tmp_w;
      input_scale_image.dim.h = random_tmp_h;
      input_scale_image.format = HD_VIDEO_PXLFMT_RGB888_PLANAR;
      input_scale_image.p_phy_addr[0] = yuv_scale_img[net_id].pa;
      input_scale_image.p_phy_addr[1] = input_scale_image.p_phy_addr[0] + ALIGN_CEIL_4(random_tmp_w)*random_tmp_h;
      input_scale_image.p_phy_addr[2] = input_scale_image.p_phy_addr[1] + ALIGN_CEIL_4(random_tmp_w)*random_tmp_h;
      input_scale_image.lineoffset[0] = ALIGN_CEIL_4(input_scale_image.dim.w);
      input_scale_image.lineoffset[1] = ALIGN_CEIL_4(input_scale_image.dim.w);
      input_scale_image.lineoffset[2] = ALIGN_CEIL_4(input_scale_image.dim.w);

      yuv_input_scale(&input_image,&input_scale_image,HD_GFX_SCALE_QUALITY_BILINEAR);

      VENDOR_AIS_IMG_PARM p_src_img_tmp = {0};
      p_src_img_tmp.width = input_scale_image.dim.w;
      p_src_img_tmp.height = input_scale_image.dim.h;
      p_src_img_tmp.channel = p_img->channel;
      p_src_img_tmp.line_ofs = input_scale_image.dim.w*bits/8;
      p_src_img_tmp.fmt = p_img->fmt;
      p_src_img_tmp.pa = yuv_scale_img[net_id].pa;
      p_src_img_tmp.va = yuv_scale_img[net_id].va;
      test_scale_rgb->p_src_img= p_src_img_tmp;
      test_scale_rgb->random_tmp_w = random_tmp_w;
      test_scale_rgb->random_tmp_h = random_tmp_h;


}
static INT32 result_compare(UINT8 *p1, UINT8 *p2, UINT32 size)
{
    UINT32 flag;
    for (UINT32 t = 0; t<size; t++) {
        if (p1[t] == p2[t]) {
            flag = 1;
        } else {
            flag = 0;
            break;
        }
    }
    return flag;

}



static VOID write_err_txt(CHAR *save_error_path, CHAR *model_name,int cnn_id,
                          CHAR*img_name,UINT32 random_tmp_w, UINT32 random_tmp_h,UINT32 kk,int error_type)
{
    FILE *efp = NULL;
    efp = fopen(save_error_path, "a+");
    if (efp == NULL) {
        DBG_ERR("open error txt fail.\r\n");
    } else {
        fprintf(efp,"%s,%d,%s,%d,%d,%d,%d\n",model_name,cnn_id,img_name,random_tmp_w,random_tmp_h,kk,error_type);

    }
	if (efp) {
		fclose(efp);
		//efp = NULL;
	}


}
static VOID *nn_thread_api(VOID *arg)
{
	HD_RESULT ret;
	VENDOR_AIS_NETWORK_PARM *p_net_parm = (VENDOR_AIS_NETWORK_PARM*)arg;
	VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem = &p_net_parm->mem;
	UINT32 run_id = p_net_parm->run_id;
    UINT32 load_addr = p_tot_mem->va;


    //NN_IOMEM first_iomem;
    if (debug_mode) {
        DBG_DUMP("net id:%d, address pa start:%d, va:%d, size:%d\r\n", run_id, p_tot_mem->pa,p_tot_mem->va, p_tot_mem->size);
    }


    UINT32 net_id = run_id;


    CHAR *model_list;
    CHAR *model_path;
    CHAR *image_list;
    CHAR *image_path;
    model_list = p_net_parm->p_model_list;
    image_list= p_net_parm->p_image_list;
    model_path = p_net_parm->p_model_path;
    image_path= p_net_parm->P_image_path;


    //UINT32 model_num = p_net_parm->test_img_num;;

    //CHAR model_path_file[256];

    //fscanf(fp, "total_model_num = %ld\n\n", &model_num);
    //printf("total_model_num= %ld\r\n", model_num);

	CHAR model_name[256] = { 0 };
	CHAR img_path[256] = { 0 };
  	int img_w = 0, img_h = 0, ch = 0, bits = 0;
    HD_VIDEO_PXLFMT img_fmt = 0;

	UINT32 random_tmp_w = 0;
	UINT32 random_tmp_h = 0;



     //FILE *fp_image = NULL;
     //fp_image = fopen(image_list, "r");
     //if (fp_image == NULL) {
      //   printf("open image_info fail.\r\n");
     //}
     //fscanf(fp_image, "total_image_num = %ld\n\n", &image_num);
     //printf("total_image_num= %ld\r\n", image_num);
     //fclose(fp_image);
     UINT32 image_num = p_net_parm->test_img_num;
     TEST_IMAGE_INFO *test_image_total = (TEST_IMAGE_INFO *)malloc(image_num*sizeof(TEST_IMAGE_INFO));
     image_list_parser(image_list,test_image_total,image_num);

     CHAR model_path_file[256];
     CHAR diff_model_path_file[256];
     #if RESULT_SAVE_DUMP
     CHAR image_name_rest[256];
     CHAR folder_tmp[256];
     CHAR command_test[256];

     #endif


     CHAR save_error_path[256];
     sprintf(save_error_path,"/mnt/sd/AI_auto_test_files/output/ai_test_9/error_%ld.txt",net_id);

     FILE *fp = NULL;
     fp = fopen(model_list, "r");
     if (fp == NULL) {
         DBG_ERR("open model_info fail.\r\n");
     } else { /*************************starting loop models****************************/

        while (1) {
                if (feof(fp)) {
                    break;
                }
                if (fscanf(fp, "%s\n", model_name) == EOF) {
                    if (feof(fp)) {
                        break;
                    }
                    continue;
                }

                sprintf(model_path_file,"%s/%s/sdk/nvt_model.bin",model_path,model_name);
                sprintf(diff_model_path_file,"%s/%s/sdk/nvt_stripe_model.bin",model_path,model_name);

                 VENDOR_AIS_FLOW_MEM_PARM rslt_mem;
                 UINT32 file_size = 0, model_size = 0;
                 VENDOR_AIS_FLOW_MEM_PARM model_mem;
                 UINT32 req_size = 0;
                 NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
                 UINT32 num_output_layer = 0;
                 UINT32 i = 0;
                 VENDOR_AIS_FLOW_MAP_MEM_PARM mem_manager;
             #if !CNN_25_MATLAB
                     NN_IN_OUT_FMT *input_info = NULL;
             #endif
             #if AI_SAMPLE_TEST_BATCH
                     UINT32 input_proc_num = 0;
             #endif
             #if AUTO_UPDATE_DIM
                     VENDOR_AIS_DIFF_MODEL_INFO diff_info = {0};
                     VENDOR_AIS_FLOW_MEM_PARM diff_model_mem = {0};
             #endif
                 NN_DATA output_layer_iomem = {0};

                 //load file
                 //file_size = ai_load_file(p_net_parm->p_model_path, load_addr);
                 file_size = vendor_ais_load_file(model_path_file, load_addr, &output_layer_info, &num_output_layer);
                 if (file_size == 0) {
                   DBG_ERR("net load file fail: %s\r\n", model_path_file);
                   flag_all_net[net_id] = flag_all_net[net_id]*0;
                   write_err_txt(save_error_path,model_name,cnn_id,NULL,0,0,0,1);
                   continue;
                 }
                 //allocate memory (parm, model, io buffer)
                 model_size      = vendor_ais_auto_alloc_mem(p_tot_mem, &mem_manager);
                 model_mem.pa    = p_tot_mem->pa;
                 model_mem.va    = p_tot_mem->va;    // = load_addr
                 model_mem.size  = model_size;
                 rslt_mem.pa     = model_mem.pa + model_mem.size;
                 rslt_mem.va     = model_mem.va + model_mem.size;
                 rslt_mem.size   = sizeof(VENDOR_AIS_RESULT_INFO) + MAX_OBJ_NUM * sizeof(VENDOR_AIS_RESULT);
                 req_size        = model_mem.size + rslt_mem.size;
                 if (model_size > p_tot_mem->size) {
                   DBG_ERR("model memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)model_size);
                   if (output_layer_info != NULL) {
                       free(output_layer_info);
                   }
                   flag_all_net[net_id] = flag_all_net[net_id]*0;
                   write_err_txt(save_error_path,model_name,cnn_id,NULL,0,0,0,1);
                   continue;
                 }
                 if (req_size > p_tot_mem->size) {
                   DBG_ERR("require memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)req_size);
                   if (output_layer_info != NULL) {
                       free(output_layer_info);
                   }
                   flag_all_net[net_id] = flag_all_net[net_id]*0;
                   write_err_txt(save_error_path,model_name,cnn_id,NULL,0,0,0,1);
                   continue;
                 }
             #if !CNN_25_MATLAB
                    ret = vendor_ais_get_net_input_info(&input_info, model_mem);
                    if (ret == 0) {
                        if (debug_mode) {
                            DBG_DUMP("model_fmt = %s\n", input_info->model_fmt);
                            DBG_DUMP("model_width = %d\n", input_info->model_width);
                            DBG_DUMP("model_height = %d\n", input_info->model_height);
                            DBG_DUMP("model_channel = %d\n", input_info->model_channel);
                            DBG_DUMP("model_batch = %d\n", input_info->model_batch);
                            DBG_DUMP("in_fmt = %s\n", input_info->in_fmt);
                            DBG_DUMP("in_channel = %d\n", input_info->in_channel);
                        }

                    } else {
                        flag_all_net[net_id] = flag_all_net[net_id]*0;
                        DBG_ERR("Get net input info fail.\r\n");
                        write_err_txt(save_error_path,model_name,cnn_id,NULL,0,0,0,1);
                        continue;
                    }
             #endif

                 memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
                 memset((VOID *)rslt_mem.va, 0, rslt_mem.size);    // clear result buffer

                 // map user/kernel memory
                 //pthread_mutex_lock(&lock);
                 ret = vendor_ais_net_gen_init(mem_manager, net_id);
                 //pthread_mutex_unlock(&lock);
                 if (ret != HD_OK) {
                   flag_all_net[net_id] = flag_all_net[net_id]*0;
                   DBG_ERR("net gen init fail=%d\r\n", ret);
                   write_err_txt(save_error_path,model_name,cnn_id,NULL,0,0,0,1);
                   goto gen_init_fail;
                 } else {
                   if (debug_mode) {
                       DBG_DUMP("[ai sample] init done: %s!\n", model_path_file);
                   }
                 }
            #if MODIFY_ENG_IN_SAMPLE
                          //parse engine id for all cnn layer
                          vendor_ais_pars_engid(cnn_id, net_id);
            #endif

            #if AUTO_UPDATE_DIM
                   diff_model_mem.pa = rslt_mem.pa + rslt_mem.size;
                   diff_model_mem.va = rslt_mem.va + rslt_mem.size;
                   diff_model_mem.size = diff_model_size;
                   file_size = vendor_ais_load_file(diff_model_path_file, diff_model_mem.va, NULL, &num_output_layer);
                   if (file_size == 0) {
                       DBG_ERR("net load diff model file fail: %s\r\n", diff_model_path_file);
                       if (output_layer_info != NULL) {
                           free(output_layer_info);
                       }
                       flag_all_net[net_id] = flag_all_net[net_id]*0;
                       write_err_txt(save_error_path,model_name,cnn_id,NULL,0,0,0,1);
                       goto gen_init_fail;
                   }
            #endif

                 /*************************starting loop images***************************/
                 for (UINT32 jj=0; jj<image_num; jj++) {

                     sprintf(img_path,"%s/%s",image_path,test_image_total[jj].img_name);
                     img_w = test_image_total[jj].img_w;
                     img_h = test_image_total[jj].img_h;
                     ch = test_image_total[jj].ch;
                     bits = test_image_total[jj].bits;
                     img_fmt = test_image_total[jj].img_fmt;

                     p_net_parm->src_img.width = img_w;
                     p_net_parm->src_img.height = img_h;
                     p_net_parm->src_img.channel = ch;
                     p_net_parm->src_img.line_ofs = img_w * bits /8;
                     p_net_parm->src_img.fmt = img_fmt;

                     ret = ai_load_img(img_path, &p_net_parm->src_img,net_id);
                     if (ret != HD_OK) {
                         DBG_ERR("ai_load_img fail=%d\n", ret);
                         flag_all_net[net_id] = flag_all_net[net_id]*0;
                         write_err_txt(save_error_path,model_name,cnn_id,NULL,0,0,0,2);
                         continue;
                     }

                #if RESULT_SAVE_DUMP
                         strncpy(image_name_rest,test_image_total[jj].img_name,strlen(test_image_total[jj].img_name)-4);
                         sprintf(folder_tmp, "output_bin_%d/%s_%s",net_id,model_name,image_name_rest);
                         if (access(folder_tmp,0) == -1) {
                             sprintf(command_test, "mkdir %s", folder_tmp);
                             system(command_test);
                         }
                 #endif
                     /************************starting loop multi resolution(resize)***************************/
                     for (int ii = 0; ii < 12; ii++) {


                        TEST_SCALE_PARM test_scale_rgb;
                        random_scale(&p_net_parm->src_img, net_id, ii, bits,&test_scale_rgb);

                        random_tmp_w = test_scale_rgb.random_tmp_w;
                        random_tmp_h = test_scale_rgb.random_tmp_h;
                        VENDOR_AIS_IMG_PARM *p_src_img= &(test_scale_rgb.p_src_img);

                    #if RESULT_SAVE_DUMP
                            sprintf(folder_tmp, "output_bin_%d/%s_%s/size_%dx%d_%d",net_id,model_name,image_name_rest,random_tmp_w,random_tmp_h,ii);
                            if (access(folder_tmp,0) == -1) {
                                sprintf(command_test, "mkdir %s", folder_tmp);
                                system(command_test);
                            }
                    #endif


                    #if AUTO_UPDATE_DIM
                          diff_info.input_width = p_src_img->width;
                          diff_info.input_height = p_src_img->height;
                          diff_info.id = 0;
                          ret = vendor_ais_pars_diff_mem(&mem_manager, &diff_model_mem, &diff_info, net_id);
                          if (ret != HD_OK) {
                              DBG_ERR("vendor_ais_pars_diff_mem (%d)\n\r", ret);
                              //if (output_layer_info != NULL) {
                              //    free(output_layer_info);
                             // }
                              flag_all_net[net_id] = flag_all_net[net_id]*0;
                              write_err_txt(save_error_path,model_name,cnn_id,test_image_total[jj].img_name,random_tmp_w,random_tmp_h,0,2);
                              vendor_ais_unpars_diff_mem(&mem_manager, &diff_model_mem, &diff_info, net_id);
                              break;
                          }
                    #endif

                    #if AI_SAMPLE_TEST_BATCH
                        {
                          // test for get input layers:
                          UINT32 input_proc_idx[AI_SAMPLE_MAX_BATCH_NUM];
                          UINT32 tmp_src_va = p_src_img->va;
                          UINT32 tmp_src_pa = p_src_img->pa;
                          UINT32 src_img_size = p_src_img->width * p_src_img->height * p_src_img->channel;

                          input_proc_num = vendor_ais_net_get_input_layer_index(model_mem, input_proc_idx);
                            for (i = 0; i < input_proc_num; i++) {
                              if (i==0) {
                                  hd_common_mem_flush_cache((VOID *)(p_src_img->va), src_img_size);
                              }
                              if (i > 0) {
                                  memcpy((VOID*)(p_src_img->va + src_img_size), (VOID*)p_src_img->va, src_img_size);
                                  hd_common_mem_flush_cache((VOID *)(p_src_img->va + src_img_size), src_img_size);
                                  p_src_img->pa += src_img_size;
                                  p_src_img->va += src_img_size;
                              }
                              if (vendor_ais_net_input_layer_init(p_src_img, i, net_id) != HD_OK) {
                                   flag_all_net[net_id] = flag_all_net[net_id]*0;
                                   write_err_txt(save_error_path,model_name,cnn_id,test_image_total[jj].img_name,random_tmp_w,random_tmp_h,0,2);
                                   goto input_init_exit;
                              }
                          }
                          p_src_img->va = tmp_src_va;
                          p_src_img->pa = tmp_src_pa;
                        }
                    #else
                              if (vendor_ais_net_input_init(p_src_img, net_id) != HD_OK) {
                                  flag_all_net[net_id] = flag_all_net[net_id]*0;
                                  write_err_txt(save_error_path,model_name,cnn_id,test_image_total[jj].img_name,random_tmp_w,random_tmp_h,0,2);
                                  goto input_init_exit;
                              }
                    #endif

                        VENDOR_AIS_FLOW_MEM_PARM result_mem = p_net_parm->result_mem;
                        UINT32 result_mem_start = result_mem.va;

                        //************************starting loop test conforence***************************
                        for (UINT32 kk = 0; kk < conforence_test_numbers ; kk++) {

                            if (debug_mode) {
                                DBG_DUMP("\r\n========== %s,%d,%d,%d,%d ==========\r\n", model_name,jj,net_id,ii,kk);
                            }

                            UINT32 compare_start_address = result_mem_start;
                            /*
                        #if RESULT_SAVE_DUMP
                                sprintf(folder_tmp, "output%d",net_id);
                                if (access(folder_tmp,0) == -1){
                                    sprintf(command_test, "mkdir %s", folder_tmp);
                                    system(command_test);
                                }
                        #endif*/
                        #if RESULT_SAVE_DUMP
                                sprintf(folder_tmp, "output_bin_%d/%s_%s/size_%dx%d_%d/%d",net_id,model_name,image_name_rest,random_tmp_w,random_tmp_h,ii,kk);
                                if (access(folder_tmp,0) == -1) {
                                    sprintf(command_test, "mkdir %s", folder_tmp);
                                    system(command_test);
                                }
                                #endif

                            memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
                            if (hd_common_mem_flush_cache((VOID *)mem_manager.user_buff.va, mem_manager.user_buff.size) != HD_OK) {
                                flag_all_net[net_id] = flag_all_net[net_id]*0;
                                write_err_txt(save_error_path,model_name,cnn_id,test_image_total[jj].img_name,random_tmp_w,random_tmp_h,kk,3);
                                break;

                            }

                        #if USE_NEON
                                ret = vendor_ais_proc_net(model_mem, rslt_mem, p_src_img, net_id);
                        #else
                                ret = vendor_ais_proc_net(model_mem, rslt_mem, net_id);
                        #endif
                            if (ret != HD_OK) {
                                flag_all_net[net_id] = flag_all_net[net_id]*0;
                                write_err_txt(save_error_path,model_name,cnn_id,test_image_total[jj].img_name,random_tmp_w,random_tmp_h,kk,3);
                                break;
                            }
                            //**********************start save all output node layers***************************
                            for (UINT32 i= 0; i<num_output_layer; i++) {


                                ret = vendor_ais_get_layer_mem_v2(model_mem,&output_layer_iomem,&output_layer_info[i]);

                                if (ret == HD_OK) {
                            #if CNN_25_MATLAB
                                  VOID *output_layer_mem  = (VOID*)(output_layer_iomem);
                            #else
                                  //NN_DATA p_sao = (NN_DATA)output_layer_iomem;
                                  //VOID *output_layer_mem  = (VOID*)(p_sao[i].va);
                                  if (output_layer_iomem.va > 0 && output_layer_iomem.size > 0) {
                                      hd_common_mem_flush_cache((VOID *)output_layer_iomem.va, output_layer_iomem.size);
                                  }
                            #if RESULT_SAVE_DUMP
                                    CHAR img_save_path[256];
                                    snprintf(img_save_path, STR_MAX_LENGTH,"output_bin_%d/%s_%s/size_%dx%d_%d/%d/OUT%d.bin",(int)net_id,model_name,image_name_rest,(int)random_tmp_w,(int)random_tmp_h,(int)ii,(int)kk,(int)(output_layer_info[i].fusion_layer_index));
                                    vendor_ais_write_file(img_save_path, output_layer_iomem.va, output_layer_iomem.size);
                            #endif

                                if (kk==0) {
                                    memcpy((VOID*)result_mem.va,(VOID*)output_layer_iomem.va,output_layer_iomem.size);
                                    //memset((VOID*)result_mem.va,0,output_layer_iomem.size);
                                    result_mem.va = result_mem.va + output_layer_iomem.size;
                                } else {

                                    UINT8 *p1 = (UINT8*)(output_layer_iomem.va);
                                    UINT8 *p2 = (UINT8*)(compare_start_address);

                                    INT32 flag;
                                    flag = result_compare(p1,p2,output_layer_iomem.size);

                                    compare_start_address = compare_start_address + output_layer_iomem.size;
                                    flag_all_test[net_id] = flag_all_test[net_id]*flag;


                                }
                            #endif
                                } else {
                                    DBG_ERR("vendor_ais_get_layer_mem fail\n");
                                    flag_all_net[net_id] = flag_all_net[net_id]*0;
                                    write_err_txt(save_error_path,model_name,cnn_id,test_image_total[jj].img_name,random_tmp_w,random_tmp_h,kk,3);
                                    break;
                               }

                            }
                            if (kk != 0) {
                                if (flag_all_test[net_id] == 0) {
                                    if (debug_mode) {
                                        DBG_ERR("conforence not pass!!!\n");
                                    }
                                    write_err_txt(save_error_path,model_name,cnn_id,test_image_total[jj].img_name,random_tmp_w,random_tmp_h,kk+1,4);
                                } else {
                                    if (debug_mode) {
                                        DBG_DUMP("conforence pass!!!");
                                    }
                                }
                            }

                            /**********************finish save all output node layers***************************/
                            /*
                        #if RESULT_SAVE_DUMP
                                CHAR output_bin_path[256];
                            #if !CNN_25_MATLAB
                                    sprintf(output_bin_path, "mv output%d output_bin_%d/%s_%s/size_%ldx%ld_%d/%ld", net_id,net_id,model_name,image_name_rest,random_tmp_w,random_tmp_h,ii,kk);
                            #else
                                    sprintf(output_bin_path, "mv output%d output_bin/%d/%s", net_id, kk,model_name);

                            #endif
                                system(output_bin_path);
                                if(debug_mode){
                                    DBG_DUMP("[ai sample] write result done!\n");
                                }

                        #endif*/
                        }
                        /************************finish loop test conforence***************************/
                        input_init_exit:
                    #if AI_SAMPLE_TEST_BATCH
                        {
                          for (i = 0; i < input_proc_num; i++) {
                              ret = vendor_ais_net_input_layer_uninit(i, net_id);
                              if(ret != HD_OK) {
                                  DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                              }
                          }
                        }
                    #else
                            ret = vendor_ais_net_input_uninit(net_id);
                            if(ret != HD_OK) {
                              DBG_ERR("vendor_ais_net_input_uninit (%d)\n\r", ret);
                            }
                    #endif
                    #if AUTO_UPDATE_DIM
                            vendor_ais_unpars_diff_mem(&mem_manager, &diff_model_mem, &diff_info, net_id);
                    #endif

                    }
                    /************************finish loop multi resolution(resize)***************************/

                }
                /************************finish loop images***************************/

                gen_init_fail:
                    ret = vendor_ais_net_gen_uninit(net_id);
                    if(ret != HD_OK) {
                      DBG_ERR("vendor_ais_net_gen_uninit (%d)\n\r", ret);
                    }
                #if AI_V4
                    if (output_layer_info != NULL) {
                        free(output_layer_info);
                        output_layer_info = NULL;
                    }
                    if (input_info != NULL) {
                      free(input_info);
                      input_info = NULL;
                    }
                #endif

            }

    }

    //************************finish loop models***************************

    if (test_image_total) {
    	free(test_image_total);
    }
	if (fp) {
		fclose(fp);
		//fp = NULL;
	}

	return 0;
}

static INT32 remove_blank(CHAR *p_str)
{
	CHAR *p_str0 = p_str;
	CHAR *p_str1 = p_str;

	while (*p_str0 != '\0') {
		if ((*p_str0 != ' ') && (*p_str0 != '\t') && (*p_str0 != '\n'))
			*p_str1++ = *p_str0++;
		else
			p_str0++;
	}

	*p_str1 = '\0';
	return TRUE;
}
static INT32 split_kvalue(CHAR *p_str, CONFIG_OPT* p_config, CHAR c)
{
	CHAR *p_key = p_str;
	CHAR *p_value = NULL;
	CHAR *p_local = p_str;
	INT32 cnt = 0;

	// check only one '='
	while (*p_local != '\0') {
		p_local++;
		if (*p_local == c)
			cnt++;
	}
	if (cnt != 1)
		return FALSE;

	p_local = p_str;
	while (*p_local != c) {
		p_local++;
	}
	p_value = p_local + 1;
	*p_local = '\0';
	strncpy(p_config->key, p_key,strlen(p_key)+1);
	strncpy(p_config->value, p_value,strlen(p_value)+1);
	return TRUE;
}


static INT32 config_parser(CHAR *file_path, TEST_LIST_PATH *test_config)
{

    FILE *fp_tmp = NULL;

    //TEST_LIST_PATH test_config;
    //CHAR config_path[128];
    //sprintf(config_path, "%s", "/mnt/sd/AI_auto_test_files/paras/config_gt.txt");

    if ((fp_tmp = fopen(file_path, "r")) == NULL) {
        DBG_ERR("config_9.xt is not exist!\n");
        return 0;
    }
    CHAR line[512];
    while (1) {

        if (feof(fp_tmp))
            break;
        if (fgets(line, 512 - 1, fp_tmp) == NULL) {
            if (feof(fp_tmp))
                break;

            continue;
        }
        //printf("see:%s\n", line);
        remove_blank(line);
        //printf("see after:%s\n", line);
        if (line[0] == '#' || line[0] == '\0')
            continue;
        CONFIG_OPT tmp;
        if (split_kvalue(line, &tmp, '=') != 1) {
            continue;
        }

        if (strcmp(tmp.key, "[path/model_list]") == 0) {
            AI_STRCPY(test_config->model_list, tmp.value,sizeof(test_config->model_list));
        }
        if (strcmp(tmp.key, "[path/image_list]") == 0) {
            AI_STRCPY(test_config->image_list, tmp.value,sizeof(test_config->image_list));
        }
        if (strcmp(tmp.key, "[path/model_path]") == 0) {
            AI_STRCPY(test_config->model_path, tmp.value,sizeof(test_config->model_path));
        }
        if (strcmp(tmp.key, "[path/image_path]") == 0) {
            AI_STRCPY(test_config->image_path, tmp.value,sizeof(test_config->image_path));
        }
        if(strcmp(tmp.key, "[test_thread_num]") == 0) {
            test_config->thread_num = atoi(tmp.value);
        }
        if (strcmp(tmp.key, "[test/num]") == 0) {
            test_config->test_times = atoi(tmp.value);
        }
        if (strcmp(tmp.key, "[test_img_num]") == 0) {
            test_config->test_img_num = atoi(tmp.value);
        }
        if (strcmp(tmp.key, "[debug_mode]") == 0) {
            test_config->debug_mode = atoi(tmp.value);
        }
        if (strcmp(tmp.key, "[cnn_id]") == 0) {
            cnn_id = atoi(tmp.value);
        }
    }
    if (debug_mode) {
        DBG_DUMP("model_list_path:%s\n", test_config->model_list);
        DBG_DUMP("image_list_path:%s\n", test_config->image_list);
        DBG_DUMP("model_path:%s\n", test_config->model_path);
        DBG_DUMP("image_path:%s\n", test_config->image_path);
        DBG_DUMP("test thread num:%d\n", test_config->thread_num);
        DBG_DUMP("test num:%d\n", test_config->test_times);
        DBG_DUMP("The numbers of test_images:%d\n",test_config->test_img_num);
    }

    conforence_test_numbers = test_config->test_times;
    debug_mode = test_config->debug_mode;
    fclose(fp_tmp);
    return 1;
}


/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
    DBG_DUMP("ai_test_9 start...\r\n");
	CHAR* ai_sdk_version = {NULL};
	VENDOR_AIS_FLOW_MEM_PARM mem_parm[NN_RUN_NET_NUM] = {0};
	VENDOR_AIS_NETWORK_PARM net_parm[NN_RUN_NET_NUM] = {0};
    //pthread_mutex_init(&lock, NULL);
	pthread_t nn_thread_id[NN_RUN_NET_NUM];
	HD_RESULT           ret;
	//VENDOR_AIS_IMG_PARM	src_img = {0};
	int idx = 0;
	//NET_IMG_BIN bin_imginfo = {0};
	int model_size[NN_RUN_NET_NUM] = {0};
/*
#if defined(__LINUX)
	INT32 key;
#else
	CHAR key;
#endif

    int dbg_setting = 0;
    sscanf(argv[1], "%d", &dbg_setting);
    vendor_ais_set_dbg_mode(AI_DUMP_OUT_BIN, dbg_setting);
*/

    #if RESULT_SAVE_DUMP
	system("rm -rf output0");
	system("rm -rf output_bin");
    system("rm -rf output_bin_0");
    system("rm -rf output_bin_1");
    CHAR command_test[256];
    for (int i=0; i<NN_RUN_NET_NUM;i++) {
        sprintf(command_test,"mkdir output_bin_%d",i);
        system(command_test);
    }
    #endif

    CHAR save_error_path[256];
    CHAR result_txt_msg[256];
    sprintf(save_error_path,"%s","/mnt/sd/AI_auto_test_files/output/ai_test_9");
    if (access(save_error_path,0) == 0) {
     sprintf(result_txt_msg,"rm -rf %s",save_error_path);
     system(result_txt_msg);
    }
    if (access(save_error_path,0) == -1) {
     sprintf(result_txt_msg,"mkdir %s",save_error_path);
     system(result_txt_msg);
    }


    CHAR config_path[256];
    TEST_LIST_PATH test_config = {0};
    sprintf(config_path,"%s","/mnt/sd/AI_auto_test_files/paras/config_9.txt");
    if (!(config_parser(config_path,&test_config))) {
        DBG_ERR("config parser failed!!!\n");
        return 0;
    }

    INT32 total_mem_size = 0;
    for (int i=0; i< NN_RUN_NET_NUM; i++) {
        total_mem_size = total_mem_size + model_size[i];

    }
	if ((total_mem_size)*MBYTE > NN_TOTAL_MEM_SIZE) {
		DBG_ERR("total size should <= %d! use default setting\n", NN_TOTAL_MEM_SIZE/MBYTE);
	} else {
	    for (int i=0; i< NN_RUN_NET_NUM; i++) {
		    mem_size[i] = model_size[i]*MBYTE;
	    }
	}

	ai_sdk_version = vendor_ais_chk_sdk_version();
    if (debug_mode) {
        DBG_DUMP("ai_sdk_version: %s\n", ai_sdk_version);
    }



	ret = hd_common_init(0);
	if (ret != HD_OK) {
		DBG_ERR("hd_common_init fail=%d\n", ret);
		goto exit;
	}

	//set project config for AI
	hd_common_sysconfig(0, (1<<16), 0, VENDOR_AI_CFG); //enable AI engine
	// global init for ai sdk
	vendor_ai_global_init();

	// init memory
	//read model num from file
	FILE *fp = NULL;


    //CHAR model_info_file[256];
    UINT32 max_mem_size = 0;
    UINT32 tmp_mem_size = 0;
	CHAR model_name[50] = { 0 };
    CHAR model_path_file[256];

	//UINT32 model_num = 0;
  	//int cnn_id;

#if DYNAMIC_MEM
	for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {
        //sprintf(model_info_file, "model_info_%d.txt", idx);
        fp = fopen(test_config.model_list, "r");
        if (fp == NULL) {
		    DBG_ERR("open model_info fail.\r\n");
		    goto exit;
	    }
	    //fscanf(fp, "total_model_num = %ld\n\n", &model_num);
	    //printf("total_model_num= %ld\r\n", model_num);
	    //for (UINT32 j = 0;j<model_num; j++)
        while (1) {
            if (feof(fp)) {
                break;
            }
            if (fscanf(fp, "%s\n", model_name) == EOF) {
                if (feof(fp)) {
                    break;
                }
                continue;
            }

            sprintf(model_path_file,"%s/%s/sdk/nvt_model.bin",test_config.model_path,model_name);

            ret = vendor_ais_get_model_mem_sz(&tmp_mem_size, model_path_file);
            if (ret != HD_OK) {
			    DBG_WRN("something wrong in get model %s size info\n", model_name);
		    }
            if (tmp_mem_size > max_mem_size) {
                max_mem_size = tmp_mem_size;
                //printf("%d\r\n",max_mem_size);
            }
        }
        if (fp) {
		    fclose(fp);
		    fp = NULL;
	    }
		if (max_mem_size > 0) {
			mem_size[idx] = max_mem_size + (sizeof(VENDOR_AIS_RESULT_INFO) + MAX_OBJ_NUM * sizeof(VENDOR_AIS_RESULT));
			mem_size[idx] = ALIGN_CEIL_16(mem_size[idx]);
		}
	}
#endif
#if AUTO_UPDATE_DIM
        UINT32 diff_max_mem_size = 0;
        for (idx = 0; idx < NN_RUN_NET_NUM; idx++) {

            fp = fopen(test_config.model_list,"r");
            if (fp == NULL) {
                DBG_ERR("open model_info fail.\r\n");
                goto exit;
            }
            //fscanf(fp, "total_model_num = %ld\n\n", &model_num);
            while (1) {
                if (feof(fp)) {
                    break;
                }
                if (fscanf(fp, "%s\n", model_name) == EOF) {
                    if (feof(fp)) {
                        break;
                    }
                    continue;
                }

                sprintf(model_path_file,"%s/%s/sdk/nvt_stripe_model.bin",test_config.model_path,model_name);




                ret = vendor_ais_get_diff_model_mem_sz(&diff_model_size, model_path_file);
                if (ret != HD_OK) {
                    DBG_WRN("something wrong in get diff model %s size info\n", model_path_file);
                }
                if (diff_model_size > diff_max_mem_size) {
                    diff_max_mem_size = diff_model_size;

                }
           }
           if (fp) {
		    fclose(fp);
		    fp = NULL;
	       }
           if (diff_max_mem_size > 0) {
             mem_size[idx] = ALIGN_CEIL_16(mem_size[idx] + diff_max_mem_size);
           }
       }

#endif


	ret = mem_init();
	if (ret != HD_OK) {
		DBG_ERR("mem_init fail=%d\n", ret);
		goto exit;
	}

    ret = hd_gfx_init();
	if (ret != HD_OK) {
		DBG_ERR("hd_gfx_init fail=%d\n", ret);
		goto exit;
	}


	// allocate memory
	if (debug_mode) {
        DBG_DUMP("mem start ga:%d\n", g_mem.pa);
        DBG_DUMP("mem start va:%d\n", g_mem.va);
    }


	mem_parm[0].pa 		= g_mem.pa;
	mem_parm[0].va 		= g_mem.va;
	mem_parm[0].size 	= mem_size[0];
	for (idx = 1; idx < NN_RUN_NET_NUM; idx++) {
		mem_parm[idx].pa 		= mem_parm[idx - 1].pa + mem_parm[idx - 1].size;
		mem_parm[idx].va 		= mem_parm[idx - 1].va + mem_parm[idx - 1].size;
		mem_parm[idx].size 		= mem_size[idx];
        if (debug_mode) {
            DBG_DUMP("mem_size[idx] pa:%d\n", mem_parm[idx].pa);
            DBG_DUMP("mem_size[idx] va:%d\n", mem_parm[idx].va);
        }

	}

    yuv_scale_img[0].pa = mem_parm[idx-1].pa + mem_parm[idx-1].size;
    yuv_scale_img[0].va = mem_parm[idx-1].va + mem_parm[idx-1].size;
   	for (idx = 1; idx < NN_RUN_NET_NUM; idx++) {
		yuv_scale_img[idx].pa= yuv_scale_img[idx - 1].pa + YUV_OUT_BUF_SIZE/(NN_RUN_NET_NUM);
		yuv_scale_img[idx].va = yuv_scale_img[idx-1].va  + YUV_OUT_BUF_SIZE/(NN_RUN_NET_NUM);
        if (debug_mode) {
            DBG_DUMP("reult_address[idx] pa:%d\n", yuv_scale_img[idx].pa);
            DBG_DUMP("reult_address[idx] va:%d\n", yuv_scale_img[idx].va);
        }
	}

    result_compare_mem[0].pa = yuv_scale_img[idx-1].pa + YUV_OUT_BUF_SIZE/(NN_RUN_NET_NUM);
    result_compare_mem[0].va = yuv_scale_img[idx-1].va + YUV_OUT_BUF_SIZE/(NN_RUN_NET_NUM);
   	for (idx = 1; idx < NN_RUN_NET_NUM; idx++) {
		result_compare_mem[idx].pa= result_compare_mem[idx - 1].pa + 15 * MBYTE;
		result_compare_mem[idx].va = result_compare_mem[idx- 1].va  + 15 * MBYTE;
        if (debug_mode) {
            DBG_DUMP("yuv_scale_img[idx] pa:%d\n", result_compare_mem[idx].pa);
            DBG_DUMP("yuv_scale_img[idx] va:%d\n", result_compare_mem[idx].va);
        }

	}


    yuv_out_buffer_tmp[0].phy_addr[0] = yuv_out_buffer.phy_addr[0];
    yuv_out_buffer_va_tmp[0] = yuv_out_buffer_va;
	for (idx = 1; idx < NN_RUN_NET_NUM; idx++) {
		yuv_out_buffer_tmp[idx].phy_addr[0] = yuv_out_buffer_tmp[idx - 1].phy_addr[0] + YUV_OUT_BUF_SIZE/(NN_RUN_NET_NUM);
		yuv_out_buffer_va_tmp[idx] = yuv_out_buffer_va_tmp[idx-1]  + YUV_OUT_BUF_SIZE/(NN_RUN_NET_NUM);

	}


	// create encode_thread (pull_out bitstream)
	for (idx = 0; idx < test_config.thread_num; idx++) {
        flag_all_test[idx] = 1;
		net_parm[idx].mem = mem_parm[idx];
        net_parm[idx].result_mem = result_compare_mem[idx];
		net_parm[idx].run_id = idx;
        net_parm[idx].p_model_list = test_config.model_list;
        net_parm[idx].p_image_list = test_config.image_list;
        net_parm[idx].p_model_path = test_config.model_path;
        net_parm[idx].P_image_path= test_config.image_path;
        net_parm[idx].test_img_num = test_config.test_img_num;
		ret = pthread_create(&nn_thread_id[idx], NULL, nn_thread_api, (VOID*)(&net_parm[idx]));
		if (ret < 0) {
			DBG_ERR("create encode thread failed");
			goto exit;
		}

	}

	// wait encode thread destroyed

	UINT32 flag_final = 1;
	for (idx = 0; idx < test_config.thread_num; idx++) {
		pthread_join(nn_thread_id[idx], NULL);
        flag_final = flag_final*flag_all_test[idx]*flag_all_net[idx];
	}
    if (flag_final) {
        DBG_DUMP("success.\n");
    } else {
        DBG_ERR("failed.\n");
    }
    DBG_DUMP("ai_test_9 finished...\r\n");


exit:
    //pthread_mutex_destroy(&lock);
    ret = hd_gfx_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_gfx_uninit fail=%d\n", ret);
	}

	ret = mem_uninit();
	if (ret != HD_OK) {
		DBG_ERR("mem_uninit fail=%d\n", ret);
	}
	// global uninit for ai sdk
	vendor_ai_global_uninit();

	ret = hd_common_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_common_uninit fail=%d\n", ret);
	}


	return ret;
}

