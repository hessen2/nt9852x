/**
	@brief Sample code of ai network with sensor input.\n

	@file alg_net_app_user_sample_stream.c

	@author Joshua Liu

	@ingroup mhdal

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "hdal.h"
#include "hd_debug.h"


#include "vendor_ai/vendor_ai.h"
#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include "net_flow_user_sample/net_layer_sample.h"
#include "vendor_common.h"

// platform dependent
#if defined(__LINUX)
#include <pthread.h>			//for pthread API
#define MAIN(argc, argv) 		int main(int argc, char** argv)
#define GETCHAR()				getchar()
#else
#include <FreeRTOS_POSIX.h>
#include <FreeRTOS_POSIX/pthread.h> //for pthread API
#include <kwrap/util.h>		//for sleep API
#define sleep(x)    			vos_util_delay_ms(1000*(x))
#define msleep(x)    			vos_util_delay_ms(x)
#define usleep(x)   			vos_util_delay_us(x)
#include <kwrap/examsys.h> 	//for MAIN(), GETCHAR() API
#define MAIN(argc, argv) 		EXAMFUNC_ENTRY(hd_video_liveview, argc, argv)
#define GETCHAR()				NVT_EXAMSYS_GETCHAR()
#endif

#define DEBUG_MENU 			1

//#define CHKPNT			printf("\033[37mCHK: %s, %s: %d\033[0m\r\n",__FILE__,__func__,__LINE__)
//#define DBGH(x)			printf("\033[0;35m%s=0x%08X\033[0m\r\n", #x, x)
//#define DBGD(x)			printf("\033[0;35m%s=%d\033[0m\r\n", #x, x)

//#define DBG_DUMP(fmtstr, args...)   printf(fmtstr, ##args)


///////////////////////////////////////////////////////////////////////////////

//header
#define DBGINFO_BUFSIZE()	(0x200)

//RAW
#define VDO_RAW_BUFSIZE(w, h, pxlfmt)   (ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NRX: RAW compress: Only support 12bit mode
#define RAW_COMPRESS_RATIO 59
#define VDO_NRX_BUFSIZE(w, h)           (ALIGN_CEIL_4(ALIGN_CEIL_64(w) * 12 / 8 * RAW_COMPRESS_RATIO / 100 * (h)))
//CA for AWB
#define VDO_CA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 3) << 1)
//LA for AE
#define VDO_LA_BUF_SIZE(win_num_w, win_num_h) ALIGN_CEIL_4((win_num_w * win_num_h << 1) << 1)

//YUV
#define VDO_YUV_BUFSIZE(w, h, pxlfmt)	(ALIGN_CEIL_4((w) * HD_VIDEO_PXLFMT_BPP(pxlfmt) / 8) * (h))
//NVX: YUV compress
#define YUV_COMPRESS_RATIO 75
#define VDO_NVX_BUFSIZE(w, h, pxlfmt)	(VDO_YUV_BUFSIZE(w, h, pxlfmt) * YUV_COMPRESS_RATIO / 100)

///////////////////////////////////////////////////////////////////////////////

#define SEN_OUT_FMT			HD_VIDEO_PXLFMT_RAW12
#define CAP_OUT_FMT			HD_VIDEO_PXLFMT_RAW12
#define CA_WIN_NUM_W		32
#define CA_WIN_NUM_H		32
#define LA_WIN_NUM_W		32
#define LA_WIN_NUM_H		32
#define VA_WIN_NUM_W		16
#define VA_WIN_NUM_H		16
#define YOUT_WIN_NUM_W		128
#define YOUT_WIN_NUM_H		128
//#define ETH_8BIT_SEL		0 //0: 2bit out, 1:8 bit out
//#define ETH_OUT_SEL			1 //0: full, 1: subsample 1/2

#define VDO_SIZE_W			1920
#define VDO_SIZE_H			1080

// nn
#define DEFAULT_DEVICE		"/dev/" VENDOR_AIS_FLOW_DEV_NAME

#define YUV_WIDTH			224
#define YUV_HEIGHT			224
#define MAX_FRAME_WIDTH		1280
#define MAX_FRAME_HEIGHT	720

#define BS_BUF_SIZE			(MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT)
#define FRAME_BUF_SIZE		(2 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT)
#define YUV_OUT_BUF_SIZE	(3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT)

#define MBYTE				(1024 * 1024)
#define NN_TOTAL_MEM_SIZE	(100 * MBYTE)
#define VENDOR_AI_CFG  		0x000f0000  //ai project config

#define	VDO_FRAME_FORMAT	HD_VIDEO_PXLFMT_YUV420

#define NN_USE_DSP			FALSE
#define MAX_OBJ_NUM			1024


typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	VENDOR_AIS_IMG_PARM	src_img;
	VENDOR_AIS_FLOW_MEM_PARM mem;
	UINT32 run_id;
} VENDOR_AIS_NETWORK_PARM;

typedef struct _VIDEO_LIVEVIEW {
	// (1)
	HD_VIDEOCAP_SYSCAPS cap_syscaps;
	HD_PATH_ID cap_ctrl;
	HD_PATH_ID cap_path;

	HD_DIM  cap_dim;
	HD_DIM  proc_max_dim;

	// (2)
	HD_VIDEOPROC_SYSCAPS proc_syscaps;
	HD_PATH_ID proc_ctrl;
	HD_PATH_ID proc_path;

	HD_DIM  out_max_dim;
	HD_DIM  out_dim;

	// (3)
	HD_VIDEOOUT_SYSCAPS out_syscaps;
	HD_PATH_ID out_ctrl;
	HD_PATH_ID out_path;

	// (4) --
	HD_VIDEOPROC_SYSCAPS proc_alg_syscaps;
	HD_PATH_ID proc_alg_ctrl;
	HD_PATH_ID proc_alg_path;

	HD_DIM  proc_alg_max_dim;
	HD_DIM  proc_alg_dim;

	// (5) --
	HD_PATH_ID mask_alg_path;

    HD_VIDEOOUT_HDMI_ID hdmi_id;
} VIDEO_LIVEVIEW;


static CHAR model_name[1][256]	= { "para/nvt_model.bin" };
static UINT32 mem_size[1]		= { 30 * MBYTE };
static CHAR g_label_name[] 		= "accuracy/labels.txt";
static HD_VIDEODEC_BS bs_in_buffer;
static VOID *bs_in_buffer_va = NULL, *dec_out_buffer_va = NULL, *yuv_out_buffer_va = NULL;
static VENDOR_AIS_FLOW_MEM_PARM g_mem = {0};
static HD_COMMON_MEM_VB_BLK g_blk_info[4];
static HD_VIDEO_FRAME dec_out_buffer, yuv_out_buffer;
#if DYNAMIC_LABEL
#define NN_RUN_NET_NUM 1
static CHAR* g_class_labels[NN_RUN_NET_NUM] = {NULL};
#else
static CHAR g_class_labels[MAX_CLASS_NUM * VENDOR_AIS_LBL_LEN];
#endif
static BOOL is_net_proc						= TRUE;
static BOOL is_net_run						= FALSE;

VIDEO_LIVEVIEW g_stream[1] = {0}; //0: main stream


///////////////////////////////////////////////////////////////////////////////


static HD_RESULT mem_init(void)
{
	HD_RESULT              		ret;
	HD_COMMON_MEM_INIT_CONFIG	mem_cfg = {0};

	HD_COMMON_MEM_DDR_ID	  	ddr_id = DDR_ID0;
	HD_COMMON_MEM_VB_BLK      	blk;
	UINT32                    	pa, va;

	// config common pool (cap)
	mem_cfg.pool_info[0].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[0].blk_size = DBGINFO_BUFSIZE()+VDO_RAW_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, CAP_OUT_FMT)
        											 +VDO_CA_BUF_SIZE(CA_WIN_NUM_W, CA_WIN_NUM_H)
        											 +VDO_LA_BUF_SIZE(LA_WIN_NUM_W, LA_WIN_NUM_H);
	mem_cfg.pool_info[0].blk_cnt = 2;
	mem_cfg.pool_info[0].ddr_id = DDR_ID0;
	// config common pool (main)
	mem_cfg.pool_info[1].type = HD_COMMON_MEM_COMMON_POOL;
	mem_cfg.pool_info[1].blk_size = DBGINFO_BUFSIZE()+VDO_YUV_BUFSIZE(VDO_SIZE_W, VDO_SIZE_H, VDO_FRAME_FORMAT);
	mem_cfg.pool_info[1].blk_cnt = 3;
	mem_cfg.pool_info[1].ddr_id = DDR_ID0;

	// nn mem pool ---
	mem_cfg.pool_info[2].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[2].blk_size = NN_TOTAL_MEM_SIZE;
	mem_cfg.pool_info[2].blk_cnt = 1;
	mem_cfg.pool_info[2].ddr_id = DDR_ID0;
	mem_cfg.pool_info[3].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[3].blk_size = BS_BUF_SIZE;
	mem_cfg.pool_info[3].blk_cnt = 1;
	mem_cfg.pool_info[3].ddr_id = DDR_ID0;
	mem_cfg.pool_info[4].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[4].blk_size = FRAME_BUF_SIZE;
	mem_cfg.pool_info[4].blk_cnt = 1;
	mem_cfg.pool_info[4].ddr_id = DDR_ID0;
	mem_cfg.pool_info[5].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[5].blk_size = YUV_OUT_BUF_SIZE;
	mem_cfg.pool_info[5].blk_cnt = 1;
	mem_cfg.pool_info[5].ddr_id = DDR_ID0;
	// - end nn mem pool

	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		printf("hd_common_mem_init err: %d\r\n", ret);
		return ret;
	}

	// nn get block ---
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, NN_TOTAL_MEM_SIZE, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		DBG_DUMP("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		DBG_DUMP("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, NN_TOTAL_MEM_SIZE);
	g_blk_info[0] = blk;

	// Release buffer
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			DBG_DUMP("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = NN_TOTAL_MEM_SIZE;

	// Allocate in buffer
	bs_in_buffer.ddr_id = DDR_ID0;
	bs_in_buffer.size = BS_BUF_SIZE;

	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, bs_in_buffer.size, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		DBG_DUMP("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	bs_in_buffer.phy_addr = hd_common_mem_blk2pa(blk);
	if (bs_in_buffer.phy_addr == 0) {
		DBG_DUMP("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			DBG_DUMP("bs_in_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	bs_in_buffer_va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
										 bs_in_buffer.phy_addr,
										 bs_in_buffer.size);
	g_blk_info[1] = blk;
	DBG_DUMP("Allocate bs_in_buffer pa(%#lx) va(%p)\n",
			bs_in_buffer.phy_addr, bs_in_buffer_va);

	// Allocate out buffer
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, FRAME_BUF_SIZE, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		DBG_DUMP("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	dec_out_buffer.phy_addr[0] = hd_common_mem_blk2pa(blk);
	if (dec_out_buffer.phy_addr[0] == 0) {
		DBG_DUMP("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			DBG_DUMP("dec_out_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	dec_out_buffer_va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
										   dec_out_buffer.phy_addr[0],
										   FRAME_BUF_SIZE);
	g_blk_info[2] = blk;
	DBG_DUMP("Allocate dec_out_buffer pa(%#lx) va(%p)\n",
			dec_out_buffer.phy_addr[0], dec_out_buffer_va);

	// Allocate scale out buffer
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, YUV_OUT_BUF_SIZE, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		DBG_DUMP("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	yuv_out_buffer.phy_addr[0] = hd_common_mem_blk2pa(blk);
	if (yuv_out_buffer.phy_addr[0] == 0) {
		DBG_DUMP("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			DBG_DUMP("yuv_out_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	yuv_out_buffer_va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
						  yuv_out_buffer.phy_addr[0],
						  YUV_OUT_BUF_SIZE);
	g_blk_info[3] = blk;
	DBG_DUMP("Allocate yuv_out_buffer pa(%#lx) va(%p)\n",
			yuv_out_buffer.phy_addr[0], yuv_out_buffer_va);

exit:
	// - end nn get block

	return ret;
}

static HD_RESULT mem_exit(void)
{
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			DBG_DUMP("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		DBG_DUMP("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release in buffer */
	if (bs_in_buffer_va) {
		ret = hd_common_mem_munmap(bs_in_buffer_va, BS_BUF_SIZE);
		if (ret != HD_OK) {
			DBG_DUMP("mem_uninit : (bs_in_buffer_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)bs_in_buffer.phy_addr);
	ret = hd_common_mem_release_block(g_blk_info[1]);
	if (ret != HD_OK) {
		DBG_DUMP("mem_uninit : (bs_in_buffer.phy_addr)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release out buffer */
	if (dec_out_buffer_va) {
		ret = hd_common_mem_munmap(dec_out_buffer_va, FRAME_BUF_SIZE);
		if (ret != HD_OK) {
			DBG_DUMP("mem_uninit : (dec_out_buffer_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)dec_out_buffer.phy_addr[0]);
	ret = hd_common_mem_release_block(g_blk_info[2]);
	if (ret != HD_OK) {
		DBG_DUMP("mem_uninit : (dec_out_buffer.phy_addr[0])hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release scale out buffer */
	if (yuv_out_buffer_va) {
		ret = hd_common_mem_munmap(yuv_out_buffer_va, YUV_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			DBG_DUMP("mem_uninit : (yuv_out_buffer_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)yuv_out_buffer.phy_addr[0]);
	ret = hd_common_mem_release_block(g_blk_info[3]);
	if (ret != HD_OK) {
		DBG_DUMP("mem_uninit : (yuv_out_buffer.phy_addr[0])hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	hd_common_mem_uninit();
	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT get_cap_caps(HD_PATH_ID video_cap_ctrl, HD_VIDEOCAP_SYSCAPS *p_video_cap_syscaps)
{
	HD_RESULT ret = HD_OK;
	hd_videocap_get(video_cap_ctrl, HD_VIDEOCAP_PARAM_SYSCAPS, p_video_cap_syscaps);
	return ret;
}

static HD_RESULT get_cap_sysinfo(HD_PATH_ID video_cap_ctrl)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOCAP_SYSINFO sys_info = {0};

	hd_videocap_get(video_cap_ctrl, HD_VIDEOCAP_PARAM_SYSINFO, &sys_info);
	printf("sys_info.devid =0x%X, cur_fps[0]=%d/%d, vd_count=%llu, output_started=%d, cur_dim(%dx%d)\r\n",
		sys_info.dev_id, GET_HI_UINT16(sys_info.cur_fps[0]), GET_LO_UINT16(sys_info.cur_fps[0]), sys_info.vd_count, sys_info.output_started, sys_info.cur_dim.w, sys_info.cur_dim.h);
	return ret;
}

static HD_RESULT set_cap_cfg(HD_PATH_ID *p_video_cap_ctrl)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOCAP_DRV_CONFIG cap_cfg = {0};
	HD_PATH_ID video_cap_ctrl = 0;
	HD_VIDEOCAP_CTRL iq_ctl = {0};
#if 1
	snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, "nvt_sen_imx290");
	printf("sen_imx290 MIPI Interface\n");
	cap_cfg.sen_cfg.sen_dev.if_type = HD_COMMON_VIDEO_IN_MIPI_CSI;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  0x220; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.serial_if_pinmux = 0xF01;//PIN_MIPI_LVDS_CFG_CLK2 | PIN_MIPI_LVDS_CFG_DAT0|PIN_MIPI_LVDS_CFG_DAT1 | PIN_MIPI_LVDS_CFG_DAT2 | PIN_MIPI_LVDS_CFG_DAT3
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = 0x10;//PIN_I2C_CFG_CH2
	cap_cfg.sen_cfg.sen_dev.pin_cfg.clk_lane_sel = HD_VIDEOCAP_SEN_CLANE_SEL_CSI0_USE_C0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[0] = 0;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[1] = 1;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[2] = 2;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[3] = 3;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[4] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[5] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[6] = HD_VIDEOCAP_SEN_IGNORE;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.sen_2_serial_pin_map[7] = HD_VIDEOCAP_SEN_IGNORE;
#else
    snprintf(cap_cfg.sen_cfg.sen_dev.driver_name, HD_VIDEOCAP_SEN_NAME_LEN-1, "nvt_sen_ar0237ir");
    printf("sen_ar0237ir Parallel Interface\n");
	cap_cfg.sen_cfg.sen_dev.if_type = HD_COMMON_VIDEO_IN_P_RAW;
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.sensor_pinmux =  0x204; //PIN_SENSOR_CFG_MIPI | PIN_SENSOR_CFG_MCLK
	cap_cfg.sen_cfg.sen_dev.pin_cfg.pinmux.cmd_if_pinmux = 0x10;//PIN_I2C_CFG_CH2
#endif
	ret = hd_videocap_open(0, HD_VIDEOCAP_0_CTRL, &video_cap_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_DRV_CONFIG, &cap_cfg);
	iq_ctl.func = HD_VIDEOCAP_FUNC_AE | HD_VIDEOCAP_FUNC_AWB;
	ret |= hd_videocap_set(video_cap_ctrl, HD_VIDEOCAP_PARAM_CTRL, &iq_ctl);

	*p_video_cap_ctrl = video_cap_ctrl;
	return ret;
}

static HD_RESULT set_cap_param(HD_PATH_ID video_cap_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	{//select sensor mode, manually or automatically
		HD_VIDEOCAP_IN video_in_param = {0};

		video_in_param.sen_mode = HD_VIDEOCAP_SEN_MODE_AUTO; //auto select sensor mode by the parameter of HD_VIDEOCAP_PARAM_OUT
		video_in_param.frc = HD_VIDEO_FRC_RATIO(30,1);
		video_in_param.dim.w = p_dim->w;
		video_in_param.dim.h = p_dim->h;
		video_in_param.pxlfmt = SEN_OUT_FMT;
		video_in_param.out_frame_num = HD_VIDEOCAP_SEN_FRAME_NUM_1;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN, &video_in_param);
		//printf("set_cap_param MODE=%d\r\n", ret);
		if (ret != HD_OK) {
			return ret;
		}
	}
	#if 1 //no crop, full frame
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_OFF;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN_CROP, &video_crop_param);
		//printf("set_cap_param CROP NONE=%d\r\n", ret);
	}
	#else //HD_CROP_ON
	{
		HD_VIDEOCAP_CROP video_crop_param = {0};

		video_crop_param.mode = HD_CROP_ON;
		video_crop_param.win.rect.x = 0;
		video_crop_param.win.rect.y = 0;
		video_crop_param.win.rect.w = 1920/2;
		video_crop_param.win.rect.h= 1080/2;
		video_crop_param.align.w = 4;
		video_crop_param.align.h = 4;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_IN_CROP, &video_crop_param);
		//printf("set_cap_param CROP ON=%d\r\n", ret);
	}
	#endif
	{
		HD_VIDEOCAP_OUT video_out_param = {0};

		//without setting dim for no scaling, using original sensor out size
		video_out_param.pxlfmt = CAP_OUT_FMT;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		ret = hd_videocap_set(video_cap_path, HD_VIDEOCAP_PARAM_OUT, &video_out_param);
		//printf("set_cap_param OUT=%d\r\n", ret);
	}

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_proc_cfg(HD_PATH_ID *p_video_proc_ctrl, HD_DIM* p_max_dim)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOPROC_DEV_CONFIG video_cfg_param = {0};
	HD_VIDEOPROC_CTRL video_ctrl_param = {0};
	HD_PATH_ID video_proc_ctrl = 0;

	ret = hd_videoproc_open(0, HD_VIDEOPROC_0_CTRL, &video_proc_ctrl); //open this for device control
	if (ret != HD_OK)
		return ret;

	if (p_max_dim != NULL ) {
		video_cfg_param.pipe = HD_VIDEOPROC_PIPE_RAWALL;
		video_cfg_param.isp_id = 0;
		video_cfg_param.ctrl_max.func = 0;
		video_cfg_param.in_max.func = 0;
		video_cfg_param.in_max.dim.w = p_max_dim->w;
		video_cfg_param.in_max.dim.h = p_max_dim->h;
		video_cfg_param.in_max.pxlfmt = CAP_OUT_FMT;
		video_cfg_param.in_max.frc = HD_VIDEO_FRC_RATIO(1,1);
		ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_DEV_CONFIG, &video_cfg_param);
		if (ret != HD_OK) {
			return HD_ERR_NG;
		}
	}

	video_ctrl_param.func = 0;
	ret = hd_videoproc_set(video_proc_ctrl, HD_VIDEOPROC_PARAM_CTRL, &video_ctrl_param);

	*p_video_proc_ctrl = video_proc_ctrl;

	return ret;
}

static HD_RESULT set_proc_param(HD_PATH_ID video_proc_path, HD_DIM* p_dim)
{
	HD_RESULT ret = HD_OK;

	if (p_dim != NULL) { //if videoproc is already binding to dest module, not require to setting this!
		HD_VIDEOPROC_OUT video_out_param = {0};
		video_out_param.func = 0;
		video_out_param.dim.w = p_dim->w;
		video_out_param.dim.h = p_dim->h;
		video_out_param.pxlfmt = VDO_FRAME_FORMAT;
		video_out_param.dir = HD_VIDEO_DIR_NONE;
		video_out_param.frc = HD_VIDEO_FRC_RATIO(1,1);

		video_out_param.depth = 1;	// set > 0 to allow pull out (nn)

		ret = hd_videoproc_set(video_proc_path, HD_VIDEOPROC_PARAM_OUT, &video_out_param);
	}

	return ret;
}

///////////////////////////////////////////////////////////////////////////////

static HD_RESULT set_out_cfg(HD_PATH_ID *p_video_out_ctrl, UINT32 out_type, HD_VIDEOOUT_HDMI_ID hdmi_id)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOOUT_MODE videoout_mode = {0};
	HD_PATH_ID video_out_ctrl = 0;

	ret = hd_videoout_open(0, HD_VIDEOOUT_0_CTRL, &video_out_ctrl); //open this for device control
	if (ret != HD_OK) {
		return ret;
	}

	printf("out_type=%d\r\n", out_type);

	#if 1
	videoout_mode.output_type = HD_COMMON_VIDEO_OUT_LCD;
	videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
	videoout_mode.output_mode.lcd = HD_VIDEOOUT_LCD_0;
	if (out_type != 1) {
		printf("520 only support LCD\r\n");
	}
	#else
	switch(out_type){
	case 0:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_CVBS;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.cvbs= HD_VIDEOOUT_CVBS_NTSC;
	break;
	case 1:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_LCD;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.lcd = HD_VIDEOOUT_LCD_0;
	break;
	case 2:
		videoout_mode.output_type = HD_COMMON_VIDEO_OUT_HDMI;
		videoout_mode.input_dim = HD_VIDEOOUT_IN_AUTO;
		videoout_mode.output_mode.hdmi= hdmi_id;
	break;
	default:
		printf("not support out_type\r\n");
	break;
	}
	#endif
	ret = hd_videoout_set(video_out_ctrl, HD_VIDEOOUT_PARAM_MODE, &videoout_mode);

	*p_video_out_ctrl=video_out_ctrl ;
	return ret;
}

static HD_RESULT get_out_caps(HD_PATH_ID video_out_ctrl,HD_VIDEOOUT_SYSCAPS *p_video_out_syscaps)
{
	HD_RESULT ret = HD_OK;
    HD_DEVCOUNT video_out_dev = {0};

	ret = hd_videoout_get(video_out_ctrl, HD_VIDEOOUT_PARAM_DEVCOUNT, &video_out_dev);
	if (ret != HD_OK) {
		return ret;
	}
	printf("##devcount %d\r\n", video_out_dev.max_dev_count);

	ret = hd_videoout_get(video_out_ctrl, HD_VIDEOOUT_PARAM_SYSCAPS, p_video_out_syscaps);
	if (ret != HD_OK) {
		return ret;
	}
	return ret;
}

static HD_RESULT set_out_param(HD_PATH_ID video_out_path, HD_DIM *p_dim)
{
	HD_RESULT ret = HD_OK;
	HD_VIDEOOUT_IN video_out_param={0};

	video_out_param.dim.w = p_dim->w;
	video_out_param.dim.h = p_dim->h;
	video_out_param.pxlfmt = VDO_FRAME_FORMAT;
	video_out_param.dir = HD_VIDEO_DIR_NONE;
	ret = hd_videoout_set(video_out_path, HD_VIDEOOUT_PARAM_IN, &video_out_param);
	if (ret != HD_OK) {
		return ret;
	}
	memset((void *)&video_out_param,0,sizeof(HD_VIDEOOUT_IN));
	ret = hd_videoout_get(video_out_path, HD_VIDEOOUT_PARAM_IN, &video_out_param);
	if (ret != HD_OK) {
		return ret;
	}
	printf("##video_out_param w:%d,h:%d %x %x\r\n", video_out_param.dim.w, video_out_param.dim.h, video_out_param.pxlfmt, video_out_param.dir);

	return ret;
}

///////////////////////////////////////////////////////////////////////////////
static HD_RESULT init_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_init()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_init()) != HD_OK)
		return ret;
	if ((ret = hd_videoout_init()) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT open_module(VIDEO_LIVEVIEW *p_stream, HD_DIM* p_proc_max_dim, UINT32 out_type)
{
	HD_RESULT ret;
	// set videocap config
	ret = set_cap_cfg(&p_stream->cap_ctrl);
	if (ret != HD_OK) {
		printf("set cap-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set videoproc config
	ret = set_proc_cfg(&p_stream->proc_ctrl, p_proc_max_dim);
	if (ret != HD_OK) {
		printf("set proc-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set videoproc config for nn
	ret = set_proc_cfg(&p_stream->proc_alg_ctrl, p_proc_max_dim);
	if (ret != HD_OK) {
		printf("set proc-cfg alg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	// set videoout config
	ret = set_out_cfg(&p_stream->out_ctrl, out_type, p_stream->hdmi_id);
	if (ret != HD_OK) {
		printf("set out-cfg fail=%d\n", ret);
		return HD_ERR_NG;
	}
	if ((ret = hd_videocap_open(HD_VIDEOCAP_0_IN_0, HD_VIDEOCAP_0_OUT_0, &p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_0, &p_stream->proc_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_open(HD_VIDEOPROC_0_IN_0, HD_VIDEOPROC_0_OUT_1, &p_stream->proc_alg_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoout_open(HD_VIDEOOUT_0_IN_0, HD_VIDEOOUT_0_OUT_0, &p_stream->out_path)) != HD_OK)
		return ret;

	return HD_OK;
}

static HD_RESULT close_module(VIDEO_LIVEVIEW *p_stream)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_close(p_stream->cap_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_close(p_stream->proc_path)) != HD_OK)
		return ret;
	if ((ret = hd_videoout_close(p_stream->out_path)) != HD_OK)
		return ret;
	return HD_OK;
}

static HD_RESULT exit_module(void)
{
	HD_RESULT ret;
	if ((ret = hd_videocap_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoproc_uninit()) != HD_OK)
		return ret;
	if ((ret = hd_videoout_uninit()) != HD_OK)
		return ret;
	return HD_OK;
}

#if NN_USE_DSP
static HD_RESULT dsp_init(VOID)
{
	UINT32 dsp_running = 0;
	CUSTNN_IPC_INIT init = {0};
	NVTIPC_ER ipc_ret;
	HD_RESULT ret = HD_OK;

	// init ipc module
	ipc_ret = nvt_ipc_init();
	if (ipc_ret != NVTIPC_ER_OK) {
		DBG_DUMP("nvt_ipc_init fail=%d\n", ipc_ret);
		return HD_ERR_INIT;
	}

	// init dsp module
	ret = vendor_dsp_init();
	if (ret != HD_OK) {
		DBG_DUMP("vendor_dsp_init fail=%d\n", ret);
		return ret;
	}

	// init share memory
	init.size = custnn_get_buf_size();
	ret = vendor_dsp_init_share_mem(init.size, &init.pa, &init.va);
	if (ret != HD_OK) {
		DBG_DUMP("vendor_dsp_init_share_mem fail=%d\n", ret);
		return ret;
	}

	ret = vendor_dsp_is_running(DSP_CORE_ID_1, &dsp_running);
	if (ret != HD_OK) {
		DBG_DUMP("vendor_dsp_is_running fail=%d\r\n", ret);
		return ret;
	}

	if (!dsp_running) {
		DBG_DUMP("load dsp...\n");
		vendor_dsp_load(DSP_CORE_ID_1);
		sleep(2);
	}
	else {
		DBG_DUMP("dsp is running...\n");
	}

	if (custnn_init(&init, CUSTNN_SENDTO_DSP1) != CUSTNN_STA_OK) {
		DBG_DUMP("custnn_init fail\n");
		return HD_ERR_INIT;
	};

	return ret;
}

static HD_RESULT dsp_uninit(VOID)
{
	vendor_dsp_close(DSP_CORE_ID_1);
	vendor_dsp_uninit_share_mem();
	vendor_dsp_uninit();
	nvt_ipc_uninit();

	return HD_OK;
}
#endif // NN_USE_DSP

#if DYNAMIC_LABEL
HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, UINT32 label_num, const CHAR *filename)
#else
HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, const CHAR *filename)
#endif
{
	FILE *fd;
	CHAR *p_line = (CHAR *)addr;
#if DYNAMIC_LABEL
	UINT32 label_idx = 0;
#endif

	fd = fopen(filename, "r");
	if (!fd) {
		DBG_ERR("cannot read %s\r\n", filename);
		return HD_ERR_NG;
	}

	DBG_DUMP("open %s ok\r\n", filename);

	while (fgets(p_line, line_len, fd) != NULL) {
		p_line[strlen(p_line) - 1] = '\0'; // remove newline character
		p_line += line_len;
#if DYNAMIC_LABEL
		label_idx++;
		if (label_idx == label_num) {
			break;
		}
#endif
	}

	if (fd) {
		fclose(fd);
	}

	return HD_OK;
}

static UINT32 ai_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;
	//DBG_DUMP("model addr = %08x\r\n", (int)model_addr);

	fd = fopen(p_filename, "rb");
	if (!fd) {
		DBG_DUMP("cannot read %s\r\n", p_filename);
		return 0;
	}

	fseek ( fd, 0, SEEK_END );
	file_size = ALIGN_CEIL_4( ftell(fd) );
	fseek ( fd, 0, SEEK_SET );

	read_size = fread ((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
		DBG_DUMP("size mismatch, real = %d, idea = %d\r\n", (int)read_size, (int)file_size);
	}
	fclose(fd);
	return read_size;
}

#if 0
static BOOL net_sample_write_file(CHAR *filepath, UINT32 addr, UINT32 size)
{
	FILE *fsave = NULL;

	fsave = fopen(filepath, "wb");
	if (fsave == NULL) {
		DBG_ERR("fopen fail\n");
		return FALSE;
	}
	fwrite((UINT8 *)addr, size, 1, fsave);

	return TRUE;
}

static VOID *nn_thread_api(VOID *arg)
{
	INT32 i;
	UINT32 va;
	HD_VIDEO_FRAME video_frame = {0};

	hd_videoproc_pull_out_buf(g_stream[0].proc_alg_path, &video_frame, -1); // -1 = blocking mode, 0 = non-blocking mode, >0 = blocking-timeout mode

	printf("\n\rVideo frame info\n\r");
	printf("  ddr_id %d\n\r", video_frame.ddr_id);
	printf("  pxlfmt %d\n\r", video_frame.pxlfmt);
	printf("  plane-0 width %d, height %d\n\r", video_frame.dim.w, video_frame.dim.h);
	for(i=0; i<HD_VIDEO_MAX_PLANE; i++) {
		printf("  plane %d width %d, height %d\n\r", i, video_frame.pw[i], video_frame.ph[i]);
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, video_frame.phy_addr[0], video_frame.pw[0]*video_frame.ph[0]*3/2);
	net_sample_write_file("net_sample_write_yuv.raw", va, video_frame.pw[0]*video_frame.ph[0] * 3 / 2);

	hd_videoproc_release_out_buf(g_stream[0].proc_alg_path, &video_frame);

	return 0;
}
#else
static VOID *nn_thread_api(VOID *arg)
{
	//INT32 key;

	HD_RESULT ret;
	UINT32 model_size, req_size=0;
	VENDOR_AIS_NETWORK_PARM *p_net_parm = (VENDOR_AIS_NETWORK_PARM*)arg;
	VENDOR_AIS_IMG_PARM src_img;
	VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem = &p_net_parm->mem;
	VENDOR_AIS_FLOW_MAP_MEM_PARM mem_manager;
	VENDOR_AIS_FLOW_MEM_PARM rslt_mem;
	VENDOR_AIS_FLOW_MEM_PARM model_mem;
	VENDOR_AIS_RESULT_INFO *p_net_rslt;
	VENDOR_AIS_RESULT *p_rslt;
	UINT32 i;
	HD_VIDEO_FRAME video_frame = {0};
#if DYNAMIC_LABEL
	UINT32 label_num = 0;
#endif

	if (p_net_parm->p_model_path == NULL) {
		printf("input network model is null\r\n");
		return 0;
	}

	//allocate memory (parm, model, io buffer)
	model_size      = vendor_ais_auto_alloc_mem(p_tot_mem, &mem_manager);
	model_mem.pa    = p_tot_mem->pa;
	model_mem.va    = p_tot_mem->va;    // = load_addr
	model_mem.size  = model_size;
	rslt_mem.pa     = model_mem.pa + model_mem.size;
	rslt_mem.va     = model_mem.va + model_mem.size;
	rslt_mem.size   = sizeof(VENDOR_AIS_RESULT_INFO) + MAX_OBJ_NUM * sizeof(VENDOR_AIS_RESULT);
	req_size        = model_mem.size + rslt_mem.size;
	if (req_size > p_tot_mem->size) {
		printf("memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)req_size);
		return 0;
	}
	if (model_size > p_tot_mem->size) {
		printf("memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)model_size);
		return 0;
	}
	memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer

	// map user/kernel memory
	ret = vendor_ais_net_gen_init(mem_manager, p_net_parm->run_id);
	if (ret != HD_OK) {
		printf("net gen init fail=%d\r\n", ret);
		goto gen_init_fail;
	} else {
		printf("[ai sample] init done: %s!\n", p_net_parm->p_model_path);
	}
#if DYNAMIC_LABEL
	vendor_ais_get_label_num(&label_num, model_mem, p_net_parm->run_id);
	if (label_num > 0) {
		g_class_labels[p_net_parm->run_id] = (CHAR*)malloc(label_num*VENDOR_AIS_LBL_LEN);//alloc label buffer
		ret = ai_read_label((UINT32)g_class_labels[p_net_parm->run_id], VENDOR_AIS_LBL_LEN, label_num, g_label_name);
		if (ret != HD_OK) {
			DBG_ERR("ai_read_label fail=%d\n", ret);
			goto gen_init_fail;
		}
	}
#endif

	do {
        if (is_net_run) {
		ret = hd_videoproc_pull_out_buf(g_stream[0].proc_alg_path, &video_frame, -1); // -1 = blocking mode, 0 = non-blocking mode, >0 = blocking-timeout mode
		if(ret != HD_OK) {
			printf("hd_videoproc_pull_out_buf fail (%d)\n\r", ret);
			goto gen_init_fail;
		}

		src_img.width   	= video_frame.dim.w;
		src_img.height  	= video_frame.dim.h;
		src_img.channel 	= 2;
		src_img.line_ofs	= video_frame.loff[0];
		src_img.fmt 		= VDO_FRAME_FORMAT;
		src_img.pa			= video_frame.phy_addr[0];
		src_img.va			= (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, video_frame.phy_addr[0], video_frame.pw[0]*video_frame.ph[0]*3/2);
		src_img.fmt_type    = VENDOR_COMMON_PXLFMT_YUV420_TYPE;
		
		if (vendor_ais_net_input_init(&src_img, p_net_parm->run_id) != HD_OK) {
			goto gen_init_fail;
		}

		memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
		hd_common_mem_flush_cache((VOID *)mem_manager.user_buff.va, mem_manager.user_buff.size);
#if USE_NEON
		vendor_ais_proc_net(model_mem, rslt_mem, &src_img, p_net_parm->run_id);
#else
		vendor_ais_proc_net(model_mem, rslt_mem, p_net_parm->run_id);
#endif

		p_net_rslt = vendor_ais_get_results(p_net_parm->run_id);
		if (p_net_rslt != NULL) {
			if (p_net_rslt->result_num == 1) {
				printf("\r\nClassification Results (Top 5):\r\n");
				p_rslt = &p_net_rslt->p_result[0];
				for (i = 0; i < TOP_N; i++) {
				#if DYNAMIC_LABEL
					printf("%ld. no=%ld, label=%s, score=%f\r\n", i + 1, p_rslt->no[i]
								 , &g_class_labels[p_net_parm->run_id][p_rslt->no[i] * VENDOR_AIS_LBL_LEN], p_rslt->score[i]);
				#else
					printf("%ld. no=%ld, label=%s, score=%f\r\n", i + 1, p_rslt->no[i],
							 &g_class_labels[p_rslt->no[i] * VENDOR_AIS_LBL_LEN], p_rslt->score[i]);
				#endif
				}
			} else {
				printf("\r\nClassification Results (Top 1):\r\n");
				for (i = 0; i < p_net_rslt->result_num; i++) {
					p_rslt = &p_net_rslt->p_result[i];
				#if DYNAMIC_LABEL
					printf("%ld. no=%ld, label=%s, score=%f\r\n", i + 1, p_rslt->no[0]
							, &g_class_labels[p_net_parm->run_id][p_rslt->no[0] * VENDOR_AIS_LBL_LEN], p_rslt->score[0]);
				#else
					printf("%ld. no=%ld, label=%s, score=%f\r\n", i + 1, p_rslt->no[0],
							 &g_class_labels[p_rslt->no[0] * VENDOR_AIS_LBL_LEN], p_rslt->score[0]);
				#endif
				}
			}
		}

		ret = hd_common_mem_munmap((void *)src_img.va, video_frame.pw[0]*video_frame.ph[0]*3/2);
		if (ret != HD_OK) {
			printf("nn_thread_api : (src_img.va)hd_common_mem_munmap fail\r\n");
			goto gen_init_fail;
		}

		vendor_ais_net_input_uninit(p_net_parm->run_id);

		ret = hd_videoproc_release_out_buf(g_stream[0].proc_alg_path, &video_frame);
        if(ret != HD_OK) {
			printf("hd_videoproc_release_out_buf fail (%d)\n\r", ret);
			goto gen_init_fail;
		}
		printf("[ai sample] write result done!\n");

		//key = getchar();
		//if(key=='q' || key==0x3) break;
        }
	} while(is_net_proc);

gen_init_fail:
	ret = vendor_ais_net_gen_uninit(0);
	if(ret != HD_OK) {
		printf("vendor_ais_net_gen_uninit fail (%d)\n\r", ret);
	}
#if DYNAMIC_LABEL
	if (g_class_labels[p_net_parm->run_id] != NULL) {
		free(g_class_labels[p_net_parm->run_id]);
		g_class_labels[p_net_parm->run_id] = NULL;
	}
#endif

	return 0;
}
#endif

MAIN(argc, argv)
{
	HD_RESULT ret;
	INT key;
	UINT32 out_type = 1;

	VENDOR_AIS_FLOW_MEM_PARM mem_parm[1] = {0};
	VENDOR_AIS_NETWORK_PARM net_parm[1] = {0};
	pthread_t nn_thread_id[1];
	UINT32 file_size = 0;

	// query program options
	if (argc >= 2) {
		out_type = atoi(argv[1]);
		printf("out_type %d\r\n", out_type);
		if(out_type > 2) {
			printf("error: not support out_type!\r\n");
			return 0;
		}
	}
    g_stream[0].hdmi_id=HD_VIDEOOUT_HDMI_1920X1080I60;//default

	// query program options
	if (argc >= 3 && (atoi(argv[2]) !=0)) {
		g_stream[0].hdmi_id = atoi(argv[2]);
		printf("hdmi_mode %d\r\n", g_stream[0].hdmi_id);
	}

	// init hdal
	ret = hd_common_init(0);
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
		goto exit;
	}

	//set project config for AI
	hd_common_sysconfig(0, (1<<16), 0, VENDOR_AI_CFG); //enable AI engine

	// init memory
	ret = mem_init();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
		goto exit;
	}

	// init all modules
	ret = init_module();
	if (ret != HD_OK) {
		printf("init fail=%d\n", ret);
		goto exit;
	}

	// open video_liveview modules (main)
	g_stream[0].proc_max_dim.w = VDO_SIZE_W; //assign by user
	g_stream[0].proc_max_dim.h = VDO_SIZE_H; //assign by user
	ret = open_module(&g_stream[0], &g_stream[0].proc_max_dim, out_type);
	if (ret != HD_OK) {
		printf("open fail=%d\n", ret);
		goto exit;
	}

	// get videocap capability
	ret = get_cap_caps(g_stream[0].cap_ctrl, &g_stream[0].cap_syscaps);
	if (ret != HD_OK) {
		printf("get cap-caps fail=%d\n", ret);
		goto exit;
	}

	// get videoout capability
	ret = get_out_caps(g_stream[0].out_ctrl, &g_stream[0].out_syscaps);
	if (ret != HD_OK) {
		printf("get out-caps fail=%d\n", ret);
		goto exit;
	}
	g_stream[0].out_max_dim = g_stream[0].out_syscaps.output_dim;

	// set videocap parameter
	g_stream[0].cap_dim.w = VDO_SIZE_W; //assign by user
	g_stream[0].cap_dim.h = VDO_SIZE_H; //assign by user
	ret = set_cap_param(g_stream[0].cap_path, &g_stream[0].cap_dim);
	if (ret != HD_OK) {
		printf("set cap fail=%d\n", ret);
		goto exit;
	}

	// set videoproc parameter (main)
	ret = set_proc_param(g_stream[0].proc_path, NULL);
	if (ret != HD_OK) {
		printf("set proc fail=%d\n", ret);
		goto exit;
	}

	// set videoproc parameter (alg)
	g_stream[0].proc_alg_max_dim.w = VDO_SIZE_W;
	g_stream[0].proc_alg_max_dim.h = VDO_SIZE_H;
	ret = set_proc_param(g_stream[0].proc_alg_path, &g_stream[0].proc_alg_max_dim);
	if (ret != HD_OK) {
		printf("set proc alg fail=%d\n", ret);
		goto exit;
	}

	// set videoout parameter (main)
	g_stream[0].out_dim.w = g_stream[0].out_max_dim.w; //using device max dim.w
	g_stream[0].out_dim.h = g_stream[0].out_max_dim.h; //using device max dim.h
	ret = set_out_param(g_stream[0].out_path, &g_stream[0].out_dim);
	if (ret != HD_OK) {
		printf("set out fail=%d\n", ret);
		goto exit;
	}

	// bind video_liveview modules (main)
	hd_videocap_bind(HD_VIDEOCAP_0_OUT_0, HD_VIDEOPROC_0_IN_0);
	hd_videoproc_bind(HD_VIDEOPROC_0_OUT_0, HD_VIDEOOUT_0_IN_0);


	// nn S ------------------------------------------------
	#if NN_USE_DSP
	ret = dsp_init();
	if (ret != HD_OK) {
		DBG_ERR("dsp_init fail=%d\n", ret);
		goto exit;
	}
	#endif

#if (DYNAMIC_LABEL == 0)
	ret = ai_read_label((UINT32)g_class_labels, VENDOR_AIS_LBL_LEN, g_label_name);
	if (ret != HD_OK) {
		DBG_ERR("ai_read_label fail=%d\n", ret);
		goto exit;
	}
#endif
	// allocate memory
	mem_parm[0].pa		= g_mem.pa;
	mem_parm[0].va		= g_mem.va;
	mem_parm[0].size	= mem_size[0];

	// create encode_thread (pull_out bitstream)
	//net_parm[0].src_img = src_img;
	net_parm[0].mem = mem_parm[0];
	net_parm[0].run_id = 0;
	net_parm[0].p_model_path = model_name[0];

	// load model
	file_size = ai_load_file(net_parm[0].p_model_path, net_parm[0].mem.va);
	if (file_size == 0) {
		DBG_DUMP("net load file fail: %s\r\n", net_parm[0].p_model_path);
		return 0;
	}

	ret = pthread_create(&nn_thread_id[0], NULL, nn_thread_api, (VOID*)(&net_parm[0]));
	if (ret < 0) {
		DBG_ERR("create encode thread failed");
		goto exit;
	}
	// nn E ------------------------------------------------


	// start video_liveview modules (main)
	hd_videocap_start(g_stream[0].cap_path);
	hd_videoproc_start(g_stream[0].proc_path);
	hd_videoproc_start(g_stream[0].proc_alg_path);
	// just wait ae/awb stable for auto-test, if don't care, user can remove it
	sleep(1);
	hd_videoout_start(g_stream[0].out_path);

	// query user key
	do {
		printf("usage:\n");
		printf("  enter q: exit\n");
		printf("  enter r: run engine\n");
		key = getchar();
		if (key == 'q' || key == 0x3) {
			is_net_proc = FALSE;
            is_net_run = FALSE;
			break;
		} else if (key == 'r') {
			//  run network
			is_net_proc = TRUE;
            is_net_run = TRUE;
			printf("[ai sample] write result done!\n");
			continue;
		}
        if (key == '0') {
			get_cap_sysinfo(g_stream[0].cap_ctrl);
		}
	} while(1);

	pthread_join(nn_thread_id[0], NULL);

	// stop video_liveview modules (main)
	hd_videocap_stop(g_stream[0].cap_path);
	hd_videoproc_stop(g_stream[0].proc_path);
	hd_videoproc_stop(g_stream[0].proc_alg_path);
	hd_videoout_stop(g_stream[0].out_path);

	// unbind video_liveview modules (main)
	hd_videocap_unbind(HD_VIDEOCAP_0_OUT_0);
	hd_videoproc_unbind(HD_VIDEOPROC_0_OUT_0);

exit:
	#if NN_USE_DSP
	ret = dsp_uninit();
	if (ret != HD_OK) {
		DBG_ERR("dsp_uninit fail=%d\n", ret);
	}
	#endif

	// close video_liveview modules (main)
	ret = close_module(&g_stream[0]);
	if (ret != HD_OK) {
		printf("close fail=%d\n", ret);
	}

	// uninit all modules
	ret = exit_module();
	if (ret != HD_OK) {
		printf("exit fail=%d\n", ret);
	}

	// uninit memory
	ret = mem_exit();
	if (ret != HD_OK) {
		printf("mem fail=%d\n", ret);
	}

	// uninit hdal
	ret = hd_common_uninit();
	if (ret != HD_OK) {
		printf("common fail=%d\n", ret);
	}

	return 0;
}
