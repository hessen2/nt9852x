/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include "hdal.h"
#include "hd_type.h"
#include "hd_common.h"
//#include "nvtipc.h"
//#include "vendor_dsp_util.h"
#include "vendor_ai/vendor_ai.h"

#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include "net_flow_user_sample/net_layer_sample.h"
#include <sys/time.h>

/*-----------------------------------------------------------------------------*/
/* Probe Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <sys/stat.h>
/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define AI_DEVICE          		"/proc/kdrv_ai"
#define MAX_FRAME_WIDTH         1920
#define MAX_FRAME_HEIGHT        1080
#define DEVICE_32X			    (0x01)
#define DEVICE_52X				(0x02)
#define DEVICE_SETTING			DEVICE_52X
#define AI_SAMPLE_TEST_BATCH    1 // v2 not support



#if (DEVICE_SETTING == DEVICE_32X)
#define DDR_TYPE (DDR_ID0)
#define MEM_TYPE  (HD_COMMON_MEM_USER_BLK)
#define MEM_GET_BLOCK (0)
#elif (DEVICE_SETTING == DEVICE_52X)
#define DDR_TYPE (DDR_ID1)
#define MEM_TYPE  (HD_COMMON_MEM_CNN_POOL)
#define MEM_GET_BLOCK (1)
#endif
#define MBYTE					(1024 * 1024)
UINT32 YUV_OUT_BUF_SIZE 		= 0;
UINT32 NN_TOTAL_MEM_SIZE       	= 0;
#define VENDOR_AI_CFG  0x000f0000  //ai project config

/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
#define _MAX(a,b) (((a)>(b))?(a):(b))
static VENDOR_AIS_FLOW_MEM_PARM g_mem	= {0};
static HD_VIDEO_FRAME yuv_out_buffer;
static VOID *yuv_out_buffer_va = NULL;
#if (MEM_GET_BLOCK)
static HD_COMMON_MEM_VB_BLK g_blk_info[2];
#endif

/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/
static INT check_driver(void)
{
	struct stat st = { 0 };
	CHAR device[20] = AI_DEVICE;

	if (stat(device, &st) == 0 && S_ISDIR(st.st_mode)) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

#if MEM_GET_BLOCK
static int mem_init(CHAR *model_path, int img_buf_size)
{
	HD_RESULT                 ret;
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};
	UINT32                    pa, va;
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_TYPE;
	HD_COMMON_MEM_VB_BLK      blk;

	printf("YUV size src = %d\r\n", img_buf_size);
	
	/* Allocate parameter buffer */
	if (g_mem.va != 0) {
		printf("err: mem has already been inited\r\n");
		return -1;
	}

	UINT32 mem_size = 0;
	ret = vendor_ais_get_model_mem_sz(&mem_size, model_path);
	if (ret != HD_OK) {
	        printf("vendor_ais_get_model_mem_sz fail: %s\r\n", model_path);
	        return ret;
	}
	printf("vendor_ais_get_model_mem_sz: %d\r\n", (int)mem_size);
	
    NN_TOTAL_MEM_SIZE = ALIGN_CEIL_64(mem_size);
	YUV_OUT_BUF_SIZE  = ALIGN_CEIL_64(img_buf_size);
	//CNET need more image buffer
	printf("NN size = %d\r\n", (int)NN_TOTAL_MEM_SIZE);
	printf("YUV size = %d\r\n", (int)YUV_OUT_BUF_SIZE);
    
	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = NN_TOTAL_MEM_SIZE;
	mem_cfg.pool_info[0].blk_cnt = 1;
	mem_cfg.pool_info[0].ddr_id = DDR_TYPE;
	mem_cfg.pool_info[1].type = MEM_TYPE;
	mem_cfg.pool_info[1].blk_size = YUV_OUT_BUF_SIZE;
	mem_cfg.pool_info[1].blk_cnt = 1;
	mem_cfg.pool_info[1].ddr_id = DDR_TYPE;

#if (DEVICE_SETTING == DEVICE_52X)
	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		printf("hd_common_mem_init err: %d\r\n", ret);
		return ret;
	}
#endif

	blk = hd_common_mem_get_block(MEM_TYPE, NN_TOTAL_MEM_SIZE, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		printf("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, NN_TOTAL_MEM_SIZE);
	g_blk_info[0] = blk;

	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = NN_TOTAL_MEM_SIZE;

	/* Allocate scale out buffer */
	blk = hd_common_mem_get_block(MEM_TYPE, YUV_OUT_BUF_SIZE, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	yuv_out_buffer.phy_addr[0] = hd_common_mem_blk2pa(blk);
	if (yuv_out_buffer.phy_addr[0] == 0) {
		printf("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			printf("yuv_out_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	yuv_out_buffer_va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
						  yuv_out_buffer.phy_addr[0],
						  YUV_OUT_BUF_SIZE);
	g_blk_info[1] = blk;
	printf("Allocate yuv_out_buffer pa(%#lx) va(%p)\n",
			yuv_out_buffer.phy_addr[0], yuv_out_buffer_va);
			
exit:
	return ret;
}

static HD_RESULT mem_uninit(void)
{
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
		g_mem.va = 0;
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)g_mem.pa);
	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		printf("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release scale out buffer */
	if (yuv_out_buffer_va) {
		ret = hd_common_mem_munmap(yuv_out_buffer_va, YUV_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (yuv_out_buffer_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
		yuv_out_buffer_va = 0;
	}
	//ret = hd_common_mem_release_block((HD_COMMON_MEM_VB_BLK)yuv_out_buffer.phy_addr[0]);
	ret = hd_common_mem_release_block(g_blk_info[1]);
	if (ret != HD_OK) {
		printf("mem_uninit : (yuv_out_buffer.phy_addr[0])hd_common_mem_release_block fail.\r\n");
		return ret;
	}
#if (DEVICE_SETTING == DEVICE_52X)
	ret = hd_common_mem_uninit();
#endif
	return ret;
}

#else
static int mem_init(CHAR *model_path, int img_buf_size)
{
	HD_RESULT                 ret;

	UINT32                    pa, va;
	HD_COMMON_MEM_DDR_ID      ddr_id = DDR_TYPE;
    HD_COMMON_MEM_POOL_TYPE   pool_type = HD_COMMON_MEM_CNN_POOL;
	HD_COMMON_MEM_POOL_INFO   mem_info = { 0 };
	/* Allocate parameter buffer */
	if (g_mem.va != 0) {
		printf("err: mem has already been inited\r\n");
		return -1;
	}
	UINT32 mem_size = 0;
	ret = vendor_ais_get_model_mem_sz(&mem_size, model_path);
	if (ret != HD_OK) {
	        printf("vendor_ais_get_model_mem_sz fail: %s\r\n", model_path);
	        return ret;
	}
	printf("vendor_ais_get_model_mem_sz: %d\r\n", mem_size);
	
    NN_TOTAL_MEM_SIZE = ALIGN_CEIL_64(mem_size);
	YUV_OUT_BUF_SIZE  = ALIGN_CEIL_64(img_buf_size);
	ret = HD_OK;
	mem_info.type = pool_type;
	mem_info.ddr_id = ddr_id;
	if (hd_common_mem_get(HD_COMMON_MEM_PARAM_POOL_CONFIG, (VOID *)&mem_info) != HD_OK) {
		DBG_DUMP("hd_common_mem_get (CNN) fail\r\n");
		return HD_ERR_NOBUF;
	}
    pa = mem_info.start_addr;
    if (pa == 0) {
		DBG_DUMP("not get buffer, pa=0x%08x\r\n", (int)pa);
		return HD_ERR_NOBUF;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, NN_TOTAL_MEM_SIZE+YUV_OUT_BUF_SIZE);
	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, NN_TOTAL_MEM_SIZE+YUV_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			printf("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}
	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = NN_TOTAL_MEM_SIZE;
    yuv_out_buffer.phy_addr[0] = g_mem.pa + NN_TOTAL_MEM_SIZE;
    yuv_out_buffer_va = (void *)(g_mem.va + NN_TOTAL_MEM_SIZE);
	/* Allocate scale out buffer */

	printf("Allocate yuv_out_buffer pa(%#lx) va(%p)\n",
			yuv_out_buffer.phy_addr[0], yuv_out_buffer_va);

	return ret;
}

static HD_RESULT mem_uninit(void)
{
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (g_mem.va!=0 && yuv_out_buffer_va!=0 ) {
		ret = hd_common_mem_munmap((void *)g_mem.va, NN_TOTAL_MEM_SIZE+YUV_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : hd_common_mem_munmap fail.\r\n");
			return ret;
		}
		g_mem.va = 0;
		yuv_out_buffer_va = 0;
	}
#if (DEVICE_SETTING == DEVICE_52X)
	ret = hd_common_mem_uninit();
#endif
	return ret;
}

#endif


static HD_RESULT ai_load_img(CHAR *filename, VENDOR_AIS_IMG_PARM *p_img)
{
	INT32 file_len;
	UINT32 key = 0;

	file_len = vendor_ais_loadbin((UINT32)yuv_out_buffer_va, filename);
	if (file_len < 0) {
		return HD_ERR_IO;
	}

#if AI_SUPPORT_MULTI_FMT
		p_img->fmt_type = 0;
#endif

	if (p_img->width == 0) {
		while (TRUE) {
			printf("Enter width = ?\r\n");
			if (scanf("%hu", &p_img->width) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->height == 0) {
		while (TRUE) {
			printf("Enter height = ?\r\n");
			if (scanf("%hu", &p_img->height) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->channel == 0) {
		while (TRUE) {
			printf("Enter channel = ?\r\n");
			if (scanf("%hu", &p_img->channel) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->line_ofs == 0) {
		while (TRUE) {
			printf("Enter line offset = ?\r\n");
			if (scanf("%lu", &p_img->line_ofs) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->fmt == 0) {
		while (TRUE) {
			printf("Enter image format = ?\r\n");
			printf("[%d] RGB888 (0x%08x)\r\n", NET_IMG_RGB_PLANE, HD_VIDEO_PXLFMT_RGB888_PLANAR);
			printf("[%d] YUV420 (0x%08x)\r\n", NET_IMG_YUV420, HD_VIDEO_PXLFMT_YUV420);
			printf("[%d] YUV422 (0x%08x)\r\n", NET_IMG_YUV422, HD_VIDEO_PXLFMT_YUV422);
			printf("[%d] Y only (0x%08x)\r\n", NET_IMG_YONLY, HD_VIDEO_PXLFMT_Y8);
#if AI_SUPPORT_MULTI_FMT
			printf("[%d] YUV420_NV21 (0x%08x)\r\n", NET_IMG_YUV420_NV21, HD_VIDEO_PXLFMT_YUV420+1);
#endif

			if (scanf("%lu", &key) != 1) {
				printf("Wrong input\n");
				continue;
			} else if (key == NET_IMG_YUV420) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
			} else if (key == NET_IMG_YUV422) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV422;
			} else if (key == NET_IMG_RGB_PLANE) {
				p_img->fmt = HD_VIDEO_PXLFMT_RGB888_PLANAR;
			} else if (key == NET_IMG_YONLY) {
				p_img->fmt = HD_VIDEO_PXLFMT_Y8;
#if AI_SUPPORT_MULTI_FMT
			} else if (key == NET_IMG_YUV420_NV21) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
				p_img->fmt_type = 1;
#endif

			} else {
				printf("Wrong enter format\n");
				continue;
			}
			break;
		}
#if AI_SUPPORT_MULTI_FMT
	} else if (p_img->fmt == HD_VIDEO_PXLFMT_YUV420 + 1) {
		p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
		p_img->fmt_type = 1;
#endif
	}
	p_img->pa = yuv_out_buffer.phy_addr[0];
	p_img->va = (UINT32)yuv_out_buffer_va;
	return HD_OK;
}

int app_nn_test_proc(CHAR *model_path, CHAR *model_name, CHAR *pattern_name, int width, int height, int channel, int bits, HD_VIDEO_PXLFMT fmt)
{
	HD_RESULT ret;
	int net_id = 0;
	VENDOR_AIS_FLOW_MEM_PARM rslt_mem = {0};
	VENDOR_AIS_FLOW_MAP_MEM_PARM nn_flow_mem_manager;
	VENDOR_AIS_IMG_PARM	src_img = { 0 };
	int model_file_size, model_size;
	NN_LAYER_OUTPUT_INFO *output_layer_info = NULL;
	UINT32 num_output_layer = 0;
	CHAR model_path_file[256];
	CHAR output_bin_path[256];
	
#if AI_SAMPLE_TEST_BATCH
	INT32 BATCH_SIZE = 8;
	UINT32 input_proc_num = 0;
#else
	INT32 BATCH_SIZE = 1;
#endif

	
// set output bin file
	vendor_ais_set_dbg_mode(AI_DUMP_OUT_BIN, 1);
	//vendor_ais_set_dbg_mode(AI_DUMP_INFO, 1);
	
	src_img.width = width;
	src_img.height = height;
	src_img.channel = channel;
	src_img.line_ofs = src_img.width * bits /8;
	src_img.fmt = fmt;

	
	
	//load cnn1 app model file
#if !CNN_25_MATLAB
	sprintf(model_path_file, "%s/%s_APP_CNN1_FR8B.bin", model_path, model_name);
#else
	sprintf(model_path_file, "%s/nvt_model_cnn1_app_%s.bin", model_path, model_name);
#endif

	printf("****** cnn1 app: %s, %s, %dx%dx%d\r\n", model_path_file, pattern_name, width, height, channel);
	ret = mem_init(model_path_file, width * height * _MAX(3, channel) * (bits / 8) * BATCH_SIZE);//avoid YUV420 but only need Y
	if (ret != HD_OK) {
	    printf("mem_init fail = %s\r\n", model_path_file);
	    vendor_ais_net_gen_uninit(net_id);
		mem_uninit();
		return -1;
	}
		
       
   

	model_file_size = vendor_ais_load_file(model_path_file, g_mem.va, &output_layer_info, &num_output_layer);

	if (model_file_size == 0) {
		printf("net load model file fail: %s\r\n", model_path_file);
		return -1;
	}

	model_size = vendor_ais_auto_alloc_mem(&g_mem, &nn_flow_mem_manager);
	printf("      memory real need(%d)\r\n", (int)model_size);
	ret = vendor_ais_net_gen_init(nn_flow_mem_manager, net_id);
	if (ret != HD_OK) {
		printf("net gen init fail=%d\r\n", ret);
		vendor_ais_net_gen_uninit(net_id);
		mem_uninit();
		return -1;
	}

    ret = ai_load_img(pattern_name, &src_img);
    if (ret != HD_OK) {
        printf("ai_load_img fail=%d\n", ret);
		vendor_ais_net_gen_uninit(net_id);
		mem_uninit();
        return -1;
    }
#if AI_SAMPLE_TEST_BATCH
	VENDOR_AIS_IMG_PARM *p_src_img = &src_img;
	UINT32 input_proc_idx[8];
	UINT32 tmp_src_va = p_src_img->va;
	UINT32 tmp_src_pa = p_src_img->pa;
	UINT32 src_img_size = p_src_img->line_ofs * p_src_img->height * p_src_img->channel;
	UINT32 i = 0;
	
	input_proc_num = vendor_ais_net_get_input_layer_index(g_mem, input_proc_idx);
	for (i = 0; i < input_proc_num; i++) {
		if (i > 0) {
			memcpy((VOID*)(p_src_img->va + src_img_size), (VOID*)p_src_img->va, src_img_size);
			hd_common_mem_flush_cache((VOID *)(p_src_img->va + src_img_size), src_img_size);
			p_src_img->pa += src_img_size;
			p_src_img->va += src_img_size;
		}
		if (vendor_ais_net_input_layer_init(p_src_img, i, net_id) != HD_OK) {
			vendor_ais_net_gen_uninit(net_id);
			mem_uninit();
			return -1;
		}
	}
	p_src_img->va = tmp_src_va;
	p_src_img->pa = tmp_src_pa;
#else
	if (vendor_ais_net_input_init(&src_img, net_id) != HD_OK) {
		vendor_ais_net_gen_uninit(net_id);
		mem_uninit();
		return -1;
	}
#endif
	
	memset((VOID *)nn_flow_mem_manager.user_buff.va, 0, nn_flow_mem_manager.user_buff.size);
	hd_common_mem_flush_cache((VOID *)nn_flow_mem_manager.user_buff.va, nn_flow_mem_manager.user_buff.size);

#if USE_NEON
			vendor_ais_proc_net(g_mem, rslt_mem, &src_img, net_id);
#else
			vendor_ais_proc_net(g_mem, rslt_mem, net_id);
#endif

	
#if !CNN_25_MATLAB
	sprintf(output_bin_path, "mv output0 output_bin/%s_APP_CNN1_FR8B", model_name);
#else
	sprintf(output_bin_path, "mv output0 output_bin/%s_cnn1_app", model_name);
#endif
	system(output_bin_path);
#if AI_SAMPLE_TEST_BATCH
	for (i = 0; i < input_proc_num; i++) {
		ret = vendor_ais_net_input_layer_uninit(i, net_id);
		if(ret != HD_OK) {
			printf("vendor_ais_net_input_uninit (%d)\n\r", ret);
		}
	}
#else
	vendor_ais_net_input_uninit(net_id);
#endif
	vendor_ais_net_gen_uninit(net_id);
    mem_uninit();

	
	
	//load cnn1 ll model file
#if !CNN_25_MATLAB
	sprintf(model_path_file, "%s/%s_LL_CNN1_FR8B.bin", model_path, model_name);
#else
	sprintf(model_path_file, "%s/nvt_model_cnn1_ll_%s.bin", model_path, model_name);
#endif
	printf("****** cnn1 ll: %s, %s, %dx%dx%d\r\n", model_path_file, pattern_name, width, height, channel);
	ret = mem_init(model_path_file, width * height  * _MAX(3,channel) * (bits / 8 ) * BATCH_SIZE);//avoid YUV420 but only need Y
	if (ret != HD_OK) {
	    printf("mem_init fail = %s\r\n", model_path_file);
	    vendor_ais_net_gen_uninit(net_id);
		mem_uninit();
	    return -1;
	}

	model_file_size = vendor_ais_load_file(model_path_file, g_mem.va, &output_layer_info, &num_output_layer);
	if (model_file_size == 0) {
		printf("net load model file fail: %s\r\n", model_path_file);
		return -1;
	}

	model_size = vendor_ais_auto_alloc_mem(&g_mem, &nn_flow_mem_manager);
	printf("      memory real need(%d)\r\n", (int)model_size);
	ret = vendor_ais_net_gen_init(nn_flow_mem_manager, net_id);
	if (ret != HD_OK) {
		printf("net gen init fail=%d\r\n", ret);
		vendor_ais_net_gen_uninit(net_id);
		mem_uninit();
		return -1;
	}

    ret = ai_load_img(pattern_name, &src_img);
    if (ret != HD_OK) {
        printf("ai_load_img fail=%d\n", ret);
		mem_uninit();
        return -1;
    }
	
#if AI_SAMPLE_TEST_BATCH
		i = 0;
		
		input_proc_num = vendor_ais_net_get_input_layer_index(g_mem, input_proc_idx);
		printf("=========input_proc_num ========== (%d)\n\r", (int)input_proc_num);
		for (i = 0; i < input_proc_num; i++) {
			if (i > 0) {
				memcpy((VOID*)(p_src_img->va + src_img_size), (VOID*)p_src_img->va, src_img_size);
				hd_common_mem_flush_cache((VOID *)(p_src_img->va + src_img_size), src_img_size);
				p_src_img->pa += src_img_size;
				p_src_img->va += src_img_size;
			}
			if (vendor_ais_net_input_layer_init(p_src_img, i, net_id) != HD_OK) {
				printf("error=========vendor_ais_net_input_layer_init ==========\n\r");
				vendor_ais_net_gen_uninit(net_id);
				mem_uninit();
			
				return -1;
			}
		}
		p_src_img->va = tmp_src_va;
		p_src_img->pa = tmp_src_pa;
#else
	if (vendor_ais_net_input_init(&src_img, net_id) != HD_OK) {
		vendor_ais_net_gen_uninit(net_id);
		mem_uninit();
		return -1;
	}
#endif

	memset((VOID *)nn_flow_mem_manager.user_buff.va, 0, nn_flow_mem_manager.user_buff.size);
	hd_common_mem_flush_cache((VOID *)nn_flow_mem_manager.user_buff.va, nn_flow_mem_manager.user_buff.size);
	
#if USE_NEON
	vendor_ais_proc_net(g_mem, rslt_mem, &src_img, net_id);
#else
	vendor_ais_proc_net(g_mem, rslt_mem, net_id);
#endif
#if !CNN_25_MATLAB
	sprintf(output_bin_path, "mv output0 output_bin/%s_LL_CNN1_FR8B", model_name);
#else
	sprintf(output_bin_path, "mv output0 output_bin/%s_cnn1_ll", model_name);
#endif
	system(output_bin_path);
#if AI_SAMPLE_TEST_BATCH
	for (i = 0; i < input_proc_num; i++) {
		ret = vendor_ais_net_input_layer_uninit(i, net_id);
		if(ret != HD_OK) {
			printf("ERROR : vendor_ais_net_input_uninit (%d)\n\r", ret);
		}
	}
#else
	vendor_ais_net_input_uninit(net_id);
#endif
	vendor_ais_net_gen_uninit(net_id);
    mem_uninit();
	
	
	
	return 0;
}

/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
	HD_RESULT ret;
	FILE *fp = NULL;
	UINT32 i = 0;
	CHAR model_name[50] = { 0 };
	CHAR img_path[256] = { 0 };
	UINT32 model_num = 0;
  	int img_w = 0, img_h = 0, ch = 0, bits = 0;
	CHAR fmt[50] = { 0 };
	HD_VIDEO_PXLFMT img_fmt = 0;
	
	ret	= hd_common_init(0);
	if (ret != HD_OK) {
		printf("hd_common_init fail=%d\n", ret);
		goto exit;
	}
#if (DEVICE_SETTING == DEVICE_52X)
	hd_common_sysconfig(0, (1<<16), 0, VENDOR_AI_CFG); //enable AI engine
#endif

	if (check_driver() == FALSE) {
		system("modprobe kdrv_ai.ko");
		system("modprobe netflowsample.ko");
	}

	system("rm -rf output0");
	system("rm -rf output_bin");
	system("mkdir output_bin");

	//read model num from file
	fp = fopen("model_info.txt", "r");
	if (fp == NULL) {
		printf("open model_info fail.\r\n");
		goto exit;
	}
	fscanf(fp, "total_model_num = %ld\n\n", &model_num);
	printf("total_model_num= %ld\r\n", model_num);

	for (i = 0; i < model_num; i++) {
		//read model name
		fscanf(fp, "%s %s %d %d %d %d %s\n", model_name, img_path, &img_w, &img_h, &ch, &bits, fmt);

		if (img_w <= 0) {
			printf("img_w read fail\r\n");
			goto exit;
		}
		if (img_h <= 0) {
			printf("img_h read fail\r\n");
			goto exit;
		}
		if (ch <= 0) {
			printf("ch read fail\r\n");
			goto exit;
		}
		if (bits <=0 || bits > 32 || bits % 8 != 0){
			printf("bits read fail\r\n");
			goto exit;
		}
		if (strcmp(fmt, "HD_VIDEO_PXLFMT_YUV420")== 0) {
			img_fmt = 0x520c0420;
		} else if (strcmp(fmt, "HD_VIDEO_PXLFMT_Y8") == 0) {
			img_fmt = 0x51080400;
		} else if (strcmp(fmt, "HD_VIDEO_PXLFMT_RGB888_PLANAR") == 0) {
			img_fmt = 0x23180888;
		} else {
			printf("fmt not enough.\r\n");
			goto exit;
			//more see hd_type.h
		}

		printf("\r\n========== %s ==========\r\n", model_name);
		if (app_nn_test_proc("para/nvt_model", model_name, img_path, img_w, img_h, ch, bits, img_fmt)) {
			printf("fail.\r\n");
			goto exit;
		}
	}
	system("sync");
	
exit:
	if (fp) {
		fclose(fp);
		fp = NULL;
	}
	
	ret = hd_common_uninit();
	if (ret != HD_OK) {
		printf("hd_common_uninit fail=%d\n", ret);
	}

	printf("network test finish...\r\n");
	return ret;
}
