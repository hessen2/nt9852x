/**
	@brief Source file of vendor net application sample using user-space net flow.

	@file net_app_user_sample.c

	@ingroup net_app_sample

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2018.  All rights reserved.
*/

/*-----------------------------------------------------------------------------*/
/* Including Files                                                             */
/*-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <stdlib.h>

#include "hdal.h"
#include "hd_type.h"
#include "hd_common.h"
#include "vendor_ai/vendor_ai.h"
#include "pdcnn_lib.h"
#include "net_util_sample.h"
#include "net_gen_sample/net_gen_sample.h"
#include "net_flow_sample/net_flow_sample.h"
#include "net_pre_sample/net_pre_sample.h"
#include "net_post_sample/net_post_sample.h"
#include "net_flow_user_sample/net_flow_user_sample.h"
#include "net_flow_user_sample/net_layer_sample.h"
//#include "custnn/custnn_lib.h"

#include <sys/time.h>

#define PROF                   ENABLE// DISABLE

#if PROF
	static struct timeval tstart, tend;
	#define PROF_START()    gettimeofday(&tstart, NULL);
	#define PROF_END(msg)   gettimeofday(&tend, NULL);  \
			printf("%s time (us): %lu\r\n", msg,         \
					(tend.tv_sec - tstart.tv_sec) * 1000000 + (tend.tv_usec - tstart.tv_usec));
#else
	#define PROF_START()
	#define PROF_END(msg)
#endif

/*-----------------------------------------------------------------------------*/
/* Constant Definitions                                                        */
/*-----------------------------------------------------------------------------*/
#define AI_IPC              ENABLE

#define DEFAULT_DEVICE          "/dev/" VENDOR_AIS_FLOW_DEV_NAME

#define SAVE_SCALE              DISABLE
#if (!AI_IPC)
#define NN_TOTAL_MEM_SIZE  		(NETWORKS_MEM_SIZE + POSTPROC_MEM_SIZE)
#define YUV_OUT_BUF_SIZE        (3 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT)
#define SCALE_OUT_BUF_SIZE		(2 * MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT)
#else
#define NN_TOTAL_MEM_SIZE       (0x600000)//(NETWORKS_MEM_SIZE + POSTPROC_MEM_SIZE)
#define YUV_OUT_BUF_SIZE        (0x5eec00)
#define SCALE_OUT_BUF_SIZE		(0x3f4800)
#endif


#define VENDOR_AI_CFG  0x000f0000  //ai project config

/*-----------------------------------------------------------------------------*/
/* Type Definitions                                                            */
/*-----------------------------------------------------------------------------*/
/**
	Parameters of network
*/
typedef struct _VENDOR_AIS_NETWORK_PARM {
	CHAR *p_model_path;
	VENDOR_AIS_IMG_PARM	src_img;
	VENDOR_AIS_FLOW_MEM_PARM mem;
	UINT32 run_id;
} VENDOR_AIS_NETWORK_PARM;

typedef struct _IMG_PARAM {
	INT32 width;
	INT32 height;
	INT32 channel;
	UINT32 line_offset;
	HD_VIDEO_PXLFMT ffmt;
} IMG_PARAM;

/*-----------------------------------------------------------------------------*/
/* Local Global Variables                                                      */
/*-----------------------------------------------------------------------------*/
static CHAR model_name[256]	= { "/mnt/sd/CNNLib/para/pdcnn_v4_ll/nvt_model.bin"};
//static UINT32 mem_size	= { 8 * MBYTE};
static VENDOR_AIS_FLOW_MEM_PARM g_mem	= {0};
static HD_VIDEO_FRAME yuv_out_buffer;
static VOID *yuv_out_buffer_va = NULL;
static HD_COMMON_MEM_VB_BLK g_blk_info[3];

static HD_VIDEO_FRAME scale_out_buffer;
static VOID *scale_out_buffer_va = NULL;

/*-----------------------------------------------------------------------------*/
/* Local Functions                                                             */
/*-----------------------------------------------------------------------------*/
BOOL need_ise_resize(VENDOR_AIS_IMG_PARM img, INT32* size)
{
	if (img.width < size[0] || img.height < size[1])
		return TRUE;
	else if (img.width > MAX_FRAME_WIDTH || img.height > MAX_FRAME_HEIGHT)
		return TRUE;
	else
		return FALSE;
}

HD_RESULT ai_read_label(UINT32 addr, UINT32 line_len, const CHAR *filename)
{
	FILE *fd;
	CHAR *p_line = (CHAR *)addr;

	fd = fopen(filename, "r");
	if (!fd) {
		DBG_ERR("cannot read %s\r\n", filename);
		return HD_ERR_NG;
	}

	printf("open %s ok\r\n", filename);

	while (fgets(p_line, line_len, fd) != NULL) {
		p_line[strlen(p_line) - 1] = '\0'; // remove newline character
		p_line += line_len;
	}

	if (fd) {
		fclose(fd);
	}

	return HD_OK;
}

static int mem_init(UINT32 ddr)
{
	HD_RESULT                 ret;
	UINT32                    pa, va;
	HD_COMMON_MEM_DDR_ID      ddr_id = ddr;
	HD_COMMON_MEM_VB_BLK      blk;

	/* Allocate parameter buffer */
#if(AI_IPC)
	HD_COMMON_MEM_INIT_CONFIG mem_cfg = {0};

	if (g_mem.va != 0) {
		printf("err: mem has already been inited\r\n");
		return -1;
	}

	mem_cfg.pool_info[0].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[0].blk_size = NN_TOTAL_MEM_SIZE;
	mem_cfg.pool_info[0].blk_cnt = 1;
	mem_cfg.pool_info[0].ddr_id = DDR_ID0;
	mem_cfg.pool_info[1].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[1].blk_size = YUV_OUT_BUF_SIZE;
	mem_cfg.pool_info[1].blk_cnt = 1;
	mem_cfg.pool_info[1].ddr_id = DDR_ID0;
	//scale img buf
	mem_cfg.pool_info[2].type = HD_COMMON_MEM_CNN_POOL;
	mem_cfg.pool_info[2].blk_size = SCALE_OUT_BUF_SIZE;
	mem_cfg.pool_info[2].blk_cnt = 1;
	mem_cfg.pool_info[2].ddr_id = DDR_ID0;


	ret = hd_common_mem_init(&mem_cfg);
	if (HD_OK != ret) {
		printf("hd_common_mem_init err: %d\r\n", ret);
		return ret;
	}
#else
	ret = hd_common_mem_init(NULL);
	if (HD_OK != ret) {
		printf("hd_common_mem_init err: %d\r\n", ret);
		return ret;
	}
	if (g_mem.va != 0) {
		printf("err: mem has already been inited\r\n");
		return -1;
	}
#endif

	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, NN_TOTAL_MEM_SIZE, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("alg hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	pa = hd_common_mem_blk2pa(blk);
	if (pa == 0) {
		printf("not get buffer, pa=%08x\r\n", (int)pa);
		return -1;
	}
	va = (UINT32)hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE, pa, NN_TOTAL_MEM_SIZE);
	g_blk_info[0] = blk;

	/* Release buffer */
	if (va == 0) {
		ret = hd_common_mem_munmap((void *)va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem unmap fail\r\n");
			return ret;
		}
		return -1;
	}

	g_mem.pa = pa;
	g_mem.va = va;
	g_mem.size = NN_TOTAL_MEM_SIZE;

	/* Allocate scale out buffer */
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, YUV_OUT_BUF_SIZE, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("yuv hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	yuv_out_buffer.phy_addr[0] = hd_common_mem_blk2pa(blk);
	if (yuv_out_buffer.phy_addr[0] == 0) {
		printf("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			printf("yuv_out_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	yuv_out_buffer_va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
						  yuv_out_buffer.phy_addr[0],
						  YUV_OUT_BUF_SIZE);
	g_blk_info[1] = blk;
	printf("Allocate yuv_out_buffer pa(%#lx) va(%p)\n",
			yuv_out_buffer.phy_addr[0], yuv_out_buffer_va);

	/* scale out buffer */
	blk = hd_common_mem_get_block(HD_COMMON_MEM_CNN_POOL, SCALE_OUT_BUF_SIZE, ddr_id);
	if (HD_COMMON_MEM_VB_INVALID_BLK == blk) {
		printf("scale hd_common_mem_get_block fail\r\n");
		ret =  HD_ERR_NG;
		goto exit;
	}
	scale_out_buffer.phy_addr[0] = hd_common_mem_blk2pa(blk);
	if (scale_out_buffer.phy_addr[0] == 0) {
		printf("hd_common_mem_blk2pa fail, blk = %#lx\r\n", blk);
		ret = hd_common_mem_release_block(blk);
		if (ret != HD_OK) {
			printf("scale_out_buffer release fail\r\n");
			return ret;
		}
		return HD_ERR_NG;
	}
	scale_out_buffer_va = hd_common_mem_mmap(HD_COMMON_MEM_MEM_TYPE_CACHE,
						  scale_out_buffer.phy_addr[0],
						  SCALE_OUT_BUF_SIZE);
	g_blk_info[2] = blk;
	printf("Allocate scale_out_buffer pa(%#lx) va(%p)\n",
			scale_out_buffer.phy_addr[0], scale_out_buffer_va);

exit:
	return ret;
}

static HD_RESULT mem_uninit(void)
{
	HD_RESULT ret = HD_OK;

	/* Release in buffer */
	if (g_mem.va) {
		ret = hd_common_mem_munmap((void *)g_mem.va, NN_TOTAL_MEM_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (g_mem.va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}

	ret = hd_common_mem_release_block(g_blk_info[0]);
	if (ret != HD_OK) {
		printf("mem_uninit : (g_mem.pa)hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release scale out buffer */
	if (yuv_out_buffer_va) {
		ret = hd_common_mem_munmap(yuv_out_buffer_va, YUV_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (yuv_out_buffer_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}
	
	ret = hd_common_mem_release_block(g_blk_info[1]);
	if (ret != HD_OK) {
		printf("mem_uninit : (yuv_out_buffer.phy_addr[0])hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	/* Release scale out buffer */
	if (scale_out_buffer_va) {
		ret = hd_common_mem_munmap(scale_out_buffer_va, SCALE_OUT_BUF_SIZE);
		if (ret != HD_OK) {
			printf("mem_uninit : (scale_out_buffer_va)hd_common_mem_munmap fail.\r\n");
			return ret;
		}
	}

	ret = hd_common_mem_release_block(g_blk_info[2]);
	if (ret != HD_OK) {
		printf("mem_uninit : (scale_out_buffer.phy_addr[0])hd_common_mem_release_block fail.\r\n");
		return ret;
	}

	return hd_common_mem_uninit();
}

static UINT32 ai_load_file(CHAR *p_filename, UINT32 va)
{
	FILE  *fd;
	UINT32 file_size = 0, read_size = 0;
	const UINT32 model_addr = va;
	//DBG_DUMP("model addr = %08x\r\n", (int)model_addr);

	fd = fopen(p_filename, "rb");
	if (!fd) {
		printf("cannot read %s\r\n", p_filename);
		return 0;
	}

	fseek ( fd, 0, SEEK_END );
	file_size = ALIGN_CEIL_4( ftell(fd) );
	fseek ( fd, 0, SEEK_SET );

	read_size = fread ((void *)model_addr, 1, file_size, fd);
	if (read_size != file_size) {
		printf("size mismatch, real = %d, idea = %d\r\n", (int)read_size, (int)file_size);
	}
	fclose(fd);
	return read_size;
}

static HD_RESULT ai_load_img(CHAR *filename, VENDOR_AIS_IMG_PARM *p_img)
{
	INT32 file_len;
	UINT32 key = 0;

	file_len = vendor_ais_loadbin((UINT32)yuv_out_buffer_va, filename);
	if (file_len < 0) {
		return HD_ERR_IO;
	}

	if (p_img->width == 0) {
		while (TRUE) {
			printf("Enter width = ?\r\n");
			if (scanf("%hu", &p_img->width) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->height == 0) {
		while (TRUE) {
			printf("Enter height = ?\r\n");
			if (scanf("%hu", &p_img->height) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->channel == 0) {
		while (TRUE) {
			printf("Enter channel = ?\r\n");
			if (scanf("%hu", &p_img->channel) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->line_ofs == 0) {
		while (TRUE) {
			printf("Enter line offset = ?\r\n");
			if (scanf("%lu", &p_img->line_ofs) != 1) {
				printf("Wrong input\n");
				continue;
			} else {
				break;
			}
		}
	}
	if (p_img->fmt == 0) {
		while (TRUE) {
			printf("Enter image format = ?\r\n");
			printf("[%d] RGB888 (0x%08x)\r\n", NET_IMG_RGB_PLANE, HD_VIDEO_PXLFMT_RGB888_PLANAR);
			printf("[%d] YUV420 (0x%08x)\r\n", NET_IMG_YUV420, HD_VIDEO_PXLFMT_YUV420);
			printf("[%d] YUV422 (0x%08x)\r\n", NET_IMG_YUV422, HD_VIDEO_PXLFMT_YUV422);
			printf("[%d] Y only (0x%08x)\r\n", NET_IMG_YONLY, HD_VIDEO_PXLFMT_Y8);

			if (scanf("%lu", &key) != 1) {
				printf("Wrong input\n");
				continue;
			} else if (key == NET_IMG_YUV420) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV420;
			} else if (key == NET_IMG_YUV422) {
				p_img->fmt = HD_VIDEO_PXLFMT_YUV422;
			} else if (key == NET_IMG_RGB_PLANE) {
				p_img->fmt = HD_VIDEO_PXLFMT_RGB888_PLANAR;
			} else if (key == NET_IMG_YONLY) {
				p_img->fmt = HD_VIDEO_PXLFMT_Y8;
			} else {
				printf("Wrong enter format\n");
				continue;
			}
			break;
		}
	}
	p_img->pa = yuv_out_buffer.phy_addr[0];
	p_img->va = (UINT32)yuv_out_buffer_va;
	return HD_OK;
}


static VOID *nn_thread_api(VOID *arg)
{
	HD_RESULT ret;
	VENDOR_AIS_NETWORK_PARM *p_net_parm = (VENDOR_AIS_NETWORK_PARM*)arg;
	VENDOR_AIS_IMG_PARM	src_img = {0};
	HD_GFX_IMG_BUF gfx_img = {0};
	VENDOR_AIS_IMG_PARM	*p_src_img;
	VENDOR_AIS_FLOW_MEM_PARM *p_tot_mem = &p_net_parm->mem;
	UINT32 net_id = p_net_parm->run_id;
	UINT32 file_size = 0, model_size = 0;
	UINT32 load_addr = p_tot_mem->va;
	VENDOR_AIS_FLOW_MAP_MEM_PARM mem_manager;
	UINT32 req_size = 0;
	
	UINT32 all_time = 0;
	INT32 img_num = 0;
	#if SAVE_SCALE
	CHAR BMP_FILE[256];
	#endif
	CHAR IMG_PATH[256];
	CHAR IMG_FILE[512];
	CHAR SAVE_TXT[256];
	CHAR IMG_LIST[256];
	CHAR list_infor[256];
	CHAR *line_infor;
	BOOL INPUT_STATE = TRUE;

	PDCNN_MEM pdcnn_mem;

	FLOAT score_thr = 0.45, nms_thr = 0.2;
	LIMIT_PARAM limit_param;
	CHAR para_file[] = "/mnt/sd/CNNLib/para/pdcnn_v4_ll/para.txt";
	
	PROPOSAL_PARAM proposal_params;
	proposal_params.score_thres = score_thr;
	proposal_params.nms_thres = nms_thr;
	limit_param.sm_thr_num = 6;
	proposal_params.run_id = net_id;

	if (p_net_parm->p_model_path == NULL) {
		printf("input network model is null\r\n");
		return 0;
	}

	//load file
	file_size = ai_load_file(p_net_parm->p_model_path, load_addr);
	if (file_size == 0) {
		printf("net load file fail: %s\r\n", p_net_parm->p_model_path);
		return 0;
	}

	model_size = vendor_ais_auto_alloc_mem(p_tot_mem, &mem_manager);
	get_pdcnn_mem(p_tot_mem, &pdcnn_mem, model_size);
	BACKBONE_OUT* backbone_outputs = (BACKBONE_OUT*)pdcnn_mem.backbone_output.va;

	req_size = pdcnn_mem.model.size + pdcnn_mem.out_result.size + pdcnn_mem.pps_result.size + pdcnn_mem.procnet_result.size + pdcnn_mem.reshape_cls.size + pdcnn_mem.backbone_output.size;
	if (req_size > p_tot_mem->size) {
		printf("memory is not enough(%d), need(%d)\r\n", (int)p_tot_mem->size, (int)req_size);
		return 0;
	}
	
	memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer

	ret = vendor_ais_net_gen_init(mem_manager, net_id);
	if (ret != HD_OK) {
		printf("net gen init fail=%d\r\n", ret);
		goto gen_init_fail;
	} else {
		printf("[ai sample] init done: %s!\n", p_net_parm->p_model_path);
	}

	ret = pdcnn_init(&proposal_params, &pdcnn_mem, backbone_outputs, &limit_param, para_file);
	if(ret != HD_OK){
		printf("ERR: pdcnn init fail!\r\n");
		goto gen_init_fail;
	}

	sprintf(IMG_LIST, "/mnt/sd/jpg/image_list.txt");
	
	src_img.channel = 2;
	src_img.fmt = 0x520c0420;

	sprintf(IMG_PATH, "/mnt/sd/jpg/test");

	FILE *fs, *fr;

    sprintf(SAVE_TXT, "/mnt/sd/det_results/test_results.txt");
	
	fr = fopen(IMG_LIST, "r");
	fs = fopen(SAVE_TXT, "w+");

	INT32 len = 0;
	CHAR img_name[256]={0};
	CHAR *token;
	INT32 sl = 0;
	BOOL iseresize = FALSE;

	if(NULL == fr)
	{
		printf("Failed to open img_list!\r\n");	
	} 
	while(fgets(list_infor, 256, fr) != NULL)
	{
		len = strlen(list_infor);
		list_infor[len - 1] = '\0';
		sl = 0;
		line_infor = list_infor;

		while((token = strtok(line_infor, " ")) != NULL)
		{
			if(sl > 2){
				break;
			}
			if (sl == 0){
				strcpy(img_name, token);
				sprintf(IMG_FILE, "%s/%s", IMG_PATH, token);
				printf("%s ", token);
			}
			if (sl == 1){
				src_img.width = atoi(token);
				src_img.line_ofs = ALIGN_CEIL_4(src_img.width);
				printf("%s ", token);
			}
			if (sl == 2){
				src_img.height = atoi(token);
				printf("%s\r\n", token);
			}
			line_infor = NULL;
			sl++;
		}

		limit_param.ratiow = (FLOAT)src_img.width / (FLOAT)proposal_params.input_size[0];
		limit_param.ratioh = (FLOAT)src_img.height / (FLOAT)proposal_params.input_size[1];

		ret = ai_load_img(IMG_FILE, &src_img);
		if (ret != HD_OK) {
			DBG_ERR("ai_load_img fail=%d\n", ret);
		}
		iseresize = need_ise_resize(src_img, proposal_params.input_size);
		if(iseresize)
		{		
			gfx_img.dim.w = proposal_params.input_size[0];
			gfx_img.dim.h = proposal_params.input_size[1];
			gfx_img.format = src_img.fmt;
			gfx_img.lineoffset[0] = ALIGN_CEIL_4(proposal_params.input_size[0]);
			gfx_img.lineoffset[1] = ALIGN_CEIL_4(proposal_params.input_size[0]);
			gfx_img.p_phy_addr[0] = scale_out_buffer.phy_addr[0];
			gfx_img.p_phy_addr[1] = scale_out_buffer.phy_addr[0] + proposal_params.input_size[0] * proposal_params.input_size[1];
			//printf("scale image width: %d, height: %d\r\n", gfx_img.dim.w, gfx_img.dim.h);
			ret = ai_crop_img(&gfx_img, src_img, HD_GFX_SCALE_QUALITY_BILINEAR);
			if (ret != HD_OK) {
				DBG_ERR("ai_crop_img fail=%d\n", ret);
			}
			gfx_img_to_vendor(p_src_img, &gfx_img, (UINT32)scale_out_buffer_va);

#if SAVE_SCALE
			FILE *fb;
			sprintf(BMP_FILE, "/mnt/sd/save_bmp/%s_scale.bin", img_name);
			fb = fopen(BMP_FILE, "wb+");
			fwrite((UINT32 *)scale_out_buffer_va, sizeof(UINT32), (gfx_img.dim.h * gfx_img.dim.w + gfx_img.dim.h * gfx_img.dim.w / 2), fb);
			fclose(fb);
#endif
		} 
		else {
			p_src_img = &src_img;
		}

		if (vendor_ais_net_input_init(p_src_img, net_id) != HD_OK) {
			goto gen_init_fail;
		}

		memset((VOID *)mem_manager.user_buff.va, 0, mem_manager.user_buff.size);    // clear io buffer
		hd_common_mem_flush_cache((VOID *)mem_manager.user_buff.va, mem_manager.user_buff.size);
		PROF_START();
		ret = pdcnn_process(&proposal_params, &pdcnn_mem, &limit_param, src_img, backbone_outputs);
		if(ret != HD_OK){
			printf("pdcnn_process fail!\r\n");
			goto gen_init_fail;
		}
		PROF_END("PDCNN");
		all_time += (tend.tv_sec - tstart.tv_sec) * 1000000 + (tend.tv_usec - tstart.tv_usec); 

		PDCNN_RESULT *final_out_results = (PDCNN_RESULT*)pdcnn_mem.out_result.va;
		for(INT32 num = 0; num < pdcnn_mem.out_num; num++){
			FLOAT xmin = final_out_results[num].x1 * limit_param.ratiow;
			FLOAT ymin = final_out_results[num].y1 * limit_param.ratioh;
			FLOAT width = final_out_results[num].x2 * limit_param.ratiow - xmin;
			FLOAT height = final_out_results[num].y2 * limit_param.ratioh - ymin;
			FLOAT score = final_out_results[num].score;
				
			printf("obj information is: (socre: %f [%f %f %f %f])\r\n", score, xmin, ymin, width, height);
			fprintf(fs, "%s %f %f %f %f %f\r\n", img_name, score, xmin, ymin, width, height);
		}
		
		img_num++;
		
		vendor_ais_net_input_uninit(net_id);
	}
	//ai_input_uninit(&first_iomem, mem_manager.user_parm, net_id);
	if (INPUT_STATE == TRUE){
    	printf("all test img number is: %d\r\n", img_num);
	}
	fclose(fs);
	fclose(fr);
	printf("mean_time: %d\r\n", all_time / img_num);
	
gen_init_fail:
	vendor_ais_net_gen_uninit(net_id);

	return 0;
}

/*-----------------------------------------------------------------------------*/
/* Interface Functions                                                         */
/*-----------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
	VENDOR_AIS_FLOW_MEM_PARM mem_parm = {0};
	VENDOR_AIS_NETWORK_PARM net_parm = {0};
	pthread_t nn_thread_id;
	HD_RESULT           ret;
	UINT32 ddr_id = 0;
	//INT32 param_num = 0;
	//int idx = 0;
	/*
	if(argc < 2){
		DBG_WRN("usage : net_app_user_sample wrong input\n");
		return -1;
	}*/
	// get images info
	/*
	param_num = 2;
	if (argc > param_num) {
		sscanf(argv[param_num++], "%hu", &img_para.channel);
	}
	if (argc > param_num) {
		sscanf(argv[param_num++], "%x", &img_para.ffmt);
	}*/

#if(AI_IPC)
	ret = hd_common_init(0);
#else
	ret = hd_common_init(2);
#endif
	if (ret != HD_OK) {
		DBG_ERR("hd_common_init fail=%d\n", ret);
		goto exit;
	}

	ret = hd_gfx_init();
	if (ret != HD_OK) {
		printf("hd_gfx_init fail\r\n");
		goto exit;
	}
	
	//set project config for AI
	hd_common_sysconfig(0, (1<<16), 0, VENDOR_AI_CFG); //enable AI engine

	// init memory
	ret = mem_init(ddr_id);
	if (ret != HD_OK) {
		DBG_ERR("mem_init fail=%d\n", ret);
		goto exit;
	}

	ret = hd_videoproc_init();
	if (ret != HD_OK) {
		DBG_ERR("hd_videoproc_init fail=%d\n", ret);
		goto exit;
	}


	// allocate memory
	mem_parm.pa 	= g_mem.pa;
	mem_parm.va 	= g_mem.va;
	mem_parm.size 	= NN_TOTAL_MEM_SIZE;

	net_parm.mem = mem_parm;
	net_parm.run_id = 0;
	net_parm.p_model_path = model_name;
	ret = pthread_create(&nn_thread_id, NULL, nn_thread_api, (VOID*)(&net_parm));
	if (ret < 0) {
		DBG_ERR("create encode thread failed");
		goto exit;
	}
	
	pthread_join(nn_thread_id, NULL);

exit:
	ret = hd_gfx_uninit();
	if (ret != HD_OK) {
		printf("hd_gfx_uninit fail\r\n");
	}

	ret = hd_videoproc_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_videoproc_uninit fail=%d\n", ret);
	}

	ret = mem_uninit();
	if (ret != HD_OK) {
		DBG_ERR("mem_uninit fail=%d\n", ret);
	}

	ret = hd_common_uninit();
	if (ret != HD_OK) {
		DBG_ERR("hd_common_uninit fail=%d\n", ret);
	}

	printf("network test finish...\r\n");

	return ret;
}
